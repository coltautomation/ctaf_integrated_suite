package baseClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class Configuration{

	public static String URL_INFO = null;
	// public static String PASSWORD= properties.getProperty("password");
	private static GlobalVariables glb = new GlobalVariables();
	static Properties prop = new Properties();
	static GlobalVariables g = new GlobalVariables();

	public static String C4C_URL, SalesUserC4C_Username, SalesUserC4C_Password, SalesUserCPQ_Username, SalesUserCPQ_Password, CPQ_URL, SEUser_Username, SEUser_Password, CSTUser_Username, CSTUser_Password, ContactDetails_URL,EXPLORE_URL,EXPLORE_Offnet_User,EXPLORE_Nearnet_User,
	EXPLORE_Password,PSUser_Username,PSUser_Password,ADUser_Username,DPUser_Username,DPUser_Password,VP_SalesUser1_Username,VP_SalesUser1_Password,VP_SalesUser2_Username,VP_SalesUser2_Password,QT_Username,QT_Password,
	SiebelUser_Username,SiebelUser_Password,Siebel_URL,CEOS_URL,CEOS_Username,CEOS_Password,OMP_URL, OMP_Username, OMP_Password,NH_B2B_URL,NH_B2B_Username,NH_B2B_Password,NMTS_URL,NMTS_Username,NMTS_Password,APT_URL, APT_Username, APT_Password,Premise_URL, Premise_Username, Premise_Password,
	XNG_URL,XNG_Username,XNG_Password,NC_URL, NC_Username, NC_Password,NOD_URL,NOD_Username,NOD_Password,NOD_ExtUsername,NOD_ExtPassword,User_Type,Agent_Username,CAM_Username,Agent_Password,CAM_Password,AD_Password,Novitas_Username,Novitas_Password,Novitas_URL,NC_SyncLog;
	
	public static void setUrlInfoDataSet(String automationSuite) throws IOException, InterruptedException {
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
		{

	        // load a properties file
	        prop.load(input);               

	    }
		catch (IOException ex) 
		{
	        ex.printStackTrace();
	    }
				
		 String Environment = prop.getProperty("Environment");
		 
		 switch (automationSuite) {
			case "CPQ Quote Processing":
				
				C4C_URL = DataMiner.fngetconfigvalue(Environment, "C4C_URL");
				 System.out.println("C4C_URL in Configuratio class: "+ C4C_URL);
				 SalesUserC4C_Username = DataMiner.fngetconfigvalue(Environment, "Sales_User");
				 SalesUserC4C_Password = DataMiner.fngetconfigvalue( Environment, "SalesC4C_Password");
				 SalesUserCPQ_Username = DataMiner.fngetconfigvalue( Environment, "Sales_User");
				 SalesUserCPQ_Password = DataMiner.fngetconfigvalue(Environment, "SalesCPQ_Password");
				 QT_Username = DataMiner.fngetconfigvalue(Environment, "QT_User");
				 QT_Password = DataMiner.fngetconfigvalue(Environment, "QT_Password");
			
				 CPQ_URL = DataMiner.fngetconfigvalue(Environment, "CPQ_URL");
				 SEUser_Username = DataMiner.fngetconfigvalue(Environment, "SE_User");
				 SEUser_Password = DataMiner.fngetconfigvalue( Environment, "SEUser_Password");
				 CSTUser_Username = DataMiner.fngetconfigvalue(Environment, "CST_User");
				 CSTUser_Password = DataMiner.fngetconfigvalue( Environment, "CSTUser_Password");
				 PSUser_Username = DataMiner.fngetconfigvalue( Environment, "PS_User");
				 PSUser_Password = DataMiner.fngetconfigvalue( Environment, "PSUser_Password");
				 DPUser_Username = DataMiner.fngetconfigvalue(Environment, "DP_User");
				 DPUser_Password = DataMiner.fngetconfigvalue( Environment, "DPUser_Password");
				 VP_SalesUser1_Username = DataMiner.fngetconfigvalue( Environment, "VP_User1");
				 VP_SalesUser1_Password = DataMiner.fngetconfigvalue( Environment, "VP_SalesUser1_Password");
				 VP_SalesUser2_Username = DataMiner.fngetconfigvalue( Environment, "VP_User2");
				 VP_SalesUser2_Password = DataMiner.fngetconfigvalue( Environment, "VP_SalesUser2_Password");
				 User_Type = DataMiner.fngetconfigvalue( Environment, "User_Type");
				 Agent_Username = DataMiner.fngetconfigvalue( Environment, "Agent_User");
				 Agent_Password = DataMiner.fngetconfigvalue( Environment, "Agent_Password");
				 CAM_Username = DataMiner.fngetconfigvalue( Environment, "CAM_User");
				 CAM_Password = DataMiner.fngetconfigvalue( Environment, "CAM_Password");
				 ContactDetails_URL = DataMiner.fngetconfigvalue( Environment, "ContactDetails_URL");
				 
				 EXPLORE_URL = DataMiner.fngetconfigvalue( Environment, "Explore_URL");
				 EXPLORE_Offnet_User = DataMiner.fngetconfigvalue( Environment, "Explore_Offnet_User");
				 EXPLORE_Nearnet_User = DataMiner.fngetconfigvalue(Environment, "Explore_Nearnet_User");
				 EXPLORE_Password = DataMiner.fngetconfigvalue( Environment, "Explore_Password");
				
				 ADUser_Username = DataMiner.fngetconfigvalue( Environment, "AD_User");
				 AD_Password = DataMiner.fngetconfigvalue( Environment, "AD_Password");		
				
				 break;
			case "Siebel Order Management":
				
				Siebel_URL = DataMiner.fngetconfigvalue( Environment, "Siebel_URL");
				SiebelUser_Username = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Username");
				SiebelUser_Password = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Password");
				
				CEOS_URL = DataMiner.fngetconfigvalue(Environment, "CEOS_URL");
				CEOS_Username = DataMiner.fngetconfigvalue( Environment, "CEOS_Username");
				CEOS_Password = DataMiner.fngetconfigvalue( Environment, "CEOS_Password");
				
				OMP_URL = DataMiner.fngetconfigvalue(Environment, "OMP_URL");
				OMP_Username = DataMiner.fngetconfigvalue( Environment, "OMP_Username");
				OMP_Password = DataMiner.fngetconfigvalue( Environment, "OMP_Password");
				
				Premise_URL = DataMiner.fngetconfigvalue( Environment, "Premise_URL");
				Premise_Username = DataMiner.fngetconfigvalue( Environment, "Premise_Username");
				Premise_Password = DataMiner.fngetconfigvalue(Environment, "Premise_Password");
				
				XNG_URL = DataMiner.fngetconfigvalue(Environment, "XNG_URL");
				XNG_Username = DataMiner.fngetconfigvalue( Environment, "XNG_Username");
				XNG_Password = DataMiner.fngetconfigvalue( Environment, "XNG_Password");
				
				break;
			case "NMTS Number Management":
				
				NMTS_URL = DataMiner.fngetconfigvalue( Environment, "URL");
				NMTS_Username = DataMiner.fngetconfigvalue( Environment, "Username");
				NMTS_Password = DataMiner.fngetconfigvalue( Environment, "Password");
				
				break;
			case "APT Service Provisioning":
				
				APT_URL = DataMiner.fngetconfigvalue( Environment, "URL");
				APT_Username = DataMiner.fngetconfigvalue(Environment, "Username");
				APT_Password = DataMiner.fngetconfigvalue( Environment, "Password");
				
				break;
			case "NC Standalone Service Provisioning":
				
				NC_URL= DataMiner.fngetconfigvalue( Environment,"NC_URL");
				NC_Username= DataMiner.fngetconfigvalue( Environment, "NC_Username");
				NC_Password= DataMiner.fngetconfigvalue(Environment, "NC_Password");
				
				Siebel_URL = DataMiner.fngetconfigvalue( Environment, "Siebel_URL");
				SiebelUser_Username = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Username");
				SiebelUser_Password = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Password");
				
				CEOS_URL = DataMiner.fngetconfigvalue(Environment, "CEOS_URL");
				CEOS_Username = DataMiner.fngetconfigvalue( Environment, "CEOS_Username");
				CEOS_Password = DataMiner.fngetconfigvalue( Environment, "CEOS_Password");
				
				break;
			case "Number on Demand(NOD)":
				
				NOD_URL = DataMiner.fngetconfigvalue(Environment, "URL");
				NOD_Username = DataMiner.fngetconfigvalue(Environment, "Username");
				NOD_Password = DataMiner.fngetconfigvalue( Environment, "Password");
				NOD_ExtUsername = DataMiner.fngetconfigvalue(Environment, "ExtUsername");
				NOD_ExtPassword = DataMiner.fngetconfigvalue( Environment, "ExtPassword");
				
				break;
			case "SiebelNC Integrated Service Provisioning":
				
				NC_URL= DataMiner.fngetconfigvalue( Environment,"NC_URL");
				NC_Username= DataMiner.fngetconfigvalue( Environment, "NC_Username");
				NC_Password= DataMiner.fngetconfigvalue(Environment, "NC_Password");
				
				Siebel_URL = DataMiner.fngetconfigvalue( Environment, "Siebel_URL");
				SiebelUser_Username = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Username");
				SiebelUser_Password = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Password");
				
				CEOS_URL = DataMiner.fngetconfigvalue(Environment, "CEOS_URL");
				CEOS_Username = DataMiner.fngetconfigvalue( Environment, "CEOS_Username");
				CEOS_Password = DataMiner.fngetconfigvalue( Environment, "CEOS_Password");
				
				break;
				
           case "Novitas":
				
				NC_URL= DataMiner.fngetconfigvalue( Environment,"NC_URL");
				NC_Username= DataMiner.fngetconfigvalue( Environment, "NC_Username");
				NC_Password= DataMiner.fngetconfigvalue(Environment, "NC_Password");
				NC_SyncLog = DataMiner.fngetconfigvalue(Environment, "NC_SyncLog");

				
				Novitas_Username = DataMiner.fngetconfigvalue( Environment, "Novitas_Username");
				Novitas_Password = DataMiner.fngetconfigvalue( Environment, "Novitas_Password");
				Novitas_URL = DataMiner.fngetconfigvalue( Environment, "Novitas_URL");
				
				
				
				break;
           case "Siebel Galileo":
				
				Siebel_URL = DataMiner.fngetconfigvalue( Environment, "Siebel_URL");
				SiebelUser_Username = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Username");
				SiebelUser_Password = DataMiner.fngetconfigvalue( Environment, "SiebelUser_Password");
				break;
				
			default: 
				Report.LogInfo("AutomationSuite", "Unsupported automation suite selected", "INFO");
				break;
			}	

		
	}

	
}