package baseClasses;

import com.sun.org.apache.xerces.internal.parsers.SAXParser;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class LowMemoryExcelFileReader extends MainDriver
{
	 private String file;

	    public LowMemoryExcelFileReader(String file) {
	        this.file = file;
	    }

	    public List<String[]> read() {
	        try {
	            return processFirstSheet(file);
	        } catch (Exception e) {
	           throw new RuntimeException(e);
	        }
	    }

	    public List<String []> readSheet(Sheet sheet) {
	        List<String []> res = new LinkedList<>();
	        Iterator<Row> rowIterator = sheet.rowIterator();

	        while (rowIterator.hasNext()) {
	            Row row = rowIterator.next();
	            int cellsNumber = row.getLastCellNum();
	            String [] cellsValues = new String[cellsNumber];

	            Iterator<Cell> cellIterator = row.cellIterator();
	            int cellIndex = 0;

	            while (cellIterator.hasNext()) {
	                Cell cell = cellIterator.next();
	                cellsValues[cellIndex++] = cell.getStringCellValue();
	            }

	            res.add(cellsValues);
	        }
	        return res;
	    }

	    public String getFile() {
	        return file;
	    }

	    public void setFile(String file) {
	        this.file = file;
	    }

	    private List<String []> processFirstSheet(String filename) throws Exception {
	        OPCPackage pkg = OPCPackage.open(filename, PackageAccess.READ);
	        XSSFReader r = new XSSFReader(pkg);
	        SharedStringsTable sst = r.getSharedStringsTable();

	        SheetHandler handler = new SheetHandler(sst);
	        XMLReader parser = fetchSheetParser(handler);
	        Iterator<InputStream> sheetIterator = r.getSheetsData();

	        if (!sheetIterator.hasNext()) {
	            return Collections.emptyList();
	        }

	        InputStream sheetInputStream = sheetIterator.next();
	        BufferedInputStream bisSheet = new BufferedInputStream(sheetInputStream);
	        InputSource sheetSource = new InputSource(bisSheet);
	        parser.parse(sheetSource);
	        List<String []> res = handler.getRowCache();
	        bisSheet.close();
	        return res;
	    }
	    
	    public OPCPackage opcPackage() throws IOException, OpenXML4JException
	    {
	    	OPCPackage pkg = OPCPackage.open(file);
	    		    	
	    	return pkg;
	    }
	    
	    public XSSFReader xssfReader(OPCPackage pkg) throws IOException, OpenXML4JException
	    {
	    	XSSFReader r = new XSSFReader(pkg);	 
	    	
	    	return r;
	    }
	    
	    @SuppressWarnings("deprecation")
	    public ArrayList<HashMap<String, String>> getStreamDataSheetRID(XSSFReader r, String sheetRID) throws IOException, OpenXML4JException, XMLStreamException 
	    {
	
	    	final String ROW = "row";
	    	final String CELL = "c";
	    	final String CELL_TYPE = "t";
	    	final String CELL_TYPE_STRING = "s";
	    	String rowValue = "";
	    	String cellName = "";
	    	
	    	HashMap<String, String> columnsMap = new HashMap<String, String>();	    	
	    	ArrayList<HashMap<String, String>> rowsList = new ArrayList<HashMap<String, String>>();
	    	HashMap<String, String> rowData = new HashMap<String, String>();
	    	
	    	SharedStringsTable sst = r.getSharedStringsTable();

	    	InputStream sheet2 = r.getSheet(sheetRID);

	    	XMLInputFactory fac = XMLInputFactory.newInstance();
	    	XMLStreamReader parser = fac.createXMLStreamReader(sheet2);
	    	String cellType = "";
	    	
	    	while (parser.hasNext()) 
	    	{
	    		int eventType = parser.next();

	    		if (eventType == XMLStreamConstants.START_ELEMENT) 
	    		{
	    			if (parser.getLocalName().equals(ROW)) 
	    			{
	    				//System.out.println();
	    				//System.out.print("row : " + parser.getAttributeValue(0));
	    				rowValue = parser.getAttributeValue(0);
	    				if(!rowValue.equalsIgnoreCase("1") && !rowValue.equalsIgnoreCase("2"))
						{
	    					rowsList.add(rowData);	    					
	    					rowData = new HashMap<String, String>();
						}
	    			} else if (parser.getLocalName().equals(CELL)) 
	    			{
	    				//System.out.print(" | " + parser.getAttributeValue(0));
	    				String cellN = parser.getAttributeValue(0);
	    				cellName = cellN.replaceAll("\\d","");
						cellType = parser.getAttributeValue(2);
	    			}
	    		} else if (eventType == XMLStreamConstants.CDATA| eventType == XMLStreamConstants.CHARACTERS) 
	    		{
	    			if (cellType != null && cellType.equals(CELL_TYPE_STRING)) 
	    			{
	    				String value = parser.getText();
	    					    				
	    				try {
	    						//System.out.print(" = "+ new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));	 
	    						String cValue = new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))).getString();
	    						if(rowValue.equalsIgnoreCase("1"))
	    						{
	    							columnsMap.put(cellName, cValue);
	    						}else
	    						{
	    							rowData.put(columnsMap.get(cellName), cValue);
	    						}
	    					} catch (NumberFormatException ex) 	    				
	    					{
	    						//System.out.print(" = " + value);
	    						if(rowValue.equalsIgnoreCase("1"))
	    						{
	    							columnsMap.put(cellName, value);
	    						}else if (!value.contains("$"))
	    						{
	    							rowData.put(columnsMap.get(cellName), value);
	    						}
	    					}
	    			} else 
	    			{
	    				//System.out.print(" = " + parser.getText());
	    				if(rowValue.equalsIgnoreCase("1"))
						{
							columnsMap.put(cellName, parser.getText());
						}else
						{
							rowData.put(columnsMap.get(cellName), parser.getText());
						}
	    			}
	    		}
	    	}

	    	rowsList.add(rowData);
	    	parser.close();
	    	sheet2.close();	   
	    	return rowsList;
	    }	
	    
	    @SuppressWarnings("deprecation")
	    public ArrayList<HashMap<String, String>> getStreamDataSheetName(XSSFReader r, String sheetName) throws IOException, OpenXML4JException, XMLStreamException 
	    {
	
	    	final String ROW = "row";
	    	final String CELL = "c";
	    	final String CELL_TYPE = "t";
	    	final String CELL_TYPE_STRING = "s";
	    	String rowValue = "";
	    	String cellName = "";
	    	String cValue="";
	    	
	    	HashMap<String, String> columnsMap = new HashMap<String, String>();	    	
	    	ArrayList<HashMap<String, String>> rowsList = new ArrayList<HashMap<String, String>>();
	    	HashMap<String, String> rowData = new HashMap<String, String>();
	    	
	    	SharedStringsTable sst = r.getSharedStringsTable();
	    	
	    	String sheetRID = getSheetRID(r, sheetName);

	    	InputStream sheet2 = r.getSheet(sheetRID);

	    	XMLInputFactory fac = XMLInputFactory.newInstance();
	    	XMLStreamReader parser = fac.createXMLStreamReader(sheet2);
	    	String cellType = "";
	    	
	    	while (parser.hasNext()) 
	    	{
	    		int eventType = parser.next();

	    		if (eventType == XMLStreamConstants.START_ELEMENT) 
	    		{
	    			if (parser.getLocalName().equals(ROW)) 
	    			{
	    				//System.out.println();
	    				//System.out.print("row : " + parser.getAttributeValue(0));
	    				rowValue = parser.getAttributeValue(0);
	    				if(!rowValue.equalsIgnoreCase("1") && !rowValue.equalsIgnoreCase("2"))
						{
	    					rowsList.add(rowData);	    					
	    					rowData = new HashMap<String, String>();
						}
	    			} else if (parser.getLocalName().equals(CELL)) 
	    			{
	    				//System.out.print(" | " + parser.getAttributeValue(0));
	    				String cellN = parser.getAttributeValue(0);
	    				cellName = cellN.replaceAll("\\d","");
						cellType = parser.getAttributeValue(2);
	    			}
	    		} else if (eventType == XMLStreamConstants.CDATA| eventType == XMLStreamConstants.CHARACTERS) 
	    		{
	    			if (cellType != null && cellType.equals(CELL_TYPE_STRING)) 
	    			{
	    				String value = parser.getText();
	    					    				
	    				try {
	    						//System.out.print(" = "+ new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));	 
	    						cValue = new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))).getString();
	    						if(rowValue.equalsIgnoreCase("1"))
	    						{
	    							columnsMap.put(cellName, cValue);
	    						}else 
	    						{
	    							rowData.put(columnsMap.get(cellName), cValue);
	    						}
	    					} catch (NumberFormatException ex) 	    				
	    					{
	    						//System.out.print(" = " + value);
	    						if(rowValue.equalsIgnoreCase("1"))
	    						{
	    							columnsMap.put(cellName, value);
	    						}else if (!value.contains("$"))
	    						{
	    							rowData.put(columnsMap.get(cellName), value);
	    						}
	    					}
	    			} else 
	    			{
	    				//System.out.print(" = " + parser.getText());
	    				if(rowValue.equalsIgnoreCase("1"))
						{
							columnsMap.put(cellName, parser.getText());
						}else
						{
							rowData.put(columnsMap.get(cellName), parser.getText());
						}
	    			}
	    		}
	    	}

	    	rowsList.add(rowData);
	    	parser.close();
	    	sheet2.close();	   
	    	return rowsList;
	    }	
	    
	    @SuppressWarnings("deprecation")
	    public void getAllXLStreamData(String sheetName) throws IOException, OpenXML4JException, XMLStreamException 
	    {
	
	    	final String ROW = "row";
	    	final String CELL = "c";
	    	final String CELL_TYPE = "t";
	    	final String CELL_TYPE_STRING = "s";

	    	OPCPackage pkg = OPCPackage.open(file);
	    	XSSFReader r = new XSSFReader(pkg);
	    	SharedStringsTable sst = r.getSharedStringsTable();
	    	
	    	String sheetRID = getSheetRID(r, sheetName);
	    	
	    	InputStream sheet2 = r.getSheet(sheetRID);

	    	XMLInputFactory fac = XMLInputFactory.newInstance();
	    	XMLStreamReader parser = fac.createXMLStreamReader(sheet2);
	    	String cellType = "";
	    	
	    	while (parser.hasNext()) 
	    	{
	    		int eventType = parser.next();

	    		if (eventType == XMLStreamConstants.START_ELEMENT) 
	    		{
	    			if (parser.getLocalName().equals(ROW)) 
	    			{
	    				System.out.println();
	    				System.out.print("row : " + parser.getAttributeValue(0));
	    			} else if (parser.getLocalName().equals(CELL)) 
	    			{
	    				System.out.print(" | " + parser.getAttributeValue(0));
						cellType = parser.getAttributeValue(2);
	    			}
	    		} else if (eventType == XMLStreamConstants.CDATA| eventType == XMLStreamConstants.CHARACTERS) 
	    		{
	    			if (cellType != null && cellType.equals(CELL_TYPE_STRING)) 
	    			{
	    				String value = parser.getText();
	    				try {
	    						System.out.print(" = "
	    						+ new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));
	    					} catch (NumberFormatException ex) 
	    					{
	    						System.out.print(" = " + value);
	    					}
	    			} else 
	    			{
	    				System.out.print(" = " + parser.getText());
	    			}
	    		}
	    	}

	    	parser.close();
	    	sheet2.close();
	    	pkg.close();
	    }	    


	    public XMLReader fetchSheetParser(ContentHandler handler) throws SAXException {
	        XMLReader parser = new SAXParser();
	        parser.setContentHandler(handler);
	        return parser;
	    }
	    
	    public String getSheetRID(XSSFReader reader, String sheetName) throws InvalidFormatException, IOException {
            String rID = "";
            int sheetCnt = 0;
            
            Iterator<InputStream> sheets = reader.getSheetsData();

            if (sheets instanceof XSSFReader.SheetIterator) {
             XSSFReader.SheetIterator sheetiterator = (XSSFReader.SheetIterator)sheets;

             while (sheetiterator.hasNext()) {
              InputStream dummy = sheetiterator.next();
              
              sheetCnt++;
              String currentName = sheetiterator.getSheetName();
              //System.out.println("Sheet name: "+ currentName);
              
              if(sheetName.equalsIgnoreCase(currentName))
              {
            	  rID = "rId"+sheetCnt;
            	 //System.out.println("Sheet RID: "+ rID);
            	  dummy.close();
            	  break;
              }             
              
             }
            }
	    	
            return rID;
        }
	    
	    public HashMap<String, ArrayList<HashMap<String, String>>> readStreamFullXL(XSSFReader reader) throws IOException, OpenXML4JException, XMLStreamException {
            String rID = "";
            int sheetCnt = 0;
            
          ArrayList<HashMap<String, String>> rowsList = new ArrayList<HashMap<String, String>>();
            
            Iterator<InputStream> sheets = reader.getSheetsData();

            if (sheets instanceof XSSFReader.SheetIterator) {
             XSSFReader.SheetIterator sheetiterator = (XSSFReader.SheetIterator)sheets;

             while (sheetiterator.hasNext()) {
              InputStream dummy = sheetiterator.next();
              
              sheetCnt++;
              String currentName = sheetiterator.getSheetName();
              //System.out.println("Sheet name: "+ currentName);
              
              if(!currentName.equalsIgnoreCase("Master") && !currentName.equalsIgnoreCase("OCN_List"))
              {
            	  rID = "rId"+sheetCnt;
            	  //System.out.println("Sheet RID: "+ rID);
            	  
            	  rowsList = getStreamDataSheetRID(reader, rID);
            	  fullTestDataMap.put(currentName, rowsList);
              }             
              
             }
            }
            return fullTestDataMap;
            
        }
	    
	    @SuppressWarnings("deprecation")
	    public LinkedHashMap<String, String> getColNamesFromSheet(XSSFReader r, String sheetName) throws IOException, OpenXML4JException, XMLStreamException 
	    {
	
	    	final String ROW = "row";
	    	final String CELL = "c";
	    	final String CELL_TYPE = "t";
	    	final String CELL_TYPE_STRING = "s";
	    	String rowValue = "";
	    	String cellName = "";
	    	
	    	LinkedHashMap<String, String> columnsMap = new LinkedHashMap<String, String>();	    	
	    		    	
	    	SharedStringsTable sst = r.getSharedStringsTable();
	    	
	    	String sheetRID = getSheetRID(r, sheetName);

	    	InputStream sheet2 = r.getSheet(sheetRID);

	    	XMLInputFactory fac = XMLInputFactory.newInstance();
	    	XMLStreamReader parser = fac.createXMLStreamReader(sheet2);
	    	String cellType = "";
	    	
	    	while (parser.hasNext()) 
	    	{
	    		int eventType = parser.next();

	    		if (eventType == XMLStreamConstants.START_ELEMENT) 
	    		{
	    			if (parser.getLocalName().equals(ROW)) 
	    			{
	    				//System.out.println();
	    				//System.out.print("row : " + parser.getAttributeValue(0));
	    				rowValue = parser.getAttributeValue(0);
	    				if(!rowValue.equalsIgnoreCase("1"))
						{
	    					break;
						}
	    			} else if (parser.getLocalName().equals(CELL)) 
	    			{
	    				//System.out.print(" | " + parser.getAttributeValue(0));
	    				String cellN = parser.getAttributeValue(0);
	    				cellName = cellN.replaceAll("\\d","");
						cellType = parser.getAttributeValue(2);
	    			}
	    		} else if (eventType == XMLStreamConstants.CDATA| eventType == XMLStreamConstants.CHARACTERS) 
	    		{
	    			if (cellType != null && cellType.equals(CELL_TYPE_STRING)) 
	    			{
	    				String value = parser.getText();
	    					    				
	    				try {
	    						//System.out.print(" = "+ new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));	 
	    						String cValue = new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))).getString();
	    						if(rowValue.equalsIgnoreCase("1"))
	    						{
	    							columnsMap.put(cellName, cValue);
	    						}else
	    						{
	    							break;
	    						}
	    					} catch (NumberFormatException ex) 	    				
	    					{
	    						//System.out.print(" = " + value);
	    						if(rowValue.equalsIgnoreCase("1"))
	    						{
	    							columnsMap.put(cellName, value);
	    						}else
	    						{
	    							break;
	    						}
	    					}
	    			} else 
	    			{
	    				//System.out.print(" = " + parser.getText());
	    				if(rowValue.equalsIgnoreCase("1"))
						{
							columnsMap.put(cellName, parser.getText());
						}else
						{
							break;
						}
	    			}
	    		}
	    	}

	    	parser.close();
	    	sheet2.close();	   
	    	return columnsMap;
	    }

	    /**
	     * See org.xml.sax.helpers.DefaultHandler javadocs
	     */
	    private static class SheetHandler extends DefaultHandler {

	        private static final String ROW_EVENT = "row";
	        private static final String CELL_EVENT = "c";

	        private SharedStringsTable sst;
	        private String lastContents;
	        private boolean nextIsString;

	        private List<String> cellCache = new LinkedList<>();
	        private List<String[]> rowCache = new LinkedList<>();

	        private SheetHandler(SharedStringsTable sst) {
	            this.sst = sst;
	        }

	        public void startElement(String uri, String localName, String name,
	                                 Attributes attributes) throws SAXException {
	            // c => cell
	            if (CELL_EVENT.equals(name)) {
	                String cellType = attributes.getValue("t");
	                if(cellType != null && cellType.equals("s")) {
	                    nextIsString = true;
	                } else {
	                    nextIsString = false;
	                }
	            } else if (ROW_EVENT.equals(name)) {
	                if (!cellCache.isEmpty()) {
	                    rowCache.add(cellCache.toArray(new String[cellCache.size()]));
	                }
	                cellCache.clear();
	            }

	            // Clear contents cache
	            lastContents = "";
	        }

	        public void endElement(String uri, String localName, String name)
	                throws SAXException {
	            // Process the last contents as required.
	            // Do now, as characters() may be called more than once
	            if(nextIsString) {
	                int idx = Integer.parseInt(lastContents);
	                lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
	                nextIsString = false;
	            }

	            // v => contents of a cell
	            // Output after we've seen the string contents
	            if(name.equals("v")) {
	                cellCache.add(lastContents);
	            }
	        }

	        public void characters(char[] ch, int start, int length)
	                throws SAXException {
	            lastContents += new String(ch, start, length);
	        }

	        public List<String[]> getRowCache() {
	            return rowCache;
	        }	        
	    }

}
