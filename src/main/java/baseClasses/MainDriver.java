package baseClasses;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.jvnet.winp.WinProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.testng.TestNG;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class MainDriver 
{
	private static String testsuite=null;
	
	private static String RunBrowser=null;
	private static String Port=null;
	private static String ThreadCount=null;
	private static DocumentBuilderFactory dbFactory=null;
	private static DocumentBuilder dBuilder=null;
	private static Document doc=null;
	private static GlobalVariables g;
	private static String testScenario = null;
	private static Utilities testSuite = new Utilities();
	private static ReadExcelFile readexcel = new ReadExcelFile();
	private static  ReadingAndWritingTextFile readText=new ReadingAndWritingTextFile();
	private static Properties prop = new Properties();
	private static Properties prop2 = new Properties();
	private static Properties prop3 = new Properties();
		
	public static String sheet;
	
	public static String botId;
	public static String testData;
	public static String environment;
	public static String executionType;
	public static String testCases;
	public static String[] testCasesArray;
	public static String automationSuite;
	public static int TotalTestCases;
	public static String gridMode;
	public static String host;
	public static String port;
	public static String elasticIp;
	public static int elasticPort;
	public static String elasticIndex;
	public static Connection con = null;
	public static Statement st = null;
	public static RestHighLevelClient client = null;
	public static String driverEXE;
	public static String browserPIDs;
	public static String cmdPIDs;
	public static String javaPIDs;
	public static String javawPIDs;
	public static WinProcess winProcess;
	public static HashMap<String, ArrayList<HashMap<String, String>>> fullTestDataMap = new HashMap<String, ArrayList<HashMap<String, String>>>();
	public static ArrayList<HashMap<String, String>> suiteRowsList = new ArrayList<HashMap<String, String>>();
	
	public static void main(String[] args) throws IOException, InterruptedException, OpenXML4JException, XMLStreamException
	{
		MainDriver m = new MainDriver();

		//Set Relative path and summary file in GlobalVaribles Class for Further Use
		String path = new File(".").getCanonicalPath();
		g = new GlobalVariables();
		g.setRelativePath(path);	
		
		
		botId = System.getProperty("BOTID").trim();
		testData = System.getProperty("TESTDATA").trim();
		environment = System.getProperty("ENVIRONMENT").trim();
		executionType = System.getProperty("EXECUTIONTYPE").trim();
		testCases = System.getProperty("TESTCASES").trim();
		testCasesArray = testCases.split(",");
		TotalTestCases = testCasesArray.length;
		automationSuite = System.getProperty("AUTOMATIONSUITE").trim(); 

		
//		botId = "0103_WN";
//		testData = "Siebel_Galileo_testdata.xlsx";
//		environment = "crmip20";
//		executionType = "Sequential";
//		testCases = "Verify_Welcomemail_Netherland";
//		testCasesArray = testCases.split(",");
//		int TotalTestCases = testCasesArray.length;
//		automationSuite = "Siebel Galileo";
		
		
		InputStream input3 = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		prop3.load(input3);
				
		try (OutputStream output = new FileOutputStream(g.getRelativePath()+"/configuration.properties")) {

            // set the properties value
            prop.setProperty("BotId", botId);
            prop.setProperty("TestDataSheet", testData);
            prop.setProperty("Environment", environment);
            prop.setProperty("TotalTestCases", String.valueOf(TotalTestCases));
            prop.setProperty("AuomationSuite", automationSuite);
            prop.setProperty("ExecutionType", executionType);
            
            // save properties to project root folder
            prop.store(output, null);
    		g.setBotId(botId);

            System.out.println(prop);

        } catch (IOException io) {
            io.printStackTrace();
        }
		
		//deletePOMXMLNodes(g.getRelativePath() +"//pom.xml");
		//createPOMXML(g.getRelativePath() +"//pom.xml", "Resources/New1.xml");
				
		switch (automationSuite) {
		case "CPQ Quote Processing":
			sheet="CPQ_Quote_Processing";
			break;
		case "Siebel Order Management":
			sheet="Siebel_Order_Management";
			break;
		case "NMTS Number Management":
			sheet="NMTS_Number_Management";
			break;
		case "NH B2B Rest API":
			sheet="Number_Hosting_B2B";
			break;
		case "APT Service Provisioning":
			sheet="APT_Service_Provisioning";
			break;
		case "NC Standalone Service Provisioning":
			sheet="NC_Service_Provisioning";
			break;
		case "Number on Demand(NOD)":
			sheet="NOD";
			break;
		case "SiebelNC Integrated Service Provisioning":
			sheet="SiebelNC_Service_Provisioning";
			break;
		case "Novitas":
			sheet="Novitas";
			break;
		case "Siebel Galileo":
			sheet="Galileo";
			break;
		default: 
			Report.LogInfo("AutomationSuite", "Unsupported automation suite selected", "INFO");
			break;
		}
		
		if(executionType.equalsIgnoreCase("Parallel"))
		{
			gridMode= "ON";
		}else
		{
			gridMode= "OFF";
		}
		
		
		if(gridMode.trim().equalsIgnoreCase("ON"))
		{
			//Execute hub
			try
			{
				RunScript(g.getRelativePath()+"//Grid","Hub.bat");
				
				RunBrowser = testSuite.getValue("Browsers", "firefox");
				Port = testSuite.getValue("ports", "5555");
				String brwArray[] =RunBrowser.split(",");
				
					String portArray[] =Port.split(",");

					//g.setPortNumber(portArray);

					for(int i=0;i<portArray.length;i++)
					{
						if(automationSuite.equalsIgnoreCase("NMTS Number Management")){
							brwArray[i] = "IE";
						}
						m.prepareNode(portArray[i],brwArray[i]);
						RunScript(g.getRelativePath()+"//Grid","node.bat");
						Thread.sleep(3000);
					}								
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
		} else
		{			
				RunBrowser = testSuite.getValue("Browsers", "firefox");
				String brwArray[] =RunBrowser.split(",");
				RunBrowser = brwArray[0];			
		}

		testsuite = testSuite.getValue("ExecutionSuite", "").trim();
						
		//delete all the testcases added in testng xml file
		m.checkndDeleteAllnodes(g.getRelativePath() +"//Resources//testng.xml");
		
		String ColName= "Test_Scenario";
		
		 File path1 = new File("./Resources/"+testsuite+".xlsx");
	     String testSuiteFile = path1.toString(); 
		
		 File path2 = new File("./TestData/"+testData);
	     String testDataFile = path2.toString();
	     
	     LowMemoryExcelFileReader quickExcelReader = new LowMemoryExcelFileReader(testSuiteFile);
	     OPCPackage pkg = quickExcelReader.opcPackage();
	     XSSFReader reader = quickExcelReader.xssfReader(pkg);
	     suiteRowsList = quickExcelReader.getStreamDataSheetName(reader, sheet);
	     
	     LowMemoryExcelFileReader quickExcelReader2 = new LowMemoryExcelFileReader(testDataFile);
	     OPCPackage pkg2 = quickExcelReader2.opcPackage();
	     XSSFReader reader2 = quickExcelReader2.xssfReader(pkg2);
	     quickExcelReader2.readStreamFullXL(reader2);	
	     
	    int totalrow = suiteRowsList.size();
	  	
		for(int tcCount=0;tcCount<testCasesArray.length;tcCount++)
		{
			String expectedTC = testCasesArray[tcCount];
			
			for(int rowC=0;rowC<totalrow;rowC++)
			{
				testScenario = getTCNameFromSuiteMap(suiteRowsList.get(rowC),ColName,expectedTC);
				
				if(testScenario!=null)
				{										
					ArrayList<String> iSubScriptArry = getDataSetValues4TC(suiteRowsList.get(rowC).get("Sheet_Name"), "Scenario_Name", testScenario);
									
					for(int sCount=0;sCount<iSubScriptArry.size();sCount++)
					{
						//Pass the testcase details to create testng xml function
						m.CreateTestngXml(g.getRelativePath() +"//Resources//testng.xml",suiteRowsList.get(rowC),executionType,tcCount,iSubScriptArry.get(sCount));
					}
					
					break;
				}
			}
		}
		
		pkg.close();
		pkg2.close();
		
		if(automationSuite.equalsIgnoreCase("NMTS Number Management")){
			RunBrowser = "IE";
		}else
		{
			RunBrowser = testSuite.getValue("Browsers", "firefox");
			String brwArray[] =RunBrowser.split(",");
			RunBrowser = brwArray[0];
		}
		
		if(RunBrowser.trim().equalsIgnoreCase("FIREFOX"))
		{
			driverEXE = "geckodriver.exe";

		}else if(RunBrowser.trim().equalsIgnoreCase("EDGE"))
		{
			driverEXE = "msedgedriver.exe";
		}else if(RunBrowser.trim().equalsIgnoreCase("CHROME"))
		{
			driverEXE = "chromedriver.exe";
		}
		else if(RunBrowser.trim().equalsIgnoreCase("IE"))
		{
			driverEXE = "IEDriverServer.exe";
		}
			
			//Running the Runscript batch file to Run the Maven
			try {
								
				RunScript(g.getRelativePath(),"RunScript.bat");
				
				if(!automationSuite.equalsIgnoreCase("NH B2B Rest API"))
				{
					
					InputStream input2 = new FileInputStream(g.getRelativePath()+"/PortalDBConfig.properties");

					prop2.load(input2);

					System.setProperty("logback.configurationFile",g.getRelativePath()+"/logback.xml");
					host = prop2.getProperty("Host");
					port = prop2.getProperty("Port");
					elasticIp = prop2.getProperty("elasticIp");
					elasticPort = Integer.parseInt(prop2.getProperty("elasticPort"));
					elasticIndex = prop2.getProperty("elasticIndex");			
				
					Class.forName("com.mysql.jdbc.Driver"); 
					con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/colt", "root", "admin");
					st = con.createStatement();
			
					client = new RestHighLevelClient(RestClient.builder(new HttpHost(elasticIp, elasticPort, "http")));
				
					String currentPIDConfig = prop3.getProperty("CmdPIDs");
					String currentPIDConfig2 = prop3.getProperty("JavaPIDs"); //Rubica
					String currentPIDConfig3 = prop3.getProperty("JavawPIDs");//Rubica;
					
					String taskList1 = getTaskList();
					cmdPIDs = getProcessIds(taskList1, "cmd.exe", currentPIDConfig);
					// String taskList3=getTaskList();//Rubica
					javaPIDs=getProcessIds(taskList1, "java.exe", currentPIDConfig2); //Rubica
					javawPIDs=getProcessIds(taskList1, "javaw.exe", currentPIDConfig3); //Rubica
					
					for(int i=1;i<=60;i++)
					{
						if(isProcessRunning(driverEXE))
						{
							String currentPIDConfig1 = prop3.getProperty("BrowserPIDs");
							
							String taskList2 = getTaskList();
							browserPIDs = getProcessIds(taskList2, driverEXE, currentPIDConfig1);
							
							if(!browserPIDs.equalsIgnoreCase(""))
							{
							
								System.out.println("Tests triggered successfully");	
							
								try (OutputStream output = new FileOutputStream(g.getRelativePath()+"/configuration.properties")) {

					            // set the properties value
					            prop.setProperty("BrowserPIDs", browserPIDs);	
					            prop.setProperty("CmdPIDs", cmdPIDs);
					            prop.setProperty("JavaPIDs", javaPIDs); //Rubica
					            prop.setProperty("JavawPIDs", javawPIDs); //Rubica
					            					            
					            // save properties to project root folder
					            prop.store(output, null);

					            //System.out.println(prop);

								} catch (IOException io) {
					            io.printStackTrace();
								}
								break;
							}else
							{
								Thread.sleep(2000);
							}
						}
						else
						{
							Thread.sleep(2000);
						}
					
						if(i==60)
						{
							System.out.println("Tests triggering failed");
							Report.LogInfo("TestRun", "Tests triggering failed and execution skipped, please verify.", "FAIL");
							Report.logToDashboard("Tests triggering failed and execution skipped, please verify.");
						
//							Logger logger = LoggerFactory.getLogger(Report.class);         
//							MDC.put("order_id", String.valueOf(botId));
							Logger logger = LoggerFactory.getLogger(Report.class);
							String botId1= g.getBotId();
							MDC.put("order_id", botId1); 
							System.out.println("BOTID +" +botId1); 
							MDC.put("Extra", "CTAF");         
							MDC.put("log_level", "INFO");         
							logger.info("Tests triggering failed and execution skipped, please verify.");         
							MDC.remove("Extra");         
							MDC.remove("order_id");         
							MDC.remove("log_level"); 
						
							ExtentTestManager.startTest("TestRun","");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Tests triggering failed and execution skipped, please verify.");
							ExtentTestManager.endTest();
							ExtentManager.getReporter().flush();
				    	
							st.execute("UPDATE req_summary SET Pass=0,Total="+TotalTestCases+",Fail="+TotalTestCases+",req_status='failed',Elapsed='00:00:00' WHERE req_id="+botId+";");
							
							if(isProcessRunning("cmd.exe"))
							{
								if(!cmdPIDs.isEmpty())
								{
									String[] cmdPIDArray = cmdPIDs.split(",");
									for(int k=0; k<cmdPIDArray.length; k++)
									{
										if(currentPIDConfig != null)
										{
											if(!currentPIDConfig.contains(cmdPIDArray[k]))
											{
												String cmd = "taskkill /F /PID " + cmdPIDArray[k];
												Runtime.getRuntime().exec(cmd);
											}
										}else
										{
											String cmd = "taskkill /F /PID " + cmdPIDArray[k];
											Runtime.getRuntime().exec(cmd);
										}
									}
									
								}
								
							}
							
							//Rubica


							if(isProcessRunning("java.exe"))
							{
							if(!javaPIDs.isEmpty())
							{
							String[] javaPIDArray = javaPIDs.split(",");
							for(int l=0; l<javaPIDArray.length; l++)
							{
							if(currentPIDConfig2 != null)
							{
							if(!currentPIDConfig2.contains(javaPIDArray[l]))
							{



							String javaprocess = "taskkill /F /PID " + javaPIDArray[l];
							Runtime.getRuntime().exec(javaprocess);
							}
							}else
							{
							String javaprocess = "taskkill /F /PID " + javaPIDArray[l];
							Runtime.getRuntime().exec(javaprocess);
							}
							}

							}

							}
							if(isProcessRunning("javaw.exe"))
							{
							if(!javawPIDs.isEmpty())
							{
							String[] javawPIDArray = javawPIDs.split(",");
							for(int l=0; l<javawPIDArray.length; l++)
							{
							if(currentPIDConfig3 != null)
							{
							if(!currentPIDConfig3.contains(javawPIDArray[l]))
							{



							String javawprocess = "taskkill /F /PID " + javawPIDArray[l];
							Runtime.getRuntime().exec(javawprocess);
							}
							}else
							{
							String javawprocess = "taskkill /F /PID " + javawPIDArray[l];
							Runtime.getRuntime().exec(javawprocess);
							}
							}

							}

							}
							//Finished by Rubica
													
						}
					}					
				}else
				{
					Thread.sleep(10000);
				}								
				
				if(automationSuite.equalsIgnoreCase("NH B2B Rest API"))
				{
					try (OutputStream output = new FileOutputStream(g.getRelativePath()+"/configuration.properties")) {

						String currentPIDConfig = prop3.getProperty("CmdPIDs");
						
						String taskList1 = getTaskList();
						cmdPIDs = getProcessIds(taskList1, "cmd.exe", currentPIDConfig);
						
						if(!cmdPIDs.equalsIgnoreCase(""))
						{						
							System.out.println("Maven triggered successfully");	
							// set the properties value
				            prop.setProperty("CmdPIDs", cmdPIDs);					        
				            					            
				            // save properties to project root folder
				            prop.store(output, null);
						}else
						{
							String[] cmdPIDArray = cmdPIDs.split(",");
							for(int k=0; k<cmdPIDArray.length; k++)
							{
								if(currentPIDConfig != null)
								{
									if(!currentPIDConfig.contains(cmdPIDArray[k]))
									{
										String cmd = "taskkill /F /PID " + cmdPIDArray[k];
										Runtime.getRuntime().exec(cmd);
									}
								}else
								{
									String cmd = "taskkill /F /PID " + cmdPIDArray[k];
									Runtime.getRuntime().exec(cmd);
								}
							}
							
						}       

			         
			        } catch (IOException io) {
			            io.printStackTrace();
			        }
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	}

	public void checkndDeleteAllnodes(String xmlPath)
	{
		try
		{
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlPath);

			NodeList list = doc.getElementsByTagName("suite");
			for (int i = 0; i <  list.getLength(); i++) 
			{  
				// Get Node  
				Node node =  list.item(i);  
				System.out.println(node.getNodeName());
				
				NodeList childList = node.getChildNodes();  
				// Look through all the children  
				for (int x = 0; x < childList.getLength(); x++) 
				{  
					Node child = (Node) childList.item(x); 
					if(child.getNodeName().equalsIgnoreCase("test") || child.getNodeName().equalsIgnoreCase("parameter") || child.getNodeName().equalsIgnoreCase("classes"))
					{
						child.getParentNode().removeChild(child);  
					}
				}
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult streamResult =  new StreamResult(new File(xmlPath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, streamResult);


		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

	}
	
	public static void deletePOMXMLNodes(String xmlPath)
	{
		try
		{
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlPath);

			NodeList list = doc.getElementsByTagName("suiteXmlFiles");
			for (int i = 0; i <  list.getLength(); i++) 
			{  
				// Get Node  
				Node node =  list.item(i);  
				//System.out.println(node.getNodeName());
				
				NodeList childList = node.getChildNodes();  
				// Look through all the children  
				for (int x = 0; x < childList.getLength(); x++) 
				{  
					Node child = (Node) childList.item(x); 
					if(child.getNodeName().equalsIgnoreCase("suiteXmlFile"))
					{
						child.getParentNode().removeChild(child);  
					}
				}
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult streamResult =  new StreamResult(new File(xmlPath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, streamResult);


		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

	}

	public void CreateTestngXml(String xmlPath, HashMap<String, String> rowData, String executionType, int tcCount, String iSubScript)
	{
		try
		{
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlPath);
			
			ThreadCount = testSuite.getValue("ThreadCount", "1");

			//checkndDeleteAllnodes(xmlPath);
			Node suite = doc.getFirstChild();
			NamedNodeMap attr = suite.getAttributes();	       
			
			if(tcCount==0)	
			{			
				Node nameAttr = attr.getNamedItem("name");
				nameAttr.setTextContent(automationSuite);								
				Node parallelAttr = attr.getNamedItem("parallel");
				Node threadCount = attr.getNamedItem("thread-count");
				if(executionType.trim().equalsIgnoreCase("parallel"))
				{
					parallelAttr.setTextContent("tests");
					threadCount.setTextContent(ThreadCount);
				}else
				{
					parallelAttr.setTextContent("false");					
				}
							
			}
			
			Element testElm = doc.createElement("test");
			testElm.setAttribute("name", rowData.get("Test_Scenario")+"_"+iSubScript);
			suite.appendChild(testElm);
				
			Element parameterElm1 = doc.createElement("parameter");
			parameterElm1.setAttribute("name", "dataSheet");
			parameterElm1.setAttribute("value", rowData.get("Sheet_Name"));
			testElm.appendChild(parameterElm1);
			
			Element parameterElm2 = doc.createElement("parameter");
			parameterElm2.setAttribute("name", "ScenarioName");
			parameterElm2.setAttribute("value", rowData.get("Test_Scenario"));
			testElm.appendChild(parameterElm2);
			
			Element parameterElm3 = doc.createElement("parameter");
			parameterElm3.setAttribute("name", "scriptNo");
			parameterElm3.setAttribute("value", rowData.get("Script_No"));
			testElm.appendChild(parameterElm3);
			
			Element parameterElm4 = doc.createElement("parameter");
			parameterElm4.setAttribute("name", "dataSetNo");
			parameterElm4.setAttribute("value", iSubScript);
			testElm.appendChild(parameterElm4);
			
			Element classesElm = doc.createElement("classes");
			testElm.appendChild(classesElm);
			
			Element classElm = doc.createElement("class");
			classElm.setAttribute("name", rowData.get("Package_Name")+"."+rowData.get("Class_Name"));
			classesElm.appendChild(classElm);
		
			doc.normalize();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult streamResult =  new StreamResult(new File(xmlPath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, streamResult);

		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}

	}
	
	public static void createPOMXML(String xmlPath, String fileName)
	{
		try
		{
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlPath);
			
			NodeList list = doc.getElementsByTagName("suiteXmlFiles");
			for (int i = 0; i <  list.getLength(); i++) 
			{  
				// Get Node  
				Node node =  list.item(i);  
				//System.out.println(node.getNodeName());
				
				//Attr testElm = doc.createAttributeNS("suiteXmlFile", fileName);
				//testElm.setNodeValue(fileName);
				//node.appendChild(testElm);
				
				Element testElm = doc.createElement("suiteXmlFile");
				testElm.appendChild(doc.createTextNode(fileName));
				node.appendChild(testElm);
			}
				
			doc.normalize();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult streamResult =  new StreamResult(new File(xmlPath));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, streamResult);

		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}

	}

	private static String[] getTestcaseDetails(String xlFilePath, String sheetName,int coloumn, int rowC)
	{
		String temp[]=new String[coloumn];

		for(int i=0;i<coloumn;i++)
		{
			temp[i] = readexcel.getDataFromCell(xlFilePath, sheetName, rowC, i);	
		}

		return temp;
	}
	
	public void prepareNode(String port,String browsertype)
	{
		BufferedReader br= null;
		BufferedWriter wr = null;
		String line=null;
		String nodeFile=g.getRelativePath()+"//Grid//Nodetemplate//nodeTemplate.bat";
		String tempFile=g.getRelativePath()+"//Grid//node.bat";
		try
		{
			br = new BufferedReader(new FileReader(nodeFile));
			wr = new BufferedWriter(new FileWriter(tempFile));
			
			while ((line = br.readLine()) != null)
			{
				if(line.indexOf("PORTNUM")>0)
				{
					line=line.replace("PORTNUM", port);
				}
				if(line.indexOf("BROWSER")>0)
				{
					String browser=null;
					if(browsertype.trim().equalsIgnoreCase("FIREFOX"))
					{
						browser = "browserName=\"firefox\",version=ANY,platform=WINDOWS,maxInstances=1 -maxSession 1";

					}else if(browsertype.trim().equalsIgnoreCase("EDGE"))
					{
						browser = "browserName=\"edge\",version=ANY,platform=WINDOWS,maxInstances=1 -maxSession 1";
					}else if(browsertype.trim().equalsIgnoreCase("CHROME"))
					{
						browser = "browserName=\"chrome\",version=ANY,platform=WINDOWS,maxInstances=1 -maxSession 1";
					}
					else if(browsertype.trim().equalsIgnoreCase("IE"))
					{
						browser = "browserName=\"iexplore\",version=ANY,platform=WINDOWS,maxInstances=1 -maxSession 1";
					}

					line =line.replace("BROWSER", browser);
				}
				if(line.indexOf("DRIVER")>0)
				{
					String driver=null;
					if(browsertype.trim().equalsIgnoreCase("FIREFOX"))
					{
						driver = "-Dwebdriver.gecko.driver=\"%~dp0geckodriver.exe\"";

					}else if(browsertype.trim().equalsIgnoreCase("EDGE"))
					{
						driver = "-Dwebdriver.edge.driver=\"%~dp0msedgedriver.exe\"";
					}else if(browsertype.trim().equalsIgnoreCase("CHROME"))
					{
						driver = "-Dwebdriver.chrome.driver=\"%~dp0chromedriver.exe\"";
					}
					else if(browsertype.trim().equalsIgnoreCase("IE"))
					{
						driver = "-Dwebdriver.chrome.driver=\"%~dp0IEDriverServer.exe\"";
					}

					line =line.replace("DRIVER", driver);
				}
								
				wr.write(line);
			}
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			try {
				br.close();
				wr.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
		
	public static void RunScript(String path,String batchFile) throws Exception
	{
		Process p = null;
		try
		{
			//Runtime.getRuntime().exec("cmd /c start "+g.getRelativePath()+"//RunScript.bat");
			p =Runtime.getRuntime().exec("cmd /c start "+path+"//"+batchFile+"");
			p.waitFor();
		}
		catch (IOException e) 
		{
			System.out.println("Error While Executing RunScript batch file");
			System.out.println(e.getMessage());
		}
			
		
	}

	//====================================================================================================
	// FunctionName    	: getTestcaseName
	// Description     	: Function to get the Scenario Name which need to execute based on execute flag
	// Input Parameter 	: testSuitepath- Testsuite Excel file path
	//					: sheetName- Sheet Name
	//					: row - row number
	//					: Col - Column name
	// Return Value    	: TestScenario Name
	//====================================================================================================	

	public String getTestScenarioName(String testSuitepath, String sheet , int row, String Col, String expectedTC)
	{


		String flag = readexcel.getDataFromExcel(testSuitepath, sheet, Col, row);
		if(flag.trim().equalsIgnoreCase((expectedTC)))
		{
			return (readexcel.getDataFromExcel(testSuitepath, sheet, Col, row));
		}
		else
		{
			return null;	
		}
	}
	
	public static ArrayList<String> getDataSetValues4TC(String sheetName, String Col, String expectedTC)
	{
		
		ArrayList<String> temp = new ArrayList<String>();
		
		ArrayList<HashMap<String, String>> listOfRows = fullTestDataMap.get(sheetName);
		
		int totalRows = listOfRows.size();
		
		for(int i=0;i<totalRows;i++)
		{
			HashMap<String, String> rowData = listOfRows.get(i);
			
			String flag = rowData.get(Col);
			if(expectedTC.trim().equalsIgnoreCase(flag))
			{
				temp.add(rowData.get("Data_Set_No"));
			}			
		}
		
		return temp;
	}
	
	public static String getTCNameFromSuiteMap(HashMap<String, String> rowData, String Col, String expectedTC)
	{

		String flag = rowData.get(Col);
		if(expectedTC.trim().equalsIgnoreCase(flag))
		{
			return flag;
		}
		else
		{
			return null;	
		}
	}
	
	 private static boolean isProcessRunning(String processName) throws IOException, InterruptedException
	    {
	        ProcessBuilder processBuilder = new ProcessBuilder("tasklist.exe");
	        Process process = processBuilder.start();
	        String tasksList = toString(process.getInputStream());

	        return tasksList.contains(processName);
	    }
	 
	 private static String getTaskList() throws IOException, InterruptedException
	 {
	        ProcessBuilder processBuilder = new ProcessBuilder("tasklist.exe");
	        Process process = processBuilder.start();
	        String tasksList = toString(process.getInputStream());

	        return tasksList;
	  }
	 
	  private static String toString(InputStream inputStream)
	    {
	        Scanner scanner = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
	        String string = scanner.hasNext() ? scanner.next() : "";
	        scanner.close();

	        return string;
	    }
	  
	  private static String getProcessIds(String taskList, String processName, String currentPIDConfig) throws IOException, InterruptedException
	  {
		  String procIds = "";
		  String[] tasksArray = taskList.split(" ");
			
			for(int i=0; i<tasksArray.length; i++)
			{
				//System.out.println("Process Name: "+ tasksArray[i].trim());
				if(tasksArray[i].trim().contains(processName))
				{
					for(int j=1; j<=25; j++)
					{						
						int index = i+j;
						if(index<tasksArray.length)
						{
							String procId = tasksArray[index].trim();
							if(!procId.isEmpty())
							{
								if(currentPIDConfig != null)
								{
									if(!currentPIDConfig.contains(procId))
									{
										//System.out.println("Process Id: "+ procId);
										if(procIds == "")
										{
											procIds = procId;
										}else
										{
											procIds = procIds + "," + procId;
										}									
									}
								}else
								{
									//System.out.println("Process Id: "+ procId);
									if(procIds == "")
									{
										procIds = procId;
									}else
									{
										procIds = procIds + "," + procId;
									}	
								
								}
								break;
							}
						}						
					}					
				}
			}
		        
		  return procIds;
	  }
	  
}