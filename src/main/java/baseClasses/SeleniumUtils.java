package baseClasses;

import java.awt.AWTException;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.CookieStore;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyStore;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.httpclient.util.HttpURLConnection;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.yaml.snakeyaml.Yaml;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;

import com.gargoylesoftware.htmlunit.javascript.host.Iterator;
import com.opera.core.systems.OperaDriver;
import com.relevantcodes.extentreports.LogStatus;

import pageObjects.aptObjects.APT_MCN_Create_CustomerObj;
import pageObjects.siebelGalileoObjects.SiebelCockpitObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import testHarness.commonFunctions.ReusableFunctions;

public class SeleniumUtils extends MainDriver
{
	public static Map<ITestResult, List<Throwable>> verificationFailuresMap = new HashMap<ITestResult, List<Throwable>>();
	public static WebDriver webDriver=null;
	public static final String EMPTY_KEYWORD = "{empty}";
	public static final String SPACE_KEYWORD = "{space}";
	private final long waitForWebElementTimeOut = 35000l;
	private final long waitForWebElementNotExistTimeOut = 1l;
	//ReusableFunctions Reusable = new ReusableFunctions();

	public SendEmail email=new SendEmail();
	static PrintWriter write = new PrintWriter(System.out, true); 
	public static ReadingAndWritingTextFile file=new ReadingAndWritingTextFile();
	DesiredCapabilities capability;

	protected static GlobalVariables g = new GlobalVariables();
	private static Utilities suiteConfig = new Utilities();
	private static String gridMode =null;
	protected static String timestamp=null;
	protected static Utilities util = new Utilities();
	//public static HashMap<String, String> testData = new HashMap<String, String>();
	
	public Connection con = null;
	public static Statement st = null;
	public static RestHighLevelClient client = null;
	public static String NBORDERID;
	public String host;
	public String port;
	public String elasticIp;
	public int elasticPort;
	public static String elasticIndex;
	public static int TotalCases;
	Properties prop = new Properties();
	Properties prop1 = new Properties();
			
	static SoftAssert softAssert = new SoftAssert();	
	public String AutomationSuite;
	public String ExecutionType;
	public int PID;
	
	public static final ThreadLocal<WebDriver> WEB_DRIVER_THREAD_LOCAL = new InheritableThreadLocal<>();
	public static HashMap<String, ArrayList<HashMap<String, String>>> testDataSet;
	
	@SuppressWarnings("unchecked")
	public static HashMap<String, ArrayList<HashMap<String, String>>> setInfomationForTestData(String fileName) 
	{

		HashMap<String, ArrayList<HashMap<String, String>>> basicInfoDataset=null;
		try
		{
			 LowMemoryExcelFileReader quickExcelReader2 = new LowMemoryExcelFileReader(fileName);
		     OPCPackage pkg2 = quickExcelReader2.opcPackage();
		     XSSFReader reader2 = quickExcelReader2.xssfReader(pkg2);
		     basicInfoDataset = quickExcelReader2.readStreamFullXL(reader2);			

		}catch(Exception e)
		{
			
		}
		return basicInfoDataset;
	}
	
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() throws Exception
	{
		try{
		String path = new File(".").getCanonicalPath();
		g.setRelativePath(path);
		
		InputStream inputA = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		 prop.load(inputA); 
		System.setProperty("logback.configurationFile",g.getRelativePath()+"/logback.xml");
		AutomationSuite = prop.getProperty("AuomationSuite");
		ExecutionType = prop.getProperty("ExecutionType");
		g.setBotId(prop.getProperty("BotId"));
		if(AutomationSuite.equals("NMTS Number Management"))
		{
			g.setBrowser("IE");
		}else{
		
		g.setBrowser(util.getValue("Browsers", "firefox").trim().split(",")[0]);

		}
		
		if(ExecutionType.equalsIgnoreCase("Parallel"))
		{
			g.setGridMode("ON");		
			g.setHubAddress("http://localhost:5555/wd/hub");
		}else
		{
			g.setGridMode("OFF");
		}
		
		String testDataXL = prop.getProperty("TestDataSheet");
		File path2 = new File("./TestData/"+testDataXL);
	    String testDataFile = path2.toString();
	     
		//Return an Array List of HashMap with All values stored from Config_Sheet
	    testDataSet = setInfomationForTestData(testDataFile);

		//Setting all the Required public variable with values from Config_Sheet
		Configuration.setUrlInfoDataSet(AutomationSuite);
				
		gridMode = g.getGridMode();

		timestamp = "RegressionSuite_"+suiteConfig.getCurrentDatenTime("MM-dd-yyyy")+"_"+suiteConfig.getCurrentDatenTime("hh-mm-ss_a");

		String resultPath = path+"//CTAFResults//"+timestamp;
		String ScreenshotsPath = resultPath+"//Screenshots";

		g.setResultFolderPath(resultPath);
		g.setTestSuitePath(path+"//Resources//TestSuite.xls");

		new File(resultPath).mkdirs();
		new File(ScreenshotsPath).mkdirs();

		String SummaryReportfile = resultPath+"\\Index.html";

		Report.createSummaryReportHeader(SummaryReportfile);				
		
			InputStream input1 = new FileInputStream(g.getRelativePath()+"/configuration.properties");
			InputStream input2 = new FileInputStream(g.getRelativePath()+"/PortalDBConfig.properties");

            // load a properties file
            prop.load(input1); 
            prop.load(input2);

       	System.setProperty("logback.configurationFile",g.getRelativePath()+"/logback.xml");
//		NBORDERID = prop.getProperty("BotId");
       	NBORDERID = g.getBotId();
//		System.out.println("BotidSU "+NBORDERID);
		elasticIp = prop.getProperty("elasticIp");
		elasticPort = Integer.parseInt(prop.getProperty("elasticPort"));
		elasticIndex = prop.getProperty("elasticIndex");
		
		client = new RestHighLevelClient(RestClient.builder(new HttpHost(elasticIp, elasticPort, "http")));
	
		 } catch (IOException ex) {
	            ex.printStackTrace();
	   }	
	}
	
	@Parameters({ "ScenarioName" })
	@BeforeMethod()
	public void setUp(String ScenarioName) throws Exception
	{
		//file.setEmailFlag(false);
		//String Automessage=email.getExecutionStartMessage();
		//email.sendMessageWithAttachment("CTAF Automation Regression Test Run Started",Automessage,"",false);
		
		iniliseBrowser();			
		
	}

	@AfterMethod()
	public void tearDownWebDriver() throws Exception
	{
		closeWebdriver();	
	}
	
	@AfterSuite(alwaysRun = true)
	public void afterSuite() throws Exception
	{
		/**************************Zip the Output generated HTML Files********************************/
		ZipDirectory zip = new ZipDirectory();
		File directoryToZip = new File(g.getResultFolderPath());
		List<File> fileList = new ArrayList<File>();
		System.out.println("---Getting references to all files in: " + directoryToZip.getCanonicalPath());
		zip.getAllFiles(directoryToZip, fileList);
		System.out.println("---Creating zip file");

		String zipOut=g.getResultFolderPath() +"//"+directoryToZip.getName() + ".zip";
		System.out.println(zipOut);
		zip.writeZipFile(directoryToZip, fileList,zipOut);
		System.out.println("---Done");

		zipOut=g.getResultFolderPath() +"//"+directoryToZip.getName() + ".zip";
		Report.createSummaryReportFooter();
		client.close();
		
		InputStream inputB = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		prop1.load(inputB); 
		
		//System.out.println("SeleniumUtils: "+ prop1);
		
		if (isProcessRunning("chromedriver.exe"))
        {
			String currentPIDConfig1 = prop1.getProperty("BrowserPIDs");
			
			if(currentPIDConfig1 != null)
			{
				String[] brwPIDArray = currentPIDConfig1.split(",");
				for(int k=0; k<brwPIDArray.length; k++)
				{
					String cmd = "taskkill /F /PID " + brwPIDArray[k];
					Runtime.getRuntime().exec(cmd);				
				}
        	}
			
        }
		
		if(isProcessRunning("cmd.exe"))
        {
			String currentPIDConfig = prop1.getProperty("CmdPIDs");
			
			if(currentPIDConfig != null)
			{
				String[] cmdPIDArray = currentPIDConfig.split(",");
				for(int k=0; k<cmdPIDArray.length; k++)
				{
					String cmd = "taskkill /F /PID " + cmdPIDArray[k];
					Runtime.getRuntime().exec(cmd);				
				}			
			}
        }
//		if(isProcessRunning("java.exe"))
//		{
//		String currentPIDConfig2 = prop1.getProperty("JavaPIDs");
//
//		if(currentPIDConfig2 != null)
//		{
//		String[] javaPIDArray = currentPIDConfig2.split(",");
//		for(int k=0; k<javaPIDArray.length; k++)
//		{
//		String javaprocess = "taskkill /F /PID " + javaPIDArray[k];
//		Runtime.getRuntime().exec(javaprocess);
//		}
//		}
//		}
//
//		if(isProcessRunning("javaw.exe"))
//		{
//		String currentPIDConfig3 = prop1.getProperty("JavawPIDs");
//
//		if(currentPIDConfig3 != null)
//		{
//		String[] javawPIDArray = currentPIDConfig3.split(",");
//		for(int k=0; k<javawPIDArray.length; k++)
//		{
//		String javawprocess = "taskkill /F /PID " + javawPIDArray[k];
//		Runtime.getRuntime().exec(javawprocess);
//		}
//		}
//		}
	
		/**************************Send the mail with zip output HTML Files********************************/
		//email.sendMessageWithAttachment("CTAF Automated Regression Test Run Completed",file.readEntireFile(g.getResultFolderPath() +"//Index.html"),zipOut,true );
	}
	

	public void iniliseBrowser() throws FileNotFoundException, IOException, InterruptedException 
	{
		openBrowser(g.getBrowser());
		//open("");
		//selenium = new WebDriverBackedSelenium(webDriver,Configuration.url);
		
	}
	@SuppressWarnings("deprecation")
	public void openBrowser(String browser) {
		try {

			if (browser.equalsIgnoreCase("firefox"))
			{
				System.setProperty("webdriver.gecko.driver",g.getRelativePath()+"\\Resources\\geckodriver.exe");
				
				/*FirefoxProfile prof2 = new FirefoxProfile(); 				
				capability= DesiredCapabilities.firefox();
				capability.setCapability("firefox_profile", prof2);
				capability.setCapability("marionette", true); */
				
				File pathToBinary = new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
				FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
				FirefoxProfile firefoxProfile = new FirefoxProfile();

				if(gridMode.equalsIgnoreCase("OFF"))
				{

					webDriver = new FirefoxDriver(ffBinary,firefoxProfile);
				}else
				{
					webDriver= new RemoteWebDriver(new URL(g.getHubAddress()), capability);

				}
				//webDriver.manage().window().maximize();

			}
			
			else if(browser.equalsIgnoreCase("chrome"))
			{
				String driverPath = g.getRelativePath()+"\\Resources\\chromedriver.exe";
				System.setProperty("webdriver.chrome.driver",driverPath);
	
				Map<String,Object> preferences= new HashMap<>();
				preferences.put("profile.default_content_settings.popups", 0);
				preferences.put("download.prompt_for_download", "false");
				preferences.put("download.default_directory", g.getRelativePath()+"\\TestData\\Downloads");
	
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", preferences);
	
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				//capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				//capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				capabilities.setBrowserName("chrome");
				capabilities.setPlatform(Platform.WINDOWS);

			if(gridMode.equalsIgnoreCase("OFF"))
			{
				webDriver = new ChromeDriver(capabilities);
			}else
			{
				RemoteWebDriver remoDriver= new RemoteWebDriver(new URL(g.getHubAddress()), capabilities);
				remoDriver.setFileDetector(new LocalFileDetector());
				webDriver = remoDriver;
			}
			}


			else if(browser.equalsIgnoreCase("ie")) 
			{
				System.setProperty("webdriver.ie.driver",g.getRelativePath()+"//Resources//IEDriverServer.exe");
				capability = DesiredCapabilities.internetExplorer();
				capability.setBrowserName("IE");
				capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				capability.setPlatform(Platform.WINDOWS);
				capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
				capability.acceptInsecureCerts();
				//capability.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, Configuration.getConfig("My URL"));
				capability.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
				if(gridMode.equalsIgnoreCase("OFF"))
				{
					webDriver= new InternetExplorerDriver(capability);
				}else
				{
					RemoteWebDriver RemWebDrive= new RemoteWebDriver(new URL(g.getHubAddress()), capability);
					webDriver = RemWebDrive;
				}
			}
			else if(browser.equalsIgnoreCase("edge")) 
			{
				System.setProperty("webdriver.edge.driver",g.getRelativePath()+"\\Resources\\msedgedriver.exe");

				if(gridMode.equalsIgnoreCase("OFF"))
				{
					webDriver= new EdgeDriver();
				}else
				{

					RemoteWebDriver RemWebDrive= new RemoteWebDriver(new URL(g.getHubAddress()), capability);
					webDriver = RemWebDrive;
				}
			}
			else if(browser.equalsIgnoreCase("safari")) 
			{
				capability= DesiredCapabilities.safari();
				capability.setBrowserName("safari");
				capability.setJavascriptEnabled(true);
				capability.setPlatform(org.openqa.selenium.Platform.ANY);
				capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
				webDriver= new SafariDriver(capability);		 
			}

			else if(browser.equalsIgnoreCase("opera")) 
			{		
				capability= DesiredCapabilities.opera();
				capability.setCapability("opera.port", "-1");
				capability.setCapability("opera.profile", "");
				webDriver= new OperaDriver(capability);					 
			}
			
			
			WEB_DRIVER_THREAD_LOCAL.set(webDriver);
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().manage().deleteAllCookies();
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().manage().window().maximize();
			//allowPopups();
		} catch (Exception e) {
			//System.out.println("Could not obtain webdriver for browser \n"	+ e.getMessage());
			Report.LogInfo("Browser", "Could not obtain webdriver for browser \n"+e.getMessage(), "FAIL");
			Report.logToDashboard("Could not obtain webdriver for browser \n"	+ e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Could not obtain webdriver for browser \n"	+ e.getMessage());
			Assert.assertTrue(false);
		}
	}	
		
	public void closeWebdriver()
	{
		if (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get() == null) {

            return;
        }
		quit();
	}

	public static void info(String message)
	{
		write.println(message);
		//System.out.println(message);
	}

	public WebDriver getWebDriver() {

		return SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
	}


	public void setWebDriver(WebDriver webDriver) {
		SeleniumUtils.webDriver = webDriver;
	}

	/**
	 * This method will open the url given in param
	 * @param url
	 * @throws IOException 
	 * @throws Exception 
	 */
	public void open(String url) throws IOException 
	{
		try {
			if(url.endsWith("/"))
			{
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().get(Configuration.C4C_URL+url);
			}
			else
			{
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().get(Configuration.C4C_URL+url);
			}
			setImplicitWaitTimeout(50000);
			ExtentTestManager.getTest().log(LogStatus.PASS, "Launched application: " + Configuration.C4C_URL);
			Report.logToDashboard("Launched application: " + Configuration.C4C_URL);
			Report.LogInfo("Application Launch", "Launched application: " + Configuration.C4C_URL, "PASS");
		}
		catch(RuntimeException e) {
			//throw new Exception("WebDriver unable to handle the request to open url [url='" + url + "']: "+e.getMessage());
			Report.LogInfo("Exception", "Exception in launching URL: "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in launching URL: "	+ e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL,  "Exception in launching URL: "+e.getMessage());
			Assert.assertTrue(false);
		}

	}

	public static void openurl(String url) {

		try {
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().manage().deleteAllCookies();
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().navigate().to(url);	
			ExtentTestManager.getTest().log(LogStatus.PASS, "Launched application: " + url);
			Report.logToDashboard("Launched application: " + url);
			Report.LogInfo("Application Launch", "Launched application: " + url, "PASS");
		}
		catch(RuntimeException e) {
			Report.LogInfo("Exception", "Runtime exception in launching application: " + url, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Runtime exception in launching application: " + url);
			Report.logToDashboard("Runtime exception in launching application: " + url);
			Assert.assertTrue(false);
		}
	}

	public void goBack()
	{
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().navigate().back();
		ExtentTestManager.getTest().log(LogStatus.INFO, "Browser back action performed");
		Report.logToDashboard("Browser back action performed");
	}

	public void refreshPage()
	{
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().navigate().refresh();
	}

	public String getTitleOfAnotherWindow(String currentHandle)
	{
		for (String handle : SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles()) 
		{

			if (!currentHandle.equals(handle))
			{
				return SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle).getTitle();
			}
		}
		return currentHandle; 
	}

	public void close() {
		try {
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().close();
		}
		catch(RuntimeException e) {
			//	throw new Exception("WebDriver unable to handle the request to close : "+e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @throws Exception 
	 * @see #quit()
	 */
	public void quit() {
		
		if (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get() == null) {

            return;
        }
		
		try {
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().quit();
		}
		catch(RuntimeException e) {
			//	throw new Exception("WebDriver unable to handle the request to close browser!"+e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @see Browser#setImplicitWaitTimeout(int)
	 */
	public void setImplicitWaitTimeout(long milliseconds) {
		if (milliseconds > 0) {
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().manage().timeouts().implicitlyWait(milliseconds, TimeUnit.MILLISECONDS);
		} else {
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
		}
	}


	/**
	 * MouseHover on specified locator
	 * Example
	 * 
	 * @param locator
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void mouseOverAction(String locator) throws IOException, InterruptedException {
		WebElement element = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			element = findWebElement(locator);
			if(element != null)
			{	
				new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).moveToElement(element).build().perform();
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(element == null)
		{	
			Report.LogInfo("sendKeys", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("click","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}


	public void mouseOver(String locator) throws IOException
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					Locatable hoverItem = (Locatable) element;
					Report.LogInfo("MouseOver","Mouse over on "+locator +" is done Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Mouse over on "+locator +" is done Successfully");
					Report.logToDashboard("Mouse over on "+locator +" is done Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("mouseOver", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}			

		}catch(Exception e)
		{
			Report.LogInfo("MouseOver",locator +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	/**
	 * click on specified locator
	 * Example
	 * 
	 * @param locator
	 * @throws IOException 
	 * @throws Exception
	 */
	public void click(String locator) throws IOException 
	{
		WebElement element = null;
		try

		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					if(g.getBrowser().trim().equalsIgnoreCase("edge"))
					{
						((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);

					}
					else
					{
						element.click();
					}
					locator = element.getAttribute("id");
					Report.LogInfo("Click","\""+locator +"\" Is Clicked Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Clicked Successfully");
					Report.logToDashboard(locator +" Is Clicked Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("click", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			
		}catch(Exception e)
		{
			Report.LogInfo("Click","<font color=red>"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}
	
	public void javaScriptclick(String locator) throws IOException 
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					if(g.getBrowser().equalsIgnoreCase("firefox"))
					{
						element.click();
					}
					else
					{
						((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);
					}			

					Report.LogInfo("Click","\""+locator +"\" Is Clicked Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Clicked Successfully");
					Report.logToDashboard(locator +" Is Clicked Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("Click", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			
		}catch(Exception e)
		{
			Report.LogInfo("Click",locator +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	public void javaScriptclick(String locator,String Object) throws IOException 
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					if(g.getBrowser().equalsIgnoreCase("firefox"))
					{
						element.click();
					}
					else if(g.getBrowser().equalsIgnoreCase("chrome"))
					{
						element.click();
					}
					else 
					{
						((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);
					}			

					Report.LogInfo("Click","\""+Object +"\" Is Clicked Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, Object +" Is Clicked Successfully");
					Report.logToDashboard(Object +" Is Clicked Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("Click", Object +" not present on Screen", "FAIL");
				Report.logToDashboard(Object +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}		
			
		}catch(Exception e)
		{
			Report.LogInfo("Click",Object +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" Is not Present on Screen");
			Report.logToDashboard(Object +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	public void javaScriptclick1(String locator,String Object) throws IOException 
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);			
					Report.LogInfo("Click","\""+Object +"\" Is Clicked Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, Object +" Is Clicked Successfully");
					Report.logToDashboard(Object +" Is Clicked Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("Click", Object +" not present on Screen", "FAIL");
				Report.logToDashboard(Object +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}			
			
		}catch(Exception e)
		{
			Report.LogInfo("Click",Object +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" Is not Present on Screen");
			Report.logToDashboard(Object +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}
	
	public void javaScriptClickWE(WebElement locator,String Object) throws IOException 
	{

		try
		{
			((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  locator);			
			Report.LogInfo("Click","\""+locator +"\" Is Clicked Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Object +" Is Clicked Successfully");
			Report.logToDashboard(Object +" Is Clicked Successfully");
		}catch(Exception e)
		{
			Report.LogInfo("Click",locator +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" Is not Present on Screen");
			Report.logToDashboard(Object +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}
	
	static String sException = null; static int j = 0;
	public void scrollToViewNClick(String locator,String Object) throws IOException 
	{
		WebElement element = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			element = findWebElement(locator);
			if(element != null)
			{	
				try {
					element.click();    			
	    			
			} catch (Exception e1) {
				for (int j = 0; j <= 10; j++) {
					try {
							Thread.sleep(1000);
							waitToElementVisible(locator);
							scrollIntoView(element);
							Thread.sleep(250);
							element.click();
			    			break;
					} catch (Exception e) {
						sException = e.toString();
						j = i+1;
						continue;
					}	
				}
			}
			if (j > 10) {
				
			}
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(element == null)
		{	
			Report.LogInfo("ScrollNClick", Object +" not present on Screen", "FAIL");
			Report.logToDashboard(Object +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("ScrollNClick","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
		
	}
	
	 public void ScrollIntoViewByString(String locator) throws IOException {
		 WebElement element = null;
		
		 try
		 {
		 for(int i=1; i<=2; i++)
		 {
			element = findWebElement(locator);
		 	if(element != null)
		 	{	
		 		scrollIntoView(element);
		 		break;
		 	}else
		 	{
		 		Thread.sleep(2500);
		 	}				
		 }
		 			
		 if(element == null)
		 {	
		 	Report.LogInfo("ScrollToView", locator +" not present on Screen", "INFO");
		 	Report.logToDashboard(locator +" not present on Screen");
		 	ExtentTestManager.getTest().log(LogStatus.INFO, locator +" not present on Screen");
			 }
		 }catch(Exception e)
		 {
	
		}
		 
	 }
	 
	 public static boolean scrollIntoView(WebElement webElement) throws IOException {
		 boolean status = false;
	    	
		 try {
			 	String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                        + "var elementTop = arguments[0].getBoundingClientRect().top;"
                        + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
			 	((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript(scrollElementIntoMiddle, webElement);
			 	isElementEnabled(webElement);
			 	Thread.sleep(250);
			 	status = true;
			 } catch (Exception e) {
				 e.printStackTrace();
				 status = false;				
			 }
			
		 return status;
	 }

	 public void click(String locator,String ObjectName) throws IOException 
		{
		 	WebElement element = null;
			try
			{
				
				for(int i=1; i<=2; i++)
				{
					element = findWebElement(locator);
					if(element != null)
					{	
						WebDriverWait wait = new WebDriverWait(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get(), 30);
						wait.until(ExpectedConditions.elementToBeClickable(element));
						if(g.getBrowser().trim().equalsIgnoreCase("EDGE"))
						{
							JavascriptExecutor js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
							((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);
							Report.LogInfo("click","<b><i>"+ObjectName +"</i></b> Is Clicked Successfully", "INFO");	
							ExtentTestManager.getTest().log(LogStatus.INFO, ObjectName +" Is Clicked Successfully");
							Report.logToDashboard(ObjectName +" Is Clicked Successfully");
						}
						else
						{
											
							try {
								element.click();
				    					    			
							} catch (Exception e1) {
							for (int j = 0; j <= 5; j++) {
								try {
										Thread.sleep(1000);
										waitForElementToBeVisible(locator, 5);
										scrollIntoView(element);
										Thread.sleep(250);
										element.click();
						    			break;
								} catch (Exception e) {
									sException = e.toString();
									j = i+1;
									continue;
								}	
							}
						}					
							Report.LogInfo("click","<b><i>"+ObjectName +"</i></b> Is Clicked Successfully", "INFO");
							ExtentTestManager.getTest().log(LogStatus.INFO, ObjectName +" Is Clicked Successfully");
							Report.logToDashboard(ObjectName +" Is Clicked Successfully");
						}
					
						break;
					}else
					{
						Thread.sleep(2500);
					}				
				}
							
				if(element == null)
				{	
					Report.LogInfo("click", ObjectName +" not present on Screen", "FAIL");
					Report.logToDashboard(ObjectName +" not present on Screen");
					ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" not present on Screen");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					Assert.assertTrue(false);
				}

			}catch(Exception e)
			{
				Report.LogInfo("click","<font color=red"+ObjectName +" Is not Present on Screen</font>", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" Is not Present on Screen");
				Report.logToDashboard(ObjectName +" Is not Present on Screen");
				Assert.assertTrue(false);
			}
		}

	public boolean isClickable(String locator) 
	{
		WebElement element = null;
		boolean status = false;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					if(element.isDisplayed() && element.isEnabled())
					{
						status = true;
					}				

					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				status = false;
			}
			
			
		}catch(Exception e)
		{
			return false;
		}
		
		return status;
	}

	public void clickAndWait(String locator) throws IOException 
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					element.click();
					javaScriptWait();
					Report.LogInfo("click",locator +" Is Clicked Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Clicked Successfully");
					Report.logToDashboard(locator +" Is Clicked Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("click", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in clickAndWait "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in clickAndWait "+e.getMessage());
			Report.logToDashboard("Exception in clickAndWait "+e.getMessage());
			Assert.assertTrue(false);
		}
	}


	/**
	 * doubleClick on specified locator
	 * Example
	 * 
	 * @param locator
	 * @throws IOException 
	 * @throws Exception
	 */
	public void doubleClick(String locator) throws IOException 
	{
		WebElement element = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			element = findWebElement(locator);
			if(element != null)
			{	
				new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).moveToElement(element).doubleClick();
				
				Report.LogInfo("dobuleClick",locator +" Is Double Clicked Successfully", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Double Clicked Successfully");
				Report.logToDashboard(locator +" Is Double Clicked Successfully");
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(element == null)
		{	
			Report.LogInfo("dobuleClick", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}	
		}catch(Exception e)
		{
			Report.LogInfo("dobuleClick","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}


	/**
	 * submit on specified locator
	 * Example
	 * 
	 * @param locator
	 * @throws IOException 
	 * @throws Exception
	 */
	public void submit(String locator) throws IOException
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					element.submit();
					Report.LogInfo("submit",locator +" Is Submitted Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Submitted Successfully");
					Report.logToDashboard(locator +" Is Submitted Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("submit", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}			

		}catch(Exception e)
		{
			Report.LogInfo("submit", locator +" Is not Submitted", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Submitted");
			Report.logToDashboard(locator +" Is not Submitted");
			Assert.assertTrue(false);
		}
	}

	public void submitAndWait(String locator) throws IOException
	{
		WebElement element = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				element = findWebElement(locator);
				if(element != null)
				{	
					element.submit();
					javaScriptWait();
					Report.LogInfo("submit",locator +" Is Submitted Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Submitted Successfully");
					Report.logToDashboard(locator +" Is Submitted Successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfo("submit", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}			
			
		}catch(Exception e)
		{
			Report.LogInfo("submit",locator +" Is not Submitted Successfully", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Submitted");
			Report.logToDashboard(locator +" Is not Submitted Successfully");
			Assert.assertTrue(false);
		}
	}

	/**
	 * Select one option from a single select drop down list
	 * Single Selection: Select option
	 * 
	 * @param locator (@id=option) && xpath
	 * @param field   (@id)
	 * @throws IOException 
	 * @throws Exception
	 */
	public void select(String locator, String field) throws IOException
	{
		WebElement webElement = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					Select element = new Select(webElement);
					selectValue(element, field);
					Report.LogInfo("dropdown",locator +" Is Selected Successfully with option " +field, "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, locator +" Is Selected Successfully with option " +field);
					Report.logToDashboard(locator +" Is Selected Successfully with option " +field);
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(webElement == null)
			{	
				Report.LogInfo("dropdown", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			
		}
		catch(Exception e)
		{
			Report.LogInfo("dropdown",locator +" Is not Selected Successfully with option " +field, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Selected Successfully with option " +field);
			Report.logToDashboard(locator +" Is not Selected Successfully with option " +field);
			Assert.assertTrue(false);
		}
	}

	public void selectByIndex(String locator, int index,String ObjectName) throws IOException 
	{
		WebElement webElement = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					Select element = new Select(webElement);
					element.selectByIndex(index);
					Report.LogInfo("dropDown",index +" item is Selected in "+ObjectName+" successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, index +" item is Selected in "+ObjectName+" successfully");
					Report.logToDashboard(index +" item is Selected in "+ObjectName+" successfully");
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(webElement == null)
			{	
				Report.LogInfo("dropDown", ObjectName +" not present on Screen", "FAIL");
				Report.logToDashboard(ObjectName +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			
		}catch(Exception e)
		{
			Report.LogInfo("dropDown", ObjectName + " Not present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName + " Not present on Screen");
			Report.logToDashboard(ObjectName + " Not present on Screen");
			Assert.assertTrue(false);
		}
	}
	public void selectByValue(String locator, String field,String ObjectName) throws IOException 
	{
		WebElement webElement = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					Select element = new Select(webElement);
					element.selectByValue(field);
					Report.LogInfo("dropdown",ObjectName +" Is Selected Successfully with option " +field, "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, ObjectName +" Is Selected Successfully with option " +field);
					Report.logToDashboard(ObjectName +" Is Selected Successfully with option " +field);
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(webElement == null)
			{	
				Report.LogInfo("dropdown", ObjectName +" not present on Screen", "FAIL");
				Report.logToDashboard(ObjectName +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}			
		}
		catch(Exception e)
		{
			Report.LogInfo("dropdown",ObjectName +" Is not Selected Successfully with option " +field, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" Is not Selected Successfully with option " +field);
			Report.logToDashboard(ObjectName +" Is not Selected Successfully with option " +field);
			Assert.assertTrue(false);
		}
	}

	public void selectByVisibleText(String locator, String field,String ObjectName) throws IOException 
	{
		WebElement webElement = null;
		try
		{
			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					Select element = new Select(webElement);
					element.selectByVisibleText(field);
					Report.LogInfo("dropdown",ObjectName +" Is Selected Successfully with option " +field, "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, ObjectName +" Is Selected Successfully with option " +field);
					Report.logToDashboard(ObjectName +" Is Selected Successfully with option " +field);
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
						
			if(webElement == null)
			{	
				Report.LogInfo("dropdown", ObjectName +" not present on Screen", "FAIL");
				Report.logToDashboard(ObjectName +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}			
		}
		catch(Exception e)
		{
			Report.LogInfo("dropdown",ObjectName +" Is not Selected Successfully with option " +field, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" Is not Selected Successfully with option " +field);
			Report.logToDashboard(ObjectName +" Is Selected Successfully with option " +field);
			Assert.assertTrue(false);
		}
	}

	/**
	 * @param singleSelect
	 * @param field
	 * @throws IOException 
	 * @throws Exception 
	 */
	protected void selectValue(Select singleSelect, String field) throws IOException
	{
		try
		{
			if(field.startsWith("@value")){
				String value = removeStart(field, "@value=");
				if (!isBlankOrNull(value)){
					singleSelect.selectByValue(removeStart(field, "@value="));
					Report.LogInfo("SelectOptionByValue",field +"  value is selected in dropdown", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, field +"  value is selected in dropdown");
					Report.logToDashboard(field +"  value is selected in dropdown");
				}
			} else if(field.startsWith("@index")){
				String index = removeStart(field, "@index=");
				if (!isBlankOrNull(index)){
					singleSelect.selectByIndex(Integer.parseInt(index));
					Report.LogInfo("SelectOptionByIndex",field +" index is selected in dropdown", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, field +" index is selected in dropdown");
					Report.logToDashboard(field +" index is selected in dropdown");
				}
			} else if (field.startsWith("@visibleText")){
				String text = removeStart(field, "@visibleText=");
				if (!isBlankOrNull(text)){
					singleSelect.selectByVisibleText(removeStart(field, "@visibleText="));
					Report.LogInfo("SelectOptionByVisibleText",field +" visible text is selected in dropdown", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, field +" visible text is selected in dropdown");
					Report.logToDashboard(field +" visible text is selected in dropdown");
				}
			} else
			{
				//	throw new Exception("Invalid format of the field [field='" + field+"']");
			}
		}catch(Exception e)
		{
			Report.LogInfo("SelectValue",field +"is not selected in "+singleSelect , "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, field +"is not selected in "+singleSelect);
			Report.logToDashboard(field +"is not selected in "+singleSelect);
			Assert.assertTrue(false);
		}
		//log.trace("Exiting method selectValue");
	}


	/**
	 * @throws IOException 
	 * @throws Exception 
	 * @see selectFromMultipleSelect
	 */
	public void selectMultiple(String locator, String values) throws IOException {
		WebElement webElement = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			webElement = findWebElement(locator);
			if(webElement != null)
			{	
				if(isBlankOrNull(locator) || 
						(!locator.startsWith("/") && !locator.startsWith("@name=") && !locator.startsWith("@id="))) {
				}
				Select element = new Select(webElement);
				if(isValueEmpty(values)) {
					element.deselectAll();
					return;
				}
				String[] args = values.split(",");
				for ( String field : args) {
					selectValue(element, field);
				}
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(webElement == null)
		{	
			Report.LogInfo("selectMultiple", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}	
		}catch(Exception e)
		{
			Report.LogInfo("selectMultiple","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
	}

	/**
	 * @param locator
	 * @param value
	 * @throws IOException 
	 * @throws Exception
	 */
	public void selectRadio(String locator, String value) throws IOException {

		List<WebElement> webElements = null;	
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			webElements = findWebElements(locator);
			if(webElements != null)
			{	
				if(isBlankOrNull(locator) || (!locator.startsWith("@xpath=") && !locator.startsWith("@name=") )) {
					//	throw new Exception("Invalid locator format [locator='"+locator+"']");
				}
				

				if(value.startsWith("@id=")){
					String id = removeStart(value,"@id=");
					for ( WebElement element : webElements ) {
						if(id.equals(element.getAttribute("id"))) {
							element.click();
							//log.debug("Element to select identified with id [value='"+value+"']");
						}
					}
				} else if(value.startsWith("@value=")){
					String id = removeStart(value,"@value=");
					for ( WebElement element : webElements ) {
						if(id.equals(element.getAttribute("value"))) {
							element.click();
							//log.debug("Element to select identified with value [value='"+value+"']");
						}
					}
				}
				else {
					//	throw new Exception("Invalid value format [value='"+value+"']");
				}
			
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(webElements == null)
		{	
			Report.LogInfo("selectRadio", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("selectRadio","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
	}

	/**
	 * @param locator
	 * @param value
	 * @throws IOException 
	 * @throws Exception
	 */
	public void check(String locator, String value) throws IOException {
		
		clickCheckBox(locator, value, true);
	}

	public void checkbox(String locator,Keys key) throws IOException {

		if(verifyExists(locator))
		{
			findWebElement(locator).sendKeys(key);
			Report.LogInfo("isChecked",locator , "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, locator+" is checked.");
		}
		else
		{
			Report.LogInfo("isChecked", locator+ "is not found", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator+" is not found");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			softAssert.fail(locator+ "is not found");
		}

	}

	/**
	 * @param locator
	 * @param value
	 * @throws IOException 
	 * @throws Exception
	 */
	public boolean isSelected(String locator,String infoMessage) throws IOException {

		Report.LogInfo("isChecked", infoMessage, "Info");

		if(verifyExists(locator))
		{
			return (findWebElement(locator).isSelected());
		}
		else

		{
			Report.LogInfo("isChecked", locator+ "is not found", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator+ "is not found");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			softAssert.fail(locator+ "is not found");
			return false;
		}

	}

	/**
	 * @param locator
	 * @param value
	 * @throws IOException 
	 * @throws Exception
	 */
	public void uncheck(String locator, String value) throws IOException {
		clickCheckBox(locator, value, false);

	}

	/**
	 * @param isChecked
	 * @param element
	 * @return
	 */
	protected boolean canClickElement(boolean isCheckCommand, WebElement element) {
		boolean canClick = (isCheckCommand && !element.isSelected()) ||
				(!isCheckCommand && element.isSelected());
		return canClick;
	}

	protected void clickCheckBox(String locator, String value, boolean isCheckCommand) throws IOException{
		
		List<WebElement> webElements = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			webElements = findWebElements(locator);
			if(webElements != null)
			{	
				if(isBlankOrNull(locator) || (!locator.startsWith("/") && !locator.startsWith("@name=") )) {
					//	throw new Exception("Invalid locator format [locator='"+locator+"']");
				}
				
				if("all".equalsIgnoreCase(value)) {
					for ( WebElement element : webElements ) {
						if( canClickElement(isCheckCommand, element) ) {
							//log.debug("Checkbox clicked [value='"+value+"']");
							element.click();
						}
					}
				} else if(value.startsWith("@id=")){
					String id = removeStart(value,"@id=");
					for ( WebElement element : webElements ) {
						if(id.equals(element.getAttribute("id")) && canClickElement(isCheckCommand, element)) {
							//log.debug("Checkbox clicked [value='"+value+"']");
							element.click();
							break;
						}
					}
				} else if(value.startsWith("@value=")){
					String valueToSet = removeStart(value,"@value=");
					for ( WebElement element : webElements ) {
						if(valueToSet.equals(element.getAttribute("value")) && canClickElement(isCheckCommand, element)) {
							//log.debug("Checkbox clicked [value='"+value+"']");
							element.click();
							break;
						}
					}
				}
				else {
					//	throw new Exception("Invalid Value format [value='"+value+"']");
				}
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(webElements == null)
		{	
			Report.LogInfo("clickCheckBox", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("clickCheckBox","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
	}


	/**
	 * The method returns true, if the value just has the empty keyword.
	 * 
	 * @param value
	 * @return
	 * @throws Exception 
	 */
	protected boolean isValueEmpty(String value) {
		if(StringUtils.containsIgnoreCase(value,EMPTY_KEYWORD)) {
			if(EMPTY_KEYWORD.equalsIgnoreCase(value.trim())) {
				return true;
			}
			else {
				//	throw new Exception("Invalid empty value format! [value='" + value + "']");
			}
		}
		return false;
	}

	/**
	 * The method replaces ${space} with " " and returns the string.  The end result after processing 
	 * should be all spaces if not, throws an Exception.
	 * 
	 * @param value
	 * @return
	 * @throws Exception
	 */
	protected String processSpaceValues(String value) {
		if(StringUtils.containsIgnoreCase(value,SPACE_KEYWORD)) {
			value = value.replace(SPACE_KEYWORD, " ");
			value = value.replace(SPACE_KEYWORD.toUpperCase(), " ");
			if("".equals(value.trim())) {
				//info("Set the value in text box as [value='"+value+"']");
				//log.trace("Exit method processSpaceValues");
				return value;
			}
			else {
				//	throw new Exception("Invalid space value format! [value='" + value + "']");
			}
		}
		return value;
	}

	/**
	 * Verify if the page contains specified element
	 *           
	 * @param elementLocator
	 * @throws IOException 
	 * @Modified on 
	 * @Modified By 
	 */
	public boolean verifyExists(String locator) throws IOException {
		if(!isBlankOrNull(locator)){
			try {
				setImplicitWaitTimeout(2);
				WebElement element=findWebElement(locator);
				setImplicitWaitTimeout(20000);
				if(element != null)
				{
					return true;
				}
				else
				{
					return false;
				}


			} catch (Exception e) {

				return false;
			} 
		}else{

				return false;
		}


	}

	public boolean verifyExists(String locator,String Object) throws InterruptedException, IOException {
		if(g.getBrowser().equalsIgnoreCase("edge") || g.getBrowser().equalsIgnoreCase("firefox"))
		{
			waitForElementToAppear(locator,5);
		}
		if(!isBlankOrNull(locator)){
			try {
				waitForElementToAppear(locator,15);
				WebElement element=findWebElement(locator);
				WebDriverWait wait = new WebDriverWait(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get(), 30);
				wait.until(ExpectedConditions.visibilityOf(element));
				wait.until(ExpectedConditions.elementToBeClickable(element));
				if(element != null)
				{
					Report.LogInfo("verifyExists","<b><i>"+Object +"<i></b> is present on screen", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, Object +" is present on screen");
					Report.logToDashboard(Object +" is present on screen");
					return true;
				}
				else
				{
					Report.LogInfo("verifyExists","<b><i>"+Object +"</i></b> is not present on screen", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, Object +" is not present on screen");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					Report.logToDashboard(Object +" is not present on screen");
					return false;
				}				
			} catch (Exception e) {

				setImplicitWaitTimeout(20000);
				//Report.LogInfo("getTextFrom","<b><i>'"+Object +"'</i></b> is not present on Screen", "FAIL");
				Report.LogInfo("verifyExists","<b><i>"+Object +"</i></b> is not present on screen", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, Object +" is not present on screen");
				Report.logToDashboard(Object +" is not present on screen");
				return false;
			} 
		}else{

			Report.LogInfo("verifyExists","<b><i>"+Object +"</i></b> is not present on screen", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Object +" is not present on screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard(Object +" Element not found");
			return false;
		}
	}

	/**
	 * Set page to accept/reject all confirm boxes
	 * 
	 * @param elementLocator
	 */
	public void clickConfirmBox(String option) {
		Alert alert = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().alert();
		if(!isBlankOrNull(option)){
			if(option.equalsIgnoreCase("OK")) {
				alert.accept();
			} else {
				alert.dismiss();
			}
		}
	}

	/**
	 * Set window as current active window for specified title
	 * 
	 * @param title - 
	 * @throws Exception 
	 */
	public void setWindow(String title) {
		String currentHandle = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandle();

		for (String handle : SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles()) {

			if (title.equals(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle).getTitle())) {
				return;
			}
		} 

		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(currentHandle);
	}

	/**
	 * This method will find the webElement based on id/name/xpath/linkText
	 * 
	 * @param locator
	 * @param webElement
	 * @return
	 * @throws IOException 
	 * @throws Exception 
	 */
	protected WebElement findWebElement(String locator) throws IOException{

		WebElement webElement = null;
		if(!isBlankOrNull(locator)){
			try {

				//Report.LogInfo("findWebElement","Web element '"+locator+ "' is finding", "INFO");
				if(locator.startsWith("@id")){ // e.g @id='elementID'
					// Find the text input element by its id
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.id(removeStart(locator, "@id=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}else if(locator.startsWith("@name")){
					// Find the text input element by its name
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.name(removeStart(locator, "@name=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}else if(locator.startsWith("@linkText")){
					// Find the text input element by its link text
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.linkText(removeStart(locator, "@linkText=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}else if(locator.startsWith("@partialLinkText")){
					// Find the text input element by its link text
					webElement =SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.partialLinkText(removeStart(locator, "@partialLinkText=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}else if(locator.startsWith("@xpath")){
					//using XPATH locator.
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(removeStart(locator, "@xpath="))); 
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}else if(locator.startsWith("@css")){
					// Find the text input element by its css locator
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.cssSelector(removeStart(locator, "@css=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}else if(locator.startsWith("@className")){
					// Find the text input element by its class Name
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.className(removeStart(locator, "@className=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}
				else if(locator.startsWith("@tagName")){
					// Find the text input element by its class Name
					webElement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.tagName(removeStart(locator, "@tagName=")));
					((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
				}

			} catch(NoSuchElementException e) { 
				e.printStackTrace();
			} catch(RuntimeException e) { 
				e.printStackTrace();
				Report.LogInfo("Exception", "Runtime exception: "+e, "FAIL");
				Report.logToDashboard("Runtime exception: "+e);
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Runtime exception: "+e);
				Assert.assertTrue(false);
			}
		}
		
		return webElement;
	}


	/**
	 * This method will find the webElement based on id/name/xpath/linkText
	 * 
	 * @param locator
	 * @param webElement
	 * @return
	 * @throws IOException 
	 * @throws Exception 
	 */
	protected List<WebElement> findWebElements(String locator) throws IOException
	{
		List<WebElement> webElements = null;
		if(!isBlankOrNull(locator)){
			try {
				if(locator.startsWith("@id")){ // e.g @id='elementID'
					// Find the text input element by its id
					webElements = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.id(removeStart(locator, "@id=")));
				}else if(locator.startsWith("@name")){
					// Find the text input element by its name
					webElements = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.name(removeStart(locator, "@name=")));
				}else if(locator.startsWith("@linkText")){
					// Find the text input element by its link text
					webElements =SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.linkText(removeStart(locator, "@linkText=")));
				}else if(locator.startsWith("@partialLinkText")){
					// Find the text input element by its link text
					webElements = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.partialLinkText(removeStart(locator, "@partialLinkText=")));
				}else if(locator.startsWith("@xpath")){
					//using XPATH locator.
					webElements = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(removeStart(locator, "@xpath="))); 
				}else if(locator.startsWith("@css")){
					// Find the text input element by its link text
					webElements =SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.cssSelector(removeStart(locator, "@css=")));
				}
				else if(locator.startsWith("@className")){
					// Find the text input element by its class Name
					webElements = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.className(removeStart(locator, "@className=")));
				}
			} catch(NoSuchElementException e) { 
				e.printStackTrace();
			} catch(RuntimeException e) { 
				e.printStackTrace();
				Report.LogInfo("Exception", "Runtime exception: "+e, "FAIL");
				Report.logToDashboard("Runtime exception: "+e);
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Runtime exception: "+e);
				Assert.assertTrue(false);
			}
		}
				
		return webElements;
	}

	/**
	 * @return boolean
	 * @throws IOException 
	 * @throws Exception
	 */
	public boolean verifyDoesNotExist(String locator) throws IOException {
		//log.trace("Entering method verifyDoesNotExist [locator='"+locator+"']");
		boolean result = false;
		if(!isBlankOrNull(locator)){
			try {
				setImplicitWaitTimeout(waitForWebElementNotExistTimeOut);
				findWebElement(locator);
			} catch (Exception e) {
				if(e.getCause().getClass().equals(NoSuchElementException.class)){
					result= true;
				}else {
					Report.LogInfo("verifyNotExists","<b><i>"+locator +"</i></b> is not present on screen", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" is not present on screen");
					Report.logToDashboard(locator +" Element not found");
					Assert.assertTrue(false);
				}
			} finally{
				setImplicitWaitTimeout(waitForWebElementTimeOut);
			}
			//info("Verify field exists [locator='"+locator+"']");

		}
		//log.trace("Exiting method verifyDoesNotExist");
		return result;
	}



	/**
	 * @throws IOException 
	 * @throws Exception 
	 * 
	 */
	public String getAttributeFrom(String locator, String attributeName) throws IOException {
		//log.trace("Entering method getAttributeValue [locator='"+locator+"']");
		String returnValue = null;
		if(!isBlankOrNull(locator) && !isBlankOrNull(attributeName)){
			try {

				WebElement webElement = findWebElement(locator);
				returnValue = webElement.getAttribute(attributeName).trim();// This should be parameter
				Report.LogInfo("getAttributeValue","Attribute <B>'"+ attributeName +"'</B> in locator : <I>'"+locator+"'</I> is :"+returnValue,"INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Attribute "+ attributeName +" in locator : "+locator+" is :"+returnValue);
				Report.logToDashboard("Attribute "+ attributeName +" in locator : "+locator+" is :"+returnValue);
			}catch(NoSuchElementException e) { 
				//log.trace("Did not find specified HTML element [locator="+locator+"] \n "+e.getMessage());
				//	throw new Exception("Element does not exist [locator='"+locator+"']: "+e.getMessage());
				Report.LogInfo("Exception", "Element does not exist [locator='"+locator+"']: "+e.getMessage(), "FAIL");
				Report.logToDashboard("Element does not exist [locator='"+locator+"']: "+e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.INFO, "Element does not exist [locator='"+locator+"']: "+e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}catch(Exception e){
				//	throw new Exception("Exception encountered while trying to getAttributeValue [locator='"+locator+"']: "+e.getMessage(), e);
				Report.LogInfo("Exception", "Exception encountered while trying to getAttributeValue [locator='"+locator+"']: "+e.getMessage(), "FAIL");
				Report.logToDashboard("Exception encountered while trying to getAttributeValue [locator='"+locator+"']: "+e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception encountered while trying to getAttributeValue [locator='"+locator+"']: "+e.getMessage());
				Assert.assertTrue(false);
			}
		}
		return returnValue;
	}


	public String getAttributeFrom(String locator, String attributeName,String objectName) throws IOException
	{
		String returnValue = null;
		if(!isBlankOrNull(locator) && !isBlankOrNull(attributeName))
		{
			try {
				WebElement webElement = findWebElement(locator);
				returnValue = webElement.getAttribute(attributeName).trim();// This should be parameter
				Report.LogInfo("getAttributeValue","Value of attribute <B><i>'"+ attributeName +"'</i></B> of Object '"+objectName+"' is :'"+returnValue+"'","INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Value of attribute '"+ attributeName +"' of Object '"+objectName+"' is :'"+returnValue+"'");
				Report.logToDashboard("Value of attribute '"+ attributeName +"' of Object '"+objectName+"' is :'"+returnValue+"'");
			}catch(NoSuchElementException e) { 
				Report.LogInfo("Exception", "Element does not exist [locator='"+locator+"']: "+e.getMessage(), "FAIL");
				Report.logToDashboard("Element does not exist [locator='"+locator+"']: "+e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.INFO, "Element does not exist [locator='"+locator+"']: "+e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}catch(Exception e)
			{
				Report.LogInfo("Exception","Exception in getAttributeFrom "+e.getMessage(),"FAIL");	
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in getAttributeFrom "+e.getMessage());
				Report.logToDashboard("Exception in getAttributeFrom "+e.getMessage());
				Assert.assertTrue(false);
			}
		}
		return returnValue;
	}


	public String getTextFrom(String locator) throws IOException {
		String returnText = null;
		setImplicitWaitTimeout(2);
		if(!isBlankOrNull(locator))
		{

			WebElement webElement = findWebElement(locator);

			try {
				returnText = webElement.getText().trim();
				Report.LogInfo("getTextFrom","getting text from locator : ' "+ locator+" is "+returnText, "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "getting text from locator : ' "+ locator+" is "+returnText);
				Report.logToDashboard("getting text from locator : ' "+ locator+" is "+returnText);
			}catch(NoSuchElementException e) { 
				Report.LogInfo("getTextFrom","Text isn't found for locator: ' "+ locator, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Text isn't found for locator: ' "+ locator);
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Report.logToDashboard("Text isn't found for locator: ' "+ locator);				
				//	throw new Exception("Element does not exist [locator='"+locator+"']: "+e.getMessage());
			}catch(Exception e){
				Report.LogInfo("getTextFrom","Text isn't found for locator: ' "+ locator, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Text isn't found for locator: ' "+ locator);
				Report.logToDashboard("Text isn't found for locator: ' "+ locator);	
				Assert.assertTrue(false);
			}
		}
		return returnText;
	}

	public String getTextFrom(String locator,String Object) throws IOException {
		String returnText = null;
		setImplicitWaitTimeout(2);
		if(!isBlankOrNull(locator)){
			try {

				WebElement webElement = findWebElement(locator);

				returnText = webElement.getText().trim();
				Report.LogInfo("getTextFrom","Text from object : '<b><i> "+ Object+"</i></b>' is '<i>"+returnText+"</i>'","INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Text from object : ' "+ Object+"' is '"+returnText+"'");
				Report.logToDashboard("Text from object : ' "+ Object+"' is '"+returnText+"'");
			}catch(NoSuchElementException e) { 
				Report.LogInfo("getTextFrom","<b><i>'"+Object +"'</i></b> is not present on Screen", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.INFO, "'"+Object +"' is not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Report.logToDashboard("'"+Object +"' is not present on Screen");				
			}catch(Exception e){
				Report.LogInfo("getTextFrom","<b><i>'"+Object +"'</i></b> is not present on Screen", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+Object +"' is not present on Screen");
				Report.logToDashboard("'"+Object +"' is not present on Screen");
				Assert.assertTrue(false);
			}
		}

		return returnText;
	}

	public void javaScriptWait() throws IOException
	{
		String  str;
		try{
			do
			{
				JavascriptExecutor js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
				str =(String) js.executeScript("return document.readyState");

			}while(!str.equals("complete"));
		}catch(Exception e)
		{
			Report.LogInfo("Exception","Exception: "+e.getMessage(), "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Exception: "+e.getMessage());
			Report.logToDashboard("Exception: "+e.getMessage());
		}
	}

	public boolean isElementPresent(By locator) throws IOException 
	{
		String loc=null;

		try {
			setImplicitWaitTimeout(1);
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(locator);
			return true;

			//log.trace("Exiting method isTextPreset returning result [TRUE]");
		} catch (NoSuchElementException e) {
			//log.trace("Exiting method isTextPreset returning result [FALSE]");
			Report.LogInfo("isElementPresent","verify locator : \""+locator+"\" is not present", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "verify locator : \""+locator+"\" is not present");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard("verify locator : \""+locator+"\" is not present");
			return false;
			//throw e;
		}catch (Exception e) {
			//log.trace("Exiting method isTextPreset returning result [FALSE]");
			Report.LogInfo("isElementPresent","verify locator : \""+loc+"\" is not present", "INFO");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "verify locator : \""+locator+"\" is not present");
			Report.logToDashboard("verify locator : \""+locator+"\" is not present");
			Assert.assertTrue(false);
			return false;
		}	

	}

	public boolean isElementPresent(By locator,String object) throws IOException 
	{
		String loc=null;

		try {
			setImplicitWaitTimeout(1);
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(locator);

			//loc = webDriver.findElement(locator).getAttribute("id");
			Report.LogInfo("isElementPresent",object +": is present on screen", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, object +": is present on screen");
			Report.logToDashboard(object +": is present on screen");
			return true;

			//log.trace("Exiting method isTextPreset returning result [TRUE]");
		} catch (NoSuchElementException e) {
			//log.trace("Exiting method isTextPreset returning result [FALSE]");
			Report.LogInfo("isElementPresent","verify locator : \""+object+"\" is not present", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "verify locator : \""+object+"\" is not present");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard(object +": is present on screen");
			return false;
			//throw e;
		}catch (Exception e) {
			//log.trace("Exiting method isTextPreset returning result [FALSE]");
			Report.LogInfo("isElementPresent",object +": is not present on screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "verify locator : \""+object+"\" is not present");
			Report.logToDashboard(object +": is present on screen");
			Assert.assertTrue(false);
			return false;
		}

	}

	public void clearTextBox(String locator) throws IOException
	{
		WebElement webElement = findWebElement(locator);
		webElement.clear();
	}

	public void sendKeys(String locator, String textValue) throws IOException, InterruptedException 
	{ 

		WebElement webElement = null;
		if(!isBlankOrNull(locator)){
			webElement = findWebElement(locator);
			if(isBlankOrNull(textValue)) {
				return;
			}

			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					if(isBlankOrNull(textValue)) {
						return;
					}
				
					textValue = processSpaceValues(textValue);

					webElement.clear();
					webElement.sendKeys(textValue);
					Report.LogInfo("sendKeys","<i>"+textValue +"</i> entered in :<b><i>"+locator+"</i></b>", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, textValue +" entered in : "+locator);
					Report.logToDashboard(textValue +" Entered in: "+ locator);
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
			
			if(webElement == null)
			{	
				Report.LogInfo("sendKeys", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
		}
		else {
			//	throw new Exception("Invalid locator format [locator='"+locator+"']");
			Report.LogInfo("Exception", "Invalid locator format [locator='"+locator+"']", "FAIL");
			Report.logToDashboard("Invalid locator format [locator='"+locator+"']");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid locator format [locator='"+locator+"']");
			
		}
	}
	public void sendKeys1(String locator, String textValue) throws IOException, InterruptedException 
	{ 

		WebElement webElement = null;
		if(!isBlankOrNull(locator)){
			webElement = findWebElement(locator);
			if(isBlankOrNull(textValue)) {
				return;
			}

			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					if(isBlankOrNull(textValue)) {
						return;
					}
				
					textValue = processSpaceValues(textValue);

					//webElement.clear();
					webElement.sendKeys(textValue);
					Report.LogInfo("sendKeys","<i>"+textValue +"</i> entered in :<b><i>"+locator+"</i></b>", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, textValue +" entered in : "+locator);
					Report.logToDashboard(textValue +" Entered in: "+ locator);
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
			
			if(webElement == null)
			{	
				Report.LogInfo("sendKeys", locator +" not present on Screen", "FAIL");
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
		}
		else {
			//	throw new Exception("Invalid locator format [locator='"+locator+"']");
			Report.LogInfo("Exception", "Invalid locator format [locator='"+locator+"']", "FAIL");
			Report.logToDashboard("Invalid locator format [locator='"+locator+"']");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid locator format [locator='"+locator+"']");
			
		}
	}

	public void sendKeys(String locator, String textValue,String message) throws IOException 
	{ 
		try
		{
			WebElement webElement = null;
			if(!isBlankOrNull(locator)){
			
			for(int i=1; i<=2; i++)
			{
				webElement = findWebElement(locator);
				if(webElement != null)
				{	
					if(isBlankOrNull(textValue)) {
						return;
					}
				
					textValue = processSpaceValues(textValue);

					webElement.clear();
					webElement.sendKeys(textValue);
					Report.LogInfo("sendKeys","<i>"+textValue +"</i> entered in :<b><i>"+message+"</i></b>", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, textValue +" entered in : "+message);
					Report.logToDashboard(textValue +" Entered in: "+ message);
					break;
				}else
				{
					Thread.sleep(2500);
				}				
			}
			
			if(webElement == null)
			{	
				Report.LogInfo("sendKeys", message +" not present on Screen", "FAIL");
				Report.logToDashboard(message +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, message +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			}
			else {
				//	throw new Exception("Invalid locator format [locator='"+locator+"']");
				Report.LogInfo("Exception", "Invalid locator format [locator='"+locator+"']", "FAIL");
				Report.logToDashboard("Invalid locator format [locator='"+locator+"']");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid locator format [locator='"+locator+"']");
				Assert.assertTrue(false);
			}
		}
		catch(Exception e)
		{
			Report.LogInfo("sendKeys",message +" not present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, message +" not present on Screen");
			Report.logToDashboard(message +" not present on Screen");
			Assert.assertTrue(false);
		}
	}

	 public void sendKeysByJS(String locator, String value) throws InterruptedException, IOException {
	    	JavascriptExecutor js = null;
	    	WebElement webElement = null;
	    	
	    	try
	    	{
	    	for(int i=1; i<=2; i++)
	    	{
	    		webElement = findWebElement(locator);
	    		if(webElement != null)
	    		{	
	    			js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
	    	    	js.executeScript("arguments[0].value='"+ value +"';", webElement);
	    			break;
	    		}else
	    		{
	    			Thread.sleep(2500);
	    		}				
	    	}
	    				
	    	if(webElement == null)
	    	{	
	    		Report.LogInfo("sendKeys", locator +" not present on Screen", "FAIL");
	    		Report.logToDashboard(locator +" not present on Screen");
	    		ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
	    		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	    		Assert.assertTrue(false);
	    	}
	    	}catch(Exception e)
	    	{
	    		Report.LogInfo("sendKeys","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
	    		ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
	    		Report.logToDashboard(locator +" Is not Present on Screen");
	    		Assert.assertTrue(false);
	    	}
	    	
	 }
	 
	public ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator) {
		return new ExpectedCondition<WebElement>() {
			public WebElement apply(WebDriver driver) {
				WebElement toReturn = driver.findElement(locator);
				if (toReturn.isDisplayed()) {
					return toReturn;
				}
				return null;
			}
		};
	}

	public boolean isVisible(String locator) throws NoSuchElementException, IOException {
		boolean result = true;
		/*WebElement element=findWebElement(locator);
		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(element));*/
		if(!isBlankOrNull(locator)){
			try {

				result=findWebElement(locator).isDisplayed();

			} catch (Exception ignored) {
				result= false;
				
			} 
		}
		else{
			result = false;
		}
		//info("Exiting method isVisible returning result="+result);
		//log.trace("Exiting method isVisible returning result="+result);
		return result;
	}

	public boolean isVisible(By locator) throws IOException {
		//log.trace("Entering method isVisible [locator='"+locator+"']");
		boolean result = true;

		try {

			result =SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(locator).isDisplayed();

		} catch (Exception e) {
			result= false;
			//throw e;
		} 
		//info("Exiting method isVisible returning result="+result);
		//log.trace("Exiting method isVisible returning result="+result);
		return result;
	}

	
	public void waitForElementToAppear(String locator,int waitTimeInSeconds) throws InterruptedException, NoSuchElementException, IOException
	{
		WebElement element=null;		
		
		long sleepTime = (waitTimeInSeconds * 1000)/10;
		
		try
		{
		for(int i=1; i<=10; i++)
		{
			element = findWebElement(locator);
			if(element != null)
			{	
				WebDriverWait wait = new WebDriverWait(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get(), waitTimeInSeconds);
				wait.until(ExpectedConditions.visibilityOf(element));
				break;
			}else
			{
				Thread.sleep(sleepTime);
			}				
		}
					
		if(element == null)
		{	
			Report.LogInfo("waitForElementToAppear", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("click","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
	}

	public void waitForElementToPresent(By locator,int waitTimeInSeconds) throws IOException
	{
		for(int i=0;i<waitTimeInSeconds;i++)
		{
			if(isElementPresent(locator))
			{
				break;
			}
			else
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Report.LogInfo("Exception","Exception: "+e.getMessage(), "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception: "+e.getMessage());
					Report.logToDashboard("Exception: "+e.getMessage());
					Assert.assertTrue(false);
				}
			}
		}
	}

	public void waitForElementToAppear(String locator,int waitTimeInSeconds,int sleepTimeInMillSeconds) throws NoSuchElementException, IOException
	{
		for(int i=0;i<waitTimeInSeconds;i++)
		{
			if(isVisible(locator))
			{
				break;
			}
			else
			{
				sleep(sleepTimeInMillSeconds);
			}
		}
	}

	public void waitRefreshForElementToAppear(String locator,int waitTimeInSeconds,int sleepTimeInMillSeconds) throws NoSuchElementException, IOException
	{
		for(int i=0;i<waitTimeInSeconds;i++)
		{
			if(isVisible(locator))
			{
				break;
			}
			else
			{
				sleep(sleepTimeInMillSeconds);
				refreshPage();
			}
		}
	}

	public void sleep(int sleepTimeInMillSeconds) throws IOException
	{
		try {
			Thread.sleep(sleepTimeInMillSeconds);
		} catch (InterruptedException e) {
			
		}
	}

	public boolean isAlertPresent() throws IOException {
		try {
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().alert();
			return true;
		} // try
		catch (NoAlertPresentException Ex) {
			Report.LogInfo("Exception","Exception: "+Ex.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Exception: "+Ex.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard("Exception: "+Ex.getMessage());
			return false;
		} // catch
	}
	public void waitForElementToDisappear(String locator,int waitTimeInSeconds) throws NoSuchElementException, IOException
	{
		for(int i=0;i<waitTimeInSeconds;i++)
		{
			if(!isVisible(locator))
			{
				break;
			}
			else
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Report.LogInfo("Exception","Exception: "+e.getMessage(), "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception: "+e.getMessage());
					Report.logToDashboard("Exception: "+e.getMessage());
					Assert.assertTrue(false);
					
				}
			}
		}
	}

	public void waitForElementToDisappear(By locator,int waitTimeInSeconds) throws IOException
	{
		for(int i=0;i<waitTimeInSeconds;i++)
		{
			if(!isVisible(locator))
			{
				break;
			}
			else
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Report.LogInfo("Exception","Exception: "+e.getMessage(), "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception: "+e.getMessage());
					Report.logToDashboard("Exception: "+e.getMessage());
					Assert.assertTrue(false);
				}
			}
		}
	}
	
	  public boolean waitForElementToBeVisible(String locator, int timeOut) throws IOException, InterruptedException {
	    	boolean status = false;
	    	
	    	WebElement webElement=findWebElement(locator);
	    		    	
	    	for(int i=1; i<=2; i++)
	    	{
	    		webElement = findWebElement(locator);
	    		if(webElement != null)
	    		{	
	    			try {							
						FluentWait<WebDriver> fluentWait = new FluentWait<>(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get())    		     
		    				.withTimeout(timeOut, TimeUnit.SECONDS)
		    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
		    		        .ignoring(NoSuchElementException.class);  
		    				fluentWait.until(ExpectedConditions.elementToBeClickable(webElement));
		    				status = true;
	    				} catch (Exception e) {
	    					e.printStackTrace();
	    				}
	    			break;
	    		}else
	    		{
	    			Thread.sleep(2500);
	    		}				
	    	}
	    				
	    	if(webElement == null)
	    	{	
	    		status = false;
	    	}
			
			return status;
		}
	public static void assertTrue(boolean condition, String message) {
		Assert.assertTrue(condition, message);
	}

	public static void assertFalse(boolean condition, String message) {
		Assert.assertFalse(condition, message);
	}
	public static void assertEquals(Object actual, Object expected, String message) {
		Assert.assertEquals(actual, expected, message);
	}

	public static void verifyTrue(boolean condition, String message) throws IOException {
		try {
			assertTrue(condition, message);
			Report.LogInfo("verifyTrue",message, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, message);
		} catch(Throwable e) {
			addVerificationFailure(e);
			Report.LogInfo("Exception","Exception: "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception: "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard("Exception: "+e.getMessage());
			failureLog(message, "verifyTrue: " + condition);
		}
	}

	public static void verifyFalse(boolean condition) {
		verifyFalse(condition, "");
	}

	public static void verifyFalse(boolean condition, String message) {
		try {
			assertFalse(condition, message);
			Report.LogInfo("verifyFalse",message, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, message);
		} catch(Throwable e) {
			addVerificationFailure(e);
			failureLog(message, "verifyFalse: " + condition);
			Report.LogInfo("verifyFalse",message, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, message);
			softAssert.fail(message);
		}
	}

	public static void verifyEquals(boolean actual, boolean expected) throws IOException {
		verifyEquals(actual, expected, "");
	}

	public static void verifyEquals(Object actual, Object expected) throws IOException {
		verifyEquals(actual, expected, "");
	}

	public static void verifyEquals(Object actual, Object expected, String message) throws IOException {
		try {
			//log.trace("Entering method verifyEquals [actual object='"+actual +"'][expected object='"+expected +"']");
			assertEquals (actual, expected, message);
			if(message.isEmpty())
			{

				Report.LogInfo("verifyEquals","ActualDisplay text <i>'"+actual +"'</i> equal to Expected Text '<i>" +expected+"</i>'", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "ActualDisplay text '"+actual +"' equal to Expected Text '" +expected+"'");
			}
			else
			{
				Report.LogInfo("verifyEquals","ActualDisplay text <i>'"+actual +"'</i> equal to Expected Text '<i>" +expected+"</i>'", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "ActualDisplay text '"+actual +"' equal to Expected Text '" +expected+"'");
			}
		} catch(Throwable e) {
			//   addVerificationFailure(e);
			if(message.isEmpty())
			{

				Report.LogInfo("verifyEquals","ActualDisplay text <i>'"+actual +"'</i> not equal to Expected Text '<i>" +expected+"</i>'", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "ActualDisplay text '"+actual +"' not equal to Expected Text '" +expected+"'");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				softAssert.fail("ActualDisplay text "+actual +" not equal to Expected Text " +expected);
			}
			else
			{
				Report.LogInfo("verifyEquals","ActualDisplay text <i>'"+actual +"'</i> not equal to Expected Text '<i>" +expected+"</i>'", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "ActualDisplay text '"+actual +"' not equal to Expected Text '" +expected+"'");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				softAssert.fail("ActualDisplay text "+actual +" not equal to Expected Text " +expected);
			}
			failureLog(message, "verifyEquals: " + actual + " NOT EQUAL to " + expected);
		}
		//log.trace("Exiting method verifyEquals [actual object='"+actual +"'][expected object='"+expected +"']");
	}

	public static void verifyEquals(Object[] actual, Object[] expected) throws IOException {
		verifyEquals(actual, expected, "");
	}

	public static void verifyEquals(Object[] actual, Object[] expected, String message) throws IOException {
		try {
			assertEquals(actual, expected,"");
			Report.LogInfo("verifyEquals",actual +" Object Is verified with " +expected, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, actual +" Object Is verified with " +expected);
		} catch(Throwable e) {
			String failureReason = "verifyEquals: " + actual + " NOT EQUAL to " + expected;
			Report.LogInfo("verifyEquals",actual +" Object Is not verified with " +expected, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, actual +" Object Is not verified with " +expected);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			softAssert.fail(actual +" Object Is not verified with " +expected);
			
			failureReason += "\n\t\tExpected:";
			for (int i=0; i<expected.length; i++)
			{
				failureReason += (String) expected[i] + "|";
			}

			failureReason += "\n\t\tActual  : |";
			for (int i=0; i<actual.length; i++)
			{
				failureReason += (String) actual[i] + "|";
			}
			addVerificationFailure(e);
			failureLog(message, failureReason);
		}
	}


	public static void logVerificationFailure (Exception e, String message) {

		addVerificationFailure(e);
		failureLog(message, "Failure due to exception: " + e.getMessage());


	}

	public static void fail(String message) {
		Assert.fail(message);
	}

	public static List<Throwable> getVerificationFailures() {
		List<Throwable> verificationFailures = verificationFailuresMap.get(Reporter.getCurrentTestResult());
		return verificationFailures == null ? new ArrayList<Throwable>() : verificationFailures;
	}


	public static void addVerificationFailure(Throwable e)  {

		try{
			List<Throwable> verificationFailures = getVerificationFailures();
			verificationFailuresMap.put(Reporter.getCurrentTestResult(), verificationFailures);

			verificationFailures.add(e);
			failureLog (Reporter.getCurrentTestResult().getName(), "Verification Failure # " + verificationFailures.size());
			verificationFailuresMap.put(Reporter.getCurrentTestResult(), verificationFailures);

			// takeSnapshot(Reporter.getCurrentTestResult().getName() + " Failure # " + verificationFailures.size());
		}catch (Exception ex) {
			ex.getMessage();
		}
	}

	protected static void failureLog (String customMessage, String failureReason) {

		if (customMessage == "")
		{
			//log.debug("\n\t" + failureReason);
			info("\n\t" + failureReason);
			//log.trace("\n\t" + failureReason);
		}
		else
		{
			//log.debug("\n\t" + customMessage + "\n\t\t" + failureReason);

			info("\n\t" + customMessage + "\n\t\t" + failureReason);
			//log.trace("\n\t" + customMessage + "\n\t\t" + failureReason);
		}
	}


	public String getFirstSelectedOptionFromDropdown(String locator) throws IOException
	{

		WebElement element = null;
		String selectedValue = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			element = findWebElement(locator);
			if(element != null)
			{	
				Select select = new Select(element);
				String getSelectedOptionText=select.getFirstSelectedOption().toString();
				selectedValue = getSelectedOptionText;				
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(element == null)
		{	
			Report.LogInfo("getFirstSelectedOptionFromDropdown", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("getFirstSelectedOptionFromDropdown","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		return selectedValue;
		
	}

	public int getXPathCount(String locator) throws IOException {

		setImplicitWaitTimeout(2);
		return findWebElements(locator).size();
	} 

	public static String removeStart(String str, String remove)
	{
		//log.trace((new StringBuilder()).append("Entering method removeStart [str=").append(str).append(", remove=").append(remove).append("]").toString());
		String returnStr = "";
		if(isBlankOrNull(str) || isBlankOrNull(remove))
		{
			//log.debug((new StringBuilder()).append("Returned value is [str='").append(str).append("']").toString());
			returnStr = str;
		}
		if(str.startsWith(remove))
			returnStr = str.substring(remove.length());
		//log.trace((new StringBuilder()).append("Exiting method removeStart [returnStr='").append(returnStr).append("']").toString());
		return returnStr;


	}
	public static boolean isEmptyOrNull(String str)
	{
		//log.debug((new StringBuilder()).append("Inside isEmptyOrNull [str=").append(str).append("]").toString());
		return str == null || str.length() == 0;
	}

	public static boolean isFieldNone(String str)
	{
		//log.debug((new StringBuilder()).append("Inside isEmptyOrNull [str=").append(str).append("]").toString());
		return str.equalsIgnoreCase("None");
	}

	public static boolean isBlankOrNull(String str)
	{
		//log.debug((new StringBuilder()).append("Inside isBlankOrNull [str=").append(str).append("]").toString());
		return str == null || str.trim().length() == 0;
	}

	public static boolean isAlphanumeric(String str)
	{
		//log.trace((new StringBuilder()).append("Entering method isAlphanumeric [str=").append(str).append("]").toString());
		if(isBlankOrNull(str))
		{
			//log.debug("returning false");
			return false;
		}
		int sz = str.length();
		for(int i = 0; i < sz; i++)
			if(!Character.isLetterOrDigit(str.charAt(i)))
			{
				//log.debug("returning false");
				return false;
			}

		//log.trace("Exiting method isAlphanumeric");
		return true;
	}

	public static boolean validateT(Object spattern, String text, WebElement webElement) {

		String patternToBeMatched = (String) spattern;
		Pattern pattern = Pattern.compile(patternToBeMatched);

		Matcher matcher = pattern.matcher(text);
		
		try{
	
		if (!matcher.matches()) {
			//					System.out.println("FAIL");
			((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid red'", webElement);
			Report.LogInfo("validateStringPatternMatch", "Text \"" + text + "\" is not matched with pattern : \""+ spattern + "\"", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Text \"" + text + "\" is not matched with pattern : \""+ spattern + "\"");
			softAssert.fail("Text \"" + text + "\" is not matched with pattern : \""+ spattern + "\"");
		}
		else
		{
			//					System.out.println("PASS");
			((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].style.border='3px solid green'", webElement);
			Report.LogInfo("validateStringPatternMatch", "Text \"" + text + "\" is matched with pattern : \""+ spattern + "\"", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Text \"" + text + "\" is matched with pattern : \""+ spattern + "\"");
		}
		}catch(Exception e)
		{
			Report.LogInfo("validateStringPatternMatch","Exception in validateStringPatternMatch: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in validateStringPatternMatch: "+e);
			Report.logToDashboard("Exceptionin validateStringPatternMatch: "+e);
			Assert.assertTrue(false);
		}
		return matcher.matches();
	}

	public static boolean validate(Object spattern, String text) {

		String patternToBeMatched = (String) spattern;
		Pattern pattern = Pattern.compile(patternToBeMatched);

		Matcher matcher = pattern.matcher(text);
		if (!matcher.matches()) {
			//					System.out.println("FAIL");
			Report.LogInfo("validateStringPatternMatch", "Text \"" + text + "\" is not matched with pattern : \""+ spattern + "\"", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Text \"" + text + "\" is not matched with pattern : \""+ spattern + "\"");
			softAssert.fail("Text \"" + text + "\" is not matched with pattern : \""+ spattern + "\"");
		}
		else
		{
			//					System.out.println("PASS");
			Report.LogInfo("validateStringPatternMatch", "Text \"" + text + "\" is matched with pattern : \""+ spattern + "\"", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Text \"" + text + "\" is matched with pattern : \""+ spattern + "\"");
		}
		return matcher.matches();
	}

	public static boolean isNumeric(String str)
	{
		//log.trace((new StringBuilder()).append("Entering method isNumeric [str=").append(str).append("]").toString());
		if(str == null)
		{
			//log.debug("returning false");
			return false;
		}
		int sz = str.length();
		for(int i = 0; i < sz; i++)
			if(!Character.isDigit(str.charAt(i)))
			{
				//log.debug("returning false");
				return false;
			}

		//log.debug("returning true");
		//log.trace("Exiting method isNumeric");
		return true;
	}

	public boolean switchWindow(String urlContent, String windowName)
	{
		boolean flag=false;
		
		try
		{
		Set<String> handlers = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles();  
		if (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles().size()>= 1)
		{  
			for(String handler : handlers)
			{  
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handler);  
				if (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getCurrentUrl().contains(urlContent))
				{  
					Report.LogInfo("switchWindow", "Window switched to "+windowName, "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Window switched to "+windowName);
					flag=true;
					break;  
				}  
			}  
		}  
		}catch(Exception e)
		{
			Report.LogInfo("switchWindow","Exception in switchWindow: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in switchWindow: "+e);
			Report.logToDashboard("Exceptionin switchWindow: "+e);
			Assert.assertTrue(false);
		}
		return flag;

	}	    

	public void setWin(String title){

		try
		{
		String currentHandle =SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandle();


		for (String handle : SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles()) {

			if (title.equals(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle).getTitle())) {
				return;
			}
		} 
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(currentHandle);
		}catch(Exception e)
		{
			Report.LogInfo("setWindow","Exception in setWindow: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in setWindow: "+e);
			Report.logToDashboard("Exceptionin setWindow: "+e);
			Assert.assertTrue(false);
		}

	}

	public String getTitle(String currentHandle)
	{
		try
		{
		for (String handle : SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles()) 
		{

			if (!currentHandle.equals(handle))
			{
				return SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle).getTitle();
			}
		}
		}catch(Exception e)
		{
			Report.LogInfo("getTitle","Exception in getTitle: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in getTitle: "+e);
			Report.logToDashboard("Exceptionin getTitle: "+e);
			Assert.assertTrue(false);
		}
		
		return currentHandle; 
	}


	public void isChecked(String locator, String Object) throws IOException
	{
		try
		{
		boolean checked =findWebElement(locator).isSelected();

		if(checked != true)
		{
			Report.LogInfo("Unchecked", Object+" Is unchecked", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Object+" Is unchecked");
		}
		else
		{
			Report.LogInfo("Checked", Object+" Is checked", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Object+" Is checked");
		}
		}catch(Exception e)
		{
			Report.LogInfo("isChecked","Exception in isChecked: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in isChecked: "+e);
			Report.logToDashboard("Exceptionin isChecked: "+e);
			Assert.assertTrue(false);
		}
	}



	public void theSearch(String value, String locator) throws Exception
	{
		try
		{
		List<WebElement>  elementTable= SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(locator));

		for (WebElement listofElement : elementTable)
		{
			String theElement= listofElement.getText();

			if (theElement.contains(value)){
				Assert.assertEquals(value, theElement);
				// System.out.println("The Expected Value " + value + " Equals the actual " + theElement);;
			}
		}
		}catch(Exception e)
		{
			Report.LogInfo("theSearch","Exception in theSearch: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in theSearch: "+e);
			Report.logToDashboard("Exception in theSearch: "+e);
			Assert.assertTrue(false);
		}

	}

	public String getURLFromPage()
	{
		String url = null;
		try
		{
		//WebDriver driver = new WebDriver();
		url = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getCurrentUrl();
		}catch(Exception e)
		{
			Report.LogInfo("getURLFromPage","Exception in getURLFromPage: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in getURLFromPage: "+e);
			Report.logToDashboard("Exception in getURLFromPage: "+e);
			Assert.assertTrue(false);
		}
		return url;
		
	}

	public void waitToElementVisible(String locator1) throws InterruptedException, NoSuchElementException, IOException
	{
		waitForElementToAppear(locator1, 5);
	}


	public void waitToPageLoad()
	{
		try
		{
		JavascriptExecutor js = (JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		js.executeScript("return document.readyState").toString().equals("complete");
		}catch(Exception e)
		{
			Report.LogInfo("waitToPageLoad","Exception in waitToPageLoad: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in waitToPageLoad: "+e);
			Report.logToDashboard("Exception in waitToPageLoad: "+e);
			Assert.assertTrue(false);
		}
	}
	public void waitToFrameVisible() throws IOException
	{
		try
		{
			sleep(8000);
			WebDriverWait driverWait = new WebDriverWait(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get(), 20);
			driverWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
			Thread.sleep(2000);
			//webDriver.switchTo().frame(0);
			Report.LogInfo("Frame","Frame present on screen", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Frame present on screen");
			Report.logToDashboard("Frame present on screen");
		}catch(Exception e)
		{
			Report.LogInfo("Element is", "Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Element is not present on screen");
			Report.logToDashboard("Frame not present on screen");
			Assert.assertTrue(false);
		}
	}

	public void mouseMoveOn(String locator) throws IOException 
	{
		try
		{
		sleep(2000);
		WebElement element1 = findWebElement(locator);
		Actions builder = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
		//builder.build().perform();
		builder.moveToElement(element1).perform();
		sleep(2000);
		}catch(Exception e)
		{
			Report.LogInfo("mouseMoveOn", locator+" is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator+" is not Present on Screen");
			Report.logToDashboard(locator+" is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	public void keyPressOn(String locator) throws IOException 
	{
		try
		{
		WebElement element1 = findWebElement(locator);
		Actions builder = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());

		builder.keyDown(element1, Keys.CONTROL).perform();
		}catch(Exception e)
		{
			Report.LogInfo("keyPressOn", locator+" is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator+" is not Present on Screen");
			Report.logToDashboard(locator+" is not Present on Screen");
			Assert.assertTrue(false);
		}

	}
	
	public void keyPress(String locator, Keys command) throws IOException 
	{
		try
		{
			WebElement element1 = findWebElement(locator);
			element1.sendKeys(command);
		}catch(Exception e)
		{
			Report.LogInfo("keyPress", locator+" is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator+" is not Present on Screen");
			Report.logToDashboard(locator+" is not Present on Screen");
			Assert.assertTrue(false);
		}

	}

	public void clickHiddenElement(String locator)
	{
		try
		{
			JavascriptExecutor js = (JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();

			WebElement hiddenEle = (WebElement) js.executeScript("return document.getElementBy.Xpath(locator).mouseOver()");

			hiddenEle.click();
		}catch(Exception e)
		{
			Report.LogInfo("clickHiddenElement", locator+" is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator+" is not Present on Screen");
			Report.logToDashboard(locator+" is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	public static String toToggleCase(String inputString) {
		String result = "";
		for (int i = 0; i < inputString.length(); i++) {
			char currentChar = inputString.charAt(i);
			if (Character.isUpperCase(currentChar)) {
				char currentCharToLowerCase = Character.toLowerCase(currentChar);
				result = result + currentCharToLowerCase;
			} else {
				char currentCharToUpperCase = Character.toUpperCase(currentChar);
				result = result + currentCharToUpperCase;
			}
		}
		return result;
	}

	public void scrollUp() throws IOException 
	{
		try{
			JavascriptExecutor jse = (JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
			jse.executeScript("scroll(0, -250);");
		}
		catch(Exception e)
		{
			Report.LogInfo("ScrollUp","Page not able to scroll up" + e , "INFO");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Page not able to scroll up");
			Report.logToDashboard("Page not able to scroll up");
			Assert.assertTrue(false);
		}
	}

	public void scrollDown(String locator) throws IOException  
	{
		sleep(2000);
		WebElement webElement = null;
		
		try
		{
		for(int i=1; i<=2; i++)
		{
			webElement = findWebElement(locator);
			if(webElement != null)
			{	
				((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].scrollIntoView(false);", webElement);
				break;
			}else
			{
				Thread.sleep(2500);
			}				
		}
					
		if(webElement == null)
		{	
			Report.LogInfo("scrollDown", locator +" not present on Screen", "FAIL");
			Report.logToDashboard(locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Assert.assertTrue(false);
		}
		}catch(Exception e)
		{
			Report.LogInfo("scrollDown","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
	}

	public boolean isEnable(String locator) throws NoSuchElementException {
		boolean result = true;
		try
		{
		if(!isBlankOrNull(locator)){
			try {

				result=findWebElement(locator).isEnabled();

			} catch (Exception ignored) {
				result= false;
			} 
		}
		else{
			result = false;
		}
		}catch(Exception e)
		{
			Report.LogInfo("isEnable","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
		
		return result;
	}

	 @SuppressWarnings({ "resource", "deprecation" })
	public void uploadFile(String url, File file) throws IOException
	{
		 HttpResponse response = null;
		 Header[] cookie = null;
		 HttpClient httpclient = new DefaultHttpClient(); 
         HttpPost httppost = new HttpPost(url);

        
         httppost.addHeader("Accept", "*/*"); 
         httppost.addHeader("Content-type", "application/json"); 

         //File fileToUse = new File("/path_to_file/YOLO.jpg"); //e.g. /temp/dinnerplate-special.jpg 
         FileBody data = new FileBody(file); 
         String file_type = "JPG" ; 

         MultipartEntity reqEntity = new MultipartEntity(); 
         /*reqEntity.addPart("file_name", new StringBody( fileToUse.getName() ) ); 
         reqEntity.addPart("folder_id", new StringBody(folder_id)); 
         reqEntity.addPart("description", new StringBody(description)); 
         reqEntity.addPart("source", new StringBody(source)); */
         reqEntity.addPart("file_type", new StringBody(file_type)); 
         reqEntity.addPart("data", data); 
         
         httppost.setEntity(reqEntity); 
         cookie = (Header[]) response.getHeaders("Set-Cookie");
         for(int h=0; h<cookie.length; h++){
             System.out.println(cookie[h]);
         }
        httpclient.execute(httppost); 
        System.out.println( response ) ; 
       
        HttpEntity resEntity = response.getEntity(); 
        System.out.println( resEntity ) ; 
         System.out.println( EntityUtils.toString(resEntity) ); 

         EntityUtils.consume(resEntity); 
         httpclient.getConnectionManager().shutdown(); 
 } 	 
	 
	 public void waitForAjax() {

		    try {
		        WebDriverWait driverWait = new WebDriverWait(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get(), 10);

		        ExpectedCondition<Boolean> expectation;   
		        expectation = new ExpectedCondition<Boolean>() {

		            public Boolean apply(WebDriver driverjs) {

		                JavascriptExecutor js = (JavascriptExecutor) driverjs;
		                return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
		            }
		        };
		        driverWait.until(expectation);
		    }       
		    catch (TimeoutException exTimeout) {

		       // fail code
		    }
		    catch (WebDriverException exWebDriverException) {

		       // fail code
		    }
		    return;
		}
	 
	 public static boolean ClickonElementByString(String xPath, int timeOut) throws IOException, InterruptedException {
				
			boolean status = false; boolean sFlag = false;
					
			int i = 1;
			Thread.sleep(2500);   
			
	          try {
	        	  while (sFlag ==false ) { 
	        		  if(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(xPath)).isDisplayed() ){
	        		  if (i > timeOut) { status = false; break; }
	                      Thread.sleep(2000);
	                      SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(xPath)).isEnabled();
	                      scrollIntoView(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(xPath)));
	                      SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(xPath)).click();
//	                      System.out.println("Waiting for an element "+xPath+" to get clicked");
	                      sFlag = true;
	                      status = true;
	                      i = i+1;
	                } 
	        	  }
	          }  catch(NoSuchElementException e) { 
					e.printStackTrace();
					Report.LogInfo("ClickonElementByString", xPath +" not present on Screen", "FAIL");
					Report.logToDashboard(xPath +" not present on Screen");
					ExtentTestManager.getTest().log(LogStatus.FAIL, xPath +" not present on Screen");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					Assert.assertTrue(false);
				} catch(RuntimeException e) { 
					e.printStackTrace();
					Report.LogInfo("Exception", "Runtime exception: "+e, "FAIL");
					Report.logToDashboard("Runtime exception: "+e);
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Runtime exception: "+e);
					Assert.assertTrue(false);
				}
	          
	          return status;
		}
	 
	 public static boolean fluentWaitForElementToBeVisible(WebElement webElement, int timeOut) throws IOException {
	    	boolean status = false;
	    	
	    	try {							
					FluentWait<WebDriver> fluentWait = new FluentWait<>(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get())    		     
	    				.withTimeout(timeOut, TimeUnit.SECONDS)
	    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
	    		        .ignoring(NoSuchElementException.class);  
	    				fluentWait.until(ExpectedConditions.elementToBeClickable(webElement));
	    				status = true;
			} catch (Exception e) {
				Report.LogInfo("Exception","Exception in fluentWaitForElementToBeVisible: "+e.getMessage(), "FAIL");
	        	ExtentTestManager.getTest().log(LogStatus.FAIL,"Exception in fluentWaitForElementToBeVisible: "+e.getMessage());
	        	Report.logToDashboard("Exception in fluentWaitForElementToBeVisible: "+e.getMessage());
	        	Assert.assertTrue(false);
			}
			
			return status;
		}
	 
		public static boolean isElementEnabled(WebElement webElement) throws IOException {
		   	boolean status = false;
		   	
		   	try {
	   			status = webElement.isEnabled();	
		   	} catch(Exception e) {
		   		e.printStackTrace();
		   		status = false;
		   	}
			  
	       return status;
	   	} 
		
		public void clickByJS(String locator) throws IOException {
	    	 
	    	 JavascriptExecutor js = null;
	    	 WebElement webElement = null;    	 
	    	 
	    	 try {
	    		 	 			
	     			for(int i=1; i<=2; i++)
	     			{
	     				webElement = findWebElement(locator);
	     				if(webElement != null)
	     				{	
	     					js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
	    	    		 	js.executeScript("arguments[0].setAttribute('style', 'border: 2px solid blue;');", webElement);
	    	    		 	js.executeScript("arguments[0].removeAttribute('style', 'border: 2px solid blue;');", webElement);
	    	     			js.executeScript("arguments[0].click();", webElement);
	    	     			
	     					break;
	     				}else
	     				{
	     					Thread.sleep(2500);
	     				}				
	     			}
	     						
	     			if(webElement == null)
	     			{	
	     				Report.LogInfo("clickByJS", locator +" not present on Screen", "FAIL");
	     				Report.logToDashboard(locator +" not present on Screen");
	     				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
	     				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	     				Assert.assertTrue(false);
	     			}
	     			
		     } catch (Exception e) {
		    	 Report.LogInfo("Exception","Exception in clickByJS: "+e.getMessage(), "FAIL");
		    	 ExtentTestManager.getTest().log(LogStatus.FAIL,"Exception in clickByJS: "+e.getMessage());
		    	 Report.logToDashboard("Exception in clickByJS: "+e.getMessage());
		    	 Assert.assertTrue(false);
		     }			 
			 
	     }
	
		public static String Capturefullscreenshot() throws IOException {
			String screenshot2 = null;
			try
			{
				Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ImageIO.write(screenshot.getImage(), "jpg", bos);
				byte[] imageBytes = bos.toByteArray();
				screenshot2 = "data:image/png;base64," + Base64.getMimeEncoder().encodeToString(imageBytes);
				bos.close();
			}catch(Exception e)
			{
				return screenshot2;
			}
			return screenshot2;
		}
		
		public static boolean switchToDefaultFrame() throws IOException {
			 boolean status = false;
			 try {
				 SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().defaultContent();
	              status = true;
			 } catch (Exception e) {
				 Report.LogInfo("Exception","Exception in switchToDefaultFrame: "+e.getMessage(), "FAIL");
		    	 ExtentTestManager.getTest().log(LogStatus.FAIL,"Exception in switchToDefaultFrame: "+e.getMessage());
		    	 Report.logToDashboard("Exception in switchToDefaultFrame: "+e.getMessage());
		    	 Assert.assertTrue(false);
			 }

	       return status;
		  }			
		 
		 public void selectByValueDIV(String mainElement, String listElement, String value) throws InterruptedException, IOException 
		 {
			 WebElement element = null;
			 WebElement Listelement = null;
			 
			 try
			 {
			 for(int i=1; i<=2; i++)
			 {
				element = findWebElement(mainElement);
			    Listelement = findWebElement(listElement);
			 	if((element != null) && (Listelement != null))
			 	{	
			 		scrollIntoView(element);
			    	element.click();
			    	Thread.sleep(1000);
			    	isVisible(listElement);
			    	List<WebElement> options = Listelement.findElements(By.tagName("li"));
					for (WebElement option : options)
					{
						if (option.getText().equals(value))
						{
						    scrollIntoView(option);
						    option.click();
						    Thread.sleep(1250);
						    break;
						 }
					}	
			 		break;
			 	}else
			 	{
			 		Thread.sleep(2500);
			 	}				
			 }
			 			
			 if((element == null) | (Listelement == null))
			 {	
			 	Report.LogInfo("selectByValueDIV", mainElement+ " OR "+ listElement +" not present on Screen", "FAIL");
			 	Report.logToDashboard(mainElement+ " OR "+ listElement +" not present on Screen");
			 	ExtentTestManager.getTest().log(LogStatus.FAIL, mainElement+ " OR "+ listElement +" not present on Screen");
			 	ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			 	Assert.assertTrue(false);
			 }		    			    	
			 }catch(Exception e)
			 {
			 	Report.LogInfo("selectByValueDIV","<font color=red"+mainElement+ " OR "+ listElement +" Is not Present on Screen</font>", "FAIL");
			 	ExtentTestManager.getTest().log(LogStatus.FAIL, mainElement+ " OR "+ listElement +" not present on Screen");
			 	Report.logToDashboard(mainElement+ " OR "+ listElement +" not present on Screen");
			 	Assert.assertTrue(false);
			 }						
		}
	
		 public static boolean scrollIntoTop() throws IOException {
			 boolean status = false;
		    	
			 try {
				 	((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
				 } catch (Exception e) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to scroll to top of the page, please verify");
					Report.LogInfo("Exception","Unable to scroll to top of the page, please verify", "FAIL");   	 
			    	Report.logToDashboard("Unable to scroll to top of the page, please verify");
			    	Assert.assertTrue(false);
				 }
				
			 return status;
		 }
		 
		  public boolean clickByAction(String locator) throws IOException, InterruptedException {
		     	 boolean status = false;     
		     	WebElement webElement = null;
		     	waitForElementToBeVisible(locator, 30);
		     			     	 
		     	 try {
		     		 	webElement = findWebElement(locator);
		     		 	Actions build = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
		        		build.moveToElement(webElement).click().build().perform();
		      			status = true;
		      			
		 	     } catch (Exception e) {
		 	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to ActionClick the webelement: " + webElement.toString()+ "due to " +e.toString());
		 	    	Report.LogInfo("Exception","Unable to ActionClick the webelement: " + webElement.toString()+ "due to " +e.toString(), "FAIL");   	 
			    	Report.logToDashboard("Unable to ActionClick the webelement: " + webElement.toString()+ "due to " +e.toString());
			    	Assert.assertTrue(false);
		 	     }
		 		 
		 		 return status;
		      }		  


/*public static void switchToFrame(String idNameIndex) throws IOException {
    boolean status = false;
    try {
    	SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().frame(idNameIndex);
         status = true;
    } catch (Exception e) {
       ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to switch to frame: " + idNameIndex+ "due to " +e.toString());
       ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
       Report.LogInfo("Exception","Unable to switch to frame: " + idNameIndex+ "due to " +e.toString(), "FAIL");   	 
   	   Report.logToDashboard("Unable to switch to frame: " + idNameIndex+ "due to " +e.toString());
   	   Assert.assertTrue(false);
    }          
 }*/

public static void switchToFrame(String idNameIndex) throws IOException {
    boolean status = false;
    try {
    	SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().frame(idNameIndex);
         status = true;
    } catch (Exception e) {
       ExtentTestManager.getTest().log(LogStatus.ERROR, "Unable to switch to frame: " + idNameIndex+ "due to " +e.toString());
       ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
       System.out.println("Unable to switch to frame: " + idNameIndex + "due to " +e.toString());               
    }          
 }

public String storeByValueDIV(String mainElement, String listElement) throws IOException 
{
	 String listValues="";
 	 WebElement mainEle = findWebElement(mainElement);
 	 WebElement subElements = findWebElement(listElement);
	try {
		ScrollIntoViewByString(mainElement);
		Actions build = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
		build.moveToElement(mainEle).click().build().perform();
		waitForElementToAppear(listElement,15);
		List<WebElement> options = subElements.findElements(By.tagName("li"));
		for (WebElement option : options)
		{
			String value=option.getText();
			listValues=listValues+value+"|";
		}
			
	} catch (Exception e) {
		ExtentTestManager.getTest().log(LogStatus.ERROR, "Unable to select the value from listbox: " + listElement.toString()+ "due to " +e.toString());
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		System.out.println("Unable to select the value from listbox: " + listElement.toString()+ "due to " +e.toString());
		throw new SkipException("Skipping this test");
	}
	return listValues;
}

public boolean waitForInvisibilityOfElement(String locator, int timeOut) throws IOException {
	boolean status = false;
	
	WebElement webElement=findWebElement(locator);
	
	try {	  	
			FluentWait<WebDriver> fluentWait = new FluentWait<>(WEB_DRIVER_THREAD_LOCAL.get()) 
				.withTimeout(timeOut, TimeUnit.SECONDS)
		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
		        .ignoring(NoSuchElementException.class);
				fluentWait.until(ExpectedConditions.invisibilityOf(webElement));
				status = true;
						
	} catch (Exception e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Webelement was still Visible: " + webElement.toString()+ "due to " +e.toString());
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		Report.LogInfo("Exception","Webelement was still Visible: " + webElement.toString()+ "due to " +e.toString(), "FAIL");   	 
	   	Report.logToDashboard("Webelement was still Visible: " + webElement.toString()+ "due to " +e.toString()); 
	   	Assert.assertTrue(false);
	}
	
	return status;
}

public static void pause(Integer waitTime) throws IOException {
    try {
        	Thread.sleep(waitTime);
    } catch (Exception e) {
    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to wait the execution");
    	ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		Report.LogInfo("Exception", "Unable to wait the execution", "FAIL");   	 
	   	Report.logToDashboard("Unable to wait the execution"); 
    }
 }	

	public boolean isPresent(String locator, int timeOut) throws IOException, InterruptedException {
	   	boolean status = false;
	   	
	   	WebElement webElement=findWebElement(locator);	   	
	    
	   	try {
	   		if(waitForElementToBeVisible(locator, timeOut))
	   		{
	   			webElement.isDisplayed();
	   			status = true;
	   		}else
	   		{
	   			Report.LogInfo("isPresent", "Webelement is not present: " + locator, "INFO");   	 
			   	Report.logToDashboard("Webelement is not present: " + locator);
			   	ExtentTestManager.getTest().log(LogStatus.INFO, "Webelement is not present: " + locator);
			}
	   	} catch(Exception e) {
	   		
	   	}
			  
	    return status;
   	} 

	public void MoveToElement(String locator) throws IOException {
		
		try
		{
			WebElement element = findWebElement(locator);
			Actions actions = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
			actions.moveToElement(element);
			actions.perform();
		}catch(Exception e)
		{
			Report.LogInfo("MoveToElement","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	 }
	
	////////nmts
	public void WaitForAjax() throws IOException {

	    try {
	        WebDriverWait driverWait = new WebDriverWait(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get(), 5);

	        ExpectedCondition<Boolean> expectation;   
	        expectation = new ExpectedCondition<Boolean>() {

	            public Boolean apply(WebDriver driverjs) {

	                JavascriptExecutor js = (JavascriptExecutor) driverjs;
	                return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
	            }
	        };
	        driverWait.until(expectation);
	    }       
	    catch (TimeoutException exTimeout) {

	       // fail code	    	 
	    }
	    catch (WebDriverException exWebDriverException) {

	       // fail code
	    }
	    return;
	}

	public void javaScriptDoubleclick(String locator,String Object) throws IOException 
	{

		try
		{
			WebElement element = findWebElement(locator);
			if(g.getBrowser().equalsIgnoreCase("firefox"))
			{
				element.click();
			}
			else if(g.getBrowser().equalsIgnoreCase("chrome"))
			{
				element.click();
			}
			else 
			{
				scrollIntoView(element);
				sleep(1000);
				((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].fireEvent('ondblclick');",element);
					
			}			

			Report.LogInfo("doubleClick","\""+Object +"\" Is Clicked Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Object +" Is Clicked Successfully");
			Report.logToDashboard(Object +" Is Clicked Successfully");
		}catch(Exception e)
		{
			Report.LogInfo("DoubleClick",Object +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" Is not Present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard(Object +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	public static void AcceptAlert() throws AWTException {
		try
		{
		Robot robot = new Robot();
		robot.delay(3000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(5000);
		}catch(Exception e)
		{
			Report.LogInfo("AcceptAlert","Exception in AcceptAlert: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in AcceptAlert: "+e);
			Report.logToDashboard("Exception in AcceptAlert: "+e);
			Assert.assertTrue(false);
		}
	}

	public void AcceptJavaScriptMethod() throws InterruptedException{
		Thread.sleep(1000);
		try
		{
		Alert alert = WEB_DRIVER_THREAD_LOCAL.get().switchTo().alert();
		alert.accept();
		WEB_DRIVER_THREAD_LOCAL.get().switchTo().defaultContent();
		}catch(Exception e)
		{
			Report.LogInfo("AcceptJavaScriptMethod","Exception in AcceptJavaScriptMethod: "+e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in AcceptJavaScriptMethod: "+e);
			Report.logToDashboard("Exception in AcceptJavaScriptMethod: "+e);
			Assert.assertTrue(false);
		}
	}
	
	
	public static void openNMTSUrl(String url) throws AWTException {

		Robot robot = new Robot();
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
	    try{
	    	SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().get(url);
	    } catch (TimeoutException e){
	        	robot.delay(5000);
	        	String UserName = Configuration.NMTS_Username;
		        StringSelection stringSelection = new StringSelection(UserName);
		        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		        clipboard.setContents(stringSelection, stringSelection);

		        robot.keyPress(KeyEvent.VK_CONTROL);
		        robot.keyPress(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_CONTROL);
	      
		    //    robot.keyPress(KeyEvent.VK_U);
		        robot.keyPress(KeyEvent.VK_TAB);
		        
		        robot.delay(2000);
		        String Password = Configuration.NMTS_Password;
		        StringSelection stringSelection2 = new StringSelection(Configuration.NMTS_Password);
		        Clipboard clipboard2 = Toolkit.getDefaultToolkit().getSystemClipboard();
		        clipboard.setContents(stringSelection2, stringSelection2);

		        robot.keyPress(KeyEvent.VK_CONTROL);
		        robot.keyPress(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_V);
		        robot.keyRelease(KeyEvent.VK_CONTROL);
		        robot.delay(2000);
		        
		   //    robot.keyPress(KeyEvent.VK_P);
		        robot.keyPress(KeyEvent.VK_ENTER);
	       // robot.keyPress(KeyEvent.);
	        
		        robot.delay(5000);
	        
	     
		    
	    }
	}
	public void javaScriptclick2(String locator,String Object) throws IOException 
	{

		try
		{
			WebElement element = findWebElement(locator);
			if(g.getBrowser().equalsIgnoreCase("firefox"))
			{
				element.click();
			}
			else if(g.getBrowser().equalsIgnoreCase("chrome"))
			{
				element.click();
			}
			else 
			{
			//	Thread.sleep(15000);
				((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);
				Thread.sleep(20000);
				//	((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();",  element);
			//	((JavascriptExecutor)SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].fireEvent.click();",element);
				
			}			

			Report.LogInfo("Click","\""+Object +"\" Is Clicked Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Object +" Is Clicked Successfully");
			Report.logToDashboard(Object +" Is Clicked Successfully");
		}catch(Exception e)
		{
			Report.LogInfo("Click",Object +" Is not Present on Screen", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, Object +" Is not Present on Screen");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.logToDashboard(Object +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}

	
	//============================= APT Reusables ====================================
	public void compareText_InViewPage(String labelname, String expectedVal) throws IOException {
		WebElement element = null;
		String el1 = "//div[div[label[contains(text(),'";
		String el2 = "')]]]/div[2]";
		// element =
		// findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
		element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(el1 + labelname + el2));
		String actualVal = element.getText().toString();

		if (actualVal.contains(expectedVal)) {
			Report.LogInfo("CompareText", "Text match on View page", "PASS");
			Report.logToDashboard("Text match on View page");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Text match on View page");
		} else {
			Report.LogInfo("CompareText", "Text not match on View page", "FAIL");
			Report.logToDashboard("Text not match on View page");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Text not match on View page");
		}
	}

	public void compareText_InViewPage1(String labelname, String expectedVal) throws IOException {
		WebElement element = null;
		String el1 = "//div[div[label[contains(text(),'";
		String el2 = "')]]]/div[2]";
		// element =
		// findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
		element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(el1 + labelname + el2));
		String actualVal = element.getText().toString();

		if (expectedVal.contains(actualVal)) {
			Report.LogInfo("CompareText", "Text match on View page", "PASS");
		} else {
			Report.LogInfo("CompareText", "Text not match on View page", "FAIL");
		}

	}

	public void compareText_InViewPage2(String labelname, String expectedVal) throws IOException {
		WebElement element = null;
		String el1 = "(//div[div[label[contains(text(),'";
		String el2 = "')]]]//following-sibling::div[1])[1]";
		// element =
		// findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
		element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(el1 + labelname + el2));
		String actualVal = element.getText().toString();

		if (actualVal.contains(expectedVal)) {
			Report.LogInfo("CompareText", "Text match on View page", "PASS");
		} else {
			Report.LogInfo("CompareText", "Text not match on View page", "Failed");
		}
	}

	public void compareText_InViewPage3(String labelname, String expectedVal) throws IOException {
		WebElement element = null;
		String el1 = "(//div[div[label[contains(text(),'";
		String el2 = "')]]]/div[2])[2]";
		// element =
		// findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
		element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(el1 + labelname + el2));
		String actualVal = element.getText().toString();

		if (actualVal.contains(expectedVal)) {
			Report.LogInfo("CompareText", "Text match on View page", "PASS");
			Report.logToDashboard("Text match on View page");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Text match on View page");
		} else {
			Report.LogInfo("CompareText", "Text not match on View page", "FAIL");
			Report.logToDashboard("Text not match on View page");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Text not match on View page");
		}
	}

	public void edittextFields_commonMethod(String labelname, String xpathname, String expectedValueToEdit)
			throws InterruptedException, IOException {
		boolean availability = false;

		availability = isVisible(xpathname);

		if (availability) {

			if (expectedValueToEdit.equalsIgnoreCase("null")) {

				String actualvalue = getAttributeFrom(xpathname, "value");
			} else {

				clearTextBox(xpathname);
				waitForAjax();

				sendKeys(xpathname, expectedValueToEdit);
				String actualvalue = getAttributeFrom(xpathname, "value");
				Report.LogInfo("CompareText", "text field is edited", "PASS");
			}

		} else {
			Report.LogInfo("CompareText", "text field is not displaying", "FAIL");

		}

	}

	public void selectValueInsideDropdown(String xpath, String labelname, String expectedValueToAdd)
			throws IOException, InterruptedException {
		// getAllValuesInsideDropDown
		List<String> ls = new ArrayList<String>();

		// availability=getwebelement(xml.getlocator("//locators/" + application
		// + "/"+ xpath +"")).isDisplayed();
		if (isVisible(xpath)) {
			Report.LogInfo("DropDown", labelname + "dropdown is displaying", "PASS");

			Select element = new Select(findWebElement(xpath));
			String firstSelectedOption = element.getFirstSelectedOption().getText();
			List<WebElement> we = element.getOptions();

			for (WebElement a : we) {
				if (!a.getText().equals("select")) {
					ls.add(a.getText());

				}
			}

			// ExtentTestManager.getTest().log(LogStatus.PASS, "list of values
			// inside "+labelname+" dropdown is: "+ls);
			// Log.info("list of values inside "+labelname+" dropdown is: "+ls);

			if (expectedValueToAdd.equalsIgnoreCase("null")) {
				Report.LogInfo("DropDown", "No values selected under" + labelname, "PASS");
			} else {
				Select s1 = new Select(findWebElement(xpath));
				s1.selectByVisibleText(expectedValueToAdd);

				String SelectedValueInsideDropdown = element.getFirstSelectedOption().getText();
				Report.LogInfo("DropDown", labelname + "dropdown value selected as:" + SelectedValueInsideDropdown,
						"PASS");

			}
		}

	}

	public void editcheckbox_commonMethod(String expectedResult, String xpath, String labelname) throws IOException {

		if (!expectedResult.equalsIgnoreCase("null")) {
			// boolean
			// isElementSelected=getwebelement(xml.getlocator("//locators/" +
			// application + "/"+ xpath +"")).isSelected();
			boolean isElementSelected = findWebElement(xpath).isSelected();

			if (expectedResult.equalsIgnoreCase("yes")) {

				if (isElementSelected) {
					Report.LogInfo("CheckBox", "checkbox is not edited and it is already Selected while creating",
							"PASS");
					// ExtentTestManager.getTest().log(LogStatus.PASS, labelname
					// +" checkbox is not edited and it is already Selected
					// while creating");
				} else {
					click(xpath);
					Report.LogInfo("CheckBox", labelname + "checkbox is selected", "PASS");
				}
			} else if (expectedResult.equalsIgnoreCase("no")) {

				if (isElementSelected) {
					click(xpath);
					Report.LogInfo("CheckBox", labelname + "is edited and gets unselected", "PASS");
				} else {
					Report.LogInfo("CheckBox", labelname + "is not edited and it remains unselected", "PASS");
				}

			}
		} else {
			Report.LogInfo("CheckBox", "No changes made for" + labelname, "PASS");
		}
	}

	public void addCheckbox_commonMethod(String xpath, String labelname, String expectedValue)
			throws InterruptedException, IOException {

		boolean availability = false;

		availability = isVisible(xpath);
		if (availability) {
			Report.LogInfo("INFO", "checkbox is displaying as expected", "PASS");
			if (!expectedValue.equalsIgnoreCase("null")) {
				if (expectedValue.equalsIgnoreCase("yes")) {
					click(xpath, labelname);
					waitForAjax();
					boolean CPEselection = isSelected(xpath, labelname);
					if (CPEselection) {
						Report.LogInfo("INFO", "checkbox is selected as expected", "PASS");
					} else {
						Report.LogInfo("INFO", "checkbox is not selected", "FAIL");
					}
				} else {
					Report.LogInfo("INFO", "checkbox is not selected as expected", "PASS");
				}
			}
		} else {
			Report.LogInfo("INFO", "checkbox is not available", "FAIL");
		}
	}

	public void selectAndRemoveValueFromRightDropdown(String labelname, String xpath, String[] selectValue,
			String xpathForRemoveButton) {

		WebElement availability = null;
		List<String> ls = new ArrayList<String>();

		try {
			// List<WebElement> elements=
			// getwebelements(xml.getlocator("//locators/" + application + "/"+
			// xpath +""));
			List<WebElement> elements = findWebElements(xpath);
			// int element_count= elements.size();
			int element_count = getXPathCount(xpath);

			if (element_count >= 1) {

				// Print list of values inside Dropdown
				for (WebElement a : elements) {
					ls.add(a.getText());
				}
				Report.LogInfo("INFO",
						"list of values displaying inside " + labelname + " available dropdown is: " + ls, "PASS");

				// select value inside the dropdown
				for (int i = 0; i < selectValue.length; i++) {
					waitForAjax();
					for (int j = 0; j < ls.size(); j++) {
						// Log.info("ls value "+ ls.get(j));
						if (selectValue[i].equals(ls.get(j))) {
							elements.get(j).click();
							Report.LogInfo("INFO", elements.get(j) + " got selected", "PASS");
							waitForAjax();
							// WebElement
							// removeButton=getwebelement(xml.getlocator("//locators/"
							// + application + "/"+ xpathForRemoveButton
							// +"").replace("value", "<<"));
							WebElement removeButton = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(
									By.xpath(removeStart(xpathForRemoveButton, "@xpath=").replace("value", "<<")));
							// WebElement removeButton =
							// findWebElement(xpathForRemoveButton).replace("value",
							// "<<");
							removeButton.click();
							// click(removeButton);
							Report.LogInfo("INFO", "clicked on remove '<<' button", "PASS");
							// ExtentTestManager.getTest().log(LogStatus.PASS,
							// "clicked on remove '<<' button");
							waitForAjax();
						}
					}
				}

			} else {
				Report.LogInfo("INFO", "No values displaying under " + labelname + " dropdown", "PASS");
			}
		} catch (Exception e) {
			Report.LogInfo("INFO", "No values displaying under " + labelname + " available dropdown", "FAIL");
		}
	}

	public void selectAndAddValueFromLeftDropdown(String labelname, String xpath, String[] selectValue,
			String xpathForAddButton) {

		WebElement availability = null;
		List<String> ls = new ArrayList<String>();

		try {
			// List<WebElement> elements=
			// getwebelements(xml.getlocator("//locators/" + application + "/"+
			// xpath +""));
			// int element_count= elements.size();
			List<WebElement> elements = findWebElements(xpath);
			// int element_count= elements.size();
			int element_count = getXPathCount(xpath);

			if (element_count >= 1) {

				// Print list of values inside Dropdown
				for (WebElement a : elements) {
					ls.add(a.getText());
				}
				Report.LogInfo("INFO",
						"list of values displaying inside " + labelname + " available dropdown is: " + ls, "PASS");
				// select value inside the dropdown
				for (int i = 0; i < selectValue.length; i++) {
					//Thread.sleep(5000);
					for (int j = 0; j < ls.size(); j++) {
						// Log.info("ls value "+ ls.get(j));
						if (selectValue[i].equals(ls.get(j))) {
							elements.get(j).click();
							//Report.LogInfo("INOF", elements.get(j) + " got selected", "PASS");
							waitForAjax();
							click(xpathForAddButton, "Add");
							waitForAjax();
						}
					}
				}

			} else {
				Report.LogInfo("INFO", "No values displaying under " + labelname + " dropdown", "PASS");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", "No values displaying under " + labelname + " available dropdown", "FAIL");

		}
	}

	public void verifySelectedValuesInsideRightDropdown(String labelname, String xpath) {
		// getAllValuesInsideDropDown
		boolean availability = false;
		List<String> ls = new ArrayList<String>();

		try {

			// List<WebElement> elements=
			// getwebelements(xml.getlocator("//locators/" + application + "/"+
			// xpath +""));
			// int element_count= elements.size();
			List<WebElement> elements = findWebElements(xpath);
			int element_count = getXPathCount(xpath);

			if (element_count >= 1) {

				// Print list of values inside Dropdown
				for (WebElement a : elements) {
					ls.add(a.getText());
				}
				Report.LogInfo("INFO",
						"list of values displaying inside " + labelname + " available dropdown is: " + ls, "PASS");
			} else {
				Report.LogInfo("INFO", "No values displaying under " + labelname + " dropdown", "PASS");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", "No values displaying under " + labelname + " available dropdown", "FAIL");
		}
	}

	public void compareTextForViewUserPage(String labelname, String ExpectedText) throws InterruptedException {

		String text = null;
		// WebElement element = null;

		try {
			Thread.sleep(1000);

			// element =
			// getwebelement(xml.getlocator("//locators/"+application+"/viewUser_fetchValuesForAddedUser").replace("value",
			// labelname));
			// String emptyele = element.getText().toString();
			String emptyele = getTextFrom(APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_fetchValuesForAddedUser1
					+ labelname + APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_fetchValuesForAddedUser2,"Fetch value for add user");
			if (emptyele != null && emptyele.isEmpty()) {
				// ExtentTestManager.getTest().log(LogStatus.PASS, labelname +
				// "' value is empty");
				// emptyele= "Null";
				// sa.assertEquals(emptyele, ExpectedText, labelname + " value
				// is not displaying as expected");

				if (emptyele.equalsIgnoreCase(ExpectedText)) {
					Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
							+ "' is same as the Acutal value '" + text + "'", "PASS");
				} else {
					Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
							+ "' is not same as the Acutal value '" + text + "'", "FAIL");
				}
			} else {
				text = getTextFrom(APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_fetchValuesForAddedUser1
						+ labelname + APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_fetchValuesForAddedUser2,"");
				if (text.equals(ExpectedText)) {
					Report.LogInfo("INFO", " The Expected value for '" + labelname + "' field '" + ExpectedText
							+ "' is same as the Acutal value '" + text + "'", "PASS");
				} else if (text.contains(ExpectedText)) {
					Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
							+ "' is same as the Acutal value '" + text + "'", "PASS");
				} else {
					Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
							+ "' is not same as the Acutal value '" + text + "'", "FAIL");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", " field is not displaying", "FAIL");

		}

	}

//	public void waitforPagetobeenable() throws InterruptedException {
//		WebElement el = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//body"));
//
//		// Log.info("Start");
//		while (el.getAttribute("class").contains("loading-indicator")) {
//			// Log.info("Page Loading");
//			Thread.sleep(5000);
//		}
//
//	}

	public void compareText(String labelname, String xpath, String ExpectedText) throws InterruptedException {

		String text = null;
		WebElement element = null;

		try {
			// element = getwebelement(xml.getlocator("//locators/" +
			// application + "/" + xpath + ""));
			element = findWebElement(xpath);
			String emptyele = getTextFrom(xpath);
			// String emptyele = getwebelement(xml.getlocator("//locators/" +
			// application + "/" + xpath + ""))
			// .getAttribute("value");
			if (element == null) {
				Report.LogInfo("INFO", "Step:  '" + labelname + "' not found", "FAIL");
				// ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: '" +
				// labelname + "' not found");
			} else if (emptyele != null && emptyele.isEmpty()) {
				Report.LogInfo("INFO", "Step : '" + labelname + "' value is empty", "PASS");
				// ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '" +
				// labelname + "' value is empty");
			} else {

				text = element.getText();
				if (text.contains("-")) {

					String[] actualTextValue = text.split(" ");
					String[] expectedValue = ExpectedText.split(" ");

					if (expectedValue[0].equalsIgnoreCase(actualTextValue[0])) {
						Report.LogInfo("INFO", " The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
					} else if (expectedValue[0].contains(actualTextValue[0])) {
						Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
					} else {
						Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is not same as the Acutal value '" + text + "'", "FAIL");
					}
				} else {
					if (ExpectedText.equalsIgnoreCase(text)) {
						Report.LogInfo("INFO", " The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
					} else if (ExpectedText.contains(text)) {
						Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
					} else {
						Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is not same as the Acutal value '" + text + "'", "FAIL");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");

		}
	}

	public void addDropdownValues_commonMethod(String labelname, String xpath, String expectedValueToAdd)
			throws InterruptedException {
		boolean availability = false;
		List<String> ls = new ArrayList<String>();

		try {

			// availability=getwebelementNoWait(xml.getlocator("//locators/" +
			// application + "/"+ xpath +"")).isDisplayed();
			if (isVisible(xpath)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				if (expectedValueToAdd.equalsIgnoreCase("null")) {
					Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {
					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[label[text()='" + labelname + "']]//div[text()='�']"))
							.click();
					// Clickon(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//div[text()='�']"));

					// verify list of values inside dropdown
					List<WebElement> listofvalues = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()
							.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

					for (WebElement valuetypes : listofvalues) {
						// Log.info("List of values : " + valuetypes.getText());
						ls.add(valuetypes.getText());
					}
					waitForAjax();

					// ExtentTestManager.getTest().log(LogStatus.PASS, "list of
					// values inside "+labelname+" dropdown is: "+ls);
					// System.out.println("list of values inside "+labelname+"
					// dropdown is: "+ls);

					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[label[text()='" + labelname + "']]//input"))
							.sendKeys(expectedValueToAdd);
					// SendKeys(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//input"), expectedValueToAdd);

					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//div[label[text()='" + labelname + "']]//div[contains(text(),'"
							+ expectedValueToAdd + "')])[1]")).click();
					
				}
			} else {
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
				// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + "
				// is not displaying");
				// System.out.println(labelname + " is not displaying");
			}
		} catch (NoSuchElementException e) {
			// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is
			// not displaying");
			System.out.println(labelname + " is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" NOt able to perform selection under " + labelname + " dropdown");
			System.out.println(" NO value selected under " + labelname + " dropdown");
		}
	}

	public void compareText_InViewPage_ForNonEditedFields(String labelname) throws InterruptedException {

		// String text = null;
		WebElement element = null;

		try {
			Thread.sleep(1000);
			// element = getwebelement("//div[div[label[contains(text(),'" +
			// labelname + "')]]]/div[2]");
			element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[div[label[contains(text(),'" + labelname + "')]]]/div[2]"));
			String emptyele = element.getText().toString();

			Report.LogInfo("INFO", labelname + " field is not edited. It is displaying as '" + emptyele + "'", "PASS");
			/*
			 * ExtentTestManager.getTest().log(LogStatus.PASS, labelname +
			 * " field is not edited. It is displaying as '" + emptyele + "'");
			 * Log.info(labelname +
			 * " field is not edited. It is displaying as '" + emptyele + "'");
			 */
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");

			/*
			 * ExtentTestManager.getTest().log(LogStatus.FAIL, labelname +
			 * " field is not displaying"); Log.info(labelname +
			 * " field is not displaying");
			 */
		}

	}

	public void SendkeyusingAction(Keys k) {
		Actions keyAction = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
		keyAction.sendKeys(k).perform();
	}

	public void CompareText(String locator, String newValueFromExcel) throws IOException {
		// WebElement webElement = findWebElement(locator);
		String value = getTextFrom(locator);

		if (value.equals(newValueFromExcel)) {
			System.out.println("Field matches new value");
		} else {
			System.out.println("Field is not edited");
		}
	}

	public boolean isElementPresent(String locator) {
		String loc = null;
		try {
			setImplicitWaitTimeout(1);
			WebElement element = findWebElement(locator);

			if (element.isDisplayed() && element.isEnabled())
				return true;

		} catch (NoSuchElementException e) {
			return false;
			// throw e;
		} catch (Exception e) {
			return false;
			// throw e;
		}
		return false;
	}

	public void SelectDropdownValueUnderSelectTag(String labelname, String dropdownToBeSelectedInTheEnd,
			String dropdownXpath) throws InterruptedException, IOException {
		{
			// SelectDropdownValueUnderSelectTag
			boolean availability = false;
			List<String> ls = new ArrayList<String>();

			try {
				// availability=getwebelement(xml.getlocator("//locators/" +
				// application + "/"+ dropdownXpath +"")).isDisplayed();
				if (isVisible(dropdownXpath)) {
					Report.LogInfo("INFO", " dropdown is displaying", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
					// System.out.println(labelname + " dropdown is
					// displaying");

					// WebElement el =getwebelement(xml.getlocator("//locators/"
					// + application + "/"+ dropdownXpath +""));
					WebElement el = findWebElement(dropdownXpath);
					Select sel = new Select(el);

					String firstSelectedOption = sel.getFirstSelectedOption().getText();
					// ExtentTestManager.getTest().log(LogStatus.PASS, "By
					// default "+ labelname+" dropdown is displaying as:
					// "+firstSelectedOption);
					// System.out.println("By default "+ labelname+" dropdown is
					// displaying as: "+firstSelectedOption);

					List<WebElement> we = sel.getOptions();

					for (WebElement a : we) {
						if (!a.getText().equals("select")) {
							ls.add(a.getText());

						}
					}

					Report.LogInfo("INFO", "list of values inside " + labelname + " dropdown is: " + ls, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"list of values inside " + labelname + " dropdown is: " + ls);
					// System.out.println("list of values inside "+labelname+"
					// dropdown is: "+ls);

					if (dropdownToBeSelectedInTheEnd.equalsIgnoreCase("null")) {
						Report.LogInfo("INFO", "No values selected under " + labelname + " dropdown", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"No values selected under " + labelname + " dropdown");
					} else {
						Select s1 = new Select(el);
						s1.selectByVisibleText(dropdownToBeSelectedInTheEnd);

						String SelectedValueInsideDropdown = sel.getFirstSelectedOption().getText();
						Report.LogInfo("INFO",
								labelname + " dropdown value selected as: " + SelectedValueInsideDropdown, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								labelname + " dropdown value selected as: " + SelectedValueInsideDropdown);
						// System.out.println(labelname+" dropdown value
						// selected as: "+SelectedValueInsideDropdown);
					}
				}

			} catch (NoSuchElementException e) {
				Report.LogInfo("INFO", labelname + " Value is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " Value is not displaying");
				// System.out.println(labelname + " value is not displaying");
			} catch (Exception ee) {
				ee.printStackTrace();
				Report.LogInfo("INFO", " NOt able to perform selection under " + labelname + " dropdown", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" NOt able to perform selection under " + labelname + " dropdown");
				// System.out.println(" NO value selected under "+ labelname + "
				// dropdown");
			}
		}
	}

	public void ClearAndEnterTextValue(String labelname, String xpath, String newValue) {
		WebElement element = null;
		try {
			// Thread.sleep(1000);
			// element= getwebelement(xml.getlocator("//locators/" + application
			// + "/"+ xpath +""));
			element = findWebElement(xpath);
			String value = element.getAttribute("value");

			if (value.isEmpty()) {
				Report.LogInfo("INFO", "Step: '" + labelname + "' text field is empty", "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Step: '" + labelname + "' text field is empty");

			} else {
				element.clear();
				Thread.sleep(1000);
				element.sendKeys(newValue);
				Report.LogInfo("INFO", "Step: Entered '" + newValue + "' into '" + labelname + "' text field", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step: Entered '" + newValue + "' into '" + labelname + "' text field");
			}

		} catch (Exception e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Not able to enter '" + newValue + "' into '" + labelname + "' text field");
			e.printStackTrace();
		}

	}

	public void openLinkInNewTab(String xpath, String linkName) throws InterruptedException, AWTException, IOException {

		WebElement el;
		el = findWebElement(xpath);
		Actions action = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
		Robot r = new Robot();

		action.keyDown(Keys.CONTROL).click(el).build().perform();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on " + linkName + " link");
		Reporter.log("Clicked on " + linkName + " link");

	}

	public void Switchtotab() throws Exception {
		String parentWinHandle = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandle();
		Set<String> totalopenwindow = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles();
		for (String handle : totalopenwindow) {
			if (!handle.equals(parentWinHandle)) {
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle);
				Thread.sleep(4000);

			}
		}
		// driver.close();
		// driver.switchTo().window(parentWinHandle);
	}

	public void CloseExploreWindow() throws InterruptedException {
		Report.LogInfo("INFO", "Close Proposal window method", "INFO");

		String parentWinHandle = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandle();
		Report.LogInfo("info", SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getTitle(), "INFO");
		
		Set<String> totalopenwindow = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles();
		java.util.Iterator<String> it=totalopenwindow.iterator();
		String CPQWindow=it.next();

		if (totalopenwindow.size() > 1) {
			for (String handle : totalopenwindow) {
				if (handle.equals(parentWinHandle)) {
					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle);
					Report.LogInfo("info", SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getTitle(), "INFO");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Tab title is '"+ SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getTitle()) ;

                  
				}
			}

			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().close();
			Report.LogInfo("INFO", "Explore tab is closed", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore tab is closed ") ;

			Thread.sleep(10000);
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(CPQWindow);
			Report.LogInfo("INFO", "Switched to CPQ Tab", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Switched to CPQ tab ") ;


		} else {
			Reporter.log("Something went wrong. Proposal has not be generated");
		}
	}
	
	public void CloseProposalwindow() throws InterruptedException {
		String parentWinHandle = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandle();
		Set<String> totalopenwindow = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().getWindowHandles();
		if (totalopenwindow.size() > 1) {
		for (String handle : totalopenwindow) {
		if (!handle.equals(parentWinHandle)) {
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(handle);

		 }
		}
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().close();
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().window(parentWinHandle);
		} else {
		Reporter.log("Something went wrong. Proposal has not be generated");
		}
		}
	
	public static String genRandomUptoThousand() 
	{
	
		Random ran = new Random();
		int RandomNumber = ran.nextInt(1000) + 100;
		
		String random = Integer.toString(RandomNumber);
		
		return random;
	}
	public static String genRandomUptoHundred() 
	{
	
		Random ran = new Random();
		int RandomNumber = ran.nextInt(50)+49;
		
		String random = Integer.toString(RandomNumber);
		
		return random;
	}
	
	public void compareText_InViewPPPconfigurationPage(String labelname,  String ExpectedText) throws InterruptedException {

		String text = null;
		WebElement element = null;

		try {
			Thread.sleep(1000);
			element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//div[div[label[contains(text(),'" + labelname + "')]]]//label)[2]"));
			String emptyele = element.getText().toString();

			if(emptyele!=null && emptyele.isEmpty()) {
				
				emptyele= "Null";
				
				if(emptyele.equalsIgnoreCase(ExpectedText)) {
					Report.LogInfo("INFO", " The Expected value for '"+ labelname +"' field is same as the Acutal value '"+text+"'", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, " The Expected value for '"+ labelname +"' field is same as the Acutal value '"+text+"'");
					
				}else {
					Report.LogInfo("INFO", "The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
				}
		
			}else 
			{   
				text = element.getText();
				if(text.equalsIgnoreCase(ExpectedText)) {					
					ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
				}
				else if(ExpectedText.contains(text)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
				}
				else
				{
					Report.LogInfo("INFO", "The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
		}
	}
	
	public void scrolltoend() throws InterruptedException, IOException {//Or Scroll Down
		((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		clickOnBankPage();
		waitForAjax();
		Actions action=new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
		action.keyDown(Keys.CONTROL).sendKeys(Keys.END).keyUp(Keys.CONTROL).perform();
	}



	public void scrollToTop() throws InterruptedException, IOException {
		
		
	try {	
		WebElement element = findWebElement("//ol[@class='breadcrumb']//a[text()='Home']");
		((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].scrollIntoView(false);",element); 
	}catch(StaleElementReferenceException e) {
		e.printStackTrace();
	}
	}
	
	public void scrolltoview(WebElement element) {

		((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	public void ClickCommon(String labelname) throws InterruptedException {
		WebElement element= null;

		try {
			//Thread.sleep(1000);
			//element = getwebelement(.getlocator("//locators/" + application + "/"+ xpath +"").replace("Value", labelname));
			element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//label[text()='"+labelname+"']//parent::div//div//input"));
			if(element==null)
			{
				Report.LogInfo("INFO", "Step:  '"+labelname+"' not found", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step:  '"+labelname+"' not found");
			}
			else {
				element.click();	
				Report.LogInfo("INFO", "Step: Clicked on '"+labelname+"' button", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Clicked on '"+labelname+"' button");
			}

		} catch (Exception e) {
			Report.LogInfo("INFO", "Step: Clicking on '"+labelname+"' button is unsuccessful", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,"Step: Clicking on '"+labelname+"' button is unsuccessful");
			e.printStackTrace();
		}
	}
	
	public void clickOnBankPage() {
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//body)[1]")).click();
	}

	public void javascriptexecutor(String locator) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		js.executeScript("arguments[0].scrollIntoView(true);", locator);
	}
	
	///// Added Siebel
	public void PickValue(String value)
	{

		WebElement el = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//td[text()='" + value + "']/parent::*"));
		Moveon(el);
		//scrollIntoView(el);
		SendkeyusingAction(Keys.ENTER);
	}
	
	 public void Moveon(WebElement el)
		{
			Actions action = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());

			action.moveToElement(el).build().perform();
		}
	 
	 public String GetTextFrom(WebElement el)
		{
			String actual = el.getText().toUpperCase().toString();
			// String actual1=el.getText().toUpperCase().toString();
			return actual;
		}
	 public void ScrolltoElement(String xpath) throws InterruptedException, IOException {
			WebElement element = findWebElement(xpath);
			((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].scrollIntoView();",(element));
		}
	 
	 public void addDropdownValues_commonMethodDivSpan(String labelname, String xpath, String expectedValueToAdd)
				throws InterruptedException, IOException {
			  boolean availability=false;
			  List<String> ls = new ArrayList<String>();
				  waitForAjax();
				  if(expectedValueToAdd.equalsIgnoreCase("null")) {
					  
					  ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under "+ labelname + " dropdown");
					  System.out.println(" No values selected under "+ labelname + " dropdown");
				  }else {
					  
					  SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
				  
					  //verify list of values inside dropdown
					  List<WebElement> listofvalues = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//span[@role='option']"));
									  
						for (WebElement valuetypes : listofvalues) {
									 ls.add(valuetypes.getText());
						}
						
						SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd);   		
						
						SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//div[label[text()='"+ labelname +"']]//span[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();   		

					  Thread.sleep(1000);
					  
					  String actualValue=SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//label[text()='"+ labelname +"']/following-sibling::div//span")).getText();
					  Report.LogInfo("INFO", labelname + " dropdown value selected as: "+ actualValue, "PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
					  
				  }
			  
		}
	
	 public void safeJavaScriptClick(WebElement element) throws Exception {
			try {
				if (element.isEnabled() && element.isDisplayed()) {
					Reporter.log("Clicking on element with using java script click");

					((JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()).executeScript("arguments[0].click();", element);
				} else {
					Reporter.log("Unable to click on element");
				}
			} catch (StaleElementReferenceException e) {
				Reporter.log("Element is not attached to the page document "+ e.getStackTrace());
			} catch (NoSuchElementException e) {
				Reporter.log("Element was not found in DOM "+ e.getStackTrace());
			} catch (Exception e) {
				Reporter.log("Unable to click on element "+ e.getStackTrace());
			}
		}
	 
		public void getUrl(String URL) 
		{
			
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().navigate().to(URL);			 
			
		}

		public String[] GetText(String locator) throws IOException
		{ 
			WebElement el=findWebElement(locator);
//			String text="Activation Start Confirmation [New]";

			String text=el.getText().toString();
			if (text.contains("[New]["))
			{
				String[] text2=text.split(" \\[New\\]\\[");
				String[] text3=text2[1].split("\\]");
				text2[1]=text3[0];
				Report.LogInfo("Read","New Task name is "+text2[0]+" is read Successfully", "INFO");
				return text2;
			}
			else
			{
				String[] text2=text.split(" \\[");
				Report.LogInfo("Read","New Task name is "+text2[0]+" is read Successfully", "INFO");
				return text2;
			}
			
//			String[] text2=text;
		}

		public void SendKeys(WebElement el,String value) 
		{
			el.sendKeys(value);
		}

		public List<WebElement> GetWebElements(final String locator) throws InterruptedException
		{
			List<WebElement> el = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(locator));
			return el;
		}
		
		 private static boolean isProcessRunning(String processName) throws IOException, InterruptedException
		    {
		        ProcessBuilder processBuilder = new ProcessBuilder("tasklist.exe");
		        Process process = processBuilder.start();
		        String tasksList = toString(process.getInputStream());

		        return tasksList.contains(processName);
		    }

		    // http://stackoverflow.com/a/5445161/3764804
		    private static String toString(InputStream inputStream)
		    {
		        Scanner scanner = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
		        String string = scanner.hasNext() ? scanner.next() : "";
		        scanner.close();

		        return string;
		    }		   

		    public static String getTimeInHhMmSs(long time)
		    {
		    	int hrs = (int) TimeUnit.MILLISECONDS.toHours(time) % 24;
		    	int min = (int) TimeUnit.MILLISECONDS.toMinutes(time) % 60;
		    	int sec = (int) TimeUnit.MILLISECONDS.toSeconds(time) % 60;
		    	return String.format("%02d:%02d:%02d", hrs, min, sec);
		    }
		    
		    public static String getTimeInDateFormat(long time)
		    {
		    	Date date = new Date(time);
		        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		        String convDate = formatter.format(date);
		        System.out.println(convDate); 
		    	return convDate;
		    }
		  
		    public void Waittilljquesryupdated1() throws InterruptedException, SocketTimeoutException {

		    	// JavascriptExecutor js = null;
		    	boolean Status = false;
		    	Thread.sleep(500);
		    	JavascriptExecutor js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		    	for (int i=1; i<10; i++) {
		    	if (js == null) {
		    	Thread.sleep(150);
		    	js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		    	continue;
		    	} else {
		    	try {
		    	while(!(js.executeScript("return document.readyState").equals("complete")))
		    	{
		    	// System.out.println("dom state is" +(js.executeScript("return document.readyState")));
		    	// Thread.sleep(150);
		    	}
		    	Status = true;
		    	if (Status = true) { Thread.sleep(250); break; }
		    	} catch (Exception e) {
		    	continue;
		    	}
		    	}
		    	}
		    	}


		    	public void waitforPagetobeenable() throws InterruptedException
		    	{
		    	boolean Status = false;
		    	Thread.sleep(500);
		    	for (int j=0; j < 10; j++) {
		    	try {
		    	WebDriver driver = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		    	try {
		    	for(int i=0;i<=20;i++) {
		    	while(driver.findElement(By.xpath("//body[@class='loading-indicator']")).isDisplayed()) {
		    	// Thread.sleep(150);
		    	Status = true;
		    	}
		    	}
		    	} catch(Exception e) {
		    	// Thread.sleep(100);
		    	Waittilljquesryupdated1();
		    	}
		    	} catch (SocketTimeoutException e1) {
		    	continue;
		    	}
		    	if (Status = true) { break; }
		    	}


		    	// Thread.sleep(300);
		    	}



		    	public void Waittilljquesryupdated() throws InterruptedException, SocketTimeoutException {

		    	boolean Status = false;
		    	Thread.sleep(500);
		    	JavascriptExecutor js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		    	for (int i=1; i<10; i++) {
		    	if (js == null) {
		    	Thread.sleep(150);
		    	js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		    	continue;
		    	} else {
		    	try {
		    	while(!(js.executeScript("return document.readyState").equals("complete")))
		    	{
		    	// System.out.println("dom state is" +(js.executeScript("return document.readyState")));
		    	// Thread.sleep(150);
		    	}
		    	Status = true;
		    	if (Status = true) { Thread.sleep(250); break; }
		    	} catch (Exception e) {
		    	continue;
		    	}
		    	}
		    	}
		    	}

		public void addDropdownValues_commonMethod1(String labelname, String xpath, String expectedValueToAdd) throws InterruptedException {
		boolean availability = false;
		List<String> ls = new ArrayList<String>();

		try {

			// availability=getwebelementNoWait(xml.getlocator("//locators/" +
			// application + "/"+ xpath +"")).isDisplayed();
			if (isVisible(xpath)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				if (expectedValueToAdd.equalsIgnoreCase("null")) {
					Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {
					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[label[text()='" + labelname + "']]//div[text()='×']"))
							.click();
					// Clickon(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//div[text()='×']"));

					// verify list of values inside dropdown
					List<WebElement> listofvalues = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()
							.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

					for (WebElement valuetypes : listofvalues) {
						// Log.info("List of values : " + valuetypes.getText());
						ls.add(valuetypes.getText());
					}
					
					// ExtentTestManager.getTest().log(LogStatus.PASS, "list of
					// values inside "+labelname+" dropdown is: "+ls);
					// System.out.println("list of values inside "+labelname+"
					// dropdown is: "+ls);
					//Thread.sleep(5000);
					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[label[text()='" + labelname + "']]//input"))
							.sendKeys(expectedValueToAdd);
					// SendKeys(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//input"), expectedValueToAdd);

					SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//div[label[text()='" + labelname + "']]//div[contains(text(),'"
							+ expectedValueToAdd + "')])[1]")).click();
					
				}
			} else {
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
				// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + "
				// is not displaying");
				// System.out.println(labelname + " is not displaying");
			}
		} catch (NoSuchElementException e) {
			// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is
			// not displaying");
			System.out.println(labelname + " is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" NOt able to perform selection under " + labelname + " dropdown");
			System.out.println(" NO value selected under " + labelname + " dropdown");
		}
	}
		
		public void compareText_attributeID(String labelname, String xpath, String ExpectedText) throws InterruptedException {
			String text = null;
			WebElement element = null;

			try {
			// element = getwebelement(xml.getlocator("//locators/" +
			// application + "/" + xpath + ""));
			element = findWebElement(xpath);
			String emptyele = getAttributeFrom(xpath, "id");
			// String emptyele = getwebelement(xml.getlocator("//locators/" +
			// application + "/" + xpath + ""))
			// .getAttribute("value");
			if (element == null) {
			Report.LogInfo("INFO", "Step: '" + labelname + "' not found", "FAIL");
			// ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: '" +
			// labelname + "' not found");
			} else if (emptyele != null && emptyele.isEmpty()) {
			Report.LogInfo("INFO", "Step : '" + labelname + "' value is empty", "PASS");
			// ExtentTestManager.getTest().log(LogStatus.PASS, "Step : '" +
			// labelname + "' value is empty");
			} 
			else {

			text = element.getText();
			if (text.contains("-")) {

			String[] actualTextValue = text.split(" ");
			String[] expectedValue = ExpectedText.split(" ");

			if (expectedValue[0].equalsIgnoreCase(actualTextValue[0])) {
			Report.LogInfo("INFO", " The Expected value for '" + labelname + "' field '" + ExpectedText
			+ "' is same as the Acutal value '" + text + "'", "PASS");
			} else if (expectedValue[0].contains(actualTextValue[0])) {
			Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
			+ "' is same as the Acutal value '" + text + "'", "PASS");
			} else {
			Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
			+ "' is not same as the Acutal value '" + text + "'", "FAIL");
			}
			} else {
			if (ExpectedText.equalsIgnoreCase(text)) {
			Report.LogInfo("INFO", " The Expected value for '" + labelname + "' field '" + ExpectedText
			+ "' is same as the Acutal value '" + text + "'", "PASS");
			} else if (ExpectedText.contains(text)) {
			Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
			+ "' is same as the Acutal value '" + text + "'", "PASS");
			} else {
			Report.LogInfo("INFO", "The Expected value for '" + labelname + "' field '" + ExpectedText
			+ "' is not same as the Acutal value '" + text + "'", "FAIL");
			}
			}
			}
			} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");



			}
			}
		   
		// Created for cockpit
		public void verifyCockpitColumns(String expectedColumns, String settingsMenu, String columnsDisplayed,
				String selectedColumns, String object) throws InterruptedException, IOException {
			
			List<String> expectedTempColumns = Arrays.asList(expectedColumns.split(",", -1));
			List<String> expectedColumnsList = new ArrayList<>();
			for (String s : expectedTempColumns) {
				expectedColumnsList.add(s.trim());
			}

			try {
				waitForElementToAppear(settingsMenu, 15);
				click(settingsMenu, object + " Settings Menu");
				click(columnsDisplayed, object + " Columns Displayed button");
				waitForElementToAppear(selectedColumns, 15);
				WebElement selectedColumnsPath = findWebElement(selectedColumns);
				Select select = new Select(selectedColumnsPath);
				List<WebElement> selectedColumnsList = select.getOptions();
				List<String> listOfColumns = new ArrayList<>();
				/*
				 * selectedColumnsList.stream() .map((WebElement w)
				 * ->w.getText().trim()).collect(Collectors.toList());
				 */
				for (WebElement we : selectedColumnsList) {
					listOfColumns.add(we.getText().trim());

				}
				ExtentTestManager.getTest().log(LogStatus.INFO, "List of " + object + " column names: " + listOfColumns);

				if (expectedColumnsList.containsAll(listOfColumns)) {
					Report.LogInfo("Verify exists", object + " Column names are matching with expected column names",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							object + " Column names are matching with expected column names");
					Report.logToDashboard(object + " Column names are matching with expected column names");
				} else {
					Report.LogInfo("Verify exists", object + " Column names are not matching with expected column names",
							"FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							object + " Column names are not matching with expected column names");
					Report.logToDashboard(object + " Column names are not matching with expected column names");
				}
			} catch (Exception e) {
				Report.LogInfo("Verify exists", "Exception in " + object + " Column Names verification: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in " + object + " Column Names verification: " + e);
				Report.logToDashboard("Exception in " + object + " Column Names verification: " + e);
				Assert.assertTrue(false);
			}

		}

		public void verifyOrderStatus(String CockpitSection, String orderNumberLocator, String orderNumberField, String orderStatusLocator,
				String orderNumber,String settingsMenu, String newQuery, String orderStatusValue, String object) throws IOException {



				try {

				waitForElementToAppear(settingsMenu, 10);
				click(settingsMenu, "Service order settings menu");
				waitForElementToAppear(newQuery, 10);
				click(newQuery, "New Query");
				waitForElementToAppear(orderNumberLocator, 10);
				click(orderNumberLocator, "Service Order Ref");
				waitForElementToAppear(orderNumberField, 10);
				sendKeysByJS(orderNumberField, orderNumber);
				SendkeyusingAction(Keys.ENTER);
				waitForAjax();
				boolean isOrderAvailable=false;

				try{
				WebElement orderInfo= findWebElement(orderNumberLocator);
				isOrderAvailable= orderInfo.isDisplayed();
				}
				catch(Exception e){
				Report.LogInfo("Verify exists", "Order is not displayed", "FAIL");
				}
				if(isOrderAvailable)
				{
				waitForElementToAppear(orderStatusLocator, 10);
				WebElement orderStatus= findWebElement(orderStatusLocator);
				String orderStatusText= orderStatus.getText();
				if(orderStatusText.equalsIgnoreCase(orderStatusValue)){
				Report.LogInfo("Verify exists", object + " for order '"+orderNumber+ "' is displayed as: " +orderStatusValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, object + " for order '"+orderNumber+ "' is displayed as: " +orderStatusValue);
				Report.logToDashboard(object + " for order '"+orderNumber+ "' is displayed as: " +orderStatusValue);
				//Report.logToDashboard("Order '"+orderNumber+ "' with "+object + "as '"+orderStatusValue+ "' is displayed");

				}
				else{
				Report.LogInfo("Verify exists", "Expected value "+orderStatusValue+" is not same as Actual value "+orderStatusText, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Expected value "+orderStatusValue+" is not same as Actual value "+orderStatusText);
				Report.logToDashboard("Expected value "+orderStatusValue+" is not same as Actual value "+orderStatusText);
				}

				}
				else{
				Report.LogInfo("Verify exists", "'"+orderNumber+ "' order is not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+orderNumber+ "' order is not displayed");
				Report.logToDashboard("'"+orderNumber+ "' order is not displayed");
				}
				click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
				waitforPagetobeenable();

				//Commented code for searching order by clicking on Next arrow
				// List<WebElement> listOfOrders = findWebElements(orderNumbersLocator);
				//
				// WebElement we = findWebElement(nextRecordArrow);
				// // while()
				// boolean isMatched = false;
				// while (!isMatched) {
				// isMatched = identifyAndPrintId(nextRecordArrow, orderNumber, object, listOfOrders);
				//
				// if (!we.isEnabled()) {
				// break;
				// }
				// if (!isMatched) {
				// click(nextRecordArrow, "Next arrow");
				// listOfOrders.clear();
				// listOfOrders = findWebElements(orderNumbersLocator);
				// }
				// }
				} catch (Exception e) {
				Report.LogInfo("Verify exists", "Exception in " + object + " verification: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in " + object + " verification: " + e);
				Report.logToDashboard("Exception in " + object + " verification: " + e);
				Assert.assertTrue(false);
				}



				}

		//orderType means 'My Orders/My Team Orders'
		public String verifyRecordCount(String settingsMenu, String recordCountButton, String dialogbox, String object, String orderType) throws IOException, NoSuchElementException, InterruptedException{
			String recordCountValue = null;
			try{
			ScrollIntoViewByString(settingsMenu);
			javaScriptclick(settingsMenu, object + " Settings Menu");
			WebElement recordcountbutton= findWebElement(recordCountButton);
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, 15);
			if(recordcountbutton.isDisplayed())
			{
			click(recordCountButton, object + " Record Count button");
			waitForAjax();
			WebElement dialogBox= findWebElement(dialogbox);
			WebElement recordCount= findWebElement(SiebelCockpitObj.ServiceOrderCockpit.recordCount);
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.dialogbox, 15);
			if(dialogBox.isDisplayed())
			{
				recordCountValue= recordCount.getText();
				Report.LogInfo("Verify exists", "Open orders of "+orderType+" under " +object+ " Applet: "+recordCountValue,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Open orders of "+orderType+" under " +object+ " Applet: "+recordCountValue);
				Report.logToDashboard("Open orders of "+orderType+" under " +object+ " Applet: "+recordCountValue);
				click(SiebelCockpitObj.ServiceOrderCockpit.recordCount_OKbutton, "Ok button");
			
			}
			else{
				Report.LogInfo("Verify exists", "Record Count dialog window is not opened",
						"FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Record Count dialog window is not opened");
				Report.logToDashboard("Record Count dialog window is not opened");
			}
			
			click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
			WaitForAjax();
			waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader, 10);
		
			}
			else{
				Report.LogInfo("Verify exists", "No orders available under" +object+ " Applet",
						"FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No orders available under" +object+ " Applet");
				Report.logToDashboard("No orders available under" +object+ " Applet");
			}
			} catch (Exception e) {
							
				Report.LogInfo("Verify exists", "Exception in " + object + " Applet Record count verification: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in " + object + " Applet Record count verification: " + e);
				Report.logToDashboard("Exception in " + object + " Applet Record count verification: " + e);
				Assert.assertTrue(false);
				
			}
			return recordCountValue;
		}
		
	    
		public void verifyCPDICDDueOrderNumbers(String filterDropdown, String filterValue, String cpdColumnValue, 
				String orderNumbersLocator, String nextRecordArrow, String rowCounter) throws IOException{
			
			try{
			selectByVisibleText(filterDropdown, filterValue, filterValue+" dropdown value");
			waitForAjax();
				
			List<String> allOrderNumbers= new ArrayList<String>();
			List<WebElement> listOfOrders = findWebElements(orderNumbersLocator);
			List<String> orderNumbers;
			WebElement rowCounterValue= findWebElement(rowCounter);
									
				if (!listOfOrders.isEmpty()) {
		
				orderNumbers = identifyAndPrintOrderNumbers(nextRecordArrow, listOfOrders, cpdColumnValue);
				allOrderNumbers.addAll(orderNumbers);
				
				while(rowCounterValue.getText().contains("+")) {
					click(nextRecordArrow, "Next arrow");
					orderNumbers.clear();
					//listOfOrders = findWebElements(orderNumbersLocator);
					orderNumbers = identifyAndPrintOrderNumbers(nextRecordArrow, listOfOrders, cpdColumnValue);
					allOrderNumbers.addAll(orderNumbers);
					
				}
				Report.LogInfo("Verify exists", "Orders with "+filterValue+": "+allOrderNumbers,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Orders with "+filterValue+": "+allOrderNumbers);
				Report.logToDashboard("Orders with "+filterValue+": "+allOrderNumbers);
		
		}
				else{
					Report.LogInfo("Verify exists", "No orders to display with "+filterValue,
							"WARNING");
					ExtentTestManager.getTest().log(LogStatus.WARNING,
							"No orders to display with "+filterValue);
					Report.logToDashboard("No orders to display with "+filterValue);
				}
				
			} catch (Exception e) {
								
				Report.LogInfo("Verify exists", "Exception in fetching orders with " +filterValue+ ": " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in fetching orders with " +filterValue+ ": " + e);
				Report.logToDashboard("Exception in fetching orders with " +filterValue+ ": " + e);
				Assert.assertTrue(false);
				
			}
		}

		
		private List<String> identifyAndPrintOrderNumbers(String nextRecordArrow, List<WebElement> listOfOrders, String cpdColumnValue) throws IOException {
	
			List<String> orderNumbers = new ArrayList<>();
			try{
			
			int listOfOrdersCount = listOfOrders.size();
			List<WebElement> cpdValue=findWebElements(cpdColumnValue);
			LocalDate todayDate=LocalDate.now();
			waitForAjax();
			
			int totalDays= TotalDueDays(todayDate);
			for (int i = 0; i < listOfOrdersCount; i++) {
				String cpdText=cpdValue.get(i).getText();
				waitForAjax();
				LocalDate cpdDateValue=LocalDate.parse(cpdText, DateTimeFormatter.ofPattern("dd/M/yyyy"));
				LocalDate after10Days= todayDate.plusDays(totalDays);
				if((cpdDateValue.isAfter(todayDate)) && (cpdDateValue.isBefore(after10Days))){
					orderNumbers.add(listOfOrders.get(i).getText());
				}
			}
			
	} catch (Exception e) {
		
		Report.LogInfo("Verify exists", "Exception in fetching order numbers: " + e, "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,
				"Exception in fetching order numbers: " + e);
		Report.logToDashboard("Exception in fetching order numbers: " + e);
		Assert.assertTrue(false);
	}
			return orderNumbers;
		}

		public int TotalDueDays(LocalDate todayDate){
			int totalDays=0;
			try{
			//DateTimeFormatter.ofPattern(pattern)
			LocalDate Date= todayDate;
			
			
			while(totalDays<10)
			{
				
				if(!(Date.getDayOfWeek()== DayOfWeek.SATURDAY || Date.getDayOfWeek()== DayOfWeek.SUNDAY)){
					totalDays++;
				}
				Date= Date.plusDays(1);
			}
			
			} catch (Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Not able to fetch count of Due days");
				System.out.println("Not able to fetch count of Due days");
				
				Report.LogInfo("Verify exists", "Exception in fetching count of Due days: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in fetching count of Due days: " + e);
				Report.logToDashboard("Exception in fetching count of Due days: " + e);
				Assert.assertTrue(false);
			}
			return totalDays;
		}
		
		public void selectMyTeamOrdersOption() throws IOException, NoSuchElementException, InterruptedException{
			ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader);
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader, 10);
			selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Team Orders", "Order Type");
			waitForAjax();
		}
		
		public void selectAllOrdersOption() throws IOException, NoSuchElementException, InterruptedException{
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"Advanced Search");
		click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "Advanced Search");
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch_allOrders, 15);
		selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch_allOrders, "All Orders", "Orders dropdown");
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.okButton);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.okButton, 10);
		click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "Ok button");
		waitForAjax();
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 15);
}
		
		public void verifyJeopardyColumns(String destColLocator, String orderNumber, String destColValue, String object) throws IOException 
		{



			try {
			//waitForElementToAppear(destColLocator.replace("imagename", destColValue), 15);
			//WebElement value= findWebElement(destColLocator.replace("imagename", destColValue));
			//String orderStatusText= value.getAttribute("alt");
			//Report.LogInfo("Verify exists", orderStatusText,"INFO");
			if(destColValue.contains(","))
			{
				String destValue[] = destColValue.split("\\,");				
				for(int i=0;i<destValue.length;i++)
				{
					WebElement value= findWebElement(destColLocator.replace("imagename", destValue[i]));
					String orderStatusText= value.getAttribute("alt");
					Report.LogInfo("Verify exists", orderStatusText,"INFO");
					if(orderStatusText.contains(destValue[i])){
						Report.LogInfo("Verify exists", object + orderStatusText + " value is verified Successfully", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, object + orderStatusText + " value is verified Successfully");
						Report.logToDashboard(object + orderStatusText + " value is verified Successfully");
						break;
					}
					else
					{
					Report.LogInfo("Verify exists", object + orderStatusText + " value is not matching with the expected value", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, object + orderStatusText + "value is not matching with the expected value");
					Report.logToDashboard(object + orderStatusText + " value is not matching with the expected value");
					}
				}
			}
			else
			{
				WebElement value= findWebElement(destColLocator.replace("imagename", destColValue));
				String orderStatusText= value.getAttribute("alt");
				Report.LogInfo("Verify exists", orderStatusText,"INFO");
				if(orderStatusText.contains(destColValue)){
				Report.LogInfo("Verify exists", object + orderStatusText + " value is verified Successfully", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, object + orderStatusText + " value is verified Successfully");
				Report.logToDashboard(object + orderStatusText + " value is verified Successfully");
				}
				else
				{
				Report.LogInfo("Verify exists", object + orderStatusText + " value is not matching with the expected value", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, object + orderStatusText + "value is not matching with the expected value");
				Report.logToDashboard(object + orderStatusText + " value is not matching with the expected value");
				}
				
			}			

			} catch (Exception e) {
			Report.LogInfo("Verify exists", "Exception in " + object + " verification: " + e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in " + object + " verification: " + e);
			Report.logToDashboard("Exception in " + object + " verification: " + e);
			Assert.assertTrue(false);
			}

		}		
		
		public int TotalDays(LocalDate todayDate, LocalDate cpd){
			int totalDays=0;
			try{
			//DateTimeFormatter.ofPattern(pattern)
			LocalDate Date= todayDate;
			
			
			while(Date.isBefore(cpd))
			{
//				if(!(Date.getDayOfWeek()== DayOfWeek.SATURDAY || Date.getDayOfWeek()== DayOfWeek.SUNDAY)){
//				}
				
				totalDays++;
				Date= Date.plusDays(1);
			}
			while(Date.isAfter(cpd))
			{	
				totalDays--;
				Date= Date.minusDays(1);
			}
			
			} catch (Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Not able to fetch count of Due days");
				System.out.println("Not able to fetch count of Due days");
				
				Report.LogInfo("Verify exists", "Exception in fetching count of Due days: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in fetching count of Due days: " + e);
				Report.logToDashboard("Exception in fetching count of Due days: " + e);
				Assert.assertTrue(false);
			}
			return totalDays;
		}
		
		public void selectDropdownValueByExpandingArrow_commonMethod(String labelname, String dropdownArrow, String expectedValueToAdd)
				throws InterruptedException {
				waitForAjax();
				List<String> ls = new ArrayList<String>();



				try {



				if (isVisible(dropdownArrow)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				if (expectedValueToAdd.equalsIgnoreCase("null")) {
				Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {
				findWebElement(dropdownArrow).click();
				waitForAjax();
				// verify list of values inside dropdown
				List<WebElement> listofvalues = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()
				.findElements(By.xpath("//ul[@role='combobox']/li/div"));



				for (WebElement valuetypes : listofvalues) {
				// Log.info("List of values : " + valuetypes.getText());
				ls.add(valuetypes.getText());
				}
				waitForAjax();



				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//input[@aria-label='" + labelname + "']"))
				.sendKeys(expectedValueToAdd);
				waitForAjax();
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//ul[@role='combobox']/li/div[contains(text(),'"+expectedValueToAdd + "')])[1]")).click();

				Report.LogInfo("Verify exists", labelname + " dropdown value is selected as: " +expectedValueToAdd, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value is selected as: " +expectedValueToAdd);
				Report.logToDashboard(labelname + " dropdown value is selected as: " +expectedValueToAdd);


				}
				} else {
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " is not displaying");
				Report.logToDashboard(labelname + " is not displaying");

				}
				} catch (NoSuchElementException e) {

				System.out.println(labelname + " is not displaying");
				} catch (Exception ee) {
				ee.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
				" NOt able to perform selection under " + labelname + " dropdown");
				System.out.println(" NO value selected under " + labelname + " dropdown");
				}
			}
		
		public void verifyActivitiesFilterOptions(String filterDropdown, String filterValue, String activityTypeValue,
				String orderNumberLocator, String orderNumberField, String orderNumber, String settingsMenu, String newQuery, String statusDropdownArrow) throws IOException{

				try{
				selectByVisibleText(filterDropdown, filterValue, filterValue+" dropdown value");
				waitForAjax();

				waitForElementToAppear(settingsMenu, 10);
				click(settingsMenu, "Service order settings menu");
				waitForElementToAppear(newQuery, 10);
				click(newQuery, "New Query");
				waitForElementToAppear(orderNumberLocator, 10);
				click(orderNumberLocator, "Service Order Ref");
				waitForElementToAppear(orderNumberField, 10);
				sendKeysByJS(orderNumberField, orderNumber);
				SendkeyusingAction(Keys.ENTER);
				waitForAjax();
				boolean isOrderAvailable=false;

				try{
				WebElement orderInfo= findWebElement(orderNumberLocator);
				isOrderAvailable= orderInfo.isDisplayed();
				}
				catch(Exception e){
				Report.LogInfo("Verify exists", "Order is not displayed", "FAIL");
				}
				if(isOrderAvailable)
				{
				click(activityTypeValue, "Activity Type link");
				waitforPagetobeenable();
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.activitiesTab, 10);
				selectDropdownValueByExpandingArrow_commonMethod("Status", statusDropdownArrow, "Done");
				waitForAjax();
				Actions action=new Actions(webDriver);
				action.keyDown(Keys.CONTROL).sendKeys("S").keyUp(Keys.CONTROL).build().perform();
				WebDriverWait w = new WebDriverWait(webDriver, 5);
				//alertIsPresent() condition applied
				if(w.until(ExpectedConditions.alertIsPresent())!=null)
				{
				webDriver.switchTo().alert().accept();
				}

				click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
				waitforPagetobeenable();
				ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader);
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader, 10);

				waitForElementToAppear(settingsMenu, 10);
				click(settingsMenu, "Service order settings menu");
				waitForElementToAppear(newQuery, 10);
				click(newQuery, "New Query");
				waitForElementToAppear(orderNumberLocator, 10);
				click(orderNumberLocator, "Service Order Ref");
				waitForElementToAppear(orderNumberField, 10);
				sendKeysByJS(orderNumberField, orderNumber);
				SendkeyusingAction(Keys.ENTER);
				waitForAjax();
				boolean isOrderDisplayed= false;
				try{
				WebElement orderInfo= findWebElement(orderNumberLocator);
				isOrderDisplayed= orderInfo.isDisplayed();
				}
				catch(Exception e){
				Report.LogInfo("Verify exists", "'"+orderNumber+ "' order is not displayed as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "'"+orderNumber+ "' order is not displayed as expected");
				Report.logToDashboard("'"+orderNumber+ "' order is not displayed as expected");
				}
				if(isOrderDisplayed)
				{
				Report.LogInfo("Verify exists", "'"+orderNumber+ "' order is not displayed as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "'"+orderNumber+ "' order is not displayed as expected");
				Report.logToDashboard("'"+orderNumber+ "' order is not displayed as expected");
				}
				else{
				Report.LogInfo("Verify exists", "'"+orderNumber+ "' order is still displaying under Activities applet", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+orderNumber+ "' order is still displaying under Activities applet");
				Report.logToDashboard("'"+orderNumber+ "' order is still displaying under Activities applet");
				}
				}
				else{
				Report.LogInfo("Verify exists", "'"+orderNumber+ "' order is not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+orderNumber+ "' order is not displayed");
				Report.logToDashboard("'"+orderNumber+ "' order is not displayed");
				}
				click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
				waitforPagetobeenable();

				} catch (Exception e) {

				Report.LogInfo("Verify exists", "Exception in fetching orders with " +filterValue+ ": " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
				"Exception in fetching orders with " +filterValue+ ": " + e);
				Report.logToDashboard("Exception in fetching orders with " +filterValue+ ": " + e);
				Assert.assertTrue(false);

				}
				}
		
		public void openChromeBrowser(String browser,String url, String username, String password) {
			try {
										
					String driverPath = g.getRelativePath()+"\\Resources\\chromedriver.exe";
					System.setProperty("webdriver.chrome.driver",driverPath);
		
					Map<String,Object> preferences= new HashMap<>();
					preferences.put("profile.default_content_settings.popups", 0);
					preferences.put("download.prompt_for_download", "false");
					preferences.put("download.default_directory", g.getRelativePath()+"\\TestData\\Downloads");
		
					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", preferences);
		
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					//capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					//capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					capabilities.setBrowserName("chrome");
					capabilities.setPlatform(Platform.WINDOWS);

					g.setHubAddress("http://localhost:5555/wd/hub");
					
					Thread.sleep(50000);
					
					RemoteWebDriver remoDriver= new RemoteWebDriver(new URL(g.getHubAddress()), capabilities);
					remoDriver.setFileDetector(new LocalFileDetector());
					webDriver = remoDriver;	

					webDriver.get(url);
					ExtentTestManager.getTest().log(LogStatus.PASS, "Launched application: " + url);
					Report.logToDashboard("Launched application: " + url);
					Report.LogInfowebDriver("Application Launch", "Launched application: " + url, "PASS",webDriver);
					
					waitForSiebelLoader(webDriver);
					if (webDriver.findElement((By.xpath("//*[@id='details-button']"))).isDisplayed());
					{
						verifyExistswebDriver(SiebelLoginObj.Login.Advanced,"Advanced button",webDriver);
						clickwebDriver(SiebelLoginObj.Login.Advanced,"Advanced button",webDriver);
					
					verifyExistswebDriver(SiebelLoginObj.Login.Navigate,"Navigate button",webDriver);
					clickwebDriver(SiebelLoginObj.Login.Navigate,"Navigate button",webDriver);
					}
					waitForElementToAppearwebDriver(SiebelLoginObj.Login.userNameTxb, 5,webDriver);
					verifyExistswebDriver(SiebelLoginObj.Login.userNameTxb,"User ID textbox",webDriver);
					sendKeys(SiebelLoginObj.Login.userNameTxb, username,"User ID textbox field");
				
					verifyExistswebDriver(SiebelLoginObj.Login.passWordTxb,"Password textbox",webDriver);
					sendKeys(SiebelLoginObj.Login.passWordTxb, password,"Password textbox field");
					verifyExistswebDriver(SiebelLoginObj.Login.loginBtn,"Login button",webDriver);
					clickwebDriver(SiebelLoginObj.Login.loginBtn,"Login Button",webDriver);
					WaitForAjaxwebDriver(webDriver);

			} catch (Exception e) {
				//System.out.println("Could not obtain webdriver for browser \n"	+ e.getMessage());
				Report.LogInfo("Browser", "Could not obtain webdriver for browser \n"+e.getMessage(), "FAIL");
				Report.logToDashboard("Could not obtain webdriver for browser \n"	+ e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Could not obtain webdriver for browser \n"	+ e.getMessage());
				Assert.assertTrue(false);
			}
//			return webDriver;			
			
	}
		public void SiebelLoginChrome(String url,String username, String password) throws InterruptedException, IOException
		{
//			webDriver = openChromeBrowser("chrome");
			webDriver.get(url);
			ExtentTestManager.getTest().log(LogStatus.PASS, "Launched application: " + url);
			Report.logToDashboard("Launched application: " + url);
			Report.LogInfo("Application Launch", "Launched application: " + url, "PASS");
			
			//waitForSiebelLoader(webDriver);
			if (webDriver.findElement((By.xpath("//*[@id='details-button']"))).isDisplayed());
			{
			verifyExistswebDriver(SiebelLoginObj.Login.Advanced,"Advanced button",webDriver);
			clickwebDriver(SiebelLoginObj.Login.Advanced,"Advanced button",webDriver);
			
			verifyExistswebDriver(SiebelLoginObj.Login.Navigate,"Navigate button",webDriver);
			clickwebDriver(SiebelLoginObj.Login.Navigate,"Navigate button",webDriver);
			}
			waitForElementToAppearwebDriver(SiebelLoginObj.Login.userNameTxb, 5,webDriver);
			verifyExistswebDriver(SiebelLoginObj.Login.userNameTxb,"User ID textbox",webDriver);
			sendKeys(SiebelLoginObj.Login.userNameTxb, username,"User ID textbox field");
		
			verifyExistswebDriver(SiebelLoginObj.Login.passWordTxb,"Password textbox",webDriver);
			sendKeys(SiebelLoginObj.Login.passWordTxb, password,"Password textbox field");
			verifyExistswebDriver(SiebelLoginObj.Login.loginBtn,"Login button",webDriver);
			clickwebDriver(SiebelLoginObj.Login.loginBtn,"Login Button",webDriver);
			WaitForAjaxwebDriver(webDriver);
			
		}
		
		public boolean verifyExistswebDriver(String locator,String Object, WebDriver driver ) throws InterruptedException, IOException {
			if(g.getBrowser().equalsIgnoreCase("edge") || g.getBrowser().equalsIgnoreCase("firefox"))
			{
				waitForElementToAppear(locator,5);
			}
			if(!isBlankOrNull(locator)){
				try {
					waitForElementToAppearwebDriver(locator,15,driver);
					WebElement element=findWebElementwebDriver(locator,webDriver);
					WebDriverWait wait = new WebDriverWait(driver, 30);
					//wait.until(ExpectedConditions.visibilityOf(element));
					//wait.until(ExpectedConditions.elementToBeClickable(element));
					if(element != null)
					{
						Report.LogInfowebDriver("verifyExists","<b><i>"+Object +"<i></b> is present on screen", "PASS",webDriver);
						ExtentTestManager.getTest().log(LogStatus.PASS, Object +" is present on screen");
						Report.logToDashboard(Object +" is present on screen");
						return true;
					}
					else
					{
						Report.LogInfowebDriver("verifyExists","<b><i>"+Object +"</i></b> is not present on screen", "INFO",webDriver);
						ExtentTestManager.getTest().log(LogStatus.INFO, Object +" is not present on screen");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
						Report.logToDashboard(Object +" is not present on screen");
						return false;
					}				
				} catch (Exception e) {

					setImplicitWaitTimeout(20000);
					//Report.LogInfo("getTextFrom","<b><i>'"+Object +"'</i></b> is not present on Screen", "FAIL");
					Report.LogInfo("verifyExists","<b><i>"+Object +"</i></b> is not present on screen", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, Object +" is not present on screen");
					Report.logToDashboard(Object +" is not present on screen");
					return false;
				} 
			}else{

				Report.LogInfo("verifyExists","<b><i>"+Object +"</i></b> is not present on screen", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, Object +" is not present on screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Report.logToDashboard(Object +" Element not found");
				return false;
			}
		}
		
		public void clickwebDriver(String locator,String ObjectName,WebDriver driver) throws IOException 
		{
		 	WebElement element = null;
			try
			{
				
				for(int i=1; i<=2; i++)
				{
					element = findWebElementwebDriver(locator,webDriver);
					if(element != null)
					{	
						WebDriverWait wait = new WebDriverWait(driver, 30);
//						wait.until(ExpectedConditions.elementToBeClickable(element));
						if(g.getBrowser().trim().equalsIgnoreCase("EDGE"))
						{
							JavascriptExecutor js = (JavascriptExecutor) driver;
							((JavascriptExecutor)driver).executeScript("arguments[0].click();",  element);
							Report.LogInfo("click","<b><i>"+ObjectName +"</i></b> Is Clicked Successfully", "INFO");	
							ExtentTestManager.getTest().log(LogStatus.INFO, ObjectName +" Is Clicked Successfully");
							Report.logToDashboard(ObjectName +" Is Clicked Successfully");
						}
						else
						{
											
							try {
								element.click();
				    					    			
							} catch (Exception e1) {
							for (int j = 0; j <= 5; j++) {
								try {
										Thread.sleep(1000);
										waitForElementToBeVisiblewebDriver(locator, 5,driver);
										scrollIntoViewwebDriver(element,driver);
										Thread.sleep(250);
										element.click();
						    			break;
								} catch (Exception e) {
									sException = e.toString();
									j = i+1;
									continue;
								}	
							}
						}					
							Report.LogInfowebDriver("click","<b><i>"+ObjectName +"</i></b> Is Clicked Successfully", "INFO",webDriver);
							ExtentTestManager.getTest().log(LogStatus.INFO, ObjectName +" Is Clicked Successfully");
							Report.logToDashboard(ObjectName +" Is Clicked Successfully");
						}
					
						break;
					}else
					{
						Thread.sleep(2500);
					}				
				}
							
				if(element == null)
				{	
					Report.LogInfo("click", ObjectName +" not present on Screen", "FAIL");
					Report.logToDashboard(ObjectName +" not present on Screen");
					ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" not present on Screen");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					Assert.assertTrue(false);
				}

			}catch(Exception e)
			{
				Report.LogInfo("click","<font color=red"+ObjectName +" Is not Present on Screen</font>", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, ObjectName +" Is not Present on Screen");
				Report.logToDashboard(ObjectName +" Is not Present on Screen");
				Assert.assertTrue(false);
			}
		}
		
		public void waitForElementToAppearwebDriver(String locator,int waitTimeInSeconds, WebDriver driver) throws InterruptedException, NoSuchElementException, IOException
		{
			WebElement element=null;		
			
			long sleepTime = (waitTimeInSeconds * 1000)/10;
			
			try
			{
			for(int i=1; i<=10; i++)
			{
				element = findWebElementwebDriver(locator,webDriver);
				if(element != null)
				{	
					//WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds);
					//wait.until(ExpectedConditions.visibilityOf(element));
					break;
				}else
				{
					Thread.sleep(sleepTime);
				}				
			}
						
			if(element == null)
			{	
				Report.LogInfowebDriver("waitForElementToAppear", locator +" not present on Screen", "FAIL",webDriver);
				Report.logToDashboard(locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" not present on Screen");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Assert.assertTrue(false);
			}
			}catch(Exception e)
			{
				Report.LogInfowebDriver("click","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL",webDriver);
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
				Report.logToDashboard(locator +" Is not Present on Screen");
				Assert.assertTrue(false);
			}
			
		}
		public void WaitForAjaxwebDriver(WebDriver driver) throws IOException {

		    try {
		        WebDriverWait driverWait = new WebDriverWait(driver, 5);

		        ExpectedCondition<Boolean> expectation;   
		        expectation = new ExpectedCondition<Boolean>() {

		            public Boolean apply(WebDriver driverjs) {

		                JavascriptExecutor js = (JavascriptExecutor) driverjs;
		                return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
		            }
		        };
		        driverWait.until(expectation);
		    }       
		    catch (TimeoutException exTimeout) {

		       // fail code	    	 
		    }
		    catch (WebDriverException exWebDriverException) {

		       // fail code
		    }
		    return;
		}
		
		public void waitForSiebelLoader(WebDriver driver) throws InterruptedException, IOException
		{
		int i = 1;
		Thread.sleep(1000);
		WebElement el = driver.findElement(By.xpath("//html"));
		try {
		while (el.getAttribute("class").contains("siebui-busy")){
		if (i > 300) { break; }
		Thread.sleep(2000);
		i = i+1;
		}
		} catch(Exception e) {
//		Waittilljquesryupdated();
		}
		}
		public String CurrentDate()
		{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return (formatter.format(date));
		}
		
		public boolean waitForElementToBeVisiblewebDriver(String locator, int timeOut,WebDriver driver) throws IOException, InterruptedException {
	    	boolean status = false;
	    	
	    	WebElement webElement=findWebElementwebDriver(locator,webDriver);
	    		    	
	    	for(int i=1; i<=2; i++)
	    	{
	    		webElement = findWebElementwebDriver(locator,webDriver);
	    		if(webElement != null)
	    		{	
	    			try {							
						FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)    		     
		    				.withTimeout(timeOut, TimeUnit.SECONDS)
		    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
		    		        .ignoring(NoSuchElementException.class);  
		    				fluentWait.until(ExpectedConditions.elementToBeClickable(webElement));
		    				status = true;
	    				} catch (Exception e) {
	    					e.printStackTrace();
	    				}
	    			break;
	    		}else
	    		{
	    			Thread.sleep(2500);
	    		}				
	    	}
	    				
	    	if(webElement == null)
	    	{	
	    		status = false;
	    	}
			
			return status;
		}
		
		public static boolean scrollIntoViewwebDriver(WebElement webElement,WebDriver driver) throws IOException {
			 boolean status = false;
		    	
			 try {
				 	String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
	                        + "var elementTop = arguments[0].getBoundingClientRect().top;"
	                        + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
				 	((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, webElement);
				 	isElementEnabled(webElement);
				 	Thread.sleep(250);
				 	status = true;
				 } catch (Exception e) {
					 e.printStackTrace();
					 status = false;				
				 }
				
			 return status;
		 }
		
		protected WebElement findWebElementwebDriver(String locator,WebDriver driver) throws IOException{

			WebElement webElement = null;
			if(!isBlankOrNull(locator)){
				try {

					//Report.LogInfo("findWebElement","Web element '"+locator+ "' is finding", "INFO");
					if(locator.startsWith("@id")){ // e.g @id='elementID'
						// Find the text input element by its id
						webElement = driver.findElement(By.id(removeStart(locator, "@id=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}else if(locator.startsWith("@name")){
						// Find the text input element by its name
						webElement = driver.findElement(By.name(removeStart(locator, "@name=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}else if(locator.startsWith("@linkText")){
						// Find the text input element by its link text
						webElement = driver.findElement(By.linkText(removeStart(locator, "@linkText=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}else if(locator.startsWith("@partialLinkText")){
						// Find the text input element by its link text
						webElement =driver.findElement(By.partialLinkText(removeStart(locator, "@partialLinkText=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}else if(locator.startsWith("@xpath")){
						//using XPATH locator.
						webElement = driver.findElement(By.xpath(removeStart(locator, "@xpath="))); 
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}else if(locator.startsWith("@css")){
						// Find the text input element by its css locator
						webElement = driver.findElement(By.cssSelector(removeStart(locator, "@css=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}else if(locator.startsWith("@className")){
						// Find the text input element by its class Name
						webElement = driver.findElement(By.className(removeStart(locator, "@className=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}
					else if(locator.startsWith("@tagName")){
						// Find the text input element by its class Name
						webElement = driver.findElement(By.tagName(removeStart(locator, "@tagName=")));
						((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid green'", webElement);
					}

				} catch(NoSuchElementException e) { 
					e.printStackTrace();
				} catch(RuntimeException e) { 
					e.printStackTrace();
					Report.LogInfo("Exception", "Runtime exception: "+e, "FAIL");
					Report.logToDashboard("Runtime exception: "+e);
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Runtime exception: "+e);
					Assert.assertTrue(false);
				}
			}
			
			return webElement;
		}
		public void MoveToElementwebDriver(String locator,WebDriver driver) throws IOException {
			
			try
			{
				WebElement element = findWebElementwebDriver(locator,webDriver);
				Actions actions = new Actions(driver);
				actions.moveToElement(element);
				actions.perform();
			}catch(Exception e)
			{
				Report.LogInfowebDriver("MoveToElement","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL",webDriver);
				ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
				Report.logToDashboard(locator +" Is not Present on Screen");
				Assert.assertTrue(false);
			}
		 }
}
