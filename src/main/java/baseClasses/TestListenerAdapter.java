package baseClasses;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.*;

import com.relevantcodes.extentreports.LogStatus;

public class TestListenerAdapter extends SeleniumUtils implements IInvokedMethodListener, ITestListener{
	
	List<ITestNGMethod> passedtests = new ArrayList<ITestNGMethod>();
	List<ITestNGMethod> failedtests = new ArrayList<ITestNGMethod>();
	int executedcases = 0;
		
	private static String getTestMethodName(ITestResult iTestResult) {
				        return iTestResult.getMethod().getConstructorOrMethod().getName();
		    }
		   
		    //Before starting all tests, below method runs.
		    public void onStart(ITestContext iTestContext) {
		    	 
		    	 try {
		    		 
		    		if(executedcases<1)
		    		{
		    		 
		    			InputStream input1 = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		    			InputStream input2 = new FileInputStream(g.getRelativePath()+"/PortalDBConfig.properties");

		    			// load a properties file
		    			prop.load(input1); 
		    			prop.load(input2);

		    			System.setProperty("logback.configurationFile",g.getRelativePath()+"/logback.xml");
		    			NBORDERID = prop.getProperty("BotId");
		    			host = prop.getProperty("Host");
		    			port = prop.getProperty("Port");
		        			        		 			
		    			Class.forName("com.mysql.jdbc.Driver"); 
		    			con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/colt", "root", "admin");
		    			st = con.createStatement();		    		 
		    		 
		    			TotalCases = iTestContext.getSuite().getAllMethods().size();
		    			st.execute("UPDATE req_summary SET Total="+TotalCases+" WHERE req_id="+ NBORDERID+";");
		    		}
			    	} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				}
		    }
		 
		    //After completing all tests, below method runs.
		    public void onFinish(ITestContext iTestContext) {
			
		        ExtentTestManager.endTest();
		        ExtentManager.getReporter().flush();
		        
		        try {
		    	   	int overallPass = passedtests.size();
			        int overallFail = failedtests.size();
			        executedcases = overallPass + overallFail;
			        
			        if(TotalCases == executedcases)
			        {
			        	 long elapsedTimeMs = iTestContext.getEndDate().getTime() - iTestContext.getStartDate().getTime();
					        String elapsedTime = getTimeInHhMmSs(elapsedTimeMs);
					        
							st.execute("UPDATE req_summary SET Pass="+overallPass+",Total="+executedcases+",Fail="+overallFail+",req_status='completed',Elapsed='"+elapsedTime+"' WHERE req_id="+ NBORDERID+";");
			        }			       
					
		        } catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   			
		      
		    }
		 
		  public void onTestStart(ITestResult iTestResult) 
		  {
			  String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");

			  try {
//				 String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
				st.execute("UPDATE tc_execution set STATUS=\"Inprogress\" WHERE req_id="+ NBORDERID + " AND tc_name='"+ScenarioName+"';");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		    	
		        DateFormat df = new SimpleDateFormat("yyyyMMdd-HHmm");
		        String Filename = iTestResult.getName()+ "-" + df.format(new Date());
		        String ScenarioName1 = iTestResult.getTestContext().getCurrentXmlTest().getName();

		        ExtentTestManager.startTest(ScenarioName1,"");
		    }
		 
		    public void onTestSuccess(ITestResult iTestResult) 
		    {
		    	passedtests.add(iTestResult.getMethod());
	    		
	    		String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
			
		    	try {
		    		st.execute("UPDATE tc_execution set STATUS='PASS' WHERE req_id="+ NBORDERID + " AND tc_name='"+ScenarioName+"';");
		     						
					long endTimeMs = iTestResult.getEndMillis();
					String endTime = getTimeInDateFormat(endTimeMs);
			    	long startTimeMs = iTestResult.getStartMillis();
			    	long elapseTimeMs = endTimeMs - startTimeMs;
			    	String elapsedTime = getTimeInHhMmSs(elapseTimeMs);
			    	 
			    	st.execute("UPDATE tc_execution set End_Time='"+endTime+"',Elapsed='"+elapsedTime+"' WHERE req_id="+NBORDERID+" AND tc_name='"+ScenarioName+"';");
		    	
		    	} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 		    	
		    	
		    	String ScenarioName1 = iTestResult.getTestContext().getCurrentXmlTest().getName();
		    	
		    	ExtentTestManager.getTest().log(LogStatus.INFO, ScenarioName1+" : Scenario has been completed");
		    	ExtentTestManager.endTest();
		    	ExtentManager.getReporter().flush();
		    }
		 
		    public void onTestFailure(ITestResult iTestResult) {
		    	
		    	failedtests.add(iTestResult.getMethod());
				String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
		       //	NBORDERID = String.valueOf(g.getBotId());

					try {
						st.execute("UPDATE tc_execution set STATUS='FAIL' WHERE req_id="+ NBORDERID + " AND tc_name='"+ScenarioName+"';");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				long endTimeMs = iTestResult.getEndMillis();
				String endTime = getTimeInDateFormat(endTimeMs);
				long startTimeMs = iTestResult.getStartMillis();
				long elapseTimeMs = endTimeMs - startTimeMs;
				String elapsedTime = getTimeInHhMmSs(elapseTimeMs);
				 
			    	try {
						st.execute("UPDATE tc_execution set End_Time='"+endTime+"',Elapsed='"+elapsedTime+"' WHERE req_id="+NBORDERID+" AND tc_name='"+ScenarioName+"';");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
				String ScenarioName1 = iTestResult.getTestContext().getCurrentXmlTest().getName();
				String Err_Msg = iTestResult.getThrowable().getMessage();
				System.out.println("Scenario " +  ScenarioName1 + " got failed, with the exception "+Err_Msg);
				
				try {
					String screenShot =SeleniumUtils.Capturefullscreenshot();
					if(screenShot != null)
					{
						ExtentTestManager.getTest().log(LogStatus.FAIL,ScenarioName1 +" : with the exception "+Err_Msg+ExtentTestManager.getTest().addBase64ScreenShot(screenShot));
					}else
					{
						ExtentTestManager.getTest().log(LogStatus.FAIL,ScenarioName1 +" : with the exception "+Err_Msg);
					}
					ExtentTestManager.endTest();
					ExtentManager.getReporter().flush();
				} catch (IOException e) {
					ExtentTestManager.endTest();
					ExtentManager.getReporter().flush();
				} 	    	
		    }
		 
		    public void onTestError(ITestResult iTestResult) {
				
		    	try {
		    		String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
		    		
					st.execute("UPDATE tc_execution set STATUS='FAIL' WHERE req_id="+ NBORDERID + " AND tc_name='"+ScenarioName+"';");
				
					long endTimeMs = iTestResult.getEndMillis();
			    	String endTime = getTimeInHhMmSs(endTimeMs);
			    	long startTimeMs = iTestResult.getStartMillis();
			    	long elapseTimeMs = endTimeMs - startTimeMs;
			    	String elapsedTime = getTimeInHhMmSs(elapseTimeMs);
			    	 
			    	st.execute("UPDATE tc_execution set End_Time='"+endTime+"',Elapsed='"+elapsedTime+"' WHERE req_id="+NBORDERID+" AND tc_name='"+ScenarioName+"';");
		    	
		    	} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
		    
		    	String ScenarioName1 = iTestResult.getTestContext().getCurrentXmlTest().getName();
		    	System.out.println("Error while Executing the scenario " +  ScenarioName1 + " ,Please verify");
		    	String Err_Msg = iTestResult.getThrowable().getMessage();
		       
		    	ExtentTestManager.getTest().log(LogStatus.ERROR,ScenarioName1 +" : with the exception "+Err_Msg);
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().close();
				ExtentTestManager.endTest();
				ExtentManager.getReporter().flush();
		    }

		    public void onTestSkipped(ITestResult iTestResult) {
					    	
		    	//String ScenarioName = iTestResult.getMethod().getMethodName();
		    	//String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
		    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getName();								

//		    	System.out.println("Scenario "+  ScenarioName + " got Skipped from execution");
		        ExtentTestManager.getTest().log(LogStatus.SKIP, "Scenario "+  ScenarioName + " got Skipped from execution");
		        ExtentTestManager.endTest();
		        ExtentManager.getReporter().flush();
		        
		    }
		 
		    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
					    	
		    	//String ScenarioName = iTestResult.getMethod().getMethodName();
		    	//String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getParameter("ScenarioName");
		    	String ScenarioName = iTestResult.getTestContext().getCurrentXmlTest().getName();
		    	System.out.println("Scenario " +ScenarioName+" failed but it is in defined success ratio ");
		    	ExtentTestManager.getTest().log(LogStatus.INFO, "Scenario "+ScenarioName+" failed but it is in defined success ratio ");
		        ExtentTestManager.endTest();
		        ExtentManager.getReporter().flush();
		    }

			@Override
			public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
				// TODO Auto-generated method stub
				
			}
}
