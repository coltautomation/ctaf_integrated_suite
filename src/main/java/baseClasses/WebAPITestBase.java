package baseClasses;

import static io.restassured.RestAssured.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;
import org.skyscreamer.jsonassert.ValueMatcher;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.skyscreamer.jsonassert.comparator.JSONComparator;
import static org.skyscreamer.jsonassert.JSONCompare.compareJSON;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.yaml.snakeyaml.Yaml;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

public class WebAPITestBase extends MainDriver{
	
	protected HashMap<String,String> testData=new HashMap<String,String>();
	XmlPath path;
	public static Response response;
	protected static GlobalVariables g = new GlobalVariables();
	public Connection con = null;
	public static Statement st = null;
	public static RestHighLevelClient client = null;
	private static Utilities suiteConfig = new Utilities();
	public static String NBORDERID;
	public String host;
	public String port;
	public String elasticIp;
	public int elasticPort;
	public static String elasticIndex;
	public static int TotalCases;
	public String AutomationSuite;
	protected static String timestamp=null;
	Properties prop = new Properties();
	Properties prop1 = new Properties();
	static Properties prop2 = new Properties();
	
	public static String keyFound;
	public static JSONObject jsonSource;
	public static JSONObject bodyObject;
	public static JSONObject bodyObject1;
	public static JSONObject bodyObject2;
	public static JSONObject bodyObject3;
	public static JSONObject bodyObject4;
	public static JSONObject bodyObject5;
	public static int nameCnt;
	public static int txnCounter;
	public static String compareKey1;
	public static String compareKey2;
	public static String compareKey3;
	public static String compareKey4;
	public static String compareKey5;
	public static HashMap<String, String> colValues = new HashMap<String, String>();
	public static HashMap<String, ArrayList<HashMap<String, String>>> testDataSet;
	
	@SuppressWarnings("unchecked")
	public static HashMap<String, ArrayList<HashMap<String, String>>> setInfomationForTestData(String fileName) 
	{

		HashMap<String, ArrayList<HashMap<String, String>>> basicInfoDataset=null;
		try
		{
			 LowMemoryExcelFileReader quickExcelReader2 = new LowMemoryExcelFileReader(fileName);
		     OPCPackage pkg2 = quickExcelReader2.opcPackage();
		     XSSFReader reader2 = quickExcelReader2.xssfReader(pkg2);
		     basicInfoDataset = quickExcelReader2.readStreamFullXL(reader2);			

		}catch(Exception e)
		{
			
		}
		return basicInfoDataset;
	}

	
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() throws Exception
	{
		String path = new File(".").getCanonicalPath();
		g.setRelativePath(path);
		
		InputStream inputA = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		 prop.load(inputA); 
		System.setProperty("logback.configurationFile",g.getRelativePath()+"/logback.xml");
		AutomationSuite = prop.getProperty("AuomationSuite");
		if(AutomationSuite.equals("NH B2B Rest API"))
		{
			g.setBrowser("IE");
		}else{
		
		g.setBrowser(suiteConfig.getValue("Browsers", "firefox").trim().split(",")[0]);

		}
		
		String testDataXL = prop.getProperty("TestDataSheet");
		File path2 = new File("./TestData/"+testDataXL);
	    String testDataFile = path2.toString();
	     
		//Return an Array List of HashMap with All values stored from Config_Sheet
	    testDataSet = setInfomationForTestData(testDataFile);

		g.setGridMode(suiteConfig.getValue("GridMode", "off").trim());
		g.setTestSuitePath(path+"//Resources//TestSuite.xlsx");
		
		timestamp = "RegressionSuite_"+suiteConfig.getCurrentDatenTime("MM-dd-yyyy")+"_"+suiteConfig.getCurrentDatenTime("hh-mm-ss_a");

		String resultPath = path+"//CTAFResults//"+timestamp;
		String ScreenshotsPath = resultPath+"//Screenshots";

		g.setResultFolderPath(resultPath);
		g.setTestSuitePath(path+"//Resources//TestSuite.xls");

		new File(resultPath).mkdirs();
		new File(ScreenshotsPath).mkdirs();

		String SummaryReportfile = resultPath+"\\Index.html";

		Report.createSummaryReportHeader(SummaryReportfile);
		

		try{
			InputStream input1 = new FileInputStream(g.getRelativePath()+"/configuration.properties");
			InputStream input2 = new FileInputStream(g.getRelativePath()+"/PortalDBConfig.properties");

            // load a properties file
            prop.load(input1); 
            prop.load(input2);

        } catch (IOException ex) {
            ex.printStackTrace();
        }			
		System.setProperty("logback.configurationFile",g.getRelativePath()+"/logback.xml");
		NBORDERID = prop.getProperty("BotId");
		elasticIp = prop.getProperty("elasticIp");
		elasticPort = Integer.parseInt(prop.getProperty("elasticPort"));
		elasticIndex = prop.getProperty("elasticIndex");
				
		client = new RestHighLevelClient(RestClient.builder(new HttpHost(elasticIp, elasticPort, "http")));
}

	@Parameters({ "ScenarioName" })
	@BeforeMethod()
	public void setUp(String ScenarioName) throws Exception
	{
			
		//file.setEmailFlag(false);
		//String Automessage=email.getExecutionStartMessage();
		//email.sendMessageWithAttachment("CTAF Automation Regression Test Run Started",Automessage,"",false);
		
	}

	@AfterSuite(alwaysRun = true)
	public void afterSuite() throws Exception
	{
		/**************************Zip the Output generated HTML Files********************************/
		ZipDirectory zip = new ZipDirectory();
		File directoryToZip = new File(g.getResultFolderPath());
		List<File> fileList = new ArrayList<File>();
		System.out.println("---Getting references to all files in: " + directoryToZip.getCanonicalPath());
		zip.getAllFiles(directoryToZip, fileList);
		System.out.println("---Creating zip file");

		String zipOut=g.getResultFolderPath() +"//"+directoryToZip.getName() + ".zip";
		System.out.println(zipOut);
		zip.writeZipFile(directoryToZip, fileList,zipOut);
		System.out.println("---Done");

		//text.setEmailFlag(true);

		//file.setEmailFlag(true);
		zipOut=g.getResultFolderPath() +"//"+directoryToZip.getName() + ".zip";
		Report.createSummaryReportFooter();
		client.close();
		
		InputStream inputB = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		prop1.load(inputB); 
		
		if(isProcessRunning("cmd.exe"))
        {
			String currentPIDConfig = prop1.getProperty("CmdPIDs");
			
			if(currentPIDConfig != null)
			{
				String[] cmdPIDArray = currentPIDConfig.split(",");
				for(int k=0; k<cmdPIDArray.length; k++)
				{
					String cmd = "taskkill /F /PID " + cmdPIDArray[k];
					Runtime.getRuntime().exec(cmd);				
				}			
			}
        }		
		
		/**************************Send the mail with zip output HTML Files********************************/
		//email.sendMessageWithAttachment("CTAF Automated Regression Test Run Completed",file.readEntireFile(g.getResultFolderPath() +"//Index.html"),zipOut,true );
	}
	
	public Boolean verifyStatusCode(Response response, int statusCode){
		Boolean bResults;
		ExtentTestManager.getTest().log(LogStatus.INFO,"Response status - "+ response.getStatusLine());
		Report.LogInfo("Verify Status Code","Response status - "+ response.getStatusLine(), "INFO");
		if (response.getStatusCode() == statusCode){
			ExtentTestManager.getTest().log(LogStatus.PASS,"Actual Response Code matches the Expected Response Code.");
			Report.LogInfo("Verify Status Code","Actual Response Code matches the Expected Response Code.", "PASS");
			Report.logToDashboard("Actual Response Code matches the Expected Response Code.");
			bResults = true;
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL,"Expected code "+statusCode+" but returned "+ response.getStatusCode());
			Report.LogInfo("Verify Status Code","Expected code "+statusCode+" but returned "+ response.getStatusCode(), "FAIL");
			Report.logToDashboard("Expected code "+statusCode+" but returned "+ response.getStatusCode());
			bResults = false;
			Assert.assertTrue(false);
		}
		return bResults;
	}	
	
	public void compareActualWithExpected(Response response, String ExpectedStatus)
	{
		String ActualStatus;
		String[] TransArray;
		String TransactionId;
		String actualRes = response.getBody().asString();
		String prettyResponse = response.getBody().asPrettyString();
		
		if(response.getBody().asString() != null)
		{
			//path = new XmlPath(actualRes);
			path = new XmlPath(prettyResponse);
			
			ActualStatus = path.getString("status"); 
			TransArray = ActualStatus.split(ExpectedStatus.trim());
			if(TransArray.length>1)
			{
				TransactionId = TransArray[1];
			
				JSONObject json = XML.toJSONObject(actualRes);   
				String jsonActual = json.toString(4);  
						
				if(ActualStatus.contains(ExpectedStatus))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS,"Response Body is: "+ jsonActual);
					Report.LogInfo("compareActualWithExpected","Response Body is: "+ jsonActual, "PASS");
					Report.logToDashboard("Response Body is: "+ jsonActual);
					//ExtentTestManager.getTest().log(LogStatus.PASS,"Transaction Id is: "+TransactionId);'
				}else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL,"<br />Response:<br />"+ jsonActual);
					Report.LogInfo("compareActualWithExpected","<br />Response:<br />"+ jsonActual, "FAIL");
					Report.logToDashboard("Response Body is: "+ jsonActual);
					Assert.assertTrue(false);
				}
			}else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL,"Failed<br />Actual Response:<br />"+ actualRes);
				Report.LogInfo("compareActualWithExpected","Failed<br />Actual Response:<br />"+ actualRes, "FAIL");
				Report.logToDashboard("Response Body is: "+ actualRes);
				Assert.assertTrue(false);
			}
		}else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL,"Failed<br />Actual Response:<br />"+ actualRes);
			Report.LogInfo("compareActualWithExpected","Failed<br />Actual Response:<br />"+ actualRes, "FAIL");
			Report.logToDashboard("Response Body is: "+ actualRes);
			Assert.assertTrue(false);
		}
			
	}
	
	public Response get(String userName, String password, String endPoint, String inputDataType, String inputData, String dataLog){
		try
		{
			ExtentTestManager.getTest().log(LogStatus.INFO,"GET request on the API EndPoint : "+ endPoint);	
			ExtentTestManager.getTest().log(LogStatus.INFO,"Request Data : "+ dataLog);
			Report.LogInfo("GET Request","GET request on the API EndPoint : "+ endPoint, "INFO");
			Report.LogInfo("GET Request","Request Data : "+ dataLog, "INFO");
			Report.logToDashboard("GET request on the API EndPoint : "+ endPoint);
			Report.logToDashboard("Request Data : "+ dataLog);
			
			switch (inputDataType) {
			case "None":
				  response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).get(endPoint).andReturn();
				break;
			case "URLEncoded":
				endPoint= endPoint+inputData;
				response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).get(endPoint).andReturn();
				break;
			case "Payload":
				  response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).body(inputData).get(endPoint).andReturn();
				break;
			default: 
				
				break;
			   }	
			return response;
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in get request: "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in get request: "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in get request: "+e.getMessage());
			Assert.assertTrue(false);
		} 	
		return response;
		
	}
			
	public Response post(String userName, String password, String endPoint, String payLoad, String dataLog) throws IOException
	{
		try
		{
		ExtentTestManager.getTest().log(LogStatus.INFO,"POST request on the API EndPoint : "+ endPoint);
		ExtentTestManager.getTest().log(LogStatus.INFO,"Request Data : "+ dataLog);
		Report.LogInfo("POST Request","POST request on the API EndPoint : "+ endPoint, "INFO");
		Report.LogInfo("POST Request","Request Data : "+ dataLog, "INFO");
		Report.logToDashboard("POST request on the API EndPoint : "+ endPoint);
		Report.logToDashboard("Request Data : "+ dataLog);

		response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).body(payLoad).when().post(endPoint);
		//System.out.println(response.getBody().asString());
		return response;
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in post request: "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in post request: "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in post request: "+e.getMessage());
			Assert.assertTrue(false);
		} 
		return response;
	}	
	
	public Response delete(String userName, String password, String endPoint, String inputDataType, String inputData, String dataLog)
	{
		ExtentTestManager.getTest().log(LogStatus.INFO,"DELETE request on the API EndPoint : "+ endPoint);
		ExtentTestManager.getTest().log(LogStatus.INFO,"Request Data : "+ dataLog);
		Report.LogInfo("DELETE Request","DELETE request on the API EndPoint : "+ endPoint, "INFO");
		Report.LogInfo("DELETE Request","Request Data : "+ dataLog, "INFO");
		Report.logToDashboard("POST request on the API EndPoint : "+ endPoint);
		Report.logToDashboard("Request Data : "+ dataLog);
		
		switch (inputDataType) {
			    case "None":
				   response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).delete(endPoint).andReturn();
				     break;
			    case "URLEncoded":
				      endPoint= endPoint+inputData;
				      response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).delete(endPoint).andReturn();
				     break;
			    case "Payload":
				      response = given().urlEncodingEnabled(false).request().auth().basic(userName, password).body(inputData).delete(endPoint).andReturn();
				     break;
			    default: 
				  
				    break;
			       }  
		return response;
	}
	
	private static boolean isProcessRunning(String processName) throws IOException, InterruptedException
    {
        ProcessBuilder processBuilder = new ProcessBuilder("tasklist.exe");
        Process process = processBuilder.start();
        String tasksList = toString(process.getInputStream());

        return tasksList.contains(processName);
    }

    // http://stackoverflow.com/a/5445161/3764804
    private static String toString(InputStream inputStream)
    {
        Scanner scanner = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
        String string = scanner.hasNext() ? scanner.next() : "";
        scanner.close();

        return string;
    }
    
 	 
    protected String convertJSONToXML(JSONObject json, String originalXML) {
    	
    	String xmlString = null;    	
    	
    	try {   
    			String xmlString1 = XML.toString(json);
    			String replaceStr1 = "<xmlns:soapenv>http://schemas.xmlsoap.org/soap/envelope/</xmlns:soapenv><xmlns:v1>http://www.colt.net/numberHosting/v1</xmlns:v1>";
    			String xmlString2 = xmlString1.replace(replaceStr1, "");	
    			String replaceStr2 = "<soapenv:Header/>";
    			String xmlString3 = xmlString2.replace(replaceStr2, "");
    			String replaceStr3 = "<soapenv:Envelope><xmlns:v11>http://www.colt.net/xml/ns/NumberHosting/v1.0</xmlns:v11><xmlns:v12>http://www.colt.net/xml/ns/cbe/nhm/v1.0</xmlns:v12>";
	        	String newString1 = "<soapenv:Envelope xmlns:soapenv="+'"'+"http://schemas.xmlsoap.org/soap/envelope/"+'"'+" xmlns:v1="+'"'+"http://www.colt.net/numberHosting/v1"+'"'+" xmlns:v11="+'"'+"http://www.colt.net/xml/ns/NumberHosting/v1.0"+'"'+" xmlns:v12="+'"'+"http://www.colt.net/xml/ns/cbe/nhm/v1.0"+'"'+"><soapenv:Header/>";  
	        	String xmlString4 = xmlString3.replace(replaceStr3, newString1);
	        	
	        	xmlString = xmlString4;
	        	
		} catch (Exception e) {
			
				Report.LogInfo("Exception", "Test data exception, please verify the input data and XML.", "FAIL");
				Report.logToDashboard("Test data exception, please verify the input data and XML.");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Test data exception, please verify the input data and XML.");
				e.printStackTrace();
				Assert.assertTrue(false);
		}
		
		return xmlString;
	}
    
    public SOAPMessage getSoapMessage(String xmlData, boolean isToIgnoringComments) throws Exception {
      
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(xmlData.getBytes());
        MimeHeaders mimeHeaders = new MimeHeaders();
        mimeHeaders.addHeader("Content-Type", "text/xml; charset=UTF-8;");
        SOAPMessage message = MessageFactory.newInstance(SOAPConstants.DEFAULT_SOAP_PROTOCOL).createMessage(mimeHeaders, byteArrayInputStream);
        return message;
    }    
  
    public static JSONObject updateXMLValue(JSONObject jsonSource, String nodeString, String nodeValue) 
    {
    	Iterator<String> key;
    	keyFound = "false";
    	nameCnt = 0;
    	
    	String [] nodeName = nodeString.split("_");
    	int numOfNodes = nodeName.length;    	
    	
    	JSONObject innerObject = jsonSource.getJSONObject("soapenv:Envelope");
	    bodyObject = innerObject.getJSONObject("soapenv:Body");
     	    
	    key = bodyObject.keys();
        while (key.hasNext()) {
        	String keyName = key.next().toString();
     		//System.out.println(keyName);
     		
     		if (bodyObject.get(keyName) instanceof JSONObject) 
     		{ 
     			bodyObject = bodyObject.getJSONObject(keyName);
     			
     			 bodyObject.keys().forEachRemaining(k ->
     		    {
     		    	 String keyStr = k.toString();
     			     
     			     //Print key and value
     			        //System.out.println("************************************");
     			        //System.out.println("key: "+ keyStr);
     			        //System.out.println("************************************");
     			      
     			        if(keyStr.contains(nodeName[nameCnt]))     			    	
            			{
     			        	if(nameCnt!=(numOfNodes-1))
     			        	{
     			        		bodyObject = bodyObject.getJSONObject(keyStr);
     			        		nameCnt++;
     			        		
     			        		 bodyObject.keys().forEachRemaining(j ->
     			     		    {
     			     		    	 String keyStr2 = j.toString();
     			     			     
     			     			     //Print key and value
     			     			       //System.out.println("************************************");
     			     			       //System.out.println("key: "+ keyStr2);
     			     			       //System.out.println("************************************");
     			     			      
     			     			        if(keyStr2.contains(nodeName[nameCnt]))     			    	
     			            			{
     			     			        	if(nameCnt!=(numOfNodes-1))
     			     			        	{
     			     			        		bodyObject = bodyObject.getJSONObject(keyStr2);
     			     			        		nameCnt++;
     			     			        		
     			     			        		 bodyObject.keys().forEachRemaining(i ->
     			     			     		    {
     			     			     		    	 String keyStr3 = i.toString();
     			     			     			     
     			     			     			     //Print key and value
     			     			     			      //System.out.println("************************************");
     			     			     			      //System.out.println("key: "+ keyStr3);
     			     			     			      //System.out.println("************************************");
     			     			     			      
     			     			     			        if(keyStr3.contains(nodeName[nameCnt]))     			    	
     			     			            			{   
     			     			     			        	if(nameCnt!=(numOfNodes-1))
     			     			     			        	{
     			     			     			        		bodyObject = bodyObject.getJSONObject(keyStr3);
     			     			     			        		nameCnt++;
     		     			     			        		
     			     			     			        		bodyObject.keys().forEachRemaining(h ->
     			     			     			        		{
     			     			     			        			String keyStr4 = h.toString();
     	     			     			     			     
     			     			     			        			//Print key and value
     			     			     			        			//System.out.println("************************************");
     			     			     			        			//System.out.println("key: "+ keyStr4);
     			     			     			        			//System.out.println("************************************");
     	     			     			     			      
     			     			     			        			if(keyStr4.contains(nodeName[nameCnt]))     			    	
     			     			     			        			{  
     			     			     			        				bodyObject.put(keyStr4, nodeValue);            				
     			     			     			        				keyFound = "true";     			     			     			        	     			     			     			        	
     			     			     			        			}     			     			     			        			
     			     			     			        		});
     			     			     			        	}else
         			     			            			{
         			     			     			        	bodyObject.put(keyStr3, nodeValue);            				
         			     			     			        	keyFound = "true";     			     			     			        	     			     			     			        	
         			     			            			}  		     			     			        
     			     			            			} 	
     			     			       		    });
     			     			        		
     			     			        	}else
     			     			        	{
     			     			        		bodyObject.put(keyStr2, nodeValue);            				
     			     			        		keyFound = "true";     			     			        		
     			     			        	}
     			            			} 			     		    	
     			     		    	
     			     		    });
     			        		
     			        	}else
     			        	{
     			        		bodyObject.put(keyStr, nodeValue);            				
     			        		keyFound = "true";     			        		
     			        	}
            			}
     		    }); 
     			
     			 if(keyFound.equalsIgnoreCase("true"))
     			 {
     				break; 
     			 }     			
     		}else
     		{
     			break;
     		}
     		
     		key = bodyObject.keys();	           
        }	    
	    
    	return jsonSource;    	
    }
    
    public static JSONObject updateXMLValueBulkUpload(JSONObject jsonSrc) 
    {
    	jsonSource = jsonSrc;
    	Iterator<String> key;
    	txnCounter = 0; 	
    	compareKey1 = ""; compareKey2 = ""; compareKey3 = ""; compareKey4 = ""; compareKey5 = "";
    	
    	JSONObject innerObject = jsonSource.getJSONObject("soapenv:Envelope");
    	JSONObject bObject = innerObject.getJSONObject("soapenv:Body");
    	bodyObject = bObject.getJSONObject("v1:bulkNhTransactions");
    	//bodyObject = bulkNHObject.getJSONObject("nhTransactionsRequest");	    
	    //bodyObject = nhTransObject.getJSONObject("v11:nhTransactionsRequest");
     	    
	    key = bodyObject.keys();
        while (key.hasNext()) {
        	String keyName = key.next().toString();
     		//System.out.println(keyName);
     		
     		if (bodyObject.get(keyName) instanceof JSONObject) 
     		{ 
     			bodyObject = bodyObject.getJSONObject(keyName);
     			
     			 bodyObject.keys().forEachRemaining(k ->
     		    {
     		    	 String keyStr = k.toString();
     			     
     			     //Print key and value
     			        //System.out.println("************************************");
     			       // System.out.println("key: "+ keyStr);
     			       // System.out.println("************************************");
     			       
     			    if (bodyObject.get(keyStr) instanceof JSONObject) 
     		     	{ 
     		     		bodyObject1 = bodyObject.getJSONObject(keyStr);
     		     		
     		     		 bodyObject1.keys().forEachRemaining(l ->
     	     		    {
     	     		    	 String keyStr1 = l.toString();
     	     			     
     	     			     //Print key and value
     	     			        //System.out.println("************************************");
     	     			        //System.out.println("key: "+ keyStr1);
     	     			        //System.out.println("************************************");
     		     			 	
     	     			        if(keyStr1.contains("transactions"))
     	     			        {
     	     			        	if (bodyObject1.get(keyStr1) instanceof JSONObject) 
     	     	     		     	{ 
     	     			        		txnCounter++;
     	     			        		compareKey1 = txnCounter+"."+keyStr1.split(":")[1];
     	     			        		bodyObject2 = bodyObject1.getJSONObject(keyStr1) ;
     	     			        		jsonSource = updateJSONObject(jsonSource, keyStr1);
     	     	     		     	}else
     	     	     		     	{
     	     	     		     	
     	     	     		     		JSONArray txnArray = new JSONArray(bodyObject1.getJSONArray(keyStr1).toString());
     	     	     		     		jsonSource = updateJSONArray(jsonSource, txnArray, keyStr1);
     	     	     		     	}     	     			        	
     	     						
     	     			        }     		  		    	 		
     		    	 	    		    	 	
     	     			        compareKey1 = ""; 
     	     			        bodyObject1.remove(keyStr);
     	     		    });
     		     	}
     		    });    			
     				
     		}else
     		{
     			break;
     		}
     		
     		key = bodyObject.keys();	           
        }	    
	    
    	return jsonSource;    	
    }
    
    public static Document getBody(String fromText) {
        Document result = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            result = builder.parse(new ByteArrayInputStream(fromText.getBytes()));
           
            NodeList list = result.getElementsByTagNameNS("soapenv", "Body");
            if(list.getLength() > 0) {
                Node body = list.item(0);
                result = builder.newDocument();
                result.adoptNode(body);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public void setColumnValues(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
	{		
    	List<String> xmlColNames;
    	String colNameString = "";
    	String tempString;
		try {
			xmlColNames = fngetcolNames(dataSheet);
			
			 for(String colName : xmlColNames)
		 	    {
		 	    	String NodeValue = fngetcolvalue(dataSheet, scriptNo, dataSetNo, colName);
		 	    	
		 	       	if(!NodeValue.equalsIgnoreCase(""))
		 	       	{
		 	       		if(!colName.equalsIgnoreCase("senderSystem"))
		 	       		{
		 	       			String[] colNameArr = colName.split("_");
		 	       			for(int i=1;i<colNameArr.length;i++)
		 	       			{
		 	       				if(i==1)
		 	       				{
		 	       					tempString = colNameArr[i];
		 	       				}else
		 	       				{
		 	       					tempString = "_" + colNameArr[i];
		 	       				}
		 	       				colNameString = colNameString + tempString;
		 	       			}
		 	    	
		 	       			colValues.put(colNameString, NodeValue);
		 	       			colNameString = "";
		 	       		}else
		 	       		{
		 	       			colValues.put(colName, NodeValue);
		 	       		}
		 	       			
		 	       	}
		 	    
		 	    }
			 
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	       
 	   
	}
    
    public static JSONObject updateJSONArray(JSONObject jsonSource, JSONArray txnArray, String keyStr1)
   	{
    	for(int x=0; x<txnArray.length(); x++)
			{
				txnCounter++;
				if(!txnArray.isNull(x))
				{
					//System.out.println(txnArray.getJSONObject(x).toString());     	     								
					compareKey1 = txnCounter+"."+keyStr1.split(":")[1];     	     								
					bodyObject2 = txnArray.getJSONObject(x);
					
					bodyObject2.keys().forEachRemaining(j ->
	        		{
	        			String keyStr2 = j.toString();
			     
	        			//Print key and value
	        			//System.out.println("************************************");
	        			//System.out.println("key: "+ keyStr2);
	        			//System.out.println("************************************");
	        			
	        			    	 	
	        			compareKey2 = compareKey1 +"_"+ keyStr2.split(":")[1];
	        			if(colValues.containsKey(compareKey2))
	        			{
	        				bodyObject2.put(keyStr2, colValues.get(compareKey2));          				
		        		     
	        			}else
	        			{
	        				if (bodyObject2.get(keyStr2) instanceof JSONObject) 
	        				{ 
	        					bodyObject3 = bodyObject2.getJSONObject(keyStr2);
		        			
	        					bodyObject3.keys().forEachRemaining(i ->
	        					{
	        						String keyStr3 = i.toString();
		        			     
	        						//Print key and value
	        						//System.out.println("************************************");
		        			        //System.out.println("key: "+ keyStr3);
		        			        //System.out.println("************************************");
		        		    	 	
		        		    	  	compareKey3 = compareKey2 +"_"+ keyStr3.split(":")[1];
		        		    	 	if(colValues.containsKey(compareKey3))
		        		    	 	{
		        		    	 		bodyObject3.put(keyStr3, colValues.get(compareKey3));          				
		    			        		     
		        		    	 	}else
		        		    	 	{
		        		    	 		if (bodyObject3.get(keyStr3) instanceof JSONObject) 
		        		        		{ 
		        		        			bodyObject4 = bodyObject3.getJSONObject(keyStr3);
		        		        			
		        		        			 bodyObject4.keys().forEachRemaining(h ->
		        		        		    {
		        		        		    	 String keyStr4 = h.toString();
		        		        			     
		        		        			     //Print key and value
		        		        			       	//System.out.println("************************************");
		        		        			        //System.out.println("key: "+ keyStr4);
		        		        			        //System.out.println("************************************");
		        		        		    	 	
		        		        		    	  	compareKey4 = compareKey3 +"_"+ keyStr4.split(":")[1];
		        		        		    	 	if(colValues.containsKey(compareKey4))
		        		        		    	 	{
		        		        		    	 		bodyObject4.put(keyStr4, colValues.get(compareKey4));          				
		        		    			        		     
		        		        		    	 	}else
		        		        		    	 	{
		        		        		    	 		if (bodyObject4.get(keyStr4) instanceof JSONObject) 
		        		        		        		{ 
		        		        		        			bodyObject5 = bodyObject4.getJSONObject(keyStr4);
		        		        		        			
		        		        		        			 bodyObject5.keys().forEachRemaining(g ->
		        		        		        		    {
		        		        		        		    	 String keyStr5 = g.toString();
		        		        		        			     
		        		        		        			     //Print key and value
		        		        		        			        //System.out.println("************************************");
		        		        		        			       // System.out.println("key: "+ keyStr5);
		        		        		        			        //System.out.println("************************************");
		        		        		        		    	 	
		        		        		        		    	  	compareKey5 = compareKey4 +"_"+ keyStr5.split(":")[1];
		        		        		        		    	 	if(colValues.containsKey(compareKey5))
		        		        		        		    	 	{
		        		        		        		    	 		bodyObject5.put(keyStr5, colValues.get(compareKey5));          				
		        		        		    			        		     
		        		        		        		    	 	}  
		        		        		        		    	 	compareKey5 = "";
		        		        		        		    	 	bodyObject5.remove(keyStr4);
		        		        		        		    });      		        				
		        		        		        		}     	
		        		        		    	 	}    
		        		        		    	 	compareKey4 = "";
		        		        		    	 	bodyObject4.remove(keyStr3);
		        		        		    });      		        				
		        		        		}     	
		        		    	 	}    
		        		    	 	compareKey3 = "";
		        		    	 	bodyObject3.remove(keyStr2);
		        		    });      		        				
		        		}     		  
		    	 		
	        		}    
		    	 	compareKey2 = "";
		    	 	bodyObject2.remove(keyStr1);
	        		}); 
				}
				compareKey1 = "";
			}
    	return jsonSource;
    	
   	}
    
    public static JSONObject updateJSONObject(JSONObject jsonSrc, String keyStr1)
   	{
    	jsonSource = jsonSrc;
    	bodyObject2.keys().forEachRemaining(j ->
  		{
  			String keyStr2 = j.toString();
	     
  			//Print key and value
  			//System.out.println("************************************");
  			///System.out.println("key: "+ keyStr2);
  			//System.out.println("************************************");
  		 		 	 	
  			compareKey2 = compareKey1 +"_"+ keyStr2.split(":")[1];
  			if(colValues.containsKey(compareKey2))
  			{
  				bodyObject2.put(keyStr2, colValues.get(compareKey2));          				
     		     
  			}else
  			{
  				if (bodyObject2.get(keyStr2) instanceof JSONArray) 
  				{ 
  					JSONArray txnArray = new JSONArray(bodyObject2.getJSONArray(keyStr2).toString());
   		     		jsonSource = updateJSONArray2(jsonSource, txnArray, keyStr2);
  				}
  		}    
 	 	compareKey2 = "";
 	 	bodyObject2.remove(keyStr1);
  		}); 
    	
    	return jsonSource;
   	}
    
    public static JSONObject updateJSONArray2(JSONObject jsonSource, JSONArray txnArray, String keyStr2)
   	{
    	for(int x=0; x<txnArray.length(); x++)
			{
				txnCounter++;
				if(!txnArray.isNull(x))
				{
					System.out.println(txnArray.getJSONObject(x).toString());     	     								
						     								
					bodyObject3 = txnArray.getJSONObject(x);
					
					bodyObject3.keys().forEachRemaining(j ->
	        		{
	        			String keyStr3 = j.toString();
			     
	        			  
	        						//Print key and value
	        						//System.out.println("************************************");
		        			        //System.out.println("key: "+ keyStr3);
		        			        //System.out.println("************************************");
		        		    	 	
		        		    	  	compareKey3 = compareKey2 +"_"+ keyStr3.split(":")[1];
		        		    	 	if(colValues.containsKey(compareKey3))
		        		    	 	{
		        		    	 		bodyObject3.put(keyStr3, colValues.get(compareKey3));          				
		    			        		     
		        		    	 	}else
		        		    	 	{
		        		    	 		if (bodyObject3.get(keyStr3) instanceof JSONObject) 
		        		        		{ 
		        		        			bodyObject4 = bodyObject3.getJSONObject(keyStr3);
		        		        			
		        		        			 bodyObject4.keys().forEachRemaining(h ->
		        		        		    {
		        		        		    	 String keyStr4 = h.toString();
		        		        			     
		        		        			     //Print key and value
		        		        			       	//System.out.println("************************************");
		        		        			        //System.out.println("key: "+ keyStr4);
		        		        			        //System.out.println("************************************");
		        		        		    	 	
		        		        		    	  	compareKey4 = compareKey3 +"_"+ keyStr4.split(":")[1];
		        		        		    	 	if(colValues.containsKey(compareKey4))
		        		        		    	 	{
		        		        		    	 		bodyObject4.put(keyStr4, colValues.get(compareKey4));          				
		        		    			        		     
		        		        		    	 	}else
		        		        		    	 	{
		        		        		    	 		if (bodyObject4.get(keyStr4) instanceof JSONObject) 
		        		        		        		{ 
		        		        		        			bodyObject5 = bodyObject4.getJSONObject(keyStr4);
		        		        		        			
		        		        		        			 bodyObject5.keys().forEachRemaining(g ->
		        		        		        		    {
		        		        		        		    	 String keyStr5 = g.toString();
		        		        		        			     
		        		        		        			     //Print key and value
		        		        		        			        //System.out.println("************************************");
		        		        		        			        //System.out.println("key: "+ keyStr5);
		        		        		        			        //System.out.println("************************************");
		        		        		        		    	 	
		        		        		        		    	  	compareKey5 = compareKey4 +"_"+ keyStr5.split(":")[1];
		        		        		        		    	 	if(colValues.containsKey(compareKey5))
		        		        		        		    	 	{
		        		        		        		    	 		bodyObject5.put(keyStr5, colValues.get(compareKey5));          				
		        		        		    			        		     
		        		        		        		    	 	}  
		        		        		        		    	 	compareKey5 = "";
		        		        		        		    	 	bodyObject5.remove(keyStr4);
		        		        		        		    });      		        				
		        		        		        		}     	
		        		        		    	 	}    
		        		        		    	 	compareKey4 = "";
		        		        		    	 	bodyObject4.remove(keyStr3);
		        		        		    });      		        				
		        		        		}     	
		        		    	 	}    
		        		    	 	compareKey3 = "";
		        		    	 	bodyObject3.remove(keyStr2);
		        		    });      		        				
		        		}     		  
		    	 		
	        		}    
		    	 	
    	return jsonSource;
    	
   	}
    
    @SuppressWarnings("deprecation")
	public static String fngetcolvalue(String sheet_name, String iScript, String iSubScript, String col_name )throws IOException, InterruptedException {
	/*----------------------------------------------------------------------
	Method Name : fngetcolvalue
	Purpose     : to get the column value of the required column name
	Input       : file_name,sheet_name, iScript, iSubScript, col_name
	Output      : sVal3
	 ----------------------------------------------------------------------*/	
	String sResult = "";
	
	ArrayList<HashMap<String, String>> listOfRows = testDataSet.get(sheet_name);
	
	int totalRows = listOfRows.size();
	
	for(int i=0;i<totalRows;i++)
	{
		HashMap<String, String> rowData = listOfRows.get(i);
		
		String scriptValue = rowData.get("iScript");
		String dataSetValue = rowData.get("Data_Set_No");
		
		if(iScript.trim().equalsIgnoreCase(scriptValue) && iSubScript.equalsIgnoreCase(dataSetValue))
		{
			if(rowData.containsKey(col_name)){
			sResult = rowData.get(col_name);
			}else{
				sResult ="";
			}
			break;
		}			
	}
		
		return sResult;
	}
	
	public static void fnsetcolvalue(String sheet_name, String iScript, String iSubScript, String col_name, String col_value)throws IOException, InterruptedException
	/*----------------------------------------------------------------------
	Method Name : fnsetcolvalue
	Purpose     : to enter the value to required cell
	Input       : file_name,sheet_name, iScript, iSubScript, col_name, col_value
	Output      : sVal3
	 ----------------------------------------------------------------------*/	
	{
		ArrayList<HashMap<String, String>> listOfRows = testDataSet.get(sheet_name);
		
		int totalRows = listOfRows.size();
		
		for(int i=0;i<totalRows;i++)
		{
			HashMap<String, String> rowData = listOfRows.get(i);
			
			String scriptValue = rowData.get("iScript");
			String dataSetValue = rowData.get("Data_Set_No");
			
			if(iScript.trim().equalsIgnoreCase(scriptValue) && iSubScript.equalsIgnoreCase(dataSetValue))
			{
				rowData.put(col_name, col_value);
			}			
		}
	}
	
	public static String fngetconfigvalue(String Environment, String col_name)throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : fngetconfigvalue
		Purpose     : to get the credentials of the particular environment
		Input       : String file_name, String col_name
		Output      : sVal3
		 ----------------------------------------------------------------------*/
		String returnValue = "";
		
		//System.out.println("Full test data map: "+ testDataSet);
		
		ArrayList<HashMap<String, String>> listOfRows = testDataSet.get("Config_Sheet");
		
		int totalRows = listOfRows.size();
		
		for(int i=0;i<totalRows;i++)
		{
			HashMap<String, String> rowData = listOfRows.get(i);
			
			String envValue = rowData.get("Environment");
			if(Environment.trim().equalsIgnoreCase(envValue))
			{
				if(rowData.containsKey(col_name)){
				
				returnValue = rowData.get(col_name);
				}else{
					returnValue = "empty";
				}
				break;
			}			
		}
			return returnValue;
		}
	
	@SuppressWarnings("deprecation")
	public static List<String> fngetcolNames(String sheet_name)throws IOException, InterruptedException {
	/*----------------------------------------------------------------------
	Method Name : fngetcolvalue
	Purpose     : to get the column value of the required column name
	Input       : file_name,sheet_name, iScript, iSubScript, col_name
	Output      : sVal3
	 ----------------------------------------------------------------------*/	
	List<String> sResult = new ArrayList<>();
	String getCols = "false";
	LinkedHashMap<String, String> columnsMap = new LinkedHashMap<String, String>();
	
		try {
			InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties");
			prop2.load(input);
			
			String dataFileName = prop2.getProperty("TestDataSheet");
			String testDataFile = "./TestData/"+dataFileName;	
			
			LowMemoryExcelFileReader quickExcelReader = new LowMemoryExcelFileReader(testDataFile);
		    OPCPackage pkg = quickExcelReader.opcPackage();
		    XSSFReader reader = quickExcelReader.xssfReader(pkg);
		    columnsMap = quickExcelReader.getColNamesFromSheet(reader, sheet_name);
		    
		    Set<String> keys = columnsMap.keySet();
		    
		    for (String key : keys) {
	            System.out.println(key + " -- " + columnsMap.get(key));
	            if(columnsMap.get(key).equalsIgnoreCase("senderSystem"))
	            {
	            	getCols = "true";
	            }
	            
	            if(columnsMap.get(key).equalsIgnoreCase("EndPoint_URL"))
	            {
	            	break;
	            }
	            
	            if(getCols.equalsIgnoreCase("true"))
	            {
	            	sResult.add(columnsMap.get(key));
	            }	            
	            
	        }
				
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sResult;
	}
}
