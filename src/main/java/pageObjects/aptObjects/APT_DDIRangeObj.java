package pageObjects.aptObjects;

public class APT_DDIRangeObj {
	
	public static class DDI
	{


				public static final String searchDDi_actionDropdown="@xpath=//button[text()='Action']";
				public static final String searchDDI_viewLink="@xpath=//a[text()='View']";

				public static final String selectTrunkName="@xpath=//div[text()='value']";
				public static final String view="@xpath=//td//a[contains(.,'View')]";

			//	public static final String selectTrunk="@xpath=//div[text()='value']";

				public static final String FileuploadSuccess="@xpath=//span[contains(text(),'File Upload successfull')]";
				public static final String TrunkPanelView="@xpath=(//a[text()='View'])[1]";
				public static final String TrunkPanelAction="@xpath=//div[div[div[text()='Trunk Group/Site Orders']]]//button[text()='Action']";
				public static final String TrunkValue="@xpath=//div[text()=' + TrunkValue + ']";
				public static final String OKbutton="@xpath=//span[contains(text(),'OK')]";
				public static final String Cancelbtn="@xpath=//span[contains(text(),'Cancel')]";
				public static final String DDIRange_Header="@xpath=//h5[text()='DDI Range']";
				public static final String DuplicateRecord="@xpath=//span[contains(text(),'* Showing Duplicate DDI Ranges.')]";
				public static final String NoDuplicateRecord="@xpath=//span[contains(text(),' * No Duplicate DDI Ranges Found.')]";
				public static final String DDICount="@xpath=//span[contains(text(),'DDI Range Count:')]";

				public static final String NonServiceImpacting_Header="@xpath=//p[contains(text(),'Non Service Impacting')]";
				public static final String Configuration_Header="@xpath=//div[text()='Configuration']";


				public static final String DDIpanelHeader="@xpath=//div[h5[text()='DDI Range']]";
				public static final String ChosefileDDI="@xpath=(//input[@name='submitBulkJob'])";
				public static final String ManageCustomerServiceLink="@xpath=//b[contains(text(),'MANAGE CUSTOMER')]";
				public static final String SearchService="@xpath=//li[text()='Search Order/Service']";
				public static final String TrunkName="@xpath=//input[@id='trunkGrouptgName']";
				public static final String Searchbtn="@xpath=//span[text()='Search']";
				public static final String DDICheckbox="@xpath=(//span[@class='ag-icon ag-icon-checkbox-unchecked'])[8]";
				public static final String CountrycodeLabel="@xpath=//th[text()='Country Code']";
				public static final String LacLabel="@xpath=//th[text()='LAC']";
				public static final String MainNumberLabel="@xpath=(//th[text()='Main Number/Range Start-End'])";
				public static final String ExtensionLabel="@xpath=(//th[text()='Extension digits'])";
				public static final String EmergLabel="@xpath=(//th[text()='Emerg Area'])";
				public static final String IncomingLabel="@xpath=(//th[text()='Incoming Routing'])";
				public static final String InGeoLabel="@xpath=(//th[text()='IN GEO'])";
				public static final String CreateInGeo="@xpath=//label[text()='IN GEO']";
				public static final String ActivateIncomingRouting="@xpath=//label[text()='Activate Incoming Routing']";
				public static final String CreateDDIExtension="@xpath=//label[text()='Extension digits']";
				public static final String CreateDDIRangeEnd="@xpath=//label[text()='Range End']";
				public static final String CreateDDIRangeStart="@xpath=//label[text()='Range Start']";
				public static final String CreateDDIMainnumber="@xpath=//label[text()='Main Number']";
				public static final String CreateDDIPhoneNumbers="@xpath=//label[text()='Phone Numbers']";
				public static final String CreateDDICountry="@xpath=//label[text()='Country Code']";
				public static final String CreateDDILAC="@xpath=//label[text()='LAC']";
				public static final String AddMore="@xpath=//span[text()='Add More']";
				public static final String CountryCodeText="@xpath=//input[@id='countryCode']";
				public static final String LacText="@xpath=//input[@id='lac']";
				public static final String MainnumberText="@xpath=//input[@id='mainNumber']";
				public static final String RangeStartText="@xpath=//input[@id='rangeStart']";
				public static final String RangeEndText="@xpath=//input[@id='rangeEnd']";
				public static final String ExtensionText="@xpath=//input[@id='extensionDigitsAdd']";
				public static final String forwardarrow="@xpath=//span[contains(text(),'>>')]";
				public static final String CheckboxRouting="@xpath=//input[@id='incomingRoutingCheckAdd']";
				public static final String EmergencyAreaChooseButton="@xpath=//span[contains(text(),'Choose')]";
				public static final String EmergencyAreaSearchButton="@xpath=//span[contains(text(),'Search')]";
				public static final String EmergencyAreaIDCheckbox="@xpath=(//div[1]//span[1]//span[1]//span[2])[1]";
				public static final String CheckboxInGeo="@xpath=//input[@id='ingeoCheckAdd']";
				public static final String DDIcreationMsg="@xpath=//span[text()='DDI Range successfully created.']";
				public static final String LACFilter="@xpath=(//span[@class='ag-icon ag-icon-menu'])[1]";
				public static final String LACDropDown="@xpath=//select[@id='filterType']";
				public static final String LACFilter_Text="@xpath=//input[@id='filterText']";
				public static final String DDIRadiobtn="@xpath=//input[@type='radio']";
				public static final String Deletebtn="@xpath=//button[text()='Delete']";
				public static final String DDIEdit="@xpath=(//a[text()='Edit'])[1]";
				public static final String DDIDelete="@xpath=(//a[text()='Delete'])[1]";
				public static final String DDIPanelAction="@xpath=(//button[text()='Action'])[3]";
				public static final String AddDDIRange="@xpath=//span[text()='Add DDI Range']";

				public static final String AddDDIRangeSuccessMessage="@xpath=//span[contains(text(),'DDI Range successfully created.')]";
				public static final String DeleteDDIRangeSuccessMessage="@xpath=//span[contains(text(),'DDIRange Delete successfully')]";
				public static final String UpdateDDIRangeSuccessMessage="@xpath=//span[contains(text(),'DDI Range successfully updated.')]";
				public static final String DuplicateDDIRange="@xpath=//div//a[text()='Show Duplicate DDI Ranges']";
				public static final String DownlaodDDIRanges="@xpath=//div//a[text()='Download DDI Ranges']";
				public static final String UploadDDIRanges="@xpath=//div//a[text()='Upload DDI Ranges']";
				public static final String ShowDDIRanges="@xpath=//div//a[text()='Show DDI Ranges']";
				
				public static final String countryLabel_viewPage="@xpath=//label[text()='Country']";
				public static final String LAClabel_viewPage="@xpath=//label[text()='LAC']";
				public static final String extensionLabel_viewPage="@xpath=//label[text()='Extension digits']";
				public static final String ConfigurationLabel="@xpath=//div[text()='Configuration']";
				public static final String PsxConfigurationLabel="@xpath=//label[text()='PSX Configuration']";
				public static final String PhoneNumerLabel="@xpath=//label[text()='Phone Numbers']";
				public static final String ViewEmergLabel="@xpath=//label[text()='Emerg Area']";
				public static final String Backbutton="@xpath=//span[text()='Back']";
				public static final String DDIRangeUpdate="@xpath=//span[text()='DDI Range successfully updated.']";
				public static final String ManageColtNetworkLink="@xpath=//span/b[contains(text(),'MANAGE COLT')]";
				public static final String SearchDDI="@xpath=//li[text()='Search DDI Ranges']";
				public static final String DDIWildcard="@xpath=//label[text()='(You can use % as wildcard)']";
				public static final String DDIISdcode="@xpath=//label[text()='ISD Code(Country Code)']";
				public static final String DDISTdcode="@xpath=//label[text()='STD Code(LAC) ']";
				public static final String DDIManageNmbr="@xpath=//label[text()='Main Number/Range Start-End']";
				public static final String DDIISdcode_Text="@xpath=//input[@id='isdcode']";
				public static final String DDILAC_Text="@xpath=//input[@id='stdcode']";
				public static final String Action="@xpath=//button[text()='Action']";

				public static final String viewPage_countryValue="@xpath=//div[div[div[div[label[text()='Country']]]]]//div[@class='customLabelValue form-label']";
				public static final String viewPage_LACvalue="@xpath=//div[div[div[div[label[text()='LAC']]]]]//div[@class='customLabelValue form-label']";
				public static final String viewPage_extensionDigitVale="@xpath=//div[div[div[div[label[text()='Extension digits']]]]]//div[@class='customLabelValue form-label']";
				public static final String viewPage_activateIncomingRouting="@xpath=//div[div[div[div[label[text()='Activate Incoming Routing']]]]]//div[@class='customLabelValue form-label']";
				public static final String viewPage_INGEOValue="@xpath=//div[div[div[div[label[text()='IN GEO']]]]]//div[@class='customLabelValue form-label']";


				public static final String PSXconfigurationDropdown_viewDDI="@xpath=(//div[label[text()='PSX Configuration']]//input)[1]";
				public static final String executeButton_vieDDI="@xpath=//div[div[div[label[text()='PSX Configuration']]]]//following-sibling::div//button/span";
				public static final String DDIPSXConfigurationSuccessMessage="@xpath=//span[contains(text(),'PSX sync started successfully. Please check the sy')]";
				public static final String DDIPSXConfigurationWarningMessage="@xpath=//li[contains(text(),'PSX sync could not be started because this Trunk G')]";
				public static final String alertMSG_viewPage="@xpath=//div[@role='alert']/span/li";
				public static final String alertMSGviewPage="@xpath=//div[@role='alert']/span/li";



				public static final String viewLink_selectAddedDDI="@xpath=(//div[div[h5[text()='DDI Range']]]//following-sibling::div//div[div[text()='43']])[1]//a[text()='value']";
				public static final String selectLACvalue_searchFoeDDipage="@xpath=(//span[@class='ag-selection-checkbox']//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";
				public static final String ssearchForDDI_viewLink="@xpath=//a[text()='View']";
				public static final String searchForDDI_actionDropdown="@xpath=//button[text()='Action']";
				public static final String ViewTrunk_Header="@xpath=//div/div[contains(text(),'View Trunk')]";

			//	public static final String !--  success Message --="@xpath=
				public static final String alertMsg="@xpath=(//div[@role='alert'])[1]";
				public static final String AlertForServiceCreationSuccessMessage="@xpath=(//div[@role='alert']/span)[1]";
}

}
