package pageObjects.aptObjects;

public class APT_DoimainManagementObj {
	public static class Domainmanagement {

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]//span";
		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customer's Service')]";
		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";
		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		// !-- Create Customer Page --= "@xpath=
		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String nametextfield = "@xpath=//input[@id='name']";
		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";
		public static final String country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String ocntextfield = "@xpath=//input[@id='ocn']";
		public static final String referencetextfield = "@xpath=//input[@id='reference']";
		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";
		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";
		//public static final String emailtextfield = "@xpath=//input[@id='email']";
		public static final String phonetextfield = "@xpath=//input[@id='phone']";
		public static final String faxtextfield = "@xpath=//input[@id='fax']";
		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";
		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String resetbutton = "@xpath=//span[text()='Reset']";
		public static final String clearbutton = "@xpath=//span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// !-- Verify warning messages --= "@xpath=
		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";
		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";
		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";
		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";
		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";
		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";
		public static final String sidwarngmsg = "@xpath=//input[@id='serviceIdentification']/parent::div//div";
		public static final String userfieldwarngmsg = "@xpath=//label[text()='User']/parent::div//div";
		public static final String passwordfieldwarngmsg = "@xpath=//label[text()='Password']/parent::div//div";
		public static final String defaultemailwarngmsg = "@xpath=//label[text()='Default Email']/parent::div//div";
		public static final String firstnamefieldwarngmsg = "@xpath=//label[text()='First Name']/parent::div//div";
		public static final String lastnamefieldwarngmsg = "@xpath=//label[text()='Last Name']/parent::div//div";
		public static final String organizationnamewarngmsg = "@xpath=//label[text()='Organization Name']/parent::div//div";
		public static final String addressfieldwarngmsg = "@xpath=//label[text()='Address']/parent::div//div";
		public static final String postalcodewarngmsg = "@xpath=//label[text()='Postal Code']/parent::div//div";
		public static final String cityfieldwarngmsg = "@xpath=//label[text()='City']/parent::div//div";
		public static final String phonefieldwarngmsg = "@xpath=//label[text()='Phone']/parent::div//div";
		public static final String faxfieldwarngmsg = "@xpath=//label[text()='Fax']/parent::div//div";
		public static final String emailfieldwarngmsg = "@xpath=(//label[text()='Email']/parent::div//div)[2]";

		// !-- Verify Customer details panel --= "@xpath=
		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath=//label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";
		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath=//label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// String !-- create order/service page --= "@xpath=
//***		public static final String Custnametextfield = "@xpath=//input[@id='customerSearch']";
		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";
		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";
		//public static final String country = "@xpath=//div[div[label[text()='Country']]]/div[2]";
		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";
		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";
		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";
		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";
		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";
		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";
		public static final String useractionbutton = "@xpath=//button[@id='dropdown-basic-button']";
		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";

		// Create Order/service panel in view customer page --= "@xpath=
		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorderswitch = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/following-sibling::input/ancestor::div[@class='form-group']/parent::div/following-sibling::div//div[@class='react-switch-bg']";
		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";
		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath=//input[@id='orderName']";
		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String order_contractnumber_warngmsg = "@xpath=//div[label[contains(text(),'Order/Contract Number(Parent SID)')]]/following-sibling::span";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";

		public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String Country = "@xpath=//div[div[label[text()='Country']]]//input";
		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String successmsg = "@xpath=//div[@role='alert']//span";

		// !-- Service Creation Form --= "@xpath=
		public static final String createorderservice_header = "@xpath=//div//div[text()='Create Order / Service']";
		public static final String serviceidentificationtextfield = "@xpath=//input[@id='serviceIdentification']";
		public static final String servicetypevalue = "@xpath=//div[label[text()='Service Type']]/div";
		public static final String emailtextfield = "@xpath=//input[@id='email']";
		public static final String emailtextfieldvalue = "@xpath=(//div[label[text()='Email']]//input)[1]";
		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContact']";
		public static final String phonecontacttextfieldvalue = "@xpath=//label[text()='Phone Contact']/following-sibling::input";
		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'remark')]";
		public static final String remarktextareavalue = "@xpath=//div[label[text()='Remark']]//textarea";
		public static final String service_country = "@xpath=(//label[text()='Country']/parent::div//div)[3]";
		public static final String performancereportingcheckbox = "@xpath=//input[@id='performanceReporting']";
		//public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String servicecreationmessage = "@xpath=//div[@role='alert']//span";
		public static final String servicetype_value = "@xpath=//label[text()='Service Type']/following-sibling::div";
		public static final String emailaddarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phoneaddarrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String userfield = "@xpath=//label[text()='User']/following-sibling::input";
		public static final String defaultemail = "@xpath=//label[text()='Default Email']/following-sibling::input";
		public static final String servicefirstname = "@xpath=//label[text()='First Name']/following-sibling::input";
		public static final String servicelastname = "@xpath=//label[text()='Last Name']/following-sibling::input";
		public static final String organizationname = "@xpath=//label[text()='Organization Name']/following-sibling::input";
		public static final String serviceaddress = "@xpath=//label[text()='Address']/following-sibling::input";
		public static final String servicecomplement = "@xpath=//label[text()='Complement']/following-sibling::input";
		public static final String servicepostalcode = "@xpath=//label[text()='Postal Code']/following-sibling::input";
		public static final String servicecity = "@xpath=//label[text()='City']/following-sibling::input";
		public static final String servicestate = "@xpath=//label[text()='State']/following-sibling::input";
		public static final String servicephone = "@xpath=//label[text()='Phone']/following-sibling::input";
		public static final String servicefax = "@xpath=//label[text()='Fax']/following-sibling::input";
		public static final String serviceemail = "@xpath=(//label[text()='Email']/following-sibling::input)[2]";
		public static final String emailremovearrow = "@xpath=(//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String phoneremovearrow = "@xpath=(//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span)[2]";

		// !-- public static final String cancelbutton=
		// "@xpath=//button[@type='submit']//span[text()='Cancel']";

		// !-- Search order --= "@xpath=
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		// !-- order panel - view service page --= "@xpath=
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//input[@id='orderNameNew']";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";
		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[2]";
		public static final String orderpanelheader = "@xpath=//div[text()='Order']";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";
		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// !-- service panel --= "@xpath=
		public static final String servicepanel_serviceidentificationvalue = "@xpath=//div[label[contains(text(),'Service Identification')]]/div";
		public static final String servicepanel_servicetypevalue = "@xpath=//div[label[contains(text(),'Service Type')]]/div";
		public static final String servicepanel_remarksvalue = "@xpath=//div[label[contains(text(),'Remark')]]/div";
		public static final String servicepanel_header = "@xpath=//div[text()='Service']";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";
		public static final String serviceupdate_successmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String synchronizelink_servicepanel = "@xpath=//a[text()='Synchronize']";
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";
		public static final String status_ordername = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String status_servicename = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String status_servicetype = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String status_servicedetails = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String status_currentstatus = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='currentStatus']";
		public static final String status_modificationtime = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='mTime']";
		public static final String statuslink = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell']//a[text()='Status']";
		public static final String sync_ordername = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String sync_servicename = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String sync_servicetype = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String sync_servicedetails = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String sync_status = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='syncStatus']";
		public static final String synchronizelink = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell']//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";
		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String editservice_next = "@xpath=//button//span[contains(text(),'Next')]";

		// !-- // Domain management service panel verify
		// --= "@xpath=
		public static final String sidvalue = "@xpath=//label[text()='Service Identification']/following-sibling::div";
		public static final String emailvalue = "@xpath=//label[text()='Email']/following-sibling::div";
		public static final String phonecontactvalue = "@xpath=//label[text()='Phone Contact']/following-sibling::div";
		public static final String remarksvalue = "@xpath=//label[text()='Remark']/following-sibling::div";
		public static final String userfieldvalue = "@xpath=//label[text()='User']/following-sibling::div";
		public static final String defaultemailvalue = "@xpath=//label[text()='Default Email']/following-sibling::div";
		public static final String servicefirstnamevalue = "@xpath=//label[text()='First Name']/following-sibling::div";
		public static final String servicelastnamevalue = "@xpath=//label[text()='Last Name']/following-sibling::div";
		public static final String organizationnamevalue = "@xpath=//label[text()='Organization Name']/following-sibling::div";
		public static final String serviceaddressvalue = "@xpath=//label[text()='Address']/following-sibling::div";
		public static final String servicecomplementvalue = "@xpath=//label[text()='Complement']/following-sibling::div";
		public static final String servicepostalcodevalue = "@xpath=//label[text()='Postal Code']/following-sibling::div";
		public static final String servicecityvalue = "@xpath=//label[text()='City']/following-sibling::div";
		public static final String servicestatevalue = "@xpath=//label[text()='State']/following-sibling::div";
		public static final String servicecountryvalue = "@xpath=//label[text()='Country']/following-sibling::div";
		public static final String servicephonevalue = "@xpath=//label[text()='Phone']/following-sibling::div";
		public static final String servicefaxvalue = "@xpath=//label[text()='Fax']/following-sibling::div";
		public static final String serviceemailvalue = "@xpath=(//label[text()='Email']/following-sibling::div)[2]";
		public static final String editservice = "@xpath=//div[text()='Edit Service']";
		public static final String selectedemail = "@xpath=//select[@name='selectedEmail']//option";
		public static final String selectedphone = "@xpath=//select[@name='selectedPhoneContact']//option";
		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";

		// !-- Management Options --= "@xpath=
		public static final String managementoptions_customeradminvalue = "@xpath=//div[label[text()='Customer Administration']]/div";
		public static final String managementoptions_sanadminvalue = "@xpath=//div[label[text()='SAN Administration']]/div";
		public static final String managementoptions_reselladminvalue = "@xpath=//div[label[text()='Reseller Administration']]/div";

		// !-- Users panel in view service page --= "@xpath=

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";
		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";
		public static final String EmailColumn = "@xpath=//div[@col-id='email']";
		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";
		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";
		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";
		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// !-- New User creation in view service page --= "@xpath=

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";
		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";
		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";
		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";
		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";
		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";
		public static final String Email = "@xpath=(//div[@class='position-relative form-group'])[5]//input[@id='email']";
		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";
		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";
		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";
		//public static final String OkButton = "@xpath=//button[@type='submit']//span[text()='Ok']";
		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()='Delete']";
		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String userspanel_header = "@xpath=//div[text()='Users']";

		// !-- Management Options panel in view service page --= "@xpath=

		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";
		public static final String customeradmin_text = "@xpath=//label[text()='Customer Administration']";
		public static final String customeradmin_value = "@xpath=(//div[@class='position-relative form-group']//div[text()='Yes'])[1]";
		public static final String sanadmin_text = "@xpath=//label[text()='SAN Administration']";
		public static final String sanadmin_value = "@xpath=(//div[@class='position-relative form-group']//div[text()='Yes'])[2]";
		public static final String reselleradmin_text = "@xpath=//label[text()='Reseller Administration']";
		public static final String reselleradmin_value = "@xpath=(//div[@class='position-relative form-group']//div[text()='Yes'])[3]";

		// !-- success Message --= "@xpath=
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";
	}

}
