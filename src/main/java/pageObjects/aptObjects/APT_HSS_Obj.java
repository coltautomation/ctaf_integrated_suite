package pageObjects.aptObjects;

public class APT_HSS_Obj {
	public static class APT_HSS {
		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";
		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customer')]";
		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";
		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		// Create Customer Page
		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String nametextfield = "@xpath=//input[@id='name']";
		
		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";
		public static final String country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String ocntextfield = "@xpath=//input[@id='ocn']";
		public static final String referencetextfield = "@xpath=//input[@id='reference']";
		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";
		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";
		public static final String emailtextfield = "@xpath=//input[@id='email']";
		public static final String phonetextfield = "@xpath=//input[@id='phone']";
		public static final String faxtextfield = "@xpath=//input[@id='fax']";
		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";
		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";
		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String resetbutton = "@xpath=//span[text()='Reset']";
		public static final String clearbutton = "@xpath=//span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// Verify warning messages
		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";
		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";
		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";
		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";
		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";
		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";
		public static final String sidwarngmsg = "@xpath=//input[@id='serviceIdentification']/parent::div//div";

		// Verify Customer details panel
		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath=//label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";
		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath=//label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// create order/service page
		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";

		// Create Order/service panel in view customer page
		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorder_switch = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/following-sibling::input/ancestor::div[@class='form-group']/parent::div/following-sibling::div//div[@class='react-switch-bg']";
		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";
		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath=//input[@id='orderName']";
		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String order_contractnumber_warngmsg = "@xpath=//div[label[contains(text(),'Order/Contract Number(Parent SID)')]]//span[contains(text(),'Order/Contract Number(Parent SID)')]";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";

		//public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";
		
		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// Service Creation
		public static final String createorderservice_header = "@xpath=//div//div[text()=' Create Order / Service']";
		public static final String userspanel_header = "@xpath=//div[text()='Users']";
		public static final String ServiceMessg = "@xpath=//span[contains(text(),'Service successfully created.')]";
		public static final String ServiceIdentification = "@xpath=//input[@id='serviceIdentification']";
		public static final String forwardarrow = "@xpath=//span[contains(text(),'>>')]";
		public static final String forwardarrow_1 = "@xpath=(//span[contains(text(),'>>')])[2]";
		public static final String EmailText = "@xpath=//div[label[contains(text(),'Email')]]//input";
		public static final String EmailLabel = "@xpath=//label[contains(text(),'Email')]";
		public static final String PhoneContact = "@xpath=//input[@id='phoneContact']";
		public static final String RemarkText = "@xpath=//textarea[@name='remark']";
		public static final String RemarkLabel = "@xpath=//label[contains(text(),'Remark')]";
		public static final String PerformancereportingLabel = "@xpath=//label[contains(text(),'Performance Reporting')]";
		public static final String performanceReportingcheckbox = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String ServiceIdentificationLabel = "@xpath=//label[contains(text(),'Service Identification')]";
		public static final String ServiceTypeLabel = "@xpath=//label[contains(text(),'Service Type')]";
		public static final String PhoneContactLabel = "@xpath=//label[contains(text(),'Phone Contact')]";
		// public static final String PerformancereportingLabel
		// ="@xpath=//label[contains(text(),'Performance Reporting')]";
		public static final String WaveServicelabel = "@xpath=//label[contains(text(),'Wave Service')]";
		public static final String waveServiceCheckbox = "@xpath=//div[label[contains(text(),'Wave Service')]]//input";
		public static final String serviceIdentificationerrmsg = "@xpath=//div[contains(text(),'Service Identification')]";
		public static final String OKbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String Cancelbtn = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";
		public static final String orderpanelheader = "@xpath=//div[text()='Order']";

		// service panel
		public static final String servicepanel_header = "@xpath=//div[text()='Service']";
		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";
		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String serviceidentificationtextfield = "@xpath=//input[@id='serviceIdentification']";
		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'remark')]";
		public static final String servicepanel_serviceidentificationvalue = "@xpath=//label[contains(text(),'Service Identification')]/parent::div/following-sibling::div";
		public static final String servicepanel_servicetypevalue = "@xpath=//label[contains(text(),'Service Type')]/parent::div/following-sibling::div";
		public static final String servicepanel_remarksvalue = "@xpath=//label[contains(text(),'Remark')]/following-sibling::div";
		public static final String servicepanel_terminationdate = "@xpath=//label[contains(text(),'Termination Date')]/parent::div/following-sibling::div";
		public static final String servicepanel_email = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Email')]/parent::div/following-sibling::div";
		public static final String servicepanel_phone = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Phone Contact')]/parent::div/following-sibling::div";
		public static final String servicepanel_billingtype = "@xpath=//label[contains(text(),'Billing Type')]/parent::div/following-sibling::div";
		// public static final String servicepanel_header ="@xpath=//div[text()='Service']";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";

		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String synchronizelink_servicepanel = "@xpath=//a[text()='Synchronize']";
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";

		public static final String status_serviceheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service']";
		public static final String status_servicetypeheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service Type']";
		public static final String status_detailsheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Details']";
		public static final String status_statusheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Status']";
		public static final String status_modificationheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Last Modification']";
		public static final String sync_serviceheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service']";
		public static final String sync_servicetypeheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service Type']";
		public static final String sync_detailsheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Details']";
		public static final String sync_syncstatus = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Sync Status']";
		public static final String devicesforservice_deviceheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Device']";
		public static final String devicesforservice_syncstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Sync Status']";
		public static final String devicesforservice_fetchinterfacesheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Fetch Interfaces']";
		public static final String devicesforservice_vistamartdeviceheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='VistaMart Device']";

		public static final String status_ordername = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[2]/div/label";
		public static final String status_servicename = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]/a";
		public static final String status_servicetype = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";
		public static final String status_servicedetails = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]//a";
		public static final String status_currentstatus = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";
		public static final String status_modificationtime = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]";
		public static final String statuslink = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div/a[text()='Status']";

		public static final String sync_ordername = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[2]/div/label";
		public static final String sync_servicename = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]/a";
		public static final String sync_servicetype = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";
		public static final String sync_servicedetails = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]//a";
		public static final String sync_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";
		public static final String vistamart_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]/span";
		public static final String vistamart_datetime = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]//p";
		public static final String synchronization_serviceerror = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]/span";
		public static final String synchronizelink = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";

		public static final String statuspanel_header = "@xpath=//div[text()='Status']";
		public static final String servicestatus_header = "@xpath=//div[@class='modal-header']/div[text()='Service Status']";
		public static final String statuspage_serviceidentification = "@xpath=//label[text()='Service Identification']/following-sibling::div";
		public static final String statuspage_servicetype = "@xpath=//label[text()='Service Type']/following-sibling::div";
		public static final String servicestatushistory_header = "@xpath=//div[text()='Service Status History']";
		public static final String statuspage_currentstatus = "@xpath=//label[text()='Current Status']/following-sibling::div";
		public static final String statuspage_newstatusdropdown = "@xpath=//label[text()='New Status']/following-sibling::select";
		public static final String statuspage_newstatusdropdownvalue = "@xpath=//label[text()='New Status']/following-sibling::select/option";

		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String devicesforservice_header = "@xpath=//div[text()='Devices for service']";
		public static final String deviceforservicepanel_devicename = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[1]";
		public static final String deviceforservicepanel_syncstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[2]";
		public static final String deviceforservicepanel_smartsstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/span";
		public static final String deviceforservicepanel_smartsdatetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/p";
		public static final String deviceforservicepanel_fetchinterfaces_status = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/span";
		public static final String deviceforservicepanel_fetchinterfaces_datetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/p";
		public static final String deviceforservicepanel_vistamart_status = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[4]/span";
		public static final String deviceforservicepanel_vistamart_datetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[4]/p";
		public static final String deviceforservicepanel_managelink = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div/a[text()='Manage']";

		public static final String managenetwork_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div";

		// public static final String successmsg
		// ="@xpath=//div[@role='alert']//span";
		public static final String editservice_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String managesubnets_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets']";
		public static final String managesubnet_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Manage')]";
		public static final String managesubnet_successmsg = "@xpath=(//div[@role='alert']//span)[2]";
		public static final String spacename_column = "@xpath=//span[text()='Space Name']";
		public static final String blockname_column = "@xpath=//span[text()='Block Name']";
		public static final String subnetname_column = "@xpath=//span[text()='Subnet Name']";
		public static final String startaddress_column = "@xpath=//span[text()='Start Address']";
		public static final String size_column = "@xpath=//span[text()='Size']";
		public static final String closesymbol = "@xpath=//button[@type='button']//span[text()='�']";
		public static final String dumppage_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Service')]";
		public static final String serviceretrieved_text = "@xpath=//div[@class='div-margin row'][contains(text(),'Service retrieved')]";
		public static final String service_header = "@xpath=//label[text()='Service']";
		public static final String dumppage_text = "@xpath=//label[text()='Service']/following-sibling::textarea";

		public static final String managesubnetsipv6_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";
		public static final String shownewinfovistareport_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";
		public static final String showinfovistareport_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show Infovista Report']";
		public static final String dump_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Dump']";
		public static final String selectedemail = "@xpath=//select[@name='selectedEmail']//option";
		public static final String selectedphone = "@xpath=//select[@name='selectedPhoneContact']//option";
		public static final String emailremovearrow = "@xpath=(//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String phoneremovearrow = "@xpath=(//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";
		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";
		public static final String emailaddarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phoneaddarrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";

		public static final String performancereporting_value = "@xpath=//div[label[text()='Performance Reporting']]/following-sibling::div";
		public static final String waveservice_value = "@xpath=//div[label[text()='Wave Service']]/following-sibling::div";
		public static final String interfacespeed_value = "@xpath=//div[label[text()='Interface Speed']]/following-sibling::div";
		public static final String typeofservice_value = "@xpath=//div[label[text()='Type Of Service']]/following-sibling::div";

		// Device Header
		public static final String addeddevices_list = "@xpath=//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row']//b";
		public static final String addeddevice_selectinterfaceslink = "@xpath=//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";
		public static final String deviceheader = "@xpath=//div[text()='Device']";
		public static final String AddDevice = "@xpath=//a[text()='Add Device']";
		public static final String showinterfaces_link = "@xpath=//a[text()='Show Interfaces']";
		public static final String adddevice_header = "@xpath=//p[text()='Add Device']";
		public static final String addnewdevice_togglebutton = "@xpath=(//div[@class='react-switch-handle'])[1]";
		// public static final String Next_Button
		// ="@xpath=//button//Span[text()='Next']";
		public static final String portalaccess_header = "@xpath=//label[text()='Portal Access']";
		public static final String successmsg = "@xpath=//div[@role='alert']//span";
		public static final String breadcrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";

		// Create Device Page
		public static final String name_dropdown = "@xpath=(//div[label[text()='Name']]//input)[1]";
		public static final String devicename_textfield = "@xpath=//input[@id='deviceName']";
		public static final String vendormodelinput = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String snmprotextfield = "@xpath=//input[@id='snmpro']";
		public static final String telnetradiobutton = "@xpath=//input[@value='telnet']";
		public static final String sshradiobutton = "@xpath=//input[@value='ssh']";
		public static final String c2cradiobutton = "@xpath=//input[@value='2c']";
		public static final String c3radiobutton = "@xpath=//input[@value='3']";
		// public static final String snmprotextfield
		// ="@xpath=//input[@id='snmpro']";
		public static final String snmprwtextfield = "@xpath=//input[@id='snmprw']";
		public static final String snmpv3username = "@xpath=//div[label[text()='Snmp v3 Username']]//input";
		public static final String snmpv3authpassword = "@xpath=//div[label[text()='Snmp v3 Auth Password']]//input";
		public static final String snmpv3privpassword = "@xpath=//div[label[text()='Snmp v3 Priv Password']]//input";
		public static final String securityprotocols_dropdown = "@xpath=//div[label[contains(text(),'Security Protocols')]]//input";
		public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String managementaddresstextbox = "@xpath=//input[@id='addressSelect']";
		public static final String citydropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//input";
		public static final String sitedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//input";
		public static final String premisedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//input";
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String successMessage_Alert = "@xpath=//div[@role='alert']//span";
		public static final String addeddevice_interface_actiondropdown = "@xpath=//div[div[b[text()='value']]]//following-sibling::div//button[text()='Action']";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";

		// view device page
		public static final String viewpage_devicename = "@xpath=//label[text()='Name']/parent::div/following-sibling::div";
		public static final String viewpage_vendormodel = "@xpath=//label[text()='Vendor/Model']/parent::div/following-sibling::div";
		public static final String viewpage_managementaddress = "@xpath=//label[text()='Management Address']/parent::div/following-sibling::div";
		public static final String viewpage_securityprotocols = "@xpath=//label[text()='Security Protocols']/parent::div/following-sibling::div";
		public static final String viewpage_country = "@xpath=//label[text()='Country']/parent::div/following-sibling::div";
		public static final String viewpage_city = "@xpath=//label[text()='City']/parent::div/following-sibling::div";
		public static final String viewpage_site = "@xpath=//label[text()='Site']/parent::div/following-sibling::div";
		public static final String viewpage_premise = "@xpath=//label[text()='Premise']/parent::div/following-sibling::div";
		public static final String viewpage_connectivityprotocol = "@xpath=//label[text()='Connectiviy Protocol']/parent::div/following-sibling::div";
		public static final String viewpage_snmpversion = "@xpath=//label[text()='SNMP Version']/parent::div/following-sibling::div";
		public static final String viewpage_snmpro = "@xpath=//label[text()='Snmpro']/parent::div/following-sibling::div";
		public static final String viewpage_snmprw = "@xpath=//label[text()='Snmprw']/parent::div/following-sibling::div";
		public static final String viewpage_snmpv3username = "@xpath=//label[text()='Snmp V3 User Name']/following-sibling::div";
		public static final String viewpage_snmpv3privpassword = "@xpath=//label[text()='Snmp V3 Priv Password']/following-sibling::div";
		public static final String viewpage_snmpv3authpassword = "@xpath=//label[text()='Snmp V3 Auth Password']/following-sibling::div";

		// Edit Device
		public static final String viewdevice_Actiondropdown = "@xpath=//div[text()='Device Details']/following-sibling::div//button[text()='Action']";
		public static final String viewdevice_Edit = "@xpath=//div[text()='Device Details']/following-sibling::div//a[text()='Edit']";
		public static final String editdeviceheader = "@xpath=//div[@class='heading-green-row row']//p[text()='Edit Device']";

		public static final String existingdevicegrid = "@xpath=(//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row'])[2]";
		public static final String viewservicepage_viewdevicelink = "@xpath=//a//span[text()='View']";
		public static final String viewdevicepage_header = "@xpath=//div[text()='Device Details']";
		public static final String viewservicepage_editdevicelink = "@xpath=//a/span[text()='Edit']";

		// Delete Device
		public static final String viewdevice_delete = "@xpath=//div[text()='Device Details']/following-sibling::div//a[text()='Delete']";
		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='�']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";
		public static final String managementaddressbutton = "@xpath=//button//span[text()='Management Address']";
		public static final String addeddevice_deletefromservicelink = "@xpath=//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete']";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()='Delete']";

		public static final String Deviceemsg = "@xpath=//span[contains(text(),'Device successfully created.')]";
		public static final String Devicedeleteemsg = "@xpath=//span[contains(text(),'Device successfully deleted')]";
		public static final String InterfaceSpeedDropdown = "@xpath=(//div[label[text()='Interface Speed']]//input)[1]";
		public static final String TypeofServiceDropdown = "@xpath=(//div[label[text()='Type Of Service']]//input)[1]";
		public static final String ChangeordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number (Parent SID)']]//input)[1]";
		public static final String AddDeviceName = "@xpath=(//div[label[text()='Name']]//input)[1]";
		public static final String Xbutton = "@xpath=//div[text()='�']";
		public static final String Backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String Synchronizemsg = "@xpath=//span[contains(text(),'Sync started successfully. Please check the sync status of this service.')]";
		public static final String Errormsg = "@xpath=//div[text()='Error']";

		// order panel - view service page
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";
		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";
		// public static final String orderpanelheader
		// ="@xpath=(//div[contains(text(),'Order')])[1]";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// Search order
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		// Select Interface
		public static final String viewdeviceheader = "@xpath=//div[text()='Device']";
		public static final String interfaceheader = "@xpath=//div[text()='Interfaces']";
		public static final String interfacespanel_actiondropdown = "@xpath=//div[div[text()='Interfaces']]//following-sibling::div//button[text()='Action']";
		public static final String interfacespanel_OK_link = "@xpath=//button[text()='Action']/following-sibling::div/a[text()='Ok']";

		// success Message
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		// =========================================================================================================////////////////////////

		public static final String InterfacesGridCheck = "@xpath=(//div[text()='Interfaces']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]";
		public static final String AddedInterfaces = "@xpath=(//span[@class='ag-selection-checkbox'])[1]";
		//public static final String AddedInterfaces = "@xpath=(//div[text()='Interfaces']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[contains(@class,'unchecked')])[1]";
		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String selectorderswitch = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/following-sibling::input/ancestor::div[@class='form-group']/parent::div/following-sibling::div//div[@class='react-switch-bg']";
		public static final String billingtype_warngmsg = "@xpath=//label[text()='Billing Type']/parent::div//div";
		public static final String billingtype_dropdown = "@xpath=//label[text()='Billing Type']/parent::div//select ";
		public static final String terminationdate_field = "@xpath=//label[text()='Termination Date']/parent::div//input[@name='terminationDate'] ";
		public static final String emailtextfield_Service = "@xpath=//label[text()='Email']/following-sibling::input";
		public static final String emailarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContact'] ";
		public static final String phonearrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String servicecreationmessage = "@xpath=//div[@role='alert']//span ";
		public static final String cancelbutton1 = "@xpath=//span[contains(text(),'Cancel')] ";
		public static final String providerequipment_header = "@xpath=//div[text()='Provider Equipment (PE)'] ";
		public static final String addpedevice_link = "@xpath=//a[text()='Add PE Device'] ";
		public static final String addpedevice_header = "@xpath=//div[text()='Add PE Device'] ";
		public static final String Devicedeletionsuccessmessage = "@xpath=//span[text()='Device successfully removed from service.']";
		public static final String back = "@xpath=(//nav//ol//li/following-sibling::li/following-sibling::li)[1]";
		public static final String deviceAction = "@xpath=(//button[@type='button' and contains(text(),'Action')])[1]";
		public static final String deviceDelete = "@xpath=//a[@role='button' and contains(text(),'Delete')]";
		public static final String deleteMsg = "@xpath=//span[contains(text(),'Device successfully removed from service.')]";
		public static final String existingdevice_country = "@xpath=(//label[text()='Country']/parent::div/div)[1]";
		public static final String existingdevice_city = "@xpath=(//label[text()='City']/parent::div/div)[1]";
		public static final String existingdevice_site = "@xpath=(//label[text()='Site']/parent::div/div)[1]";
		public static final String existingdevice_premise = "@xpath=(//label[text()='Premise']/parent::div/div)[1]";
		public static final String typepename_dropdown = "@xpath=//label[text()='Type PE name to filter']/parent::div//div[contains(@class,'react-dropdown-select-content')]";
		public static final String existingdevice_vendormodelvalue = "@xpath=(//label[text()='Vendor/Model']/parent::div/div)[1]";
		public static final String existingdevice_managementaddressvalue = "@xpath=(//label[text()='Management Address']/parent::div/div)[1]";
		public static final String existingdevice_connectivityprotocol = "@xpath=(//label[text()='Connectivity Protocal']/parent::div/div)[1]";
		public static final String existingdevice_snmpversion = "@xpath=(//label[text()='SNMP Version']/parent::div/div)[1]";
		public static final String existingdevice_snmpro = "@xpath=(//label[text()='Snmpro']/parent::div/div)[1]";
		public static final String existingdevice_snmprw = "@xpath=(//label[text()='Snmprw']/parent::div/div)[1]";
		public static final String existingdevice_snmpv3username = "@xpath=(//label[text()='Snmp v3 Username']/parent::div/div)[1]";
		public static final String existingdevice_snmpv3authpassword = "@xpath=(//label[text()='Snmp v3 Auth password']/parent::div/div)[1]";
		public static final String existingdevice_snmpv3privpassword = "@xpath=(//label[text()='Snmp v3 Priv password']/parent::div/div)[1]";
		public static final String Routes_OKButton = "@xpath=//span[text()='OK']";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1] ";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1] ";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1] ";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1] ";
		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2] ";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2] ";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2] ";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2] ";
		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3] ";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3] ";
		public static final String citynameinputfield = "@xpath=//input[@id='cityName'] ";
		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode'] ";
		public static final String warningMessage_name = "@xpath=//div[text()='Name'] ";
		public static final String warningMessage_vendor = "@xpath=//div[text()='Vendor/Model'] ";
		public static final String servicetypetextfield = "@xpath=(//div[div[label[text()='Service Type']]]//input)[1]";
		public static final String devicename = "@xpath=//input[@id='deviceName']";
		
		public static final String existingdevicegrid1 = "@xpath=//div[text()='Device']/parent::div/following-sibling::table";
		public static final String deletealertbutton="@xpath=//button[@class='btn btn-danger']";


	}
}
