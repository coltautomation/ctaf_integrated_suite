package pageObjects.aptObjects;

import baseClasses.SeleniumUtils;

public class APT_LoginObj extends SeleniumUtils{		
   public static class APT_login_1
			{
				public static final String Username="@xpath=//input[@placeholder='Username']";
				public static final String Password="@xpath=//input[@placeholder='Password']";
				public static final String Loginbutton="@xpath=//button[text()='Login']";
			}
   public static class APT_NonVoiceService
			{
				public static final String Username="@xpath=//input[@placeholder='Username']";
				public static final String Password="@xpath=//input[@placeholder='Password']";
				public static final String Loginbutton="@xpath=//button[text()='Login']";
			}
			
	public static class APT_VoiceService
			{
				public static final String Username="@xpath=//input[@placeholder='Username']";
				public static final String Password="@xpath=//input[@placeholder='Password']";
				public static final String Loginbutton="@xpath=//button[text()='Login']";
			}
		

		public static class APT_reset
		{
			public static final String resetButton="@xpath=//input[@type='reset']";
	        public static final String APT_forgotpassword="@xpath=//a[contains(text(),'Forgot your Password?')]";
			//public static final String resetButton="@xpath=//a[contains(text(),'Forgot your Password?')]";
			
		}
		public static class APT_editprofile
		{
		    public static final String FirstName="@name=user.givenName";
		    public static final String Surname="@name=user.lastName";
		    public static final String Password="@name=user.password";
		    public static final String ConfirmPassword="@name=confirmPassword";
		    public static final String PostalAddress="@name=user.postalAddress";
		    public static final String Phone="@name=user.telephoneNumber";
		    public static final String Email="@name=user.email";
		    public static final String OkButton="@xpath=//input[@type='submit' and @value='OK']";
		    public static final String CancelButton="@xpath=//input[@type='submit' and @value='Cancel']";
		}
	public static class APT_logout
	{
		public static final String resetButton="@xpath=//a[contains(text(),'Forgot your Password?')]";
		public static final String Logout_Button="@xpath=//a[text()='Logout']";
	}
			
}
