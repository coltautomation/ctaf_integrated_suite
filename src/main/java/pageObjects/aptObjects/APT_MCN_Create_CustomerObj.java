package pageObjects.aptObjects;

public class APT_MCN_Create_CustomerObj {
	public static class CreateCustomer {
		public static final String ManageCustomerServiceLink = "@xpath=//b[contains(text(),'MANAGE CUSTOMER')]";

		public static final String SearchCustomerLink = "@xpath=//li[contains(text(),'Search Customer')]";
		public static final String CreateCustomerLink = "@xpath=//li[contains(text(),'Create Customer')]";

		// warning message create customer page --="@xpath=
		public static final String name_validationmsg = "@xpath=//div[text()='Legal Customer Name']";
		public static final String country_validationmsg = "@xpath=//div[text()='Country']";
		public static final String OCN_validationmsg = "@xpath=//div[text()='OCN']";
		public static final String type_validationmsg = "@xpath=//div[text()='Type']";
		public static final String email_Validationmsg = "@xpath=//div[text()='Email']";

		public static final String Name = "@xpath=//input[@id='name']";
		public static final String Name_Textfield = "@xpath=//label[text()='Name']";
		public static final String Nameisrequired_Text = "@xpath=//div[contains(text(),'Name')]";
		public static final String MainDomain = "@xpath=//input[@id='mainDomain']";
		public static final String MainDomain_Textfield = "@xpath=//label[contains(text(),'Main Domain')]";
		public static final String MainDomainisrequired_Text = "@xpath=//div[contains(text(),'Main Domain')]";
		public static final String CountrySelect = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String CountrySelectold = "@xpath=//select[@id='country_Id']";
		public static final String Country_Textfield = "@xpath=//label[contains(text(),'Country')]";
		public static final String Countryisrequired_Text = "@xpath=//div[contains(text(),'Country')]";
		public static final String OCN = "@xpath=//input[@id='ocn']";
		public static final String OCN_Textfield = "@xpath=//label[contains(text(),'OCN')]";
		public static final String OCNisrequired_Text = "@xpath=//div[contains(text(),'OCN')]";
		public static final String Reference = "@xpath=//input[@id='reference']";
		public static final String Search_Button = "@xpath=//button/span[contains(text(),'Search')]";
		public static final String Reference_Textfield = "@xpath=//label[contains(text(),'Reference')]";
		public static final String TechnicalContactName = "@xpath=//input[@id='technicalContactName']";
		public static final String TechnicalContactName_Textfield = "@xpath=//label[contains(text(),'Technical Contact Name')]";
		public static final String TypeSelect = "@xpath=//div[label[contains(text(),'Type')]]//div[@class='react-dropdown-select-clear css-w49n55-ClearComponent e11qlq5e0']";
		public static final String Type_Textfield = "@xpath=//label[contains(text(),'Type')]";
		public static final String Typeisrequired_Text = "@xpath=//div[contains(text(),'Name')]";
		public static final String Email = "@xpath=//input[@id='email']";
		public static final String EmailLabelname = "@xpath=//label[text()='Email']";
		public static final String Email_Textfield = "@xpath=//label[contains(text(),'Email')]";
		public static final String Emailisrequired_Text = "@xpath=//div[contains(text(),'Email')]";
		public static final String Phone = "@xpath=//input[@id='phone']";
		public static final String Phone_Textfield = "@xpath=//label[contains(text(),'Phone')]";
		public static final String Fax = "@xpath=//input[@id='fax']";
		public static final String Fax_Textfield = "@xpath=//label[contains(text(),'Fax')]";
		public static final String EnableDedicatedPortalCheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";
		public static final String dedicatedPortalDropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input[@placeholder='Select ...']";
		public static final String EnableDedicatedPortal_TextField = "@xpath=//label[contains(text(),'Enable Dedicated Portal')]";
		public static final String DedicatePortal_Text = "@xpath=//body//div[4]//div[4]//div[1]//label[1]";
		public static final String DedicatePortal_Select = "@xpath=//select[@id='dedicatedPortal']";
		public static final String OkButton_CreateCustomer = "@xpath=//span[contains(text(),'OK')]";
		public static final String CancelButton_CreateCustomer = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String ClearButton_CreateCustomer = "@xpath=//span[contains(text(),'Clear')]";
		public static final String CustomerCreatedSuccessfullyMessage_Text = "@xpath=//div[text()='Customer Created successfully']";
		// public static final String !-- --="@xpath=public static final String
		// CustomerCreatedSuccessfullyMessage_Text="@xpath=//div[contains(text(),'Customer
		// Created successfully')]";

		public static final String SearchCustomerResult_Actionmenu = "@xpath=//button[@id='dropdown-basic-button']";
		public static final String SearchResultRowsRadioButton = "@xpath=//span[@class='ag-selection-checkbox']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String View_Link = "@xpath=//a[text()='View']";
		public static final String Order_Service_Action = "@xpath=(//button[@id='dropdown-basic-button'])[4]";
		public static final String Supply_Link = "@xpath=//a[contains(text(),'Supply')]";
		public static final String SupplyServicetoCustomer_Name = "@xpath=//input[@id='customerName']";
		public static final String SupplyServicetoCustomer_ChooseACustomer = "@xpath=//div[label[text()='Choose a customer']]//input";
		public static final String OK_Button = "@xpath=//span[contains(text(),'Ok')]";
		public static final String SubscriberPanel = "@xpath=//div[text()='Subscribes']/parent::div";
		public static final String order_viewLink = "@xpath=//a[contains(text(),'View')]";
		public static final String alertPopupForviewLink_underOrderpanel = "@xpath=//div[@class='modal-content']";
		public static final String alertmsgForviewlink_underOrderPanel = "@xpath=//div[@class='modal-body']";
		public static final String xbuttonForviewlink_underOrderPanel = "@xpath=//span[contains(text(),'�')]";

		public static final String viewServicepage_OrderPanel = "@xpath=//div[text()='Order']";
		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";
		public static final String viewService_deletLink = "@xpath=//a[contains(text(),'Delete')]";
		public static final String viweServicepage_deleteButton = "@xpath=//button[@class='btn btn-danger']";
		public static final String service_deleteSuccessMessage = "@xpath=//span[contains(text(),'Service successfully deleted.')]";

		public static final String actionbutton_orderDeletion = "@xpath=//button[@id='dropdown-basic-button']";

		public static final String SupplyHeadertab = "@xpath=//div[div[text()='Supplies']]";
		public static final String suppliesPanel_customerName = "@xpath=//div[@col-id='customer'][@role='gridcell']";
		public static final String suppliesPanel_orderNumber = "@xpath=//div[@col-id='order'][@role='gridcell']";
		public static final String suppliesPanel_ServiceId = "@xpath=//div[@col-id='service'][@role='gridcell']";
		public static final String suppliesPanel_statusColmn = "@xpath=//div[text()='Supplies']/parent::div//following-sibling::div//div[@col-id='status'][@role='gridcell']";
		public static final String suppliesPanel_serviceType = "@xpath=//div[text()='Supplies']/parent::div//following-sibling::div//div[@col-id='serviceType'][@role='gridcell']";
		public static final String suppliesPanel_syncStatus = "@xpath=//div[text()='Supplies']/parent::div//following-sibling::div//div[@col-id='syncStatus'][@role='gridcell']";

		public static final String subscribesPanel_CustomerColumnValue = "@xpath=//div[text()='Subscribes']/parent::div//following-sibling::div//div[@col-id='customer'][@role='gridcell']";
		public static final String subscribesPanel_orderClumnValue = "@xpath=//div[text()='Subscribes']/parent::div//following-sibling::div//div[@col-id='order'][@role='gridcell']";
		public static final String subscribesPanel_serviceIdColumnValue = "@xpath=//div[text()='Subscribes']/parent::div//following-sibling::div//div[@col-id='service'][@role='gridcell']";
		public static final String subscribesPanel_ServiceTypeColumnValue = "@xpath=//div[text()='Subscribes']/parent::div//following-sibling::div//div[@col-id='serviceType'][@role='gridcell']";
		public static final String subscribesPanel_StatusColumnValue = "@xpath=//div[text()='Subscribes']/parent::div//following-sibling::div//div[@col-id='status'][@role='gridcell']";
		public static final String subscribesPanel_syncStatusColumnValue = "@xpath=//div[text()='Subscribes']/parent::div//following-sibling::div//div[@col-id='syncStatus'][@role='gridcell']";

		public static final String viewServicePage_customerDetailsPanel = "@xpath=//div[text()='Customer Details']/parent::div";

		public static final String CreatedCustomer_LegalCustomerName_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[1]";
		public static final String CreatedCustomer_MainDomain_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[2]";
		public static final String CreatedCustomer_Country_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[3]";
		public static final String CreatedCustomer_OCN_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[4]";
		public static final String CreatedCustomer_Reference_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[5]";
		public static final String CreatedCustomer_Type_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[6]";
		public static final String CreatedCustomer_TechnicalContactName_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[7]";
		public static final String CreatedCustomer_Email_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[8]";
		public static final String CreatedCustomer_Phone_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[9]";
		public static final String CreatedCustomer_Fax_Value = "@xpath=(//div[@class='position-relative form-group']/div[2])[10]";

		public static final String FirstRowTexts = "@xpath=//*[@id='root']/div/div/main/div[2]/div[2]/div[4]/div/div/div/div[2]/div/div[1]/div/div[3]/div[2]/div/div/div[1]";
		public static final String CustomerDetails_Action = "@xpath=(//button[@id='dropdown-basic-button'])[1]";
		public static final String EditCustomerLink = "@xpath=//a[contains(text(),'Edit')]";
		public static final String OkButton_EditCustomer = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String CancelButton_EditCustomer = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String BackButton_EditCustomer = "@xpath=//span[contains(text(),'Back')]";
		public static final String DeleteCustomerLink = "@xpath=//a[contains(text(),'Delete')]";
		public static final String DeleteButton_DeleteCustomer = "@xpath=//button[@class='btn btn-danger']";
		public static final String ManageServiceLink = "@xpath=//a[contains(text(),'Manage Service')]";

		public static final String Name_EditCustomer = "@xpath=//input[@id='name']";
		public static final String domain_EditCustomer = "@xpath=//input[@id='mainDomain']";
		public static final String country_EditCustomer = "@xpath=//select[@id='country_Id']";
		public static final String ocn_EditCustomer = "@xpath=//input[@id='ocn']";
		public static final String reference_EditCustomer = "@xpath=//input[@id='reference']";
		public static final String Technicalname_EditCustomer = "@xpath=//input[@id='techinicalContactName']";
		public static final String type_EditCustomer = "@xpath=//select[@id='type']";
		public static final String Email_EditCustomer = "@xpath=//input[@id='email']";
		public static final String phone_EditCustomer = "@xpath=//input[@id='phone']";
		public static final String fax_EditCustomer = "@xpath=//input[@id='fax']";
		public static final String enablededicatedPortal_EditCustomer = "@xpath=//div[label[contains(text(),'Enable Dedicated Portal')]]//input";
		public static final String EditCustomerTabname_EditCustomer = "@xpath=//div[contains(text(),'Edit Customer')]";

		public static final String Header_name = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String Header_Domain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";
		public static final String Header_Country = "@xpath=//div[div[label[text()='Country']]]/div[2]";
		public static final String Header_OCN = "@xpath=//div[div[label[text()='OCN']]]/div[2]";
		public static final String Header_Reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";
		public static final String Header_TechnicalContact = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String Header_TypeToBeSelected = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String Header_Email = "@xpath=//div[div[label[text()='Email']]]/div[2]";
		public static final String Header_Phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";
		public static final String Header_Fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";
		public static final String Header_Portal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";

		public static final String CountrySelect_Build4 = "@xpath=(//input[@placeholder='Select...'])[1]";
		public static final String Referance_Build4 = "@xpath=//input[@id='reference']";
		public static final String TechinicalContactName_Build4 = "@xpath=//input[@id='techinicalContactName']";
		public static final String TypeSelect_Build4 = "@xpath=//div[label[contains(text(),'Type')]]//input";
		public static final String DedicatePortal_Select_Build4 = "@xpath=///div[label[text()='Dedicated Portal']]//input";
		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String viewUser_userNameLabelName = "@xpath=//b[contains(text(),'User Name')]";
		public static final String Users_Action = "@xpath=//div[div[text()='Users']]//button[text()='Action']";
		public static final String Users_Action1 = "@xpath=//div[@class='position-relative form-group']//button[@id='dropdown-basic-button']";
		public static final String Users_Action3 = "@xpath=//div[div[text()='Users']]//div//button[text()='Action']";
		public static final String EditUserLink = "@xpath=//a[contains(text(),'Edit')]";
		public static final String ViewuserLink = "@xpath=//a[text()='View']";
		public static final String viewUser_fetchValuesForAddedUser = "@xpath=//div[label[b[text()='value']]]//following-sibling::div[@class='customLabelValue form-label']";
		public static final String viewUser_fetchValuesForAddedUser1 = "@xpath=//div[label[b[text()='";
		public static final String viewUser_fetchValuesForAddedUser2 = "']]]//following-sibling::div[@class='customLabelValue form-label']";
		
		public static final String backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String DeleteUserLink = "@xpath=//a[text()='Delete']";
		public static final String DeleteButton_DeleteUser = "@xpath=//button[@class='btn btn-danger']";
		public static final String FirstRowRadioButtonInUsers = "@xpath=//span[@class='ag-selection-checkbox']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String FirstRowRadioButtonInOrderService = "@xpath=(//span[@class='ag-selection-checkbox']//span[@class='ag-icon ag-icon-checkbox-unchecked'])[2]";
		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";
		public static final String AddUserLink = "@xpath=//a[contains(text(),'Add')]";
		public static final String UserName = "@xpath=//input[@id='userName']";
		public static final String UserName_Text = "@xpath=//label[contains(text(),'User Name')]";
		public static final String FirstName = "@xpath=//input[@id='firstName']";
		public static final String FirstName_Text = "@xpath=//label[contains(text(),'First Name')]";
		public static final String SurName = "@xpath=//input[@id='surname']";
		public static final String SurName_Text = "@xpath=//label[contains(text(),'Surname')]";
		public static final String PostalAddress = "@xpath=//textarea[contains(@name,'postalAddress')]";
		public static final String PostalAddress_Text = "@xpath=//label[contains(text(),'Postal Address')]";
		public static final String IPGuardianAccountGroup = "@xpath=//input[@id='ipGuardianAccountGrp']";
		public static final String IPGuardianAccountGroup_Text = "@xpath=//label[contains(text(),'IPGuardian Account Group')]";
		public static final String ColtOnlineUser = "@xpath=//input[@id='onlineUser']";
		public static final String ColtOnlineUser_Text = "@xpath=//label[contains(text(),'Colt Online User')]";
		public static final String Password_Text = "@xpath=//label[contains(text(),'Password')]";
		public static final String Password_Textfield = "@xpath=//input[@id='password']";
		public static final String GeneratePasswordLink = "@xpath=//span[text()='Generate Password']";
		public static final String GeneratePasswordLink_Text = "@xpath=//span[@class='badge-success badge badge-secondary']";
		public static final String RolesSelect = "@xpath=//div[contains(@class,'div-border container')]/form/div[4]/div[1]/div[1]/div[1]/div[1]";
		public static final String Roles_Text = "@xpath=//label[contains(text(),'Roles')]";
		public static final String Roles_Texfield = "@xpath=//input[@placeholder='Select...' and @xpath='1']";
		public static final String HideRouterToolsIPv6Commands_CiscoSelect2 = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//input";
		public static final String HideRouterToolsIPv6Commands_CiscoSelect = "@xpath=//div[4]//div[2]//div[1]//div[1]//div[1]//div[1]//div[1]";

		public static final String HideRouterToolsIPv6CommandsCisco_Text = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]";
		public static final String HideRouterToolsIPv4Commands_HuiwaiSelect = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawai)']]//input";
		public static final String HideRouterToolsIPv4CommandsHuiwai_Text = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawai)']]";
		public static final String HideRouterToolsIPv4Commands_CiscoSelect = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//input";
		public static final String HideRouterToolsIPv4CommandsCisco_Text = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]";
		public static final String HideServicesSelect = "@xpath=//div[label[text()='Hide Services']]//input";
		public static final String HideService_Text = "@xpath=//div[label[text()='Hide Services']]";
		public static final String HideSiteOrderSelect = "@xpath=//div[label[text()='Hide Site Order']]//input";
		public static final String HideSiteOrder_Text = "@xpath=//div[label[text()='Hide Site Order']]";
		public static final String CrealAllButton = "@xpath=//button[contains(text(),'Clear all')]";
		public static final String OutSideOfDropdown = "@xpath=//div[@class='col']";
		public static final String OkButton_AddUser = "@xpath=//span[text()='OK']";
		public static final String CancelButton_AddUser = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String BackButton_AddCustomer = "@xpath=//span[contains(text(),'Back')]";

		public static final String EdituserTabname_EditUser = "@xpath=//p[text()='User']";

		public static final String NoRowsToShow_ResultText = "@xpath=//div[contains(text(),'Total Record : ') and text()='0']";

		public static final String AddedUser_LoginUserName_Value = "@xpath=(//div[div[@role='gridcell']]//div[2])[1]";
		public static final String AddedUser_Name_Value = "@xpath=(//div[div[@role='gridcell']]//div[3])[1]";
		public static final String AddedUser_Email_Value = "@xpath=(//div[div[@role='gridcell']]//div[4])[1]";
		public static final String AddedUser_Roles_Value = "@xpath=(//div[div[@role='gridcell']]//div[5])[1]";
		public static final String AddedUser_Resources_Value = "@xpath=(//div[div[@role='gridcell']]//div[6])[1]";
		public static final String AddedUser_Address_Value = "@xpath=(//div[div[@role='gridcell']]//div[7])[1]";

		public static final String selectUser = "@xpath=//div[div[text()='Users']]//following-sibling::div//div[text()='value']";
		public static final String selectUser1 = "@xpath=//div[div[text()='Users']]//following-sibling::div//div[text()='";
		public static final String selectUser2 = "']";

		public static final String UserNameisrequired_Text = "@xpath=//div[contains(text(),'User Name')]";
		public static final String FirstNameisrequired_Text = "@xpath=//div[contains(text(),'First Name')]";
		public static final String Surnameisrequired_Text = "@xpath=//div[contains(text(),'Surname')]";
		public static final String PostalAddressisrequired_Text = "@xpath=//div[contains(text(),'Postal Address')]";
		public static final String Emailisrequired_AddUser_Text = "@xpath=//div[contains(text(),'Email')]";
		public static final String Phoneisrequired_Text = "@xpath=//div[contains(text(),'Phone')]";
		public static final String Passwordisrequired_Text = "@xpath=//div[contains(text(),'Password')]";

		public static final String  AlertForServiceCreationSuccessMessage="@xpath=//div[@role='alert']/span";
		public static final String serivceAlert = "@xpath=//div[@role='alert']";

		//public static final String!--===
		//SUPLY SERVICE===--="@xpath=

		public static final String Orders_Services_Action = "@xpath=(//button[@id='dropdown-basic-button'])[4]";
		public static final String AddOrder_Link = "@xpath=//a[@class='dropdown-item']";

		//public static final String!--
		//create order/
		//service page--="@xpath=
		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		public static final String  choosecustomerdropdown="@xpath=//div[label[text()='Customer']]//input";
		public static final String  customerdropdown="@xpath=//div[label[text()='Customer']]/div";

		//Enter data for Order/
		//ContractNumber and RFI/RFQ/IPVoiceLinenumber--="@xpath=
		public static final String  ordercontractnumber="@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";

		public static final String  selectorderswitch="@xpath=(//div[div[label[text()='Select Order']]]//div[@class='react-switch-bg'])[1]";
		public static final String  selectorderswitch2="@xpath=//div[@class='div-border div-margin marginTop-30px container']//div[1]//div[1]//div[2]//div[1]//div[1]//div[1]";

		public static final String  createorderswitch="@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String  rfireqiptextfield="@xpath=//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String  existingorderdropdown="@xpath=//div[label[text()='Order/Contract Number(Parent SID)']]//input[@placeholder='Select ...']";
		public static final String  existingorderdropdownvalue="@xpath=//span[text()='12345']";
		public static final String  createorderbutton="@xpath=//button[@class='btn btnSquar btnWitdh undefined btn-secondary   ']";
		public static final String  newordertextfield="@xpath=//input[@id='orderName']";
		public static final String  newrfireqtextfield="@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";

		//public static final String!--
		//Select Type
		//and Subtype--="@xpath=
		public static final String  servicetypetextfield="@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String  serviceSubtypeInputfield="@xpath=//div[div[label[text()='Service Subtype:']]]//input";
		public static final String  networkconfigurationinputfield="@xpath=(//div[div[label[text()='Network Configuration']]]//input)[1]";
		public static final String  nextbutton="@xpath=//span[contains(text(),'Next')]";

		public static final String  servicetypevalue="@xpath=//div[label[contains(text(),'Service Type')]]/div";
		public static final String  networkconfigurationvalue="@xpath=//div[label[contains(text(),'Network Configuration')]]/div";
		public static final String  serviceidentificationerror="@xpath=//div[text()='Service Identification']";
		public static final String  billingtypeerror="@xpath=//div[text()='Billing Type']";

		public static final String  serviceidentificationtextfield="@xpath=//input[@id='serviceName']";
		public static final String  billingtypeinputfield="@xpath=//div[label[text()='Billing Type']]//input";
		public static final String  billingType_selectTag="@xpath=//select[@id='billingType']";
		public static final String  terminationinputfield="@xpath=//input[contains(@name,'terminationDate')]";
		public static final String  emailtextfield="@xpath=//div[label[contains(text(),'Email')]]//input";
		public static final String  phonecontacttextfield="@xpath=//input[@id='phoneContactText']";
		public static final String  remarktextfield="@xpath=//textarea[@name='serviceRmark']";

		public static final String  topologydropdownerror="@xpath=//div[div[label[contains(text(),'Topology')]]]/div[2]";

		//public static final String!--
		//Management Options--="@xpath=
		public static final String  managedservicecheckbox="@xpath=//div[label[contains(text(),'Managed Service')]]//input[@type='checkbox']";
		public static final String  performancereportingcheckbox="@xpath=//div[label[contains(text(),'Performance Reporting')]]//input[@type='checkbox']";
		public static final String  ipguardianchecbox="@xpath=//div[label[contains(text(),'IP Guardian')]]//input[@type='checkbox']";
		public static final String  routerconfigurationviewipv4checkbox="@xpath=//div[label[contains(text(),'Router Configuration View IPv4')]]//input[@type='checkbox']";
		public static final String  routerconfigurationviewipv6checkbox="@xpath=//div[label[contains(text(),'Router Configuration View IPv6')]]//input[@type='checkbox']";
		public static final String  snmpNotificationcheckbox="@xpath=//div[label[contains(text(),'SNMP Notification')]]//input[@type='checkbox']";
		public static final String  deliverychannelinput="@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";
		public static final String  deliveryChannel_selectTag="@xpath=//select[@id='deliveryChannel']";
		public static final String  traptargetAddress="@xpath=//div[label[contains(text(),'Trap Target Address')]]//input";

		//public static final String!--
		//Configuration Options--="@xpath=

		public static final String  routerbasedfirewalcheckbox="@xpath=//div[label[contains(text(),'Router Based Firewall')]]//input[@type='checkbox']";
		public static final String  Qoscheckbox="@xpath=//div[label[contains(text(),'Qos')]]//input[@type='checkbox']";
		public static final String  cancelbutton="@xpath=//span[contains(text(),'Cancel')]";

		//public static final String!--
		//view service page--="@xpath=
		public static final String  servicecreationsuccessmsg="@xpath=//span[contains(text(),'Service successfully created')]";

		//public static final String!--
		//supply Table--="@xpath=
		public static final String  selectvalueUnderChooseAcustomerDropdown="@xpath=//div[text()='value']";
		public static final String  selectvalueUnderChooseAcustomerDropdown1="@xpath=//div[text()='";
		public static final String  selectvalueUnderChooseAcustomerDropdown2="']";
	
		public static final String  selectRowUnderSupplyPanel="@xpath=(//div[div[@class='heading-green-row row']//div[text()='Supplies']]//div[text()='value'])[1]";
		public static final String  selectRowUnderSupplyPanel1="@xpath=(//div[div[@class='heading-green-row row']//div[text()='Supplies']]//div[text()='";
		public static final String  selectRowUnderSupplyPanel2="'])[1]";

		public static final String  suppliesTable_ActionDropdown="@xpath=//div[text()='Supplies']/parent::div//button[text()='Action']";
		public static final String  suppliesTable_removeLink="@xpath=//a[text()='Remove']";

		//public static final String!--
		//view Customer
		//Page Order Panel--="@xpath=
		public static final String  selectRowUnderOrderPanel="@xpath=(//div[div[@class='heading-green-row row']//div[text()='Order / Service']]//div[text()='value'])[1]";
		public static final String  selectRowUnderOrderPanel1="@xpath=(//div[div[@class='heading-green-row row']//div[text()='Order / Service']]//div[text()='";
		public static final String  selectRowUnderOrderPanel2="'])[1]";
		public static final String  breadCrumbNav="@xpath=(//li[@class='breadcrumb-item']//a)[2]";
		//public static final String!--
		//Subscribe panel--="@xpath=
		public static final String  subscribePanel_totalPage="@xpath=//div[div[text()='Subscribes']]/following-sibling::div//span[@ref='lbTotal']";
		public static final String  subscribePanel_currentPage="@xpath=//div[div[text()='Subscribes']]/following-sibling::div//span[@ref='lbCurrent']";
		public static final String  subscribePanel_nextPage="@xpath=//div[div[text()='Subscribes']]/following-sibling::div//button[@ref='btNext']";
		public static final String  subscribePanel_ActionDropdown="@xpath=//div[div[text()='Subscribes']]//button[text()='Action']";
		public static final String  subscribePanel_viewlink="@xpath=//a[text()='View']";

		//public static final String!--
		//Select Dropdown--="@xpath=
		public static final String  legalcustomerName_labelName="@xpath=//label[text()='Legal Customer Name']";
		public static final String  roleDropdown_available="@xpath=//select[@id='availableRoles']//option";
		public static final String  roleDropdown_addButton="@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";
		public static final String  roleDropdown_removeButton="@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='value']";
		public static final String  roleDropdown_selectedValues="@xpath=//select[@id='selectedRoles']//option";

		public static final String  hideRouterToolIPv4_Huawei_available="@xpath=//select[@id='availableIPV4CommandHuawei']//option";
		public static final String  hideRouterToolIPv4__Huawei_addButton="@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";

		public static final String  hideRouterToolIPv4_Huawei_removeButton="@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";
		public static final String  hideRouterToolIpv4_Huawei_selectedvalues="@xpath=//select[@id='selectedIPV4CommandHuawei']//option";

		public static final String  hideRouterToolIPv4_Cisco_Available="@xpath=//select[@id='availableIPV4CommandCisco']//option";
		public static final String  hideRouterToolIPv4_Cisco_addButton="@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";
		
		public static final String  hideRouterToolIPv4_Cisco_removeButton="@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";
		public static final String  hideRouterToolIPv6_Cisco_removeButton="@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='value']";
		public static final String  hideRouterToolIpv4_Cisco_selectedvalues="@xpath=//select[@id='selectedIPV4CommandCisco']//option";
		public static final String  hidenRouterIPv6_cisco="@xpath=//select[@id='selectedIPV6Command']//option";

		public static final String  HideService_Available="@xpath=//select[@id='availableHideService']//option";
		public static final String  HideService_addButton="@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";
		public static final String  HideService_removeButton="@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";
		public static final String  HideServicesDropdown_selectedValues="@xpath=//select[@id='selectedHideService']//option";

		public static final String  HideSiteOrder_Available="@xpath=//select[@id='availableHideSiteOrder']//option";
		public static final String hideSiteOrder_addbutton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";
		public static final String  hideSiteOrder_removeButton="@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";
		public static final String HideSiteOrderDropdown_selectedValues = "@xpath=//select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath=//select[@id='availableIPV6Command']//option";
		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";
		
		//New Locator 
		public static final String closeX = "@xpath=//div[div[@class='modal-body']]//div[text()='�']";
	}

}
