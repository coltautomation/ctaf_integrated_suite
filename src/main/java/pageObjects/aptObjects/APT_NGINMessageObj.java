package pageObjects.aptObjects;

public class APT_NGINMessageObj {

	public static class NGINMessage

	{
		public static final String SelectExistingSAN = "@xpath=//div[text()='"; // modify
		public static final String SelectExistingSAN1 = "']/preceding-sibling::div//span[contains(@class,'unchecked')]"; // modify

		public static final String managecoltnetworklink = "@xpath=(//div[@class='ant-menu-submenu-title']/span/b)[2]";
		public static final String san_searchbutton = "@xpath=//button[@type='submit']//span";
		public static final String ebodyNoSearch = "@xpath=//div[@ref='eBodyContainer']";

		// !-- NGIN Message --="@xpath=
		public static final String customernamecolumn = "@xpath=//span[@role='columnheader'][text()='Customer Name']";
		public static final String searchsan_actiondropdown = "@xpath=//button[text()='Action']";
		public static final String nginmessageslink = "@xpath=//li[text()='NGIN Messages']";
		public static final String nginmessageheader = "@xpath=//div[contains(text(),'Manage Messages - Messages Search')]";
		public static final String nginmsg_sannumber = "@xpath=//div[contains(text(),'Manage Messages')]/parent::div/following-sibling::form//div//input[@id='msgsan']";
		public static final String nginmsg_customername_textfield = "@xpath=//label[text()='Customer Name ']/following-sibling::input";
		public static final String nginmsg_frcnumbercolumn = "@xpath=//span[@role='columnheader'][text()='FRC Number ']";
		public static final String nginmsg_namecolumn = "@xpath=//span[@role='columnheader'][text()='Name']";
		public static final String editmessageheader = "@xpath=//div[text()='Edit Message']";
		public static final String editmessage_customername = "@xpath=//label[text()='Customer Name']/following-sibling::input";
		public static final String editmessage_countrycode = "@xpath=//label[text()='Country Code']/following-sibling::input";
		public static final String editmessage_sanreference = "@xpath=//label[text()='SAN Reference']/following-sibling::input";
		public static final String editmessage_name = "@xpath=//label[text()='Name']/following-sibling::input";
		public static final String editmessage_description = "@xpath=//label[text()='Description']/following-sibling::input";
		public static final String editmessage_bodyfield = "@xpath=//label[text()='Body']/following-sibling::input";
		public static final String editmessage_prefixfield = "@xpath=//label[text()='Prefix']/following-sibling::input";
		public static final String editmessage_repetitionsfield = "@xpath=//label[text()='Repetitions']/following-sibling::input";
		public static final String editmessage_durationfield = "@xpath=//label[text()='Duration']/following-sibling::input";
		public static final String editmessage_suffixfield = "@xpath=//label[text()='Suffix']/following-sibling::input";
		public static final String editmessage_startnetworkcharge = "@xpath=//label[text()='Start Network Charge']/following-sibling::div//input";
		public static final String editmessage_okbutton = "@xpath=//button[@type='button']//span[text()='Ok']";
		public static final String editmessage_successmessage = "@xpath=//div[@role='alert']//span";

		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// public static final String !-- success Message --="@xpath=
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

	}

}
