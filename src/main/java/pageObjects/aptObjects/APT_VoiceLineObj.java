package pageObjects.aptObjects;

public class APT_VoiceLineObj {

	public static class APT_VoiceLine {

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";
		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@name='customerName']";
		public static final String chooseCustomerdropdown = "@xpath= //div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath= //button//Span[text()='Next']";

		public static final String mcslink = "@xpath= //a[contains(text(),'Manage Customer')]";
		public static final String createcustomerlink = "@xpath= //li[text()='Create Customer']";
		public static final String createorderlink = "@xpath= //li[text()='Create Order/Service']";

		// !-- Create Customer Page --="@xpath=
		public static final String createcustomer_header = "@xpath= //div[@class='heading-green-row row']//p";
		public static final String nametextfield = "@xpath= //input[@id='name']";
		public static final String maindomaintextfield = "@xpath= //input[@id='mainDomain']";
		public static final String country = "@xpath= //div[label[contains(text(),'Country')]]//input";
		public static final String ocntextfield = "@xpath= //input[@id='ocn']";
		public static final String referencetextfield = "@xpath= //input[@id='reference']";
		public static final String technicalcontactnametextfield = "@xpath= //input[@id='techinicalContactName']";
		public static final String typedropdown = "@xpath= //div[label[contains(text(),'Type')]]//input";
		public static final String emailtextfield = "@xpath= //input[@id='email']";
		public static final String phonetextfield = "@xpath= //input[@id='phone']";
		public static final String faxtextfield = "@xpath= //input[@id='fax']";
		public static final String enablededicatedportalcheckbox = "@xpath= //input[@id='enabledDedicatedPortal']";
		public static final String dedicatedportaldropdown = "@xpath= //div[label[text()='Dedicated Portal']]//input";
		public static final String okbutton = "@xpath= //span[contains(text(),'OK')]";
		public static final String customercreationsuccessmsg = "@xpath= //div[@role='alert']//span";
		public static final String resetbutton = "@xpath= //span[text()='Reset']";
		public static final String clearbutton = "@xpath= //span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath= //label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath= //label[text()='Type']/parent::div//div[text()='�']";

		// !-- Verify warning messages --="@xpath=
		public static final String customernamewarngmsg = "@xpath= //input[@id='name']/parent::div//div";
		public static final String countrywarngmsg = "@xpath= //div[label[contains(text(),'Country')]]/following-sibling::div";
		public static final String ocnwarngmsg = "@xpath= //input[@id='ocn']/parent::div//div";
		public static final String typewarngmsg = "@xpath= //div[label[contains(text(),'Type')]]/following-sibling::div";
		public static final String emailwarngmsg = "@xpath= //input[@id='email']/parent::div//div";
		public static final String customer_createorderpage_warngmsg = "@xpath= //div[text()='Choose a customer']";
		public static final String sidwarngmsg = "@xpath= //input[@id='serviceIdentification']/parent::div//div";

		// !-- Verify Customer details panel --="@xpath=
		public static final String customerdetailsheader = "@xpath= //div[text()='Customer Details']";
		public static final String Name_Text = "@xpath= (//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath= //label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";
		public static final String MainDomain_Text = "@xpath= (//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath= //label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath= (//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath= //label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath= (//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath= //label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath= (//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath= //label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath= (//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath= //label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath= (//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath= //label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath= (//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath= //label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath= (//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath= //label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath= (//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath= //label[text()='Fax']/parent::div/parent::div//div[2]";

		// create order/service page --="@xpath=
		// public static final String nametextfield="@xpath=
		// //input[@id='customerSearch']";
		public static final String createordernametextfield = "@xpath= //input[@id='customerName']";
		public static final String choosecustomerdropdown = "@xpath= //div[label[text()='Customer']]//input";
		public static final String customerdropdown = "@xpath= //div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath= //span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath= //body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";
		public static final String leaglcustomername = "@xpath= //div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String maindomain = "@xpath= //div[div[label[text()='Main Domain']]]/div[2]";
		// public static final String country="@xpath=
		// //div[div[label[text()='Country']]]/div[2]";
		public static final String ocn = "@xpath= //div[div[label[text()='OCN']]]/div[2]";
		public static final String reference = "@xpath= //div[div[label[text()='Reference']]]/div[2]";
		public static final String type = "@xpath= //div[div[label[text()='Type']]]/div[2]";
		public static final String technicalcontactname = "@xpath= //div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String email = "@xpath= //div[div[label[text()='Email']]]/div[2]";
		public static final String phone = "@xpath= //div[div[label[text()='Phone']]]/div[2]";
		public static final String fax = "@xpath= //div[div[label[text()='Fax']]]/div[2]";
		public static final String dedicatedportal = "@xpath= //div[div[label[text()='Dedicated Portal']]]/div[2]";
		public static final String useractionbutton = "@xpath= //button[@id='dropdown-basic-button']";
		public static final String adduserbutton = "@xpath= //a[contains(text(),'Add')]";
		public static final String viewpage_backbutton = "@xpath= //span[text()='Back']";

		// Create Order/service panel in view customer page --="@xpath=
		public static final String CreateOrderHeader = "@xpath= //div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath= //div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorderswitch = "@xpath= (//div[@class='react-switch-bg'])[1]/following-sibling::div[@class='react-switch-handle']";
		public static final String createorderswitch = "@xpath= //form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath= //div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath= //label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath= //span[text()='12345']";
		public static final String createorderbutton = "@xpath= //button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath= //input[@id='orderName']";
		public static final String newrfireqtextfield = "@xpath= //div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderCreatedSuccessMsg = "@xpath= //div[@role='alert']//span[text()='Order created successfully']";

		public static final String order_contractnumber_warngmsg = "@xpath= //div[label[contains(text(),'Order/Contract Number(Parent SID)')]]/following-sibling::span";
		public static final String servicetype_warngmsg = "@xpath= //div[label[contains(text(),'Service Type')]]/following-sibling::span";
		public static final String createorder_header = "@xpath= //div[text()='Create Order / Service']";
		public static final String servicetypetextfield = "@xpath= //div[div[label[text()='Service Type']]]//input";
		public static final String networkconfigurationinputfield = "@xpath= //div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath= (//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String changeorder_cancelbutton = "@xpath= //span[contains(text(),'Cancel')]";

		// public static final String !-- Service Creation Form --="@xpath=
		public static final String createorderservice_header = "@xpath= //div//p[text()='Create Order / Service']";
		public static final String serviceidentificationtextfield = "@xpath= //input[@id='serviceIdentification']";
		public static final String servicetypevalue = "@xpath= //div[label[text()='Service Type']]/following-sibling::div";
		public static final String resellercode_textfield = "@xpath= //input[@id='resellerCode']";
		public static final String servicepage_thirdpartyinternet_checkbox = "@xpath= //input[@id='thirdPartyInternet']";
		public static final String thirdpartyinternet_checkbox = "@xpath= //input[@id='tgthirdpartyinternet']";
		// public static final String emailtextfield="@xpath=
		// //input[@id='email']";
		public static final String emailtextfieldvalue = "@xpath= //div[label[text()='Email']]//input";
		public static final String phonecontacttextfield = "@xpath= //input[@id='phoneContact']";
		public static final String phonecontacttextfieldvalue = "@xpath= //div[label[text()='Phone Contact']]//input";
		public static final String remarktextarea = "@xpath= //textarea[contains(@name,'remark')]";
		public static final String remarktextareavalue = "@xpath= //div[label[text()='Remark']]//textarea";
		public static final String packagefield = "@xpath= (//div[label[text()='Package']]//div/div)[1]";
		public static final String managedservice_checkbox = "@xpath= //label[text()='Managed Service']/parent::div//input";
		public static final String syslogevent_checkbox = "@xpath= //label[text()='Syslog Event View']/parent::div//input";
		public static final String servicestatusview_checkbox = "@xpath= //label[text()='Service Status View']/parent::div//input";
		public static final String routerconfigview_checkbox = "@xpath= //label[text()='Router Configuration View']/parent::div//input";
		public static final String performancereporting_checkbox = "@xpath= //label[text()='Performance Reporting']/parent::div//input";
		public static final String proactivenotification_checkbox = "@xpath= //label[text()='Pro-active Notification']/parent::div//input";
		public static final String dialuseradministration_checkbox = "@xpath= //label[text()='Dial User Administration']/parent::div//input";
		// public static final String cancelbutton="@xpath=
		// //span[contains(text(),'Cancel')]";
		public static final String servicecreationmessage = "@xpath= //div[@role='alert']//span";
		public static final String servicetype_value = "@xpath= //label[text()='Service Type']/following-sibling::div";
		public static final String emailaddarrow = "@xpath= //label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phoneaddarrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String emailremovearrow = "@xpath= (//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String phoneremovearrow = "@xpath= (//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String selectedemail = "@xpath= //select[@name='selectedEmail']//option";
		public static final String selectedphone = "@xpath= //select[@name='selectedPhoneContact']//option";
		public static final String notificationmanagementteam_dropdown = "@xpath= //label[text()='Notification Management Team']/parent::div//input";
		public static final String managesubnets_link = "@xpath= //div[@class='dropdown-menu show']//a[text()='Manage Subnets']";
		public static final String managesubnet_header = "@xpath= //div[@class='modal-header']//div[contains(text(),'Manage')]";
		public static final String managesubnet_successmsg = "@xpath= //div[@class='modal-content']//div[@role='alert']//span";
		public static final String spacename_column = "@xpath= //span[text()='Space Name']";
		public static final String blockname_column = "@xpath= //span[text()='Block Name']";
		public static final String subnetname_column = "@xpath= //span[text()='Subnet Name']";
		public static final String startaddress_column = "@xpath= //span[text()='Start Address']";
		public static final String size_column = "@xpath= //span[text()='Size']";
		public static final String closesymbol = "@xpath= //button[@type='button']//span[text()='�']";
		public static final String dumppage_header = "@xpath= //div[@class='modal-header']//div[contains(text(),'Service')]";
		public static final String serviceretrieved_text = "@xpath= //div[@class='div-margin row']//textarea[contains(text(),'Service retrieved')]";
		public static final String service_header = "@xpath= //label[text()='Service']";
		public static final String dumppage_text = "@xpath= //label[text()='Service']/following-sibling::textarea";

		public static final String managesubnetsipv6_link = "@xpath= //div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";
		public static final String shownewinfovistareport_link = "@xpath= //div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";
		public static final String dump_link = "@xpath= //div[@class='dropdown-menu show']//a[text()='Dump']";

		// public static final String cancelbutton="@xpath=
		// //button[@type='submit']//span[text()='Cancel']";
		public static final String cancelbutton = "@xpath= //span[text()='Cancel']";
		// Search order --="@xpath=
		public static final String searchorderlink = "@xpath= //li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath= //label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath= //span[text()='Search']";
		public static final String serviceradiobutton = "@xpath= //div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath= //div[@class='dropdown']//button";

		// -- order panel - view service page --="@xpath=
		public static final String orderactionbutton = "@xpath= //div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath= //div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath= //a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath= //label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath= //label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath= //label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath= //label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath= //div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath= //label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";
		public static final String changeorder_backbutton = "@xpath= //span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath= //span[contains(text(),'OK')]";
		public static final String orderpanelheader = "@xpath= //div[text()='Order']";
		public static final String editorderheader = "@xpath= //div[text()='Edit Order']";
		public static final String editorderno = "@xpath= //label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath= //label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath= (//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath= //div[text()='Change Order']";
		public static final String createorder_button = "@xpath= //button//span[text()='Create Order']";
		public static final String changeorder_dropdownlist = "@xpath= //label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";

		// public static final String !-- service panel --="@xpath=
		public static final String servicepanel_serviceidentificationvalue = "@xpath= //div[label[contains(text(),'Service Identification')]]/following-sibling::div";
		public static final String servicepanel_servicetypevalue = "@xpath= //div[label[contains(text(),'Service Type')]]/following-sibling::div";
		public static final String servicepanel_remarksvalue = "@xpath= //div[label[contains(text(),'Remark')]]/div";
		public static final String servicepanel_resellercode = "@xpath= //div[label[contains(text(),'Reseller Code')]]/following-sibling::div";
		public static final String servicepanel_email = "@xpath= //div[text()='Service']/parent::div/following-sibling::div[@class='row']//div[label[contains(text(),'Email')]]/following-sibling::div";
		public static final String servicepanel_phonecontact = "@xpath= //div[label[contains(text(),'Phone Contact')]]/following-sibling::div";
		public static final String servicepanel_thirdpartyinternet = "@xpath= //div[label[contains(text(),'3rd Party Internet')]]/following-sibling::div";
		public static final String servicepanel_header = "@xpath= //div[text()='Service']";
		public static final String serviceactiondropdown = "@xpath= //div[div[text()='Service']]//button";
		public static final String serviceupdate_successmsg = "@xpath= (//div[@role='alert'])[1]";
		public static final String manageLink = "@xpath= //div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String synchronizelink_servicepanel = "@xpath= //a[text()='Synchronize']";
		public static final String manageservice_header = "@xpath= //div[text()='Manage Service']";
		public static final String status_ordername = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String status_servicename = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String status_servicetype = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String status_servicedetails = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String status_currentstatus = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='currentStatus']";
		public static final String status_modificationtime = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='mTime']";
		public static final String statuslink = "@xpath= //div[text()='Status']/parent::div/parent::div//div[@role='gridcell']//a[text()='Status']";
		public static final String sync_ordername = "@xpath= //div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String sync_servicename = "@xpath= //div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String sync_servicetype = "@xpath= //div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String sync_servicedetails = "@xpath= //div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String sync_status = "@xpath= //div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='syncStatus']";
		public static final String synchronizelink = "@xpath= //div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell']//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath= //span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath= //div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath= (//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath= //label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath= //label[text()='Change Status']/parent::div//span";
		public static final String servicestatushistory = "@xpath= //div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath= //div[@role='alert']//span";
		public static final String successmsg = "@xpath= (//div[@role='alert']//span)[1]";

		// public static final String !-- Users panel in view service page
		// --="@xpath=

		public static final String LoginColumn = "@xpath= //div[@col-id='userName']";
		public static final String NameColumn = "@xpath= //div[@col-id='firstName']";
		public static final String EmailColumn = "@xpath= //div[@col-id='email']";
		public static final String RolesColumn = "@xpath= //div[@col-id='roles']";
		public static final String AddressColumn = "@xpath= //div[@col-id='postalAddress']";
		public static final String ResourcesColumn = "@xpath= //div[@col-id='0']";
		public static final String ExistingUsers = "@xpath= //div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String UserUnchecked = "@xpath= //div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String UserChecked = "@xpath= //div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// public static final String !-- New User creation in view service page
		// --="@xpath=

		// public static final String !-- Warning messages --="@xpath=
		public static final String warningmsg_username = "@xpath= //label[text()='User Name']/parent::div//div";
		public static final String warningmsg_firstname = "@xpath= //label[text()='First Name']/parent::div//div";
		public static final String warningmsg_surname = "@xpath= //label[text()='Surname']/parent::div//div";
		public static final String warningmsg_postaladdress = "@xpath= //label[text()='Postal Address']/parent::div/following-sibling::div";
		public static final String warningmsg_useremail = "@xpath= //label[text()='Email']/parent::div//div";
		public static final String warningmsg_userphone = "@xpath= //label[text()='Phone']/parent::div//div";
		public static final String warningmsg_userpassword = "@xpath= //label[text()='Password']/parent::div//div";

		public static final String UserActionDropdown = "@xpath= //div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";
		public static final String AddLink = "@xpath= //div[@class='dropdown-menu show']//a[text()='Add']";
		public static final String CreateUserHeader = "@xpath= //div[@class='heading-green-row row']//p";
		public static final String UserName = "@xpath= (//div[@class='position-relative form-group'])[1]//input[@id='userName']";
		public static final String FirstName = "@xpath= (//div[@class='position-relative form-group'])[2]//input[@id='firstName']";
		public static final String SurName = "@xpath= (//div[@class='position-relative form-group'])[3]//input[@id='surname']";
		public static final String PostalAddress = "@xpath= (//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";
		public static final String Email = "@xpath= (//div[@class='position-relative form-group'])[5]//input[@id='email']";
		public static final String Phone = "@xpath= (//div[@class='position-relative form-group'])[6]//input[@id='phone']";
		public static final String Password = "@xpath= (//div[@class='position-relative form-group'])[9]//input[@id='password']";
		public static final String GeneratePassword = "@xpath= //div//span[text()='Generate Password']";
		public static final String OkButton = "@xpath= //button[@type='submit']//span[text()='Ok']";
		public static final String edituser_header = "@xpath= //div[@class='heading-green-row row']//p";
		public static final String delete_alertpopup = "@xpath= //div[@class='modal-content']";
		public static final String delete_alertpopupASR = "@xpath= //div[@class='modal-content']//div[contains(text(),'Delete Device')]";
		public static final String deletebutton = "@xpath= //button[text()='Delete']";
		public static final String deletesuccessmsg = "@xpath= //div[@role='alert']//span";
		// public static final String userspanel_header="@xpath=
		// //div[@class='heading-green-row row']//div[text()='Users']";
		public static final String userspanel_header = "@xpath= //div[text()='Users']";

		public static final String usernamevalue = "@xpath= //b[text()='User Name']/parent::label/parent::div/following-sibling::div";
		public static final String firstnamevalue = "@xpath= //b[text()='First Name']/parent::label/parent::div/following-sibling::div";
		public static final String surnamevalue = "@xpath= //b[text()='Surname']/parent::label/parent::div/following-sibling::div";
		public static final String postaladdressvalue = "@xpath= //b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";
		public static final String emailvalue = "@xpath= //b[text()='Email']/parent::label/parent::div/following-sibling::div";
		public static final String phonevalue = "@xpath= //b[text()='Phone']/parent::label/parent::div/following-sibling::div";
		public static final String OK_button = "@xpath= //button[@type='submit']//span[text()='OK']";

		public static final String userdelete = "@xpath= //button[text()='Delete']";

		// public static final String !-- Select Dropdown --="@xpath=
		public static final String legalcustomerName_labelName = "@xpath= //label[text()='Legal Customer Name']";

		public static final String IPGuardianAccountGroup = "@xpath= //input[@id='ipGuardianAccountGrp']";
		public static final String IPGuardianAccountGroup_Text = "@xpath= //label[contains(text(),'IPGuardian Account Group')]";
		public static final String IPGuardianAccountGroup_viewpage = "@xpath= //div[div[label//b[contains(text(),'IPGuardian Account Group')]]]/div[2]";
		public static final String ColtOnlineUser = "@xpath= //input[@id='onlineUser']";
		public static final String ColtOnlineUser_Text = "@xpath= //label[contains(text(),'Colt Online User')]";
		public static final String coltonlineuser_viewpage = "@xpath= //div[div[label//b[contains(text(),'Colt Online User')]]]/div[2]";
		public static final String Password_Text = "@xpath= //label[contains(text(),'Password')]";
		public static final String Password_Textfield = "@xpath= //input[@id='password']";
		public static final String GeneratePasswordLink = "@xpath= //span[text()='Generate Password']";
		public static final String GeneratePasswordLink_Text = "@xpath= //span[@class='badge-success badge badge-secondary']";

		public static final String roleDropdown_available = "@xpath= //select[@id='availableRoles']//option";
		public static final String roleDropdown_addButton = "@xpath= //div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";
		public static final String roleDropdown_removeButton = "@xpath= //div[label[text()='Roles']]//following-sibling::div//span[text()='value']";
		public static final String roleDropdown_selectedValues = "@xpath= //select[@id='selectedRoles']//option";

		public static final String hideRouterToolIPv4_Huawei_available = "@xpath= //select[@id='availableIPV4CommandHuawei']//option";
		public static final String hideRouterToolIPv4__Huawei_addButton = "@xpath= //div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";
		public static final String hideRouterToolIPv4_Huawei_removeButton = "@xpath= //div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";
		public static final String hideRouterToolIpv4_Huawei_selectedvalues = "@xpath= //select[@id='selectedIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4_Cisco_Available = "@xpath= //select[@id='availableIPV4CommandCisco']//option";
		public static final String hideRouterToolIPv4_Cisco_addButton = "@xpath= //div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";
		public static final String hideRouterToolIPv4_Cisco_removeButton = "@xpath= //div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";
		public static final String hideRouterToolIpv4_Cisco_selectedvalues = "@xpath= //select[@id='selectedIPV4CommandCisco']//option";

		public static final String HideService_Available = "@xpath= //select[@id='availableHideService']//option";
		public static final String HideService_addButton = "@xpath= //div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";
		public static final String HideService_removeButton = "@xpath= //div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";
		public static final String HideServicesDropdown_selectedValues = "@xpath= //select[@id='selectedHideService']//option";

		public static final String HideSiteOrder_Available = "@xpath= //select[@id='availableHideSiteOrder']//option";
		public static final String hideSiteOrder_addbutton = "@xpath= //div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";
		public static final String hideSiteOrder_removeButton = "@xpath= //div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";
		public static final String HideSiteOrderDropdown_selectedValues = "@xpath= //select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath= //select[@id='availableIPV6Command']//option";
		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath= //div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath= //div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath= //div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath= //div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String selectValueUnderUserPanel_ViewSericePage = "@xpath= //div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		// !-- Management Options panel in view service page

		public static final String managementoptions_header = "@xpath= //div[text()='Management Options']";
		public static final String viewservice_managedservicevalue = "@xpath= //label[contains(text(),'Managed Service')]/parent::div/following-sibling::div";
		public static final String viewservice_syslogeventview = "@xpath= //label[contains(text(),'Syslog Event View')]/parent::div/following-sibling::div";
		public static final String viewservice_servicestatusview = "@xpath= //label[contains(text(),'Service Status View')]/parent::div/following-sibling::div";
		public static final String viewservice_routerconfigview = "@xpath= //label[contains(text(),'Router Configuration View')]/parent::div/following-sibling::div";
		public static final String viewservice_performancereporting = "@xpath= //label[contains(text(),'Performance Reporting')]/parent::div/following-sibling::div";
		public static final String viewservice_proactivenotification = "@xpath= //label[contains(text(),'Pro-active Notification')]/parent::div/following-sibling::div";
		public static final String viewservice_dialuseradministration = "@xpath= //label[contains(text(),'Dial User Administration')]/parent::div/following-sibling::div";

		public static final String edit_trunk = "@xpath= //div[text()='Trunk Group/Site Orders']/following::table//td//a[contains(text(),'Edit')]";
		public static final String edit = "@xpath= //div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String view = "@xpath= //div[@class='dropdown-menu show']//a[text()='View']";
		public static final String delete = "@xpath= //div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		public static final String breadcrumb = "@xpath= //ol[@class='breadcrumb']//a[text()='value']";
		public static final String breadcrumb1 = "@xpath= //ol[@class='breadcrumb']//a[text()='";
		public static final String breadcrumb2 = "']";

		// !-- Device --
		public static final String asrdevice_header = "@xpath= //div[text()='ASR Device']";
		public static final String adddevice_link = "@xpath= //a[text()='Add ASR Device']";
		public static final String adddevice_header = "@xpath= //div[text()='Add ASR Device']";
		public static final String imspoplocation_dropdown = "@xpath= //label[text()='IMS POP Location']/parent::div//input";
		public static final String imspoplocation_warngmsg = "@xpath= //div[label[contains(text(),'IMS POP Location')]]/following-sibling::div";
		// public static final String existingdevicegrid="@xpath=
		// (//div[text()='ASR
		// Device']/parent::div/following-sibling::div[@class='div-margin
		// row'])[2]";
		// public static final String addeddevicename="@xpath=
		// (//div[text()='ASR
		// Device']/parent::div/following-sibling::div[@class='div-margin
		// row'])//b";
		public static final String existingdevicegrid = "@xpath= (//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row'])/following-sibling::table";
		public static final String addeddevicename = "@xpath=(//div[text()='ASR Device']/following::tr)//b";
		public static final String addeddevice_viewlink = "@xpath= (//div[contains(text(),'ASR Device')]//parent::div//following-sibling::table//tr//td//span//a[text()='View'])[1]";
		public static final String addeddevice_viewlink1 = "@xpath= (//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']/following::table[1])//b[contains(text(),'";
		public static final String addeddevice_viewlink2 = "')]/following::span//a[contains(text(),'View')]";

		// addeddevice_editlink =(//div[contains(text(),'ASR
		// Device')]//parent::div//following-sibling::table//tr//td//span//a[text()='Edit'])[1]
		public static final String addeddevice_editlink = "@xpath= (//div[contains(text(),'ASR Device')]//parent::div//following-sibling::table//tr//td//span//a[text()='Edit'])[1]";
		public static final String addeddevice_editlink1 = "@xpath= (//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']/following::table[1])//b[contains(text(),'";
		public static final String addeddevice_editlink2 = "')]/following::span//a[contains(text(),'Edit')]";

		public static final String addeddevice_Deletefromservicelink = "@xpath= //div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";
		public static final String addeddevice_Deletefromservicelink1 = "@xpath= (//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']/following::table[1])//b[contains(text(),'";
		public static final String addeddevice_Deletefromservicelink2 = "')]/following::span//a[contains(text(),'Delete from Service')]";

		public static final String addeddevice_SelectInterfaceslink = "@xpath= //div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";
		public static final String addeddevice_SelectInterfaceslink1 = "@xpath= (//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']/following::table[1])//b[contains(text(),'";
		public static final String addeddevice_SelectInterfaceslink2 = "')]/following::span//a[contains(text(),'Select Interfaces')]";

		// public static final String addeddevices_list="@xpath=
		// //div[text()='ASR
		// Device']/parent::div/following-sibling::div[@class='div-margin
		// row']//b";
		public static final String addeddevices_list = "@xpath= //div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row']/following::table//b";
		public static final String viewpage_vendormodel = "@xpath= //label[text()='Vendor/Model']/parent::div/following-sibling::div";

		// !-- Edit ASR Device --="@xpath=
		// public static final String editasrdevice_header="@xpath=
		// //div[@class='heading-green-row row']//div";
		public static final String editasrdevice_header = "@xpath=//div[text()='Edit ASR Device']";
		public static final String edit_asrdevicename = "@xpath= //input[@id='deviceName']";
		public static final String edit_asrvendormodel = "@xpath= //label[text()='Vendor/Model']/parent::div/div/div";
		public static final String edit_managementaddressvalue = "@xpath= //input[@id='managementAddress']";
		public static final String edit_asrdevice_snmpro = "@xpath= //input[@id='snmpro']";

		// !-- View ASR Device --="@xpath=
		public static final String viewdevice_Actiondropdown = "@xpath= //div[text()='Device']/following-sibling::div//button[text()='Action']";
		// public static final String viewdevice_Edit="@xpath=
		// //div[text()='Device']/following-sibling::div//a[text()='Edit']";
		public static final String viewdevice_Edit = "@xpath=//div[text()='Device']/following-sibling::div//a[contains(text(),'Edit')]";
		public static final String viewdevice_delete = "@xpath= //div[text()='Device']/following-sibling::div//a[contains(text(),'Delete')]";
		public static final String deletealertclose = "@xpath= //div[@class='modal-content']//button//span[contains(text(),'�')]";
		public static final String viewasrdevice_header = "@xpath= //div[@class='heading-green-row row']/div[text()='Device']";
		public static final String asrdevicename_viewpage = "@xpath= //label[text()='Name']/parent::div/following-sibling::div";
		public static final String asrvendormodel_viewpage = "@xpath= //label[text()='Vendor/Model']/parent::div/following-sibling::div";
		public static final String asrmanagementaddess_viewpage = "@xpath= //label[text()='Management Address']/parent::div/following-sibling::div";
		public static final String asrsnmpro_viewpage = "@xpath= //label[text()='Snmpro']/parent::div/following-sibling::div";
		public static final String asrcountry_viewpage = "@xpath= //label[text()='Country']/parent::div/following-sibling::div";
		public static final String asrcity_viewpage = "@xpath= //label[text()='City']/parent::div/following-sibling::div";
		public static final String asrsite_viewpage = "@xpath= //label[text()='Site']/parent::div/following-sibling::div";
		public static final String asrpremise_viewpage = "@xpath= //label[text()='Premise']/parent::div/following-sibling::div";

		// !-- Fetch Interface --="@xpath=
		public static final String viewdevice_fetchinterfacelink = "@xpath= //div[text()='Device']/following-sibling::div//a[text()='Fetch Device Interfaces']";
		public static final String fetchsuccessmsg = "@xpath= (//div[@role='alert']//span)[1]";
		public static final String herelink_fetchinterface = "@xpath= (//div[@role='alert']//span)[1]/a";
		public static final String fetchDeviceInterace_SuccessMessage = "@xpath= (//div[contains(@class,'alert-success')]/span)[1]";
		public static final String fetchdeviceInterface_successtextMessage = "@xpath= (//div[@role='alert']/span)[1]";

		// !-- Manage Network --="@xpath=
		public static final String managenetwork_header = "@xpath= (//div[@class='heading-green-row row'])[1]//div";
		public static final String status_header = "@xpath= (//div[@class='heading-green-row row'])[2]//div";
		public static final String synchronization_header = "@xpath= (//div[@class='heading-green-row row'])[3]//div";
		public static final String status_devicecolumn = "@xpath= //div[text()='Status']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String status_statuscloumn = "@xpath= //div[text()='Status']/parent::div/following-sibling::div//label[text()='Status']";
		public static final String status_lastmodificationcolumn = "@xpath= //div[text()='Status']/parent::div/following-sibling::div//label[text()='Last Modification']";
		public static final String status_Action = "@xpath= //div[text()='Status']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String synchronization_devicecolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String synchronization_syncstatuscolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Sync Status']";
		public static final String synchronization_fmccolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='FMC']";
		public static final String synchronization_smartscolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Smarts']";
		public static final String synchronization_dcsdevice = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='DCS']";
		public static final String synchronization_FetchInterfacescolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Fetch Interfaces']";
		public static final String synchronization_vistamartdevicecolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='VistaMart Device']";
		public static final String synchronization_pgwcolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='PGW']";
		public static final String synchronization_actioncolumn = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String status_devicevalue = "@xpath= (//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[1]";
		public static final String status_statusvalue = "@xpath= //div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[2]";
		public static final String status_lastmodificationvalue = "@xpath= //div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[3]";
		public static final String status_statuslink = "@xpath= (//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[2]";
		public static final String status_viewinterfaceslink = "@xpath= (//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[3]";
		public static final String synchronization_devicevalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//a)[1]";
		public static final String synchronization_syncstatusvalue = "@xpath= //div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']/div[2]/span";
		public static final String synchronization_fmcvalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[3]//span";
		public static final String synchronization_smartsvalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[4]//span";
		public static final String synchronization_dcsvalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[5]//span";
		public static final String synchronization_fetchinterfacesvalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[6]//span";
		public static final String synchronization_vistamartdevicevalue = "@xpath= (//div[@class='div-margin row']//div)[10]//span";
		public static final String synchronization_pgwvalue = "@xpath= (//div[@class='div-margin row']//div)[11]//span";
		public static final String synchronization_synchronizelink = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//a)[2]";
		public static final String smartsvalue_asrdevicepanel = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[3]//span";
		public static final String smarts_datetime_asrdevicepanel = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[3]//span/parent::div";
		public static final String fetchinterfacevalue_asrdevicepanel = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[5]//span";
		public static final String fetchinterface_datetime_asrdevicepanel = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[5]//span/parent::div";
		public static final String vistamartvalue_asrdevicepanel = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[6]//span";
		public static final String vistamart_datetime_asrdevicepanel = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[6]//span/parent::div";
		public static final String managenetwork_backbutton = "@xpath= (//div[@class='div-margin row'])[4]//button";
		public static final String fmc_datetimevalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[3]//span/parent::div";
		public static final String smarts_datetimevalue = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[4]//span/parent::div";
		public static final String dcs_datetime = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[5]//span/parent::div";
		public static final String fetchinterfaces_datetime = "@xpath= (//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[6]//span/parent::div";
		public static final String vistamartdevice_datetime = "@xpath= (//div[@class='div-margin row']//div)[10]//span/parent::div";
		public static final String pgw_datetime = "@xpath= (//div[@class='div-margin row']//div)[11]//span/parent::div";
		// public static final String Sync_successmsg="@xpath=
		// //div[@role='alert']//span";
		public static final String staus_statuspopup = "@xpath= //div[@class='modal-content']";
		public static final String Statuspage_header = "@xpath= //div[@class='modal-header']//div";
		public static final String statuspage_nameheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']";
		public static final String statuspage_vendormodelheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']";
		public static final String statuspage_managementaddressheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']";
		public static final String statuspage_snmproheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']";
		public static final String statuspage_countryheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']";
		public static final String statuspage_cityheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']";
		public static final String statuspage_siteheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']";
		public static final String statuspage_premiseheader = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']";
		public static final String statuspage_namevalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']/following-sibling::div";
		public static final String statuspage_vendormodelvalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']/following-sibling::div";
		public static final String statuspage_managementaddressvalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']/following-sibling::div";
		public static final String statuspage_snmprovalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']/following-sibling::div";
		public static final String statuspage_countryvalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']/following-sibling::div";
		public static final String statuspage_cityvalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']/following-sibling::div";
		public static final String statuspage_sitevalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']/following-sibling::div";
		public static final String statuspage_premisevalue = "@xpath= (//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']/following-sibling::div";
		public static final String Statuspage_statusheader = "@xpath= //div[@class='modal-content']//div[@class='heading-green-row row']//div";
		public static final String statuspage_currentstatusfieldheader = "@xpath= //div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']";
		public static final String statuspage_newstatusfieldheader = "@xpath= //div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='New Status']";
		public static final String statuspage_currentstatusvalue = "@xpath= //div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']/following-sibling::div";
		public static final String statuspage_newstatusdropdown = "@xpath= (//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select";
		public static final String statuspage_newstatusdropdownvalue = "@xpath= ((//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select//option)[1]";
		public static final String statuspage_okbutton = "@xpath= (//div[@class='modal-content']//button[@type='button']//span)[3]";
		public static final String statuspage_statuscolumnheader = "@xpath= //span[@role='columnheader'][text()='Status']";
		public static final String statuspage_changedon_columnheader = "@xpath= //span[@role='columnheader'][text()='Changed On']";
		public static final String statuspage_changedby_columnheader = "@xpath= //span[@role='columnheader'][text()='Changed By']";
		public static final String statuspage_newstatusvalue = "@xpath= (//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[1]";
		public static final String statuspage_changedonvalue = "@xpath= (//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[2]";
		public static final String statuspage_changedbyvalue = "@xpath= (//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[3]";
		public static final String statuspage_closebutton = "@xpath= (//div[@class='modal-content']//button[@type='button'])[1]";
		public static final String viewinterfacepage_header = "@xpath= //div[@class='modal-header']//div";
		public static final String viewinterfacepage_interfacesubheader = "@xpath= //div[@class='modal-body']//div[@class='heading-green-row row']/div";
		public static final String viewinterface_devicenamecolumnheader = "@xpath= (//div[@col-id='deviceName']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfacename_columnheader = "@xpath= (//div[@col-id='name']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfaceaddress_columnheader = "@xpath= (//div[@col-id='address']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfacetype_columnheader = "@xpath= (//div[@col-id='type.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfaceaddress_rowvalue = "@xpath= (//div[@role='row']//div[@role='gridcell'][@col-id='address'])[1]";
		public static final String interfacetype_rowvalue = "@xpath= (//div[@role='row']//div[@role='gridcell'][@col-id='type.desc'])[1]";
		public static final String viewinterface_status_columnheader = "@xpath= (//div[@col-id='currentStatus.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String viewinterface_status_rowvalue = "@xpath= (//div[@role='row']//div[@role='gridcell'][@col-id='currentStatus.desc'])[1]";
		public static final String viewinterface_lastmod_columnheader = "@xpath= (//div[@col-id='m_time']//div[@ref='eLabel'])[1]/span[1]";
		public static final String viewinterface_lastmod_rowvalue = "@xpath= (//div[@role='row']//div[@role='gridcell'][@col-id='m_time'])[1]";
		public static final String viewinterface_closebutton = "@xpath= (//div[@class='modal-header']//button[@type='button']/span)[1]";
		public static final String statuspage_interfaceheader = "@xpath= (//div[@class='modal-header']//div)[2]";
		public static final String interface_statuspage_namefield = "@xpath= //label[text()='Name']";
		public static final String interface_statuspage_interfaceaddressfield = "@xpath= //label[text()='Interface Address']";
		public static final String interface_statuspage_currentstatusfield = "@xpath= //label[text()='Current Status']";
		public static final String interface_statuspage_newstatusfield = "@xpath= //label[text()='New Status']";
		public static final String interface_statuspage_namevalue = "@xpath= //label[text()='Name']/following-sibling::div";
		public static final String interface_statuspage_interfaceaddressvalue = "@xpath= //label[text()='Interface Address']/following-sibling::div";
		public static final String interface_statuspage_currentstatusvalue = "@xpath= //label[text()='Current Status']/following-sibling::div";
		public static final String interface_statuspage_newstatusdropdown = "@xpath= //label[text()='New Status']/parent::div//select";
		public static final String interface_statuspage_newstatusdropdownvalue = "@xpath= //label[text()='New Status']/parent::div//select/option";
		public static final String interface_statuspage_okbutton = "@xpath= //div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//button[@type='button']//span";
		public static final String interface_statuspage_statuscolumnheader = "@xpath= (//div[@ref='eLabel']//span[text()='Status'])[2]";
		public static final String interface_statuspage_changedon_columnheader = "@xpath= //div[@ref='eLabel']//span[text()='Changed On']";
		public static final String interface_statuspage_changedby_columnheader = "@xpath= //div[@ref='eLabel']//span[text()='Changed By']";
		public static final String interface_statuspage_newstatusvalue = "@xpath= (//div[@role='gridcell'][@col-id='status.desc'])[1]";
		public static final String interface_statuspage_changedonvalue = "@xpath= (//div[@role='gridcell'][@col-id='changeDate'])[1]";
		public static final String interface_statuspage_changedbyvalue = "@xpath= (//div[@role='gridcell'][@col-id='user'])[1]";
		public static final String interface_statuspage_closebutton = "@xpath= (//div[@class='modal-header']//button[@type='button'])[2]";
		public static final String interface_statuspage_statusheader = "@xpath= (//div[@class='modal-body']//div[@class='heading-green-row row']//div)[2]";
		public static final String searchdevice_header = "@xpath= (//div[@class='heading-green-row row'])[1]//div";

		// !-- Router Panel --="@xpath=
		public static final String routertools_header = "@xpath= //div[text()='Router Tools']";
		public static final String commandIPV4_dropdown = "@xpath= //div[label[text()='Command IPV4']]//input";
		public static final String commandIPv4_hostnameTextfield = "@xpath= //input[@id='routertools.hostnameOrIPAddress']";
		public static final String commandIPv4_vrfnameTextField = "@xpath= (//input[@id='routertools.vrfName'])[1]";

		public static final String commandIPV6_dropdown = "@xpath= //div[label[text()='Command IPV6']]//input";
		public static final String commandIPv6_hostnameTextfield = "@xpath= //input[@id='routertools.hostnameOrIPv6Address']";
		public static final String commandIPv6_vrfnameTextField = "@xpath= (//input[@id='routertools.vrfName'])[2]";

		public static final String executebutton_Ipv4 = "@xpath= (//span[text()='Execute'])[1]";
		public static final String executebutton_IPv6 = "@xpath= (//span[text()='Execute'])[2]";

		public static final String result_textArea = "@xpath= //div[label[text()='Result']]//textarea";

		// !-- Add Interface --="@xpath=
		public static final String interfaces_header = "@xpath= //div[contains(text(),'Interfaces')]";
		public static final String interfacepanel_actiondropdown = "@xpath= //div[text()='Interfaces']/parent::div//button[@id='dropdown-basic-button']";
		public static final String addinterface_link = "@xpath= //a[text()='Add Interface']";
		public static final String addinterface_header = "@xpath= //div[text()='Add Interface']";
		public static final String addedinterfaces = "@xpath= //div[text()='Interfaces']/parent::div/following-sibling::div//div[@ref='eBodyContainer']";
		public static final String searchadedinterface = "@xpath= //div[@align='right']/input[@id='filter-text-box']";
		public static final String interface_warngmsg = "@xpath= //input[@id='interfaceName']/parent::div//div";
		public static final String allocate_button = "@xpath= //button//span[text()='Allocate']";
		public static final String interfacename_textfield = "@xpath= //input[@id='interfaceName']";
		public static final String interfaceaddress_textfield = "@xpath= //input[@id='interfaceAddress']";
		public static final String virtualtemplate_textfield = "@xpath= //input[@id='virtualTemplate']";
		public static final String cpeaddressrange_textfield = "@xpath= //input[@id='cpeAddressRange']";
		public static final String localpresharekey_textfield = "@xpath= //input[@id='localPreShareKey']";
		public static final String remotepresharekey_textfield = "@xpath= //input[@id='remotePreShareKey']";
		public static final String identityemail_textfield = "@xpath= //input[@id='identityEmail']";
		public static final String configuration_header = "@xpath= //div[text()='Configuration']";
		public static final String configuration_dropdown = "@xpath= (//label[text()='Configuration']/parent::div//input)[1]";
		public static final String generateconfiguration_link = "@xpath= //div/a[text()='Generate Configuration']";
		public static final String configuration_textarea = "@xpath= //label[@for='configurationDetails']/following-sibling::textarea";
		public static final String saveconfiguration_link = "@xpath= //div/a[text()='Save Configuration']";

		public static final String interface_column = "@xpath= //span[text()='Interface']";
		public static final String link_column = "@xpath= //span[text()='Link/Circuit Id']";
		public static final String interfaceaddressrange_column = "@xpath= //span[text()='Interface Address Range']";
		public static final String interfaceaddress_column = "@xpath= //span[text()='Interface Address']";
		public static final String bearertype_column = "@xpath= //span[text()='Bearer Type']";
		public static final String bandwidth_column = "@xpath= //span[text()='Bandwidth']";
		public static final String vlanid_column = "@xpath= //span[text()='VLAN Id']";
		public static final String ifinoctets_column = "@xpath= //span[text()='IfInOctets']";
		public static final String ivmanagement_column = "@xpath= //span[text()='IV Management']";
		public static final String interfacename_value = "@xpath= //div[@role='gridcell'][@col-id='interfaceName']";
		public static final String link_value = "@xpath= //div[@role='gridcell'][@col-id='linkId']";
		public static final String interfaceaddressrange_value = "@xpath= //div[@role='gridcell'][@col-id='interfaceAddressRange']";
		public static final String interfaceaddress_value = "@xpath= //div[@role='gridcell'][@col-id='interfaceAddress']";
		public static final String bearertype_value = "@xpath= //div[@role='gridcell'][@col-id='bearerType']";
		public static final String bandwidth_value = "@xpath= //div[@role='gridcell'][@col-id='bandWidth']";
		public static final String vlanid_value = "@xpath= //div[@role='gridcell'][@col-id='vlanId']";
		public static final String ifinoctets_value = "@xpath= //div[@role='gridcell'][@col-id='ifInOctets']";
		// public static final String interfaces_header="@xpath=
		// //div[text()='Interfaces']";
		public static final String showinterfaces_link = "@xpath= //a[text()='Show Interfaces']";
		public static final String selectinterfaces_link = "@xpath= //a[contains(text(),'Select Interfaces')]";// added
																												// new
		public static final String editinterface_header = "@xpath= //div[@class='modal-header']//div[text()='Edit Interface']";
		public static final String selectinterface = "@xpath= //div[@role='gridcell'][text()='value']/parent::div//span[contains(@class,'unchecked')]";
		public static final String addeddevice_interface_actiondropdown = "@xpath= //div[div[b[text()='value']]]//following-sibling::div//button[text()='Action']";

		// !-- Select Interface --="@xpath=
		public static final String interfacesinservice_list = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacesinservice_list1 = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]/following::div[contains(text(),'";
		public static final String interfacesinservice_list2 = "')]";
		public static final String SelectaddInterface = "@xpath=//div[contains(text(),'Interfaces to Select')]/following::div[@col-id='name']";

		public static final String SelectaddInterface1 = "//div[contains(text(),'";
		public static final String SelectaddInterface2 = "')]";
		public static final String InterfaceInselect_currentpage = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceInselect_totalpage = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceInselect_Prevouespage = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceInselect_nextpage = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceToselect_Actiondropdown = "@xpath= //div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String InterfaceToselect_addbuton = "@xpath= //div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";
		public static final String InterfaceToselect_Checkbox = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='value']/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String InterfaceToselect_Checkbox1 = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='";
		public static final String InterfaceToselect_Checkbox2 = "']/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String Interfacetoselect = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[@role='gridcell'][text()='value']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String Interfacetoselect1 = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[@role='gridcell'][text()='";
		public static final String Interfacetoselect2 = "']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String InterfaceToselect_currentpage = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceToselect_totalpage = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceToselect_Prevouespage = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceToselect_nextpage = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceInselect_Actiondropdown = "@xpath= //div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String InterfaceInselect_removebuton = "@xpath= //div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String InterfaceToselect_backbutton = "@xpath= //span[contains(text(),'Back')]";
		public static final String interfacesToSelect_header = "@xpath= //div[text()='Interfaces to Select']";
		public static final String InteraceColumn_Filter = "@xpath= (//div[div[text()='Interfaces to Select']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String InterfacefilterTxt = "@xpath= //div[text()='Interfaces to Select']/parent::div/parent::div//div[@class='ag-menu']//div[@class='ag-filter-body']//input[@id='filterText']";
		public static final String InterfaceInService_panelHeader = "@xpath= //div[div[contains(text(),'Interfaces in Service')]]";
		public static final String interface_gridselect = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='value']";
		public static final String interface_gridselect1 = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='";
		public static final String interface_gridselect2 = "']";

		public static final String InterfaceToSelect_interfaceColumnHeader = "@xpath= //div[div[text()='Interfaces to Select']]//following-sibling::div//span[text()='Interface']";
		public static final String InterfacetoSelect_listOFinterfaceValuesDisplaying = "@xpath= //div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		// !-- Trunk --="@xpath=
		public static final String trunkPanel = "@xpath= //div[div[text()='Trunk Group/Site Orders']]";
		public static final String addTrunkSiteOrderlink = "@xpath= //a[text()='Add Trunk Group/Site Order']";
		public static final String addtrunkSiteorderPage_panelheader = "@xpath= //div[div[p[text()='Trunk Group/Site Order']]]";
		public static final String trunkGroupOrder_checkbox = "@xpath= //div[label[text()='Trunk Group Order']]//input";
		public static final String trunkGrouporder_warningmsg = "@xpath= //div[text()='Trunk Group Order']";
		public static final String trunkGroupOrderName_textField = "@xpath= //div[label[text()='Trunk Group Order Number']]//input";
		public static final String trunkGroupOrderName_warningmsg = "@xpath= //div[text()='Trunk Group Order Number']";
		public static final String trunkGroupOrderName_errMsg = "@xpath= //div[@role='alert']";
		public static final String alertMSg_siteorderCreation = "@xpath= (//div[@role='alert']/span)[1]";
		public static final String trunk_okButton = "@xpath= //span[text()='OK']";
		public static final String customerid = "@xpath= //input[@id='custId']";
		public static final String voipProtocol_Dropdown = "@xpath= //select[@id='voipProtocol']/option";
		public static final String billingCoutry_Dropdown = "@xpath= //select[@id='billingCountry']";
		public static final String CDRdelivery_Dropdown = "@xpath= //select[@id='cdrDelivery']";
		public static final String gateway_Dropdown = "@xpath= //select[@id='gateway']";
		public static final String quality_Dropdown = "@xpath= //select[@id='quality']";
		public static final String quality_Dropdown1 = "@xpath= //label[contains(text(),'Quality')]/following::select[@name='quality']";
		public static final String cosprofile = "@xpath= //select[@id='cosProfile']";
		public static final String lanrange = "@xpath= //input[@id='ipRangeField']";
		public static final String IPaddresstype_Dropdown = "@xpath= //select[@id='ipAddressType']";
		public static final String subInterfaceSlot_Dropdown = "@xpath= //select[@id='sifSlot']";
		public static final String signallingTransportProtocol_Dropdown = "@xpath= //select[@id='transProt']";
		public static final String sourceAddressFiltering_Dropdown = "@xpath= //select[@id='sourceAddressFiltering']";
		public static final String callLimit_Dropdown = "@xpath= //select[@id='callLimit']";
		public static final String primaryTrunkGroup_Dropdown = "@xpath= //select[@id='priTgName']";
		public static final String resellerid = "@xpath= //input[@id='resellerId']";
		public static final String SIPURI_textField = "@xpath= //input[@id='sipUri']";
		public static final String SIPURI_defaultValue_textvalue = "@xpath= //div[label[text()='SIP URI']]/div";
		public static final String trunkGroupDEscription_textField = "@xpath= //input[@id='tgDescription']";
		public static final String trunkGroupName_TextField = "@xpath= //input[@id='tgName']";
		public static final String SIPsignallingport_textField = "@xpath= //input[@id='sipSignalingPort']";
		public static final String SIPsignallingPOrt_defaultValue_textvalue = "@xpath= //div[label[text()='SIP Signalling Port']]/div";
		public static final String vlanTag_textField = "@xpath= //input[@id='vlanTg']";
		public static final String signallingZone_textField = "@xpath= //input[@id='sigZone']";
		public static final String subInterfaceName_textField = "@xpath= //input[@id='sifName']";
		public static final String NIFgrouopp_textField = "@xpath= //input[@id='nifGroup']";
		public static final String signallingPort_textField = "@xpath= //input[@id='sigPort']";
		public static final String ipInterface_textField = "@xpath= //input[@id='ipIinterface']";
		public static final String AddressContext_textField = "@xpath= //input[@id='addContext']";
		public static final String ipInterfaceGroup_textField = "@xpath= //input[@id='ipifgroup']";
		public static final String COSprofile_TextField = "@xpath= //input[@id='cosProfileNew']";
		public static final String limitNumber_textField = "@xpath= //input[@id='limit']";
		public static final String callRateLimitt_textField = "@xpath= //input[@id='callrateLimit']";
		public static final String internetBasedCustomer_checkbox = "@xpath= //div[label[text()='Internet Based Customer']]//input";
		public static final String numberporting_checkbox = "@xpath= //input[@id='numberPorting']";
		public static final String callAdmissionControl_checkbox = "@xpath= //input[@id='cac']";
		public static final String callrateLimit_checkbox = "@xpath= //input[@id='cacrt']";
		public static final String COSprofile_checkbox = "@xpath= //input[@id='endiscosprof']";
		public static final String clipscreeningandclir_radio = "@xpath= //input[@value='CLIP Screening and CLIR per Call (DEFAULT)']";
		public static final String clirPermanent_radio = "@xpath= //input[@value='CLIR Permanent']";
		public static final String clipNoScreening_radio = "@xpath= //input[@value='CLIP NO Screening']";
		public static final String clipmainnumber_radio = "@xpath= //input[@value='CLIP Main Number']";
		public static final String presentationnumbers_radio = "@xpath= //input[@value='Presentation Numbers']";
		public static final String presentationnumber_textfield = "@xpath= //input[@id='presNosText']";
		public static final String presentationnumber_addarrow = "@xpath= //input[@id='presNosText']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String customerdefaultnumber_textfield = "@xpath= //input[@id='custDefNum']";
		public static final String multisitedefaultnumber = "@xpath= //input[@id='multiSiteDefaultNumber']";
		public static final String invalidclitreatment_allow = "@xpath= //input[@name='subruleChange'][@value='1']";
		public static final String invalidclitreatment_block = "@xpath= //input[@name='subruleChange'][@value='2']";
		public static final String egressnumberformat_dropdown = "@xpath= //select[@id='egressNumberFormat']";
		public static final String incomingroutingonDDIlevel_checkbox = "@xpath= //input[@id='incomingRouting']";
		public static final String DRusingTDM_checkbox = "@xpath= //input[@id='drtdm']";
		public static final String codec_dropdown = "@xpath= //select[@id='numberofCODECPorts']";
		public static final String faxdiversionnumbers_textfield = "@xpath= //input[@id='faxDiversionNumberText']";
		public static final String faxdiversion_addarrow = "@xpath= //input[@id='faxDiversionNumberText']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String partialnumberreplacement_checkbox = "@xpath= //input[@id='partialNumberReplacement']";
		public static final String cpemanualconfig_checkbox = "@xpath= //input[@id='cpeManualConfig']";
		public static final String PSXmanualConfigTG_checkbox = "@xpath= //input[@id='psxManualConfigTG']";
		public static final String PSXmanualConfigDDIRange_checkbox = "@xpath= //input[@id='psxManualConfigDDI']";
		public static final String GSXmanualConfig_checkbox = "@xpath= //input[@id='gsxManualConfig']";
		public static final String SBCmanualconfig_checkbox = "@xpath= //input[@id='sbcManualConfig']";

		public static final String country_warningMessage = "@xpath= //div[text()='Billing Country']";
		public static final String viewtrunk_resellerid = "@xpath= //label[text()='Reseller ID']/parent::div/following-sibling::div";
		public static final String viewtrunk_customerid = "@xpath= //label[text()='Customer ID']/parent::div/following-sibling::div";
		public static final String viewtrunk_trunkgroupname = "@xpath= //label[text()='Trunk Group Name']/parent::div/following-sibling::div";
		public static final String GSXconfigurationDropdown_viewtrunk = "@xpath= (//div[label[text()='GSX Configuration']]//input)[1]";
		public static final String PSXconfigurationDropdown_viewtrunk = "@xpath= (//div[label[text()='PSX Configuration']]//input)[1]";
		public static final String SBCconfigurationDropdown_viewtrunk = "@xpath= (//div[label[text()='SBC Configuration']]//input)[1]";
		public static final String viewTrunk_GSX_generateConfigurationButton = "@xpath= //button/span[text()='Generate Configuration']";
		public static final String viewTrunk_PSX_executeButton = "@xpath= //div[div[div[label[text()='PSX Configuration']]]]//following-sibling::div//button/span[text()='Execute']";
		public static final String viewTrunk_SBC_executeButton = "@xpath= (//div[div[div[label[text()='SBC Configuration']]]]//following-sibling::div//button/span[text()='Execute'])[1]";

		public static final String viewPage_ActionButton = "@xpath= //div[contains(text(),'View Trunk')]/following-sibling::div//button";
		public static final String viewpage_signallingzone = "@xpath= //div[div[label[contains(text(),'Signalling Zone')]]]/div[2]";
		public static final String viewpage_cosprofile = "@xpath= //div[div[label[contains(text(),'COS Profile')]]]/div[2]";
		public static final String viewpage_thirdpartyinternet = "@xpath= //div[div[label[contains(text(),'3rd Party Internet')]]]/div[2]";
		public static final String viewpage_subinterfaceslot = "@xpath= //div[div[label[contains(text(),'Sub Interface Slot')]]]/div[2]";
		public static final String viewpage_vlantag = "@xpath= //div[div[label[contains(text(),'VLAN Tag')]]]/div[2]";
		public static final String viewpage_signallingport = "@xpath= //div[div[label[contains(text(),'Signalling Port')]]]/div[2]";
		public static final String viewpage_CLIfeatures = "@xpath= //div[div[label[contains(text(),'CLI Features')]]]/div[2]";
		public static final String viewpage_CLItreatment = "@xpath= //div[div[label[contains(text(),'Invalid CLI Treatment')]]]/div[2]";
		public static final String viewpage_egressnumberformat = "@xpath= //div[div[label[contains(text(),'Egress Number Format')]]]/div[2]";
		public static final String viewpage_usageofincomingrouting = "@xpath= //div[div[label[contains(text(),'Usage of Incoming routing on DDI Level')]]]/div[2]";
		public static final String viewpage_codec = "@xpath= //div[div[label[contains(text(),'Codec')]]]/div[2]";
		public static final String viewpage_drusingtdm = "@xpath= //div[div[label[contains(text(),'DR Using TDM')]]]/div[2]";

		// !-- SBC Manual Execution panel --="@xpath=
		public static final String SBCmanualConfig_PanelHeader = "@xpath= //div[contains(text(),'SBC Manually Executed Configurations')]";
		public static final String SBCManualConfig_actionDropdown = "@xpath= //div[div[contains(text(),'SBC Manually Executed Configurations')]]//button";
		public static final String SBC_addLink = "@xpath= //a[text()='Add']";
		public static final String SBC_editLink = "@xpath= //a[text()='Edit']";
		public static final String SBC_deleteLink = "@xpath= //a[text()='Delete']";
		public static final String SBC_selectCreatedValue = "@xpath= (//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";

		// !-- PSX Manual Execution panel --="@xpath=
		public static final String PSXmanualConfig_PanelHeader = "@xpath= //div[contains(text(),'PSX Manually Executed Configurations')]";
		public static final String PSXManualConfig_actionDropdown = "@xpath= //div[div[contains(text(),'PSX Manually Executed Configurations')]]//button";

		public static final String manualConfiguration_textArea = "@xpath= //textarea[@name='manualConfigNote']";
		public static final String saveButton_manualConfiguration = "@xpath= //span[contains(text(),'SAVE')]";

		// !-- Add DR Plan --="@xpath=
		public static final String addDRplan_link = "@xpath= //span[text()='Add Dr Plan']";
		public static final String disasterrecoveryplans_header = "@xpath= //div[text()='Disaster Recovery Plans']";
		public static final String DRplanA = "@xpath= //div[text()='DR Plan A']";
		public static final String rangestart_columnheader = "@xpath= (//span[text()='Range Start'])[1]";
		public static final String rangefinish_columnheader = "@xpath= (//span[text()='Range Finish'])[1]";
		public static final String destinationnumber_columnheader = "@xpath= (//span[text()='Destination Number(DN)'])[1]";
		public static final String activate_drplan_columnheader = "@xpath= (//span[text()='Activate/Deactivate DR Plan'])[1]";

		// !-- Disaster Recovery Status --="@xpath=
		public static final String codecfield_viewpage = "@xpath= //label[text()='Codec']";
		public static final String disasterrecoverystatus_header = "@xpath= //div[text()='GSX/PSX Disaster Recovery Status']";
		public static final String psx_tdm_drstatus_value = "@xpath= //label[text()='PSX TDM DR Status']/parent::div/following-sibling::div";

		// !-- Port Group --="@xpath=
		public static final String portgroup_header = "@xpath= //div[text()='Port Group']";
		public static final String portgroup_Actiondropdown = "@xpath= //div[text()='Port Group']/parent::div/following-sibling::div//button[text()='Action']";
		public static final String addportgroup_link = "@xpath= //a[text()='Add Port Group']";
		public static final String prefix_dropdown = "@xpath= (//label[text()='Prefix']/parent::div//input)[1]";
		public static final String addportgroup_header = "@xpath= //div[@class='modal-content']//div[text()='Port Group']";
		public static final String routepriority_dropdown = "@xpath= (//label[text()='Route Priority']/parent::div//input)[1]";
		public static final String portsavailable_list = "@xpath= //select[@id='availableSclList']/option";
		public static final String portsavailable_addarrow = "@xpath= //button/span[text()='>>']";
		public static final String portsavailable_removearrow = "@xpath= //button/span[text()='value']";
		public static final String portsselected_list = "@xpath= //select[@id='selectedSclList']/option";
		// public static final String roleDropdown_removeButton="@xpath=
		// //button/span[text()='value']";

		// !-- View Port group --="@xpath=
		public static final String prefix_viewportgroup = "@xpath= //label[text()='Prefix']/parent::div/following-sibling::div";
		public static final String voiceCPEs_viewportgroup = "@xpath= //label[text()='Voice CPES']/parent::div/following-sibling::div";
		public static final String routepriority_viewportgroup = "@xpath= //label[text()='Route Priority']/parent::div/following-sibling::div";
		public static final String close_viewportgroup = "@xpath= //button/span[text()='�']";
		public static final String addoverflowportgroup = "@xpath= //div[@class='dropdown-menu show']//a[text()='Add Overflow PortGroup']";
		public static final String prefix_overflowport = "@xpath= //input[@id='prefix']";
		public static final String routeprioritydisabled_overflowport = "@xpath= //label[text()='Route Priority']/parent::div/div/div";
		public static final String routepriorityvalue_overflowport = "@xpath= //label[text()='Route Priority']/parent::div/div//span";
		public static final String portgrouptable_prefixvalue = "@xpath= //div[@col-id='prefix'][text()='value']/parent::div";
		public static final String portgrouptable_routepriorityvalue = "@xpath= //div[@row-id='value']//div[@col-id='routePriority']";
		public static final String portgrouptable_voiceCPEsValue = "@xpath= //div[@row-id='value']//div[@col-id='scl']";
		public static final String addedport_checkbox = "@xpath= //div[@row-id='value']//div//span[contains(@class,'unchecked')]";
		public static final String overflowportsavailable_list = "@xpath= //select[@id='availableScl']/option";
		public static final String overflow_portsselected_list = "@xpath= //select[@id='selectedScl']/option";
		public static final String delete_portgroup = "@xpath= //div[@class='dropdown-menu show']//a[text()='Delete']";
		public static final String delete_overflowportgroup = "@xpath= //div[@class='dropdown-menu show']//a[text()='Delete Overflow']";
		public static final String delete_alertDelete = "@xpath= //button/span[text()='Delete']";
		public static final String overflow_checkbox = "@xpath= //div[@role='gridcell'][text()='value']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		// !-- Voice Resiliency --="@xpath=
		public static final String ddirange_header = "@xpath= //div[text()='DDI Range']";
		public static final String ddirange_panelheader = "@xpath= //h5[text()='DDI Range']";
		public static final String voiceresiliency_header = "@xpath= //div[text()='Voice Resiliency']";
		public static final String backupnumber_value = "@xpath= //label[text()='Backup Number']/parent::div/following-sibling::div";
		public static final String obackupnumber_value = "@xpath= //label[text()='OBackup Number']/parent::div/following-sibling::div";
		public static final String billingnumber_value = "@xpath= //label[text()='Billing Number']/parent::div/following-sibling::div";
		public static final String voiceresiliency_actiondropdown = "@xpath= //div[text()='Voice Resiliency']/parent::div/following-sibling::div//button[text()='Action']";
		public static final String voiceresiliency_Edit = "@xpath= //div[text()='Voice Resiliency']/following-sibling::div//a[contains(text(),'Edit')]";// updated
		// div[text()='Voice
		// Resiliency']/parent::div/following-sibling::div//a[text()='Edit']
		public static final String voiceresiliency_popup = "@xpath= //div[@class='modal-content']";
		public static final String backupnumber_checkbox = "@xpath= //input[@id='backupNumber']";
		public static final String obackupnumber_textfield = "@xpath= //input[@id='oBackupNumber']";
		public static final String billingnumber_textield = "@xpath= //input[@id='billingNumber']";
		public static final String savebutton_voiceresiliency = "@xpath= //button/span[text()='SAVE']";

		// !-- Add Voice CPE Device --="@xpath=
		public static final String addvoicecpe_link = "@xpath= //a[contains(text(),' Add Voice CPE Device')]";// updated
		public static final String addvoicecpe_header = "@xpath= //div[@class='heading-green-row row']/div/p[contains(text(),'Add')]";
		public static final String voicecpe_devicename = "@xpath= //input[@id='deviceName']";
		public static final String vendormodel_dropdown = "@xpath= //label[text()='Vendor/Model']/parent::div//input";
		public static final String managementaddress_voicecpe = "@xpath= //input[@id='ipRangeField']";
		public static final String snmpro_textfield = "@xpath= //input[@id='snmpro']";
		public static final String countryinput = "@xpath= //div[label[contains(text(),'Country')]]//select";
		public static final String citydropdowninput = "@xpath= //div[label[@class='form-label labelStyle'][contains(text(),'City')]]//select";
		public static final String sitedropdowninput = "@xpath= //div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//select";
		public static final String premisedropdowninput = "@xpath= //div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//select";
		public static final String addcityswitch = "@xpath= //div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath= //div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath= //div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath= //div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath= //div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath= //div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String cpetoprovidedialtone_checkbox = "@xpath= //input[@id='cpeProvideDialTone']";
		public static final String cpelinepowerrequired_checkbox = "@xpath= //input[@id='cpeLinePowerrequired']";
		// public static final String numberporting_checkbox="@xpath=
		// //input[@id='numberPorting']";
		public static final String briportmapping_checkbox = "@xpath= //input[@id='briPortMapping']";
		public static final String crcsettings_dropdown = "@xpath= //label[text()='CRC Settings']/parent::div//select";
		public static final String numberofpriports_dropdown = "@xpath= //label[text()='Number of PRI Ports']/parent::div//select";
		public static final String numberofbriports_dropdown = "@xpath= //label[text()='Number of BRI Ports']/parent::div//select";
		public static final String briport1_textfield = "@xpath= //input[@id='briPort1Number']";
		public static final String briport1_addarrow = "@xpath= //label[text()='BRI Port 1 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport2_textfield = "@xpath= //input[@id='briPort2Number']";
		public static final String briport2_addarrow = "@xpath= //label[text()='BRI Port 2 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport3_textfield = "@xpath= //input[@id='briPort3Number']";
		public static final String briport3_addarrow = "@xpath= //label[text()='BRI Port 3 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport4_textfield = "@xpath= //input[@id='briPort4Number']";
		public static final String briport4_addarrow = "@xpath= //label[text()='BRI Port 4 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport5_textfield = "@xpath= //input[@id='briPort5Number']";
		public static final String briport5_addarrow = "@xpath= //label[text()='BRI Port 5 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport6_textfield = "@xpath= //input[@id='briPort6Number']";
		public static final String briport6_addarrow = "@xpath= //label[text()='BRI Port 6 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport7_textfield = "@xpath= //input[@id='briPort7Number']";
		public static final String briport7_addarrow = "@xpath= //label[text()='BRI Port 7 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";
		public static final String briport8_textfield = "@xpath= //input[@id='briPort8Number']";
		public static final String briport8_addarrow = "@xpath= //label[text()='BRI Port 8 Number']/parent::div/parent::div/following-sibling::div//button/span[text()='>>']";

		public static final String numberoffxsports_dropdown = "@xpath= //label[text()='Number of FXS Ports']/parent::div//select";
		public static final String fxsnumber1_textfield = "@xpath= //input[@id='fxsnumber1']";
		public static final String fxsnumber2_textfield = "@xpath= //input[@id='fxsnumber2']";

		public static final String voiceCPEdevice_header = "@xpath= //span/b[text()='Voice CPE Devices']";
		public static final String existingvoicecpe_devicegrid = "@xpath= //b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]";
		public static final String existing_CPE_devicegrid = "@xpath= //span//b[text()='CPE Devices']";
		public static final String addedvoiceCPEdevicename = "@xpath= //b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span/b";
		public static final String addedvoicecpedevice_viewlink = "@xpath= //b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//b[contains(text(),'value')]/parent::span/following-sibling::span[@class='deviceCrudRight']/a/span[text()='View']";
		public static final String addedvoicecpedevice_editlink = "@xpath= //b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//b[contains(text(),'value')]/parent::span/following-sibling::span[@class='deviceCrudRight']/a/span[text()='Edit']";
		public static final String addedvoicecpedevice_deletelink = "@xpath= //b[text()='Voice CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[2]//span//b[contains(text(),'value')]/parent::span/following-sibling::span[@class='deviceCrudRight']/a/span[text()='Delete']";

		// !-- View page VOice CPE Device --="@xpath=
		public static final String viewpage_voicecpedevice_header = "@xpath= //div[@class='heading-green-row row']/div[text()='Voice CPE Device']";
		public static final String country_voiceCPE_viewpage = "@xpath= //label[text()='Country']/parent::div/following-sibling::div";
		public static final String city_voiceCPE_viewpage = "@xpath= //label[text()='City']/parent::div/following-sibling::div";
		public static final String site_voiceCPE_viewpage = "@xpath= //label[text()='Site']/parent::div/following-sibling::div";
		public static final String premise_voiceCPE_viewpage = "@xpath= //label[text()='Premise']/parent::div/following-sibling::div";
		public static final String briport1number_viewpage = "@xpath= //label[text()='BRI Port 1 Number']/parent::div/following-sibling::div";
		public static final String briport2number_viewpage = "@xpath= //label[text()='BRI Port 2 Number']/parent::div/following-sibling::div";
		public static final String briport3number_viewpage = "@xpath= //label[text()='BRI Port 3 Number']/parent::div/following-sibling::div";
		public static final String briport4number_viewpage = "@xpath= //label[text()='BRI Port 4 Number']/parent::div/following-sibling::div";
		public static final String briport5number_viewpage = "@xpath= //label[text()='BRI Port 5 Number']/parent::div/following-sibling::div";
		public static final String briport6number_viewpage = "@xpath= //label[text()='BRI Port 6 Number']/parent::div/following-sibling::div";
		public static final String briport7number_viewpage = "@xpath= //label[text()='BRI Port 7 Number']/parent::div/following-sibling::div";
		public static final String briport8number_viewpage = "@xpath= //label[text()='BRI Port 8 Number']/parent::div/following-sibling::div";
		public static final String cpetoprovide_checkboxvalue = "@xpath= //div[label[text()='CPE to Provide Dial tone']]/following-sibling::div";
		public static final String cpelinepower_checkboxvalue = "@xpath= //div[label[text()='CPE line power required']]/following-sibling::div";
		public static final String numberporting_checkboxvalue = "@xpath= //div[label[text()='Number Porting']]/following-sibling::div";
		public static final String BRIportmapping_checkboxvalue = "@xpath= //div[label[text()='BRI Port Mapping']]/following-sibling::div";

		// !-- Edit Voice CPE device page --="@xpath=
		public static final String editvoicecpe_header = "@xpath= //div[@class='heading-green-row row']//p[contains(text(),'Edit')]";

		public static final String SBC_columnNames = "@xpath= (//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";
		public static final String SBC_filenames = "@xpath= //div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";
		public static final String SBC_dates = "@xpath= //div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";

		public static final String PSX_selectCreatedValue = "@xpath= (//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";
		public static final String PSX_columnNames = "@xpath= (//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";
		public static final String PSX_fileName = "@xpath= //div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";
		public static final String PSX_dates = "@xpath= //div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";

		public static final String PSX_addLink = "@xpath= //div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[text()='Add   ']";
		public static final String PSX_editLink = "@xpath= //div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[text()='Edit ']";
		public static final String PSX_deleteLink = "@xpath= //div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[text()='Delete   ']";

		public static final String editPSX_filenamevalue = "@xpath= //label[text()='File Name']/parent::div/following-sibling::div";
		public static final String editPSX_datevalue = "@xpath= //label[text()='Date']/parent::div/following-sibling::div";
		public static final String editPSX_uservalue = "@xpath= //label[text()='User']/parent::div/following-sibling::div";
		public static final String editPSX_trunkvalue = "@xpath= //label[text()='Trunk Name']/parent::div/following-sibling::div";

		// GSX Manual Execution panel --="@xpath=
		public static final String GSXmanualConfig_PanelHeader = "@xpath= //div[contains(text(),'GSX Manually Executed Configurations')]";
		public static final String GSXManualConfig_actionDropdown = "@xpath= //div[div[contains(text(),'GSX Manually Executed Configurations')]]//button";
		public static final String GSX_selectCreatedValue = "@xpath= (//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";
		public static final String GSX_columnNames = "@xpath= (//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";
		public static final String GSX_fileName = "@xpath= //div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";
		public static final String GSX_dates = "@xpath= //div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";
		public static final String GSX_addLink = "@xpath= //div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[text()='Add']";
		public static final String GSX_editLink = "@xpath= //div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[text()='Edit']";
		public static final String GSX_deleteLink = "@xpath= //div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[text()='Delete']";
		public static final String GSX_editPage_SaveButton = "@xpath= //button/span[text()='Save']";
		public static final String GSX_editPage_teaxtArea = "@xpath= //textarea[@name='manualConfigNote']";

		// public static final String manualConfiguration_textArea="@xpath=
		// //textarea[@name='manualConfigNote']";
		public static final String save_manualConfiguration = "@xpath= //button/span[contains(text(),'Save')]";

		// !-- Add CPE device Link --="@xpath=
		public static final String trunkPanel_ActionDropdown = "@xpath= //div[div[span[text()='value']]]//following-sibling::div//button[text()='Action']";
		public static final String trunkPanel_ActionDropdown1 = "@xpath= //div[div[span[text()='";
		public static final String trunkPanel_ActionDropdown2 = "']]]//following-sibling::div//button[text()='Action']";

		public static final String addCPEdeviceLink = "@xpath= //a[text()='Add CPE Device']";
		public static final String viewCPEdeviceLink = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//span//a[text()='View']";// added
		public static final String viewCPEdeviceLink1 = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//a/span[text()='View']";
		public static final String viewCPEdeviceLink2 = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//a/span[text()='View']";

		public static final String editCPEdeviceLink = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//span//a[text()='Edit']";
		public static final String editCPEdeviceLink1 = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//a/span[text()='Edit']";
		public static final String editCPEdeviceLink2 = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//a/span[text()='Edit']";

		public static final String deleteCPEdeviceLink = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//a/span[text()='Delete']";
		public static final String addedCPEdevicename = "@xpath= //b[text()='CPE Devices']/parent::span/parent::div/parent::div/following-sibling::div[1]//b";
		public static final String routerId = "@xpath= //input[@id='routerId']";
		public static final String CPE_devicename = "@xpath= //input[@id='deviceName']";
		public static final String CPE_vendorModel = "@xpath= //div[label[text()='Vendor/Model']]//input";
		public static final String CPE_manageAddress = "@xpath= //input[@id='ipField']";
		public static final String CPE_snmpro = "@xpath= //input[@id='snmpro']";
		public static final String CPE_snmprw = "@xpath= //input[@id='snmprw']";
		public static final String CPE_snmpV3ContextName = "@xpath= //input[@id='snmpv3ContextName']";
		public static final String CPE_snmpV3ContextEngineID = "@xpath= //input[@id='snmpv3ContextEngineId']";
		public static final String CPE_snmpv3SecurityUserName = "@xpath= //input[@id='snmpv3SecurityUserName']";
		public static final String CPE_snmpv3AuthProto = "@xpath= //div[label[text()='Snmp V3 Auth Proto']]//input";
		public static final String CPE_snmpv3AuthPasswrd = "@xpath= //input[@id='snmpv3AuthPassword']";

		public static final String viewdevicePage_actionDropdown = "@xpath= //button[@id='dropdown-basic-button']";

		public static final String countrydropdown = "@xpath= //div[label[contains(text(),'Country')]]//select";
		public static final String citydropdown = "@xpath= //div[label[@class='form-label labelStyle'][contains(text(),'City')]]//select";
		public static final String sitedropdown = "@xpath= //div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//select";
		public static final String premisedropdown = "@xpath= //div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//select";

		public static final String addCitytogglebutton = "@xpath= //div[div[label[text()='Add City']]]//div[@class='react-switch-bg']";

		public static final String citynameinputfield = "@xpath= //input[@id='cityName']";
		public static final String citycodeinputfield = "@xpath= //input[@id='cityCode']";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath= (//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath= (//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath= (//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath= (//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath= (//div[label[text()='Site Name']]//input)[2]";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath= (//div[label[text()='Site Code']]//input)[2]";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath= (//div[label[text()='Premise Code']]//input)[2]";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath= (//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath= (//div[label[text()='Premise Code']]//input)[3]";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath= (//div[label[text()='Premise Name']]//input)[3]";

		public static final String selectCreatedTrunk_InViewServicePage = "@xpath= //div[div[span[b[text()='Trunks ']]]]//following-sibling::div//div[text()='value']";
		public static final String selectCreatedTrunk_InViewServicePage1 = "@xpath= //div[div[span[b[text()='Trunks ']]]]//following-sibling::div//div[text()='";
		public static final String selectCreatedTrunk_InViewServicePage2 = "']";

		public static final String trunkActionDropdown_InviewServicePage = "@xpath= //div[div[span[b[text()='Trunks ']]]]//following-sibling::div//button[text()='Action']";

		public static final String existingdevicegrid_CPEdevice = "@xpath= (//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row'])[2]";
		public static final String CPEdevice_fetchAlldevice_inViewPage = "@xpath= (//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row']//b)[4]";
		public static final String CPEdveice_viewLink = "@xpath= ((//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row'])[2]//div//div[@class='row'])[4]//b[contains(text(),'value')]/following-sibling::span//span[text()='View']";

		public static final String viewCPE_CountryValue = "@xpath= //div[div[label[contains(text(),'Country')]]]/div[2]";
		public static final String viewCPE_CityValue = "@xpath= //div[div[label[contains(text(),'City')]]]/div[2]";
		public static final String viewCPE_SiteValue = "@xpath= //div[div[label[contains(text(),'Site')]]]/div[2]";

		// !-- success Message --="@xpath=
		public static final String alertMsg = "@xpath= (//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath= (//div[@role='alert']/span)[1]";

		public static final String viewtrunk_link = "@xpath= //a[text()='View']";
		public static final String viewtrunk_link_AddDDI = "@xpath= ((//b[contains(text(),'Trunk Group/Site Order')]/following::table//tr)[2]//td)//a[contains(text(),'View')]";
		public static final String deletetrunk_link = "@xpath= //a[text()='Delete']";

		// !-- DDI Range --="@xpath=
		public static final String addDDIRange_link = "@xpath= //a[text()='Add DDI Range']"; // button/span[text()='Add
																								// DDI
																								// Range']updated
		public static final String addDDIRangepage_header = "@xpath= //div[@class='heading-green-row row']/div[text()='DDI Range']";
		public static final String addDDI_countrycode = "@xpath= //input[@id='countryCode']";
		public static final String addDDI_lactextfield = "@xpath= //input[@id='lac']";
		public static final String addDDI_mainnumber_textfield = "@xpath= //input[@id='mainNumber']";
		public static final String addDDI_rangestart = "@xpath= //input[@id='rangeStart']";
		public static final String addDDI_rangeend = "@xpath= //input[@id='rangeEnd']";
		public static final String addDDI_addArrow = "@xpath= //button/span[text()='>>']";
		public static final String addDDI_extensiondigits_textfield = "@xpath= //input[@id='extensionDigitsAdd']";
		public static final String addDDI_incomingrouting_checkbox = "@xpath= //input[@id='incomingRoutingCheckAdd']";
		public static final String addDDI_portmappingprefix_dropdown = "@xpath= //label[text()='Port Mapping Prefix']/following-sibling::select";
		public static final String addDDI_addbutton = "@xpath= //button/span[text()='Add']";
		public static final String addDDI_removebutton = "@xpath= //button/span[text()='Remove']";
		public static final String countrycode_columnheader = "@xpath= //th[text()='Country Code']";
		public static final String lac_columnheader = "@xpath= //th[text()='LAC']";
		public static final String mainrange_columnheader = "@xpath= //th[text()='Main Number/Range Start-End']";
		public static final String extensiondigits_columnheader = "@xpath= //th[text()='Extension digits']";
		public static final String portgroup_columnheader = "@xpath= //th[text()='Port Group']";
		public static final String emergencyarea_columnheader = "@xpath= //th[text()='Emerg Area']";
		public static final String incomingrouting_columnheader = "@xpath= //th[text()='Incoming Routing']";
		public static final String action_columnheader = "@xpath= //th[text()='Action']";

		public static final String countrycode_value = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[1]";
		public static final String lac_value = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[2]";
		public static final String mainrange_value = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[3]";
		public static final String extensiondigits_value = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[4]";
		public static final String portgroup_value = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[5]";
		public static final String emergencyarea_value = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[6]";
		public static final String incomingrouting_checkbox = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[7]/input";
		public static final String ddi_links = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[9]";
		public static final String ddi_viewlink = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[9]/a[text()='View']";
		public static final String ddi_editlink = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[9]/a[text()='Edit']";
		public static final String ddi_deletelink = "@xpath= //div[h5[text()='DDI Range']]/parent::div/following-sibling::div[@class='ag-div-margin row']//tr/td[9]/a[text()='Delete']";

		// !-- View DDI Range page --="@xpath=
		public static final String viewpage_countrycode = "@xpath= //div[div[div[label[text()='Country']]]]//following-sibling::div";
		public static final String viewpage_lac = "@xpath= //div[div[div[label[text()='LAC']]]]//following-sibling::div";
		public static final String viewpage_extensiondigits = "@xpath= //div[div[div[label[text()='Extension digits']]]]//following-sibling::div";
		public static final String viewpage_emergArea = "@xpath= //div[div[div[label[text()='Emerg Area']]]]//following-sibling::div";
		public static final String viewpage_incomingrouting = "@xpath= //div[div[div[label[text()='Activate Incoming Routing']]]]//following-sibling::div";
		public static final String viewpage_InGEO = "@xpath= //div[div[div[label[text()='In GEO']]]]//following-sibling::div";
		public static final String viewpage_portmapping = "@xpath= //div[div[div[label[text()='Port Mapping Prefix']]]]//following-sibling::div";
		public static final String viewpage_mainnumber = "@xpath= //label[text()='Phone Numbers']/parent::div/parent::div/following-sibling::div//th[text()='Main Number']";
		public static final String viewpage_rangestart = "@xpath= //label[text()='Phone Numbers']/parent::div/parent::div/following-sibling::div//th[text()='Range Start']";
		public static final String viewpage_rangeend = "@xpath= //label[text()='Phone Numbers']/parent::div/parent::div/following-sibling::div//th[text()='Range End']";
		public static final String viewpage_mainnumber_Value = "@xpath= //label[text()='Phone Numbers']/parent::div/parent::div/following-sibling::div//tr/td[1]";
		public static final String viewpage_rangestart_value = "@xpath= //label[text()='Phone Numbers']/parent::div/parent::div/following-sibling::div//tr/td[2]";
		public static final String viewpage_rangeend_value = "@xpath= //label[text()='Phone Numbers']/parent::div/parent::div/following-sibling::div//tr/td[3]";
		public static final String viewddipage_configheader = "@xpath= //div[@class='heading-green-row row']/div[text()='Configuration']";
		public static final String viewddipage_psxconfig_dropdown = "@xpath= (//label[text()='PSX Configuration']/following-sibling::div//input)[1]";
		public static final String viewddipage_executebutton = "@xpath= //button/span[text()='Execute']";

		public static final String editDDI_selectedphonenumbers = "@xpath= //select[@id='selectedPhone']//option";
		public static final String editDDI_phonenumber_removearrow = "@xpath= //button/span[text()='value']";

		// String !-- Add DR Plans --="@xpath=
		public static final String addDRplans_link = "@xpath= (//div//a[contains(text(),'DR Plan')])[1]";// updated
		public static final String DRplansBulkInterface_link = "@xpath= //a/span[text()='DR Plans Bulk Interface ']";
		// public static final String downloadDRplans_link="@xpath=
		// //a/span[text()='Download DR Plans']";
		public static final String addDRplan_header = "@xpath= //div[text()='Disaster Recovery Plans']";
		public static final String DRplanA_header = "@xpath= //div[text()='DR Plan A']";
		public static final String DRplanB_header = "@xpath= //div[text()='DR Plan B']";
		public static final String DRplanC_header = "@xpath= //div[text()='DR Plan C']";
		public static final String DRplanD_header = "@xpath= //div[text()='DR Plan D']";
		public static final String DRplanE_header = "@xpath= //div[text()='DR Plan E']";
		public static final String DRplanA_actiondropdown = "@xpath= (//button[text()='Action'])[1]";
		public static final String DRplanB_actiondropdown = "@xpath= (//button[text()='Action'])[2]";
		public static final String DRplanC_actiondropdown = "@xpath= (//button[text()='Action'])[3]";
		public static final String DRplanD_actiondropdown = "@xpath= (//button[text()='Action'])[4]";
		public static final String DRplanE_actiondropdown = "@xpath= (//button[text()='Action'])[5]";
		// public static final String DRplan_addlink="@xpath=
		// //a[text()='Add']";
		//// public static final String DRplan_editlink="@xpath=
		// //a[text()='Edit']";
		// public static final String DRplan_Deletelink="@xpath=
		// //a[text()='Delete']";
		public static final String DRplanD_A_addlink = "@xpath= (//a[text()='Add'])[1]";// added
		public static final String DRplanD_B_addlink = "@xpath= (//a[text()='Add'])[2]";// added
		public static final String DRplanD_C_addlink = "@xpath= (//a[text()='Add'])[3]";// added
		public static final String DRplanD_D_addlink = "@xpath= (//a[text()='Add'])[4]";// added
		public static final String DRplanD_E_addlink = "@xpath= (//a[text()='Add'])[5]";

		public static final String DRplanD_A_editlink = "@xpath= (//div[text()='DR Plan A']/following::table//tr)[2]//td//a[contains(text(),'Edit')]";// added
		public static final String DRplanD_B_editlink = "@xpath= (//div[text()='DR Plan B']/following::table//tr)[2]//td//a[contains(text(),'Edit')]";// added
		public static final String DRplanD_C_editlink = "@xpath= (//div[text()='DR Plan C']/following::table//tr)[2]//td//a[contains(text(),'Edit')]";// added
		public static final String DRplanD_D_editlink = "@xpath= (//div[text()='DR Plan D']/following::table//tr)[2]//td//a[contains(text(),'Edit')]";// added
		public static final String DRplanD_E_editlink = "@xpath= (//div[text()='DR Plan E']/following::table//tr)[2]//td//a[contains(text(),'Edit')]";

		public static final String DRplanD_A_Deletelink = "@xpath= (//div[text()='DR Plan A']/following::table//tr)[2]//td//a[contains(text(),'Delete')]";// added
		public static final String DRplanD_B_Deletelink = "@xpath= (//div[text()='DR Plan B']/following::table//tr)[2]//td//a[contains(text(),'Delete')]";// added
		public static final String DRplanD_C_Deletelink = "@xpath= (//div[text()='DR Plan C']/following::table//tr)[2]//td//a[contains(text(),'Delete')]";// added
		public static final String DRplanD_D_Deletelink = "@xpath= (//div[text()='DR Plan D']/following::table//tr)[2]//td//a[contains(text(),'Delete')]";// added
		public static final String DRplanD_E_Deletelink = "@xpath= (//div[text()='DR Plan E']/following::table//tr)[2]//td//a[contains(text(),'Delete')]";
		public static final String addDRplan_header1 = "@xpath= (//div[text()='Disaster Recovery Plans'])[2]";
		public static final String rangestart_cc_warngmsg = "@xpath= //input[@id='rangeStart_CC']/following-sibling::div";
		public static final String rangestart_lac_warngmsg = "@xpath= //input[@id='rangeStart_LAC']/following-sibling::div";
		public static final String rangestart_num_warngmsg = "@xpath= //input[@id='rangeStart_Num']/following-sibling::div";
		public static final String rangefinish_cc_warngmsg = "@xpath= //input[@id='rangeFinish_CC']/following-sibling::div";
		public static final String rangefinish_lac_warngmsg = "@xpath= //input[@id='rangeFinish_LAC']/following-sibling::div";
		public static final String rangefinish_num_warngmsg = "@xpath= //input[@id='rangeFinish_Num']/following-sibling::div";
		public static final String destinationnumber_cc_warngmsg = "@xpath= //input[@id='destinationNumberCC']/following-sibling::div";
		public static final String destinationnumber_lac_warngmsg = "@xpath= //input[@id='destinationNumberLC']/following-sibling::div";
		public static final String destinationnumber_num_warngmsg = "@xpath= //input[@id='destinationNumberNum']/following-sibling::div";
		public static final String rangestart_label = "@xpath= //label[text()='Range Start']";
		public static final String rangestart_cc = "@xpath= //input[@id='rangeStart_CC']";
		public static final String rangestart_lac = "@xpath= //input[@id='rangeStart_LAC']";
		public static final String rangestart_num = "@xpath= //input[@id='rangeStart_Num']";
		public static final String rangefinish_label = "@xpath= //label[text()='Range Finish']";
		public static final String rangefinish_cc = "@xpath= //input[@id='rangeFinish_CC']";
		public static final String rangefinish_lac = "@xpath= //input[@id='rangeFinish_LAC']";
		public static final String rangefinish_num = "@xpath= //input[@id='rangeFinish_Num']";
		public static final String destinationnumber_label = "@xpath= //label[text()='Destination Number(DN)']";
		public static final String destinationnumber_cc = "@xpath= //input[@id='destinationNumberCC']";
		public static final String destinationnumber_lac = "@xpath= //input[@id='destinationNumberLC']";
		public static final String destinationnumber_num = "@xpath= //input[@id='destinationNumberNum']";
		public static final String activate_deactivate_label = "@xpath= //label[text()='Activate/Deactivate DR Plan']";
		public static final String activate_deactivate_dropdownvalue = "@xpath= (//label[text()='Activate/Deactivate DR Plan']/parent::div/parent::div/following-sibling::div//input)[1]";

		public static final String DRplanA_rangestart_columnheader = "@xpath= (//div[text()='DR Plan A']/following::table//tr)[1]//td[1]";// updated
		public static final String DRplanA_rangefinish_columnheader = "@xpath= (//div[text()='DR Plan A']/following::table//tr)[1]//td[2]";// updated
		public static final String DRplanA_destinationnumber_columnheader = "@xpath= (//div[text()='DR Plan A']/following::table//tr)[1]//td[3]";// updated
		public static final String DRplanA_activateDeactivate_columnheader = "@xpath= (//div[text()='DR Plan ']/following::table//tr)[1]//td[4]";// updated

		public static final String DRplanB_rangestart_columnheader = "@xpath= (//div[text()='DR Plan B']/following::table//tr)[1]//td[1]";// updated
		public static final String DRplanB_rangefinish_columnheader = "@xpath= (//div[text()='DR Plan B']/following::table//tr)[1]//td[2]";// updated
		public static final String DRplanB_destinationnumber_columnheader = "@xpath= (//div[text()='DR Plan B']/following::table//tr)[1]//td[3]";// updated
		public static final String DRplanB_activateDeactivate_columnheader = "@xpath= (//div[text()='DR Plan B']/following::table//tr)[1]//td[4]";// updated

		public static final String DRplanC_rangestart_columnheader = "@xpath= (//div[text()='DR Plan C']/following::table//tr)[1]//td[1]";// updated
		public static final String DRplanC_rangefinish_columnheader = "@xpath= (//div[text()='DR Plan C']/following::table//tr)[1]//td[2]";// updated
		public static final String DRplanC_destinationnumber_columnheader = "@xpath= (//div[text()='DR Plan C']/following::table//tr)[1]//td[3]";// updated
		public static final String DRplanC_activateDeactivate_columnheader = "@xpath= (//div[text()='DR Plan C']/following::table//tr)[1]//td[4]";// updated

		public static final String DRplanD_rangestart_columnheader = "@xpath= (//div[text()='DR Plan D']/following::table//tr)[1]//td[1]";// updated
		public static final String DRplanD_rangefinish_columnheader = "@xpath= (//div[text()='DR Plan D']/following::table//tr)[1]//td[2]";// updated
		public static final String DRplanD_destinationnumber_columnheader = "@xpath= (//div[text()='DR Plan D']/following::table//tr)[1]//td[3]";// updated
		public static final String DRplanD_activateDeactivate_columnheader = "@xpath= (//div[text()='DR Plan D']/following::table//tr)[1]//td[4]";// updatedublic
																																					// static
																																					// final
																																					// String
																																					// DRplanE_rangestart_columnheader="@xpath=
																																					// (//label[text()='Range
																																					// Start'])[5]";//updated

		public static final String DRplanE_rangestart_columnheader = "@xpath= (//div[text()='DR Plan E']/following::table//tr)[1]//td[1]";// updated
		public static final String DRplanE_rangefinish_columnheader = "@xpath= (//div[text()='DR Plan E']/following::table//tr)[1]//td[2]";// updated
		public static final String DRplanE_destinationnumber_columnheader = "@xpath= (//div[text()='DR Plan E']/following::table//tr)[1]//td[3]";// updated
		public static final String DRplanE_activateDeactivate_columnheader = "@xpath= (//div[text()='DR Plan E']/following::table//tr)[1]//td[4]";// updatedpublic
																																					// static
																																					// final
																																					// String
																																					// addedDRplan_tablelist="@xpath=
																																					// (//div[text()='value']/parent::div/following-sibling::div[@class='ag-div-margin
																																					// row'])[1]//div[@ref='eBodyContainer']";
		public static final String addedDRplan_tablelist1 = "@xpath=(//div[text()='";
		public static final String addedDRplan_tablelist2 = "']/following::table)[1]";

		public static final String addedDRplan_rangestart = "@xpath= (//div[text()='value']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@role='row']//div[@col-id='rangeStart']";
		public static final String addedDRplan_rangestart1 = "@xpath= (//div[text()='";
		public static final String addedDRplan_rangestart2 = "']/following::table)[1]//tr";

		// !-- DR Plans Bulk Interface --="@xpath=
		public static final String DRplans_bulkinterface_link = "@xpath= //a[text()='DR Plans Bulk Interface']";
		public static final String bulkinterfaceheader = "@xpath= //div[text()='Bulk Interface']";
		public static final String bulkjob_choosefilebutton = "@xpath= //label[text()='File:']/parent::div/parent::div/following-sibling::div//input";
		public static final String bulkjob_choosefilebutton1 = "@xpath= //input[@name='submitBulkJob']";

		public static final String bulkjobsubmit = "@xpath= //span[text()='Submit']";
		public static final String bulkinterface_archivelink = "@xpath= //div//a[text()='Archive']";
		public static final String bulkinterface_refreshlink = "@xpath=//div//a[text()='Refresh']";
		public static final String bulkinterfacepage_cancel = "@xpath= (//span[text()='Cancel'])[2]";
		public static final String bulkinterface_actiondropdown = "@xpath= //div[contains(text(),'Bulk Interface')]/following-sibling::div/div//button[text()='Action']";
		public static final String trunkgroupname_columnheader = "@xpath= (//span[text()='TrunkGroup Name'])[1]";
		public static final String username_columnheader = "@xpath= (//span[text()='User Name'])[1]";
		public static final String submittime_columnheader = "@xpath= (//span[text()='Submit Time'])[1]";
		public static final String starttime_columnheader = "@xpath= (//span[text()='Start Time'])[1]";
		public static final String endtime_columnheader = "@xpath= (//span[text()='End Time'])[1]";
		public static final String status_columnheader = "@xpath= (//span[text()='Status'])[1]";
		public static final String completion_columnheader = "@xpath= (//span[text()='Completion (%)'])[1]";
		public static final String log_columnheader = "@xpath= (//span[text()='Log'])[1]";
		public static final String uploadfile_columnheader = "@xpath= (//span[text()='Upload File'])[1]";
		public static final String addedbulkinterface_tablelist = "@xpath= (//div[@ref='eBodyContainer'])[1]";
		public static final String addedbulkinterface_trunkgroupname = "@xpath= (//div[@ref='eBodyContainer'])[1]/div[@role='row']/div[text()='value']";
		public static final String addedbulkinterface_trunkgroupname1 = "@xpath= (//div[@ref='eBodyContainer'])[1]/div[@role='row']/div[contains(text(),'";
		public static final String addedbulkinterface_trunkgroupname2 = "')]";
		public static final String addedbulkinterface_rowid = "@xpath= (//div[@ref='eBodyContainer'])[1]//div[text()='value']/parent::div[@role='row']";
		public static final String bulkinterface_trunkgroupname_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='tgName']";
		public static final String bulkinterface_username_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='user']";
		public static final String bulkinterface_submittime_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='submitTime']";
		public static final String bulkinterface_starttime_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='startTime']";
		public static final String bulkinterface_endtime_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='endTime']";
		public static final String bulkinterface_status_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='status']";
		public static final String bulkinterface_completion_value = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='completionPercentage']";
		public static final String bulkinterface_log = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='log']//div[text()='Log']";
		public static final String bulkinterface_file = "@xpath= //div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='fileName']/div/div";
		public static final String bulkinterface_backbutton = "@xpath= (//span[text()='Back'])[1]";
		public static final String archive_backbutton = "@xpath= (//span[text()='Back'])[2]";

		// !-- Download DR Plans --="@xpath=
		public static final String downloadDRplans_link = "@xpath= //div//a[text()='Download DR Plans']";// updated

		// !-- Commands Execution --="@xpath=
		public static final String commandsexecution_header = "@xpath= //div[text()='Commands Execution']";
		public static final String PSXcommand_label = "@xpath= //label[text()='PSX Command']";
		public static final String GSXcommand_label = "@xpath= //label[text()='GSX Command']";
		public static final String PSXcommand_dropdown = "@xpath= (//label[text()='PSX Command']/following-sibling::div//input)[1]";
		public static final String GSXcommand_dropdown = "@xpath= (//label[text()='GSX Command']/following-sibling::div//input)[1]";
		public static final String PSXcommand_executebutton = "@xpath= (//div[text()='Commands Execution']/parent::div/following-sibling::div//span[text()='Execute'])[1]";
		public static final String GSXcommand_executebutton = "@xpath= (//div[text()='Commands Execution']/parent::div/following-sibling::div//span[text()='Execute'])[2]";
		public static final String command_responsetext = "@xpath= //div[@class='container']";

		// !-- Execute configuration --="@xpath=
		public static final String labelName_localRingBackTone = "@xpath= //label[text()='Local Ring Back Tone']";
		public static final String GSXcongig_textArea = "@xpath= //textarea[@name='gsxConfigNote']";
		public static final String GSX_config_executeButton = "@xpath= //span[text()='Execute']";
		public static final String GSXconfig_sucessMessage = "@xpath= (//div[@role='alert']/span)[1]";
		public static final String deleteMessage = "@xpath= //div[@class='modal-body']";

		public static final String configurationpanel_header_executecommand = "@xpath= (//div[text()='Configuration'])[1]";
		// !-- Config panel --="@xpath=
		public static final String configurationpanel_header = "@xpath= (//div[text()='Configuration'])[2]";
		public static final String voiceCPEDevices_dropdown = "@xpath= (//label[text()='Voice CPE Devices']/parent::div//input)[1]";
		public static final String configuration_dropdownvalue = "@xpath= (//label[text()='Configuration']/parent::div//input)[1]";
		public static final String generateconfig_link = "@xpath= //a[text()='Generate Configuration']";
		public static final String saveconfig_link = "@xpath= //a[text()='Save Configuration']";
		public static final String executeconfigondevice_link = "@xpath= //a[text()='Execute Configuration on Device']";
		public static final String config_textarea = "@xpath= //textarea[@name='configurationDetails']";

	}

}
