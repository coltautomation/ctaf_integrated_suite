package pageObjects.aptObjects;

public class IMSNTranslatorObj {

	public static class IMSNT {

		public static final String AlertForServiceCreationSuccessMessage = "@xpath=//span[contains(text(),'Postcode successfully created.')]";
		public static final String AlertUploadNt = "@xpath=//span[contains(text(),'NT Service Area successfully updated.')]";
		
		public static final String AlertforUploadupdatefile = "@xpath=//div[@role='alert']/span";

		public static final String alertMsg_managePOstcode = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage_managePOstcode = "@xpath=(//div[@role='alert']/span)[1]";
		public static final String successmessageForUploadUpdateFile = "@xpath=//span[contains(text(),'Emergency Number successfully updated.PSX sync started successfully.')]";
		public static final String successMessageForEmergencyNumber = "@xpath=//span[contains(text(),'Emergency Number successfully created.PSX sync sta')]";

		public static final String historyTableValue = "@xpath=//pre";

		public static final String alertMsg = "@xpath=//div[@role='alert']/span";
		public static final String fetchListofCountryNames = "@xpath=//div[@class='ag-body-viewport-wrapper ag-layout-auto-height']//div[@role='row']/div[@col-id='name']";
		public static final String alertmessageUploadNT = "@xpath=//div[@role='alert']";
		public static final String BelMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[1]";
		public static final String DenMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[2]";
		public static final String FraMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[3]";
		public static final String GerMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[4]";
		public static final String PorMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[5]";
		public static final String SpaMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[6]";
		public static final String SweMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[7]";
		public static final String SwitMngPostcode = "@xpath=(//a[text()='Manage Postcodes'])[8]";
		public static final String ManageColtNetworkLink = "@xpath=//span/b[contains(text(),'MANAGE COLT')]";
		public static final String ManagePostcode = "@xpath=//li[text()='Manage Postcode for IMS platform']";
		public static final String WildcardText = "@xpath=//div[label[text()='You can use * as wildcard']]/label";
		public static final String Searchfield = "@xpath=(//input[@name='postcode'])";
		public static final String searchField_IMSNT = "@xpath=//input[@id='imsNumber']";
		public static final String Searchbtn = "@xpath=//span[contains(text(),'Search')]";
		public static final String EmergencyAreaID = "@xpath=//span[contains(text(),'Emergency Area ID')]";
		public static final String NTServiceArea = "@xpath=//span[contains(text(),'NT Service Area')]";
		public static final String EmergencyAreaSubcom = "@xpath=//span[contains(text(),'Emergency Area (Subcom)')]";

		public static final String NtServiceAreaLabel = "@xpath=//label[contains(text(),'NT Service Area')]";
		public static final String EmergencyAreaSubcomLabel = "@xpath=//label[contains(text(),'Emergency Area (SUBCOM)')]";
		public static final String EmergencyAreaIDSubcomLabel = "@xpath=//label[contains(text(),'Emergency Area ID(SUBCOM-ID)')]";

		public static final String ToimplementLabel = "@xpath=//label[contains(text(),'To Implement Switch')]";
		public static final String CityTransLabel = "@xpath=//label[contains(text(),'City Translator Number')]";
		public static final String SubArea1ProLabel = "@xpath=//label[contains(text(),'SubArea1 (Province)')]";
		public static final String SubArea1DLabel = "@xpath=//label[contains(text(),'SubArea1-ID (SubArea1-ID)')]";
		public static final String SubArea2CommuLabel = "@xpath=//label[contains(text(),'SubArea2 (Community)')]";
		public static final String SubArea2IDLabel = "@xpath=//label[contains(text(),'SubArea2-ID (SubArea2-ID)')]";
		public static final String SubArea3BLabel = "@xpath=//label[contains(text(),'SubArea3 (b)')]";
		public static final String SubAreaZipLabel = "@xpath=//label[contains(text(),'SubArea3-ID (ZIP code)')]";

		public static final String EmergencyKeyLabel = "@xpath=//label[contains(text(),'Emergency Number Key')]";
		public static final String ActualProviderLabel = "@xpath=//label[contains(text(),'Actual Provider Mapping')]";
		public static final String DummyCodeLAbel = "@xpath=//label[contains(text(),'Dummy Code')]";

		public static final String SubAreaProvince = "@xpath=//span[contains(text(),'SubArea1 (Province)')]";
		public static final String SubArea1_ID = "@xpath=//span[contains(text(),'SubArea 1-ID (SubArea1-ID)')]";
		public static final String SubArea2 = "@xpath=//span[contains(text(),'SubArea2 (Community)')]";
		public static final String SubArea2_ID = "@xpath=//span[contains(text(),'SubArea 2-ID (SubArea2-ID)')]";
		public static final String SubArea3 = "@xpath=//span[contains(text(),'SubArea 3 (b)')]";
		public static final String SubArea3_ID = "@xpath=//span[contains(text(),'SubArea 3-ID (Zipcode)')]";
		public static final String Lastupdate = "@xpath=//span[contains(text(),'Last Update ')]";
		public static final String Addpostcode = "@xpath=//a[contains(text(),'Add Postcode')]";
		public static final String Uploadupdatefile = "@xpath=//a[contains(text(),'Upload Update File')]";
		public static final String ViewHistory = "@xpath=//a[contains(text(),'View History')]";
		public static final String AddEmergencyNumber = "@xpath=//a[contains(text(),'Add Emergency Numbers')]";
		public static final String SynchronizeAllpostcodes = "@xpath=//a[contains(text(),'Synchronize All Postcodes')]";
		public static final String DownloadNt = "@xpath=//a[contains(text(),'Download NT Service Area')]";
		public static final String UploadNt = "@xpath=//a[contains(text(),'Upload NT Service Area')]";
		// public static final String
		// PostcodeCheckbox="@xpath=(//span[@class='ag-selection-checkbox'])";
		public static final String PostcodeCheckbox = "@xpath=//div[text()='value']/parent::div//span//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		
		public static final String ImsntCheckbox = "@xpath=(//div[text()='value']/parent::div//span//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";
		public static final String PostcodeAction = "@xpath=//button[text()='Action']";
		public static final String PostcodeActionView = "@xpath=(//a[contains(text(),'View')])[2]";
		public static final String PostcodeActionEdit = "@xpath=//a[contains(text(),'Edit')]";
		public static final String PostcodeActionDelete = "@xpath=//a[contains(text(),'Delete')]";
		public static final String ViewPostcodeEAID = "@xpath=//div[text()='11']";
		public static final String PostcodeBackbtn = "@xpath=//button/span[text()='Back']";
		public static final String PostcodeDeletebtn = "@xpath=//button[contains(text(),'Delete')]";
		public static final String Chosefile = "@xpath=(//input[@id='file'])";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String ViewHistoryPage = "@xpath=(//a[contains(text(),'value')])[1]";

		public static final String EmergencyAreaIDSubcom_TextField = "@xpath=//input[@id='postcodeName']";

		public static final String NTServiceAreaPostcode_Text = "@xpath=//input[@id='ntServiceArea']";

		public static final String ToimplemetSwitch_Text = "@xpath=//input[@id='toImplementSwitch']";

		public static final String CityTranslatorNmbr_Text = "@xpath=//input[@id='cityTranslatorNumber']";

		public static final String SubAreaProvince_1_Text = "@xpath=//input[@id='subArea1']";

		public static final String SubArea1D_Text = "@xpath=//input[@id='subArea1Id']";

		public static final String SubArea2Community_Text = "@xpath=//input[@id='subArea2']";

		public static final String SubArea2ID_Text = "@xpath=//input[@id='subArea2Id']";

		public static final String SubArea3B_Text = "@xpath=//input[@id='subArea3']";

		public static final String SubAreaZipcode_Text = "@xpath=//input[@id='subArea3Id']";

		public static final String EmergencyAreaSub_Text = "@xpath=//input[@id='emergencyAreaDescription']";

		public static final String EmergencyNmbrKey_Text = "@xpath=//input[@id='enPhoneText']";

		public static final String ActualProvider_Text = "@xpath=//input[@id='enMappingText']";

		public static final String DummyCode_Text = "@xpath=//input[@id='enCodeText']";
		public static final String EmptyTextbox = "@xpath=(//select[@name='selectedNumber'])";
		public static final String forwardarrow = "@xpath=//span[contains(text(),'>>')]";
		public static final String BackwardArrow = "@xpath=(//button[contains(@class,'btn btnSquar btnWitdh undefined btn-secondary')])[2]";
		public static final String OKbtn = "@xpath=//span[contains(text(),'Ok')]";
		// public static final String
		// Cancelbtn="@xpath=//span[contains(text(),'Cancel')]";
		public static final String xbutton = "@xpath=//span[text()='�']";
		public static final String OkbtnPostcode = "@xpath=//span[contains(text(),'OK')]";
		public static final String FileNamefield = "@xpath=(//label[contains(text(),'File Name')])";
		public static final String Datefield = "@xpath=(//label[contains(text(),'Date')])";
		public static final String UserField = "@xpath=(//label[contains(text(),'User')])";
		public static final String Updatebyfield = "@xpath=(//label[contains(text(),'Updated By')])";
		// public static final String !-- Denmark --="@xpath=
		public static final String SubArea1_ID_Denmark = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String SubArea2_ID_Denmark = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String EmergencyArea_Kommune = "@xpath=//span[contains(text(),'Emergency Area(Kommune)')]";
		public static final String SubArea1Den = "@xpath=//span[contains(text(),'SubArea1(SubArea1)')]";
		public static final String SubArea2Den = "@xpath=//span[contains(text(),'SubArea2(SubArea2) ')]";
		public static final String SubArea3Den = "@xpath=//span[contains(text(),'SubArea3(SubArea3) ')]";
		public static final String SubArea3DenArea = "@xpath=//span[contains(text(),'SubArea3-ID(SubArea3-ID) ')]";
		public static final String SubArea1DenLabel = "@xpath=//label[contains(text(),'SubArea1 (SubArea1)')]";
		public static final String SubArea3Denlabel = "@xpath=//label[contains(text(),'SubArea3 (SubArea3)')]";
		public static final String SubArea2DenLabel = "@xpath=//label[contains(text(),'SubArea2 (SubArea2)')]";
		public static final String SubArea3IDDenLabel = "@xpath=//label[contains(text(),'SubArea3-ID (SubArea3-ID)')]";
		public static final String EmergencyArea_Kommunelabel = "@xpath=//label[contains(text(),'Emergency Area (Kommune)')]";
		public static final String EmergencyKommunerlabel = "@xpath=//label[contains(text(),'Emergency Area ID (Kommunenummer)')]";
		public static final String EmergencyAreaFranceLabel = "@xpath=//label[contains(text(),'Emergency Area (City (Area))')]";
		// public static final String !-- France --="@xpath=
		public static final String SubArea2_ID_France = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String EmergencyAreaIDFranceLabel = "@xpath=//label[contains(text(),'Emergency Area ID (INSEE Code)')]";
		public static final String EmergencyAreaCityArea = "@xpath=//span[contains(text(),'Emergency Area(City(Area))')]";
		public static final String SubArea1Fra = "@xpath=//span[contains(text(),'SubArea1(Department)')]";
		public static final String Department_ID = "@xpath=//span[contains(text(),'SubArea1-ID(Department-ID)')]";
		public static final String SubArea2Fra = "@xpath=//span[contains(text(),'SubArea2(SubArea2) ')]";
		public static final String SubArea3FraArea = "@xpath=//span[contains(text(),'SubArea3-ID(SubArea3-ID) ')]";
		public static final String SubAreaDepartFrance = "@xpath=//label[contains(text(),'SubArea1 (Department)')]";
		public static final String SubArea1DFranLabel = "@xpath=//label[contains(text(),'SubArea1-ID (Department ID)')]";
		// !--Germany //
		public static final String SubArea1_ID_Germany = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String SubArea2_ID_Germany = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";

		public static final String EmergencyAreaLocal = "@xpath=//span[contains(text(),'Emergency Area(Local Area Name (U_ON_NAME))')]";
		public static final String SubArea1Ger = "@xpath=//span[contains(text(),'SubArea1(SubArea1)')]";
		public static final String SubArea2Ger = "@xpath=//span[contains(text(),'SubArea2(SubArea2)')]";
		public static final String EmergencyAreaIDGermanyLabel = "@xpath=//label[contains(text(),'Emergency Area ID (Local Area ID (U_ONKZ))')]";
		public static final String EmergencyAreaLocalGermany = "@xpath=//label[contains(text(),'Emergency Area (Local Area Name (U_ON_NAME))')]";
		// public static final String !--Portugal --="@xpath=
		public static final String SubArea1_ID_Portugal = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String SubArea2_ID_Portugal = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String EmergencyAreaProvGermanyLAbel = "@xpath=//label[contains(text(),'Emergency Area (Province)')]";
		public static final String EmergencyAreaIDPortugalLabel = "@xpath=//label[contains(text(),'Emergency Area ID (CLISRVPF INDEX)')]";
		public static final String EmergencyAreaProvince = "@xpath=//span[contains(text(),'Emergency Area(Province)')]";
		// public static final String !-- Spain --="@xpath=
		public static final String SubArea2_ID_Spain = "@xpath=//span[contains(text(),'SubArea2-ID(SubArea2-ID)')]";
		public static final String EmergencyAreaID_SpainLabel = "@xpath=//label[contains(text(),'Emergency Area ID (Municip.Code)')]";
		public static final String SubAreaProvinciaSpainLabel = "@xpath=//label[contains(text(),'SubArea1 (Provincia)')]";
		public static final String SubArea1DSpainLabel = "@xpath=//label[contains(text(),'SubArea1-ID (Prov.Code)')]";
		public static final String EmergencyAReaMunc_Label = "@xpath=//label[contains(text(),'Emergency Area (Municipality)')]";
		public static final String EmergencyAreaMunicipality = "@xpath=//span[contains(text(),'Emergency Area(Municipality)')]";
		public static final String SubArea1Provincia = "@xpath=//span[contains(text(),'SubArea1(Provincia)')]";
		public static final String SubArea1DSpa = "@xpath=//span[contains(text(),'SubArea1-ID(Prov.Code)')]";
		// public static final String !--Sweden --="@xpath=
		public static final String SubArea1_ID_Sweden = "@xpath=//span[contains(text(),'SubArea1-ID(SubArea1-ID)')]";
		public static final String EmergencyAreaIDSweden_Label = "@xpath=//label[contains(text(),'Emergency Area ID (Kommun-ID/ALARM-ID)')]";
		public static final String SubAreaLanSweden_Label = "@xpath=//label[contains(text(),'SubArea1 (Lan)')]";
		public static final String SubArea2DSweden_Label = "@xpath=//label[contains(text(),'SubArea2-ID (ZIP)')]";
		public static final String EmergencyAreaKommun_Label = "@xpath=//label[contains(text(),'Emergency Area (Kommun)')]";

		public static final String EmergencyAreaKommun = "@xpath=//span[contains(text(),'Emergency Area(Kommun)')]";
		public static final String SubArea1Lan = "@xpath=//span[contains(text(),'SubArea1(Lan)')]";

		// public static final String !-- Switzerland --="@xpath=
		public static final String EmergencyAreaIDSwitLabel = "@xpath=//label[contains(text(),'Emergency Area ID (Emergency Area ID)')]";
		public static final String SubArea1Caton_Label = "@xpath=//label[contains(text(),'SubArea1 (Canton)')]";
		public static final String SubArea1DGDEKT = "@xpath=//label[contains(text(),'SubArea1-ID (GDEKT)')]";
		public static final String SubAreaDistrictLabel = "@xpath=//label[contains(text(),'SubArea2 (District)')]";
		public static final String SuArea2IDSwit = "@xpath=//label[contains(text(),'SubArea2-ID (GDEBZNR)')]";
		public static final String EmergencyAreaSwit = "@xpath=//label[contains(text(),'SubArea2-ID (GDEBZNR)')]";
		public static final String SubArea3MunLabel = "@xpath=//label[contains(text(),'SubArea3(Municipality)')]";
		public static final String EmergencyArea = "@xpath=//span[contains(text(),'Emergency Area(Emergency Area)')]";
		public static final String SubAreaCanton = "@xpath=//span[contains(text(),'SubArea1(Canton)')]";
		public static final String SuAreaGDEKT = "@xpath=//span[contains(text(),'SubArea1-ID(GDEKT)')]";
		public static final String SubAreaDistrict = "@xpath=//span[contains(text(),'SubArea2(District) ')]";
		public static final String SubAreaGDE = "@xpath=//span[contains(text(),'SubArea2-ID(GDEBZNR) ')]";
		public static final String SubArea3Mun = "@xpath=//span[contains(text(),'SubArea3(Municipality) ')]";
		public static final String SubArea3Swit = "@xpath=//span[contains(text(),'SubArea3-ID(SubArea3-ID) ')]";

		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";
		public static final String deletebutton = "@xpath=//button[@class='btn btn-danger']";

		// public static final String !-- IMSNT --="@xpath=
		public static final String OperationDD = "@xpath=//span[@role='option']";
		public static final String TranDeltemsg = "@xpath=//span[contains(text(),'NumberTranslation successfully marked for deletion.')]";
		public static final String SyncTransMsg = "@xpath=//span[text()='Sync started successfully. Please check the sync status of number translation.']";
		public static final String UpdateTransmsg = "@xpath=//span[text()='NumberTranslation successfully updated.Sync started successfully. Please check the sync status of number translation.']";
		// public static final String ActionDD="@xpath=//a[@role='button']";
		public static final String Translationmsg = "@xpath=//span[contains(text(),'NumberTranslation successfully created.Sync started successfully. Please check the sync status of number translation.')]";
		public static final String listofaddressDropDown = "@xpath=//div/span[@role='option']";
		public static final String ActCountrycode = "@xpath=//label[contains(text(),'Country Code')]/following-sibling::*[1]";
		public static final String Wildcardmsg = "@xpath=//span[contains(text(),'The following errors were encountered: At least 3 digits to be entered in the search field.')]";
		public static final String ManageIms = "@xpath=//li[text()='Manage IMS Number Translation']";
		public static final String SEManageTran = "@xpath=(//a[text()='Manage Number Translation'])[1]";
		public static final String UKManageTran = "@xpath=(//a[text()='Manage Number Translation'])[2]";
		public static final String BRManageTran = "@xpath=(//a[text()='Manage Number Translation'])[3]";
		public static final String PTManageTran = "@xpath=(//a[text()='Manage Number Translation'])[4]";
		public static final String CHManageTran = "@xpath=(//a[text()='Manage Number Translation'])[5]";
		public static final String IEManageTran = "@xpath=(//a[text()='Manage Number Translation'])[6]";
		public static final String ATManageTran = "@xpath=(//a[text()='Manage Number Translation'])[7]";
		public static final String ITManageTran = "@xpath=(//a[text()='Manage Number Translation'])[8]";

		public static final String NumberToTranslate = "@xpath=//span[contains(text(),'Number To Translate')]";
		public static final String NumberToTranslated = "@xpath=//span[contains(text(),'Number Translated')]";
		public static final String Country = "@xpath=//span[contains(text(),'Country')]";
		public static final String Carrier = "@xpath=//span[contains(text(),'Carrier')]";
		public static final String CarrierLabel = "@xpath=//label[contains(text(),'Carrier')]";
		public static final String NatureofAddress = "@xpath=//span[contains(text(),'Nature Of Address')]";
		public static final String NatureofAddress_1 = "@xpath=//label[contains(text(),'Nature of Address')]";
		public static final String PSXsyncstatus = "@xpath=//span[contains(text(),'PSX Sync Status')]";
		public static final String AddnumberTranslation = "@xpath=//a[contains(text(),'Add Number Translation')]";
		public static final String ViewUploadHistory = "@xpath=//a[contains(text(),'View Upload History')]";
		public static final String ViewUIHistory = "@xpath=//a[contains(text(),'View UI History')]";
		public static final String DownloadNumberTranslation = "@xpath=//a[contains(text(),'Download Number Translation')]";
		public static final String SynchronizeAll = "@xpath=//a[contains(text(),'Synchronize All')]";
		public static final String TranslationWildcard = "@xpath=//div[label[text()='You can use * as wildcard']]";
		public static final String Numbertranslate = "@xpath=//label[contains(text(),'Number to Translate')]";
		public static final String Countrycode = "@xpath=//label[contains(text(),'Country Code')]";
		public static final String Prefix = "@xpath=//label[contains(text(),'Prefix')]";
		public static final String Countrylabel = "@xpath=//label[contains(text(),'Country')]/following-sibling::*[1]";
		public static final String NatureofAddressLabel = "@xpath=//label[contains(text(),'Nature of Address')]";
		public static final String NumberToTranslatedLabel = "@xpath=//label[contains(text(),'Number Translated')]";
		public static final String RangeLabel = "@xpath=//label[contains(text(),'Range')]";
		public static final String sequenceLabelName = "@xpath=//label[contains(text(),'Sequence')]";
		public static final String CountrycodeValue = "@xpath=//label[contains(text(),'Country Code')]/following-sibling::*[1]";
		public static final String Natureofadddropdown = "@xpath=(//div[label[text()='Nature of Address']])[1]//input";
		public static final String Numbertotranslate_Text = "@xpath=//input[@id='numbertotranslate']";
		public static final String Numbertotranslate_Search = "@xpath=//input[@id='numbertotranslatesearch']";
		public static final String Country_Text = "@xpath=//input[@id='country']";
		public static final String Numbertranslated_Text = "@xpath=//input[@id='numbertranslated']";
		public static final String Numbertranslated_Text1 = "@xpath=//input[@id='numbertranslated0']";

		public static final String editPage_NumberToTranslateElement = "@xpath=//div[label[text()='Number to Translate']]//div";
		public static final String editPage_countryCodeElement = "@xpath=//div[label[text()='Country Code']]//div";
		public static final String RangeFlag = "@xpath=//input[@id='rangeFlag']";
		public static final String sequencetextField = "@xpath=//div[label[text()='Sequence']]//input";
		public static final String ActionDD = "@xpath=//a[@role='button']";
		public static final String TranslationView = "@xpath=(//a[contains(text(),'View')])[3]";
		public static final String Closesign = "@xpath=//span[contains(text(),'�')]";
		public static final String ddiPrefix_checkbox = "@xpath=//div[label[text()='DDI Prefix']]//input";
		public static final String prefix_checkbox = "@xpath=//input[@id='ddiPrefixFlag']";
		public static final String PrefixText = "@xpath=//input[@id='prefix']";
		public static final String PrefixText1 = "@xpath=//label[@class='form-label'][contains(text(),'Prefix')]";

		public static final String Carrier_Text = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30']";
		public static final String carrierDropdown = "@xpath=//div[label[text()='Carrier']]//input";
		public static final String viewPage_CountryField = "@xpath=(//div[label[contains(text(),'Country')]]/div[1])";
		public static final String Synchronize = "@xpath=(//a[contains(text(),'Synchronize')])[1]";
		public static final String ImsSearcfield = "@xpath=//input[@id='imsNumber']";
		public static final String NoAddDD = "@xpath=//div[(text()='�')]";
		public static final String natureOfAddressDropdown = "@xpath=(//div[label[text()='Nature of Address']]//input)[1]";
		public static final String NoAddTextbox = "@xpath=//label[contains(text(),'Nature of Address')]/following-sibling::*[2]";
		public static final String francefileupload = "@xpath=ChosefileTrans";
		public static final String ChosefileTrans = "@xpath=(//input[@id='file'])";
		public static final String ChosefileTransImsnt = "@xpath=(//input[@id='fileupload'])";
		public static final String CountryPresent = "@xpath=(//div[@class='ag-body-container ag-layout-normal']//div[@role='row'])";
		public static final String ViewUploadHisClose = "@xpath=//span[(text()='�')]";
		public static final String Operation = "@xpath=//label[contains(text(),'Operation')]";
		public static final String ViewUIUser = "@xpath=//label[contains(text(),'User')]";
		public static final String ViewUINumbertoTrans = "@xpath=//label[contains(text(),'Number to Translate')]";
		public static final String operationDropdown_viewUIhistory = "@xpath=(//div[label[text()='Operation']]//input)[1]";
		public static final String OperationText = "@xpath=//input[@class='react-dropdown-select-input css-1j547uo-InputComponent e11wid6y0']";
		public static final String ViewUISearch = "@xpath=(//span[contains(text(),'Search')])[2]";
		public static final String UserText = "@xpath=//input[@id='user']";
		public static final String OperationText_1 = "@xpath=//label[contains(text(),'Operation')]/following-sibling::*";
		// public static final String !-- public static final String
		// OperationText_1="@xpath=//div[@class='react-dropdown-select sc-bdVaJa
		// ibTDPh css-11x0fbf-ReactDropdownSelect e1gzf2xs0']";
		public static final String OperationText_2 = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30']";

		public static final String OperationTextClick = "@xpath=//div[contains(text(),'�')]";
		public static final String numberToTranslateColumn_filter = "@xpath=(//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String numberToTranslate_textField = "@xpath=//input[@id='filterText']";

		public static final String ServiceMessg = "@xpath=//span[contains(text(),'Service successfully created.')]";
		public static final String ServiceIdentification = "@xpath=//input[@id='serviceIdentification']";
		// public static final String
		// forwardarrow="@xpath=//span[contains(text(),'forwardarrow_1')]";
		public static final String forwardarrow_1 = "@xpath=(//span[contains(text(),'>>')])[2]";
		public static final String EmailText = "@xpath=//div[label[contains(text(),'Email')]]//input";
		public static final String EmailLabel = "@xpath=//label[contains(text(),'Email')]";
		public static final String PhoneContact = "@xpath=//input[@id='phoneContact']";
		public static final String RemarkText = "@xpath=//textarea[@name='remark']";
		public static final String RemarkLabel = "@xpath=//label[contains(text(),'Remark')]";
		public static final String PerformancereportingLabel = "@xpath=//label[contains(text(),'Performance Reporting')]";
		public static final String performanceReportingcheckbox = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String ServiceIdentificationLabel = "@xpath=//label[contains(text(),'Service Identification')]";
		public static final String ServiceTypeLabel = "@xpath=//label[contains(text(),'Service Type')]";
		public static final String PhoneContactLabel = "@xpath=//label[contains(text(),'Phone Contact')]";
		// public static final String
		// PerformancereportingLabel="@xpath=//label[contains(text(),'Performance
		// Reporting')]";
		public static final String WaveServicelabel = "@xpath=//label[contains(text(),'Wave Service')]";
		public static final String waveServiceCheckbox = "@xpath=//div[label[contains(text(),'Wave Service')]]//input";
		public static final String serviceIdentificationerrmsg = "@xpath=//div[contains(text(),'Service Identification')]";
		public static final String OKbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String Cancelbtn = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String OrderContractNmbr = "@xpath=(//label[contains(text(),'Order/Contract Number (Parent SID)')])[2]";
		public static final String RFIRFQ = "@xpath=(//label[contains(text(),'RFI/RFQ/IP Voice Line Number')])[2]";
		public static final String RFIRFQ_Text = "@xpath=//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderUpdatemsg = "@xpath=//span[text()='Order successfully updated']";
		public static final String Selectorderlabel = "@xpath=//label[text()='Select Order']";
		public static final String AddDeviceTogglebutton = "@xpath=(//div[div[label[text()='Add Device']]]//div[@class='react-switch-handle'])[1]";
		public static final String OrderContractNumbertoggleButton = "@xpath=(//div[div[label[text()='Select Order']]]//div[@class='react-switch-handle'])[1]";
		public static final String OrderName = "@xpath=//input[@id='orderName']";
		public static final String createButton = "@xpath=//button[@class='btn btnSquar btnWitdh undefined btn-secondary   ']";
		public static final String CreateOrderSuccessMessage = "@xpath=//span[contains(text(),'Order created successfully')]";
		public static final String OrderChangemsg = "@xpath=//span[contains(text(),'Order successfully  changed.')]";
		public static final String AddDevice = "@xpath=//a[text()='Add Device']";
		public static final String DeviceNameLabel = "@xpath=//label[text()='Device Name']";
		public static final String VendorLabel = "@xpath=//label[text()='Vendor/Model']";
		public static final String ManagementAddLabel = "@xpath=//label[text()='Management Address']";
		public static final String SnmproLabel = "@xpath=//label[text()='Snmpro']";
		// public static final String
		// Countrylabel="@xpath=//label[text()='Country']";
		public static final String CityLabel = "@xpath=//label[text()='City']";
		public static final String SiteLabel = "@xpath=//label[text()='Site']";
		public static final String PremiseLabel = "@xpath=//label[text()='Premise']";
		public static final String DeviceNameText = "@xpath=//input[@id='deviceName']";
		public static final String ManagementAddText = "@xpath=//input[@id='addressSelect']";
		public static final String Deviceemsg = "@xpath=//span[contains(text(),'Device successfully created.')]";
		public static final String Devicedeleteemsg = "@xpath=//span[contains(text(),'Device successfully deleted')]";
		public static final String InterfaceSpeedDropdown = "@xpath=(//div[label[text()='Interface Speed']]//input)[1]";
		public static final String TypeofServiceDropdown = "@xpath=(//div[label[text()='Type Of Service']]//input)[1]";
		public static final String ChangeordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number (Parent SID)']]//input)[1]";
		public static final String AddDeviceName = "@xpath=(//div[label[text()='Name']]//input)[1]";
		public static final String Xbutton = "@xpath=//div[text()='�']";
		public static final String Backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String Synchronizemsg = "@xpath=//span[contains(text(),'Sync started successfully. Please check the sync status of this service.')]";
		public static final String Errormsg = "@xpath=//div[text()='Error']";

		public static final String searchDDi_actionDropdown = "@xpath=//button[text()='Action']";
		public static final String searchDDI_viewLink = "@xpath=//a[text()='View']";

		public static final String selectTrunkName = "@xpath=//div[text()='value']";
		public static final String FileuploadSuccess = "@xpath=//span[contains(text(),'File Upload successfull')]";
		public static final String TrunkPanelView = "@xpath=(//a[text()='View'])";
		public static final String TrunkPanelAction = "@xpath=(//button[text()='Action'])[20]";
		public static final String TrunkValue = "@xpath=//div[text()=' + TrunkValue + ']";
		// public static final String
		// OKbutton="@xpath=//span[contains(text(),'OK')]";
		// public static final String
		// Cancelbtn="@xpath=//span[contains(text(),'Cancel')]";
		public static final String DuplicateRecord = "@xpath=//span[contains(text(),'* Showing Duplicate DDI Ranges.')]";
		public static final String NoDuplicateRecord = "@xpath=//span[contains(text(),' * No Duplicate DDI Ranges Found.')]";
		public static final String DDICount = "@xpath=//span[contains(text(),'DDI Range Count:')]";

		public static final String DDIpanelHeader = "@xpath=//div[h5[text()='DDI Range']]";
		public static final String ChosefileDDI = "@xpath=(//input[@name='submitBulkJob'])";
		public static final String ManageCustomerServiceLink = "@xpath=//span[contains(text(),'Manage Customer's Service')]";
		public static final String SearchService = "@xpath=//li[text()='Search Order/Service']";
		public static final String TrunkName = "@xpath=//input[@id='trunkGrouptgName']";
		// public static final String
		// Searchbtn="@xpath=//span[text()='Search']";
		public static final String DDICheckbox = "@xpath=(//span[@class='ag-icon ag-icon-checkbox-unchecked'])[8]";
		public static final String CountrycodeLabel = "@xpath=(//span[text()='Country Code'])";
		public static final String LacLabel = "@xpath=//span[text()='LAC']";
		public static final String MainNumberLabel = "@xpath=(//span[text()='Main Number/Range Start-End'])";
		public static final String ExtensionLabel = "@xpath=(//span[text()='Extension digits'])";
		public static final String EmergLabel = "@xpath=(//span[text()='Emerg Area'])";
		public static final String IncomingLabel = "@xpath=(//span[text()='Incoming Routing'])";
		public static final String InGeoLabel = "@xpath=(//span[text()='IN GEO'])";
		public static final String CreateInGeo = "@xpath=//label[text()='IN GEO']";
		public static final String ActivateIncomingRouting = "@xpath=//label[text()='Activate Incoming Routing']";
		public static final String CreateDDIExtension = "@xpath=//label[text()='Extension digits']";
		public static final String CreateDDIRangeEnd = "@xpath=//label[text()='Range End']";
		public static final String CreateDDIRangeStart = "@xpath=//label[text()='Range Start']";
		public static final String CreateDDIMainnumber = "@xpath=//label[text()='Main Number']";
		public static final String CreateDDIPhoneNumbers = "@xpath=//label[text()='Phone Numbers']";
		public static final String CreateDDICountry = "@xpath=//label[text()='Country Code']";
		public static final String CreateDDILAC = "@xpath=//label[text()='LAC']";
		public static final String AddMore = "@xpath=//span[text()='Add More']";
		public static final String CountryCodeText = "@xpath=//input[@id='countryCode']";
		public static final String LacText = "@xpath=//input[@id='lac']";
		public static final String MainnumberText = "@xpath=//input[@id='mainNumber']";
		public static final String RangeStartText = "@xpath=//input[@id='rangeStart']";
		public static final String RangeEndText = "@xpath=//input[@id='rangeEnd']";
		public static final String ExtensionText = "@xpath=//input[@id='extensionDigitsAdd']";
		// public static final String
		// forwardarrow="@xpath=//span[contains(text(),'>>')]";
		public static final String CheckboxRouting = "@xpath=//input[@id='incomingRoutingCheckAdd']";
		public static final String CheckboxInGeo = "@xpath=//input[@id='ingeoCheckAdd']";
		public static final String DDIcreationMsg = "@xpath=//span[text()='DDI Range successfully created.']";
		public static final String LACFilter = "@xpath=(//span[@class='ag-icon ag-icon-menu'])[1]";
		public static final String LACDropDown = "@xpath=//select[@id='filterType']";
		public static final String LACFilter_Text = "@xpath=//input[@id='filterText']";
		public static final String DDIRadiobtn = "@xpath=//input[@type='radio']";
		public static final String Deletebtn = "@xpath=//button[text()='Delete']";
		public static final String DDIEdit = "@xpath=(//a[text()='Edit'])";
		public static final String DDIDelete = "@xpath=(//a[text()='Delete'])";
		public static final String DDIPanelAction = "@xpath=(//button[text()='Action'])[3]";
		public static final String AddDDIRange = "@xpath=//span[text()='Add DDI Range']";
		public static final String DuplicateDDIRange = "@xpath=//span[text()='Show Duplicate DDI Ranges']";
		public static final String DownlaodDDIRanges = "@xpath=//span[text()='Download DDI Ranges']";
		public static final String UploadDDIRanges = "@xpath=//span[text()='Upload DDI Ranges']";
		public static final String ShowDDIRanges = "@xpath=//span[text()='Show DDI Ranges']";
		public static final String countryLabel_viewPage = "@xpath=//label[text()='Country']";
		public static final String LAClabel_viewPage = "@xpath=//label[text()='LAC']";
		public static final String extensionLabel_viewPage = "@xpath=//label[text()='Extension digits']";
		public static final String ConfigurationLabel = "@xpath=//div[text()='Configuration']";
		public static final String PsxConfigurationLabel = "@xpath=//label[text()='PSX Configuration']";
		public static final String PhoneNumerLabel = "@xpath=//label[text()='Phone Numbers']";
		public static final String ViewEmergLabel = "@xpath=//label[text()='Emerg Area']";
		// public static final String Backbutton="@xpath=//span[text()='Back']";
		public static final String DDIRangeUpdate = "@xpath=//span[text()='DDI Range successfully updated.']";
		// public static final String
		// ManageColtNetworkLink="@xpath=//span[contains(text(),'Manage Colt's
		// Network')]";
		public static final String SearchDDI = "@xpath=//li[text()='Search DDI Ranges']";
		public static final String DDIWildcard = "@xpath=//label[text()='(You can use % as wildcard)']";
		public static final String DDIISdcode = "@xpath=//label[text()='ISD Code(Country Code)']";
		public static final String DDISTdcode = "@xpath=//label[text()='STD Code(LAC) ']";
		public static final String DDIManageNmbr = "@xpath=//label[text()='Main Number/Range Start-End']";
		public static final String DDIISdcode_Text = "@xpath=//input[@id='isdcode']";
		public static final String DDILAC_Text = "@xpath=//input[@id='stdcode']";
		public static final String Action = "@xpath=//button[text()='Action']";

		public static final String viewPage_countryValue = "@xpath=//div[div[div[div[label[text()='Country']]]]]//div[@class='customLabelValue form-label']";
		public static final String viewPage_LACvalue = "@xpath=//div[div[div[div[label[text()='LAC']]]]]//div[@class='customLabelValue form-label']";
		public static final String viewPage_extensionDigitVale = "@xpath=//div[div[div[div[label[text()='Extension digits']]]]]//div[@class='customLabelValue form-label']";
		public static final String viewPage_activateIncomingRouting = "@xpath=//div[div[div[div[label[text()='Activate Incoming Routing']]]]]//div[@class='customLabelValue form-label']";
		public static final String viewPage_INGEOValue = "@xpath=//div[div[div[div[label[text()='IN GEO']]]]]//div[@class='customLabelValue form-label']";

		public static final String PSXconfigurationDropdown_viewDDI = "@xpath=(//div[label[text()='PSX Configuration']]//input)[1]";
		public static final String executeButton_vieDDI = "@xpath=//div[div[div[label[text()='PSX Configuration']]]]//following-sibling::div//button/span";
		public static final String alertMSG_viewPage = "@xpath=//div[@role='alert']/span/li";

		public static final String viewLink_selectAddedDDI = "@xpath=(//div[div[h5[text()='DDI Range']]]//following-sibling::div//div[div[text()='43']])[1]//a[text()='value']";
		public static final String selectLACvalue_searchFoeDDipage = "@xpath=(//div[@role='row']//div[text()='value'])[1]";
		public static final String ssearchForDDI_viewLink = "@xpath=//a[text()='View']";
		public static final String searchForDDI_actionDropdown = "@xpath=//button[text()='Action']";
		
		// IpvpnSwift
		public static final String IpvpnTypelabel = "@xpath=//label[text()='IP VPN Type']";
		public static final String IpvpnDrop = "@xpath=//div[label[contains(text(),'IP VPN Type')]]//input";
		public static final String Next_Button = "@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";

		
		public static final String Statisticslink = "@xpath=//span[contains(text(),'Statistics')]";
		public static final String ServiceStatistics = "@xpath=//li[contains(text(),'Service Statistics')]";
		public static final String VoiceStatistics = "@xpath=//li[contains(text(),'VOIP Statistics')]";
		public static final String IPAccesStatistics = "@xpath=//li[contains(text(),'IPAccess Statistics')]";
		public static final String ServicePageHeader = "@xpath=//p[contains(text(),'Search Service Statistics')]";
		public static final String ServiceDateFrm = "@xpath=//label[text()='Service Creation Date From']";
		public static final String ServiceDateto = "@xpath=//label[text()='Service Creation Date To']";
		public static final String VoipPageHeader = "@xpath=//p[text()='Search VOIP Statistics']";
		public static final String VoipDatefrom = "@xpath=//label[text()='VOIP Enabling Date From']";
		public static final String VoipDateto = "@xpath=//label[text()='VOIP Enabling Date To']";
		public static final String IpAccessHeader = "@xpath=//div[text()='Discovery Report Search']";
		public static final String IPAccessDelivery = "@xpath=//label[text()='Discovery Reports']";
		public static final String DiscoverDtFrm = "@xpath=//label[text()='Discovery Date From']";
		public static final String DiscveryDTto = "@xpath=//label[text()='Discovery Date To']";
		public static final String GenrateButton = "@xpath=//span[text()='Generate Report']";
	

		public static final String order_contractnumberErrmsg = "@xpath=(//span[text()='Order/Contract Number(Parent SID) is required'])[1]";
		public static final String servicetypeerrmsg = "@xpath=//span[contains(text(),'Service type is required')]";
		public static final String interfaceSpeedErrmsg = "@xpath=//span[contains(text(),'Interface Speed is required')]";
		public static final String servicesubtypeErrMsg = "@xpath=//span[contains(text(),'Sub service type is required')]";

		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String chooseCustomer_build3 = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";

		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Customer']]//input";
		public static final String ordererrormsg = "@xpath=(//span[text()='Order/Contract number is required'])[1]";
		public static final String Servicetypeerrmsg = "@xpath=//span[contains(text(),'Service type is required')]";

		public static final String CreateOrderService_Text = "@xpath=//li[text()='Create Order Service']";
		public static final String ChooseCustomer_Select = "@xpath=(//input[@placeholder='Select...'])[2]";
		// public static final String
		// Next_Button="@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";
		public static final String OrderContractNumber_Select = "@xpath=(//input[@placeholder='Select...'])[2]";
		public static final String ordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']]//input)[1]";
		public static final String ordercontractNumberdropdown_secondTime = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']]//input)[1]";

		public static final String OrderContractNumber_Select_build3 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String ServiceType_Select1_build3 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[2]";
		public static final String servicetypedropdowntoclick = "@xpath=//div[label[text()='Service Type']]//input";
		public static final String orderdropdown = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']])[1]//input";

		public static final String intefacespeederrormsg = "@xpath=//span[contains(text(),'Interface Speed is required')]";
		public static final String servicesubtypeerrmsg = "@xpath=//span[contains(text(),'Sub service type is required')]";
		public static final String ServiceType_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'][1])[2]";
		public static final String listOfservicetypes = "@xpath=//div/span[@role='option']";

		public static final String NotificationManagementTeam_TransmissionLink = "@xpath=//select[@id='notificationManagementTeam']";
		public static final String ServiceType = "@xpath=//div[@class='customLabelValue form-label']";
		public static final String Email = "@xpath=//input[@id='email']";
		public static final String Performancereporting = "@xpath=//input[@id='proactiveMonitoring']";
		public static final String proActivenotification_TransmissionLink = "@xpath=//input[@id='proactiveNotification']";
		
		public static final String AddMASdevice = "@xpath=//a[contains(text(),'Add MAS Switch')]";
		public static final String AddPEDevice = "@xpath=//a[contains(text(),'Add PE Device')]";
		public static final String SelectIMSlocation_MAS = "@xpath=//select[@id='imsPOP']";
		public static final String country = "@xpath=//select[@id='country']";
		public static final String city = "@xpath=//select[@id='city']";
		public static final String site = "@xpath=//select[@id='site']";
		public static final String premise = "@xpath=//select[@id='premise']";

		// public static final String !-- VOIP Access --="@xpath=
		public static final String serviceType_VOIPaccess = "@xpath=//div[contains(text(),'VOIP Access')]";
		public static final String ResellerCode = "@xpath=//input[@id='resellerCode']";
		public static final String managementOptions_Package = "@xpath=//select[@id='package']";
		public static final String managementOptions_managesservice = "@xpath=//div[label[contains(text(),'Managed Service')]]//input";
		public static final String managementOptions_SyslogEventView = "@xpath=//div[label[contains(text(),'Syslog Event View')]]//input";
		public static final String managementOptions_serviceStatusView = "@xpath=//div[label[contains(text(),'Service Status View')]]//input";
		public static final String managementOptions_RouterConfigView = "@xpath=//div[label[contains(text(),'Router Configuration View')]]//input";
		public static final String managementOptions_PerformnceReport = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String managementOptions_DialsUserAdmin = "@xpath=//div[label[contains(text(),'Dial User Administration')]]//input";
		public static final String managementOptions_ProActiveNotify = "@xpath=//div[label[contains(text(),'Dial User Administration')]]//input";
		public static final String managementOptions_NotifyManageTeam = "@xpath=//select[@id='deliveryChannel']";

		// public static final String !-- NGIN --="@xpath=
		public static final String managementOptions_CustomAdmin = "@xpath=//input[@id='customerAdministration']";
		public static final String managementOptions_SANadmin = "@xpath=//input[@id='sanAdministration']";
		public static final String managementOptions_ResellerAdmin = "@xpath=//input[@id='resellerAdministration']";

		// MDF/MVF/DI
		public static final String STRM_IP_tag = "@xpath=//input[@id='strmIpTag']";

		
		public static final String ServiceSubtype = "@xpath=//div[label[contains(text(),'Service Subtype')]]//input";
		public static final String InterfaceSpeed = "@xpath=//div[label[contains(text(),'Interface Speed')]]//input";
		public static final String Interfacespeedropdownvalue = "@xpath=//div[@role='list']//span[@role='option']";
		public static final String AvailableCircuits = "@xpath=//div[label[contains(text(),'Available Circuits')]]//input";
		public static final String Next = "@xpath=(//span[contains(text(),'Next')])[1]";
		public static final String A_Endtechnology = "@xpath=//div[label[contains(text(),'A-End Technology')]]//input";
		public static final String A_EndTechnology_xbuton = "@xpath=//div[label[text()='A-End Technology']]//div[text()='�']";
		public static final String B_Endtechnology = "@xpath=//div[label[contains(text(),'B-End Technology')]]//input";
		public static final String B_EndTechcnology_xbutton = "@xpath=//div[label[text()='B-End Technology']]//div[text()='�']";
		public static final String Autocreatebutton = "@xpath=//span[contains(text(),'AutoCreate Service')]";
		public static final String interfacespeedvaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[3]";
		public static final String interfacespeedlabel = "@xpath=//label[contains(text(),'Interface Speed')]";
		public static final String servicesubtypevaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[2]";
		public static final String servicetypevaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[1]";
		public static final String modularmspcheckbox = "@xpath=//div[@class='form-group']//div[@class='form-group']//input[@id='modularMSP']";
		public static final String AutocreateServicecheckbox = "@xpath=//input[@id='autoCreateService']";
		public static final String notificationmanagement = "@xpath=//div[label[contains(text(),'Notification Management Team')]]//input";
		public static final String E_VPNtechnologyDropdown = "@xpath=//div[label[text()='E-VPN Technology']]//input";
		public static final String E_VPNtechnologyDropdown_xButton = "@xpath=//div[label[text()='E-VPN Technology']]//div[text()='�']";

		public static final String configurtoinptionpanel_webelementToScroll = "@xpath=//div[contains(text(),'Configuration Options')]";

		public static final String circuitType_MSPselected_Default = "@xpath=//div[div[div[label[text()='Circuit Type']]]]/following-sibling::div//span[text()='Default']";
		public static final String EndpointCPE = "@xpath=//div[label[contains(text(),'Single Endpoint CPE')]]//input";
		public static final String perCoSperformncereport = "@xpath=//div[label[contains(text(),'Per CoS Performance Reporting')]]//input";
		public static final String HCoSperformanceReport = "@xpath=//div[label[text()='HCoS Performance Reporting (Modular MSP)']]//input";
		public static final String ActelisBasedcheckbox = "@xpath=//input[@id='actelisBased']";
		public static final String standardCIRtextfield = "@xpath=//input[@id='standardCir']";
		public static final String standardEIRtextfield = "@xpath=//input[@id='standardEir']";
		public static final String premiumCIRtextfield = "@xpath=//input[@id='premiumCir']";
		public static final String premiumEIRtextfield = "@xpath=//input[@id='premiumEir']";
		public static final String proactiveMonitoring = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//input";
		public static final String DeliveryChannel = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";
		public static final String managementOrder = "@xpath=//div[label[contains(text(),'Management Order')]]//input";
		public static final String VPNtopology = "@xpath=//div[label[contains(text(),'VPN Topology')]]//input";
		public static final String VPNtopology_xbutton = "@xpath=//div[label[text()='VPN Topology']]//div[text()='�']";
		public static final String IntermediateTechnology = "@xpath=//div[label[contains(text(),'Intermediate Technologies')]]//input";
		public static final String circuitReference = "@xpath=//div[label[contains(text(),'Circuit Reference')]]//input";
		public static final String Aggregate_10Gig_1Gig_Traffic = "@xpath=//div[label[contains(text(),'Aggregate 10Gig-1Gig Traffic')]]//input";
		public static final String DeliverychannelForselectTag = "@xpath=//select[@id='deliveryChannel']";
		public static final String VpnTopologyForSelctTag = "@xpath=//select[@id='vpnTechnology']";
		public static final String ENNIcheckbox = "@xpath=//input[@id='enni']";
		public static final String xButton = "@xpath=//span[contains(text(),'�')]";
		public static final String deliverychannel_withclasskey = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";

		public static final String cicuitreferenceLabel_serviceCreation = "@xpath=//label[contains(text(),'Circuit Reference')]";
		public static final String circuitTyeplabel_servicecreation = "@xpath=//label[contains(text(),'Circuit Type')]";
		public static final String intermediatetechLabelname = "@xpath=//label[contains(text(),'Intermediate Technology')]";

		public static final String vpntopoloty_xbutton = "@xpath=(//div[@class='react-dropdown-select-clear css-w49n55-ClearComponent e11qlq5e0'])[3]";
		public static final String notigymanagement_xbutton = "@xpath=//div[label[contains(text(),'Notification Management Team')]]//div[text()='�']";
		public static final String deliverychannel_xbutton = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//div[text()='�']";
		public static final String managementOrder_xButton = "@xpath=//div[label[contains(text(),'Management Order')]]//div[text()='�']";
		
		public static final String managementConnectForselectTag = "@xpath=//select[@id='managementConnection']";
		public static final String listofcircuittypes = "@xpath=//div[@class='div-border div-margin container']//div[@class='row'][4]//input";
		public static final String cancelButton = "@xpath=//span[contains(text(),'Cancel')]";
		// public static final String
		// okbutton="@xpath=//span[contains(text(),'OK')]";
		public static final String Editinterface_popupTitlename = "@xpath=//div[@class='modal-title h4']";
		public static final String EditInterfacepopup_xbutton = "@xpath=//span[text()='�']";

		public static final String Actiondropdown_siteorder = "@xpath=//div[div[text()='Site Orders']]//button[text()='Action']";
		public static final String ActiondropdownforshowInterfaceunderEquipment = "@xpath=(//button[@id='dropdown-basic-button'])[2]";

		public static final String Viewcreatedservice_serviceIdentification = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_Servicetype = "@xpath=//div[div[label[contains(text(),'Service Type')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_servicesubtype = "@xpath=//div[div[label[contains(text(),'Service Subtype')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_interfacespeed = "@xpath=//div[div[label[contains(text(),'Interface Speed')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_SinplepointCPE = "@xpath=//div[div[label[contains(text(),'Single Endpoint CPE')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_email = "@xpath=//div[div[label[contains(text(),'Email')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_phone = "@xpath=//div[div[label[contains(text(),'Phone Contact')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_msp = "@xpath=//div[div[label[contains(text(),'Modular MSP')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_remark = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_performReport = "@xpath=//div[div[label[contains(text(),'Performance Reporting')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_proactivemonitor = "@xpath=//div[div[label[contains(text(),'Proactive Monitoring')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_deliverychanel = "@xpath=//div[div[label[contains(text(),'Delivery Channel')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_vpnTopology = "@xpath=//div[div[label[contains(text(),'VPN Topology')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_circuittype = "@xpath=//div[div[label[contains(text(),'Circuit Type')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_circuitreference = "@xpath=//div[div[label[contains(text(),'Circuit Reference')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_notificationManagement = "@xpath=//div[div[label[contains(text(),'Notification Management Team')]]]//div[@class='customLabelValue form-label']";
		public static final String Viewcreatedservice_managementorder = "@xpath=//div[div[label[contains(text(),'Management Order')]]]//div[@class='customLabelValue form-label']";
		public static final String viewcreatedservice_ManagementConnection = "@xpath=//div[div[label[contains(text(),'Management Connection')]]]//div[@class='customLabelValue form-label']";
		public static final String viewcreatedservice_ENNI = "@xpath=//div[div[label[contains(text(),'ENNI')]]]//div[@class='customLabelValue form-label']";

		// public static final String !-- Add site order Error warning messages
		// --="@xpath=
		public static final String Addsiteorder_countryerrmsg = "@xpath=//div[text()='Device Country']";
		public static final String Addsiteorder_devciexngCityErrmsg = "@xpath=//div[text()='Device Xng City']";
		public static final String Addsiteorder_xngcitynameerrmsg = "@xpath=//div[text()='Device Xng City Name']";
		public static final String Addsiteorder_xngcityCodeerrmsg = "@xpath=//div[text()='Device Xng City Code']";
		public static final String Addsiteorder_csrnameErrmsg = "@xpath=//div[text()='CSR Name']";
		public static final String Addsitieorder_technologyErrmsg = "@xpath=//div[text()='Technology']";
		public static final String Addsiteorder_physicalsiteErrmsg = "@xpath=//div[text()='Physical Site']";
		public static final String Addsiteorder_sitOrderNumberErrmsg = "@xpath=//div[text()='Site Order Number(Siebel Service ID)']";
		public static final String Addsiteorder_circuitReferenceErrmsg = "@xpath=//div[text()='Circuit Reference']";
		public static final String Addsiteorder_IVReferenceErrmsg = "@xpath=//div[text()='IV Reference']";

		// public static final String !-- Add Site Order fields --="@xpath=
		public static final String Addsiteorderlink = "@xpath=//a[contains(text(),'Add Site Order')]";
		public static final String EditsideOrderlink = "@xpath=//a[contains(text(),'Edit')]";
		public static final String deletesiteorderlink = "@xpath=//a[contains(text(),'Delete')]";
		public static final String viewsiteorderlink = "@xpath=//a[@class='dropdown-item'][contains(text(),'View')]";
		public static final String Addsiteorder_Country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String Addsiteorder_City = "@xpath=//div[label[contains(text(),'Device Xng City')]]//input";
		public static final String Addsiteorder_Citytogglebutton = "@xpath=(//div[@class='react-switch']/div)[2]";
		public static final String Addsiteorder_disabledCitytogglebutton = "@xpath=(//div[@class='react-switch']/div)[3]";
		public static final String Addsiteorder_Sitetogglebutton = "@xpath=(//div[@class='react-switch']/div)[6]";
		public static final String AddsiteOrdr_disabledSitetogglebutton = "@xpath=(//div[div[label[text()='Select Site']]]//div[@class='react-switch-handle'])[1]";
		public static final String Addsiteorder_sitedropdown = "@xpath=//div[label[contains(text(),'Physical Site')]]//input";
		public static final String Addsiteorder_CSRname = "@xpath=//input[@id='csrName']";
		public static final String Addsiteorder_performancereporting = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String Addsiteorder_performancereporting_xbutton = "@xpath=//div[label[text()='Performance Reporting']]//div[text()='�']";
		public static final String Addsiteorder_performancereportingdefaultvalue = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//span[text()='Follow Service']";
		public static final String Addsiteorder_proactivemonitoring = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//input";
		public static final String Addsitorder_proactivemonitoring_xbutton = "@xpath=//div[label[text()='Proactive Monitoring']]//div[text()='�']";
		public static final String Addsiteorder_proactivemonitoringdefaultvalue = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//span[text()='Follow Service']";
		public static final String Addsiteorder_smartmonitoring = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";
		public static final String Addsiteorder_smartmonitoring_xbutton = "@xpath=//div[label[text()='Smarts Monitoring']]//div[text()='�']";
		public static final String Addsiteorder_smartmonitoringdefaultvalue = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//span[text()='Follow Service']";
		public static final String Addsiteorder_Technology = "@xpath=//div[label[contains(text(),'Technology')]]//input";
		public static final String Addsiteorder_sitealias = "@xpath=//div[label[text()='Site Alias']]//input";
		public static final String Addsiteorder_Vlanid = "@xpath=//div[label[text()='VLAN Id']]//input";
		public static final String Addsiteorder_DCAenabledsitecheckbox = "@xpath=//input[@id='dcaEnabledSite']";
		public static final String Addsiteorder_next = "@xpath=//span[contains(text(),'Next')]";
		public static final String Addsiteorder_cancel = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String Addsiteorder_cloudserviceProvider = "@xpath=//div[label[contains(text(),'Cloud Service Provider')]]//input";
		public static final String Addsiteorder_remark = "@xpath=//div[label[text()='Remark']]//textarea";
		public static final String Addsiteorder_selectcitytoggle = "@xpath=(//div[div[label[text()='Select City']]]//div[@class='react-switch-bg'])[1]";
		public static final String Addsiteorder_xngcityname = "@xpath=//input[@id='cityName']";
		public static final String Addsiteorder_XNGcitycode = "@xpath=//input[@id='cityCode']";
		public static final String Addsiteorder_IVreferencedropdown = "@xpath=//div[label[contains(text(),'IV Reference')]]//input";
		public static final String Addsiteorder_circuitReferenceField = "@xpath=//div[label[contains(text(),'Circuit Reference')]]//input";
		public static final String Addsiteorder_AccessCircuitID = "@xpath=//div[label[text()='Access Circuit ID']]//input";
		public static final String Addsiteorder_AccessCircuitIDwarningMessage = "@xpath=//div[text()='Access Circuit ID']";
		public static final String Addsiteorder_offnetCheckbox = "@xpath=//div[label[contains(text(),'Offnet')]]//input";
		public static final String Addsiteorder_spokeId = "@xpath=//div[div[label[contains(text(),'Spoke Id')]]]//div[text()='0']";
		public static final String Addsiteorder_spokeIdField = "@xpath=//label[contains(text(),'Spoke Id')]";
		public static final String Addsiteorder_GCRoloTypeDropdown = "@xpath=//div[label[contains(text(),'GCR OLO Type')]]//input";
		public static final String Addsiteorder_GCRoloTypeDropdown_xbutton = "@xpath=//div[label[text()='GCR OLO Type']]//div[text()='�']";
		public static final String Addsiteorder_VLAN = "@xpath=//div[label[text()='VLAN']]//input";
		public static final String Addsiteorder_VLANEtherTypeDropdown = "@xpath=//div[label[text()='VLAN Ether Type']]//input";
		public static final String Addsiteorder_VLAnEtheryTypeDropdown_xbutton = "@xpath=//div[label[text()='VLAN Ether Type']]//div[text()='�']";
		public static final String Addsiteorder_PrimaryVLAN = "@xpath=//div[label[text()='Primary VLAN']]//input";
		public static final String Addsiteorder_primaryVLANEtherTypeDropdown = "@xpath=//div[label[contains(text(),'Primary VLAN Ether Type')]]//input";
		public static final String Addsiteorder_primaryVLANEtherTypeDropdown_xbutton = "@xpath=//div[label[text()='Primary VLAN Ether Type']]//div[text()='�']";
		// public static final String
		// Addsiteorder_EPNoffnetCheckbox="@xpath=//div[label[contains(text(),'EPN
		// Offnet')]]//input";
		public static final String Addsiteorder_siteordernumbertextfield = "@xpath=//div[label[contains(text(),'Site Order Number(Siebel Service ID)')]]//input";
		public static final String Addsiteorder_EPNoffnetCheckbox = "@xpath=//div[label[contains(text(),'EPN Offnet')]]//input";
		public static final String Addsiteorder_EPNEOSDHCheckbox = "@xpath=//div[label[contains(text(),'EPN EOSDH')]]//input";
		public static final String Addsiteorder_IVreference_Primary = "@xpath=//span[contains(text(),'Primary')]";
		public static final String Addsiteorder_IVreference_Access = "@xpath=//span[contains(text(),'Access')]";

		public static final String Addsiteorder_Devicenamefield = "@xpath=//div[label[text()='Device Name']]//input";
		public static final String Addsiteorder_nonterminationpoint = "@xpath=//div[label[text()='Non Termination Point']]//input";
		public static final String Addsiteorder_protected = "@xpath=//div[label[text()='Protected']]//input";
		public static final String Addsiteorder_mappingModedropdown = "@xpath=//div[label[text()='Mapping Mode']]//input";
		public static final String Addsiteorder_mappingModedropdown_xbutton = "@xpath=//div[label[text()='Mapping Mode']]//div[text()='�']";
		public static final String portname_textField = "@xpath=//div[label[text()='Port Name']]//input";
		public static final String vlanid_textfield = "@xpath=//div[label[text()='VLAN Id']]//input";

		// public static final String
		// Backbutton="@xpath=//span[contains(text(),'Back')]";

		public static final String Editsiteorder_country = "@xpath=(//div[@class='customLabelValue form-label'])[1]";
		public static final String Editsiteorder_city = "@xpath=(//div[@class='customLabelValue form-label'])[2]";
		public static final String Editsiteorder_CSRname = "@xpath=(//div[@class='customLabelValue form-label'])[3]";
		public static final String Editsiteorder_performreport = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";
		public static final String Editsiteorder_proactivemonitor = "@xpath=//div[label[contains(text(),'Procative Monitoring')]]//input";
		public static final String Editsiteorder_smartmonitor = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";
		public static final String Editsiteorder_technology = "@xpath=(//div[@class='customLabelValue form-label'])[4]";
		public static final String Editsiteorder_sitealias = "@xpath=//div[label[contains(text(),'Site Alias')]]//input";
		public static final String Editsiteorder_VLANid = "@xpath=//div[label[contains(text(),'VLAN Id')]]//input";
		public static final String Editsiteorder_remark = "@xpath=//textarea[@placeholder='Enter Remarks']";
		public static final String Editsiteorder_DCAenabledsite = "@xpath=//div[label[contains(text(),'DCA Enabled Site')]]//input";
		public static final String Editsiteorder_cloudserviceprovider = "@xpath=//div[label[contains(text(),'Cloud Service Provider')]]//input";

		public static final String verifysiteOrder_Devicecountry = "@xpath=//div[div[label[contains(text(),'Device Country')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_DeviceCity = "@xpath=//div[div[label[contains(text(),'Device Xng City')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_CSRname = "@xpath=//div[div[label[contains(text(),'CSR Name')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_performReport = "@xpath=//div[div[label[contains(text(),'Performance Reporting')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_proactivemonitor = "@xpath=//div[div[label[contains(text(),'Procative Monitoring')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_smartmonitor = "@xpath=//div[div[label[contains(text(),'Smarts Monitoring')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_Technology = "@xpath=//div[div[label[contains(text(),'Technology')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_DCAenabledsite = "@xpath=//div[div[label[contains(text(),'DCA Enabled Site')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_VLAnid = "@xpath=//div[div[label[contains(text(),'VLAN Id')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_sitealias = "@xpath=//div[div[label[contains(text(),'Site Alias')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_remark = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteordertablevalue_totalpages = "@xpath=";
		public static final String verifysiteordertablevalue_currentpge = "@xpath=";
		public static final String verifysiteordertablevalue_nextpage = "@xpath=";
		public static final String verifysiteordertablevalue_previouspage = "@xpath=";

		public static final String Deletesiteorderrbutton = "@xpath=//button[@class='btn btn-danger']";

		public static final String totalpage_intermeiateshowinterfacelink = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//span[@ref='lbTotal']";
		public static final String currentpage_intermeduateshowinterfacelink = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//span[@ref='lbCurrent']";
		public static final String TotalPagesforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbTotal']";
		public static final String Currentpageforsiteorderfunc = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbCurrent']";
		public static final String pagenavigationfornextpage_underviewserviceforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//button[text()='Next']";
		public static final String Equipmentconfig_Currentpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String Equipmentconfig_Totalpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String Equipmentconfig_nextpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//button[text()='Next']";

		public static final String Equipment_configurationPanelName = "@xpath=//div[text()='Equipment']";
		public static final String IntermediateEquipment_configurationPanelName = "@xpath=//div[text()='Intermediate Equipment']";

		public static final String intermediateEquip_Totalpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//span[@ref='lbTotal']";
		public static final String intermediateequip_currentpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//span[@ref='lbCurrent']";
		public static final String intermediateequip_nextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//button[@ref='btNext']";

		public static final String Pagenavigationfornextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[2]//button[text()='Next']";

		public static final String pagenavigateforshowinterface = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//button[text()='Next']";
		public static final String pagenavigateforIntermediate = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//button[text()='Next']";

		// public static final String !-- Add CPE device for Provider Equipment
		// Error Message --="@xpath=
		public static final String AddCPEdevice_nameerrmsg = "@xpath=//span[contains(text(),' should be replaced by corresponding value on Device Name')]";
		public static final String AddCPEdevice_snmproerrmsg = "@xpath=//div[text()='Snmpro']";
		public static final String AddCPEdevice_managementAddresserrmsg = "@xpath=//div[text()='Management Address']";
		public static final String AddCPEdevice_powerAlarmerrmsg = "@xpath=//div[text()='Power Alarm']";
		public static final String AddCPEdevice_mediaselectionerrmsg = "@xpath=//div[text()='Media Selection']";
		public static final String AddCPEdevice_macAdressErrmsg = "@xpath=//div[text()='MAC Address']";
		public static final String AddCPEdevice_vendorErrmsg = "@xpath=//div[text()='Vendor/Model']";
		public static final String AddCPEdevice_serialNumberErrmsg = "@xpath=//div[text()='Serial Number']";
		public static final String AddCPEdevice_countryErrmsg = "@xpath=//div[text()='Country']";
		public static final String AddCPEdevice_hexaSerialNumnerErrmsg = "@xpath=//div[text()='Hexa Serial Number']";

		// public static final String !-- Add CPE device fields for Provider
		// Eqeuipment --="@xpath=
		public static final String CPEdevice_adddevicelink = "@xpath=//div[div[div[text()='Equipment']]]//a[text()='Add Device']";
		public static final String CPEdevice_showinterfaceslink = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Equipment']]]//a[text()='Show Interfaces']";
		public static final String CPEdevice_hideinterfaceslinkforequipment = "@xpath=//div[div[@class='div-margin row']//div[text()='Equipment']]//a[text()='Hide Interfaces']";
		public static final String AddCPEdevice_Name = "@xpath=//div[label[contains(text(),'Name')]]//input";
		public static final String AddCPEdevice_selectdevicetogglebutton = "@xpath=(//div[@class='react-switch'])[1]";
		public static final String AddCPEdevice_vender = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String AddCPEdevice_vlanId = "@xpath=//div[label[text()='VLAN Id']]//input";
		public static final String AddCPEdevice_snmpro = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";
		public static final String AddCPEdevice_manageaddress = "@xpath=//input[@id='managementAddress']";
		public static final String AddCPEdevice_manageaddressdropdown = "@xpath=//div[label[contains(text(),'Management Address')]]//input";
		public static final String AddCPEdevice_getSubnetbutton = "@xpath=//span[text()='Get Subnets']";
		public static final String AddCPEdevice_selectSubnettogglebutton = "@xpath=//div[div[label[contains(text(),'Select SubNet')]]]//div[@class='react-switch-bg']";
		public static final String AddCPEdevice_mepid = "@xpath=//div[label[contains(text(),'MEP Id')]]//input";
		public static final String AddCPEdevice_poweralarm = "@xpath=//div[label[text()='Power Alarm']]//input";
		public static final String AddCPEdevice_mediaselection = "@xpath=//div[label[text()='Media Selection']]//input";
		public static final String AddCPEdevice_macaddress = "@xpath=//div[label[text()='MAC Address']]//input";
		public static final String AddCPEdevice_serialnumber = "@xpath=//div[label[text()='Serial Number']]//input";
		public static final String AddCPEdevice_hexaserialnumber = "@xpath=//input[@id='hexaSerialNumber']";
		public static final String AddCPEdevice_linklostforowarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//input[@type='checkbox']";
		public static final String AddCPEdevice_OKbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String AddCPEdevice_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String CPEdevice_snmpro_autoPopulatedValue = "@xpath=//input[@value='JdhquA5']";

		public static final String viewCPEdevicelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='View'])[3]";
		public static final String viewCPEdevice_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_mepid = "@xpath=//div[div[label[contains(text(),'MEP Id')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_poweralarm = "@xpath=//div[div[label[contains(text(),'Power Alarm')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_mediaselection = "@xpath=//div[div[label[contains(text(),'Media Selection')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_linklostforwarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_macaddress = "@xpath=//div[div[label[contains(text(),'MAC Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_serialnumber = "@xpath=//div[div[label[text()='Serial Number']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEdevice_hexaserialnumber = "@xpath=//div[div[label[contains(text(),'Hexa Serial Number')]]]//div[@class='customLabelValue form-label']";
		public static final String viewPCEdevice_Actiondropdown = "@xpath=(//div[div[text()='View CPE Device']]//button[text()='Action'])";

		public static final String EditCPEdevicelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Edit'])[3]";
		public static final String EditCPEdevicelinkunderviewpage = "@xpath=//a[text()='Edit']";
		public static final String EditCPEdevlielink_underEquipment = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Edit'])[2]";
		public static final String EditCPEdevice_vendoModel_xbutton = "@xpath=//div[label[text()='Vendor/Model']]//div[text()='�']";
		public static final String EditCPEdevice_powerAlarm_xbutton = "@xpath=//div[label[text()='Power Alarm']]//div[text()='�']";
		public static final String EditCPEdevice_mediaselection_xbutton = "@xpath=//div[label[text()='Media Selection']]//div[text()='�']";

		public static final String DeletfromserviceforcpeDevice_Equipment = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Delete from Service'])[3]";
		public static final String deleteMessage_equipment = "@xpath=//div[@class='modal-body']";
		public static final String deletebutton_deletefromsrviceforequipment = "@xpath=//button[@class='btn btn-danger']";
		public static final String xbuttonfordeletefromservicepopup_Equipment = "@xpath=//div[@class='modal-header']//span[contains(text(),'�')]";
		public static final String deleteDevice_successmEssage = "@xpath=//span[text()='Site device deleted successfully']";

		public static final String technologyPopup = "@xpath=//div[@class='modal-title h4']";
		public static final String tchnologyPopup_dropdownValues = "@xpath=//div[label[@class='form-label labelStyle']]//span";
		public static final String technologypopup_dropdown = "@xpath=//div[label[@class='form-label labelStyle']]//input";
		public static final String AddCPEforIntermediateEquip_adddevicelink = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Add Device']";
		public static final String AddCPEforIntermediateEquip_Name = "@xpath=//input[@id='deviceName']";
		public static final String AddCPEforIntermediateEquip_selectdevicetogglebutton = "@xpath=(//div[@class='react-switch'])[1]";
		public static final String AddCPEforIntermediateEquip_vender = "@xpath=//div[label[text()='Vender/Model']]//input";
		public static final String AddCPEforIntermediateEquip_snmpro = "@xpath=//input[@id='snmpro']";
		public static final String AddCPEforIntermediateEquip_manageaddress = "@xpath=//input[@id='managementAddress']";
		public static final String AddCPEforIntermediateEquip_selectSubnettogglebutton = "@xpath=(//div[@class='react-switch'])[2]";
		public static final String AddCPEforIntermediateEquip_mepid = "@xpath=//input[@id='mePId']";
		public static final String AddCPEforIntermediateEquip_poweralarm = "@xpath=//div[label[text()='Power Alarm']]//input";
		public static final String AddCPEforIntermediateEquip_mediaselection = "@xpath=//div[label[text()='Media Selection']]//input";
		public static final String AddCPEforIntermediateEquip_macaddress = "@xpath=//input[@id='macAddress']";
		public static final String AddCPEforIntermediateEquip_serialnumber = "@xpath=//input[@id='serialNumber']";
		public static final String AddCPEforIntermediateEquip_hexaserialnumber = "@xpath=//input[@id='hexaSerialNumber']";
		public static final String AddCPEforIntermediateEquip_linklostforowarding = "@xpath=//input[@id='linkLostForwarding']";
		public static final String AddCPEforIntermediateEquip_country = "@xpath=//select[@id='country']";
		public static final String AddCPEforIntermediateEquip_countrydiv = "@xpath=//div[label[text()='Country']]//input";
		public static final String countrylabelname_IntEquipment = "@xpath=//label[text()='Country']";
		public static final String AddCPEforIntermediateEquip__city = "@xpath=//select[@id='city']";
		public static final String AddCPEforIntermediateEquip__citydiv = "@xpath=//div[label[text()='City']]//input";
		public static final String AddCPEforIntermediateEquip_citytogglebutton = "@xpath=(//div[@class='react-switch'])[3]";
		public static final String AddCPEforIntermediateEquip_disabledCityToggleButton = "@xpath=(//div[@class='react-switch-bg'])[4]";
		public static final String AddCPEforIntermediateEquip_site = "@xpath=//select[@id='site']";
		public static final String AddCPEforIntermediateEquip_sitediv = "@xpath=//div[label[text()='Site']]//input";
		public static final String AddCPEforIntermediateEquip_sitetogglebutton = "@xpath=(//div[@class='react-switch'])[5]";
		public static final String AddCPEforIntermediateEquip_premise = "@xpath=//select[@id='premise']";
		public static final String AddCPEforIntermediateEquip_premisediv = "@xpath=//div[label[text()='Premise']]//input";
		public static final String AddCPEforIntermediateEquip_premisetogglebutton = "@xpath=(//div[@class='react-switch'])[7]";
		public static final String AddCPEforIntermediateEquip_citynamefield = "@xpath=//input[@id='cityName']";
		public static final String AddCPEforIntermediateEquip_citycodefield = "@xpath=//input[@id='cityCode']";
		public static final String AddCPEforIntermediateEquip_sitenamefield = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String AddCPEforIntermediateEquip_sitecodefield = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String AddCPEforIntermediateEquip_premisenamefield = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";
		public static final String AddCPEforIntermediateEquip_premisecodefield = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";

		public static final String AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";
		public static final String AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";

		public static final String AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";
		public static final String AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";

		public static final String deletefromservicelink_IntermediateEquipment = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='Delete from Service']";
		public static final String viewCPEforIntermediateEquiplink = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='View']";
		public static final String viewCPEforIntermediateEquip_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_mepid = "@xpath=//div[div[label[contains(text(),'MEP Id')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_poweralarm = "@xpath=//div[div[label[contains(text(),'Power Alarm')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_mediaselection = "@xpath=//div[div[label[contains(text(),'Media Selection')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_linklostforwarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_macaddress = "@xpath=//div[div[label[contains(text(),'MAC Address')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_serialnumber = "@xpath=//div[div[label[text()='Serial Number']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_hexaserialnumber = "@xpath=//div[div[label[contains(text(),'Hexa Serial Number')]]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_country = "@xpath=//div[div[label[text()='Country']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_city = "@xpath=//div[div[label[text()='City']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_site = "@xpath=//div[div[label[text()='Site']]]//div[@class='customLabelValue form-label']";
		public static final String viewCPEforIntermediateEquip_premise = "@xpath=//div[div[label[text()='Premise']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_Actiondropdown = "@xpath=(//div[@class='heading-green row'][1]//button[@id='dropdown-basic-button'])[1]";
		public static final String EditCPEdeviceforIntermediateEquipmentlinkunderviewpage = "@xpath=//a[text()='Edit']";

		public static final String EditCPEdeviceforIntermediateEquipmentlink = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='Edit']";
		public static final String showInterface_ForIntermediateEquipment = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Show Interfaces']";
		public static final String HideInterface_ForIntermediateEquipment = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Hide Interfaces']";

		// public static final String !-- Fetch Dveice Interface --="@xpath=
		public static final String fetchDeviceinterfacelink_viewDevicePage = "@xpath=//a[text()='Fetch Device Interfaces']";
		public static final String succesMessageForfetchDeviceInterfcae = "@xpath=//div[contains(text(),'Fetch Interfaces started successfully. Please chec')]";
		public static final String clickherelink_fetchDeviceinterfcae = "@xpath=//a[@class='customLink heading-green-1']";

		public static final String ManageNetworkHeaderName = "@xpath=//div[div[contains(text(),'Manage Network')]]";

		// public static final String !-- xpath for table --="@xpath=

		public static final String configure_totalPage = "@xpath=//span[@ref='lbTotal']";
		public static final String configure_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String Actelisconfig_addDSLAM = "@xpath=//div[div[div[text()='Actelis Configuration']]]//a[text()='Add DSLAM and HSL']";
		public static final String DSLM_Device_Select = "@xpath=//div[div[div[label[@class='form-label']]]]//input";
		public static final String List_HSL_Link = "@xpath=//span[contains(text(),'List HSL')]";
		public static final String ActelisConfigurationPanel = "@xpath=//div[div[contains(text(),'Actelis Configuration')]]";

		public static final String ActiondropdownforEditLink_IntermediateEquipmentforcpedeviec = "@xpath=(//div[div[text()='View CPE Device']]//button[text()='Action'])[2]";

		public static final String AddPElink_LANlinkoutband = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Add Device']";
		public static final String ProviderEquipment_showInterfacelink = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Show Interfaces']";

		public static final String SelectIMSlocation = "@xpath=(//select[@id='imsPOP'])[1]";
		public static final String selectdevice_ToggleButton = "@xpath=(//div[@class='div-margin row']//div[@class='react-switch-bg'])[1]";
		public static final String Name = "@xpath=//input[@id='name']";
		public static final String VenderModel = "@xpath=//select[@id='vender']";
		public static final String managementaddress = "@xpath=//input[@id='address']";
		public static final String Snmpro = "@xpath=//input[@id='snmpro']";
		
		public static final String IntermediateEquipment_OKbuttonforpopup = "@xpath=//button[@class='btn btn-secondary']";

		public static final String verifyPEname_lanlinkoutband = "@xpath=(//div[div[label[text()='Name']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEvendor_lanlinkoutband = "@xpath=(//div[div[label[text()='Vendor/Model']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEaddress_lanlinkoutband = "@xpath=(//div[div[label[text()='Management Address']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEsnmpro_lanlinkoutband = "@xpath=(//div[div[label[text()='Snmpro']]])[2]//div[@class='customLabelValue form-label']";
		public static final String verifyPEcountry_lanlinkoutband = "@xpath=(//div[div[label[text()='Country']]])[1]//div[@class='customLabelValue form-label']";
		public static final String verifyPECity_lanlinkoutband = "@xpath=(//div[div[label[text()='City']]])[1]//div[@class='customLabelValue form-label']";
		public static final String verifyPESite_lanlinkoutband = "@xpath=(//div[div[label[text()='Site']]])[1]//div[@class='customLabelValue form-label']";
		public static final String verifyPEPremise_lanlinkoutband = "@xpath=(//div[div[label[text()='Premise']]])[1]//div[@class='customLabelValue form-label']";

		public static final String verifyIMSlocation = "@xpath=";
		public static final String Providerequipment_viewlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='View'])[3]";
		public static final String Providerequipment_Editlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Edit'])[2]";
		public static final String Providerequipment_selectinterface = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Select Interfaces'])[2]";
		public static final String Providerequipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Configure'])[2]";
		public static final String Providerequipment_deletefromservice = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Delete from Service'])[3]";
		public static final String Providerequipment_ShowInterface = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Show Interfaces']";
		public static final String providerEquipment_actiondropdownfromviewpage = "@xpath=(//button[text()='Action'])[2]";
		public static final String providerEquipment_Editlinkunderviewdevicepage = "@xpath=//a[text()='Edit']";
		public static final String providerEquipment_showinterfaceTatoalpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//span[@ref='lbTotal']";
		public static final String providerEquipment_showinterfaceCurrentpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//span[@ref='lbCurrent']";
		public static final String providerEquipment_showinterfaceNextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//button[@ref='btNext']";

		public static final String ProviderEquipment_Generateconfigforstaticlink = "@xpath=//a[text()='Generate Configuration for Static Routes']";
		public static final String providerEquipment_saveconfigurationlink = "@xpath=//a[text()='Save Configuration']";
		public static final String providerEquipment_ExecuteconfigurationOndevicelink = "@xpath=//a[text()='Execute Configuration on Device']";

		public static final String providerEquipment_currentpageInterfaceToselect = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbCurrent'])[2]";
		public static final String providerEquipment_InterfaceToselectTotalpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbTotal'])[2]";
		public static final String providerEquipment_InterfaceToselectActiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String providerEquipment_InterfaceToselectAddbutton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";
		public static final String providerEquipment_InterfaceToselectnextpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//button[@ref='btNext'])[2]";

		public static final String providerEquipment_currentpageInterfaceInselect = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbCurrent'])[1]";
		public static final String providerEquipment_InterfaceInselectTotalpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbTotal'])[1]";
		public static final String providerEquipment_InterfaceInselectActiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String providerEquipment_InterfaceInselectRemovebutton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String providerEquipment_InterfaceInselectnextpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//button[@ref='btNext'])[1]";

		public static final String customerpremiseEquipment_viewlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='View'])[1]";
		public static final String customerpremiseequipment_Editlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Edit'])[1]";
		public static final String cusomerpremiseequipment_SelectInertafeceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Select Interfaces'])[1]";
		public static final String customerpremiseequipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Configure'])[1]";
		public static final String customerpremiseEquipment_FetchdeviceInterfacelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";
		public static final String customerpremiseEquipment_PPPconfigurationlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";
		public static final String customerpremiseEquipment_deletefromservceilink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";
		public static final String customerpremiseEquipment_Adddevicelink = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";
		public static final String customerpremiseEQuipment_showinetrfacelink = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Show Interfaces']";
		public static final String customerpremiseequipment_showinterfaceTotalpage = "@xpath=";
		public static final String customerpremiseequipment_showinterfaceCurrentpage = "@xpath=";
		public static final String customerpremiseequipment_showinterfaceActiondropdown = "@xpath=";
		public static final String customerpremiseequipment_showinterfaceEditlink = "@xpath=";

		public static final String AddCPElink_LANlinkoutband = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";

		public static final String Actionbutton_PE2CPE = "@xpath=(//div[@class='heading-green div-margin row']//button[text()='Action'])[1]";
		public static final String Addnewlink_Pe2CPE = "@xpath=//a[text()='Add New Link']";

		public static final String AddnewlinkPe2CPE_LinkorCircuitId = "@xpath=//input[@id='linkCircuitId']";
		public static final String AddnewlinkPe2CPE_SourceDevice = "@xpath=//select[@id='fromDeviceName']";
		public static final String AddnewlinkPe2CPE_SourceInterface = "@xpath=//select[@id='fromInterfaceName']";
		public static final String AddnewlinkPe2CPE_TargetDevice = "@xpath=//select[@id='toDeviceName']";
		public static final String AddnewlinkPe2CPE_TargetInterface = "@xpath=//select[@id='toInterfaceName']";
		public static final String AddnewlinkPe2CPE_Nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String Headername_PE2CPElink = "@xpath=//div[@class='modal-title h4']";

		public static final String Pe2CPElinktable_totalpage = "@xpath=//div[@class='ag-div-margin row']//span[@ref='lbTotal']";
		public static final String Pe2CPElinktable_currentpage = "@xpath=//div[@class='ag-div-margin row']//span[@ref='lbCurrent']";
		public static final String Pe2CPElinktable_nextpage = "@xpath=//div[@class='ag-div-margin row']//button[@ref='btNext']";

		// public static final String !-- Copy from here --="@xpath=
		public static final String intermediateTechErrorMessage = "@xpath=//button[@id='id1']//*[contains(@class,'svg-inline--fa fa-info-circle fa-w-16')]";
		public static final String IntermediaTechnologypopup = "@xpath=//div[@class='popover-body']";
		public static final String circuitreferenceIalert = "@xpath=//button[@id='id2']";
		public static final String circuitreferencealertmessage = "@xpath=//div[@class='modal-body']";
		public static final String circuitreferencealerterrmsg = "@xpath=//div[@class='popover-body']";
		public static final String managementconnection = "@xpath=//div[label[contains(text(),'Management Connection')]]//input";

		public static final String Equipment_selectInterfaceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Select Interfaces'])[2]";
		public static final String InterfaceToselect = "@xpath=(//div[@class='row'])[7]";
		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";

		public static final String InterfaceInslect = "@xpath=(//div[@class='row'])[6]";
		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String findlistofInterfacesNames = "@xpath=//div[@class='ag-cell ag-cell-not-inline-editing ag-cell-with-height ag-cell-no-focus ag-cell-value'][1]";

		public static final String IntermediateEquipment_selectInterfaceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Intermediate Equipment']]//span[text()='Select Interfaces'])[2]";

		public static final String configure_alertPopup = "@xpath=//div[text()='Alert']";
		public static final String configure_alertMessage = "@xpath=//div[@class='modal-body']";

		public static final String IntermediateEquipment_Configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Intermediate Equipment']]//span[text()='Configure'])[2]";
		public static final String Equipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Configure'])[2]";
		public static final String Equipment_configureActiondropdown = "@xpath=//div[div[text()='Interfaces']]//button[text()='Action']";
		public static final String Equipment_configureEditlink = "@xpath=//a[text()='Edit']";
		public static final String Equipment_showintefaceedit = "@xpath=(//a[text()='Edit'])[1]";
		public static final String Equipment_configureEditInterfaceTitle = "@xpath=//div[@class='modal-title h4']";
		public static final String Equipment_configureXNGCircuitID = "@xpath=(//div[label[text()='XNG Circuit Id']])[1]//input";
		public static final String selectCircuit_togglebutton = "@xpath=//div[@class='react-switch-bg']";
		public static final String Equipment_configureBearerType = "@xpath=//div[label[text()='Bearer Type']]//input";
		public static final String Equipment_configureBearerSpeed = "@xpath=//div[label[text()='Bearer Speed']]//input";
		public static final String Equipment_configureTotalcircuitbandwidth = "@xpath=//div[label[text()='Total Circuit Bandwidth']]//input";
		public static final String Equipment_configureVLANid = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String Equipment_configureVlanType = "@xpath=//div[label[text()='VLAN Type']]//input";

		public static final String Equipment_showInterfaceActiondropdown = "@xpath=(//button[text()='Action'])[2]";
		public static final String Equipment_showInterfaceTotalPages = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[3]//span[@ref='lbTotal']";
		public static final String Currentpageforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbCurrent']";

		public static final String IntermediateEquipment_Actiondropdown = "@xpath=(//button[text()='Action'])[4]";
		public static final String IntermediateEquipment_Editlink = "@xpath=(//a[text()='Edit'])[3]";

		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";
		public static final String Editservice_actiondropdownalternate = "@xpath=(//button[@id='dropdown-basic-button'])[4]";

		public static final String Editservice_Editlink = "@xpath=//div[div[text()='Service']]//a[contains(text(),'Edit')]";

		public static final String Editservice_Deletelink = "@xpath=//a[text()='Delete']";

		public static final String Editservice_sysnchronizelink = "@xpath=//a[text()='Synchronize']";

		public static final String Editservice_infovistareport = "@xpath=//a[text()='Show New Infovista Report']";

		public static final String Editservice_managelink = "@xpath=//a[text()='Manage']";

		public static final String Editservice_managesubnets = "@xpath=//a[text()='Manage Subnets']";

		public static final String Editservice_AMNvalidator = "@xpath=//a[text()='AMN Validator']";

		public static final String Editservice_Dumplink = "@xpath=//a[text()='Dump']";

		public static final String viewServicepage_Servicepanel = "@xpath=//div[contains(text(),'Service')]";
		public static final String viewServicepage_ManagementOptionsPanel = "@xpath=//div[contains(text(),'Management Options')]";
		public static final String viewServicepage_ConfigurationoptionsPanel = "@xpath=//div[contains(text(),'Configuration Options')]";
		public static final String viewServicepage_SiteORderPanel = "@xpath=//div[contains(text(),'Site Orders')]";
		public static final String viewServicepage_OrderPanel = "@xpath=//div[text()='Order']";

		public static final String Addsiteorder_siteordernumberfield = "@xpath=//div[label[contains(text(),'Site Order Number')]]//input";
		public static final String Addsiteorder_primarysiteorder = "@xpath=//div[label[contains(text(),'Primary Site Order')]]//input";
		public static final String Addsiteorder_perCoSperformncereport = "@xpath=//div[label[contains(text(),'Per CoS Performance Reporting')]]//input";
		public static final String AddSiteorder_IVreferencedropdown = "@xpath=//div[label[contains(text(),'IV Reference')]]//input";
		public static final String Addsiteorder_RouterconfigviewIpv4 = "@xpath=//div[label[contains(text(),'Router Configuration View IPv4')]]//input";
		public static final String Addsiteorder_wholesaleProvider = "@xpath=//div[label[contains(text(),'Wholesale Provider')]]//input";
		public static final String Addsiteorder_managedsite = "@xpath=//div[label[contains(text(),'Managed Site')]]//input";
		public static final String Addsiteorder_priority = "@xpath=//div[label[contains(text(),'Priority')]]//input";
		public static final String Addsiteorder_actelisbasedcheckbox = "@xpath=//div[label[contains(text(),'Actelis Based')]]//input";
		public static final String Addsiteorder_voipcheckbox = "@xpath=//div[label[contains(text(),'VoIP')]]//input";

		public static final String verifysiteOrder_perCoSperformReporting = "@xpath=//div[div[label[contains(text(),'Per CoS Performance Reporting')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_primarysiteorder = "@xpath=//div[div[label[contains(text(),'Primary Site Order')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_IVreference = "@xpath=//div[div[label[contains(text(),'IV Reference')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_priority = "@xpath=//div[div[label[contains(text(),'Priority')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_managedSite = "@xpath=//div[div[label[contains(text(),'Managed Site')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_RouterConfigviewIPv4 = "@xpath=//div[div[label[contains(text(),'Router Configuration View IPv4')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_actelisBased = "@xpath=//div[div[label[contains(text(),'Actelis Based')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_wholesaleProvider = "@xpath=//div[div[label[contains(text(),'Wholesale Provider')]]]//div[@class='customLabelValue form-label']";
		public static final String verifysiteOrder_VoIP = "@xpath=//div[div[label[contains(text(),'VoIP')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_providerEquipment_addDevice = "@xpath=//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a[text()='Add Device']";
		public static final String outband_providerEquipment_addDevice_chooseAdevicedrodown = "@xpath=//div[label[@class='form-label labelStyle']]//input";
		public static final String outband_providerEquipment_addDevice_venderModel = "@xpath=//div[label[contains(text(),'Vendor/Model')]]//input";
		public static final String outband_providerEquipment_addDevice_managenmetaddress = "@xpath=//div[label[contains(text(),'Management Address')]]//input";
		public static final String outband_providerEquipment_addDevice_snmpro = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";
		public static final String outband_providerEquipment_addDevice_country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String outband_providerEquipment_addDevice_city = "@xpath=//div[label[contains(text(),'City')]]//input";
		public static final String outband_providerEquipment_addDevice_site = "@xpath=//div[label[contains(text(),'Site')]]//input";
		public static final String outband_providerEquipment_addDevice_premise = "@xpath=//div[label[contains(text(),'Premise')]]//input";

		public static final String outband_customerpremiseEquipment_addDevicelink = "@xpath=//div[div[div[contains(text(),'Customer Premise Equipment (CPE)')]]]//a[text()='Add Device']";
		public static final String outband_customerpremiseEquipment_addDevice_routerIderrmsg = "@xpath=//div[text()='Router Id']";
		public static final String outband_customerpremiseEquipment_addDevice_nameErrmsg = "@xpath=//div[text()='Name']";
		public static final String outband_customerpremiseEquipment_addDevice_vendorErrmsg = "@xpath=//div[text()='Vendor/Model']";
		public static final String outband_customerpremiseEquipment_addDevice_managementAddressErrmsg = "@xpath=//div[text()='Management Address']";
		public static final String outband_customerpremiseEquipment_addDevice_snmproErrmsg = "@xpath=//div[text()='Snmpro']";
		public static final String outband_customerpremiseEquipment_addDevice_routerIdField = "@xpath=//div[label[contains(text(),'Router Id')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_deviceNameField = "@xpath=//div[label[contains(text(),'Device Name')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_vendorModelField = "@xpath=//div[label[contains(text(),'Vendor/Model')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_manageAddresField = "@xpath=//div[label[contains(text(),'Management Address')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmproField = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmprwField = "@xpath=//div[label[contains(text(),'Snmprw')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3ContextnameField = "@xpath=//div[label[contains(text(),'Snmp V3 Context Name')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3ContextEngineIdField = "@xpath=//div[label[contains(text(),'Snmp V3 Context Engine ID')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3SecurityUsernameField = "@xpath=//div[label[contains(text(),'Snmp V3 Security Username')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3authprotoDropdown = "@xpath=//div[label[contains(text(),'Snmp V3 Auth Proto')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_snmpv3authPasswordField = "@xpath=//div[label[contains(text(),'Snmp V3 Auth Password')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_premisedropdown = "@xpath=//div[label[text()='Premise']]//input";
		public static final String outband_customerpremiseEquipment_addDevice_selectpremiseToggleButton = "@xpath=(//div[div[label[text()='Select Premise']]]//div[@class='react-switch-handle'])[1]";
		public static final String outband_customerpremiseEquipment_addDevice_premisenameField = "@xpath=//div[label[contains(text(),'Premise Name')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_premiseCodeField = "@xpath=//div[label[contains(text(),'Premise Code')]]//input";
		public static final String outband_customerpremiseEquipment_addDevice_networkDropdown = "@xpath=";

		public static final String outband_customerpremiseEquipment_viewDevice_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_country = "@xpath=//div[div[label[contains(text(),'Country')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_city = "@xpath=//div[div[label[contains(text(),'City')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_site = "@xpath=//div[div[label[contains(text(),'Site')]]]//div[@class='customLabelValue form-label']";
		public static final String outband_customerpremiseEquipment_viewDevice_Premise = "@xpath=//div[div[label[contains(text(),'Premise')]]]//div[@class='customLabelValue form-label']";

		// public static final String !-- For Interface speed 10GigE --="@xpath=
		public static final String circuit_10GigeActiondropdown = "@xpath=//div[@class='dropdwon-actionBtn-right show dropdown']//button[@id='dropdown-basic-button']";
		public static final String Circuit_10GigEaddOverturelink = "@xpath=//a[contains(text(),'Add Overture')]";
		public static final String Circuit_10GigEconfigurelink = "@xpath=//a[contains(text(),'Configure')]";
		public static final String Circuit_10GigEPAMtest = "@xpath=//a[contains(text(),'PAM Test')]";
		public static final String Circuit_10GigEdeletelink = "@xpath=//a[contains(text(),'Delete')]";

		public static final String Overture_servicenameErrmsg = "@xpath=//div[text()='Service Name']";
		public static final String Overture_ServiceNameField = "@xpath=//label[text()='Service Name']";
		public static final String Overture_searchButton = "@xpath=//span[contains(text(),'Search')]";

		public static final String outband_customerpremiseEquipment_viewDevicePage_RouterToolPanel_commandIPv4 = "@xpath=//div[label[text()='Command IPvp4']]//input";
		public static final String outband_customerpremiseEquipment_viewDevicePage_RouterToolPanel_execute = "@xpath=//a[contains(text(),'Execute')]";

		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		
		public static final String circuitreferenceErrmsg = "@xpath=//div[text()='Circuit Reference']";

		// public static final String !-- 10May --="@xpath=
		public static final String EquipementConfigurationPanel = "@xpath=//div[div[contains(text(),'Equipment Configuration')]]";
		public static final String equipConfig_addCPEdevice = "@xpath=//div[div[contains(text(),'Equipment Configuration')]]/following-sibling::div//a";
		public static final String addActelisCPEpage_headerName = "@xpath=//div[div[p[text()='Add Actelis CPE']]]";

		public static final String nameField_addCPE_Actelis = "@xpath=//div[label[text()='Name']]//input";
		public static final String vendorField_addCPE_Actelis = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String RouterIdField_addCPE_Actelis = "@xpath=//div[label[text()='Router Id']]//input";
		public static final String managementAddressField_addCPE_Actelis = "@xpath=//div[label[text()='Management Address']]//input";
		public static final String MEPidField_addCPE_Actelis = "@xpath=//div[label[text()='MEP Id']]//input";
		public static final String ETHportField_addCPE_Actelis = "@xpath=//div[label[text()='ETH Port']]//input";

		public static final String devicenameFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Device Name']";
		public static final String RouterIDFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Router Id']";
		public static final String manageAddressFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Management Address']";

		public static final String fetchInterface_AddDSLAMdevice = "@xpath=//span[contains(text(),'Fetched interfaces successfully')]";

		public static final String InterfaceToSelect_actelis_totalpage = "@xpath=//span[@ref='lbTotal']";
		public static final String InterfaceToSelect_actelis_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String alertForSynchronize = "@xpath=//div[@role='alert']";
		public static final String alertMSG_synchronize = "@xpath=//div[@role='alert']/span";

		public static final String showInterface_ActelisCnfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//a[text()='Show Interfaces']";
		public static final String AcionButton_ActelisConfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//button[text()='Action']";
		public static final String removeButton_ActelisConfiguration = "@xpath=//a[text()='Remove']";
		public static final String popupMessage_forRemove_ActelisConfiguration = "@xpath=//div[@class='modal-body']";
		public static final String deleteInterface_successMessage_ActelisCOnfiguration = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";
		public static final String successMessage_ActelisConfiguration_removeInterface = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

	}

}
