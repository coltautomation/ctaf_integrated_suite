package pageObjects.aptObjects;

public class Lanlink_NewTab_Obj {
	public static class Lanlink_NewTab {
		public static final String ManageCustomerServiceLink = "@xpath=//span[b[contains(text(),'MANAGE CUSTOMER')]]";
		public static final String searchorderORservicelink = "@xpath=//li[text()='Search Order/Service']";
		public static final String searchService_serviceTextField = "@xpath=//label[text()='Service']/parent::div//input";
		public static final String searchButton = "@xpath=//span[text()='Search']";
		public static final String selectSearchedService = "@xpath=//div[text()='value']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String searchSerice_ActionDropdown = "@xpath=//button[text()='Action']";
		public static final String searchService_viewLink = "@xpath=//a[text()='View']";

		// Edit Service
		public static final String OrderPanel = "@xpath=//div[div[text()='Order']]";
		public static final String ActionDropdown_InViewPage = "@xpath=//div[div[text()='Service']]//button[text()='Action']";
		public static final String editLink_InViewPage = "@xpath=//a[text()='Edit']";
		public static final String editServicePage_pnaleHeader = "@xpath=//div[div[text()='Edit Service']]";

		public static final String emailField = "@xpath=//label[text()='Email']/parent::div//input";
		public static final String phoneField = "@xpath=//label[text()='Phone Contact']/parent::div//input";
		public static final String remarkField = "@xpath=//textarea[@name='remark']";
		public static final String deliveryChannelField = "@xpath=//label[text()='Delivery Channel']/parent::div//input";
		public static final String performancereporting = "@xpath=//label[text()='Performance Reporting']/parent::div//input";
		public static final String proactiveMonitoring = "@xpath=//label[text()='Proactive Monitoring']/parent::div//input";
		public static final String managementOrder = "@xpath=//label[text()='Management Order']/parent::div//input";
		public static final String notificationManagement = "@xpath=//label[text()='Notification Management Team']/parent::div//input";
		public static final String intermediateTechnology = "@xpath=//label[text()='Intermediate Technologies']/parent::div//input";
		public static final String circuitReferenceTextField = "@xpath=//label[text()='Circuit Reference']/parent::div//input";
		public static final String okButton = "@xpath=//span[text()='OK']";

		// Success Message
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=//div[@role='alert']/span";
		public static final String serivceAlert = "@xpath=//div[@role='alert']";

		// Breadcrumb
		public static final String breadCrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";
		public static final String breadCrumbForcurrentPage = "@xpath=//ol[@class='breadcrumb']//li[contains(text(),'value')]";
		// ol[@class='breadcrumb']//li[@class='breadcrumb-item']//a[contains(text(),'value')]

		public static final String staus_statuspopup = "@xpath=//div[@class='modal-content'] ";
		public static final String Statuspage_header = "@xpath=//div[@class='modal-header']//div ";
		public static final String statuspage_nameheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name'] ";
		public static final String statuspage_vendormodelheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model'] ";
		public static final String statuspage_managementaddressheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address'] ";
		public static final String statuspage_snmproheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro'] ";
		public static final String statuspage_countryheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country'] ";
		public static final String statuspage_cityheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City'] ";
		public static final String statuspage_siteheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site'] ";
		public static final String statuspage_premiseheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise'] ";
		public static final String statuspage_namevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']/following-sibling::div ";
		public static final String statuspage_vendormodelvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']/following-sibling::div ";
		public static final String statuspage_managementaddressvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']/following-sibling::div ";
		public static final String statuspage_snmprovalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']/following-sibling::div ";
		public static final String statuspage_countryvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']/following-sibling::div ";
		public static final String statuspage_cityvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']/following-sibling::div ";
		public static final String statuspage_sitevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']/following-sibling::div ";
		public static final String statuspage_premisevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']/following-sibling::div ";
		public static final String Statuspage_statusheader = "@xpath=//div[@class='modal-content']//div[@class='heading-green-row row']//div ";
		public static final String statuspage_currentstatusfieldheader = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status'] ";
		public static final String statuspage_newstatusfieldheader = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='New Status'] ";
		public static final String statuspage_currentstatusvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']/following-sibling::div ";
		public static final String statuspage_newstatusdropdown = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select ";
		public static final String statuspage_newstatusdropdownvalue = "@xpath=((//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select//option)[1] ";
		public static final String statuspage_okbutton = "@xpath=(//div[@class='modal-content']//button[@type='button']//span)[3] ";
		public static final String statuspage_statuscolumnheader = "@xpath=//span[@role='columnheader'][text()='Status'] ";
		public static final String statuspage_changedon_columnheader = "@xpath=//span[@role='columnheader'][text()='Changed On'] ";
		public static final String statuspage_changedby_columnheader = "@xpath=//span[@role='columnheader'][text()='Changed By'] ";
		public static final String statuspage_newstatusvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[1] ";
		public static final String statuspage_changedonvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[2] ";
		public static final String statuspage_changedbyvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[3] ";
		public static final String statuspage_closebutton = "@xpath=(//div[@class='modal-content']//button[@type='button'])[1] ";
		public static final String viewinterfacepage_header = "@xpath=//div[@class='modal-header']//div ";
		public static final String viewinterfacepage_interfacesubheader = "@xpath=//div[@class='modal-body']//div[@class='heading-green-row row']/div ";
		public static final String viewinterface_devicenamecolumnheader = "@xpath=(//div[@col-id='deviceName']//div[@ref='eLabel'])[1]/span[1] ";
		public static final String interfacename_columnheader = "@xpath=(//div[@col-id='name']//div[@ref='eLabel'])[1]/span[1] ";
		public static final String interfaceaddress_columnheader = "@xpath=(//div[@col-id='address']//div[@ref='eLabel'])[1]/span[1] ";
		public static final String interfacetype_columnheader = "@xpath=(//div[@col-id='type.desc']//div[@ref='eLabel'])[1]/span[1] ";
		public static final String interfaceaddress_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='address'])[1] ";
		public static final String interfacetype_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='type.desc'])[1] ";
		public static final String viewinterface_status_columnheader = "@xpath=(//div[@col-id='currentStatus.desc']//div[@ref='eLabel'])[1]/span[1] ";
		public static final String viewinterface_status_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='currentStatus.desc'])[1] ";
		public static final String viewinterface_lastmod_columnheader = "@xpath=(//div[@col-id='m_time']//div[@ref='eLabel'])[1]/span[1] ";
		public static final String viewinterface_lastmod_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='m_time'])[1] ";
		public static final String viewinterface_closebutton = "@xpath=(//div[@class='modal-header']//button[@type='button']/span)[1] ";
		public static final String statuspage_interfaceheader = "@xpath=(//div[@class='modal-header']//div)[2] ";
		public static final String interface_statuspage_namefield = "@xpath=//label[text()='Name'] ";
		public static final String interface_statuspage_interfaceaddressfield = "@xpath=//label[text()='Interface Address'] ";
		public static final String interface_statuspage_currentstatusfield = "@xpath=//label[text()='Current Status'] ";
		public static final String interface_statuspage_newstatusfield = "@xpath=//label[text()='New Status'] ";
		public static final String interface_statuspage_namevalue = "@xpath=//label[text()='Name']/following-sibling::div ";
		public static final String interface_statuspage_interfaceaddressvalue = "@xpath=//label[text()='Interface Address']/following-sibling::div ";
		public static final String interface_statuspage_currentstatusvalue = "@xpath=//label[text()='Current Status']/following-sibling::div ";
		public static final String interface_statuspage_newstatusdropdown = "@xpath=//label[text()='New Status']/parent::div//select ";
		public static final String interface_statuspage_newstatusdropdownvalue = "@xpath=//label[text()='New Status']/parent::div//select/option ";
		public static final String interface_statuspage_okbutton = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//button[@type='button']//span ";
		public static final String interface_statuspage_statuscolumnheader = "@xpath=(//div[@ref='eLabel']//span[text()='Status'])[2] ";
		public static final String interface_statuspage_changedon_columnheader = "@xpath=//div[@ref='eLabel']//span[text()='Changed On'] ";
		public static final String interface_statuspage_changedby_columnheader = "@xpath=//div[@ref='eLabel']//span[text()='Changed By'] ";
		public static final String interface_statuspage_newstatusvalue = "@xpath=(//div[@role='gridcell'][@col-id='status.desc'])[1] ";
		public static final String interface_statuspage_changedonvalue = "@xpath=(//div[@role='gridcell'][@col-id='changeDate'])[1] ";
		public static final String interface_statuspage_changedbyvalue = "@xpath=(//div[@role='gridcell'][@col-id='user'])[1] ";
		public static final String interface_statuspage_closebutton = "@xpath=(//div[@class='modal-header']//button[@type='button'])[2] ";
		public static final String interface_statuspage_statusheader = "@xpath=//div[@class='modal-body']//div[@class='heading-green-row row']//div[text()='Status'] ";
		public static final String searchdevice_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div ";

		// Search Device
		public static final String managecoltnetworklink = "@xpath=(//div[@class='ant-menu-submenu-title']/span/b)[2] ";
		public static final String searchdevicelink = "@xpath=//li[text()='Search for Device'] ";
		public static final String devicenamefield = "@xpath=//label[text()='Device Name']/following-sibling::input ";
		public static final String searchdevice_actiondropdown = "@xpath=//div[@class='dropdown']//button ";
		public static final String deviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')] ";
		public static final String managelink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage'] ";
		public static final String searchbutton = "@xpath=//span[text()='Search'] ";

		// success Message
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1] ";
		// public static final String AlertForServiceCreationSuccessMessage
		// ="@xpath=(//div[@role='alert']/span)[1] ";

		public static final String servicepanel_serviceidentificationvalue = "@xpath=//label[contains(text(),'Service Identification')]/parent::div/following-sibling::div";
		public static final String servicepanel_servicetypevalue = "@xpath=//label[contains(text(),'Service Type')]/parent::div/following-sibling::div";
		public static final String servicepanel_remarksvalue = "@xpath=//label[contains(text(),'Remark')]/following-sibling::div";
		public static final String servicepanel_terminationdate = "@xpath=//label[contains(text(),'Termination Date')]/parent::div/following-sibling::div";
		public static final String servicepanel_email = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Email')]/parent::div/following-sibling::div";
		public static final String servicepanel_phone = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Phone Contact')]/parent::div/following-sibling::div";
		public static final String servicepanel_billingtype = "@xpath=//label[contains(text(),'Billing Type')]/parent::div/following-sibling::div";
		public static final String servicepanel_header = "@xpath=//div[text()='Service']";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";

		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String synchronizelink_servicepanel = "@xpath=//a[text()='Synchronize']";
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";

		public static final String status_serviceheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service']";
		public static final String status_servicetypeheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service Type']";
		public static final String status_detailsheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Details']";
		public static final String status_statusheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Status']";
		public static final String status_modificationheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Last Modification']";
		public static final String sync_serviceheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service']";
		public static final String sync_servicetypeheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service Type']";
		public static final String sync_detailsheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Details']";
		public static final String sync_syncstatus = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Sync Status']";
		public static final String devicesforservice_deviceheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Device']";
		public static final String devicesforservice_syncstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Sync Status']";
		public static final String devicesforservice_fetchinterfacesheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Fetch Interfaces']";
		public static final String devicesforservice_vistamartdeviceheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='VistaMart Device']";

		public static final String status_ordername = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[2]/div/label";
		public static final String status_servicename = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]/a";
		public static final String status_servicetype = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";
		public static final String status_servicedetails = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]//a";
		public static final String status_currentstatus = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";
		public static final String status_modificationtime = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]";
		public static final String statuslink = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div/a[text()='Status']";

		public static final String sync_ordername = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[2]/div/label";
		public static final String sync_servicename = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]/a";
		public static final String sync_servicetype = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";
		public static final String sync_servicedetails = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]//a";
		public static final String sync_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";
		public static final String vistamart_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]/span";
		public static final String vistamart_datetime = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]//p";
		public static final String synchronization_serviceerror = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]/span";
		public static final String synchronizelink = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";

		public static final String statuspanel_header = "@xpath=//div[text()='Status']";
		public static final String servicestatus_header = "@xpath=//div[@class='modal-header']/div[text()='Service Status']";
		public static final String statuspage_serviceidentification = "@xpath=//label[text()='Service Identification']/following-sibling::div";
		public static final String statuspage_servicetype = "@xpath=//label[text()='Service Type']/following-sibling::div";
		public static final String servicestatushistory_header = "@xpath=//div[text()='Service Status History']";
		public static final String statuspage_currentstatus = "@xpath=//label[text()='Current Status']/following-sibling::div";
		// public static final String statuspage_newstatusdropdown
		// ="@xpath=//label[text()='New Status']/following-sibling::select";
		// public static final String statuspage_newstatusdropdownvalue
		// ="@xpath=//label[text()='New
		// Status']/following-sibling::select/option";

		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String devicesforservice_header = "@xpath=//div[text()='Devices for service']";
		public static final String deviceforservicepanel_devicename = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[1]";
		public static final String deviceforservicepanel_syncstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[2]";
		public static final String deviceforservicepanel_smartsstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/span";
		public static final String deviceforservicepanel_smartsdatetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/p";
		public static final String deviceforservicepanel_fetchinterfaces_status = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/span";
		public static final String deviceforservicepanel_fetchinterfaces_datetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/p";
		public static final String deviceforservicepanel_vistamart_status = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[4]/span";
		public static final String deviceforservicepanel_vistamart_datetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[4]/p";
		public static final String deviceforservicepanel_managelink = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div/a[text()='Manage']";

		public static final String managenetwork_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div";

		public static final String successmsg = "@xpath=//div[@role='alert']//span";
		public static final String editservice_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String managesubnets_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets']";
		public static final String managesubnet_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Manage')]";
		public static final String managesubnet_successmsg = "@xpath=(//div[@role='alert']//span)[2]";
		public static final String spacename_column = "@xpath=//span[text()='Space Name']";
		public static final String blockname_column = "@xpath=//span[text()='Block Name']";
		public static final String subnetname_column = "@xpath=//span[text()='Subnet Name']";
		public static final String startaddress_column = "@xpath=//span[text()='Start Address']";
		public static final String size_column = "@xpath=//span[text()='Size']";
		public static final String closesymbol = "@xpath=//button[@type='button']//span[text()='�']";
		public static final String dumppage_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Service')]";
		public static final String serviceretrieved_text = "@xpath=//div[@class='div-margin row'][contains(text(),'Service retrieved')]";
		public static final String service_header = "@xpath=//label[text()='Service']";
		public static final String dumppage_text = "@xpath=//label[text()='Service']/following-sibling::textarea";

		public static final String managesubnetsipv6_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";
		public static final String shownewinfovistareport_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";
		public static final String showinfovistareport_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show Infovista Report']";
		public static final String dump_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Dump']";
		public static final String selectedemail = "@xpath=//select[@name='selectedEmail']//option";
		public static final String selectedphone = "@xpath=//select[@name='selectedPhoneContact']//option";
		public static final String emailremovearrow = "@xpath=(//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String phoneremovearrow = "@xpath=(//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span)[2]";
		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";
		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";
		public static final String emailaddarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phoneaddarrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";

		public static final String performancereporting_value = "@xpath=//div[label[text()='Performance Reporting']]/following-sibling::div";
		public static final String waveservice_value = "@xpath=//div[label[text()='Wave Service']]/following-sibling::div";
		public static final String interfacespeed_value = "@xpath=//div[label[text()='Interface Speed']]/following-sibling::div";
		public static final String typeofservice_value = "@xpath=//div[label[text()='Type Of Service']]/following-sibling::div";

		// Device Header
		public static final String addeddevices_list = "@xpath=//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row']//b";
		public static final String addeddevice_selectinterfaceslink = "@xpath=//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";
		public static final String deviceheader = "@xpath=//div[text()='Device']";
		public static final String AddDevice = "@xpath=//a[text()='Add Device']";
		public static final String showinterfaces_link = "@xpath=//a[text()='Show Interfaces']";
		public static final String adddevice_header = "@xpath=//p[text()='Add Device']";
		public static final String addnewdevice_togglebutton = "@xpath=(//div[@class='react-switch-handle'])[1]";
		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";
		public static final String portalaccess_header = "@xpath=//label[text()='Portal Access']";
		// public static final String successmsg
		// ="@xpath=//div[@role='alert']//span";
		public static final String breadcrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";

		// Create Device Page
		public static final String name_dropdown = "@xpath=(//div[label[text()='Name']]//input)[1]";
		public static final String devicename_textfield = "@xpath=//input[@id='deviceName']";
		public static final String vendormodelinput = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String snmprotextfield = "@xpath=//input[@id='snmpro']";
		public static final String telnetradiobutton = "@xpath=//input[@value='telnet']";
		public static final String sshradiobutton = "@xpath=//input[@value='ssh']";
		public static final String c2cradiobutton = "@xpath=//input[@value='2c']";
		public static final String c3radiobutton = "@xpath=//input[@value='3']";
		// public static final String snmprotextfield
		// ="@xpath=//input[@id='snmpro']";
		public static final String snmprwtextfield = "@xpath=//input[@id='snmprw']";
		public static final String snmpv3username = "@xpath=//div[label[text()='Snmp v3 Username']]//input";
		public static final String snmpv3authpassword = "@xpath=//div[label[text()='Snmp v3 Auth Password']]//input";
		public static final String snmpv3privpassword = "@xpath=//div[label[text()='Snmp v3 Priv Password']]//input";
		public static final String securityprotocols_dropdown = "@xpath=//div[label[contains(text(),'Security Protocols')]]//input";
		public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String managementaddresstextbox = "@xpath=//input[@id='addressSelect']";
		public static final String citydropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//input";
		public static final String sitedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//input";
		public static final String premisedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//input";
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String successMessage_Alert = "@xpath=//div[@role='alert']//span";
		public static final String addeddevice_interface_actiondropdown = "@xpath=//div[div[b[text()='value']]]//following-sibling::div//button[text()='Action']";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";

		// view device page
		public static final String viewpage_devicename = "@xpath=//label[text()='Name']/parent::div/following-sibling::div";
		public static final String viewpage_vendormodel = "@xpath=//label[text()='Vendor/Model']/parent::div/following-sibling::div";
		public static final String viewpage_managementaddress = "@xpath=//label[text()='Management Address']/parent::div/following-sibling::div";
		public static final String viewpage_securityprotocols = "@xpath=//label[text()='Security Protocols']/parent::div/following-sibling::div";
		public static final String viewpage_country = "@xpath=//label[text()='Country']/parent::div/following-sibling::div";
		public static final String viewpage_city = "@xpath=//label[text()='City']/parent::div/following-sibling::div";
		public static final String viewpage_site = "@xpath=//label[text()='Site']/parent::div/following-sibling::div";
		public static final String viewpage_premise = "@xpath=//label[text()='Premise']/parent::div/following-sibling::div";
		public static final String viewpage_connectivityprotocol = "@xpath=//label[text()='Connectiviy Protocol']/parent::div/following-sibling::div";
		public static final String viewpage_snmpversion = "@xpath=//label[text()='SNMP Version']/parent::div/following-sibling::div";
		public static final String viewpage_snmpro = "@xpath=//label[text()='Snmpro']/parent::div/following-sibling::div";
		public static final String viewpage_snmprw = "@xpath=//label[text()='Snmprw']/parent::div/following-sibling::div";
		public static final String viewpage_snmpv3username = "@xpath=//label[text()='Snmp V3 User Name']/following-sibling::div";
		public static final String viewpage_snmpv3privpassword = "@xpath=//label[text()='Snmp V3 Priv Password']/following-sibling::div";
		public static final String viewpage_snmpv3authpassword = "@xpath=//label[text()='Snmp V3 Auth Password']/following-sibling::div";

		// Edit Device
		public static final String viewdevice_Actiondropdown = "@xpath=//div[text()='Device Details']/following-sibling::div//button[text()='Action']";
		public static final String viewdevice_Edit = "@xpath=//div[text()='Device Details']/following-sibling::div//a[text()='Edit']";
		public static final String editdeviceheader = "@xpath=//div[@class='heading-green-row row']//p[text()='Edit Device']";

		public static final String existingdevicegrid = "@xpath=(//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row'])[2]";
		public static final String viewservicepage_viewdevicelink = "@xpath=//a//span[text()='View']";
		public static final String viewdevicepage_header = "@xpath=//div[text()='Device Details']";
		public static final String viewservicepage_editdevicelink = "@xpath=//a/span[text()='Edit']";

		// Delete Device
		public static final String viewdevice_delete = "@xpath=//div[text()='Device Details']/following-sibling::div//a[text()='Delete']";
		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='�']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";
		public static final String managementaddressbutton = "@xpath=//button//span[text()='Management Address']";
		public static final String addeddevice_deletefromservicelink = "@xpath=//div[text()='Device']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete']";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()='Delete']";

		public static final String Deviceemsg = "@xpath=//span[contains(text(),'Device successfully created.')]";
		public static final String Devicedeleteemsg = "@xpath=//span[contains(text(),'Device successfully deleted')]";
		public static final String InterfaceSpeedDropdown = "@xpath=(//div[label[text()='Interface Speed']]//input)[1]";
		public static final String TypeofServiceDropdown = "@xpath=(//div[label[text()='Type Of Service']]//input)[1]";
		public static final String ChangeordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number (Parent SID)']]//input)[1]";
		public static final String AddDeviceName = "@xpath=(//div[label[text()='Name']]//input)[1]";
		public static final String Xbutton = "@xpath=//div[text()='�']";
		public static final String Backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String Synchronizemsg = "@xpath=//span[contains(text(),'Sync started successfully. Please check the sync status of this service.')]";
		public static final String Errormsg = "@xpath=//div[text()='Error']";

		// order panel
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";
		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String orderpanelheader = "@xpath=(//div[contains(text(),'Order')])[1]";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// Search order
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		// Select Interface
		public static final String viewdeviceheader = "@xpath=//div[text()='Device']";
		public static final String interfaceheader = "@xpath=//div[text()='Interfaces']";
		public static final String interfacespanel_actiondropdown = "@xpath=//div[div[text()='Interfaces']]//following-sibling::div//button[text()='Action']";
		public static final String interfacespanel_OK_link = "@xpath=//button[text()='Action']/following-sibling::div/a[text()='Ok']";
		

		// =====================================================================

		public static final String VPNTopology1 = "@xpath=//div[contains(text(),'";
		public static final String VPNTopology2 = "')]";
		public static final String selectService = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String breadcrumb1 = "@xpath=//ol[@class='breadcrumb']//li[@class='breadcrumb-item']//a[contains(text(),'value')]";
		public static final String srvicePanel = "@xpath=//div[div[text()='Service']]";

	}
}
