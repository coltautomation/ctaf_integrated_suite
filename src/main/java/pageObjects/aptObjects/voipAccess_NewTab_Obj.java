package pageObjects.aptObjects;

import org.openqa.selenium.By;

public class voipAccess_NewTab_Obj {

	public static class voipAccess_NewTab {

		public static final String searchorderORservicelink = "@xpath=//li[text()='Search Order/Service']";
		public static final String searchService_serviceTextField = "@xpath=//label[text()='Service']/parent::div//input";
		public static final String searchButton = "@xpath=//span[text()='Search']";
		public static final String selectSearchedService = "@xpath=//div[text()='value']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String searchSerice_ActionDropdown = "@xpath=//button[text()='Action']";
		public static final String searchService_viewLink = "@xpath=//a[text()='View']";

		// MAS switch
		public static final String managementOptionsPanelheader = "@xpath=//div[div[contains(text(),'Management Options')]]";
		public static final String MASswitch_panelHeader = "@xpath=//div[div[text()='MAS Switch']]";
		public static final String addMASswitch_link = "@xpath=//a[contains(text(),'Add MAS Switch')]";
		public static final String MAS_AddMASSwitch_header = "@xpath=//div[contains(text(),'Add MAS Switch')]";
		public static final String MASswitch_warningmessage = "@xpath=//div[text()='IMS POP Location']";
		public static final String AddmasSWitch_IMSpopswitch_dropdown = "@xpath=//div[label[text()='IMS POP Location']]//input";
		public static final String okButton = "@xpath=//span[text()='OK']";
		public static final String MAS_fetchAlldevice_InviewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b";
		public static final String CPEdevice_fetchAlldevice_inViewPage = "@xpath=(//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row']//b)[4]";
		// need to change below locator MAS_deleteFromService_InViewPage
		public static final String MAS_deleteFromService_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";
		public static final String MAS_viewLink_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()=' Delete']";
		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";

		// Edit MAS switch
		public static final String editLink = "@xpath=(//span[text()='Edit'])[1]";
		public static final String MAS_View_ActionLink = "@xpath=(//button[text()='Action'])[1]";
		public static final String MAS_View_Action_EditLink = "@xpath=//a[text()='Edit']";
		public static final String MAS_deviceName = "@xpath=//div[label[text()='Name']]";
		public static final String MAS_vendorModel = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String MAS_managementAddress = "@xpath=//div[label[text()='Management Address']]//input";
		public static final String MAS_snmpro = "@xpath=//div[label[text()='Snmpro']]//input";

		// Success message
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=//div[@role='alert']/span";
		public static final String serivceAlert = "@xpath=//div[@role='alert']";

		// Breadcrumb
		public static final String breadCrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";
		public static final String breadCrumbForcurrentPage = "@xpath=//ol[@class='breadcrumb']//li[contains(text(),'value')]";

		///////////////////// --------Newly added-------//////
		public static final String ManageCustomerServiceLink = "@xpath=//span[b[contains(text(),'MANAGE CUSTOMER')]]";
		public static final String selectService = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String existingdevicegrid_MAS = "//div[text()='MAS Switch']/parent::div/following-sibling::table";
		public static final String MAS_deleteSuccessMessage = "";
		public static final String listofvalues = "@xpath=//div[@class='sc-bxivhb kqVrwh']";
		public static final String MASSwitch = "@xpath=//div[text()='MAS Switch']";

		public static final String editMASSwitch1 = "@xpath=//tr//*[contains(.,'";
		public static final String editMASSwitch2 = "@xpath=')]//following-sibling::span//a[contains(.,'Edit')]";
		public static final String editSwitchLink = "@xpath=(//a[contains(text(),'Edit')])[1]";
		public static final String existingdevicegrid = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::table";
		public static final String deleteBtnAlert = "@xpath=//button[@type='button' and contains(text(),'Delete')]";
		public static final String back = "@xpath=(//nav//ol//li/following-sibling::li/following-sibling::li)[1]";
		public static final String deleteFromService1 = "@xpath=//tr//*[contains(.,'";
		public static final String deleteFromService2 = "@xpath=')]//following-sibling::span//a[contains(.,'Delete from Service')]";
		public static final String vendermodel = "@xpath=(//div[label[text()='Vendor/Model']]//input)[1]";

	}

}
