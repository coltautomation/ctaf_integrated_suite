package pageObjects.c4cObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class C4CLoginObj {

	public static class Login
	 { 
	  //Login page elements
		public static final String userNameTxb="@id=userId"; //Username
		public static final String passWordTxb="@name=Ecom_Password"; //Password
		public static final String loginBtn="@xpath=//button[@type='submit' and contains(text(), 'Login')]"; //Login Button		
		
		//Landing page elements
		public static final String homeMenu="@xpath=//descendant::a[contains(text(), 'Home')]"; //Landing Page Home Menu
		public static final String verifyEditLnk="@xpath=//button[@title='Click to Personalize / Adapt']"; //Landing Page Home Menu
		
		
	 }

	
}