package pageObjects.exploreObjects;

public class ExploreLoginObj {

	public static class Login
	 { 
	  //Login page elements
		public static final String userNameTxb="@id=userId"; //Username
		public static final String passWordTxb="@name=Ecom_Password"; //Password
		public static final String loginBtn="@xpath=//button[@type='submit' and contains(text(), 'Login')]"; //Login Button		
		
		//Landing page elements
		public static final String mapViewBtn="@xpath=//span[text()='Map View']"; //Landing Page Home Menu
	 }
	
}