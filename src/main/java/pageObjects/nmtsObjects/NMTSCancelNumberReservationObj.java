package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSCancelNumberReservationObj {
	
	public static class CancelResNumber
	{ 
		//public static final String successmsg="@xpath="; 
		public static final String ProcessReservationtab="@xpath=//font[contains(@onclick,'M=PR')]";
		public static final String SelectReservationColt="@xpath=//select[@id='ucxOrderHistory_cboHostedNumberFilter']";
		public static final String madeByNMTS="@xpath=//select[@id='ucxOrderHistory_cboSiteUsers']";
		public static final String Availablity="@xpath=//select[@id='ucxOrderHistory_cboAvailability']"; 
		public static final String FilterBy="@xpath=//select[@id='ucxOrderHistory_cboFilterOption']"; 
		public static final String matching="@xpath=//select[@id='ucxOrderHistory_cboFilterMethod']";
		public static final String FilterText="@xpath=//input[@id='ucxOrderHistory_txtFilterText']";
		public static final String SubmitSearchbtn="@xpath=//input[@id='ucxOrderHistory_btnSubmitSearch']"; 
		public static final String OrderId="@xpath=//table[@id='ucxOrderHistory_grdView']//tr[contains(@ondblclick,'hdnIndex')]/td[1]"; 
		public static final String OrderId1="//table[@id='ucxOrderHistory_grdView']"; 
        public static final String SelectReservedRangeCheckbox="@xpath=(//table[@id='ucxRangeList_grdView']//input[contains(@id,'ucxRangeList_grdView')])[1]";
	    public static final String ReservedRangeSelectMarkedLink="@xpath=(//a[contains(text(),'Select Marked')])[1]";
        public static final String SelectedRangeCheckbox="@xpath=(//table[@id='ucxSelectRange_grdView']//input[contains(@id,'ucxSelectRange_grdView')])[1]";
	    public static final String selectDecision="@xpath=//select[@id='cboProcessingStatus']";
	    public static final String RejectReason="@xpath=//select[@id='cboRejectionReason']";
	    public static final String SubmitDecision="@xpath=//input[@id='btnSubmitDecision']";
	    public static final String MRN="@xpath=//input[@id='txtMRN']";
	    public static final String successmsg="@xpath=//span[@id='lblSuccessMsg']";
		}

}