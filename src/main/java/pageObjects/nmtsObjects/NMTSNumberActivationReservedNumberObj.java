package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSNumberActivationReservedNumberObj {
	
	public static class NumberActivationReserveNumber
	{
		
		//public static final String OrderId="@xpath="; 
		//public static final String ProcessReservationtab="@xpath=(//table[@id='tblTaskMenu']//tr)[2]/td";
		public static final String ProcessReservationtab="@xpath=//font[contains(@onclick,'M=PR')]";
		public static final String SelectReservationColt="@xpath=//select[@id='ucxOrderHistory_cboHostedNumberFilter']";
		public static final String madeByNMTS="@xpath=//select[@id='ucxOrderHistory_cboSiteUsers']";
		public static final String Availablity="@xpath=//select[@id='ucxOrderHistory_cboAvailability']"; 
		public static final String FilterBy="@xpath=//select[@id='ucxOrderHistory_cboFilterOption']"; 
		public static final String matching="@xpath=//select[@id='ucxOrderHistory_cboFilterMethod']";
		public static final String FilterText="@xpath=//input[@id='ucxOrderHistory_txtFilterText']";
		public static final String SubmitSearchbtn="@xpath=//input[@id='ucxOrderHistory_btnSubmitSearch']"; 
		public static final String OrderId="@xpath=//table[@id='ucxOrderHistory_grdView']//tr[contains(@ondblclick,'hdnIndex')]/td[1]"; 
		public static final String OrderId1="//table[@id='ucxOrderHistory_grdView']"; 
		
	}

}