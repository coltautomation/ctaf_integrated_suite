package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSNumberDeactivationObj {
	
	public static class NumberDeactivation
	{
		//public static final String Deactivationsuccessmsg="@xpath=//span[@id='lblSuccessfulMsg']"; 
		public static final String CeaseButton="@xpath=//font[contains(@onclick,'OrderList.aspx?M=CD')]"; 
		public static final String madeByNMTS="@xpath=//select[@id='ucxOrderHistory_cboSiteUsers']";
		                                                            
		public static final String Coltandhostednumber="@xpath=//select[@id='ucxOrderHistory_cboHostedNumberFilter']"; 
		public static final String DeactivationAvail="@xpath=//select[@id='ucxOrderHistory_cboAvailability']";
		public static final String FilterBy="@xpath=//select[@id='ucxOrderHistory_cboFilterOption']";
		public static final String matching="@xpath=//select[@id='ucxOrderHistory_cboFilterMethod']";
		public static final String FilterText="@xpath=//input[@id='ucxOrderHistory_txtFilterText']";
		public static final String SubmitDeactivate="@xpath=//input[@id='ucxOrderHistory_btnSubmitSearch']";
		public static final String errormsgnorecord="@xpath=//table[@id='ucxOrderHistory_grdView']//tr/td[contains(text(),'No Record Found.')]"; 
		public static final String OrderId="@xpath=//table[@id='ucxOrderHistory_grdView']//tr[2]/td";
		public static final String checkbox="@xpath=//input[@id='ucxRangeList_grdView__ctl2_chkMark']";
		public static final String SelectedAllLink="@xpath=//table[3]//table[1]//tr[3]//td[2]/a[contains(text(),'Select Marked')]"; 
		public static final String AllocationCheckboxagain="@xpath=//input[@id='ucxSelectRange_grdView__ctl2_chkMark']";
		public static final String CancelQuarntined="@xpath=//input[@id='btnCancel']";
		public static final String yespopup="@xpath=//img[contains(@src,'Images/General/Yes.JPG')]/..";
		public static final String Deactivationsuccessmsg="@xpath=//span[@id='ucxOrderAttributes_lblOrderStatus']"; 
	}

}