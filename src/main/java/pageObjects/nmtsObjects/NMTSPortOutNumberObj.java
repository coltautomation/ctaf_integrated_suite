package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSPortOutNumberObj {
	
	public static class PortOutNumber
	{ 
		//public static final String PortOutStatus="@xpath="; 
		public static final String portoutNMTS="@xpath=//font[contains(@onclick,'NumberSearchFree.aspx?M=PO')]";
		public static final String portoutforCH="@xpath=//font[contains(@onclick,'SwitzerlandPages/PortoutReturnTransfer')]";
		//'SwitzerlandPages/PortoutReturnTransfer.aspx?M=OUT'
		public static final String sELECThOSTEDnO="@xpath=//select[@id='ucxSearchCriteria_cboHostedNumber']";
		public static final String wholesaleradiobtn="@xpath=//input[@id='ucxSearchCriteria_radColt_1']";
		public static final String portoutradiobtn="@xpath=//input[@id='ucxSearchCriteria_radPortout']";
		//public static final String AreaCodePrefix="@xpath=//input[@id='btnArea']";
		public static final String AreaCodePrefix="@xpath=//input[@id='ucxSearchCriteria_txtNationalCode']";
		public static final String AreaCodeExt="@xpath=//input[@id='ucxSearchCriteria_txtAreaCode']";
		public static final String RangeFrom="@xpath=//input[@id='ucxSearchCriteria_txtLowerBound']";
		public static final String retandtransferRangeto="@xpath=//input[@id='ucxSearchCriteria_txtUpperBound']";
		public static final String Submitbtn="@xpath=//input[@id='ucxSearchCriteria_btnSubmitSearch']";
		public static final String checkbox3="@xpath=//input[@id='ucxRange_grdView__ctl2_chkMark']";
		public static final String Selectmark="@xpath=//a[@id='ucxRange_btnDeleteMarked']";
		public static final String AllocationCheckboxagain="@xpath=//input[@id='ucxSelectRange_grdView__ctl2_chkMark']";
		public static final String RangeHolder="@xpath=//select[@id='cboRangeHolder']";
		public static final String ContractNumber="@xpath=//input[@id='txtContractNumber']";
		public static final String ContractSrc="@xpath=//select[@id='cboContractSource']";
		public static final String portoutdateNMTS="@xpath=//input[@id='txtDate']";
		public static final String Portoutselectedrangebtn="@xpath=//input[@id='btnRequestSelectedRanges']";
		public static final String YesBtn="@xpath=//input[@id='btnYes']";
		//public static final String PortOutStatus="@xpath=//div[@id='lblSuccessfulMsg']";
		
		public static final String PortOutStatus="@xpath=//span[@id='lblSuccessfulMsg']";
		}

}