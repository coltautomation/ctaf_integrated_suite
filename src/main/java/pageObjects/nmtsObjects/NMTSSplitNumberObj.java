package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSSplitNumberObj {
	
	public static class SplitNumber
	{ 
		public static final String SplitNumberButton="@xpath=//font[contains(@onclick,'SplitNumberNL')]";
		public static final String SplitNumber="@xpath=//input[@id='txtSearchNum']";
		public static final String SplitSearchButton="@xpath=//input[@id='btnSearchNum']";
		public static final String Norecord="@xpath=//span[contains(text(),'No Record Found.')]";
		public static final String Rangefromspit="@xpath=//span[@id='lblRangeFrom']";
		public static final String Rangetospit="@xpath=//span[@id='lblRangeTo']";
		public static final String SplitblockSizesplit="@xpath=(//input[contains(@id,'_txtSplitBlockSize')])[indexrow]";
	    public static final String SplitQuantitysizesplit="@xpath=(//input[contains(@id,'_txtSplitQuantity')])[indexrow]";
		public static final String SplitAddButn="@xpath=//input[contains(@id,'_btnAddSplitNumberGrid')]";
		public static final String splitFreenumbtn="@xpath=//input[@id='btnSplitFreeNumber']";
		public static final String successmsg="@xpath=//span[@id='lblSuccessfulMsg']";
		public static final String AreaCodePrefix="@xpath=//span[@id='lblNational']";
		public static final String AreaCodeExtension="@xpath=//span[@id='lblArea']";
		public static final String Availabilty="@xpath=//span[@id='lblAvaila']";
	
	}

}