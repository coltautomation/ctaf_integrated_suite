package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODAddressUpdateObj {

	
	public static class AddressUpdate
	{ 
	  /*  public static final String ActionOption="@xpath=(//div[@tabulator-field='actions'])[2]";
		public static final String AddressUpdateOption="@xpath=//*[@id='actionModal']//*[text()='Address Update']"; 
		public static final String AddressUpdatePage ="@xpath=(//*[contains(text(),'Update Address')])[1]";
		public static final String ActiationPage ="@xpath=//*[contains(text(),'Activation')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String CustomerName ="@xpath=//input[@id='emerBusinessName']";
		public static final String CustomerNameUpdated ="@xpath=//input[@id='emerBusinessNameUpdated']";
		public static final String BuildingNumberUpdated ="@xpath=//input[@id='emerBuildingNumberUpdated']";
		public static final String StreetNameUpdated ="@xpath=//input[@id='emerStreetUpdated']";
		public static final String CityOrTownUpdated ="@xpath=//input[@id='emerTownUpdated']";
		public static final String PostalCodeUpdated ="@xpath=//input[@id='emerPostCodeUpdated']";
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		public static final String AddressValidationMsg ="@xpath=//h5[contains(text(),'Provided address has validated successfully')]";
		public static final String UpdateAddressBtn="@xpath=//button[contains(text(),'Update Address')]"; */
		
		public static final String TableRecordLoader="@xpath=//*[@class='ph-item tblLoader']"; 
		public static final String QuickLinkDrpDwn="@xpath=//div[contains(@class,'ng-select-container')]//div[contains(text(),'Quick Links')]"; 
		public static final String QuickLinkDrpDwnArw="@xpath=//body/app-root[1]/app-my-orders[1]/div[1]/div[1]/div[1]/div[2]/app-profile[1]/div[1]/form[1]/div[1]/ng-select[1]/div[1]/span[1]";
		public static final String ActivationLink ="@xpath=//label[contains(text(),'Activation')]";
		public static final String ActiationPage ="@xpath=//*[contains(text(),'Activation')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String CustomerName ="@xpath=//input[@id='emerBusinessName']";
		public static final String BuildingNumber ="@xpath=//input[@id='emerBuildingNumber']";
		public static final String StreetName ="@xpath=//input[@id='emerStreet']";
		public static final String CityOrTown ="@xpath=//input[@id='emerTown']";
		//public static final String LocalAreaCode ="@xpath=//input[@autocomplete='a8aef217964e']";
		public static final String CityOrTownFR ="@xpath=//select[@id='cityTownDirNA_FR']";
		public static final String PostalCode ="@xpath=//input[@id='emerPostCode']";
		public static final String PostalCodeFR ="@xpath=//select[@id='emerPostCode']";
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		
		public static final String NotifySupportBtn="@xpath=//button[contains(text(),'Notify Colt Support Team')]"; 
		public static final String AddDetailsTextArea="@xpath=//textarea[@name='notification']"; 
		public static final String SubmitBtn="@xpath=//*[@class='modal-footer']//button[contains(text(),'Submit')]"; 
		public static final String OrderID="@xpath=//*[contains(text(),'Order ID')]//a"; 

			
	}
	
	
}