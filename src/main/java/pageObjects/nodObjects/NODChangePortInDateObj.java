package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODChangePortInDateObj {

	
	public static class Date
	{ 
		public static final String DateChangeOption="@xpath=//*[@id='actionModals']//*[text()='Date Change']"; 
		//public static final String Calendarbtn="@xpath=//*[@id='dateUpdatePortIn']/parent::div//button"; 
		public static final String Calendarbtn="@xpath=//*[@id='requestPortDate']/parent::div//button"; 

		public static final String Month="@xpath=//select[@title='Select month']";
		public static final String Year="@xpath=//select[@title='Select year']";
		public static final String Day="@xpath=(//ngb-datepicker-month//*[text()='day'])[1]";
		
		//public static final String PortInTime="@xpath=//ng-select[@id='time']";
		public static final String PortInTime="@xpath=//ng-select[@id='requestPortTimeDD']";
		public static final String Time="@xpath=//ng-dropdown-panel//*[text()='Time']";
		//public static final String DateChangeBtn="@xpath=//button[text()='Date Change']";
		public static final String ModifyPortBtn="@xpath=//button[contains(text(),'Modify Port')]";
		public static final String CurrentOperatorBtn="@xpath=//select[@id='currentProvider']";
		
		public static final String mainBillingNumber="@xpath=//input[@id='mainBilling']";

		
		
		public static final String DateChangePopUp="@xpath=//*[contains(text(),'Change Date and Time')]"; 

		public static final String TXPg_DateChangeOpt="@xpath=//option[contains(text(),'Date Change')]"; 

		public static final String ModifyPortAction="@xpath=//*[contains(text(),'Modify Port')]"; 
		
		public static final String SelectDate="@xpath=(//*[@class='ngb-dp-day ng-star-inserted'])[1]"; 
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}