package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODDSUAddressUpdateObj {

	public static class DSUAddressUpdate
	{
		public static final String SkipAddressUpdateBtn="@xpath=//button[contains(text(),'Skip')]"; 
		public static final String telphoneNo="@xpath=//input[@id='telphoneNo']"; 
		public static final String DSUStreetType="@xpath=//input[@id='dsuStreetType']"; 
		public static final String DSUStreetName="@xpath=//input[@id='acStreet']"; 
		public static final String DSUPostalCode="@xpath=//input[@id='acPostCode']"; 
		public static final String DSUCityTown="@xpath=//input[@id='acCitytown']"; 
		public static final String DSUprovince="@xpath=//input[@id='acProvince']"; 
		public static final String DSUValidateAddressBtn="@xpath=(//button[contains(text(),'Validate Address')])[2]"; 
		public static final String PremisesNumber="@xpath=//input[@id='bNumber']"; 
		public static final String AdvertisementFlag="@xpath=//select[@id='dsuAdvFlag']"; 
		public static final String OrderReviewBtn="@xpath=//button[contains(text(),'Order Review')]"; 
		public static final String OrderReviewSection="@xpath=//p[contains(text(),'Order Review')]"; 
		public static final String SubmitButton="@xpath=//button[contains(text(),'Submit')]"; 
		public static final String OrderConfirmationMsg ="@xpath=//div[@id='addrUpdate-l-4']";
		public static final String SuccessMessage ="@xpath=//*[contains(text(),'Thank')]";	
	}




































}