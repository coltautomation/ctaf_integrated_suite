package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODLogoutObj 
{

	public static class Logout
	{ 
		public static final String settingIcon="@xpath=//*[@id='dropdownMenuButton'][@class='btn ct-icon-box dropdown-toggle']"; //Username
		public static final String logoutOption="@xpath=//a[contains(text(),'Logout')]"; //Logout option


	}

}