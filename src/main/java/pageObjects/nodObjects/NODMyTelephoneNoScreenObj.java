package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODMyTelephoneNoScreenObj {


	public static class TelephoneNoScreen
	{ 
		public static final String ActionDrpDwn="@xpath=//*[@class='actionLinks ml-4 ng-star-inserted']";
		//		public static final String AddressUpdateOption="@xpath=//*[@class='actionLinks ml-4 ng-star-inserted']//*[text()='Address Update']"; 

		public static final String Update_CustomerName="@xpath=//input[@id='emerBusinessNameUpdated']"; 
		public static final String Update_BuildingNumber="@xpath=//input[@id='emerBuildingNameUpdated']"; 
		public static final String Update_StreetName="@xpath=//input[@id='emerStreetUpdated']"; 
	//	public static final String Update_CityOrTown="@xpath=//input[@id='emerTownUpdated']"; 
	//	public static final String Update_PostalCode="@xpath=//input[@id='emerPostCodeUpdated']"; 
		public static final String Update_CityOrTown="@xpath=//input[contains(@id,'emerTown')]"; 
		public static final String Update_PostalCode="@xpath=//input[contains(@id,'emerPostCode')]"; 

		public static final String AddressSuggestion="@xpath=(//*[@class='card rounded-0'])[1]"; 
		public static final String AddressUpdateMessage="@xpath=//*[contains(text(),'Provided address has validated successfully')]"; 
		public static final String AddressUpdatebtn="@xpath=//button[contains(text(),'Update Address')]"; 
		//	public static final String AddressUpdate_SuccessMsg="@xpath=//*[contains(text(),'Thank you for your order. Your order is raised suc')]"; 

		public static final String province="@xpath=//input[@id='province']"; 
		public static final String CityOrTownDK ="@xpath=//select[@id='emerTownUpdated']"; //9/9



		public static final String ActionOption="@xpath=(//div[@tabulator-field='actions'])[2]";
		public static final String AddressUpdateOption="@xpath=//*[@id='actionModal']//*[text()='Address Update']"; 
		public static final String AddressUpdatePage ="@xpath=(//*[contains(text(),'Update Address')])[1]";
		public static final String ActiationPage ="@xpath=//*[contains(text(),'Activation')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String CustomerName ="@xpath=//input[@id='emerBusinessName']";
		public static final String CustomerNameUpdated ="@xpath=//input[contains(@id,'emerBusinessName')]";
		/*
		public static final String BuildingNumberUpdated ="@xpath=//input[@id='emerBuildingNumberUpdated']";
		public static final String StreetNameUpdated ="@xpath=//input[@id='emerStreetUpdated']";
		public static final String CityOrTownUpdated ="@xpath=//input[@id='emerTownUpdated']";
		public static final String PostalCodeUpdated ="@xpath=//input[@id='emerPostCodeUpdated']";
		*/
		public static final String BuildingNumberUpdated ="@xpath=//input[contains(@id,'emerBuildingNumber')]";
		public static final String StreetNameUpdated ="@xpath=//input[contains(@id,'emerStreet')]";
		public static final String CityOrTownUpdated ="@xpath=//input[contains(@id,'emerTown')]";
		public static final String PostalCodeUpdated ="@xpath=//input[contains(@id,'emerPostCode')]";
		
		
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		public static final String AddressValidationMsg ="@xpath=//h5[contains(text(),'Provided address has validated successfully')]";
		public static final String UpdateAddressBtn="@xpath=//button[contains(text(),'Update Address')]";
		public static final String SuccessMsg="@xpath=//*[contains(text(),'Thank you for your order. Your order is raised successfully.')]"; 

		public static final String FirstName="@xpath=//input[@id='emerFirstname']";
		public static final String LastName="@xpath=//input[@id='emerLastname']"; 
		public static final String Language="@xpath=//*[@name='emerLanguage']"; 
		public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'Customer')]";

		public static final String PostalCodeUpdatedFR ="@xpath=//select[@id='emerPostCodeUpdated']";
		public static final String CityOrTownUpdatedFR ="@xpath=//select[@id='emerTownUpdated']";
		public static final String DepartmentTypeUpdated ="@xpath=//select[@id='emerSerUpdDepartment']";
		public static final String SubcriberId="@xpath=//input[@id='companyRegNumber']";

		public static final String DirServiceUpdateOption="@xpath=//*[@id='actionModal']//*[text()='Directory Service Update']"; 
		public static final String DirServiceUpdatePage="@xpath=//*[contains(text(),'DSU Update')]"; 

		public static final String CustomerName1="@xpath=//input[@id='btName']"; 
		public static final String BuildingNumber="@xpath=//input[@id='bNumber']"; 
		public static final String StreetName="@xpath=//input[@id='acStreet']"; 
		public static final String CityOrTown="@xpath=//input[@id='acCitytown']"; 
		public static final String PostalCode="@xpath=//input[@id='acPostCode']"; 
		public static final String NIF="@xpath=//input[@id='nifUpdated']"; 
		public static final String SubLocality="@xpath=//input[@id='subLocality']"; 
		public static final String StreetNumber="@xpath=//input[@id='emerBuildingNumberUpdated']"; 

		public static final String Municipality="@xpath=//select[@id='municipalityUpdated']"; 
		public static final String StreetType="@xpath=//input[@id='streetType']"; 


		public static final String OrderTypeDrpDwn="@xpath=//select[@id='ostatusDD']"; 
		public static final String OrderTypeOption="@xpath=//option[contains(text(),'option')]"; 

		public static final String LineTypeDrpDwn="@xpath=//select[@id='lineTypeDD']"; 

		public static final String TraffDrpDwn="@xpath=//select[@id='tariffDD']"; 
		public static final String EntryTypeDrpDwn="@xpath=//select[@id='entryTypeDD']"; 
		public static final String TypeFaceDrpDwn="@xpath=//select[@id='typefaceDD']"; 
		public static final String ListingCategoryDrpDwn="@xpath=//select[@id='lCategoryDD']"; 
		public static final String listingTypeDrpDwn="@xpath=//select[@id='listingTypeDD']"; 
		public static final String PriorityDrpDwn="@xpath=//select[@id='priorityDD']"; 
		public static final String telphoneNoDrpDwn="@xpath=//input[@id='telphoneNo']"; 
		public static final String CompRegNumber="@xpath=//input[@id='acCompRegNum']"; 
		public static final String CompanyEmail="@xpath=//input[@id='acCompEmailAdd']"; 

		public static final String UpdateDSUBtn="@xpath=//button[contains(text(),'Update DSU')]"; 


		public static final String TxDetails_DSUoption="@xpath=//*[@class='actionLinks ml-4 ng-star-inserted']//*[text()='Directory Service Update']"; 
		public static final String TxDetails_AddUpdateOpt="@xpath=//*[@class='actionLinks ml-4 ng-star-inserted']//*[text()='Address Update']"; 
		public static final String CityOrTownUpdatedInp ="@xpath=//select[@id='emerTownUpdated']";

		public static final String telphoneNoAT="@xpath=//input[@id='telephoneNumber']"; 

		public static final String LanguageBE="@xpath=//select[@name='emerLanguageUpdated']";
		public static final String ListingOpt="@xpath=//select[@id='dirListOption']";
		public static final String AddressUpdate_SuccessMsg="@xpath=//*[contains(text(),'Thank')]";//KK Phase 2 
		public static final String LanguageBMU="@xpath=//*[@name='listingLanguage']";
		public static final String SecretListing="@xpath=//select[@id='servList']";

	}


	public static class TelephoneNoScreen_DSU
	{
		public static final String SkipBtn="@xpath=//button[contains(text(),'Skip')]"; 

		public static final String CustName="@xpath=//input[@id='emerBusinessNameUpdated']"; 
		public static final String VatNumber="@xpath=//input[@id='siretNumber']"; 
		public static final String StreetType="@xpath=//input[@id='streetType']"; 
		public static final String StreetName="@xpath=//input[@id='emerStreetUpdated']"; 
		public static final String HouseNumber="@xpath=//input[@id='emerBuildingNumberUpdated']"; 
		public static final String PostalCode="@xpath=//input[@id='emerPostCodeUpdated']"; 
		public static final String CityTown="@xpath=//input[@id='emerTownUpdated']"; 
		public static final String province="@xpath=//input[@id='province']"; 
		public static final String PremisesNumber="@xpath=//input[@id='bNumber']"; 
		public static final String AdvertisementFlag="@xpath=//select[@id='dsuAdvFlag']"; 
		public static final String DSUStreetType="@xpath=//input[@id='dsuStreetType']"; 
		public static final String DSUStreetName="@xpath=//input[@id='acStreet']"; 
		public static final String DSUPostalCode="@xpath=//input[@id='acPostCode']"; 
		public static final String DSUCityTown="@xpath=//input[@id='acCitytown']"; 
		public static final String DSUprovince="@xpath=//input[@id='acProvince']"; 
		public static final String DSUValidateAddressBtn="@xpath=(//button[contains(text(),'Validate Address')])[2]"; 
		public static final String ValidateAddressBtn="@xpath=(//button[contains(text(),'Validate Address')])[1]"; 

		public static final String dsuSection="@xpath=//p[contains(text(),'Directory Service Update')]"; 
		public static final String dsuUsageType="@xpath=//select[@id='dsuUsageType']"; 
		public static final String dsuConfRevSearch="@xpath=//select[@id='dsuConfRevSearch']"; 
		public static final String OrderReviewBtn="@xpath=//button[contains(text(),'Order Review')]"; 

		public static final String OrderReviewSection="@xpath=//p[contains(text(),'Order Review')]"; 
		public static final String SubmitButton="@xpath=//button[contains(text(),'Submit')]"; 

		public static final String OrderConfirmationMsg ="@xpath=//div[@id='addrUpdate-l-4']";
		public static final String SuccessMessage ="@xpath=//*[contains(text(),'Thank')]";	
	}









}