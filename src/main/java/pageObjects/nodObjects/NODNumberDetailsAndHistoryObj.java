package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODNumberDetailsAndHistoryObj {
	
	public static class NumberDetails
	{
		
		public static final String NumDetailsAndHis_DrpDwn="@xpath=//label[contains(text(),'Number Details & History')]"; 
		public static final String TypeNumberHereTxtFld="@xpath=//*[@formcontrolname='searchNumber']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group-append']//button[@type='submit']"; 
		public static final String Order="@xpath=(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"; 

		public static final String RangeStartColumn="@xpath=//div[contains(text(),'Range Start')]"; 
		public static final String RangeStartVal="@xpath=(//*[@class='tabulator-cell'])[1]"; 

		public static final String RangeEndColumn="@xpath=//div[contains(text(),'Range End')]"; 
		public static final String RangeEndVal="@xpath=(//*[@class='tabulator-cell'])[2]"; 

		public static final String AddressColumn="@xpath=//div[contains(text(),'Address')]"; 
		public static final String AddressVal="@xpath=(//*[@class='tabulator-cell'])[3]"; 

		public static final String StatusColumn="@xpath=//div[contains(text(),'Status')]"; 
		public static final String StatusVal="@xpath=(//*[@class='tabulator-cell'])[4]"; 

		public static final String NumberHistoryRdoBtn="@xpath=//label[contains(text(),'Number History')]"; 
		public static final String NumberHistory="@xpath=//div[@id='CompletedcollapseStepper']"; 
		public static final String NumberStatus="@xpath=//*[contains(@aria-controls,'collapseStepper')]"; 

		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}