package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODNumberDirectActivationObj {


	public static class NumDirActivation
	{ 
		public static final String TableRecordLoader="@xpath=//*[@class='ph-item tblLoader']"; 
		public static final String QuickLinkDrpDwn="@xpath=//div[contains(@class,'ng-select-container')]//div[contains(text(),'Quick Links')]"; 
		public static final String QuickLinkDrpDwnArw="@xpath=//body/app-root[1]/app-my-orders[1]/div[1]/div[1]/div[1]/div[2]/app-profile[1]/div[1]/form[1]/div[1]/ng-select[1]/div[1]/span[1]";
		public static final String ActivationLink ="@xpath=//label[contains(text(),'Activation')]";
		public static final String ActiationPage ="@xpath=//a//*[contains(text(),'Activation')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String HouseNumberSE ="@xpath=//input[@id='emerSerUpdStreetNumber']";
		public static final String CityOrTownFR ="@xpath=//select[@id='cityTownDirNA_FR']";
		public static final String PostalCodeFR ="@xpath=//select[@id='emerPostCode']";
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		public static final String AddressValidationMsg ="@xpath=//h5[contains(text(),'Provided address has validated successfully')]";
		public static final String SelectTypeAndQuantity ="@xpath=//button[contains(text(),'Select Type and Quantity')]";
		public static final String NumberTypeRadioBtn ="@xpath=//label[contains(text(),'NoType')]";
		public static final String ProvideNumbersBtn ="@xpath=//button[contains(text(),'Provide Numbers')]";
		public static final String NumberRangeChkBox ="@xpath=//label[@for='customCheckAll']";
		public static final String NumberAddOnsBtn ="@xpath=//button[contains(text(),'Number Add-ons')]";
		public static final String DirectoryServiceUpdateChkBox ="@xpath=//label[contains(text(),'Directory Service Update')]";
		public static final String OrderReview ="@xpath=//button[contains(text(),'Order Review')]";
		public static final String OrderConfirmationBtn ="@xpath=//button[contains(text(),'Order Confirmation')]";
		public static final String ActivationConfirmationMsg ="@xpath=//div[@id='activation-l-6']/div[@class='row ng-star-inserted']";
		public static final String SuccessMessage ="@xpath=//*[contains(text(),'Thank')]";
		public static final String EmailNotification ="@xpath=//*[contains(text(),'You will receive an email confirmation shortly at ')]";
		public static final String ActivationOrderId ="@xpath=//span[contains(text(),'Order Id : ')]/a[@class='link']";
		public static final String CheckMyOrder ="@xpath=//button[contains(text(),'Check My Order(s)')]";
		public static final String orderStatusVal="@xpath=(//*[@tabulator-field='currentStatus'])[2]";
		public static final String SearchTxtFld="@xpath=//*[@class='input-group']//*[@placeholder='Search']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group']//button[@type='button']";
		public static final String Language="@xpath=//*[@name='emerLanguage']"; 
		public static final String LanguageOption="@xpath=//option[contains(text(),'LANGUAGE')]"; 
		public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'Customer')]";
		public static final String DirectoryListingOption ="@xpath=//select[@id='dirListOption']";
		public static final String DepartmentType ="@xpath=//select[@id='emerSerUpdDepartment']";
		//public static final String LocalAreaCode ="@xpath=(//div[@class='ng-input']/input)[3]";
		public static final String Muncipality ="@xpath=//select[@name='municipality']";
		public static final String MuncipalityOption ="@xpath=//option[contains(text(),'Type')]";

		public static final String CustomerName ="@xpath=//input[@id='emerBusinessName']";
		public static final String BuildingNumber ="@xpath=//input[@id='emerBuildingNumber']";
		public static final String StreetName ="@xpath=//input[@id='emerStreet']";
		public static final String CityOrTown ="@xpath=//*[@id='emerTown']";
		public static final String PostalCode ="@xpath=//input[@id='emerPostCode']";
		public static final String BlockSize ="@xpath=//select[@id='blockSizeGeo']";
		public static final String QuantitySize ="@xpath=//select[@id='quantitySizeGeo']";
		public static final String FirstName="@xpath=//input[@id='emerFirstname']";
		public static final String LastName="@xpath=//input[@id='emerLastname']"; 
		public static final String orderType ="@xpath=(//*[@tabulator-field='transactionType'])[2]";
		public static final String LocalAreaCodeOption ="@xpath=//div[@class='ng-dropdown-panel-items scroll-host']//span[contains(text(),'option')]";

		public static final String SubresellerOCN="@xpath=//select[@id='subResellerOCN']";
		public static final String NIF="@xpath=//input[@id='nif']";
		public static final String StreetNumber="@xpath=//input[@id='emerSerUpdStreetNumber']"; //KK
		public static final String Municipality="@xpath=//select[@id='municipality']";
		public static final String LocalAreaCode ="@xpath=//ng-select[@id='areaCode']";
		public static final String LocalAreaCodeValue ="@xpath=//ng-dropdown-panel//*[text()='Value']";
		public static final String CustomerNameNL ="@xpath=//input[@name='emerBusinessName']";
		public static final String StreetNameNL ="@xpath=//input[@name='emerStreet']";
		public static final String BuildingNumberNL ="@xpath=//input[@name='emerBuildingNumber']";
		public static final String CityOrTownNL ="@xpath=//input[@name='emerTown']";
		public static final String PostalCodeNL ="@xpath=//input[@name='emerPostCode']";
		public static final String SubLocality="@xpath=//input[@id='subLocality']"; 
		public static final String PostalCodePT ="@xpath=//input[@name='emerPostCode']";
		public static final String NIFES="@xpath=//input[@id='emerCifNif']";
		public static final String StreetTypeES ="@xpath=//input[@id='emerSerUpdStreetType']";
		public static final String BuildingNumberES ="@xpath=//input[@id='emerSerUpdStreetNumber']";
		public static final String SubcriberId="@xpath=//input[@id='companyRegNumber']"; 
		public static final String EnrichmentMsg ="@xpath=//h5[contains(text(),'No enrichment available , Please continue.')]";
		public static final String EnrichmentMsgDE ="@xpath=//p[contains(text(),'There is no enrichment available.')]";

	}







































}