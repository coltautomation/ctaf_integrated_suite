package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODNumberReactivationObj {

	
	public static class Reactivate
	{ 
		public static final String ReactivateNumberBtn="@xpath=//button[text()='Reactivate Numbers']"; 
		public static final String Reactivateoption="@xpath=//*[@id='actionModal']//*[text()='Reactivate']"; 
		
	}
	
}