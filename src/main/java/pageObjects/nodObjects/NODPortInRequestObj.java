package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODPortInRequestObj {

	
	public static class portIn
	{
		 
		//public static final String customerTypeRadioBtn ="@xpath=//input[@id='Customer_Type']";
		public static final String QuickLinkDrpDwn="@xpath=//div[contains(@class,'ng-select-container')]//div[contains(text(),'Quick Links')]"; 
		public static final String QuickLinkDrpDwnArw="@xpath=//body/app-root[1]/app-my-orders[1]/div[1]/div[1]/div[1]/div[2]/app-profile[1]/div[1]/form[1]/div[1]/ng-select[1]/div[1]/span[1]";
		public static final String portInLink ="@xpath=//label[contains(text(),'Port-in')]";
		public static final String portInPage ="@xpath=//*[contains(text(),' Port-in ')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String CustomerName ="@xpath=//input[@id='customerName']";
		public static final String BuildingNumber ="@xpath=//input[@id='cadBuildingNumber']";
		public static final String StreetName ="@xpath=//input[@id='cadstreetAddress']";
		public static final String CityOrTown ="@xpath=//input[@id='cadcitytown']";
		public static final String PostalCode ="@xpath=//input[@id='cadpostCode']";
		public static final String NewBuildingNumber ="@xpath=//input[@id='newBuildingNumber']";
		public static final String NewStreetName ="@xpath=//input[@id='newstreetAddress']";
		public static final String NewCityOrTown ="@xpath=//input[@id='newcitytown']";
		public static final String NewPostalCode ="@xpath=//input[@id='newpostCode']";
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		public static final String AddressValidationMsg ="@xpath=//h5[contains(text(),'Provided address has validated successfully')]";
		public static final String ProvideNumbersBtn ="@xpath=//button[contains(text(),'Provide Numbers')]";
		public static final String NumberForPortIn ="@xpath=//textarea[@id='null']";
		public static final String ValidateNumbers ="@xpath=//button[contains(text(),'Validate Numbers')]";
		public static final String NumberEnrichmentBtn ="@xpath=//button[contains(text(),'Number Enrichment')]";
		public static final String MainBillingNumberChkBox ="@xpath=//label[@for='headerEnrichmentCheck']";
		public static final String MainBillingNumberChkBoxNL ="@xpath=//label[@for='keepCurrentSetting']";
		public static final String SingleAndMultiLine ="@xpath=//select[@id='singleOrMulti']";
		public static final String ProvidePortingDocBtn ="@xpath=//button[contains(text(),'Provide Porting Documents')]";
		public static final String MainBillingNo ="@xpath=//input[@name='mainBilling']";
		//public static final String ChooseFileBtn ="@xpath=//input[@id='portingFormBase64']";
		public static final String ChooseFileBtn ="//input[@id='portingFormBase64']";
		public static final String currentOperator ="@xpath=//select[@id='currentProvider']";
		public static final String currentOperatorOption ="@xpath=//option[contains(text(),'operator')]";
		public static final String ProvideContactDetailsBtn ="@xpath=//button[contains(text(),'Provide your contact Details')]";
		public static final String ReviewYourOrderBtn ="@xpath=//button[contains(text(),'Review your Order')]";
		public static final String ConfirmYourOrderBtn ="@xpath=//button[contains(text(),'Confirm your Order')]";
		public static final String PortInDate ="@xpath=//input[@id='requestPortDate']";
		public static final String PortInWindow ="@xpath=//ng-select[@id='requestPortTimeDD']//input[@role='combobox']";
		public static final String PortInConfirmationMsg ="@xpath=//div[@id='portIn-l-7']//div[@class='row ng-star-inserted'][1]";
	    public static final String SuccessMessage ="@xpath=//*[contains(text(),'Your order')]";
	    public static final String EmailNotification ="@xpath=//*[contains(text(),'You will receive an email confirmation shortly at ')]";	
	    public static final String portInOrderId ="@xpath=//*[contains(text(),'Order ID: ')]/a";	
		public static final String CheckMyOrder ="@xpath=//button[contains(text(),'Check My Order(s)')]";
		public static final String orderStatusVal="@xpath=(//*[@tabulator-field='currentStatus'])[2]";
		//public static final String customerTypeRadioBtn ="@xpath=//input[@id='CustomerType']";
		public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'Customer')]";
		//public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'residentialPortIn_NL')]";
		public static final String SearchTxtFld="@xpath=//*[@class='input-group']//*[@placeholder='Search']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group']//button[@type='button']"; 
	
		//sprint 3
		public static final String orderType ="@xpath=(//*[@tabulator-field='transactionType'])[2]";
		public static final String CityOrTownDE ="@xpath=//select[@id='cadcitytown']";
		public static final String CityOrTownOptionDE ="@xpath=//option[contains(text(),'option')]";
		public static final String subscriberID ="@xpath=//input[@id='subscriberID']";
	
		public static final String PhoneNum ="@xpath=//input[@id='landlinePhone']";
		public static final String Language ="@xpath=//select[@id='language']";
		public static final String CompanyRegNum ="@xpath=//input[@id='siretNumber']";
		public static final String VATNumber ="@xpath=//input[@id='siretNumber']";
		public static final String NIF ="@xpath=//input[@id='nif']";
		public static final String providedCVP ="@xpath=//input[@id='providedCVP']";
		public static final String CIF ="@xpath=//input[@id='cifNifNumber']";
		public static final String DirectoryListingOption ="@xpath=//*[@id='dirListOption']";
		
		public static final String OCN ="@xpath=//select[@id='subResellerOCN']";
		//public static final String OCNvalue ="xpath=(//*[@id='subResellerOCN']//option)[last()]";
		public static final String OCNvalue ="xpath=//*[@id='subResellerOCN']/option[2]";
		public static final String CalendarIcon ="xpath=//button[contains(@class,'calendar')]";
		public static final String Date ="xpath=(//*[@class='btn-light ng-star-inserted'])[1]";
		public static final String StreetType ="@xpath=//input[@id='cadStreetType']";
		public static final String Province ="@xpath=//input[@id='cadProvince']";
		public static final String ScreatCode ="@xpath=//input[@formcontrolname='secretCode']";
		public static final String PortInWindowOption ="@xpath=//ng-dropdown-panel//*[@role='option']";
		
		public static final String DirectoryListingOption1 ="@xpath=//*[@id='servList']"; 
	//	public static final String StreetType="@xpath=//input[@id='cadStreetType']"; 
		public static final String province="@xpath=//input[@id='acProvince']";  
		public static final String Municipality="@xpath=//select[@id='cadmunicipality']"; 
		public static final String SubcriberId="@xpath=//input[@id='subscriberID']";
		public static final String StreetNumber="@xpath=//input[@id='cadStreetNumber']"; 
		public static final String SubLocality="@xpath=//input[@id='cadSubLocality']"; 
		public static final String CityOrTownDK ="@xpath=//select[@id='cityTownDirNA_DK']"; //9/9
	//	public static final String CityOrTownDK ="@xpath=//select[@id='cadcitytown']";
		public static final String AddressValidationMessage ="@xpath=//*[contains(text(),'Address successfully validated')]";
		public static final String MunicipalityCH="@xpath=//select[@id='municipality']"; 
	
	}
	
	public static class QuickNote
	{ 
		public static final String OrderRow="@xpath=//*[@class='tabulator-row tabulator-selectable tabulator-row-odd']"; 
	//	public static final String ActionOption="@xpath=(//div[@tabulator-field='actions'])[2]"; 
		public static final String ActionOption="@xpath=(//div[@tabulator-field='actions'])[2]//a"; 
		public static final String QuickNoteAction="@xpath=//li[contains(text(),'QuickNote')]"; 
	//	public static final String AddNotePopUp="@xpath=//h4[contains(text(),'Add Notes')]"; 
		public static final String AddNotePopUp="@xpath=//*[contains(text(),'Add Notes')]"; 

		
		public static final String AddNoteTxtBox="@xpath=//textarea[@id='comment']"; 
		public static final String AddNoteBtn="@xpath=//button[contains(text(),'Add Notes')]"; 

	//	public static final String SuccessMsg="@xpath=//h6[contains(text(),'Thank you for your order. Your order is raised suc')]"; 
		public static final String SuccessMsg="@xpath=//*[contains(text(),'Thank')]";
		public static final String SuccessMsg1="@xpath=//*[contains(text(),'Thank You for your order')]";
		//public static final String OrderId="@xpath=//*[contains(text(),'Thank you for your order')]/parent::div//p//a"; 
		public static final String OrderId="@xpath=//*[contains(text(),'Order ID')]//a"; 
		
		public static final String OrderId1="@xpath=//*[contains(text(),'Order ID')]//a"; 

		
	}
	
	public static class ModifyPort
	{ 
		public static final String ModifyPortAction="@xpath=//li[contains(text(),'Modify Port')]"; 
		public static final String ModifyPortScreen="@xpath=//h4[contains(text(),'Modify Port')]"; 
		public static final String ModifyPortBtn="@xpath=//option[text()='Modify Port']"; 



		//public static final String Calendarbtn="@xpath=//*[@id='portDate']/parent::div//button"; 
		public static final String Month="@xpath=//select[@title='Select month']";
		public static final String Year="@xpath=//select[@title='Select year']";
		public static final String Day="@xpath=(//ngb-datepicker-month//*[text()='day'])[1]";

	//	public static final String PortInTime="@xpath=//ng-select[@id='portwindow']";
		//public static final String PortInTime="@xpath=(//div[@class='ng-input'])[3]";
		public static final String Time="@xpath=//ng-dropdown-panel//*[text()='Time']";

		public static final String ModifyPort="@xpath=//button[contains(text(),'Modify Port')]";
		public static final String Calendarbtn="@xpath=//*[@id='requestPortDate']/parent::div//button";
		public static final String PortInTime="@xpath=//ng-select[@id='requestPortTimeDD']";
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}