package pageObjects.siebelGalileoObjects;

public class CustomerTimeJourneyObj {

	public static class CustomerDelay
	 { 
		public static final String customerDelayTab="@xpath=//a[text()='Customer Delays']";
		public static final String coltDelayTab="@xpath=//a[text()='Colt Delays']";
		public static final String custDelayNewButton="@xpath=//button[@title='Customer Delays:New']";
		public static final String coltDelayNewButton="@xpath=//button[@title='Colt Delays:New']";
		public static final String delayReason="@xpath=//input[contains(@aria-label,'Delay Reason')]";
		public static final String searchDelayOwner="@xpath=//input[contains(@aria-label,'Delay Owner')]//following-sibling::*";
		public static final String addOwner="@xpath=//button[@aria-label='Pick Employee:OK']";
		public static final String searchGroup="@xpath=//input[contains(@aria-label,'Group')]//following-sibling::*";
		public static final String addGroup="@xpath=//button[@aria-label='Pick Group:OK']";
		public static final String delayLocation="@xpath=//input[contains(@aria-label,'Delay Location')]";
		public static final String delay="@xpath=//input[contains(@aria-label,'Group')]//following-sibling::*";
		public static final String expResolvDate="@xpath=//input[contains(@aria-label,'Expected Resolve Date')]/following-sibling::*[contains(@class,'siebui-icon-date')]";
		public static final String nextCustUpdate="@xpath=//input[contains(@aria-label,'Next Customer Update')]/following-sibling::*[contains(@class,'siebui-icon-date')]";
		public static final String diaryNote="@xpath=//input[contains(@aria-label,'Diary')]//following-sibling::*";
		public static final String searchDiaryNote="@xpath=//input[contains(@aria-label,'First Diary Note')]//following-sibling::*";
		public static final String diaryNew="@xpath=//span/button[@aria-label='Diary:New']";
		public static final String noteArea="@xpath=//td[contains(@id,'Note')]";
		public static final String noteValue="@xpath=//td/input[contains(@aria-labelledby,'Note')]";
		public static final String addNote="@xpath=//button[@aria-label='Diary:OK']";
		public static final String saveDelay="@xpath=//span[text()='Save']";
		public static final String cancelDelay="@xpath=//span[text()='Cancel']";
		public static final String addWorkitem="@xpath=//button[text()='Add Work Item(s)']";
		public static final String removeWorkitem="@xpath=//button[text()='Remove']";
		public static final String popupText="@xpath=//div[contains(@id,'_popup')]";
		public static final String wiAddedCheckbox="@xpath=//td[contains(@id,'COLT_WI_Added')]//input";
		public static final String wiSchldCheckbox="@xpath=//td[contains(@id,'COLT_WI_Schedule')]//input";
		public static final String okWorkitem="@xpath=//button[@aria-label='Add Work Items:OK']";
		public static final String workItemDownArrow="@xpath=//td[contains(@class,'downCaret')]";
		public static final String customerDelayTable="@xpath=//table[@summary='Customer Delays']//tr[@tabindex='-1']";
		public static final String delayRowCount="@xpath=//table[@summary='Customer Delays']/tbody/tr/following-sibling::tr";
		public static final String delayStatus="@xpath=//table[@summary='Customer Delays']//td[contains(@id,'Status')][1]";
		public static final String editDelay="@xpath=//button[@title='Customer Delays:Edit']";
		public static final String updateStatus="@xpath=//input[contains(@aria-label,'Delay Reason')]//parent::*//parent::*//following-sibling::*//input[@aria-label='Status']";
		public static final String status="@xpath=//input[@title='Open']";
		public static final String statusPopUp="@xpath=//span[text()='OK']";
		public static final String delaySettings="@xpath=//button[@title='Customer Delays Menu']";
		public static final String calcStatus="//table[@summary='Customer Delays']//td[contains(@id,'Status')][1]";
		public static final String auditTrailSetting="@xpath=//button[@title='Customer Delay Audit Trail Menu']";
		public static final String columnDisplayedTrail="@xpath=//button[@title='Customer Delay Audit Trail Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String customerAuditTrail="@xpath=//div[text()='Customer Delay Audit Trail']"; 
		public static final String cancelAuditTrail="@xpath=//div[@title='Columns Displayed Form Applet']//span[text()='Cancel']";
		public static final String cockpitDelayReasonLink="//table[@summary='Delays']//tr[@id=1]//td[contains(@id,'Task_Delay_Reason')]";
		public static final String ordernumberlink="@xpath=//table[@summary='Delays']//tr[@id=1]//td[contains(@id,'_Order_Number')]";
		public static final String returnWiSchedOn="@xpath=//div[text()='Return Sched WIs On']";
		public static final String auditTrailField="@xpath=//tr[@id=1]//td[contains(@id,'Field')]";
		public static final String diaryTable="@xpath=//table[@summary='Diary']";
//		public static final String diaryNoteValue="//table[@summary='Diary']//td[contains(@id,'Note')]";
		public static final String diaryNoteValue="@xpath=//td[@title='Internal']//following-sibling::td[contains(@id,'Note')]";
		public static final String newNotearea="@xpath=//div[@name='popup']//tr[@id=1]//td[@title='Internal']//following-sibling::td[contains(@id,'Note')]";
		public static final String diaryTab="@xpath=//a[text()='Diary']";
		public static final String diaryTabSelectArrow="@xpath=//select[@aria-label='Third Level View Bar']";
		public static final String orderDiaryNote="@xpath=//table[@summary='Diary']//td[contains(@title,'Customer')]";
		public static final String newNoteValue="@xpath=//table[@summary='Diary']//td[contains(@id,2)][3]";
		
		
		//Anusuya
		
		public static final String coltdelayStandbyFld="@xpath=//input[contains(@aria-labelledby,'Standby') and @title='N']";
		public static final String coltdelayReason="@xpath=//input[@aria-label='Reason']";
		public static final String coltDelayOwner="@xpath=//input[@aria-label='Owner']//following-sibling::*";
		public static final String coltdelayGroupFld="@xpath=//input[@aria-label='Group']/parent::div/span";
		public static final String issueTaskName="@xpath=//input[@aria-labelledby='Category_Label']";		
		public static final String addcoltdelayGroup="@xpath=//button[@aria-label='Pick Group:OK']";
		public static final String ERDAlert="@xpath=//div[text()='Please enter the value for Expected Resolved Date(SBL-EXL-00151)']";
		public static final String DelayOwnerAlert="@xpath=//div[text()='Please set the Delay Owner to the current logged in user(SBL-EXL-00151)']";
		public static final String alertOK="@xpath=//button[text()='Ok']";
		public static final String SearchOwnerusingMail="@xpath=//input[@aria-label='PopupQueryCombobox']";
		public static final String searchValue = "@xpath=//input[@aria-label='Query Search Specification']";
		public static final String searchButton = "@xpath=//input[@aria-label='Query Search Specification']/parent::span/following-sibling::span/button[@title='Pick Employee:Go']";
		public static final String cancelOwner="@xpath=//button[@aria-label='Pick Employee:OK']/parent::span/following-sibling::span/button[@aria-label='Pick Employee:Cancel']";
		public static final String coltdelayStatus="@xpath=//table[@summary='Colt Delays']//tr[@id][RowNo]//td[contains(@id,'Status')]";
		public static final String customerdelayStatus="@xpath=//table[@summary='Customer Delays']//tr[@id][RowNo]//td[contains(@id,'Status')]";
		public static final String workItemsTab="@xpath=//a[text()='Work Items']";
		public static final String workflowsTab="@xpath=//a[text()='Workflows']";
		
		public static final String coltDelaySettings = "@xpath=//button[contains(@title,'Delays Menu')]";
		public static final String coltDelay_AdvancedSort = "@xpath=//li/a[contains(text(),'Advanced Sort ')]";
		public static final String change_WorkflowBtn = "@xpath=//button[contains(@title,'Change Workflow')]";
		public static final String category_selection_popup = "@xpath=//span[contains(text(),'reason for workflow')]";
		public static final String AEnd_Reason = "@xpath=//span[contains(@class,'sifcat-Aend-reason')]";
		public static final String BEnd_Reason = "@xpath=//span[contains(@class,'sifcat-Bend-reason')]";
		public static final String ProceedBtn = "@xpath=//button[text()='Proceed']";
		public static final String changeOption_popup = "@xpath=//span[contains(text(),'change option')]";
		public static final String editColtDelay="@xpath=//button[@title='Colt Delays:Edit']";
		public static final String editDelayStatus="@xpath=//input[@title='Open']//parent::div//span";
		
		public static final String leadTimeCalc="@xpath=//input[@aria-label='Additional Leadtime (working days)']//parent::div//span";
		public static final String leadTimeCalcVal="@xpath=//button[@title='Use the current value.']";
		public static final String customerContact="@xpath=//input[@aria-label='Customer Contact']";
		public static final String cpdRescheduleWindow="@xpath=//div[@aria-describedby='DateDialogContainer']";
		public static final String icdText="@xpath=//input[@aria-label='Indicative Completion Date' and @title]";
		public static final String cpdText="@xpath=//input[@aria-label='Colt Promised Date' and @title]";
		public static final String DateDialog ="@xpath=//div[@id='DateDialogContainer']";
		public static final String keepExistingDate ="@xpath=//button[text()='Keep existing date(s)']";
		public static final String cancel ="@xpath=//button[@id='cpd-reschedule-cancel-btn']";
		public static final String takethisdate ="@xpath=//button[text()='Take this date']";
		
		public static final String auditTrailFields="@xpath=//tr[@id]//td[contains(@id,'Field')]";
	 }
		
	public static class LongCRD
	{
		public static final String workflowTab="@xpath=//a[text()='Workflows']";
		public static final String deliveryLineitem="@xpath=//table[@summary='Workflow']//td[text()='Delivery']";
		public static final String longCRDButton="@xpath=//button[@title='Workflow:LONGCRD']";
		public static final String returnDatePick="@xpath=//input[@name='non-strd-ld-time']/following-sibling::*[contains(@class,'siebui-icon-date')]";
		public static final String dateSelection="@xpath=//a[text()='Value']";
		public static final String dateSave="@xpath=//button[text()='Save']";
		public static final String presentCRD="@xpath=//div[label=' Present CRD: ']/label[2]";
		public static final String longCRDLabel="@xpath=//div[@class='colt-longer-crd-txt']";
		public static final String returnDate="@xpath=//div[text()='Return Date']";
		public static final String auditTrailTab="@xpath=//a[text()='Audit Trail']";
		public static final String auditTrailField="@xpath=//table[@summary='Service Order']//tr[@id=1]//td[contains(@id,'Field')]";
		
//		260222 changes
		public static final String workFlowStatus="@xpath=(//table[@summary='Workflow']/..//td[contains(@title,'Long CRD')])[3]";
		public static final String orderDate="@xpath=//a[text()='Order Dates']";
		public static final String crdDate="@xpath=(//input[@aria-label='Customer Requested Date']/following-sibling::*[contains(@class,'siebui-icon-date')])[2]";
		public static final String doneButton="@xpath=//a[text()='Done']";
		public static final String crdSave="@xpath=//button//span[text()='Save']";
		public static final String oldCRD="@xpath=//input[@aria-label='Old CRD Date']";
		public static final String newCRD="@xpath=//div[@id='RDatetext']";
	 }
	
}
