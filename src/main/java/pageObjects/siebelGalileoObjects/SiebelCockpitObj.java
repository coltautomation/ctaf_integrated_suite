package pageObjects.siebelGalileoObjects;

public class SiebelCockpitObj {

	public static class ServiceOrderCockpit
	 { 
		public static final String cockpitTab="@xpath=//a[text()='Service Orders Cockpit']";
		public static final String allOrdersOption="@xpath=//select[@id='viewfilter']";
		public static final String jeopardyAppletHeader="@xpath=//div[text()='Jeopardy']";
		public static final String cpdicdDueShortlyAppletHeader="@xpath=//div[text()='CPD/ICD Due Shortly']";
		public static final String cpdicdNotSetAppletHeader="@xpath=//div[text()='CPD / ICD not set']";
		public static final String delaysAppletHeader="@xpath=//div[text()='Delays']";
		public static final String activitiesAppletHeader="@xpath=//div[text()='Activities - Action Required']";
		public static final String priorityOrdersAppletHeader="@xpath=(//div[text()='Priority Orders'])[1]";
		public static final String serviceOrderAppletHeader="@xpath=//form[@name='SWEForm4_0']//div[text()='Service Order']";
		public static final String cpdicdDueShortlyColumns="@xpath=//div[text()='CPD/ICD Due Shortly']/parent::div/following-sibling::div//tr[@role='rowheader']/th/div[@class='ui-jqgrid-sortable']";
		public static final String cpdicdNotSetColumns="@xpath=//div[text()='CPD / ICD not set']/parent::div/following-sibling::div//tr[@role='rowheader']/th/div[@class='ui-jqgrid-sortable']";
		public static final String delaysColumns="@xpath=//div[text()='Delays']/parent::div/following-sibling::div//tr[@role='rowheader']/th/div[@class='ui-jqgrid-sortable']";
		public static final String activitiesColumns="@xpath=//div[text()='Activities - Action Required']/parent::div/following-sibling::div//tr[@role='rowheader']/th/div[@class='ui-jqgrid-sortable']";
		public static final String priorityOrdersColumns="@xpath=//div[text()='Priority Orders']/parent::div/following-sibling::div//tr[@role='rowheader']/th/div[@class='ui-jqgrid-sortable']";
		public static final String serviceOrderColumns="@xpath=//div[text()='Service Order']/parent::div/following-sibling::div//tr[@role='rowheader']/th/div[@class='ui-jqgrid-sortable']";
		public static final String cpdicdDueShortlySettingsMenu="@xpath=//button[@title='CPD/ICD Due Shortly Menu']";
		public static final String cpdicdDueShortlyColumnsDisplayedButton="@xpath=//button[@title='CPD/ICD Due Shortly Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String SelectedColumns="@xpath=//select[@title='Selected Columns']";
		public static final String SelectedColumnsA="@xpath=//select[@aria-label='Selected Columns']/option[@value='ColumnName']";
		public static final String cpdicdNotSetSettingsMenu="@xpath=//button[@title='CPD / ICD not set Menu']";
		public static final String delaysSettingsMenu="@xpath=//button[@title='Delays Menu']";
		public static final String activitiesSettingsMenu="@xpath=//button[@title='Activities - Action Required Menu']";
		public static final String priorityOrdersSettingsMenu="@xpath=//button[@title='Priority Orders Menu']";
		public static final String serviceOrderSettingsMenu="@xpath=//form[@name='SWEForm4_0']//div[text()='Service Order']/parent::div//button[@title='Service Order Menu']";
		public static final String cancelButton="@xpath=//button[@type='button']/span[text()='Cancel']";
		public static final String cpdicdNotSetColumnsDisplayedButton="@xpath=//button[@title='CPD / ICD not set Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String delaysColumnsDisplayedButton="@xpath=//button[@title='Delays Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String activitiesColumnsDisplayedButton="@xpath=//button[@title='Activities - Action Required Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String priorityOrdersColumnsDisplayedButton="@xpath=//button[@title='Priority Orders Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String serviceOrderColumnsDisplayedButton="@xpath=//button[@title='Service Order Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String serviceOrderStatus="@xpath=//div[text()='Service Order']/ancestor::form[@name='SWEForm4_0']//td[contains(@id,'Order_Status')]";
//		public static final String serviceOrderRef="@xpath=//table[@id='s_4_l']//td[contains(@id,'Order_Number')]";
		public static final String serviceOrderRefInputField="@xpath=//div[@title='CockpitSection List Applet']//td[contains(@id,'Order_Number')]/input";
		public static final String serviceOrderNextRecord="@xpath=//div[text()='Service Order']/ancestor::form[@name='SWEForm4_0']//td/span[@title='Next record set']";
		public static final String serviceOrderNewQuery="@xpath=//div[text()='Service Order']/parent::div//ul[@role='menu']//li/a[contains(text(),'New Query')]";
		
		public static final String serviceOrderCockpitTab="@xpath=//a[text()='Service Orders Cockpit']";
		public static final String AdvancedFilter="@xpath=//span[text()='Advanced Filter']"; 
		public static final String CockpitSettings="@xpath=//div[@title='CockpitSectionName List Applet']//button[@title='CockpitSectionName Menu']";
		//Advance Sorting
		public static final String AdvancedSort="@xpath=//button[@title='CockpitSectionName Menu']//parent::span//following-sibling::ul/li/a[contains(text(),'Advanced Sort')]";
		public static final String AdvancedSortHeader="@xpath=//span[text()='Sort Order']";
		public static final String SortByField1="@xpath=//input[@aria-label='Sort By Field 1']";
		public static final String SortByField2="@xpath=//input[@aria-label='Sort By Field 2']";
		public static final String SortByField3="@xpath=//input[@aria-label='Sort By Field 3']";
		public static final String AscendingOrder1="@xpath=//input[@aria-label='Sort By Field 1']//parent::td//following-sibling::td//input[@value='ascending']";
		public static final String DescendingOrder1="@xpath=//input[@aria-label='Sort By Field 1']//parent::td//following-sibling::td//input[@value='descending']";
		public static final String AscendingOrder2="@xpath=//input[@aria-label='Sort By Field 2']//parent::td//following-sibling::td//input[@value='ascending']";
		public static final String DescendingOrder2="@xpath=//input[@aria-label='Sort By Field 2']//parent::td//following-sibling::td//input[@value='descending']";
		public static final String AscendingOrder3="@xpath=//input[@aria-label='Sort By Field 3']//parent::td//following-sibling::td//input[@value='ascending']";
		public static final String DescendingOrder3="@xpath=//input[@aria-label='Sort By Field 3']//parent::td//following-sibling::td//input[@value='descending']";
		public static final String OkButton="@xpath=//span[text()='OK']";		
		public static final String AscendingSort="@xpath=//div[text()='CockpitAppletSection']//parent::div//following-sibling::div//div[text()='ColumnName']//span[@sort='asc']";
		public static final String DescendingSort="@xpath=//div[text()='CockpitAppletSection']//parent::div//following-sibling::div//div[text()='ColumnName']//span[@sort='desc']";
		
		//Columns Displayed
		public static final String ColumnsDisplayed="@xpath=//button[@title='CockpitSectionName Menu']//parent::span//following-sibling::ul/li/a[contains(text(),'Columns Displayed')]";
		public static final String ColumnsDisplayedHeader="@xpath=//span[text()='Columns Displayed']";
		public static final String SelectedColumn="@xpath=//select[@aria-label='Selected Columns']/option[@value='ColumnName']";
		public static final String AvailableColumns="@xpath=//select[@aria-label='Available Columns']/option[@value='ColumnName']";
		public static final String RemoveColumn="@xpath=//a[@aria-label='Columns Displayed:HideItem']";
		public static final String AddColumn="@xpath=//a[@aria-label='Columns Displayed:ShowItem']";
		public static final String AddAllColumns="@xpath=//a[@aria-label='Columns Displayed:ShowAllItems']";
		public static final String RemoveAllColumns="@xpath=//a[@aria-label='Columns Displayed:HideAllItems']";
		
		public static final String MoveColumnUp="@xpath=//a[@aria-label='Columns Displayed:MoveItemUp']";
		public static final String MoveColumnDown="@xpath=//a[@aria-label='Columns Displayed:MoveItemDown']";
		public static final String MoveColumnTop="@xpath=//a[@aria-label='Columns Displayed:MoveItemTop']";
		public static final String MoveColumnBottom="@xpath=//a[@aria-label='Columns Displayed:MoveItemBottom']";
		public static final String SaveColumnSettings="@xpath=//button[contains(@title,':Save')]";
		public static final String ResetColumnSettings="@xpath=//span[text()='Reset Defaults']";
		public static final String CancelColumnSettings="@xpath=//span[text()='Cancel']";
		
		//Expand/Collapse Applet
		public static final String ExpandApplet="@xpath=//div[contains(@title,'CockpitSectionName List')]//span[@title=':Show more']";
		public static final String CollapseApplet="@xpath=//div[contains(@title,'CockpitSectionName List')]//span[@title=':Show less']";
		public static final String ExpandAppletHeader="@xpath=//div[text()='CockpitSectionName')]";
		public static final String AppletColumns="@xpath=//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]";
		
		//Export Values
		public static final String ExportOption="@xpath=//button[@title='CockpitSectionName Menu']//parent::span//following-sibling::ul/li/a[contains(text(),'Export')]";
		public static final String ExportHeader="@xpath=//span[text()='Header']";
		public static final String ExportAllRows="@xpath=//span[text()='Rows to Export']//parent::td//following-sibling::td//input[@value='All Rows In Current Query']";
		public static final String ExportCurrentRow="@xpath=//span[text()='Rows to Export']//parent::td//following-sibling::td//input[@value='Only Current Row']";
		public static final String ExportAllColumns="@xpath=//span[text()='Columns To Export']//parent::td//following-sibling::td//input[@value='All']";
		public static final String ExportVisibleColumns="@xpath=//span[text()='Columns To Export']//parent::td//following-sibling::td//input[@value='Visible Columns']";
		public static final String Output_TabDelimitedText="@xpath=//span[text()='Output Format']//parent::td//following-sibling::td//input[@value='Tab Delimited Text File']";
		public static final String Output_CSV="@xpath=//span[text()='Output Format']//parent::td//following-sibling::td//input[@value='Comma Separated Text File']";
		public static final String Output_HTML="@xpath=//span[text()='Output Format']//parent::td//following-sibling::td//input[@value='HTML']";
		public static final String Output_TextDelimiter="@xpath=//span[text()='Output Format']//parent::td//following-sibling::td//input[@value='Text File With Delimiter:']";
		public static final String NextButton="@xpath=//span[text()='Next']";
		public static final String CloseButton="@xpath=//button/span[text()='Close']";
		public static final String ExportSuccess="@xpath=//span[contains(text(),'Export completed successfully')";

		public static final String advanceSearch="@xpath=//span[contains(text(),'Advanced Filter')]";
		public static final String primaryCountryA="@xpath=//input[@aria-label='Primary Delivery Team Country A']";
		public static final String primaryCountryB="@xpath=//input[@aria-label='Primary Delivery Team Country B']";
		public static final String primaryCityA="@xpath=//input[@aria-label='Primary Delivery Team City A']";
		public static final String primaryCityB="@xpath=//input[@aria-label='Primary Delivery Team City B']";
		public static final String okButton="@xpath=//span[contains(text(),'OK')]";
		public static final String countryFilter="@xpath=//table[@summary='Service Order']//td[contains(@id,'Installation_Country')]";
		
		
		
		public static final String maximizeBtn="@xpath=//*[contains(@title,':Show more')]";
		public static final String minimizeBtn="@xpath=//*[contains(@title,':Show less')]";
		
		public static final String filteredTitle="@xpath=//*[@class='filter-status']";
		
		public static final String jeopardyMenu="@xpath=//div[text()='Jeopardy']";
		public static final String jeopardyDropdown="@xpath=//select[@id='serviceordersmenu']";
		
		public static final String serviceOrderRecordCountButton="@xpath=//button[@title='Service Order Menu']/following-sibling::ul//li[contains(@data-caption,'Record Count')]";
		public static final String dialogbox="@xpath=//div[@id='colt-cockpit-custom-overlay-container']/following-sibling::div[@role='dialog']";
		public static final String recordCount="@xpath=//td[@class='scField']//div[@aria-label='Record Count']";
		public static final String recordCount_OKbutton="@xpath=//button[@title='Record Count:OK']";
		public static final String advanceSearch_allOrders="@xpath=//div[@title='Advanced Search Form Applet']//select[@id='colt-cockpit-popup-filter']";
		public static final String advancedSearchOKButton="@xpath=//button[@title='Advanced Search:OK']";
		public static final String cpdicdDueShortly_filterDropdown="@xpath=//div[text()='CPD/ICD Due Shortly']/following-sibling::select";
		public static final String cpdicdDueShortly_cpdValue="@xpath=//table[@summary='CPD/ICD Due Shortly']//tr//td[contains(@id,'s_1_l_CPD')]";
		public static final String cpdicdDueShortly_OrderNumber="@xpath=//table[@summary='CPD/ICD Due Shortly']//tr//td[contains(@id,'s_1_l_Order_Number')]/a";
		public static final String cpdicdDueShortly_NextArrow="@xpath=//div[text()='CPD/ICD Due Shortly']/ancestor::form[contains(@name,'SWEForm')]//td/span[@title='Next record set']";
		public static final String cpdicdDueShortly_icdValue="@xpath=//table[@summary='CPD/ICD Due Shortly']//tr//td[contains(@id,'s_1_l_ICD')]";
		public static final String cpdicdDueShortly_rowCounter="@xpath=//div[text()='CPD/ICD Due Shortly']/following-sibling::div/span[@id='s_1_rc']";
	 
	 
		//Jeopardy Section Milestone fields Verification
		public static final String serviceOrderfield="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Number')]";
		public static final String serviceOrderText="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Number')]/input";
		public static final String newQueryMenu="@xpath=//button[@title='CockpitSectionName Menu']//parent::span//following-sibling::ul/li/a[contains(text(),'New Query')]";
		public static final String orderStatusColumn="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Status')]";

		public static final String jeopardyStageValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'ColumnName')]//img[contains(@alt,'imagename')]";
		public static final String cpdValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Activate_Milestone')]";
		public static final String DaysToCPD="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Days_to_CPD')]";
		public static final String jeopardyColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'ColumnName')]";

		//Jeopardy MoreInfo XTRACT columns verification
		public static final String moreInfo="@xpath=//button[contains(@title,'More Info')]/span";
		public static final String workItemsHeader="@xpath=//span[text()='Work Items']";
		public static final String workItemsColumnscount="@xpath=//div[@title='Work Items List Applet']//table//th//div[text()]";
	 
		
		public static final String exclude="@xpath=//*[contains(@aria-label,'Orders with Customer Delays')]";
		public static final String maxButton="@xpath=//div[contains(text(),'Service Order')]/..//span[contains(@title,':Show more')]";
		public static final String customerDelay="@xpath=//table[@summary='Service Order']//td[contains(@id,'COLT_Customer_Delay')]";
		
		public static final String orderType="@xpath=//input[contains(@aria-label,'Order Type')]";
		public static final String productName="@xpath=//input[contains(@aria-label,'Product')]";
		public static final String orderType_max="@xpath=//table[@summary='Service Order']//td[contains(@id,'COLT_Order_Type_Subtype')]";
		public static final String productName_max="@xpath=//table[@summary='Service Order']//td[contains(@id,'Colt_Product')]";
		
		public static final String delayDropdown="@xpath=//select[@id='delaysparentmenu']";
		public static final String statusDetails="@xpath=//div[(text()='Delays')]/..//span[@class='siebui-row-counter']";
		public static final String delayMenu="@xpath=//div[text()='Delays']";
		
		public static final String excludeColtDelay="@xpath=//*[contains(@aria-label,'Orders with Colt Delays')]";
		public static final String delayMenuDropdown="@xpath=//select[@id='delaysmenu']";
		
		public static final String delayAppletMax="@xpath=//div[(text()='Delays')]/..//span[@title=':Show more']";
		
		public static final String date="@xpath=//td[contains(@id,'Expected_Resolved_Date')]";
		
		public static final String excludeICD="@xpath=//input[contains(@aria-label,'Orders with no ICD')]";
		public static final String icdMenuDropdown="@xpath=//select[contains(@id,'icdmenu')]";
		public static final String icdMax="@xpath=//div[(text()='CPD/ICD Due Shortly')]/..//span[@title=':Show more']";
		public static final String icdValues="@xpath=//table[@summary='CPD/ICD Due Shortly']//td[contains(@id,'ICD')]";
		
		public static final String excludeCPD="@xpath=//input[contains(@aria-label,'Orders with no CPD')]";
		public static final String cpdValues="@xpath=//table[@summary='CPD/ICD Due Shortly']//td[contains(@id,'CPD')]";
		
		public static final String legendOption="@xpath=//button[contains(text(),'Legend')]";
		public static final String jeopardyImage="@xpath=//img[@src='images/custom/JeopardyLegend.JPG']";	
		public static final String jeopardyMax="@xpath=//div[contains(text(),'Jeopardy')]/..//span[@title=':Show more']";
		
		public static final String delay_type="@xpath=//option[contains(@value,'Customer Delays')]";
		public static final String jeopardyCPD="@xpath=//table[@summary='Jeopardy']//td[contains(@id,'Activate_Milestone')]";
		
		public static final String productDropdownClick="@xpath=//input[@aria-label='Product']/following-sibling::span";
		public static final String priorityDropdownClick="@xpath=//input[@aria-label='Priority']/following-sibling::span";
//		public static final String Jeopardy_serviceOrderRef="@xpath=//table[@id='s_5_l']//td[contains(@id,'Order_Number')]";
		public static final String JeopardySettingsMenu="@xpath=//button[@title='Jeopardy Menu']";
		public static final String Jeopardy_serviceOrderRefInputField="@xpath=//div[@title='CockpitSection List Applet']//td[contains(@id,'Order_Number')]/input";
		public static final String Jeopardy_newQueryOption="@xpath=//div[text()='CockpitSection']/parent::div//ul[@role='menu']//li/a[contains(text(),'New Query')]";
		public static final String Jeopardy_ProductName="@xpath=//div[text()='CockpitSection']/ancestor::form[@name='SWEForm5_0']//td[contains(@id,'Product')]";
		public static final String Jeopardy_Priority="@xpath=//div[text()='CockpitSection']/ancestor::form[@name='SWEForm5_0']//td[contains(@id,'Priority')]";
		public static final String resetButton="@xpath=//span[contains(text(),'Reset')]";
//		public static final String cpdicdNotSet_serviceOrderRef="@xpath=//table[@id='s_12_l']//td[contains(@id,'Order_Number')]";
		public static final String cpdicdNotSet_filterDropdown="@xpath=//div[text()='CPD / ICD not set']/following-sibling::select";
		public static final String newQueryOption="@xpath=//div[text()='CockpitSection']/parent::div//ul[@role='menu']//li/a[contains(text(),'New Query')]";
		public static final String cpdicdNotSet_OverallConnectionType="@xpath=//div[text()='CPD / ICD not set']/ancestor::form[@name='SWEForm12_0']//td[contains(@id,'Overall_Connection_Type')]";
		
		public static final String activities_filterDropdown="@xpath=//div[text()='CockpitSection']/following-sibling::select";
		public static final String activities_ActivityTypeValue="@xpath=//table[@summary='CockpitSection']//tr/td[contains(@id,'Activtiy_Type')]/a";
		public static final String activities_OrderNumber="@xpath=//table[@summary='CockpitSection']//tr//td[contains(@id,'s_8_l_Order_Number')]/a";
		public static final String activities_serviceOrderRefInputField="@xpath=//div[@title='CockpitSection List Applet']//td[contains(@id,'Order_Number')]/input";
		public static final String activitiesTab= "@xpath=//li[@role='tab']/a[text()='Activities']";
		public static final String statusDropdownClick="@xpath=(//input[@aria-label='Status']/following-sibling::span)[1]";
		
		public static final String activitiesTabPopup="@xpath=//div[@role='dialog'][contains(@aria-describedby,'popup')]";
		public static final String PopupOkButton="@xpath=//button[@type='button'][text()='Ok']";
		public static final String orderDatesTab="@xpath=//li[@role='tab']/a[text()='Order Dates']";
		public static final String cpdDateSelector="@xpath=(//input[@aria-label='Colt Promise Date']/following-sibling::span[@aria-label='Date Field'])[1]";
		public static final String cpdDaysFromCalendar="@xpath=//td[@data-handler='selectDay']/a";
		public static final String cpdDaySelectFromCalendar="@xpath=//td[@data-handler='selectDay']/a[text()='value']";
		public static final String cpdReschedulePopup="@xpath=//div[@role='dialog']//span[text()='Reschedule - CPD']";
		public static final String cpdReschedule_ChangeDrivenBy="@xpath=//input[contains(@aria-label,'Change Driven By')]/following-sibling::span";
		public static final String cpdReschedule_RescheduleReason="@xpath=//input[contains(@aria-label,'Reschedule Reason')]/following-sibling::span";
		public static final String cpdRescheduleSave="@xpath=//button[@title='Reschedule - CPD:Save']/span";
		
		public static final String orderTypeField="@xpath=//table[@summary='Jeopardy']//tr//td[contains(@id,'COLT_Order_Type_Subtype')]";
		public static final String order_Type="@xpath=//input[@name='COLT_Order_Type_Subtype']";
		public static final String jeopardy_MenuBtn="@xpath=//button[@title='Jeopardy Menu']";
		public static final String newQuery="@xpath=(//ul[@role='menu']//li//a[contains(text(),'New Query')])";
		public static final String excludeCeaseOrder="@xpath=//input[@aria-label='Cease Orders']";
		public static final String jeoPardy_rowCounter ="@xpath=//div[contains(text(),'Jeopardy')]/..//span[@class='siebui-row-counter']";

		//CPD Recalculation Verification
		public static final String jeopardyPlanColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Plan_Rag')]";
		public static final String jeopardyPlanMilestoneColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Plan_Milestone')]";
		public static final String jeopardyBuildColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Build_Rag')]";
		public static final String jeopardyBuildmilestoneColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Build_Milestone')]";
		public static final String jeopardyActivateColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'Activate_Rag')]";
		public static final String jeopardyFlagColumnValue="@xpath=//td[@title='SiebelOrderNumber']//following-sibling::td[contains(@id,'CPD_Risk')]";
		
		//250222 changes
		public static final String jeopardyColumnsDisplayedButton="@xpath=//button[@title='Jeopardy Menu']/following-sibling::ul//li[contains(@data-caption,'Columns Displayed')]";
		public static final String serviceOrderRef="@xpath=//table[@summary='Service Order']//td[contains(@id,'Order_Number')]";
		public static final String Jeopardy_serviceOrderRef="@xpath=//table[@summary='Jeopardy']//td[contains(@id,'Order_Number')]";
		public static final String cpdicdNotSet_serviceOrderRef="@xpath=//table[@summary='CPD / ICD not set']//td[contains(@id,'Order_Number')]";
		public static final String projectOrder="@xpath=//input[@aria-label='Project Orders']";
		
		public static final String cpdDateReschedule="@xpath=//span[@id='cpd_reschedule_date_icon']";
		public static final String cpdDate="@xpath=//span[@id='cpd_date_icon']";
		
		public static final String icdDateSelector = "@xpath=(//div[@title='Dates Form Applet']//input[@aria-label='Indicative Completion Date']/following-sibling::span[contains(@id,'icon')])[2]";
		public static final String icdDateSelector1 = "@xpath=//div[@title='Dates Form Applet']//input[@aria-label='Indicative Completion Date']/following-sibling::span[contains(@id,'icon')]";
	 }
}
