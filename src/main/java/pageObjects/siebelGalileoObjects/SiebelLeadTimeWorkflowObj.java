package pageObjects.siebelGalileoObjects;

public class SiebelLeadTimeWorkflowObj {

	public static class LeadTimeWorkflow
	 { 
		public static final String cockpitTab="@xpath=//a[text()='Service Orders Cockpit']";
		public static final String allOrdersOption="@xpath=//select[@id='viewfilter']";
		public static final String jeopardyAppletHeader="@xpath=//div[text()='Jeopardy']";
		public static final String serviceOrderfield="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Number')]";
		public static final String serviceOrderText="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Number')]/input";
		public static final String newQueryMenu="@xpath=//button[@title='CockpitSectionName Menu']//parent::span//following-sibling::ul/li/a[contains(text(),'New Query')]";
		public static final String serviceOrderCockpitTab="@xpath=//a[text()='Service Orders Cockpit']";
		public static final String AdvancedFilter="@xpath=//span[text()='Advanced Filter']"; 
		public static final String CockpitSettings="@xpath=//button[@title='CockpitSectionName Menu']";
		public static final String dialogbox="@xpath=//div[@id='colt-cockpit-custom-overlay-container']/following-sibling::div[@role='dialog']";
		public static final String advanceSearch_allOrders="@xpath=//div[@title='Advanced Search Form Applet']//select[@id='colt-cockpit-popup-filter']";
		public static final String advancedSearchOKButton="@xpath=//button[@title='Advanced Search:OK']";
		public static final String qualityTab="@xpath=//li[@role='tab']/a[text()='Quality']";
		public static final String createQualityIssueButton="@xpath=//button/span[text()='Create New Quality Issue']";
		public static final String qualityIssue_TaskDropdown="@xpath=//input[contains(@aria-label,'Which task were you working')]/following-sibling::span";
		public static final String qualityTabSelectArrow="@xpath=//select[@aria-label='Third Level View Bar']";
		public static final String qualityIssue_CountryDropdown="@xpath=//input[contains(@aria-label,'Where are you based?')]/following-sibling::span";
		public static final String qualityIssue_proceedButton="@xpath=//button/span[text()='Proceed']";
		public static final String qualityIssue_TypeOfIssueList="@xpath=//input[@type='radio'][contains(@aria-labelledby,'Type_of_issue')]";
		public static final String qualityIssue_NextButton="@xpath=//button/span[text()='Next']";
		public static final String qualityIssue_IssueCausedByList="@xpath=//input[@type='radio'][contains(@aria-labelledby,'Issue_Caused_By')]";
		public static final String qualityIssue_ActionTakenList="@xpath=//input[@type='radio'][contains(@aria-labelledby,'Action_Taken_Label')]";
		public static final String qualityIssue_DescribeIssue="@xpath=//textarea[contains(@aria-label,'describe the issue')]";
		public static final String qualityIssue_IssueFixedOption="@xpath=//input[@type='radio'][contains(@aria-labelledby,'issue_been_fixed')]";
		public static final String qualityIssue_RCATeamSearch="@xpath=//input[contains(@aria-labelledby,Quality_Suggested_team_for_RCA_Label)]/following-sibling::span[@aria-label='Selection Field']";
		public static final String RCATeamPopup="@xpath=//div[@role='dialog']//span[text()='Pick Suggested Team']";
		public static final String RCATeamList="@xpath=//table[@summary='Pick Suggested Team']//td[@role='gridcell'][contains(@id,'Value')]";
		public static final String RCATeamPopup_OKButton="@xpath=//button[@title='Pick Suggested Team:OK']/span[text()='OK']";
		public static final String RCATeamPopup_NextArrow="@xpath=(//td[contains(@id,'next_pager')]//span[@title='Next record set'])[2]";
		public static final String RCATeamPopup_NextArrowDisabled="@xpath=(//td[contains(@id,'next_pager')])[2]";
		public static final String qualityIssue_CancelButton="@xpath=//td//button/span[text()='Cancel']";
		public static final String RCATeamPopup_TeamNameInputField="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String RCATeamPopup_GoButton="@xpath=(//span[@class='siebui-popup-button']/button[@title='Pick Suggested Team:Go'])[2]";
		public static final String RCATeamPopup_FinishButton="@xpath=//button[@title='SmartScript:Finish']";
		public static final String createdIssue="@xpath=//table[@summary='Quality Issues']//tr[@aria-selected='true']//td[contains(@id,'value')]";
		public static final String qualityIssue_ActionTakenOtherField="@xpath=//input[@aria-label='QualityIssueOther']";
		public static final String workflowsTab="@xpath=//li[@role='tab']/a[text()='Workflows']";
		public static final String newButton="@xpath=//button[@title='Workflow:New']";
		public static final String workflowTypeDropdownClick="@xpath=//input[@aria-label='Workflow Type']/following-sibling::span";
		public static final String workflowPopup="@xpath=//div[@role='status']/following-sibling::div[@role='dialog']";
		public static final String workflowPopupMessage="@xpath=//div[@role='status']/following-sibling::div[@role='dialog']//span[contains(@class,'moredetails-msg')]";
		public static final String workflowPopupOkButton="@xpath=//div[@role='status']/following-sibling::div[@role='dialog']//button[text()='Ok']";
		public static final String serviceOrderTabSelectArrow="@xpath=//select[@aria-label='First Level View Bar']";
		public static final String workflowTypeValue="@xpath=//td[contains(@id,'Workflow_Type')]";
		public static final String workflowTypeValuesList="@xpath=//ul[@role='combobox']/li/div";
		public static final String changeWorkflowButton="@xpath=//button[@title='Workflow:Change Workflow']";
		public static final String changeWorkflowPopupAccept="@xpath=//button[@type='button'][text()='Yes']";
		public static final String reasonCategoryDropdownArrow="@xpath=//input[@id='sifcat-reason-categoery']/following-sibling::span";
		public static final String AendReasonDropdownArrow="@xpath=//input[@id='sifcat-Aend-reason']/following-sibling::span";
		public static final String BendReasonDropdownArrow="@xpath=//input[@id='sifcat-Bend-reason']/following-sibling::span";
		public static final String proceedButton="@xpath=//button[@type='button'][text()='Proceed']";
		public static final String inflightWorkflowRadioButton="@xpath=//input[@value='In-Flight WFs']";
		public static final String changeWorkflowOKButton="@xpath=//div[@role='status']/following-sibling::div[@role='dialog']//button[text()='OK']";
		
		public static final String qualityIssuesLabel="@xpath=//span/b[text()='Quality Issues']";
		public static final String issueTaskNameTxb="@xpath=//input[@aria-labelledby='Category_Label']";
		public static final String regionTxb="@xpath=//input[@aria-labelledby='Region_Label']";
		public static final String proceedBtn="@xpath=//button[@title='Quality Issues:Proceed']";
		public static final String typeofIssueLabel="@xpath=//span[text()='Type Of Issue']";
		public static final String cancelBtn="@xpath=//button[@title='SmartScript:Cancel']";
		
		//250222
		public static final String workflowSaveButton="@xpath=//button[@title='Workflow:Save']";
		public static final String workflowStatusValue="@xpath=//td[contains(@id,'Workflow_Status')]";
		public static final String workflowPopupIgnore="@xpath=//button[@type='button'][text()='No']";
		public static final String reasonCategoryDropdownValues="@xpath=//input[@id='sifcat-reason-categoery']/following-sibling::ul/li/div";
		public static final String AendReasonDropdownValues="@xpath=//input[@id='sifcat-Aend-reason']/following-sibling::ul/li/a";
		public static final String BendReasonDropdownValues="@xpath=//input[@id='sifcat-Bend-reason']/following-sibling::ul/li/a";
		public static final String inflightChangeWorkflowPopup="@xpath=//span[contains(text(),'Inflight change')]/ancestor::div[@role='dialog']";
		public static final String workflowReasonPopup="@xpath=//span[contains(text(),'reason for workflow change')]/ancestor::div[@role='dialog']";
		public static final String primarySecondaryValue="@xpath=//td[contains(@id,'Primary_Secondary')]";
	 }
}
