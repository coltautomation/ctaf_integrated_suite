package pageObjects.siebelGalileoObjects;

public class WelcomeCallActivityObj {

	public static class WelcomeCallActivity
	 { 
		public static final String activitiesTab="@xpath=//li[@role='tab']/a[text()='Activities']";
		public static final String activitiesData="@xpath=//table[@summary='Activities']//tr[@id='1']//td[@id]";
		public static final String ActivitiesPage="@xpath=//div[@title='Service Order Activities List Applet']//div[contains(@class,'ServiceOrderActivityClass')]";
		public static final String serviceOrderfield="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Number')]";
		public static final String serviceOrderText="@xpath=//div[@title='CockpitSectionName List Applet']//td[contains(@id,'Order_Number')]/input";
		public static final String newQueryMenu="@xpath=//button[@title='CockpitSectionName Menu']//parent::span//following-sibling::ul/li/a[contains(text(),'New Query')]";
		public static final String serviceOrderCockpitTab="@xpath=//a[text()='Service Orders Cockpit']";
		public static final String AdvancedFilter="@xpath=//span[text()='Advanced Filter']"; 
		public static final String CockpitSettings="@xpath=//button[@title='CockpitSectionName Menu']";
		public static final String dialogbox="@xpath=//div[@id='colt-cockpit-custom-overlay-container']/following-sibling::div[@role='dialog']";
		public static final String advanceSearch_allOrders="@xpath=//div[@title='Advanced Search Form Applet']//select[@id='colt-cockpit-popup-filter']";
		public static final String advancedSearchOKButton="@xpath=//button[@title='Advanced Search:OK']";
		public static final String qualityTab="@xpath=//li[@role='tab']/a[text()='Quality']";
		
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		public static final String SelectOnholdValidation="@xpath=//li/*[text()='On-hold']";
	 }
}
