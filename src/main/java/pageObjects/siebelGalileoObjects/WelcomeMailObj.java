package pageObjects.siebelGalileoObjects;

public class WelcomeMailObj {
	
	public static class WelcomeMail
	 { 
		//Welcome Mail
				//		public static final String serviceOrder_Max="@xpath=//div[contains(text(),'Service Order')]/..//span[@title=':Show more']";
						public static final String orderSelection="@xpath=//table[@summary='Service Order']//a[@name='Order Number']";
						public static final String status_Reason="@xpath=//span[contains(text(),'Status Reason')]";
						public static final String orderingContact="@xpath=//table[@summary='Pick Recipient']//td[contains(text(),'Ordering Contact')]";
						public static final String okBtn="@xpath=//button//span[contains(text(),'OK')]";
						public static final String bodyInput="@xpath=//input[@aria-label='Template Name']";
//						public static final String textArea="@xpath=//div[@id='cke_1_contents']";
						public static final String sendButton="@xpath=//button//span[contains(text(),'Send')]";
						public static final String toMail="@xpath=//div[@name='popup']//span[@class='mceField siebui-email-to']//input";
						public static final String closing="@xpath=//input[@aria-label='Closing']";
						
						public static final String product="@xpath=//td[@title='Ethernet Line']";
						public static final String serviceOrder="@xpath=//option[contains(text(),'Service Order')]";
						public static final String options="@xpath=//select[contains(@id,'ctrl_tabScreen')]";
						
						public static final String footer1="@xpath=/html/body/p[1]";
						public static final String footer2="@xpath=/html/body/p[2]";
						
						public static final String contents="@xpath=//iframe[@tabindex='0']";
						
			//Mail Box
	
//						public static final String subject=("@xpath=//div[contains(@class,'email_subject')]");
//						public static final String Footer1=("@xpath=//*[@id='big_container']//div[2]//div[1]//div[4]/div/p[1]");
//						public static final String signature=("@xpath=//*[@id='big_container']//div[2]//div[1]//div[4]/div/p[2]");
						
			//250222 changes
						public static final String subject=("@xpath=//div[contains(@class,'email_subject')]");
						public static final String Footer1=("@xpath=//*[@id='big_container']//div[2]//div[1]//div[4]/div/p[1]");
						public static final String signature=("@xpath=//*[@id='big_container']//div[2]//div[1]//div[4]/div/p[2]");
						public static final String image=("@xpath=((//div[contains(@class,'siebui-emr-closing')])[1]//a//img)[1]");

						public static final String MailAddressA=("@xpath=(//u[text()='A End']/../../..//following-sibling::p[2])[1]");
						public static final String MailAddressB=("@xpath=(//u//strong[text()='B End']/../../..//following-sibling::p[2])[1]");
						//CHCC
						public static final String primayAddressA=("@xpath=//div[@id='colt-site-left']//span[contains(@class,'colt-site-top-addr')]");
						public static final String primaryAddressB=("@xpath=//div[@id='colt-site-right']//span[contains(@class,'colt-site-top-addr')]");
						
	 }

}
