package pageObjects.siebelObjects;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;


public class SiebelCeaseObj {
	

	public static class CeaseCompletedValidation
	{
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
		public static final String SubnetworkPopUP="@xpath=//button[@class='colt-primary-btn']";
		public static final String OrderStatusInput="@xpath=//input[@aria-label='Order Status']";
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
	//	public static final String SelectCompleted="@xpath=//li[text()='Completed']";
		public static final String SelectCompleted="@xpath=//li/*[text()='Completed']";
		public static final String OrderComplete="@xpath=//button[text()='Yes']";
	}
	
	public static class DeliveryValidation
	{
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		//public static final String SelectDeliveryValidation="@xpath=//li[text()='Delivery']";
		public static final String SelectDeliveryValidation="@xpath=//li/*[text()='Delivery']";
		public static final String PopupYes="@xpath=//button[text()='Yes']";
		public static final String SubnetworkPopUP="@xpath=//button[@class='colt-primary-btn']";

	}
	
	
	public static class CeaseCommercialValidationObj
	{
	public static final String OrderStatus="@xpath=//input[@aria-label='Order Status']";
	public static final String PopupYes="@xpath=//button[text()='Yes']";
	public static final String SubnetworkPopUP="@xpath=//button[@class='colt-primary-btn']";

	}
	
	
	public static class CeaseMainMethodObj
	{
	public static final String ServiceTab="@xpath=//a[text()='Service Order']";
	public static final String AlertAccept="@xpath=//span[text()='Ok']";
	public static final String ServiceOrderSearchForAll="@xpath=//input[@aria-label='Starting with']";
	public static final String ServiceOrderArrow="@xpath=//input[@aria-label='Starting with']/..//button[1]";
	public static final String ServiceOrderReferenceNo="@xpath=//a[contains(@name,'Order Num')]";
	public static final String CeaseInternal="@xpath=//button[@aria-label='Service Orders:Cease']";
	public static final String CeaseOrder="@xpath=//span[text()='Cease']";
	public static final String ServiceOrderModifyNumber="@xpath=//table[@class='ui-jqgrid-btable']//a";
	public static final String RequestReceivedDate="@xpath=//*[(@aria-label='Request Received Date')]";
	public static final String OrderSubTypeSearch="@xpath=//div[@class='mceGridField siebui-value mceField']/span[@aria-label='Multiple Selection Field']";
	public static final String AddOrderSubType="@xpath=//button[@title='Service Order Sub Type:New']";
	public static final String InputOrderSubType="@xpath=//input[@name='COLT_Order_Item_Sub_Type']";
	public static final String SubmitSubOrderType="@xpath=//button[@title='Service Order Sub Type:OK']";
	public static final String CeaseReason="@xpath=//input[@aria-labelledby='COLT_Cease_Order_Reason_Code_Label']";
	public static final String SaveOrderChanges="@xpath=//a[text()='Click here to save your order changes.']";
	public static final String OrderDates="@xpath=//a[text()='Order Dates']";
	public static final String OrderSignedDate="@xpath=//input[@aria-label='Order Signed Date']";
	public static final String OrderReceivedDate="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']";
	public static final String CustomerRequestedDate="@xpath=//input[@aria-labelledby='Customer_Requested_Date1_Label']";
	public static final String Billing="@xpath=//a[text()='Billing']";
	public static final String BillingEndDateAccessIcon="@xpath=//input[@aria-label='Billing End Date']/following-sibling::*[contains(@class,'siebui-icon-date')]";
	public static final String POStartDateAccess="@xpath=//input[@aria-label='PO Start Date']";
	public static final String ServiceOrderGridHeader="@xpath=//table[@class='ui-jqgrid-htable']//th//div";
	public static final String ServiceOrderGridItem="@xpath=//table[@class='ui-jqgrid-btable']//tr[2]//td[-10]";
	public static final String BillingStatus="@xpath=//td[contains(@id,'Billing_Status')][1]";

	}

}
