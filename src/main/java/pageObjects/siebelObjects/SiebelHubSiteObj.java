package pageObjects.siebelObjects;

public class SiebelHubSiteObj {
	

	public static class Customize
	 { 	
		public static final String clickDropdown="@xpath=(//span[text()='Value']/parent::*/span)[2]";
		//public static final String selectValueDropdown="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String selectValueDropdown="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
		public static final String serviceBandwidthDropdownAccess="@xpath=(//span[text()='Service Bandwidth']/..//span)[3]";
		//public static final String serviceBandwidthSelectAccess="@xpath=//li[text()='value']";
		public static final String serviceBandwidthSelectAccess="@xpath=//li/*[text()='value']"; //Updated 2020
		public static final String textInput="@xpath=//span[text()='Value']/parent::*//input";
		public static final String clickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";
		public static final String SearchAddressSiteA="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";
		public static final String StreetNamerfs="@xpath=//input[@name='streetName']";
		//public static final String Country="@xpath=//div[contains(@style,'block')]//span[text()='Country']/following-sibling::span";
		public static final String Country="@xpath=//*[not(contains(@style,'display'))]//span[text()='Country']/following-sibling::span";
	//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
		public static final String City="@xpath=//input[@name='cityTown']";
		public static final String PostalCode="@xpath=//input[@name='postalZipCode']";
		public static final String Premises="@xpath=//input[@name='premisesNumber']";
		public static final String Search="@xpath=//div[@id='colt-site-selection-btn-bar']//button[text()='Search']";
		public static final String SearchAddressRowSelection="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
		public static final String PickAddress="@xpath=//button[text()='Pick Address']";
		public static final String PickBuilding="@xpath=//button[text()='Pick Building']";
		public static final String PickSite="@xpath=//button[text()='Pick Site']";
		public static final String ServicePartySearchAccess="@xpath=//span[text()='Service Party']/..//span[2]";
		public static final String ServicePartyDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
	//	public static final String PartyNameAccess="@xpath=//li[text()='Party Name']";
		public static final String PartyNameAccess="@xpath=//li/*[text()='Party Name']"; //Updated2020
		public static final String InputPartyNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String PartyNameSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String PartyNameSubmitAccess="@xpath=//button[@aria-label='Pick Account:OK']";
		public static final String SiteContactSearchAccess="@xpath=//span[text()='Site Contact']/..//span[2]";
		public static final String SiteContactDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/parent::*/span";
		public static final String InputSiteNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String LastNameSiteSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String LastNameSiteSubmitAccess="@xpath=//button[@aria-label='Pick Contact:OK']";
		public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
		
		public static final String BandwidthErrorPOP="@xpath=//*[contains(@id,'colt-formalerts-popup')]";
		public static final String BandwidthErrorPOPSetToBtn="@xpath=//*[@id='colt-formalerts-hypen-btn']";
	//	public static final String SiteContactOCN="@xpath=//li[text()='OCN']";
		public static final String SiteContactOCN="@xpath=//li/*[text()='OCN']"; //UPdated 2020
		public static final String SelectContact="@xpath=(//tr[@class='ui-widget-content jqgrow ui-row-ltr ui-state-highlight'])[2]";
		
		
	 }

}

