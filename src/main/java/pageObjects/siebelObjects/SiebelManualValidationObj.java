package pageObjects.siebelObjects;

public class SiebelManualValidationObj {
	
	public static class Validation
	 { 	
	//	public static final String manualvalidation1="@xpath=(//button[@aria-label='Service Orders:Manual Validation'])[1]";
	//	public static final String manualvalidation2="@xpath=(//button[@aria-label='Service Orders:Manual Validation'])[2]";
		public static final String manualvalidation1="@xpath=//button[@aria-label='Service Orders:Manual Validation' and not(@disabled='disabled')]";
		public static final String manualvalidation2="@xpath=//button[@aria-label='Service Orders:Manual Validation' and not(@disabled='disabled')]";
		
	 }
	public static class IQNET
	 { 	
	
		public static final String IQNETInput="@xpath=//*[text()='Reason Not IQNET']/following-sibling::input";
		public static final String IQNETDetailsInput="@xpath=//*[text()='Reason Not IQNET Details']/following-sibling::input";
		
	 }

}
