package pageObjects.siebelObjects;

public class SiebelNewOrderOnnetHelperObj {

	
	public static class NewOrderOnnetHelper
	 { 
		public static final String expandAllButton="@xpath=//button[@title=\"Service Charges:Expand All\"]";
		public static final String installationChargeNRC="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[3]/td[5]";
		public static final String inputinstallationChargeNRC="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[3]/td[5]/input";
		public static final String recurringChargeMRC="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[5]/td[5]";
		public static final String inputRecurringChargeMRC="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[5]/td[5]/input";
		public static final String bCNInstallationChargeNRC="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[3]/td[6]";
		public static final String bCNInstallationChargeNRCSearch="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[3]/td[6]/span";
		public static final String bCNInstallationChargeNRCInput="@xpath=//input[@aria-labelledby=\"PopupQuerySrchspec_Label\"]";
		public static final String bCNNRCSubmit="@xpath=//button[@aria-label=\"Billing Profile:OK\"]";
		public static final String bCNRecurringChargeMRC="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[5]/td[6]";
		public static final String bCNRecurringChargeMRCSearch="@xpath=//table[@summary=\"Service Charges\"]/tbody/tr[5]/td[6]/span";
		public static final String bCNRecurringChargeMRCInput="@xpath=//input[@aria-labelledby=\"PopupQuerySrchspec_Label\"]";
		public static final String installtionDropdown="@xpath=//select[@aria-label='Third Level View Bar']";
		public static final String primaryTestingMethod="@xpath=//input[@aria-label=\"Primary Testing Method\"]";
		public static final String secondaryTestingMethod="@xpath=//input[@aria-label=\"Secondary Testing Method\"]";
		public static final String saveOrderContinue="@xpath=//a[@class='colt-noedit-config-save']";
		public static final String serviceOrderGridHeader="@xpath=//table[@class='ui-jqgrid-htable']//th//div";
		public static final String eroGirdRow="@xpath=//table[@class='ui-jqgrid-btable']//tr[@class!='jqgfirstrow']";
		public static final String eroGridData="@xpath=(//table[@class='ui-jqgrid-btable']//tr[@class!='jqgfirstrow'][-row10]//td[-td10])[1]";
		public static final String supportStarDate="@xpath=//span[text()='Support Start Date']/parent::*//input";
		public static final String supprtEndDate="@xpath=//span[text()='Support End Date']/parent::*//input";
		public static final String saveButton="@xpath=//a[@class=\"colt-primary-btn colt-s-order-save-btn\"]";
		public static final String middleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
//		public static final String middleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String middleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // updated IP2020
		public static final String drakFiberSaveButton="@xpath=//div/a[text()='Save']";
		public static final String cPECombinationIDGeneric="@xpath=//span[text()='Value']/parent::div//input";
		public static final String highAvailabilityRequired="@xpath=(//span[text()='Value']/parent::*//span)[3]";
	//	public static final String highAvailabilityRequiredli="@xpath=//li[text()='Yes']";
		public static final String highAvailabilityRequiredli="@xpath=//li/*[text()='Yes']";
		public static final String clickDropdown="@xpath=(//span[text()='Value']/parent::*/span)[2]";
	//	public static final String selectValueDropdown="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String selectValueDropdown="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String serviceBandwidthDropdownAccess="@xpath=(//span[text()='Service Bandwidth']/..//span)[3]";
	//	public static final String serviceBandwidthSelectAccess="@xpath=//li[text()='value']";
		public static final String serviceBandwidthSelectAccess="@xpath=//li/*[text()='value']"; //Updated IP2020
		public static final String textInput="@xpath=//span[text()='Value']/parent::*//input";
		public static final String clickheretoSaveAccess="@xpath=(//a[text()='Click here to save your product configuration.'])[1]";
		public static final String searchAddressSiteA="@xpath=//img[@id=\"imgSelectAddressSiteA\"]";
		public static final String streetNamerfs="@xpath=//input[@name='streetName']";
	//	public static final String country="@xpath=//div[contains(@style,'block')]//span[text()='Country']/following-sibling::span";
		public static final String country="@xpath=//*[not(contains(@style,'display'))]//span[text()='Country']/following-sibling::span"; // Updated IP2020
	//	public static final String siteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String siteABSelection="@xpath=//ul[cont//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String city="@xpath=//input[@name='cityTown']";
		public static final String postalCode="@xpath=//input[@name='postalZipCode']";
		public static final String premises="@xpath=//input[@name='premisesNumber']";
		public static final String search="@xpath=//div[@id='colt-site-selection-btn-bar']//button[text()='Search']";
		public static final String searchAddressRowSelection="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
		public static final String pickAddress="@xpath=//button[text()='Pick Address']";
		public static final String pickBuilding="@xpath=//button[text()='Pick Building']";
		public static final String pickSite="@xpath=//button[text()='Pick Site']";
		public static final String servicePartySearchAccess="@xpath=//span[text()='Service Party']/..//span[2]";
		public static final String servicePartyDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
		public static final String partyNameAccess="@xpath=//li[text()='Party Name']";
		public static final String inputPartyNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String partyNameSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String partyNameSubmitAccess="@xpath=//button[@aria-label='Pick Account:OK']";
		public static final String siteContactSearchAccess="@xpath=//span[text()='Site Contact']/..//span[2]";
		public static final String siteContactDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/parent::*/span";
		public static final String inputSiteNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String lastNameSiteSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String lastNameSiteSubmitAccess="@xpath=//button[@aria-label='Pick Contact:OK']";
		public static final String ipGurdianSave="@xpath=//a[@class=\"colt-primary-btn colt-s-order-save-btn\"]";
		public static final String serviceOrderTab="@xpath=//a[text()='Service Order']";
		public static final String inputServiceOrder="@xpath=//input[@aria-label='Starting with']";
		public static final String serviceOrderGo="@xpath=(//button[@aria-label='Service Order List:Go'])[2]";
		public static final String serviceOrderClickOn="@xpath=//a[contains(@name,'Order Num')]";
		public static final String clickLink="@xpath=//a[text()='Value']";
		public static final String networkService="@xpath=//td[@title='IP VPN Service']";
		public static final String gOIPService="@xpath=//a[text()='Value']";
		public static final String assetButton="@xpath=//span[@title=\"Assets\"]";
		public static final String alertAccept="@xpath=//span[text()='Ok']";
		public static final String serviceOrderOM="@xpath=//input[@aria-label=\"Service ID OM\"]";
		public static final String goButton="@xpath=//button[@title=\"Search:Go\"]";
		public static final String assetNumber="@xpath=//a[@name=\"Asset Number\"]";
		public static final String tabs="@xpath=//a[@class='ui-tabs-anchor' and text()='tabName']";
		public static final String save="@xpath=//div[@id='colt-s-order-connection-header']//a[text()='Save']";
		public static final String tabDropdown="@xpath=//select[@id='j_s_vctrl_div_tabScreen']";
		public static final String attachmentTabSelection="@xpath=//div[@id='s_vctrl_div_tabScreen']//ul//li//a[text()='Attachments']";
		public static final String fileUpload="@xpath=//input[@name='s_SweFileName' and @title='Attachments:New File']";
		public static final String documnetTypeOther="@xpath=//div[@id='s_S_A3_div']//table[@class='ui-jqgrid-btable']//tr//td[text()='Other']";
		public static final String downArrow="@xpath=//span[@class='siebui-icon-dropdown']";
	//	public static final String doucmentTypeSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Filetype']";
		public static final String doucmentTypeSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Filetype']";  //Updated IP2020
		public static final String partialCheckbox="@xpath=//input[@aria-label=\"Partial Delivery Flag\"]";
		public static final String operationalAttribueClick="@xpath=(//table[@summary=\"Operational Attributes\"]//tr[contains(@class,'mandatory')]//td[3])[index]";
		public static final String settingsButton="@xpath=//div[@id='colt-s-order-connection-header-btns']//span";
		public static final String operationalAttributeDropdown="@xpath=//table[@summary=\"Operational Attributes\"]//tr[contains(@class,'mandatory')]//td[3]/span";
	//	public static final String locatorSelectValueDropdown="@xpath=(//li[text()='Value'])";
		public static final String locatorSelectValueDropdown="@xpath=(//li/*[text()='Value'])"; // Updated IP2020
		public static final String okButtonOperationalAttribute="@xpath=//button[@aria-label='Operational Attributes:OK']";
		public static final String operationalAttribueCount="@xpath=(//table[@summary=\"Operational Attributes\"]//tr[contains(@class,'mandatory')]//td[3])";
		public static final String operationalAttributeText="@xpath=//table[@summary=\"Operational Attributes\"]//tr[contains(@class,'mandatory')]//td[3]/input";
		public static final String operationalAttributeOK="@xpath=//button[@aria-label='Operational Attributes:OK']";
		public static final String inputValuesVendor="@xpath=(//td[text()='Vendor Platform']//following::input)[1]";
		public static final String techonolgyValuecell="@xpath=(//td[text()='Technology']//following::td)[1]";
		public static final String inputvaluesTechonolgy="@xpath=(//td[text()='Technology']//following::input)[1]";
		public static final String oKButton="@xpath=//button[@aria-label=\"Operational Attributes:OK\"]";
		public static final String serviceGroupTab="@xpath=//a[text()='Service Group']";
		public static final String serviceOrderCheck="";
		public static final String clickOnPlusButtonService="@xpath=//button[@aria-label='Service Group:New']";
		public static final String serviceGroupReference="@xpath=//input[@name='COLT_Service_Group_Reference']/following-sibling::span";
		public static final String clickOkButton="@xpath=//button[@aria-label='Add/Pick Service Group:OK']";
		public static final String dropDown="@xpath=//*[@aria-label='Third Level View Bar']";
		public static final String serviceGroupNew="@xpath=//button[@aria-label='Service Group:New']";
		public static final String serviceGrouplookup="@xpath=//span[@class='siebui-icon-pick']";
		public static final String serviceGroupOk="@xpath=//button[@title='Add/Pick Service Group:OK']";
		public static final String siteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
		public static final String siteBInput ="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";
		public static final String siteADropdownClick  ="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
		public static final String siteAInput ="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";
	 }
	public static class AbandonedforIPVPN
	 { 
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String CustOrder="@xpath=input[@aria-labelledby='Order_Number_Label']"; 
		public static final String CustOrderGo="@xpath=//button[contains(@class,'gotoview')";
		public static final String ClickSeibelOrder="@xpath=//a[@name='Order Number']";
		public static final String NewServiceOrder="@xpath=//a[@name='Order Number']";

	 }
	public static class AbandonedOrder 
	 { 
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
	//	public static final String OrderStatusAbandoned="@xpath=//li[text()='Abandoned']"; 
		public static final String OrderStatusAbandoned="@xpath=//li/*[text()='Abandoned']";  // Updated IP2020
		public static final String ServiceTab="@xpath=//a[text()='Service Order']";
	 }
	public static class addEthernetSiteHub 
	 { 
		public static final String Searchbutton="@xpath=//span[@class='search_img']/img";
		public static final String StreetName="@xpath=//input[@id='txtStreetName']"; 
		public static final String Country="@xpath=//select[@class='ctrlClasstype'][@id='ddlCountry']";
		public static final String City="@xpath=//input[@class='ctrlClasstype'][@id='txtCityTown']";
		public static final String Premises="@xpath=//input[@class='ctrlClasstype'][@id='txtPremises']";
		public static final String Search="@xpath=//input[@value='Search']";
		public static final String pickAddress="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[1]";
		public static final String Pick="@xpath=//div[@id='PopupDiv']//input[@value='Pick']";
		public static final String PostalCode="@xpath=//input[@class='ctrlClasstype'][@id='txtPostalCode']";
		public static final String AddressTable="@xpath=//table[@id='address_table']//tr//td[contains(text(),'";
		public static final String ServicePartySearch="@xpath=//img[@name='Service Party']";
		public static final String SelectServiceParty="@xpath=//select[@id='ddlSearchData']";
		public static final String InputServicePartyName="@xpath=//input[@id='txtSearchInput']";
		public static final String SearchServicePartyName="@xpath=//img[@title='Search'][@id='imgSearchContactData']";
		public static final String pickServiceParty="@xpath=//div[@id='divAddressList']//table[@id='address_table']/tbody/tr[1]";
		public static final String PickButton="@xpath=//input[@value='Pick'][@id='PickAccount']";
		public static final String SiteContactSearch="@xpath=//img[@name='Site Contact']";
		public static final String InputContactName="@xpath=//input[@id='txtSearchInput']";
		public static final String SearchContactName="@xpath=//img[@id='imgSearchContactData']";
		public static final String pickContactName="@xpath=//div[@id='divAddressList']//table[@id='address_table']/tbody/tr[1]";
		public static final String PickContact="@xpath=//input[@id='PickContact']";

		
	 }
	public static class addEthernetSiteSpoke 
	 {  
		public static final String SearchAddressSiteB="@xpath=//img[@id='imgSelectAddressSiteB']";
		public static final String StreetName="@xpath=//input[@id='txtStreetName']"; 
		public static final String Country="@xpath=//select[@class='ctrlClasstype'][@id='ddlCountry']";
		public static final String City="@xpath=//input[@class='ctrlClasstype'][@id='txtCityTown']";
		public static final String Premises="@xpath=//input[@class='ctrlClasstype'][@id='txtPremises']";
		public static final String Search="@xpath=//input[@value='Search']";
		public static final String pickAddress="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[1]";
		public static final String Pick="@xpath=//div[@id='PopupDiv']//input[@value='Pick']";
		public static final String PostalCode="@xpath=//input[@class='ctrlClasstype'][@id='txtPostalCode']";
		public static final String ServicePartySearchSiteB="@xpath=//div[@id='SiteAddress_B']//img[@name='Service Party']";
		public static final String SelectServiceParty="@xpath=//select[@id='ddlSearchData']";
		public static final String InputServicePartyName="@xpath=//input[@id='txtSearchInput']";
		public static final String SearchServicePartyName="@xpath=//img[@title='Search'][@id='imgSearchContactData']";
		public static final String pickServiceParty="@xpath=//div[@id='divAddressList']//table[@id='address_table']/tbody/tr[1]";
		public static final String PickButton="@xpath=//input[@value='Pick'][@id='PickAccount']";
		public static final String SiteContactSearchSiteB="@xpath=//div[@id='SiteAddress_B']//img[@name='Site Contact']";
		public static final String InputContactName="@xpath=//input[@id='txtSearchInput']";
		public static final String SearchContactName="@xpath=//img[@id='imgSearchContactData']";
		public static final String pickContactName="@xpath=//div[@id='divAddressList']//table[@id='address_table']/tbody/tr[1]";
		public static final String PickContact="@xpath=//input[@id='PickContact']";

	 }
	public static class addSiteADetails
	 {  
		public static final String SearchAddressSiteA="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";
		public static final String StreetNamerfs="@xpath=//input[@name='streetName']"; 
	//	public static final String Country="@xpath=//div[contains(@style,'block')]//span[text()='Country']/following-sibling::span";
		public static final String Country="@xpath=//*[not(contains(@style,'display'))]//span[text()='Country']/following-sibling::span";// updated IP2020
		
		public static final String City="@xpath=//input[@name='cityTown']";
		public static final String Premises="@xpath=//input[@name='premisesNumber']";
		public static final String Search="@xpath=//div[@id='colt-site-selection-btn-bar']//button[text()='Search']";
		public static final String pickAddress="@xpath=//button[text()='Pick Address']";
		public static final String SearchAddressRowSelection="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
		public static final String PostalCode="@xpath=//input[@name='postalZipCode']";
	//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String PickBuilding="@xpath=//button[text()='Pick Building']";
		public static final String PickSite="@xpath=//button[text()='Pick Site']";
		public static final String ServicePartySearchAccess="@xpath=//span[text()='Service Party']/..//span[2]";
		public static final String ServicePartyDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
		public static final String SiteContactDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/parent::*/span";
		public static final String InputPartyNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String PartyNameSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String PartyNameSubmitAccess="@xpath=//button[@aria-label='Pick Account:OK']";
		public static final String SaveOrderChanges="@xpath=//a[text()='Click here to save your order changes.']";
		public static final String SiteContactSearchAccess="@xpath=//span[text()='Site Contact']/..//span[2]";
		public static final String InputSiteNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String LastNameSiteSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String LastNameSiteSubmitAccess="@xpath=//button[@aria-label='Pick Contact:OK']";
		public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";


	 }
	public static class AEndInputEnter 
	 { 
		public static final String SiteAInput="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";
	 }
	public static class AlertAccept 
	 { 
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
	 }
	public static class  alertPopUp 
	 { 
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
	 }
	public static class  BSiteCustomize
	 { 
		 public static final String BEndSite="@xpath=//a[text()='B End Site'][@href='javascript:void(0);']";
		public static final String CustomizeButton="@xpath=//button[@id='btnMiddleCustom']";
		public static final String Coverage="@xpath=//a[contains(@name,'Coverage')]//following-sibling::*//select";
		public static final String ServiceBandwidth="@xpath=//a[contains(@name,'Service Bandwidth')]//following-sibling::*//select";
		public static final String AEndResilienceOption="@xpath=//a[contains(@name,'B End Resilience Option')]//following-sibling::*//select";
		public static final String BEndResilienceOption="@xpath=//a[contains(@name,'B End Resilience Option')]//following-sibling::*//select";
		public static final String subseaWorkerpath="@xpath=//a[contains(@name,'Sub Sea Cable System (Worker Path)')]//following-sibling::*//select";
		public static final String subseaprotectedpath="@xpath=//a[contains(@name,'Sub Sea Cable System (Protected Path)')]//following-sibling::*//select";
		public static final String Connectionlink="@xpath=//a[contains(text(),'Connection')][@href='javascript:void(0);']";
		public static final String AccessTechnologySearch="@xpath=(//div[text()='Access Technology']/following::span[@class='siebui-icon-icon_pick ']/img)[1]";
		public static final String SelectAccessTechnology1="@xpath=//table[@summary='Select Access Technology']//tr//td[text()='3rd Party Leased Line']";
		public static final String AccessTechnologySubmit="@xpath=//button[@title='Select Access Technology:OK']";
		public static final String ThirdPartyAccessProviderSearch="@xpath=//div[@class='cxEdit siebui-ecfg-editfield']//span[@class='siebui-icon-icon_pick']/img";
		public static final String ThirdPartyAccessProviderSubmit="@xpath=//button[@title='Select Third Party Access Provider:OK']";
		public static final String ThirdPartyConnecRef="@xpath=//a[contains(@name,'3rd Party Connection Reference')]//following-sibling::*//input";
		public static final String ThirdPartySlaTierValue="@xpath=//option[text()='Tier 1']";
		public static final String ThirdPartySlaTier="@xpath=//a[contains(@name,'Third Party SLA Tier')]//following-sibling::*//select";
		public static final String CustomerSitePopStatusBEnd="@xpath=//a[contains(@name,'Customer Site Pop Status')]//following-sibling::*//select";
		public static final String BuildingtypeBEnd="@xpath=//a[contains(@name,'Building Type')]//following-sibling::*//select";

		public static final String CPEInformationLink="@xpath=//a[text()='CPE Information'][@href='javascript:void(0);']";
		public static final String CabinetTypeBEnd="@xpath=//a[contains(@name,'Cabinet Type')]/following-sibling::*//select";
		public static final String CabinetIdBEnd="@xpath=//a[contains(@name,'Cabinet ID')]/following-sibling::*//input";
		public static final String ShelfID="@xpath=//a[contains(@name,'Shelf ID')]/following-sibling::*//input";
		public static final String AccessPortLink="@xpath=//table[@class='siebui-ecfg-module-content']//a[text()='Access Port']";
		public static final String PresentationInterfaceSearch="@xpath=//img[@class='siebui-ctrl-img ']";
		public static final String SubmitPresentationInterface="@xpath=//button[@data-display='OK']";
		public static final String PortRoleBEnd="@xpath=//a[contains(@name,'Port Role')]/following-sibling::*//select";
		public static final String SlotID="@xpath=//a[contains(@name,'Slot ID')]/following-sibling::*//input";
		public static final String PhysicalPortID="@xpath=//span[text()='Physical Port ID']/..//input";
		public static final String VLANTaggingMode="@xpath=//a[contains(@name,'VLAN Tagging Mode')]/following-sibling::*//select";
		public static final String VLAN="@xpath=//a[text()='VLAN'][@href='javascript:void(0);']";
		public static final String VlanTagId="@xpath=//a[contains(@name,'VLAN Tag ID')]/following-sibling::*//input";
		public static final String Terminationinformation="@xpath=//a[text()='Termination Information'][@href='javascript:void(0);']";
		public static final String InstallTime="@xpath=//span[text()='Install Time']/..//input";
		public static final String AEndSite="@xpath=//a[text()='A End Site'][@href='javascript:void(0);']";
		public static final String SelectAccessTechnology="@xpath=//table[@summary='Select Access Technology']//tr//td[text()='";
		public static final String ThirdPartyAccessProviderSelect="@xpath=//table[@summary='Select Third Party Access Provider']/tbody/tr[2]";
		public static final String PresentationInterfaceConnector="@xpath=//table[@summary='Select Presentation Interface-Connector Type']//tr//td[text()='";
		public static final String InstallationTimeLink="@xpath=//a[text()='Installation Time'][@href='javascript:void(0);']";
		public static final String CustomerSitePopStatusselect="@xpath=(//div[text()='Customer Site Pop Status']/following::select)[1]";
		public static final String Buildingtyperfs="@xpath=(//div[text()='Building Type']/following::select)[1]";
		public static final String AccessPortItem="@xpath=(//select[@class='siebui-ctrl-select '])[2]";
		public static final String PortRole="@xpath=//a[contains(@name,'Port Role')]/following-sibling::*//select";
		public static final String InstallTimeBEnd="@xpath=//a[contains(@name,'Install Time')]//following-sibling::*//select";
		public static final String DoneEthernetConnection="@xpath=//button[text()='Done']";

	 }
	
	public static class  BEndDropdownSelection 
	 { 
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
//		public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020

	 }
	public static class   BEndInputEnter
	 { 
		public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";
	 }
	 public static class  ASiteCustomize
	 {      public static final String ThirdPartyAccessProviderSelect="@xpath=//table[@summary='Select Third Party Access Provider']/tbody/tr[2]";
		public static final String Buildingtyperfs="@xpath=(//div[text()='Building Type']/following::select)[1]";

		 public static final String AEndSite="@xpath=//a[text()='B End Site'][@href='javascript:void(0);']";
		 public static final String CustomizeButton="@xpath=//button[@id='btnMiddleCustom']";
			public static final String Coverage="@xpath=//a[contains(@name,'Coverage')]//following-sibling::*//select";
			public static final String ServiceBandwidth="@xpath=//a[contains(@name,'Service Bandwidth')]//following-sibling::*//select";
			public static final String AEndResilienceOption="@xpath=//a[contains(@name,'B End Resilience Option')]//following-sibling::*//select";
			public static final String BEndResilienceOption="@xpath=//a[contains(@name,'B End Resilience Option')]//following-sibling::*//select";
			public static final String subseaWorkerpath="@xpath=//a[contains(@name,'Sub Sea Cable System (Worker Path)')]//following-sibling::*//select";
			public static final String subseaprotectedpath="@xpath=//a[contains(@name,'Sub Sea Cable System (Protected Path)')]//following-sibling::*//select";
			public static final String Connectionlink="@xpath=//a[contains(text(),'Connection')][@href='javascript:void(0);']";
			public static final String AccessTechnologySearch="@xpath=(//div[text()='Access Technology']/following::span[@class='siebui-icon-icon_pick ']/img)[1]";
			public static final String SelectAccessTechnology1="@xpath=//table[@summary='Select Access Technology']//tr//td[text()='3rd Party Leased Line']";
			public static final String AccessTechnologySubmit="@xpath=//button[@title='Select Access Technology:OK']";
			public static final String ThirdPartyAccessProviderSearch="@xpath=//div[@class='cxEdit siebui-ecfg-editfield']//span[@class='siebui-icon-icon_pick']/img";
			public static final String ThirdPartyAccessProviderSubmit="@xpath=//button[@title='Select Third Party Access Provider:OK']";
			public static final String ThirdPartyConnecRef="@xpath=//a[contains(@name,'3rd Party Connection Reference')]//following-sibling::*//input";
			public static final String ThirdPartySlaTierValue="@xpath=//option[text()='Tier 1']";
			public static final String ThirdPartySlaTier="@xpath=//a[contains(@name,'Third Party SLA Tier')]//following-sibling::*//select";
			public static final String CPEInformationLink="@xpath=//a[text()='CPE Information'][@href='javascript:void(0);']";
			public static final String CabinetType="@xpath=//a[contains(@name,'Cabinet Type')]/following-sibling::*//select";
			public static final String CabinetId="@xpath=//a[contains(@name,'Cabinet ID')]/following-sibling::*//input";
			public static final String ShelfID="@xpath=//a[contains(@name,'Shelf ID')]/following-sibling::*//input";
			public static final String AccessPortLink="@xpath=//table[@class='siebui-ecfg-module-content']//a[text()='Access Port']";
			public static final String PresentationInterfaceSearch="//table[@summary='Select Presentation Interface-Connector Type']//tr//td[text()='";
			public static final String SubmitPresentationInterface="@xpath=//button[@data-display='OK']";
			//public static final String PortRole="@xpath=//a[contains(@name,'Port Role')]/following-sibling::*//select";
			public static final String SlotID="@xpath=//a[contains(@name,'Slot ID')]/following-sibling::*//input";
			public static final String PhysicalPortID="@xpath=//span[text()='Physical Port ID']/..//input";
			public static final String VLANTaggingMode="@xpath=//a[contains(@name,'VLAN Tagging Mode')]/following-sibling::*//select";
			public static final String Vlannew="@xpath=//a[text()='VLAN'][@href='javascript:void(0);']";
			public static final String VlanTagId="@xpath=//a[contains(@name,'VLAN Tag ID')]/following-sibling::*//input";
			public static final String Terminationinformation="@xpath=//a[text()='Termination Information'][@href='javascript:void(0);']";
			public static final String InstallTime="@xpath=//span[text()='Install Time']/..//input";
			public static final String InstallationTimeLink="@xpath=//a[text()='Installation Time'][@href='javascript:void(0);']";
			public static final String CustomerSitePopStatusselect="@xpath=(//div[text()='Customer Site Pop Status']/following::select)[1]";
			public static final String PortRole="@xpath=//a[contains(@name,'Port Role')]/following-sibling::*//select";
	 }
	 
	 public static class Carnor_getReferenceNo1 
	 { 
		public static final String CircuitReferenceAccess="@xpath=//button[@id='colt-circuitref-button']";
		public static final String CircuitReferenceValue="@xpath=//input[@class='colt-attribute-input siebui-ctrl-input colt-popup-link-ctrl']"; 
		public static final String CircuitReference="@xpath=//span[text()='Circuit Reference']/..//input";
		public static final String NetworkTopology="@xpath=//span[text()='Network Topology']/..//input/following-sibling::span";
	 }
	 public static class CompletedValidationforCancel
	 { 
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String CustOrder="@xpath=//input[@aria-labelledby='Order_Number_Label']"; 
		public static final String CustOrderGo="@xpath=//button[contains(@class,'gotoview')]";
		public static final String ClickSeibelOrder="@xpath=//a[@name='Order Number']";
		public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]"; 
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
//		public static final String SelectCompleted="@xpath=//li[text()='Completed']";
		public static final String SelectCompleted="@xpath=//li/*[text()='Completed']"; // updated IP2020
		public static final String OrderComplete="@xpath=//button[text()='Yes']";
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
	 }
	 public static class  CommercialValidation
	 { 
		public static final String OrderStatus="@xpath=//input[@aria-label='Order Status']";
	 }
	 public static class  closePopUp
	 { 
	//	public static final String PopClose="@xpath=//div[contains(@style,'block')]//button[@title='Close']";
		public static final String PopClose="@xpath=//*[not(contains(@style,'display'))]//button[@title='Close']"; // Updated IP2020

	 }
	 public static class ClickSection{
		 
			public static final String SectionSelection="@xpath=//span[text()='Value']/preceding-sibling::span[@class='colt-component-check-available fa fa-circle-o']";

	 }
	 public static class Check1{
		 
			public static final String ClickLink="@xpath=//a[text()='Value']";
		 public static final String CustomerOrderInput="@xpath=//input[@aria-labelledby='Order_Number_Label']";
		 public static final String CustomerOrderSearch="@xpath=//button[@title='Search:Go']";

	 }
	 public static class Check{
		 
			public static final String ServiceOrderTab="@xpath=//a[text()='Service Order']";
		    public static final String InputServiceOrder="@xpath=//input[@aria-label='Starting with']";
		    public static final String ServiceOrderGo="@xpath=(//button[@aria-label='Service Order List:Go'])[2]";
		    public static final String ServiceOrderClickOn="@xpath=//a[contains(@name,'Order Num')]";
		    public static final String MyOrder="@xpath=//a[text()='My Orders']";
	 }
	 public static class CheckServiceTab{
		 
			public static final String ModifyButtonClick="@xpath=//span[text()='Modify']";
		    public static final String ModifyBtn="@xpath=//button[@aria-label='Service Orders:Modify']";
		    public static final String OpportunityNo="@xpath=//*[(@aria-labelledby='Opportunity_Number_Label')]";
		    public static final String RequestReceivedDate="@xpath=///*[(@aria-label='Request Received Date')]";
		    public static final String ModifyOrderNumber="@xpath=//a[@name='Order Number']";
		    public static final String ServiceOrderClickOn="@xpath=//a[contains(@name,'Order Num')]";
		    public static final String OrderSubTypeSearch="@xpath=//div[@class='mceGridField siebui-value mceField']/span[@aria-label='Multiple Selection Field']";
		    public static final String AddOrderSubType="@xpath=//button[@title='Service Order Sub Type:New']";
		    public static final String SubmitSubOrderType="@xpath=//button[@title='Service Order Sub Type:OK']";
		    public static final String InputOrderSubType="@xpath=//input[@name='COLT_Order_Item_Sub_Type']";
		    public static final String MaintenancePartySearch="@xpath=///input[@aria-label='Maintenance Party']/..//span";
		    public static final String MaintenancePartyPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
	//	    public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])"; 
		    public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])"; // updated IP2020

		    public static final String InputAccountStatus="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		    public static final String AccountStatusSearch="@xpath=.siebui-popup-filter span +span+span+span+span button";
		    public static final String AccountStatusSubmit="@xpath=//button[@aria-label='Pick Account:OK']";

	 }
	 public static class createCustomerOrder{
			public static final String DeliveryChannel="@xpath=//*[(@aria-labelledby='COLT_Delivery_Channel_Label')]";
			public static final String OpportunityNo="@xpath=//*[(@aria-labelledby='Opportunity_Number_Label')]";
		    public static final String RequestReceivedDate="@xpath=//*[(@aria-label='Request Received Date')]";
		    public static final String OrderingPartySearch="@xpath=//input[@aria-labelledby='Ordering_Party_Label']/..//span";
	//	    public static final String PopupCustomDropdown="@xpath=//div[contains(@style,'block')]//span[@data-allowdblclick='true']";
		    public static final String PopupCustomDropdown="@xpath=//*[not(contains(@style,'display'))]//span[@data-allowdblclick='true']"; // Updated IP2020

		    public static final String PartyName="@xpath=//*[@class='ui-menu-item'][text()='Party Name']";
		    public static final String InputPartyname="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		    public static final String PickAccntOk="@xpath=//button[@title='Pick Account:OK']";
		    public static final String OrderingPartyAddrSearch="@xpath=//input[@aria-labelledby='COLT_Ordering_Account_Address_Name_Label']/..//span";
		    public static final String PartyAddr="@xpath=//*[@class='ui-menu-item'][text()='Street Name']";
		    public static final String InputPartyAddr="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		    public static final String PickAddrSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Address:Go']";
		    public static final String PickAddrSubmit1="@xpath=//button[@title='Pick Address:OK']";
		    public static final String OrderingPartyContactSearch="@xpath=//input[@aria-labelledby='COLT_Ordering_Contact_Full_Name_Label']/..//span";
		//    public static final String LastName="@xpath=//li[text()='Last Name']";
		    public static final String LastName="@xpath=//li/*[text()='Last Name']"; // Updated IP2020

		    public static final String InputFirstname="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']";
		    public static final String PickContactSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Contact:Go']";
		    public static final String FirstnameSubmit="@xpath=//button[@title='Pick Contact:OK']";
		    public static final String SalesChannel="@xpath=//*[(@aria-labelledby='COLT_Sales_Channel_Label')]";
		    public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]";

	 }
	 
	 public static class XNG{
		 public static final String CircuitPathsLink ="@css=table tr:nth-child(2) > td:nth-child(5) > a";
		 public static final String CircuitId ="@css=table tbody tr td:nth-child(2) input:first-child[name=pathName]";
		 public static final String SeachButton ="@css=body table:nth-child(3) > tbody > tr > td:nth-child(2) > input[type=submit]";
		 public static final String CircuitReferenceNumber ="@css='Value'";
		 //public static final String CircuitReferenceNumber ="@xpath=//a[contains(text(),'Value')";
			 
		 
	 }
	 public static class SAP{
		 public static final String CpnNumber="(//table[@class='ui-jqgrid-btable'])[2]";
			 
		 
	 }
}