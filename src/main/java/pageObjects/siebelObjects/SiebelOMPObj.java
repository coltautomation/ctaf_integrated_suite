package pageObjects.siebelObjects;

public class SiebelOMPObj {

	
	public static class omp
	 { 
		public static final String ompusername="@xpath=//input[@id='userId']";
		public static final String omppassword="@xpath=//input[@id='password']";
		public static final String omplogin="@xpath=//button[@type='submit']";
		public static final String InputOrderNumber="@xpath=//input[@id='orderNumber']";
		public static final String SearchServiceButton="@xpath=//button[@id='searchSubmit']";
		public static final String ClickOrderReferance="@xpath=//tr[@role='row']/parent::*//a[text()='Value']";
		public static final String allorders="@xpath=//*[@id='myTab']/li[1]/a";
		public static final String CloseVideoPopup="@xpath=//div[@class='modal-header']//button[@class='close marginB15']";
		public static final String CloseVideoPopupNew="@xpath=//div[@id='promotional-popup']//button[@class='close marginB15']";
		public static final String OrderNumberBold="@xpath=//span[@class='col-Dcyan']//b";
		public static final String ExpandAllButton="@xpath=//div[@class='expandBlk']/parent::a";
		public static final String ProductName="@xpath=//*[@id='collapseZero']/div[1]/div[1]/span/h4";
		public static final String OrderStatus="@xpath=(//h4[@class='ng-binding'])[2]";
		public static final String OrderType="@xpath=//label[contains(text(),'Order Type ')]/parent::*/span";
		public static final String OCNNumber="@xpath=//label[contains(text(),'OCN')]/parent::*/span";
		public static final String BCNNumber="@xpath=//label[contains(text(),'BCN')]/parent::*/span";
		public static final String SendButton="@xpath=//button[text()='Send anyway']";
	 }
	
}
