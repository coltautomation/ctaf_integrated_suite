package pageObjects.siebelObjects;

public class SiebelPremiseMasterHelperObj {
	

	
	public static class PremiseMasterHelper
	 { 
		public static final String searchInput="@xpath=//span[text()='Value']/parent::div/input";
		public static final String searchDropdown="@xpath=//span[text()='Value']/following-sibling::span";
	//	public static final String siteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String siteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
	
		public static final String searchButton="@xpath=//button[@class='colt-siteselection-button colt-primary-btn']";
		public static final String addressNotFound="@xpath=//i[@class='fa fa-search']/parent::button";
		public static final String addAddress="@xpath=//i[@class='fa fa-plus']/parent::button";
		public static final String addBuilding="@xpath=//button[text()='Building Not Found? Create a new Building']";
		public static final String saveAddress="@xpath=//i[@class='fa fa-check']/parent::button";
		public static final String buildingName="@xpath=//input[@name='buildingName']";
		public static final String roomName="@xpath=//input[@name='roomName']";
		public static final String floor="@xpath=//input[@name='floor']";
		public static final String createSite="@xpath=//td[@class='onlineData']//a";
		public static final String searchResultId="@xpath=//table[@id='DomainTable']//tr//td//a[@class='overlay'][1]";
		public static final String editLink="@xpath=//td[@class='onlineData']//a";
		public static final String editBuilding="@xpath=//a[@class='overlay'][text()='Edit']";
		public static final String buildingType="@xpath=//select[@id='buildingTypeSelectID']";
		public static final String buildingStatus="@xpath=//select[@id='buildingStatusSelectID']";
		public static final String accessType="@xpath=//select[@id='accessTypeSelectID']";
		public static final String buildingCategory="@xpath=//select[@id='buildingCategory']";
		public static final String schema="@xpath=//select[@id='cnmtSchema']";
		public static final String streetName="@xpath=//select[@id='CNMTStreetName']";
		public static final String settlementName="@xpath=//input[@id='building.cnmtSettlementName']";
		public static final String siteType="@xpath=//select[@id='siteTypeId']";
		public static final String floorCreateSite="@xpath=//select[@id='floorDropdown']";
		public static final String cityId="@xpath=//select[@id='manCityId']";
		public static final String updateSite="@xpath=//input[@name='updateSiteInfoButton']";
		public static final String updateSiteId="@xpath=//table[@id='DomainTable']//tbody//tr[1]/td[1]";
		public static final String siteOCN="@xpath=//textarea[@id='selectedSiteOCN']";
		public static final String image="@xpath=//a[@href='/PremiseMaster/interimFilter?value=KVH']";
		public static final String search="@xpath=//a[text()='Search']";
		public static final String siteStatus="@xpath=//select[@id='searchType']";
		public static final String status="@xpath=//select[@id='siteStatusId']";
		public static final String siteValue="@xpath=//option[@id='Site']";
		public static final String searchDropDown="@xpath=//option[text()='Value']";
		public static final String searchValue="@xpath=//input[@id='searchTextBox']";
		public static final String searchType="@xpath=//select[@id='searchType']";
		public static final String searchSite="@xpath//td/input[@name='Button2']";
		public static final String searchSiteSubmit="@xpath=//input[@type='submit']";
		public static final String roomId="@xpath=//input[@id='roomId']";
		public static final String submit="@xpath=//input[@name='submit']";
		public static final String buildingUpdateMessage="@xpath=//td[@class='form formtext']";
		public static final String buildingId="@xpath=(//td[@class='form formtext']//span//strong)[2]";
		public static final String country="@xpath=//select[@id='country']";
		public static final String city="@xpath=//Input[@id='cityNameEnglish']";
		public static final String houseNo="@xpath=//Input[@id='houseNumber']";
		public static final String searchlocationBuildingName="@xpath=//Input[@id='buildingName']";
		public static final String searchlocationStreetName="@xpath=//Input[@id='streetName']";
		public static final String postalCode="@xpath=//Input[@id='postalCodeId']";
		public static final String state="@xpath=//Input[@id='province']";
		public static final String searchSiteResultId="@xpath=//table[@id='DomainTable']//tr//td//a[@class='overlay'][1]";
		public static final String siteId="@xpath=(//table[@id='DomainTable']//tr//td)[1]";
		public static final String searchSiteBuildingId="@xpath=//input[@id='building.buildingId']";
				//input[@id='buildingId']/preceding-sibling::input";
		public static final String hubSiteA="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";
		public static final String hubSiteB="@xpath=//div[@id='colt-site-right']//span[@title='Select a site']";
		public static final String searchLocationSearchButton="@xpath=//Input[@id='search']";
		public static final String searchAgain="@xpath=//Input[@id='searchAgain']";
		public static final String SearchAddressRowSelection ="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
		public static final String PickAddress="@xpath=//button[text()='Pick Address']";
		public static final String PickBuilding="@xpath=//button[text()='Pick Building']";
		public static final String PickSite="@xpath=//button[text()='Pick Site']";
		public static final String username="@xpath=//*[@title='User Name']";
		public static final String password="@xpath=//*[@title='Password']";
		public static final String login="@xpath=//*[@name='submit']";
		public static final String logout="@xpath=//a[text()='Logout']";
	 }
}