package pageObjects.siebelObjects;

public class SiebelServiceOrderObj {
	
	
	public static class ServiceOrder
	 { 
		public static final String SeriviceOrderTab="@xpath=//a[text()='Service Order']";
		public static final String InputOrderNoTxb="@xpath=//input[@aria-labelledby='QuerySrchSpec_Label']";
		public static final String ServiceOrderGo="@xpath=(//button[@aria-label='Service Order List:Go'])[2]";
		
	 }

}
