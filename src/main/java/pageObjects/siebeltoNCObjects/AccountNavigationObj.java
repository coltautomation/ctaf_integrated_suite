package pageObjects.siebeltoNCObjects;

public class AccountNavigationObj 
{
	public static class Account
	{
		public static final String Topnavegation="@xpath=//*[text()='Navigation']";
		public static final String Submenu="@xpath=//span[text()='Documents']/parent::*";
		public static final String Customertype="@xpath=//span[text()='COLT OM Customers']/parent::*";
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String Customername="@xpath=//span[text()='ZZZZ UK TEST']/parent::*";
		public static final String SiebelAccounts="@xpath=//*[text()='Accounts']/parent::*";
		public static final String SiebelOCN="@xpath=//*[text()='Account Number(OCN)']/parent::*/following-sibling::*//input";
		public static final String SiebelAccountGo="@xpath=//*[text()='Go']/parent::*";
		public static final String SiebelCustomerName="@xpath//*[contains(@title,'ZZZZ ES TEST12')]";
		
		public static final String Fastsearch="@xpath=//input[@id='fast_search_val']";
		public static final String ServiceInventorySearch="@xpath=//span[text()='COLT Service Inventory Search Folder']/parent::*";
		public static final String AccountSearch="@xpath=//span[text()='COLT Customer Account Search Profile']";
		public static final String Ocnsearchcriterion="@xpath=//input[@tooltip='The input field for OCN search criterion']";
		public static final String searchButton="@xpath=//div[@id='w_searchButton']//a";
		public static final String AccountNamelink="@xpath=//td/a[@class='refImage commonReferenceLink']/span";
	}
}

