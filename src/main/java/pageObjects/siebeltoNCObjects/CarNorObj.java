package pageObjects.siebeltoNCObjects;

public class CarNorObj {
	public static class carNor{
		public static final String OrderNumber="@xpath=//td[@id='vv_-1']/span";
		public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String FilterSelectName="@xpath=//td/select[@class='inputs']";
		public static final String FilterInputValue="@xpath=//div/input[@class='inputs']";
		public static final String ApplyButton="@xpath=//*[@class='FilterButtons'] //*[text()='Apply']";
		public static final String LinkforOrder="@xpath=//a[contains(text(),'Order #')]";
		public static final String NewCompositeOrder="@xpath=//a[text()='New Composite Order']";
		public static final String Ordernumber="@xpath=//td/a[text()='Order Number']/parent::*/following-sibling::*//input";
		public static final String OrderDescription="@xpath=//*[@name='common_descr']";
		public static final String Update="@xpath=//a[text()='Update']";
		public static final String OrderTab="@xpath=//a[text()='Orders']";
		public static final String AddonOrderTab="@xpath=//a[text()='Add']";
		public static final String Productifram="@xpath=//iframe[@id='linkage']";
		public static final String EthernetProductCheckBox="@xpath=//a[text()='Ethernet Connection Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String Addbutton="@xpath=//button[text()='Add']";
		public static final String UnderlyningOrder="@xpath=//span[contains(text(),'Ethernet Connection')]/parent::a";
		public static final String UnderlyningHubOrder="@xpath=//span[contains(text(),'Hub Product Order')]/parent::a";
		public static final String NCSID="@xpath=//td/a[text()='NC Service ID']/parent::*/following-sibling::*[1]";
		public static final String ProductID="@xpath=//td/a[text()='Product ID']/parent::*/following-sibling::*";
		public static final String Edit="@xpath=//a[text()='Edit']";
		public static final String NetworkReference="@xpath=//td/a[text()='Network Reference']/parent::*/following-sibling::*//input";
		public static final String OrderSystemName="@xpath=//td/a[text()='Order System']/parent::*/following-sibling::*//select";
		public static final String Orderreferencenumber="@xpath=//td/a[text()='Order System Service ID']/parent::*/following-sibling::*//input";
		public static final String Topology="@xpath=//td/a[text()='Topology']/parent::*/following-sibling::*//select";
		public static final String CommercialProductName="@xpath=//td/a[text()='Commercial Product Name']/parent::*/following-sibling::*//input";
		public static final String CircuitCategory="@xpath=//td/a[text()='Circuit Category']/parent::*/following-sibling::*//select";
		public static final String ServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//input[@type='text']";
		public static final String RetainedNCSerId="@xpath=//td/a[text()='Retained NC Service ID']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CarNorProductId="@xpath=//td/a[text()='CARNOR Product ID']/parent::*/following-sibling::*//input[@type='text']";
		public static final String ProductInstTab="@xpath=//a[text()='Product Instances']";
		public static final String ProductInstNumber="@xpath=//span[contains(text(),'Ethernet Connection Product Instance')]";
		public static final String HubProductInstNumber="@xpath=//span[contains(text(),'Hub Product Instance')]";
		public static final String CreateDisconnectProductOrder="@xpath=//a[text()='Create Disconnect Product Order']";
		public static final String DisconnectOrder="@xpath=//span[contains(text(),'Disconnect Ethernet Connection Product Order')]/parent::a";
		public static final String HardCeaseDelay="@xpath=//*[text()='Hard Cease Delay In Days']/parent::*/following-sibling::*//input[@type='text']";
		public static final String ColtPromiseMonth="@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][1]";
		public static final String ColtPromiseDate="@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][2]";
		public static final String ColtPromiseYear="@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][3]";
		public static final String DisconnectSuborder="@xpath=//a/span[contains(text(),'Disconnect Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Scheduler="@xpath=//a[text()='Scheduler']";
		
		//public static final String GeneralInformation="@xpath=
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";
		public static final String AddFeaturelink="@xpath=//a[text()='Add Feature']";
		public static final String TypeofFeature="@xpath=//a[text()='Add Feature']/following-sibling::*//textarea";
		public static final String SelectFeature="@xpath=//input[@value='Select']";
		
		public static final String Suborder="@xpath=//a/span[contains(text(),'New Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String HubSuborder="@xpath=//a/span[contains(text(),'Hub Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Decompose="@xpath=//a[text()='Decompose']";
		public static final String NewEthernetConnProduct="@xpath=(//a/span[contains(text(),'New Ethernet Connection Product')])/parent::*";
		public static final String AddHub="@xpath=//a[text()='Add Hub']";
		public static final String TypeofHub="@xpath=//a[text()='Add Hub']/following-sibling::*//textarea";
		public static final String AddHubProdOrder="@xpath=//img[@alt='New/Modify Hub Product Order']/following-sibling::div[1]";
		public static final String NewHubSiteProductend="@xpath=(//a/span[contains(text(),'New Hub Site Product')])/parent::*";
		public static final String NewEndSiteProductAend="@xpath=//td[text()='Entering']/parent::*/td/a";
		public static final String NewEndSiteProductBend="@xpath=(//a/span[contains(text(),'New End Site Product')])[2]/parent::*";
		
		//public static final String SiteProductDetail="@xpath=
		public static final String ResilienceOption="@xpath=//td/a[text()='Resilience Option']/parent::*/following-sibling::*//select";
		public static final String AccessTechnolgy="@xpath=//td/a[text()='Access Technology']/parent::*/following-sibling::*//select";
		public static final String AccessType="@xpath=//td/a[text()='Access Type']/parent::*/following-sibling::*//select";
		public static final String SiteEnd="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*//select";
		public static final String SiteID="@xpath=//td/a[text()='Site ID']/parent::*/following-sibling::*//input";
		
		//public static final String Accessport="@xpath=
		public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
		public static final String PresentConnectType="@xpath=//td/a[text()='Presentation Connector Type']/parent::*/following-sibling::*//select";
		public static final String AccessportRole="@xpath=//td/a[text()='Port Role']/parent::*/following-sibling::*//select";
		public static final String VlanTaggingMode="@xpath=//td/a[text()='VLAN Tagging Mode']/parent::*/following-sibling::*//select";
		
		//public static final String CPE="@xpath=
		public static final String CPElink="@xpath=//span[text()='CPE Information']/parent::*";
		public static final String CabinateType="@xpath=//td/a[text()='Cabinet Type']/parent::*/following-sibling::*//select";
		public static final String CabinateID="@xpath=//td/a[text()='Cabinet ID']/parent::*/following-sibling::*//input";
		
		//public static final String VLAN="@xpath=
		public static final String VLANLink="@xpath=//span[text()='VLAN']/parent::*";
		public static final String VlanTagId="@xpath=//td/a[text()='VLAN Tag ID']/parent::*/following-sibling::*//input";
		public static final String Ethertype="@xpath=(//*[text()[contains(.,'Ethertype')]]/parent::*/following-sibling::*//input)[3]";
		
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";
		public static final String TransportCfs="@xpath=//a[starts-with(@tooltip,'New Transport CFS Order')]";
		//public static final String Tasks="@xpath=
		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String TaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[New]')]";
		public static final String Workitems="@xpath=//a[text()='Work Items']";
		public static final String Errors="@xpath=//a[text()='Errors']";
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";
		public static final String AccessPort="@xpath=//td/a[text()='Access Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AccessNetworkElement="@xpath=//td/a[text()='Access Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort="@xpath=//*[text()='CPE NNI Port 1']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort2="@xpath=//*[text()='CPE NNI Port 2']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PENetworkElement="@xpath=//*[text()='PE Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PePort="@xpath=//*[text()='PE Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String VCXController="@xpath=//*[text()='VCX Controller']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Beacon="@xpath=//*[text()='Beacon']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPETrunkPort="@xpath=//*[text()='CPE Trunk Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OAMProfile="@xpath=//*[text()='OAM Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String SerialTaskName="@xpath=//td/a[text()='Name']/parent::*/following-sibling::*";
		public static final String UpdateAntSerialNumber="@xpath=//a[text()='Update ANT Serial Number']";
		public static final String UpdateSerialNumber="@xpath=//a[text()='Update Serial Number']";
		public static final String EthernetLinkTransportRFS_1="@xpath=(//span[text()='Ethernet Link Transport RFS']/parent::*)[1]";
		public static final String EthernetLinkTransportRFS_2="@xpath=(//span[text()='Ethernet Link Transport RFS']/parent::*)[2]";
		public static final String LegacyCpeProfile="@xpath=//*[text()='Legacy CPE Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeOamLevel="@xpath=//*[text()='Legacy CPE OAM Level']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeSupportsCFM="@xpath=//*[text()='Legacy CPE supports CFM']/parent::*/following-sibling::*//select";
		public static final String Complete="@xpath=//a[@tooltip='Complete']";
		public static final String TransComplete="@xpath=//a[text()='Complete']";
		public static final String WorkItemSelect="@xpath=//a/span[contains(text(),'Reserve Access Resources')]/parent::*/parent::*/parent::*/td//input";
		public static final String View="@xpath=//a[text()='View']";
		
		//Newly created by me #Rohit
		public static final String arrorder = "//a/span[contains(text(),'";
		public static final String arrorder1 = "')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]";
		public static final String arrorder2 = "')]/parent::*";
		public static final String arrorder3 = "')]/parent::*/parent::*/parent::*/td//input";
		
	}

}
