package pageObjects.siebeltoNCObjects;

public class CeaseOrderObj {
	
	public static class CeaseOrder
	{
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String FilterSelectName="@xpath=//*[@class='inputs']";
		public static final String FilterInputValue="@xpath=//*[@name='inp']";
		public static final String ApplyButton="@xpath=//a[text()='Apply']";
		public static final String ProductInstNumber="@xpath=//span[contains(text(),'Ethernet Connection Product Instance')]";
		public static final String IPAccessProductInstNumber="@xpath=//span[contains(text(),'IP Access Product Instance')]";
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String ProductInstTab="@xpath=//a[text()='Product Instances']";
		public static final String CreateDisconnectProductOrder="@xpath=//a[text()='Create Disconnect Product Order']";
		public static final String LinkforOrder="@xpath=//a[contains(text(),'Order #')]";
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";
		public static final String EditDesc="@xpath=//*[(@aria-label='Control+e')]";
		public static final String OrderDescription="@xpath=//*[@name='common_descr']";
		public static final String Apply="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*//a[text()='Apply']";
		public static final String InstNameFiltering="//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String InstanceStatus="//a[contains(text(),'Instance Status')]/parent::*/following-sibling::*";
		public static final String OrderNumber="@xpath=//td[@id='vv_-1']/span";
		public static final String Decompose="@xpath=//a[text()='Decompose']";
		public static final String HubProductInstNumber="@xpath=//span[contains(text(),'Hub Product Instance')]";
		public static final String Edit="@xpath=//a[text()='Edit']";
		public static final String Update="@xpath=//a[text()='Update']";
		public static final String DisconnectOrder="@xpath=//span[contains(text(),'Disconnect Ethernet Connection Product Order')]/parent::a";
		public static final String DisconnectHubOrder="@xpath=//span[contains(text(),'Disconnect Hub Product Order')]/parent::a";
		public static final String DisconnectIpAccessOrder="@xpath=//span[contains(text(),'Disconnect IP Access Product Order')]/parent::a";
		public static final String HardCeaseDelay="@xpath=//*[text()='Hard Cease Delay In Days']/parent::*/following-sibling::*//input[@type='text']";
		public static final String ColtPromiseMonth="@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][1]";
		public static final String ColtPromiseDate="@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][2]";
		public static final String ColtPromiseYear="@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][3]";
		public static final String OrderSystemServiceID="@xpath=//*[text()='Order System Service ID']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OrderTab="@xpath=//a[text()='Orders']";
		public static final String EthernetProductCheckBox="@xpath=//a[text()='Ethernet Connection Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String Suborder="@xpath=//a/span[contains(text(),'Disconnect Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String HubSubOrder="@xpath=//a/span[contains(text(),'Disconnect Hub Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String SelectDisconnectIpAccessOrder="@xpath=//a/span[contains(text(),'Disconnect IP Access Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String EndsiteName="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*//input[@type='text']";
		public static final String EndsiteproductCheck="@xpath=//input[@type='checkbox']";
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";
		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String Workitems="@xpath=//a[text()='Work Items']";
		public static final String processcompleted="@xpath=//td[contains(text(),'Process Completed')]";
		public static final String processingCanceled="@xpath=//td[contains(text(),'Processing Cancelled')]";
		public static final String Scheduler="@xpath=//a[text()='Scheduler']";
		public static final String Errors="@xpath=//a[text()='Errors']";
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";
		public static final String TaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[New]')]";
		public static final String ReleaseLanRange="@xpath=//*[text()='Release LAN Range']/parent::*/following-sibling::*//select";
		public static final String DisconnectTaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[Disconnect]')]";
		public static final String Complete="@xpath=//a[@tooltip='Complete']";
	}
}
