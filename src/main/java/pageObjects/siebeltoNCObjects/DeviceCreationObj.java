package pageObjects.siebeltoNCObjects;

public class DeviceCreationObj {
	public static class DeviceCreation
	{
		public static final String Fastsearch="@xpath=//input[@id='fast_search_val']";
		public static final String ResourceInventorySearch="@xpath=//span[text()='COLT Resource Inventory Search Folder']/parent::*";
		public static final String DeviceSearch="@xpath=//span[text()='COLT Device search profile']/parent::*";
		public static final String DeviceNameCriteria="@xpath=//input[@tooltip='The input field for Device Name search criterion']";
		public static final String SearchButton="@xpath=//div[@id='w_searchButton']//a";
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String AddNewDevice="@xpath=//a[text()='Add new device']";
		public static final String NetworkElements="@xpath=//a[text()='Network Elements']";
		public static final String NewNetwkElement="@xpath=//a[text()='New Network Element']";
		public static final String AddFromTemplate="@xpath=//a[text()='Add from Template']";
		public static final String Search="@xpath=//a[text()='Search']";
		public static final String DeviceRole="@xpath=//td[text()='Device Role']/parent::*//select";
		public static final String OrderNumber="@xpath=//td[text()='Order Number']/parent::*//input";
		public static final String Site="@xpath=(//td[text()='Site']/parent::*//input)[3]";
		public static final String Template="@xpath=//td[text()='Template']/parent::*//select";
		public static final String CreateSlot="@xpath=//a[text()='Create']";
		public static final String SlotName="@xpath=//a[contains(text(),'CA')]";
		public static final String Slots="@xpath=//a[text()='Slots']";
		public static final String Ports="@xpath=//a[text()='Ports']";
		public static final String Slot1="@xpath=//a/span[contains(text(),'Slot 1')]/parent::*/parent::*/parent::*/td//input";
		public static final String SFPCombo0="@xpath=//a/span[contains(text(),'SFP Combo 0')]/parent::*/parent::*/parent::*/td//input";
		public static final String SFPCombo1="@xpath=//a/span[contains(text(),'SFP Combo 1')]/parent::*/parent::*/parent::*/td//input";
		public static final String SFPCombo8="@xpath=//a/span[contains(text(),'SFP Combo 8')]/parent::*/parent::*/parent::*/td//input";
		public static final String InsertCards="@xpath=//a[text()='Insert Card(s)']";
		public static final String InsertCardFromRepos="@xpath=//a[text()='Insert Card From Repository']";
		public static final String InsertCardFromTemplate="@xpath=//a[text()='Insert Card From Template']";
		public static final String L3cpeCard="@xpath=//a[text()='GLC-LH-SM']/parent::*/parent::*//input";
		public static final String Insert="@xpath=//a[text()='Insert']";
		public static final String Devices="@xpath=//a[text()='Devices']";
		public static final String GxDeviceName="@xpath=(//a[contains(text(),'CA')])[2]";
		public static final String DeviceName="@xpath=(//tr/td[text()='Accedian Networks']//parent::*//a)[1]";
		public static final String LinkforDevice="@xpath=//a[contains(text(),'CA')]";
		public static final String LinkforAntDevice="@xpath=//a[contains(text(),'SC')]";
		public static final String Title="@xpath=//td[text()='Title']/parent::*//input";
		public static final String L3cpeDeviceName="@xpath=(//tr/td[text()='Cisco Systems']//parent::*//a)[1]";
		public static final String LinkforL3cpeDevice="@xpath=//a[contains(text(),'IC')]";
	
	}
}



