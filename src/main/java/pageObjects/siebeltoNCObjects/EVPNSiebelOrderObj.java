package pageObjects.siebeltoNCObjects;

public class EVPNSiebelOrderObj {

	public static class EVPNSiebelOrder{
		
		public static final String Bespokecheckbox="@xpath=//input[@aria-label='Bespoke/Reference'][@type='checkbox']";
		public static final String Bespoketext="@xpath=//input[@aria-label='Bespoke/Reference'][@type='text']";
		public static final String ClickShowFullinfo="@xpath=//a[text()='Click to Show Full info']";
		public static final String SavePage="@xpath=//a[text()='Click to Show Full info']";
		public static final String Classofserviceaccess="@xpath=//span[text()='Class Of Service']/parent::div/span[1]";
		public static final String Classificationtypedrpdown="@xpath=(//span[text()='Classification Type']/parent::*//span)[3]";
		public static final String Classificationtypeinput="@xpath=//span[text()='Classification Type']/..//input";
		public static final String Closeevpnaccess="@xpath=//span[text()='Ethernet VPN Access']/parent::*/parent::*/parent::*/parent::*/parent::*//button";
		public static final String SaveButtondisp="@xpath=//div/a[text()='Save']";

		


	}
}
