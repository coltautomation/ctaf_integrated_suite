package pageObjects.siebeltoNCObjects;

public class EvpnOrderObj {
	
	public static class CompositOrders
	{
		

		public static final String NewCompositeOrder="@xpath=//a[text()='New Composite Order']";
		public static final String NcOrderNumber="@xpath=//td[@id='vv_-1']";
		public static final String OrderDescription="@xpath=//*[@name='common_descr']";
		public static final String Update="@xpath=//a[text()='Update']";
	
		public static final String OrderTab="@xpath=//a[text()='Orders']";
		public static final String AddonOrderTab="@xpath=//a[text()='Add']";
		public static final String EVPNetworkCheckBox="@xpath=//a[text()='Ethernet VPN Network Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String Addbutton="@xpath=//button[text()='Add']";
		public static final String UnderlyningEvpnnetworkOrder="@xpath=//span[contains(text(),'VPN Network Product Order')]/parent::a";
		public static final String EVPsubnetworkCheckBox="@xpath=//a[text()='Ethernet VPN Subnetwork Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String UnderlyningEvpnsubnetworkOrder="@xpath=//span[contains(text(),'VPN Subnetwork Product Order')]/parent::a";

		public static final String Edit="@xpath=//a[text()='Edit']";
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";
		public static final String EthernetProductCheckBox="@xpath=//a[text()='Ethernet Connection Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String EtherConnCheckBox="@xpath=//a/span[contains(text(),'New Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Decompose="@xpath=//a[text()='Decompose']";
		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String Workitems="@xpath=//a[text()='Work Items']";
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";
		public static final String OrderSystemName="@xpath=//td/a[text()='Order System']/parent::*/following-sibling::*//select";
		public static final String OrderSysSerId="@xpath=//td/a[text()='Order System Service ID']/parent::*/following-sibling::*//input";
		public static final String SiteOrderNumber="@xpath=//td/a[text()='Order Number']/parent::*/following-sibling::*//input";
		public static final String Ordersystemetworkid="@xpath=//td/a[text()='Order System Network ID']/parent::*/following-sibling::*//input";
		public static final String NetworkType="@xpath=//td/a[text()='Network Type']/parent::*/following-sibling::*//select";
		public static final String ClassOfService="@xpath=//td/a[text()='Class of Service']/parent::*/following-sibling::*//select";
		public static final String cosClassification="@xpath=//td/a[text()='COS Classification']/parent::*/following-sibling::*//select";

		public static final String LANtype="@xpath=//td/a[text()='LAN Type']/parent::*/following-sibling::*//select";
		public static final String Ordersystemsubnetworkid="@xpath=//td/a[text()='Order System Subnetwork ID']/parent::*/following-sibling::*//input";
		public static final String UnderlyningOrder="@xpath=//span[contains(text(),'Ethernet Connection')]/parent::a";
		public static final String NetworkReference="@xpath=//a[text()='Network Reference']/parent::*/following-sibling::*//input";
		public static final String EvpnLinkType="@xpath=//td/a[text()='E-VPN Link Type']/parent::*/following-sibling::*//select";
		public static final String CommercialProductName="@xpath=//td/a[text()='Commercial Product Name']/parent::*/following-sibling::*//input";
		public static final String Networktopologys="@xpath=//span[text()='Network Topology']/following-sibling::input";
		public static final String ServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Topology="@xpath=//td/a[text()='Topology']/parent::*/following-sibling::*//select";
		public static final String CircuitCategory="@xpath=//td/a[text()='Circuit Category']/parent::*/following-sibling::*//select";
		public static final String Fastsearch="@xpath=//input[@id='fast_search_val']";
		public static final String ServiceInventorySearch="@xpath=//span[text()='COLT Service Inventory Search Folder']/parent::*";
		public static final String ProductOrderSearch="@xpath=//span[text()='COLT Product Order Search Profile']/parent::*";
		public static final String ServiceIDsearchcriterion="@xpath=//input[@tooltip='The input field for NC Service ID search criterion']";
		public static final String searchButton="@xpath=//div/a[@id='searchButton']";
		public static final String OrderNolink="@xpath=//td[text()='Processing']/parent::tr//a[@class='commonReferenceLink']/span";
		public static final String NewEndSiteProductAend="@xpath=//td[text()='Entering']/parent::*/td/a";
		public static final String AEndResilienceOption="@xpath=//a[contains(@name,'A End Resilience Option')]//following-sibling::*//select";
		public static final String AccessTechnolgy="@xpath=//td/a[text()='Access Technology']/parent::*/following-sibling::*//select";
		public static final String AccessType="@xpath=//td/a[text()='Access Type']/parent::*/following-sibling::*//select";
		public static final String SiteEnd="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*//select";
		public static final String EvpnLinkUsage="@xpath=//td/a[text()='E-VPN Link Usage']/parent::*/following-sibling::*//select";
		public static final String BroadcastLimit="@xpath=//td/a[text()='Broadcast Limit']/parent::*/following-sibling::*//input";
		public static final String MultiCastLimit="@xpath=//td/a[text()='Multicast Limit']/parent::*/following-sibling::*//input";
		public static final String UnknownUnicastLimit="@xpath=//td/a[text()='Unknown Unicast Limit']/parent::*/following-sibling::*//input";
		public static final String Maxmaclimit="@xpath=//td/a[text()='Max MAC Limit']/parent::*/following-sibling::*//input";
		public static final String MaxMACLimitExceed="@xpath=//td/a[text()='Max MAC Limit Exceed']/parent::*/following-sibling::*//select";
		public static final String SiteID="@xpath=//td/a[text()='Site ID']/parent::*/following-sibling::*//input";
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";
		public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
		public static final String PresentConnectType="@xpath=//td/a[text()='Presentation Connector Type']/parent::*/following-sibling::*//select";
		public static final String AccessportRole="@xpath=//td/a[text()='Port Role']/parent::*/following-sibling::*//select";
		public static final String CabinetID="@xpath=//td/a[text()='Cabinet ID']/parent::*/following-sibling::*//input";
		public static final String CPElink="@xpath=//span[text()='CPE Information']/parent::*";
//		public static final String CabinetType="@xpath=//a[contains(text(),'Cabinet Type')]/following-sibling::*//select";
		public static final String CabinetType="@xpath=//td/a[contains(text(),'Cabinet Type')]/parent::*/following-sibling::*//select";
		public static final String ResilienceOption="@xpath=//td/a[text()='Resilience Option']/parent::*/following-sibling::*//select";
		public static final String Complete="@xpath=//a[@tooltip='Complete']";
		public static final String TransComplete="@xpath=//a[text()='Complete']";
		public static final String Serviceprofile="@xpath=//td/a[text()='Service Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PremiumCir="@xpath=//td/a[text()='Premium CIR %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String InternetCir="@xpath=//td/a[text()='Internet CIR %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String BMNormal="@xpath=//td/a[text()='BM Normal %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String BMPriority="@xpath=//td/a[text()='BM Priority %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OAMProfile="@xpath=//*[text()='OAM Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String peportdedicatedforStormControl="@xpath=//td/a[text()='PE Port/LAI is dedicated for Storm Control']/parent::*/following-sibling::*//select";
		public static final String ServiceInfoTab="@xpath=//a[text()='Service Information']";
		public static final String Asr="@xpath=//td/a[text()='ASR']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AsrGil="@xpath=//td/a[text()='ASR GIL']/parent::*/following-sibling::*//input[@type='text']";
		public static final String oammonitoringleg="@xpath=//*[text()='OAM Monitoring Leg']/parent::*/following-sibling::*//input[@type='text']";
		public static final String SerialTaskName="@xpath=//td/a[text()='Name']/parent::*/following-sibling::*[1]";
		public static final String UpdateAntSerialNumber="@xpath=//a[text()='Update ANT Serial Number']";
		public static final String UpdateSerialNumber="@xpath=//a[text()='Update Serial Number']";
		public static final String TaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[New]')]";
		public static final String AccessNetworkElement="@xpath=//td/a[text()='Access Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AccessPort="@xpath=//td/a[text()='Access Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort1="@xpath=//*[text()='CPE NNI Port 1']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort2="@xpath=//*[text()='CPE NNI Port 2']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PENetworkElement="@xpath=//*[text()='PE Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PePort="@xpath=//*[text()='PE Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OLONNIProfile="@xpath=//td/a[text()='OLO NNI Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OVCCircuit="@xpath=//td/a[text()='OVC Circuit']/parent::*/following-sibling::*//input[@type='text']";

	}
	


}
