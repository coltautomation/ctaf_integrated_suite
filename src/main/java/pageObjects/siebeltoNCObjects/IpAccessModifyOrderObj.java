package pageObjects.siebeltoNCObjects;

public class IpAccessModifyOrderObj 
{

	public static class IpAccessModifyOrder
	{
		public static final String Inputlayer3resillence="@xpath=//span[text()='Layer 3 Resilience']/..//input";
		public static final String InstallTimeDrpDwn="@xpath=(//span[text()='Install Time']/..//span)[3]";
		public static final String SerBandwidthDrpDwn="@xpath=(//span[text()='Service Bandwidth']/..//span)[3]";
		public static final String SerBandwidth="@xpath=//span[text()='Service Bandwidth']/..//input";
		public static final String SecondarySite="@xpath=//a[@class='colt-connection-button']";
		public static final String BackupBandwidth="@xpath=//span[text()='Backup Bandwidth']/..//input";
		public static final String PrimarySite="@xpath=//a[text()='Primary']";
		public static final String BackupBandwidthDrpDwn="@xpath=(//span[text()='Backup Bandwidth']/..//span)[3]";
		public static final String SaveButton="@xpath=//a[@class=\"colt-primary-btn colt-s-order-save-btn\"]";
		public static final String SiebRelatedVoipServiceDrpDwn="@xpath=(//span[text()='Related VoIP Service']/..//span)[3]";
		public static final String SiebRelatedVoipService="@xpath=//span[text()='Related VoIP Service']/..//input";
		public static final String ServiceGrpTab="@xpath=//a[text()='Service Group']";
		public static final String ServiceGrpNew="@xpath=//button[@title=\"Service Group:New\"]";
		public static final String ServGrpRef="@xpath=//input[@name=\"COLT_Service_Group_Reference\"]/..//span";
		public static final String AddServGrp="@xpath=//button[@title=\"Add/Pick Service Group:OK\"]";
		public static final String IpShowFullInfo="@xpath=//div/p[text()='IP Addressing']//following-sibling::*";
		public static final String SiebIpv4AddresTypeDrpDwn="@xpath=(//span[text()='IPv4 Addressing Type']/..//span)[3]";
		public static final String SiebIpv4Address="@xpath=//span[text()='IPv4 Network Address']/..//input";
		public static final String SiebIpv4Prefix="@xpath=//span[text()='IPv4 Prefix']/..//input";
		public static final String SecondCPELANInterfaceIPv4Address="@xpath=//span[text()='Second CPE LAN Interface IPv4 Address']/..//input[@value='-']";
		public static final String LANVRRPIPv4Address1="@xpath=//span[text()='LAN VRRP IPv4 Address 2']/..//input";
		public static final String SiebIPAccessClose="@xpath=//span[text()='IP Access']/following-sibling::*";
		public static final String ProviderIndependentIPv6="@xpath=//span[text()='Provider Independent IPv6']/parent::*/span[contains(@class,'check-available')]";
		public static final String SiebIpv6Address="@xpath=//span[text()='IPv6 Network Addresses']/..//input[@value='-']";
		public static final String SiebIpv6Prefix="@xpath=//span[text()='IPv6 Prefix']/..//input[@value='-']";
		public static final String LANVRRPIPv6Address1="@xpath=//span[text()='LAN VRRP IPv6 Address 2']/..//input";
		public static final String SecondCPELANInterfaceIPv6Address="@xpath=//span[text()='Second CPE LAN Interface IPv6 Address']/..//input[@value='-']";
		public static final String NextHopIP="@xpath=//span[text()='Next Hop IP']/..//input";
	    public static final String LANIPv4Remove="@xpath=//span[text()='Primary Range']/../input[@title='No']/parent::div/parent::div/parent::div//span[text()='Provider Aggregated IPv4']/parent::*//preceding-sibling::span";
	    public static final String LANIPv6Remove="@xpath=//span[text()='Primary Range']/../input[@title='No']/parent::div/parent::div/parent::div//span[text()='Provider Aggregated IPv6']/parent::*//preceding-sibling::span";
	}	
}
