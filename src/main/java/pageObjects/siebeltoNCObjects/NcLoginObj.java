package pageObjects.siebeltoNCObjects;

public class NcLoginObj {
	
	public static class Login
	{
		//NC
		public static final String Username="@id=user"; //Username
		public static final String Password="@id=pass"; //Password
		public static final String Loginbutton="@xpath=//*[@id='loginButton']";  // loginButton
		public static final String Userinfo="@xpath=//*[@id='gen_menu_6']";      // UserInfo
		public static final String skipwarning="@xpath=//a[text()='Continue']";  // SkipWarning
		
		//Siebel 
		public static final String SiebelUsername="@xpath=//*[@id='s_swepi_1']";     // SiebelUsername
		public static final String SiebelPassword="@id='s_swepi_2'";                 // SiebelPassword
		public static final String SiebelLoginbutton="@xpath=//*[@id='s_swepi_22']"; // SiebelLoginbutton
		public static final String SiebelUserinfo="@xpath=//*[@id='gen_menu_6']";    //SiebelUserinfo
		public static final String AccountSettingButton="@xpath=//li[@title='Settings']"; //AccountSettingButton
		public static final String logout="@xpath=//a[text()='Log Off']";
	}

}
