package pageObjects.siebeltoNCObjects;


public class OloModifyHelperObj {

	
	public static class OloModifyHelper{
		public static final String UndelyingModifiedOrder="//span[contains(text(),'Modify Ethernet Connection')]/parent::a";
		public static final String Edit="//a[text()='Edit']";
		public static final String LLF="//td/a[text()='LLF']/parent::*/following-sibling::*//select";
		public static final String Update="//a[text()='Update']";
		public static final String CustomizeButton="//div[@class='full_info_link']/button";
		public static final String Llfsiebelcheckbox="(//div[text()='Link Loss Forwarding']/parent::*/following::input)[1]";
		public static final String Fastsearchval="//input[@name='fast_search_val']";
		public static final String Csisf="//span[text()='COLT Resource Inventory Search Folder']/parent::*";
		public static final String Ccsp="(//span[text()='COLT Circuit Search Profile']/parent::a)[2]";
		public static final String Circuitname="(//b[contains(text(),'Circuit Name')]/following::input)[1]";
		public static final String searchButton="//div/a[@id='searchButton']";
		public static final String Ethernetport="//td[contains(text(),'Ethernet Port:')]//a";
		public static final String NNIProfile="//td/a[text()='NNI Profile']/parent::*/following-sibling::*//input[@type='text']";
		}
}
