package pageObjects.siebeltoNCObjects;

public class OvcCircuitValidHelperObj {
	
	public static class OvcCircuitValidHelper	
	{
		
	public static final String InventoryProject="@xpath=//span[text()='COLT Inventory Project']/parent::*";
	public static final String Circuits="@xpath=//*[text()='Circuits']/parent::*";
	public static final String OVCcircuit="//span[text()='OVC Circuits Folder']/parent::a";
	public static final String AccountNameSorting="@xpath=//a[text()='Name']";
	public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
	public static final String FilterSelectName="@xpath=(//*[@class='inputs'])[1]";
	public static final String ApplyButton="@xpath=//*[@class='FilterButtons'] //*[text()='Apply']";
	public static final String ActualOvcCircuitnumber="@xpath=//span[contains(text(),'CEOS')]/parent::a";
	public static final String FilterInputValue="@xpath=//*[@name='inp']";

	
	}
	
}