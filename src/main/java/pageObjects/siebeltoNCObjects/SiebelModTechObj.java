package pageObjects.siebeltoNCObjects;

public class SiebelModTechObj {

	
	public static class Mode
	 { 	
		public static final String PrimaryLeadInput="@xpath=//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']";
		public static final String CallAdmissionControl="@xpath=((//input[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[1])";
		public static final String TotalNumberDDIs="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[3])";
		public static final String TotalDDi="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])";
		public static final String RouterTypeDropdownAccess="@xpath=(//span[text()='Router Type']/..//span)[3]";
//		public static final String SelectRouterTypeDropDownAccess="@xpath=//li[text()='No Colt Router']";
		public static final String SelectRouterTypeDropDownAccess="@xpath=//li/*[text()='No Colt Router']"; // Updated IP2020
		public static final String Layer3ResillanceDropdownAccess="@xpath=(//span[text()='Layer 3 Resilience']/..//span)[3]";
//		public static final String Layer3ResillanceSelectDropdownAccess="@xpath=//li[text()='No Resilience']";
		public static final String Layer3ResillanceSelectDropdownAccess="@xpath=//li/*[text()='No Resilience']"; // Updated IP2020
		public static final String ServiceBandwidthDropdownAccess="@xpath=(//span[text()='Service Bandwidth']/..//span)[3]";
//		public static final String ServiceBandwidthSelectAccess="@xpath=//li[text()='value']";	
		public static final String ServiceBandwidthSelectAccess="@xpath=//li/*[text()='value']";	 // Updated IP2020

	 }
	public static class DarkFiber
	 { 	
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
		public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";	
	 }
	
	public static class ModTechR4
	 { 	
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
	//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li[text()='Value']"; // Updated IP2020
	public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";

	 }
	
	
	public static class ModTechPrivateEthernet
	{
		public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
//		public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
		public static final String AEndSiteDropDown="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
//		public static final String AList="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String AList="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String SiteASetting="@xpath=//div[@id='colt-site-left']//span[@class='colt-oa-btn colt-oa-std-btn colt-oa-Primary-Child']";
		public static final String SiteBSetting="@xpath=//div[@id='colt-site-right']//span[@class='colt-oa-btn colt-oa-std-btn colt-oa-Primary-Child']";
		public static final String PopPlus="@xpath=//div[@title='Operational Attributes List Applet']//button[@title='Operational Attributes:New']";
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String PopAttributeDrodown="@xpath=//div[@title='Operational Attributes List Applet']//span[@class='siebui-icon-dropdown']";
		public static final String AttributeName="@xpath=//div[@title='Operational Attributes List Applet']//input[@name='COLT_Attribute_Name']";
		public static final String AttributeValue="@xpath=//div[@title='Operational Attributes List Applet']//input[@name='COLT_Attribute_Value']";
		public static final String SiteASettingOK="@xpath=//div[@title='Operational Attributes List Applet']//button[@title='Operational Attributes:OK']";


	}
	
	
	public static class ModTechforcommanproduct
	{
		public static final String SupportStarDate="@xpath=//span[text()='Support Start Date']/parent::*//input";
		public static final String SupprtEndDate="@xpath=//span[text()='Support End Date']/parent::*//input";
		public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";

	}
	
	public static class modEthernetHubMiddleApplet
	{
		public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";
		public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
	//	public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";
		public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";  //Updated IP2020
}
	
	public static class modTechNumberHosting
	{
		public static final String CallAdmissionControl="@xpath=//span[contains(text(),'Call Admission Control ')]/..//input";
		public static final String NumberOfSignallingTrunks="@xpath=//span[text()='Number of Signalling Trunks']/..//input";
		public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";

	}
		
	public static class modTechSwiftNetMiddleApplet
	{
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']"; 
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
		public static final String CPECombinationIDGeneric="@xpath=//span[text()='Value']/parent::div//input";
		public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";

	}
	
	public static class  modTechMangedDeicatedFirewall
	{
		public static final String CPECombinationIDGeneric="@xpath=//span[text()='Value']/parent::div//input";
		public static final String HighAvailabilityRequired="@xpath=(//span[text()='Value']/parent::*//span)[3]";
//		public static final String HighAvailabilityRequiredli="@xpath=//li[text()='Yes']";
		public static final String HighAvailabilityRequiredli="@xpath=//li/*[text()='Yes']"; // Updated IP2020
		public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";


	}
	
	public static class modTechPrivateWaveNodeMiddleApplet
	{
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
//		public static final String changeMiddleLi="@xpath=//li[text()='Value']";
		public static final String changeMiddleLi="@xpath=//li/*[text()='Value']"; // Updated IP2020
		public static final String SaveButton="@xpath=//div/a[text()='Save']";
	}
	
	
	
	public static class CarnorSelectServiceGroupTab
	{
		
		public static final String DropDown="@xpath=//*[@aria-label='Third Level View Bar']";
		public static final String ServiceGroupNew="@xpath=//button[@aria-label='Service Group:New']";
		public static final String serviceGrouplookup="@xpath=//span[@class='siebui-icon-pick']";
		public static final String ServiceGroupOk="@xpath=//button[@title='Add/Pick Service Group:OK']";
						
	}
	
	public static class CarnorgetReferenceNo
	{
	public static final String CircuitReferenceAccess="@xpath=//button[@id='colt-circuitref-button']";
	public static final String CircuitReferenceValue="@xpath=//input[@class='colt-attribute-input siebui-ctrl-input colt-popup-link-ctrl']";

	}
	
	public static class OperationAttributeCarno
	{
		public static final String Voiceconfigtab="@xpath=//a[text()='Voice Config']";
		public static final String OtherTab="@xpath=//a[text()='Other']";
		public static final String OperationAttribute1="@xpath=(//span[@class='colt-oa-btn colt-oa-std-btn'])[2]";
		public static final String OperationAttributeSubmit="@xpath=//button[@title='Operational Attributes:OK']";
		public static final String AttributeValue1="@xpath=//input[@name='COLT_Attribute_Value']";
		public static final String OperationAttribute2="@xpath=(//span[@class='colt-oa-btn colt-oa-std-btn'])[3]";

	}
	
	public static class CarnorCompletedValidation
	{
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
		public static final String SubnetworkPopUP="@xpath=//button[@class='colt-primary-btn']";
		public static final String OrderStatus="@xpath=//input[@aria-label='Order Status']";
	//	public static final String SelectCompleted="@xpath=//li[text()='Completed']";
		public static final String SelectCompleted="@xpath=//li/*[text()='Completed']"; // Updated IP2020

		public static final String OrderComplete="@xpath=//button[text()='Yes']";
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";



	}
	
	public static class modTechDarkFibreMiddleApplet
	{
		public static final String CustomizeButton="@xpath=//button[@id='btnMiddleCustom']";
		public static final String Connectionlink="@xpath=//a[contains(text(),'Connection')][@href='javascript:void(0);'']";
		public static final String AendSiteLink="@xpath=//a[text()='A End Site']";
		public static final String CustomizeServiceBandwidth="@xpath=//a[contains(@name,'Service Bandwidth')]//following-sibling::*//select";
		public static final String InstallationTimeLink="@xpath=//a[text()='Installation Time'][@href='javascript:void(0);'']";
		public static final String InstallTime="@xpath=//a[contains(@name,'Install Time')]//following-sibling::*//select";
		public static final String DoneEthernetConnection="@xpath=//button[text()='Done']";
		public static final String BendSiteLink="@xpath=//a[text()='B End Site']";
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
//		public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		
		public static final String MiddleLi="@xpath=////*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String SaveButton="@xpath=//div/a[text()='Save']";		


	}
	
	public static class Wave
	{
		public static final String DoneEthernetConnection="@xpath=//button[text()='Done']";
		public static final String CustomizeServiceBandwidth="@xpath=//a[contains(@name,'Service Bandwidth')]//following-sibling::*//select";
		public static final String InstallationTimeLink="@xpath=//a[text()='Installation Time'][@href='javascript:void(0);'']";
		public static final String InstallTime="@xpath=//a[contains(@name,'Install Time')]//following-sibling::*//select";
		public static final String CustomizeButton="@xpath=//button[@id='btnMiddleCustom']";
		public static final String Connectionlink="@xpath=//a[contains(text(),'Connection')][@href='javascript:void(0);'']";
		public static final String AendSiteLink="@xpath=//a[text()='A End Site']";
		public static final String BendSiteLink="@xpath=//a[text()='B End Site']";

	}
	
	public static class ethernetVpnAccessMiddleApplet
	{
	 public static final String ServiceBandwidthIPAccess="@xpath=//span[text()='Service Bandwidth']/..//input";

	}
	
	public static class modTechDarkFibreB
	 { 	
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
		public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";
	//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
 
	 }
	
	public static class modTechDarkFibreA
	 { 	
		public static final String SiteAInput="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";
		public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
		public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";
	//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
 
	 }
	
	public static class modTechPrizmNetMiddleApplet
	 { 	
		public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*/span)[2]";
	//	public static final String SelectValueDropdown="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SelectValueDropdown="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";
		public static final String ClickheretoSaveAccess="@xpath=(//a[text()='Click here to save your product configuration.'])[1]";
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
//		public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String SaveButton="@xpath=//div/a[text()='Save']";		

	 }
	
	public static class modTechMiddleAppletCPESolutionService
	 { 	
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String SaveButton="@xpath=//div/a[text()='Save']";
			
	 }
	

	public static class modTechPrivateWaveNode
	 { 	
		public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
	//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020
		public static final String SiteAInput="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
		public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";
		
	 }

	
	public static class IPGuardianObj
	{
		public static final String AlertingNotification="@xpath=//div[@id='colt-s-order-connection-attributes']//div[1]//input";
		public static final String Customerdnsresolve="@xpath=//div[@id='colt-s-order-connection-attributes']//div[3]//input";
		public static final String servicebandwidth="@xpath=//div[@id='colt-s-order-connection-attributes']//div[6]//input";
		public static final String Testwindowipaccess="@xpath=//span[text()='Test Window']/..//input";
		public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";

	}
	public static class InterconnectObj
	{

	public static final String CallAdmissionControl ="@xpath=//span[contains(text(),'Call Admission Control ')]/..//input";
	public static final String NumberOfSignallingTrunks="@xpath=//span[text()='Number of Signalling Trunks']/..//input";
	public static final String SaveButton ="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
	}
	
	public static class EthernetSpokeObj
	{
		public static final String ClickDropdown ="@xpath(//span[text()='Value']/parent::*/span)[2]";
	//	public static final String SelectValueDropdown ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SelectValueDropdown ="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; // Updated IP2020

		public static final String ClickDropdownB ="@xpath=(//span[text()='Value']/parent::*/span)[3]";
		public static final String SaveOrderChanges ="@xpath=//a[text()='Click here to save your order changes.']";

	}
	
	public static class modTechEthernetAccessMiddleApplet
	 { 	
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";	
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";	
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";	// Updated IP2020

		public static final String SaveButton="@xpath=//div/a[text()='Save']";		

	 }
	

}
