package pageObjects.siebeltoNCObjects;

public class SiebelQuoteCreationObj {

	public static class CustomerOrderPage
	{	
		public static final String OpportunityNo="@xpath=//*[(@aria-labelledby='Opportunity_Number_Label')]"; //OpportunityNo Text
		public static final String RequestReceivedDate="@xpath=//*[(@aria-label='Request Received Date')]"; //Request Received Date picker
		public static final String DeliveryChannel="@xpath=//*[(@aria-labelledby='COLT_Delivery_Channel_Label')]"; //DeliveryChannel
		public static final String OrderingPartySearch="@xpath=//input[@aria-labelledby='Ordering_Party_Label']/..//span";//OrderingPartySearch
		public static final String SaveOrderChanges="@xpath=//a[text()='Click here to save your order changes.']"; //SaveOrderChanges
		public static final String PartySearchPopupDropdown="@xpath=//*[@class='siebui-popup-button']//*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']"; //PartySearchPopupDropdown
		public static final String PartyName="@xpath=//*[@class='ui-menu-item'][text()='Party Name']"; //Party Name
		public static final String InputPartyname="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']"; //PopupQuerySrchspec Label
		public static final String PickAccountOk="@xpath=//button[@title='Pick Account:OK']"; //Pick Account
		public static final String PopupYes="@xpath=//button[text()='Yes']"; //PopupYes
		public static final String UnassignedPortRow="@xpath=//div[@name='popup']//td[text()='PortName']/parent::tr"; //UnassignedPortRow
		public static final String Ordering_Party_Label="@xpath=//*[(@aria-labelledby='Ordering_Party_Label')]"; //Ordering_Party_Label
		public static final String OrderingPartyAddrSearch="@xpath=//input[@aria-labelledby='COLT_Ordering_Account_Address_Name_Label']/..//span"; //OrderingPartyAddrSearch
		public static final String OrderingPartyAddrSearchDropdown="@xpath=//*[@class='siebui-popup-button']//*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']"; //OrderingPartyAddrSearchDropdown
		public static final String PartyAddr="@xpath=//*[@class='ui-menu-item'][text()='Street Name']"; //Party Address
		public static final String InputPartyAddr="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']"; //Input Party Addr
		public static final String PickAccoutSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Account:Go']"; //Pick Accout Search
		public static final String PickAddrSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Address:Go']"; //PickAddrSearch
		public static final String PickContactSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Contact:Go']"; //PickContactSearch
		public static final String PickAddrSubmit1="@xpath=//button[@title='Pick Address:OK']"; //PickAddrSubmit1
		public static final String PickAccnt="@xpath=//*[contains(@title,'13869')]"; //PickAccnt
	//	public static final String PopupCustomDropdown="@xpath=//div[contains(@style,'block')]//span[@data-allowdblclick='true']"; //PopupCustomDropdown
	//	public static final String PopupCustomDropdown="@xpath=//*[not(contains(@style,'display'))]//span[@data-allowdblclick='true']"; //Updated 2020
		
		public static final String OrderingPartyContactSearch="@xpath=//input[@aria-labelledby='COLT_Ordering_Contact_Full_Name_Label']/..//span"; //OrderingPartyContactSearch
		public static final String PartyContactPopupDropdown="@xpath=//*[@class='siebui-popup-button']//*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']"; //PartyContactPopupDropdown
	//	public static final String LastName="@xpath=//li[text()='Last Name']"; //LastName
		public static final String LastName="@xpath=//li/*[text()='Last Name']"; //Updated 2020
		
		public static final String InputFirstname="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']"; //InputFirstname
		public static final String FirstnameSubmit="@xpath=//button[@title='Pick Contact:OK']"; //FirstnameSubmit
		public static final String SalesChannel="@xpath=//*[(@aria-labelledby='COLT_Sales_Channel_Label')]"; //SalesChannel
		public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]"; //NewServiceOrder
					
		public static final String newpick="@xpath=//li[@class='ui-menu-item']//div[text()='Party Name']";
		public static final String newparty="@xpath=//li[@class='ui-menu-item']//div[text()='Street Name']";
		public static final String newname="@xpath=//li[@class='ui-menu-item']//div[text()='Last Name']";
		public static final String PopupCustomDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']//following-sibling::span"; //PopupCustomDropdown
		public static final String dropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']//following-sibling::span";	
		
		
		
		
	}
	
	public static class PrimaryAddressPage
	{	
		public static final String PrimaryAddressHdr="@xpath=//span[contains(text(),'PRIMARY ADDRESS DETAILS')]"; //PRIMARY ADDRESS DETAILS Section
		public static final String SiteAddLbl="@id=siteAddressAEnd_label"; //Site Address label
		public static final String SiteAddTxFld="@id=siteAddressAEnd"; //Site Address Text Field
		//public static final String NextBtn="@id=next_model"; //Next Button
		public static final String NextBtn="@xpath=//div[@id='html_next']//*[contains(text(),'Next')]"; //Next Button
		public static final String NextBtnElem="//div[@id='html_next']//*[contains(text(),'Next')]"; //Next Button
		public static final String NextBtnId="@id=next_model"; //Next Button
		public static final String SearchBtnImg="@xpath=//div[@id='searchButtonAEnd']//img[contains(@title,'Search for an Address')]"; //Search Button img
		public static final String connectCheckElem="@xpath=//div[contains(text(), 'Connectivity Check In Progress')]"; //Next Button
	}
	
	public static class PrimaryConnectionPage
	{	
		public static final String ServiceBandwidthLbl="@id=serviceBandwidth_label"; //Service Bandwidth label
		public static final String ServiceBandwidthDrpDwn="@id=serviceBandwidth"; //Service Bandwidth Drop down
		public static final String ServiceBandwidthVal1="@xpath=//select[@id='serviceBandwidth']//option[contains(@value,'"; //Service Bandwidth Drop down
		public static final String ServiceBandwidthVal2="')]";
		
	}
	
	public static class IPFeaturesPage
	{	
		public static final String RouterTypeLbl="@id=selectRouterTypeAEnd_label"; //Router Type label
		public static final String RouterTypeDrpDwn="@id=selectRouterTypeAEnd"; //Router Type Drop Down
		public static final String RouterTypeVal1="@xpath=//select[@id='selectRouterTypeAEnd']//option[contains(@value,'"; //Router Type Value
		public static final String RouterTypeVal2="')]";
	}
	
	public static class SiteAddonsPage
	{	
		public static final String AddOnsHeader="@xpath=//span[contains(text(),'ADDONS')]"; //ADDONS Header
		
	}
	
	public static class L3RESILIENCEPage
	{	
		public static final String L3ResilienceGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'L3 Resilience')]"; //L3RESILIENCE Group Header
		
	}
	
	public static class DiversityPage
	{	
		public static final String DiversityGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Diversity')]"; //DIVERSITY Group Header
		
	}
	
	public static class ServiceAddonsPage
	{	
		public static final String ServiceLevelAddonsGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'SERVICE LEVEL ADDONS')]"; //SERVICE LEVEL ADDONS Group Header
		
	}
	
	public static class BespokeFeaturesPage
	{	
		public static final String BespokeFeaturesGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Bespoke Features')]"; //Bespoke Features Group Header
		
	}
	
	public static class AdditionalProductDataPage
	{	
		//Primary Section
		public static final String PrimaryGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Primary')]"; //Primary Group Header
		public static final String SiteAccessTechnologylbl="@id=siteAccessTechnology_label"; //Site Access Technology label
		public static final String SiteAccessTechnologyDrpDwn="@id=siteAccessTechnology"; //Site Access Technology Drop Down
		public static final String AccessTechVal1="@xpath=//select[@id='siteAccessTechnology']//option[contains(@value,'"; //Ethernet Value
		public static final String AccessTechVal2="')][1]";
		public static final String SiteCabinetlbl="@id=siteCabinetID_label"; //Site Cabinet Field label
		public static final String SiteCabinettxtfld="@id=siteCabinetID"; //Site Cabinet Text Field

		//Additional Service Info
		public static final String AdditionalServiceInfoGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Additional Service Info')]"; //Additional Service Info Group Header
		public static final String routerTechDropdown="@id=routerTechnology"; //Router Technology Dropdown
		public static final String virtualRouterValue1="@xpath=//select[@id='routerTechnology']//option[contains(@value,'"; //Router Technology Dropdown
		public static final String virtualRouterValue2="')]";
		public static final String capLeadTimeDropdown="@id=existingCapacityLeadTimeAEnd"; //Existing Capacity Lead Time	 Dropdown
		public static final String NoValueInCapLead="@xpath=//select[@id='existingCapacityLeadTimeAEnd']//option[contains(@value,'No')]"; //Router Technology Dropdown
		public static final String FirstNamelbl="@id=firstName_label"; //First Name Label
		public static final String FirstNameTxtFld="@id=firstName"; //First Name Text Field
		public static final String LastNamelbl="@id=lastName_label"; //Last Name Label
		public static final String LastNameTxtFld="@id=lastName"; //Last Name Text Field
		public static final String ContactNumberlbl="@id=contactNumber_label"; //Contact Number Label
		public static final String ContactNumberTxtFld="@id=contactNumber"; //Contact Number Text Field
		public static final String MobileNumberlbl="@id=mobileNumber_label"; //Mobile Number Label
		public static final String MobileNumberTxtFld="@id=mobileNumber"; //Mobile Number Text Field
		public static final String emailIdTxtFld="@id=email"; //Email Id Text Field
	
	}
	
	public static class PricingAndPromotionsPage
	{	
		//Primary Section
		public static final String PromotionsGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Promotions')]"; //Promotions Group Header
		public static final String PromotionsItm="@xpath=//div[@class='cfg-im-item selected']//img"; //Promotion Item Image

		public static final String UpdateBtn="@id=update"; //Update Button
		public static final String SaveToQuoteBtn="@id=add_to_transaction"; //Save to Quote Button

	
	}
	
	public static class LegalandTechnicalContactDetails
	{	
		//Primary Section
		public static final String LegalContactlink="@xpath=//*[@id=\'customerInformation_t-label\']";
		public static final String GetLegalcontactbtn="@xpath=//*[@id=\'legalGetContacts_d\']/input"; 

		public static final String searchText="@name=searchEmail";
		public static final String searchBtn="@id=searchButton";
		public static final String SelectLegalcontact="@xpath=//tr[1]//td[1]//i[1]";
		public static final String OCHLegalContact="@xpath=//a[@class='btn btn-success btn-block']";
		
		public static final String TechnicalContactslink="@xpath=//*[@id=\'technicalContactsDetails_t-label\']";
		public static final String GetTechnicalContactsbtn="@xpath=//div[@id='technicalGetContacts_d']//input"; 

		public static final String SelectTechnicalcontact="@xpath=//tr[1]//td[1]//i[1]";
		public static final String OCHTechnicalContact="@xpath=//a[@class='btn btn-success btn-block']";
		
		public static final String Savebutton="@xpath=//span[@id='ui-id-3']";

	}
	
	public static class SubmissionforCommercialApproval
	{	
		
		public static final String CommercialApprovalLink="@xpath=//*[@id=\'tab_77160218-title\']";
		public static final String SubmittoAppvlBtn="@xpath=//span[@class='oj-button-text'][text()='Submit to Approval']"; 

		
		}
	
	public static class SubmissionforTechnicalApproval
	{	
		
		public static final String TechnicalApprovalLink="@xpath=//span[@class='oj-tabbar-item-label'][text()='Technical Approval']";
		public static final String ReadyforTechAppl="@xpath=//input[@type='radio' and @id='fastlaneOption_t0']";
		public static final String readyForTechApprovalCbx="@xpath=//input[@value='readyForTechnicalApproval']//parent::span";
		public static final String readyForTechApprCbxElem="//input[@value='readyForTechnicalApproval']//parent::span";
		public static final String SubmitForTechApplBtn="@xpath=//button[@name='submit_for_technical_approval']"; 

		
	}
	
}