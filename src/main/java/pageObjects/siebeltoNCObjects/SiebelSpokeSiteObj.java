package pageObjects.siebeltoNCObjects;

public class SiebelSpokeSiteObj {
	
	public static class Spoke
	 { 	
		public static final String HubTableIDSelect="@xpath=(//table[@id='hub_table']//tr[@class='row'])[1]";
		public static final String HubTableOK="@xpath=//input[@id='PickHub']";
		public static final String SearchAddressSiteB="@xpath=//div[@id='colt-site-right']//span[@title='Select a site']";
		public static final String ClickDropdownB="@xpath=(//span[text()='Value']/parent::*/span)[3]";
		public static final String TextInputB="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::*//input";
	 }

}
