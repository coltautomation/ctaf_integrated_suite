package pageObjects.siebeltoNCObjects;

import java.io.File;
public class SiebleInFlightOrderObj {

	public static class ServiceTab
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String InflightPlusSign="@xpath=//button[@aria-label='Workflow:New']";
		
		public static final String WorkflowDropdown="@xpath=//input[@aria-labelledby='Workflow_Type_Label']/../span";
		
	//	public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";  
		public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";  // Updated IP2020
	
		public static final String WorkFlowType="@xpath=//input[@aria-label='Workflow Type']";
		
		public static final String InflightVoiceSelection="@xpath=//input[@aria-labelledby='COLT_Voice_Label']";
		
		public static final String BandwidthIPAccess="@xpath=//input[@aria-labelledby='A-End_Primary_Bandwidth_Label']";
		
		public static final String BandwidthSpoke="@xpath=//input[@aria-labelledby='B-End_Primary_Bandwidth_Label']";
		
		public static final String BandwidthBend="@xpath=//input[@aria-labelledby='B-End_Primary_Bandwidth_Label']";
		
		public static final String WorkflowSave="@xpath=//button[@aria-label='Workflow:Save']";
		
		public static final String DeliveryLineItem="@xpath=//table[@summary='Workflow']//td[@title='Delivery']";
		public static final String ChangeWorkflowBtn="@xpath=//span[contains(text(),'Change Workflow')]";
		public static final String YesBtn="@xpath=//span[contains(text(),'Yes')]";
		public static final String ASiteDropDwn="@xpath=//span[@class='siebui-icon-dropdown applet-form-combo sifcat-list-combo-icon applet-list-combo sifcat-Aend-reason-drpdwn']";
		public static final String BSiteDropDwn="@xpath=//span[@class='siebui-icon-dropdown applet-form-combo sifcat-list-combo-icon applet-list-combo sifcat-Bend-reason-drpdwn']";
	//	public static final String DropDwnValue="@xpath=//ul[contains(@style,'block')]//a[text()='Value']";
		public static final String DropDwnValue="@xpath=//*[not(contains(@style,'display'))]//a[text()='Value']";  // Updated IP2020

		public static final String ProceedBtn="@xpath=//span[contains(text(),'Proceed')]";
	//	public static final String OKBtn="@xpath=//div[contains(@style,'block')]//span[text()='OK']";
		public static final String OKBtn="@xpath=//*[not(contains(@style,'display'))]//span[text()='OK']"; // Updated IP2020
	
		

	}
	public static class cpeSolutions
	{
		public static final String FieldLabel="@xpath=//span[text()='ChangeFieldName']";
		public static final String FieldLabel2="@xpath=//span[text()='Service Bandwidth']//i[@title='An in-flight change was made to this attribute']";
		
		public static final String ServiceBandwidthCpe="@xpath=//span[text()='Service Bandwidth']/..//input/following-sibling::span";
		
	//	public static final String ValueInsideDropdown="@xpath=//li[text()='Data']";
		public static final String ValueInsideDropdown="@xpath=//li/*[text()='Data']";
	
		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
		
	}
	public static class PlusAccessWHSwifPrizm
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String FieldLabelSwift="@xpath=(//span[text()='ChangeFieldName'])[1]//i";
		
		public static final String TextInputSwift="@xpath=(//span[text()='Customer Alias'])[1]/parent::*//input";
		
		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";

		
	}
	public static class ethernetSpoke
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String FieldLabelB="@xpath=(//span[text()='Shelf ID']//i)[2]";
		
		public static final String TextInputB="@xpath=(//span[text()='Shelf ID']/parent::*//input)[2]";
		
		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
		
		public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";

		public static final String FieldLabel="@xpath=label[text()='ChangeFieldName']/following-sibling::span";
	}
	
	public static class ipAccess
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
		
		public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";

		public static final String FieldLabel="@xpath=label[text()='ChangeFieldName']/following-sibling::span";
	}
	public static class NumberHostingInflight
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
		
		public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";

		public static final String FieldLabel="@xpath=//span[text()='ChangeFieldName']//i";
	}
	public static class privateEthernet
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String FieldLabelR4="@xpath=//div[@id='colt-site-left']//span[text()='ChangeFieldName']//i";
		
		public static final String TextInputR4="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::*//input";

		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";

		public static final String AccessPortAend="@xpath=//div[@id='colt-site-left']//span[text()='Access Port']";
	}
	
	public static class privateWaveNode
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String FieldLabelR4="@xpath=//div[@id='colt-site-left']//span[text()='ChangeFieldName']//i";
		
		public static final String TextInputR4="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::*//input";

		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";

		public static final String TerminationInformationAEnd="@xpath=//div[@id='colt-site-left']//span[text()='Termination Information']";
	}
	public static class SIPTrunkingInflight
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String FieldLabel="@xpath=//span[text()='ChangeFieldName']//i";
		
		public static final String CallAdmissionControl="@xpath=((//input[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[1])";

		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";

	}
	public static class VoiceLineVInflight
	{
		public static final String ClickLink="@xpath=//a[text()='Value']";
		
		public static final String FieldLabel="@xpath=//span[text()='ChangeFieldName']//i";
		
		public static final String TotalDDi="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[3])";

		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";

	}
}


