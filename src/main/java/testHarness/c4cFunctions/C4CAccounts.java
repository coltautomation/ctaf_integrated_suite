package testHarness.c4cFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4CLoginObj;
import testHarness.commonFunctions.ReusableFunctions;

public class C4CAccounts extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
			
	public void NavigateToAccounts() throws InterruptedException, IOException 
	{		
		Reusable.WaitforC4Cloader();
		waitForElementToAppear(C4CAccountsObj.Accounts.customersMenu,30);
		clickByJS(C4CAccountsObj.Accounts.customersMenu);
		waitForElementToAppear(C4CAccountsObj.Accounts.accountsMenu,30);
		int i=0;
		for (i=0; i < 10; i++) {
			try {
				clickByJS(C4CAccountsObj.Accounts.accountsMenu);
				Reusable.WaitforC4Cloader();
				if (waitForElementToBeVisible(C4CAccountsObj.Accounts.defaultCkb,30)) {
				break;
				}else{
					pause(2500);
					Reusable.Waittilljquesryupdated();
					continue;
				}
				
			}catch (Exception e) {
				pause(2500);
				Reusable.WaitforC4Cloader();
				continue;
			}
		}
		if (i >=10) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in Selecting Account Menu in C4C Account Page after 10 attempts, Please Re-try");
			System.out.println("Error in Selecting Account Menu in C4C Account Page after 10 attempts, Please Re-try");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
}
	
	public void SearchAccountOld(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String OCN = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OCN_Number");
		String Account_Name = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Account_Name");
		
		click(C4CAccountsObj.Accounts.myAccountsSection,"Select View Dropdown");
		ClickonElementByString("//li[normalize-space(.)='All']", 30);
		
		Reusable.WaitforC4Cloader();
		waitForAjax();
		verifyExists(C4CAccountsObj.Accounts.searchBtn,"Search Textbox");		
		click(C4CAccountsObj.Accounts.searchBtn,"Search Button");
		
		Reusable.WaitforC4Cloader();
		waitForAjax();
		verifyExists(C4CAccountsObj.Accounts.searchTxb,"Search Textbox");
		sendKeys(C4CAccountsObj.Accounts.searchTxb, OCN, "Search Textbox");
		click(C4CAccountsObj.Accounts.searchField,"Search Field");
		
		Reusable.WaitforC4Cloader();
		waitForAjax();
		String accNameObj = C4CAccountsObj.Accounts.accountName1+ Account_Name +C4CAccountsObj.Accounts.accountName2;
		verifyExists(accNameObj,"Account Name");		
	}	
	
	public void SearchAccount(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		
//		Initializing the Variable
		String OCN_Number = DataMiner.fngetcolvalue( Sheet_Name, iScript, iSubScript,"OCN_Number");
		String Account_Name = DataMiner.fngetcolvalue( Sheet_Name, iScript, iSubScript,"Account_Name");
		int i=0;
		
//		Searching the Account
		if (waitForElementToBeVisible(C4CAccountsObj.Accounts.selectViewDn,150)) {
			Reusable.Waittilljquesryupdated();
			//waitForElementToBeVisible(C4CAccountsObj.Accounts.defaultCkb,90);
			ScrollIntoViewByString(C4CAccountsObj.Accounts.defaultCkb);
			//Reusable.WaitforC4Cloader();
			for (i=0; i < 25; i++) {
				try {
					waitForElementToBeVisible(C4CAccountsObj.Accounts.selectViewDn,5);
					click(C4CAccountsObj.Accounts.selectViewDn, "Select View Down");
					//Reusable.WaitforC4Cloader();
					Reusable.Waittilljquesryupdated();
					if (waitForElementToBeVisible(C4CAccountsObj.Accounts.allLst,5)) {
						//Reusable.WaitforC4Cloader();
						Reusable.Waittilljquesryupdated();
						click(C4CAccountsObj.Accounts.allLst, "All List");
						//Reusable.WaitforC4Cloader();
						Reusable.Waittilljquesryupdated();
						break;
					} else {
						pause(2500);
						//Reusable.WaitforC4Cloader();
						Reusable.Waittilljquesryupdated();
						continue;
					}
				} catch (Exception e) {
					pause(2500);
					Reusable.WaitforC4Cloader();
					continue;
				}
			}
			if (i >=25) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in Selecting All Accounts in C4C Account Page after 25 attempts, Please Re-try");
				System.out.println("Error in Selecting All Accounts in C4C Account Page after 25 attempts, Please Re-try");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
			}
			//waitForElementToAppear(C4CAccountsObj.Accounts.searchIcon,120);
			ScrollIntoViewByString(C4CAccountsObj.Accounts.searchIcon);
			//Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			click(C4CAccountsObj.Accounts.searchIcon, "Search Icon");
			//Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			if (waitForElementToBeVisible(C4CAccountsObj.Accounts.searchTxb, 60)) {
				sendKeys(C4CAccountsObj.Accounts.searchTxb, OCN_Number, "Search Textbox");
				click(C4CAccountsObj.Accounts.searchBtn, "Search Button");
				Reusable.WaitforC4Cloader();
				pause(3000);
				Reusable.Waittilljquesryupdated();
				WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
				ScrollIntoViewByString(C4CAccountsObj.Accounts.defaultCkb);
				String accNameObj = C4CAccountsObj.Accounts.accountName1+ Account_Name +C4CAccountsObj.Accounts.accountName2;
				click(accNameObj,"Account Name");
				//Reusable.WaitforC4Cloader();
				Reusable.Waittilljquesryupdated();
				ExtentTestManager.getTest().log(LogStatus.PASS, "OCN Search with Account name '"+Account_Name+"' is successfull");
				//System.out.println("OCN Search with Account name '"+Account_Name+"' is successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Search OCN Textbox is not found in C4C Home Page");
				//System.out.println("Search OCN Textbox is not found in C4C Home Page");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Select View Dropdown is not found in C4C Home Page");
			System.out.println("Select View Dropdown is not found in C4C Home Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		
	}
	
public void SearchAccount_Agent(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		
//		Initializing the Variable
		String OCN_Number = DataMiner.fngetcolvalue( Sheet_Name, iScript, iSubScript,"OCN_Number_Agent");
		String Account_Name = DataMiner.fngetcolvalue(Sheet_Name, iScript, iSubScript,"Account_Name_Agent");
		int i=0;
		
//		Searching the Account
		if (waitForElementToBeVisible(C4CAccountsObj.Accounts.selectViewDn,90)) {
			Reusable.Waittilljquesryupdated();
			//waitForElementToBeVisible(C4CAccountsObj.Accounts.defaultCkb,90);
			ScrollIntoViewByString(C4CAccountsObj.Accounts.defaultCkb);
			//Reusable.WaitforC4Cloader();
			for (i=0; i < 25; i++) {
				try {
					waitForElementToBeVisible(C4CAccountsObj.Accounts.selectViewDn,5);
					click(C4CAccountsObj.Accounts.selectViewDn, "Select View Down");
					//Reusable.WaitforC4Cloader();
					Reusable.Waittilljquesryupdated();
					if (waitForElementToBeVisible(C4CAccountsObj.Accounts.allLst,5)) {
						//Reusable.WaitforC4Cloader();
						Reusable.Waittilljquesryupdated();
						click(C4CAccountsObj.Accounts.allLst, "All List");
						//Reusable.WaitforC4Cloader();
						Reusable.Waittilljquesryupdated();
						break;
					} else {
						pause(2500);
						//Reusable.WaitforC4Cloader();
						Reusable.Waittilljquesryupdated();
						continue;
					}
				} catch (Exception e) {
					pause(2500);
					Reusable.WaitforC4Cloader();
					continue;
				}
			}
			if (i >=25) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in Selecting All Accounts in C4C Account Page after 25 attempts, Please Re-try");
				System.out.println("Error in Selecting All Accounts in C4C Account Page after 25 attempts, Please Re-try");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
			}
			//waitForElementToAppear(C4CAccountsObj.Accounts.searchIcon,120);
			ScrollIntoViewByString(C4CAccountsObj.Accounts.searchIcon);
			//Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			click(C4CAccountsObj.Accounts.searchIcon, "Search Icon");
			//Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			if (waitForElementToBeVisible(C4CAccountsObj.Accounts.searchTxb, 60)) {
				sendKeys(C4CAccountsObj.Accounts.searchTxb, OCN_Number, "Search Textbox");
				click(C4CAccountsObj.Accounts.searchBtn, "Search Button");
				Reusable.WaitforC4Cloader();
				pause(3000);
				Reusable.Waittilljquesryupdated();
				WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
				ScrollIntoViewByString(C4CAccountsObj.Accounts.defaultCkb);
				String accNameObj = C4CAccountsObj.Accounts.accountName1+ Account_Name +C4CAccountsObj.Accounts.accountName2;
				click(accNameObj,"Account Name");
				//Reusable.WaitforC4Cloader();
				Reusable.Waittilljquesryupdated();
				ExtentTestManager.getTest().log(LogStatus.PASS, "OCN Search with Account name '"+Account_Name+"' is successfull");
				//System.out.println("OCN Search with Account name '"+Account_Name+"' is successfull");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Search OCN Textbox is not found in C4C Home Page");
				//System.out.println("Search OCN Textbox is not found in C4C Home Page");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Select View Dropdown is not found in C4C Home Page");
			System.out.println("Select View Dropdown is not found in C4C Home Page");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		
	}

}
