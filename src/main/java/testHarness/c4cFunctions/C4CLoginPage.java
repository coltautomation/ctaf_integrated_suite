package testHarness.c4cFunctions;


import java.io.IOException;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4CLoginObj;
import testHarness.commonFunctions.ReusableFunctions;

public class C4CLoginPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void C4CLogin(String file_name, String usertype) throws InterruptedException, IOException 
	{
		Reusable.Waittilljquesryupdated();
		
		switch(usertype) {
		case "Sales_User":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.SalesUserC4C_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.SalesUserC4C_Password,"Password textbox");
			break;
		case "SE_User":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.SEUser_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.SEUser_Password,"Password textbox");
			break;
		case "CST_User":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.CSTUser_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.CSTUser_Password,"Password textbox");
			break;
		case "PS_User":	
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.PSUser_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.PSUser_Password,"Password textbox");
			break;
		case "DP_User":
			
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.DPUser_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.DPUser_Password,"Password textbox");
			break;
		case "VP_User1":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.VP_SalesUser1_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.VP_SalesUser1_Password,"Password textbox");
			break;
		case "VP_User2":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.VP_SalesUser2_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.VP_SalesUser2_Password,"Password textbox");
			break;
		case "AD_User":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.ADUser_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.AD_Password,"Password textbox");
			break;
		case "QT_User":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.QT_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.QT_Password,"Password textbox");
			break;	
		case "Agent_User":
			isPresent(C4CLoginObj.Login.userNameTxb,90);
			sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.Agent_Username,"Username textbox");
			sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.Agent_Password,"Password textbox");
			break;
		}
		
		Reusable.Waittilljquesryupdated();
		isPresent(C4CLoginObj.Login.loginBtn,90);
		click(C4CLoginObj.Login.loginBtn,"Login button");
		Reusable.Waittilljquesryupdated();
		waitForElementToAppear(C4CLoginObj.Login.homeMenu, 60);
	}
	
	public void C4CLogin() throws InterruptedException, IOException 
	{
		
		Reusable.Waittilljquesryupdated();
		Reusable.WaitforC4Cloader();
		isPresent(C4CLoginObj.Login.userNameTxb,90);
		sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.SalesUserC4C_Username,"Username textbox");
		
		verifyExists(C4CLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.SalesUserC4C_Password,"Password textbox");
				
		Reusable.Waittilljquesryupdated();
		verifyExists(C4CLoginObj.Login.loginBtn,"Login button");
		click(C4CLoginObj.Login.loginBtn,"Login button");
		Reusable.Waittilljquesryupdated();
		
		waitForElementToAppear(C4CLoginObj.Login.homeMenu, 30);
		verifyExists(C4CLoginObj.Login.homeMenu,"Home Tab on Landing page");
		Reusable.WaitforC4Cloader();
	}


	public String QT_C4CLogin() throws InterruptedException, IOException 
	{
		verifyExists(C4CLoginObj.Login.userNameTxb,"Username textbox");
		sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.QT_Username,"Username textbox");
	
		verifyExists(C4CLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.QT_Password,"Password textbox");
		
		waitForAjax();
		Reusable.WaitforC4Cloader();
		verifyExists(C4CLoginObj.Login.loginBtn,"Login button");
		click(C4CLoginObj.Login.loginBtn,"Login button");
		
		waitForAjax();
		Reusable.WaitforC4Cloader();
		waitForElementToAppear(C4CLoginObj.Login.homeMenu,120);
		verifyExists(C4CLoginObj.Login.homeMenu,"Home Tab on Landing Page");
		
		return "True";

	}

}
