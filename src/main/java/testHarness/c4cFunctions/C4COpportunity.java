package testHarness.c4cFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4COpportunityObj;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;

public class C4COpportunity extends SeleniumUtils
{
	CPQLoginPage Login = new CPQLoginPage();
	ReusableFunctions Reusable = new ReusableFunctions();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	
	public void NavigateToOpportunities(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String Account_Name = null;
		Account_Name = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Account_Name");
		
		Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		String accHeaderObj = C4CAccountsObj.Accounts.accountHeader1+ Account_Name +C4CAccountsObj.Accounts.accountHeader2;
		//verifyExists(accHeaderObj,"Account Header");
		
		Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.opportunityMenuItem, 90);
		verifyExists(C4COpportunityObj.Opportunity.opportunityMenuItem,"Opportunity Menu Item");
		click(C4COpportunityObj.Opportunity.opportunityMenuItem,"Opportunity Menu Item");		
	}
	
	public void OpenNewOpportunity() throws InterruptedException, IOException 
	{
		Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		verifyExists(C4COpportunityObj.Opportunity.oppNewBtn,"Opportunity New Button");
		click(C4COpportunityObj.Opportunity.oppNewBtn,"Opportunity New Button");
		
		Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		//pause(2000);
		//verifyExists(C4COpportunityObj.Opportunity.newOpportunityTab,"New Opportunity Tab");
	}
	
	public void EnterOpportunityDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String oppName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Opportunity_Name") + timestamp.getTime();
						
		Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		isEnable(C4COpportunityObj.Opportunity.nameTxb);
		//verifyExists(C4COpportunityObj.Opportunity.nameTxb, "Name Textbox");
		sendKeys(C4COpportunityObj.Opportunity.nameTxb, oppName, "Name Textbox");
		
		//Reusable.WaitforC4Cloader();
		String offerType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Offer_Type");
		//verifyExists(C4COpportunityObj.Opportunity.offerTypeList, "Offer Type List");
		click(C4COpportunityObj.Opportunity.offerTypeList,"More Save Options");
		ClickonElementByString("//li[normalize-space(.)='"+ offerType +"']", 30);
		
		//Reusable.WaitforC4Cloader();
		
		if(verifyExists(C4COpportunityObj.Opportunity.campaignTxb))
		{
			//waitForElementToAppear(C4COpportunityObj.Opportunity.campaignTxb, 15);
			String campaign = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Campaign");
			//verifyExists(C4COpportunityObj.Opportunity.campaignTxb, "Campaign Textbox");
			sendKeys(C4COpportunityObj.Opportunity.campaignTxb, campaign, "Campaign Textbox");
			pause(1000);
			keyPress(C4COpportunityObj.Opportunity.campaignTxb,Keys.ENTER);
			pause(1000);
		}	
		
		String primaryProg = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Primary_Programme");
		//Reusable.WaitforC4Cloader();
		//verifyExists(C4COpportunityObj.Opportunity.primaryProgList, "Primary Program List");
		click(C4COpportunityObj.Opportunity.primaryProgList,"Primary Program List");
		ClickonElementByString("//li[normalize-space(.)='"+ primaryProg +"']", 30);
		
		//Reusable.WaitforC4Cloader();
		String primaryAttr = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary_Attribute");
		//verifyExists(C4COpportunityObj.Opportunity.primaryAttriList, "Primary Attribute List");
		click(C4COpportunityObj.Opportunity.primaryAttriList,"Primary Attribute List");
		ClickonElementByString("//li[normalize-space(.)='"+ primaryAttr +"']", 30);
		
		//Reusable.WaitforC4Cloader();
		String oppCurrency = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Opportunity_Currency");
		//verifyExists(C4COpportunityObj.Opportunity.oppCurrencyList, "Opportunity Currency List");
		click(C4COpportunityObj.Opportunity.oppCurrencyList,"Opportunity Currency List");
		ClickonElementByString("//li[normalize-space(.)='"+ oppCurrency +"']", 30);
		
		//Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.moreSaveOptions, 60);
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.moreSaveOptions);
		click(C4COpportunityObj.Opportunity.moreSaveOptions,"More Save Options");
		pause(3000);
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.saveNOpenBtn);
		click(C4COpportunityObj.Opportunity.saveNOpenBtn,"Save and Open Button");
		Reusable.Waittilljquesryupdated();
		Reusable.WaitforC4Cloader();
		pause(3000);
		Reusable.Waittilljquesryupdated();
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.OrganisationCountry, 300);
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.opprtunityIdBx, 300);
		Reusable.Waittilljquesryupdated();
		Reusable.WaitforC4Cloader();
		String oppId = getTextFrom(C4COpportunityObj.Opportunity.opprtunityIdBx);
		DataMiner.fnsetcolvalue( sheetName, scriptNo, dataSetNo, "Opportunity_ID", oppId);
	}
	
	public void editOpportunity(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException 
	{
		String Legal_Complexity = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Legal_Complexity");
		String Technical_Complexity = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Technical_Complexity");
	
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.moreLink, 120);
		//Reusable.WaitforC4Cloader();
		//verifyExists(C4COpportunityObj.Opportunity.moreLink, "More Link");
		click(C4COpportunityObj.Opportunity.moreLink,"More Link");
		
		//waitForElementToBeVisible(C4COpportunityObj.Opportunity.editOpportunityLnk, 120);
		//verifyExists(C4COpportunityObj.Opportunity.editOpportunityLnk, "Edit Opportunity Link");
		click(C4COpportunityObj.Opportunity.editOpportunityLnk,"Edit Opportunity Link");
		
		Reusable.WaitforC4Cloader();
		//verifyExists(C4COpportunityObj.Opportunity.legalComplexityTxb, "Legal Complexity List");
		clickByJS(C4COpportunityObj.Opportunity.legalComplexityTxb);
		ClickonElementByString("//li[normalize-space(.)='"+ Legal_Complexity +"']", 30);
		
		Reusable.WaitforC4Cloader();
		//verifyExists(C4COpportunityObj.Opportunity.techComplexityTxb, "Tech Complexity List");
		clickByJS(C4COpportunityObj.Opportunity.techComplexityTxb);
		ClickonElementByString("//li[normalize-space(.)='"+ Technical_Complexity +"']", 30);
		//pause(1500);
		
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.saveBtn);
		click(C4COpportunityObj.Opportunity.saveBtn,"Save Button");
		Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		//Reusable.waitForAjax();
		
		//waitForElementToBeVisible(C4COpportunityObj.Opportunity.headerBar, 120);
	}
	
	public void ClickToAddQuote(String User) throws InterruptedException, AWTException, IOException 
	{
		/*waitForElementToBeVisible(C4COpportunityObj.Opportunity.Scrollright, 90);
		click(C4COpportunityObj.Opportunity.Scrollright,"Scrollright");
		Reusable.Waittilljquesryupdated();
		click(C4COpportunityObj.Opportunity.Scrollright,"Scrollright");
		
		//Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.quotesLnk, 90);
		click(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");*/
		
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.Scrollright, 90);
        while(!isVisible(C4COpportunityObj.Opportunity.quotesLnk))
        {
            click(C4COpportunityObj.Opportunity.Scrollright,"Scrollright");
            Reusable.Waittilljquesryupdated();
        }               
        click(CPQQuoteCreationObj.CopyQuote.quotesLnk,"quotesLnk");
        Reusable.waitForpageloadmask();
        Reusable.Waittilljquesryupdated();
		
		//pause(6000);
		Reusable.WaitforC4Cloader(); 
		Reusable.waitForAjax();
		boolean AddQuoteBtn = verifyExists(C4COpportunityObj.Opportunity.addQuoteBtn);
		boolean ProductizedNewBtn = verifyExists(C4COpportunityObj.Opportunity.productizedNewBtn);
		if(AddQuoteBtn)
		{
			isPresent(C4COpportunityObj.Opportunity.addQuoteBtn, 60);	
			ScrollIntoViewByString(C4COpportunityObj.Opportunity.addQuoteBtn);
			//Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			click(C4COpportunityObj.Opportunity.addQuoteBtn,"Add Quote Button");
		}
		else if(ProductizedNewBtn)
		{
			ScrollIntoViewByString(C4COpportunityObj.Opportunity.productizedNewBtn);
			Reusable.Waittilljquesryupdated();
			click(C4COpportunityObj.Opportunity.productizedNewBtn,"Add Quote Button");
			
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();
			
			if(isVisible(CPQLoginObj.Login.userNameTxb)){
				if(Configuration.User_Type.equalsIgnoreCase("Sales_User"))
				{
					if(User.equalsIgnoreCase("QT_User"))
					{
						CPQLogin.CPQLogin(Configuration.QT_Username, Configuration.QT_Password);
					}
					else
					{
						CPQLogin.CPQLogin(Configuration.SalesUserCPQ_Username, Configuration.SalesUserCPQ_Password);
					}
					
				}
				else if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					if(User.equalsIgnoreCase("QT_User"))
					{
						CPQLogin.CPQLogin(Configuration.QT_Username, Configuration.QT_Password);
					}
					else
					{
						CPQLogin.CPQLogin(Configuration.Agent_Username, Configuration.Agent_Password);
					}					
				}
			}			
			
		}
				
		
	}
	
	public void NavigateToQuotes(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String Account_Name = null;
		Account_Name = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Account_Name");
		
		/*String accNameObj = C4CAccountsObj.Accounts.accountName1+ Account_Name +C4CAccountsObj.Accounts.accountName2;
		click(accNameObj,"Account Name");
		
		waitForAjax();
		String accHeaderObj = C4CAccountsObj.Accounts.accountHeader1+ Account_Name +C4CAccountsObj.Accounts.accountHeader2;
		verifyExists(accHeaderObj,"Account Header");*/
		waitForAjax();
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.quotesLnk,75);
		verifyExists(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");
		click(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");	
		//for (int i = 1; i < 3; i++) { Reusable.WaitforC4Cloader(); }		
	}
	
	public void ClickToAddModComQuote() throws InterruptedException, AWTException, IOException
	{
	/*waitForElementToBeVisible(C4COpportunityObj.Opportunity.Scrollright, 90);
	click(C4COpportunityObj.Opportunity.Scrollright,"Scrollright");
	Reusable.Waittilljquesryupdated();
	click(C4COpportunityObj.Opportunity.Scrollright,"Scrollright");
	//Reusable.WaitforC4Cloader();
	Reusable.Waittilljquesryupdated();
	waitForElementToBeVisible(C4COpportunityObj.Opportunity.quotesLnk, 90);
	click(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");*/
	waitForElementToBeVisible(C4COpportunityObj.Opportunity.Scrollright, 90);
	while(!isVisible(C4COpportunityObj.Opportunity.quotesLnk))
	{
	click(C4COpportunityObj.Opportunity.Scrollright,"Scrollright");
	Reusable.Waittilljquesryupdated();
	}
	click(CPQQuoteCreationObj.CopyQuote.quotesLnk,"quotesLnk");
	Reusable.waitForpageloadmask();
	Reusable.Waittilljquesryupdated();
	//pause(6000);
	Reusable.WaitforC4Cloader();
	Reusable.waitForAjax();
	if(!isVisible(C4COpportunityObj.Opportunity.addModComBtn)&&!isVisible(C4COpportunityObj.Opportunity.addContainerModComBtn))
	{
	isPresent(C4COpportunityObj.Opportunity.MoreBtn, 60);
	ScrollIntoViewByString(C4COpportunityObj.Opportunity.MoreBtn);
	Reusable.Waittilljquesryupdated();
	click(C4COpportunityObj.Opportunity.MoreBtn,"Click on More Button");
	}
	
	boolean AddModComBtn = verifyExists(C4COpportunityObj.Opportunity.addModComBtn);
	boolean AddContainerModComBtn = verifyExists(C4COpportunityObj.Opportunity.addContainerModComBtn);
	if(AddModComBtn)
	{
		isPresent(C4COpportunityObj.Opportunity.addModComBtn, 60);	
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.addModComBtn);
		//Reusable.WaitforC4Cloader();
		Reusable.Waittilljquesryupdated();
		click(C4COpportunityObj.Opportunity.addModComBtn,"Add Quote Button");
	}
	else if(AddContainerModComBtn)
	{
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.addContainerModComBtn);
		Reusable.Waittilljquesryupdated();
		click(C4COpportunityObj.Opportunity.addContainerModComBtn,"Add Quote Button");
	}
	Reusable.waitForpageloadmask();
	Reusable.waitForAjax();
	if(isVisible(CPQLoginObj.Login.userNameTxb))
	{
		if(Configuration.User_Type.equalsIgnoreCase("Sales_User"))
		{
			CPQLogin.CPQLogin(Configuration.SalesUserCPQ_Username, Configuration.SalesUserCPQ_Password);
		}
		else if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
		{
			CPQLogin.CPQLogin(Configuration.Agent_Username, Configuration.Agent_Password);
		}
	}
	}
}
