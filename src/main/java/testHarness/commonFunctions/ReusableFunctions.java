package testHarness.commonFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

import org.apache.poi.util.DocumentFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.relevantcodes.extentreports.LogStatus;
import com.sun.mail.util.MailSSLSocketFactory;

import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.GridHelper;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelLiabrary_Obj;
import pageObjects.siebeltoNCObjects.SiebelHubSiteObj;
import pageObjects.siebeltoNCObjects.SiebelModeObj;

import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class ReusableFunctions extends SeleniumUtils
{
	GridHelper Grid = new GridHelper();
	
	public void WaitforC4Cloader() throws IOException, InterruptedException {
		 
		int i = 1;
		Thread.sleep(1000);   
		
          try {
        	  while (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//*[contains(@title,'Please wait')]")).isDisplayed()) {   
        		  if (i > 60) { break; }
                      Thread.sleep(1000);            
                      i = i+1;
                } 
          } catch(Exception e) {
        	  Waittilljquesryupdated();
          
          }
	}
	
	public void WaitforCPQloader() throws IOException, InterruptedException {
		int i = 1;
		Thread.sleep(1000);   
		
          try {
        	  while (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//*[@id='loading-mask']")).isDisplayed()) {   
        		  if (i > 60) { break; }
                      Thread.sleep(1000);
                      System.out.println("Waiting CPQ Page to load");
                      i = i+1;
                } 
          } catch(Exception e) {
        	  Waittilljquesryupdated();
          }
	}
	
	public void Waittilljquesryupdated() throws InterruptedException, SocketTimeoutException {
		
		boolean Status = false;
		Thread.sleep(500);
		JavascriptExecutor js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(150);
				js = (JavascriptExecutor) SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get();
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
						//Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}	
	}
	
	public void waitForpageloadmask() throws InterruptedException
	{    
		boolean Status = false;
		Thread.sleep(500);
		for (int j=0; j < 10; j++) {
			try {
				try {
					  for(int i=0;i<=20;i++) {
						  while(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//div[@id='lockCreateScreen' and not(@style='display: none;')]")).isDisplayed()) {
							//	Thread.sleep(1000);
								Status = true;
	//							System.out.println("Load Mask Displayed");
						  }
					  }
				} catch(Exception e) {
					Waittilljquesryupdated();
				}
			} catch (SocketTimeoutException e1) {
				continue;
			}
			if (Status = true) { break; }
		} 
				
		//Thread.sleep(300);
	}
		
	
	public void UploadFile(String locator, String filepath) throws InterruptedException, IOException 
	{
		try
		{
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(locator)).sendKeys(filepath);
		}catch(Exception e)
		{
			Report.LogInfo("UploadFile","<font color=red"+locator +" Is not Present on Screen</font>", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, locator +" Is not Present on Screen");
			Report.logToDashboard(locator +" Is not Present on Screen");
			Assert.assertTrue(false);
		}
	}
	
	public void WaitforProdConfigLoader() throws IOException, InterruptedException 
	{
 
		boolean Status = false;
//		waiting for the page to get loaded successffully
		waitForpageloadmask();
		for (int j=0; j < 20; j++) {
			try {
				try {
					  for(int i=0;i<=20;i++) {
						  while (SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//oj-progress[@title='Completed']")).isDisplayed()) {   
								Thread.sleep(1000);
								Status = true;
						  }
					  }
				} catch(Exception e) {
					Thread.sleep(5000);
					Waittilljquesryupdated();
				}
				Waittilljquesryupdated();
			} catch (SocketTimeoutException e1) {
				continue;
			}
			if (Status = true) { break; }
		} 
	}
	
	public void setTextToGridInDesiredRowNColumn(String tableIdoRxPath, String expectedRowText, String expectedColumnText, String setValue) throws IOException
	{
		WebElement element = null;
		String columnName = null;
		String rowValue = null;
		int desiredColNo = 0;
		int desiredRowNo = 0;
		
		int numOfCols = Grid.getGridNumOfColumns(tableIdoRxPath, 1);
		int numOfRows = Grid.getGridNumOfRows(tableIdoRxPath);
		
		for(int i=1;i<=numOfCols;i++)
		{
			element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tableIdoRxPath + "/thead" + "/tr[1]/th[" + i + "]"));
				
				if (element == null)
				{
					columnName = null;
				}else
				{
					columnName = element.getAttribute("title").trim();
					if(columnName!=null)
					{
						if(expectedColumnText.equalsIgnoreCase(columnName.trim()))
						{
							desiredColNo = i;
							break;
						}
					}
				}		
		}
		
		for(int j=1;j<=numOfRows;j++)
		{
			for(int k=1;k<=numOfCols;k++)
			{				
				rowValue = Grid.getGridColumnText(tableIdoRxPath, j, k);
				
				if(rowValue!=null)
				{
					if(rowValue.contains(expectedRowText.trim()))
					{
						desiredRowNo = j;
						break;
					}
				}
			}
			
			if(desiredRowNo!=0)
			{
				break;
			}
		}		
		
		if(desiredRowNo!=0 && desiredColNo!=0)
		{
			Grid.clickInGrid(tableIdoRxPath, desiredRowNo, desiredColNo);
			sleep(1000);
			Grid.sendTextToGrid(tableIdoRxPath, desiredRowNo, desiredColNo, setValue);
			sleep(1000);
		}
		
	}	
	
	public String getTextFromGridFromDesiredRowNColumn(String tableIdoRxPath, String expectedRowText, String expectedColumnText) throws IOException
	{
		WebElement element = null;
		String columnName = null;
		String rowValue = null;
		int desiredColNo = 0;
		int desiredRowNo = 0;
		String textValue = null;
		
		int numOfCols = Grid.getGridNumOfColumns(tableIdoRxPath, 1);
		int numOfRows = Grid.getGridNumOfRows(tableIdoRxPath);
		
		for(int i=1;i<=numOfCols;i++)
		{
			element = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tableIdoRxPath + "/thead" + "/tr[1]/th[" + i + "]"));
				
				if (element == null)
				{
					columnName = null;
				}else
				{
					columnName = element.getAttribute("title").trim();
					
					if(columnName!=null)
					{
						if(expectedColumnText.equalsIgnoreCase(columnName.trim()))
						{
							desiredColNo = i;
							break;
						}
					}
				}		
		}
		
		for(int j=1;j<=numOfRows;j++)
		{
			for(int k=1;k<=numOfCols;k++)
			{				
				rowValue = Grid.getGridColumnText(tableIdoRxPath, j, k);
				
				if(rowValue!=null)
				{
					rowValue = rowValue.replaceAll("\\s", "");
					if(rowValue.toUpperCase().contains(expectedRowText.trim().toUpperCase()))
					{
						desiredRowNo = j;
						break;
					}
				}
			}
			
			if(desiredRowNo!=0)
			{
				break;
			}
		}		
		
		if(desiredRowNo!=0 && desiredColNo!=0)
		{
			textValue = Grid.getGridColumnText(tableIdoRxPath, desiredRowNo, desiredColNo);
			sleep(1000);
			
		}
		return textValue;
	}
	
	public String isFileDownloaded(String Download_Path, String File_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : isFileDownloaded
		Purpose     : This method is to check whether the file is available in the directory or not
		Input       : String Download_Path, String File_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variables
		int j = 0; int i = 0;
		
		for (j = 0; j < 10; j++) {
			File dir = new File(Download_Path);     
			File[] dir_contents = dir.listFiles();           
			for (i = 0; i < dir_contents.length; i++) {         
				if (dir_contents[i].getName().equals(File_Name)) {
					System.out.println("File name "+File_Name+" is available under the path "+Download_Path);
					ExtentTestManager.getTest().log(LogStatus.PASS, "File name "+File_Name+" is available under the path "+Download_Path);
					break;
				}
			}
			if (i >= dir_contents.length) {
				Thread.sleep(3000);
				continue;
			} else {
				break;
			}
		}
		
		if (j >=10) {
			System.out.println("File name "+File_Name+" is not available under the path "+Download_Path);
			ExtentTestManager.getTest().log(LogStatus.PASS, "File name "+File_Name+" is not available under the path "+Download_Path+" , please verify");
			return "False";
		}
		
		
		return "True";
		
	}
	
	public String deleteFile(String Download_Path, String Complete_Path) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : deleteFile
		Purpose     : This method is to delete the file which downloaded earlier
		Input       : String Download_Path, String File_Name
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variables
		int i = 0;
		String File_Name = Complete_Path.split("\\\\")[Complete_Path.split("\\\\").length-1].trim();
		//String Download_Path = System.getProperty("user.dir")+"\\src\\Data\\Downloads";
		File dir = new File(Download_Path);     
		File[] dir_contents = dir.listFiles();           
		for (i = 0; i < dir_contents.length; i++) {         
			if (dir_contents[i].getName().equals(File_Name)) {
				dir_contents[i].delete();
				Thread.sleep(2000);
				System.out.println("File name "+File_Name+" was deleted under the path "+File_Name);
				ExtentTestManager.getTest().log(LogStatus.PASS, "File name "+File_Name+" was deleted under the path "+File_Name);
				break;
			}
		}		
		return "True";
		
	}
	
	public String ValidateExploreDetails(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue,int RowNumber) throws IOException, InterruptedException 
	{
		
		int row_number = 0; String tXpath = null, Row_Val;		
		
		if (waitForElementToBeVisible(CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTbl_CommPge, 35)) {
			ScrollIntoViewByString((CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTbl_CommPge));
			WebElement lineItem = findWebElement(CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTbl_CommPge);
			String sXpath = lineItem.toString();			
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
			System.out.println("tXpath: "+tXpath);
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.LogInfo("lineItemGridTable", "lineItemGridTable was not visible in CPQ Main Page, Please Verify", "FAIL");
			Report.logToDashboard("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			return "False";
		}		

//		getting the row and column number
		WebElement lineItemTab = findWebElement(CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTbl_CommPge);
		System.out.println("lineItem Tab: "+lineItemTab);

		List<WebElement> rows = lineItemTab.findElements(By.tagName("tr"));	
		List<WebElement> columns = lineItemTab.findElements(By.tagName("td"));
		int tot_row = rows.size();	
		System.out.println("tot_row: "+tot_row);
		int tot_col = columns.size();		
		System.out.println("Total Column size is "+tot_col);
		int iCol, iRow, rColumn_number = 0, aColumn_number = 0;		
		
		
//          Reading the column headers of table and set the column number with use of reference
				for(iCol = 1; iCol <= tot_col-1; iCol++){
					String Col_Val = columns.get(iCol).getText().trim();
					if (Col_Val.equals(refColumn)){ 
						rColumn_number = iCol+1; 
						
						break; 
					}
				}
				System.out.println("ref Column number is "+rColumn_number); 
		
//				Returns the function of reference column number
				if (iCol >= tot_col) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify ");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					return "False";
				}		
				
				System.out.println("Action Type value "+ActionType);
				System.out.println("Total Column size is "+tot_col);
		
				if (!ActionType.equals("Click")) {
					//Reading the actual column number ff table and set the column number with use of reference
					for(iCol = 1; iCol <= tot_col-1; iCol++){
						String Col_Val = columns.get(iCol).getText().trim();
						System.out.println("Column Value is "+Col_Val); 
						System.out.println("Act Column Value is "+actColumn); 
						if (Col_Val.contains(actColumn)){ 
							aColumn_number = iCol+1; 
//							System.out.println("act Column number is "+aColumn_number); 
							break; 
						}
					}
					System.out.println("act Column number is "+aColumn_number); 
				
//					Returns the function of column names are not matched
					if (iCol >= tot_col) {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify");
						//System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
						Report.LogInfo("Column Name", "Column Name "+actColumn+" is not found in the webtable, Please Verify", "FAIL");
						Report.logToDashboard("Column Name "+actColumn+" is not found in the webtable, Please Verify");
						return "False";
					}
				}
		
		//Taking the row value
		WebElement rowValue; 
		for(iRow = RowNumber; iRow <= tot_row; iRow++){			
			try {
				//int colValue=rColumn_number+1;
				rowValue = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//tr["+iRow+"]//td["+rColumn_number+"]"));
				Row_Val = rowValue.getText();
				System.out.println("Row_Val: "+Row_Val);
				
				if (Row_Val.toString().equalsIgnoreCase(rowRef)){ 
					row_number = iRow;
					System.out.println("iRow: "+iRow);
					System.out.println("row_number: "+row_number);
					System.out.println("row_Ref: "+rowRef);
					break;
				}
			} catch (Exception e) {
				continue;
			}
		}


//		Returns the function if rows names are not matched
		if (iRow > tot_row) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Key "+rowRef+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.LogInfo("Row Key", "Row Key "+rowRef+" is not found in the webtable, Please Verify", "FAIL");
			Report.logToDashboard("Row Key "+rowRef+" is not found in the webtable, Please Verify");
			return "False";
		}
		
		waitForpageloadmask();
		
		WebElement Cell; String sOut = null;
		switch (ActionType) {
			case "GetValue":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//tr["+row_number+"]//td["+aColumn_number+"]"));
				scrollIntoView(Cell);
				String Value = Cell.getText();
				sOut = Value;
				break;

		}
		
		return sOut;
	}
	
	public String WebTableCellAction(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue) throws IOException, InterruptedException 
	{
				
			int row_number = 0; String tXpath = null, Row_Val;
			
			if(rowRef.equalsIgnoreCase("Colt Ip Domain")) {rowRef="IP Domain";}
			else if(rowRef.equalsIgnoreCase("Colt Ip Guardian")) {rowRef="IP Guardian";}
			else if(rowRef.equalsIgnoreCase("Colt Managed Virtual Firewall")) {rowRef="IP Managed Virtual Firewall";}
			else if(rowRef.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {rowRef="IP Managed Dedicated Firewall";}
			else if(rowRef.equalsIgnoreCase("CPESolutionsSite")) {rowRef="CPE Solutions Site";}
			else if(rowRef.equalsIgnoreCase("ColtIpAccess")) {rowRef="Colt IP Access";}
			else if(rowRef.equalsIgnoreCase("Colt Ip Access")) {rowRef="Colt IP Access";}
			else { rowRef.replaceAll("(?!^)([A-Z])", " $1"); }
			
			if (waitForElementToBeVisible(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable, 35)) {
				ScrollIntoViewByString((CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable));
				WebElement lineItem = findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable);
				String sXpath = lineItem.toString();			
				tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
				System.out.println("tXpath: "+tXpath);
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "lineItemGridTable was not visible in CPQ Main Page, Please Verify");
				//System.out.println("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Report.LogInfo("lineItemGridTable", "lineItemGridTable was not visible in CPQ Main Page, Please Verify", "FAIL");
				Report.logToDashboard("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
				return "False";
			}
					
			System.out.println("rowref: "+rowRef);
//			getting the row and column number
			List<WebElement> rows = findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable).findElements(By.xpath("//div[@role='rowgroup'][2]//div[@role='row']"));
			List<WebElement> columns = rows.get(0).findElements(By.xpath("//div[@aria-label='Line Item Table']//div[@class='cpq-table-header-row oj-table-header-row']/div[@role='columnheader']"));
			int tot_row = rows.size();
			int tot_col = columns.size();		
			System.out.println("Total Column size is "+tot_col);
			int iCol, iRow, rColumn_number = 0, aColumn_number = 0;		
			
			
//	          Reading the column headers of table and set the column number with use of reference
					for(iCol = 2; iCol <= tot_col-1; iCol++){
						//String Col_Valobjmain = columns.get(iCol).toString();
						//String Col_Valobjsub = Col_Valobjmain.substring(0, Col_Valobjmain.length() - 1);
						String Col_Valobj = "@xpath=//div[@aria-label='Line Item Table']//div[@class='cpq-table-header-row oj-table-header-row']/div[@role='columnheader']["+iCol+"]/div[@class='oj-table-column-header']/div[@title]";
						System.out.println("Title "+Col_Valobj);
						ScrollIntoViewByString(Col_Valobj);
						String Col_Val = getAttributeFrom(Col_Valobj, "title");
						//WebElement Col_Valmain = webDriver.findElement(By.xpath(Col_Valobj));
						//String Col_Val = Col_Valobj.getAttribute("title").trim();
						if (Col_Val.equals(refColumn)){ 
							rColumn_number = iCol; 
							
							break; 
						}
					}
					System.out.println("ref Column number is "+rColumn_number); 
			
//					Returns the function of reference column number
					if (iCol >= tot_col) {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify");
						//System.out.println("Column Name "+refColumn+" is not found in the webtable, Please Verify ");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
						Report.LogInfo("Column Name", "Column Name "+refColumn+" is not found in the webtable, Please Verify", "FAIL");
						Report.logToDashboard("Column Name "+refColumn+" is not found in the webtable, Please Verify");
						return "False";
					}		
					
				//	System.out.println("Action Type value "+ActionType);
				//	System.out.println("Total Column size is "+tot_col);
			
					if (!ActionType.equals("Click")) {
						//Reading the actual column number ff table and set the column number with use of reference
						for(iCol = 2; iCol <= tot_col-1; iCol++){
						//	String Col_Val = columns.get(iCol).getAttribute("title").trim();
							String Col_Valobj = "@xpath=//div[@aria-label='Line Item Table']//div[@class='cpq-table-header-row oj-table-header-row']/div[@role='columnheader']["+iCol+"]/div[@class='oj-table-column-header']/div[@title]";
							System.out.println("Title "+Col_Valobj);
							ScrollIntoViewByString(Col_Valobj);
							String Col_Val = getAttributeFrom(Col_Valobj, "title");
							
							System.out.println("Column Value is "+Col_Val); 
							System.out.println("Act Column Value is "+actColumn); 
							if (Col_Val.contains(actColumn)){ 
								aColumn_number = iCol+1; 
//								System.out.println("act Column number is "+aColumn_number); 
								break; 
							}
						}
						System.out.println("act Column number is "+aColumn_number); 
					
//						Returns the function of column names are not matched
						if (iCol >= tot_col) {
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
							//System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
							ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
							Report.LogInfo("Column Name", "Column Name "+refColumn+" is not found in the webtable, Please Verify", "FAIL");
							Report.logToDashboard("Column Name "+refColumn+" is not found in the webtable, Please Verify");
							return "False";
						}
					}
			
			//Taking the row value
			for(iRow =1; iRow <= tot_row; iRow++){
				int colValue=rColumn_number;
				WebElement rowValue = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//div["+colValue+"]//span[@title]"));
				Row_Val = rowValue.getAttribute("title").trim();
				System.out.println("Row_Val "+Row_Val);
				if (Row_Val.equalsIgnoreCase(rowRef)){ 
					row_number = iRow; 
//					System.out.println("Row number is "+row_number);
					break;
				}
			}
			

//			Returns the function if rows names are not matched
			if (iRow > tot_row) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Key"+rowRef+" is not found in the webtable, Please Verify");
				//System.out.println("Row Key "+rowRef+" is not found in the webtable, Please Verify ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				Report.LogInfo("Row Key", "Row Key"+rowRef+" is not found in the webtable, Please Verify", "FAIL");
				Report.logToDashboard("Row Key"+rowRef+" is not found in the webtable, Please Verify");
				return "False";
			}
			
			waitForpageloadmask();
	
			
			WebElement Cell; String sOut = null;
			switch (ActionType) {
				case "Edit":
					Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
					scrollIntoView(Cell);
					click(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]");
					//WebElement edit_Box = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
//					WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
					sendKeys(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//input", ActionValue);
					sOut = "True";
					break;
				case "Select":
					Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
					scrollIntoView(Cell);
//					Cell.click();
					javaScriptClickWE(Cell,"Select the specific row");
					WebElement dropdown = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::ul[@role='listbox']"));
					List<WebElement> options = dropdown.findElements(By.tagName("li"));
					for (WebElement option : options)
					{
					    if (option.getText().equals(ActionValue))
					    {
					        option.click(); // click the desired option
					        break;
					    }
					}
					sOut = "True";
					break;
				case "Store":
					Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//div[3]//span[@title]"));
					scrollIntoView(Cell);
					sOut = Cell.getAttribute("title");
					break;
					
				case "Click":
					Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
					scrollIntoView(Cell);
//					Cell.click();
					javaScriptClickWE(Cell,"Select the specific row");
					sOut = "True";
					break;
			}
			
			return sOut;
		}
	
	public String MultiLineWebTableCellAction(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue,int RowNumber) throws IOException, InterruptedException 
	{
		
		int row_number = 0; String tXpath = null, Row_Val;
		if(rowRef.equalsIgnoreCase("Colt Ethernet Hub")||rowRef.equalsIgnoreCase("Colt Ethernet Spoke")) {rowRef="Colt Ethernet Hub and Spoke";}
		else if(rowRef.equalsIgnoreCase("Colt Ip Domain")) {rowRef="IP Domain";}
		else if(rowRef.equalsIgnoreCase("Colt Ip Guardian")) {rowRef="IP Guardian";}
		else if(rowRef.equalsIgnoreCase("Colt Managed Virtual Firewall")) {rowRef="IP Managed Virtual Firewall";}
		else if(rowRef.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {rowRef="IP Managed Dedicated Firewall";}
		else if(rowRef.equalsIgnoreCase("CPESolutionsSite")) {rowRef="CPE Solutions Site";}
		else if(rowRef.equalsIgnoreCase("ColtIpAccess")) {rowRef="Colt IP Access";}
		else if(rowRef.equalsIgnoreCase("Colt Ip Access")) {rowRef="Colt IP Access";}
		//else { rowRef = rowRef.replaceAll("(?!^)([A-Z])", " $1"); }
		
		if (waitForElementToBeVisible(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable, 35)) {
			ScrollIntoViewByString((CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable));
			WebElement lineItem = findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable);
			String sXpath = lineItem.toString();			
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
			System.out.println("tXpath: "+tXpath);
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			//System.out.println("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.LogInfo("lineItemGridTable", "lineItemGridTable was not visible in CPQ Main Page, Please Verify", "FAIL");
			Report.logToDashboard("lineItemGridTable was not visible in CPQ Main Page, Please Verify");
			return "False";
		}
		

		System.out.println("rowref: "+rowRef);
//		getting the row and column number
		List<WebElement> rows = findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable).findElements(By.xpath("//div[@role='rowgroup'][2]//div[@role='row']"));
		List<WebElement> columns = rows.get(0).findElements(By.xpath("//div[@aria-label='Line Item Table']//div[@class='cpq-table-header-row oj-table-header-row']/div[@role='columnheader']"));
		int tot_row = rows.size();
		int tot_col = columns.size();		
		System.out.println("Total Column size is "+tot_col);
		int iCol, iRow, rColumn_number = 0, aColumn_number = 0;		
		
		
//          Reading the column headers of table and set the column number with use of reference
				for(iCol = 2; iCol <= tot_col-1; iCol++){
					//String Col_Valobjmain = columns.get(iCol).toString();
					//String Col_Valobjsub = Col_Valobjmain.substring(0, Col_Valobjmain.length() - 1);
					String Col_Valobj = "@xpath=//div[@aria-label='Line Item Table']//div[@class='cpq-table-header-row oj-table-header-row']/div[@role='columnheader']["+iCol+"]/div[@class='oj-table-column-header']/div[@title]";
					System.out.println("Title "+Col_Valobj);
					ScrollIntoViewByString(Col_Valobj);
					String Col_Val = getAttributeFrom(Col_Valobj, "title");
					//WebElement Col_Valmain = webDriver.findElement(By.xpath(Col_Valobj));
					//String Col_Val = Col_Valobj.getAttribute("title").trim();
					if (Col_Val.equals(refColumn)){ 
						rColumn_number = iCol; 
						
						break; 
					}
				}
				System.out.println("ref Column number is "+rColumn_number); 
		
//				Returns the function of reference column number
				if (iCol >= tot_col) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify");
					//System.out.println("Column Name "+refColumn+" is not found in the webtable, Please Verify ");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					Report.LogInfo("Column Name", "Column Name "+refColumn+" is not found in the webtable, Please Verify", "FAIL");
					Report.logToDashboard("Column Name "+refColumn+" is not found in the webtable, Please Verify");
					return "False";
				}		
				
				System.out.println("Action Type value "+ActionType);
				System.out.println("Total Column size is "+tot_col);
		
				if (!ActionType.equals("Click")) {
					//Reading the actual column number ff table and set the column number with use of reference
					for(iCol = 2; iCol <= tot_col-1; iCol++){
					//	String Col_Val = columns.get(iCol).getAttribute("title").trim();
						String Col_Valobj = "@xpath=//div[@aria-label='Line Item Table']//div[@class='cpq-table-header-row oj-table-header-row']/div[@role='columnheader']["+iCol+"]/div[@class='oj-table-column-header']/div[@title]";
						System.out.println("Title "+Col_Valobj);
						ScrollIntoViewByString(Col_Valobj);
						String Col_Val = getAttributeFrom(Col_Valobj, "title");
						
						System.out.println("Column Value is "+Col_Val); 
						System.out.println("Act Column Value is "+actColumn); 
						if (Col_Val.contains(actColumn)){ 
							aColumn_number = iCol+1; 
//							System.out.println("act Column number is "+aColumn_number); 
							break; 
						}
					}
					System.out.println("act Column number is "+aColumn_number); 
				
//					Returns the function of column names are not matched
					if (iCol >= tot_col) {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
						//System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
						ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
						Report.LogInfo("Column Name", "Column Name "+refColumn+" is not found in the webtable, Please Verify", "FAIL");
						Report.logToDashboard("Column Name "+refColumn+" is not found in the webtable, Please Verify");
						return "False";
					}
				}
		
		//Taking the row value
		for(iRow =RowNumber; iRow <= tot_row; iRow++){
			int colValue=rColumn_number;
			WebElement rowValue = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//div["+colValue+"]//span[@title]"));
			Row_Val = rowValue.getAttribute("title").trim();
			System.out.println("Row_Val "+Row_Val);
			if (Row_Val.equalsIgnoreCase(rowRef)){ 
				row_number = iRow; 
//				System.out.println("Row number is "+row_number);
				break;
			}
		}
		

//		Returns the function if rows names are not matched
		if (iRow > tot_row) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Key"+rowRef+" is not found in the webtable, Please Verify");
			//System.out.println("Row Key "+rowRef+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			Report.LogInfo("Row Key", "Row Key"+rowRef+" is not found in the webtable, Please Verify", "FAIL");
			Report.logToDashboard("Row Key"+rowRef+" is not found in the webtable, Please Verify");
			return "False";
		}
		
		waitForpageloadmask();
		
		WebElement Cell; String sOut = null;
		int colValue=aColumn_number+1;
		switch (ActionType) {
			case "Edit":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
				scrollIntoView(Cell);
				click(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]");
				//WebElement edit_Box = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
//				WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				sendKeys(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//input", ActionValue);
				sOut = "True";
				break;

				
			case "Select":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
				scrollIntoView(Cell);
//				Cell.click();
				javaScriptClickWE(Cell,"Select the specific row");
				WebElement dropdown = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::ul[@role='listbox']"));
				List<WebElement> options = dropdown.findElements(By.tagName("li"));
				for (WebElement option : options)
				{
				    if (option.getText().equals(ActionValue))
				    {
				        option.click(); // click the desired option
				        break;
				    }
				}
				sOut = "True";
				break;
		
			case "Store":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//div["+colValue+"]//span[@title]"));
				scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
		
			case "Click":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
				scrollIntoView(Cell);
//				Cell.click();
				javaScriptClickWE(Cell,"Select the specific row");
				sOut = "True";
				break;
						
			case "Date":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
				scrollIntoView(Cell);
//				Cell.click();
				javaScriptClickWE(Cell,"Select the specific row");
				clickByAction(CPQQuoteApprovalsObj.QuoteApprovals.defaultDateElem);
				sOut = "True";
				break;
				
			case "GetRow":
				System.out.println("Inside GetRow Switch Case");
				sOut = Integer.toString(row_number);				
				System.out.println("sOut: "+sOut);
				break;
				
			case "LIGVerification":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]"));
				if(Cell.isDisplayed())
				{
					sOut = "True";
					ExtentTestManager.getTest().log(LogStatus.PASS, "Line Item is found in LIG with the specified Feauture" );
					System.out.println("Line Item is found in LIG with the specified Feauture");
				}
				else
				{
					sOut = "False";
					ExtentTestManager.getTest().log(LogStatus.PASS, "Line Item is not found in LIG with the specified Feauture" );
					System.out.println("Line Item is not found in LIG with the specified Feauture");
				}
				break;
			case "GetValue":
				Cell = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(tXpath+"//div[@role='rowgroup'][2]//div[@role='row']["+iRow+"]//div["+colValue+"]//span[@title]"));
				scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");			
				break;
				
		}
		
		return sOut;
	}
	
	///New added
	public void waitForSiebelLoader() throws InterruptedException, IOException
	{
	int i = 1;
	Thread.sleep(1000);
	WebElement el = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//html"));
	try {
	while (el.getAttribute("class").contains("siebui-busy")){
	if (i > 300) { break; }
	Thread.sleep(2000);
	i = i+1;
	}
	} catch(Exception e) {
	Waittilljquesryupdated();
	}
	}
	public String CurrentDate()
	{
	Date date = new Date();
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	return (formatter.format(date));

	 }
	public void SendkeaboardKeys(String locator, Keys k) throws InterruptedException, IOException
	{
	WebElement el= findWebElement(locator);
	el.sendKeys(k);
	}
	public void savePage() throws InterruptedException, IOException
	{
	if (isVisible(By.xpath("(//a[contains(text(),'Click here to')])[1]"))){
	click(SiebelLiabrary_Obj.serviceOrder.ClickheretoSaveAccess,"Click on Save link");
	waitForSiebelLoader();
	}else{
	Actions keyAction = new Actions(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get());
	keyAction.keyDown(Keys.CONTROL).sendKeys("s").keyUp(Keys.CONTROL).perform();
	}
	waitForSiebelLoader();
	}
	public void waitForSiebelSpinnerToDisappear() throws InterruptedException, IOException
	{
	waitForElementToDisappear(SiebelLiabrary_Obj.serviceOrder.Spinner,60);
	}
	public void alertPopUp() throws DocumentFormatException, InterruptedException, IOException
	{
	if (isVisible((SiebelLiabrary_Obj.serviceOrder.AlertAccept))) {
	verifyExists(SiebelLiabrary_Obj.serviceOrder.AlertAccept,"AlertAccept");
	click(SiebelLiabrary_Obj.serviceOrder.AlertAccept,"AlertAccept");
	waitForSiebelLoader();
	}
	if (isVisible((SiebelLiabrary_Obj.serviceOrder.AlertAccept1))) {
	verifyExists(SiebelLiabrary_Obj.serviceOrder.AlertAccept1,"AlertAccept");
	click(SiebelLiabrary_Obj.serviceOrder.AlertAccept1,"AlertAccept");
	waitForSiebelLoader();
	}
	if (isVisible((SiebelLiabrary_Obj.serviceOrder.AlertAccept2))) 
	{
		verifyExists(SiebelLiabrary_Obj.serviceOrder.AlertAccept2,"AlertAccept");
		click(SiebelLiabrary_Obj.serviceOrder.AlertAccept2,"AlertAccept");
		waitForSiebelLoader();
	}
	if (isVisible((SiebelLiabrary_Obj.serviceOrder.AlertAccept3))) {
		verifyExists(SiebelLiabrary_Obj.serviceOrder.AlertAccept3,"AlertAccept");
		click(SiebelLiabrary_Obj.serviceOrder.AlertAccept3,"AlertAccept");
		waitForSiebelLoader();
	}

	}
	public void SelectTodaysDate(String locator, String ObjectName ) throws IOException, InterruptedException
	{
	// verifyExists(locator,ObjectName);
	click(locator,ObjectName);
	waitForElementToAppear(SiebelLiabrary_Obj.EnterDateInFooter.CalendarNowBtn, 30);
	click(SiebelLiabrary_Obj.EnterDateInFooter.CalendarNowBtn,"Now button");
	waitForSiebelLoader();
	verifyExists(SiebelLiabrary_Obj.EnterDateInFooter.TodayDate,"Todays Date");
	click(SiebelLiabrary_Obj.EnterDateInFooter.TodayDate,"Todays Date");
	waitForSiebelLoader();
	}
	
///////nmts
	
	public void switchtodefault(){
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().defaultContent();

		}

		public void switchtoframe(String frameName){
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().frame(frameName);

		}

		public void Select(String locator, String value) throws IOException, InterruptedException
		{
		WebElement el= findWebElement(locator);
		Select s1 = new Select(el);
		s1.selectByVisibleText(value);
		}


		public void switchtofram(WebElement el){
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().frame(el);

		}
		
		///// added
		public void ClickContinue() throws DocumentFormatException, InterruptedException, IOException {
			if (isVisible(SiebelAddProdcutObj.TechnicalValidation.BlankPriceContinue)) {
				verifyExists(SiebelAddProdcutObj.TechnicalValidation.BlankPriceContinue,"BlankPriceContinue");
				click(SiebelAddProdcutObj.TechnicalValidation.BlankPriceContinue,"BlankPriceContinue");
			}
			waitForSiebelLoader();
		}
		
		public void Save() throws InterruptedException, DocumentFormatException {
			try
			{
				verifyExists(SiebelAddProdcutObj.Save.Save,"Save button");
				waitForSiebelLoader();
					
			if(isVisible(SiebelAddProdcutObj.Save.Save))
			{
			
			click(SiebelAddProdcutObj.Save.Save,"Save button");
			waitForSiebelLoader();
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		public void AEndInputEnter(String TextBoxName, String InputText) throws InterruptedException, IOException
		{
			waitToPageLoad();
			waitForSiebelLoader();
		//	verifyExists(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput,"SiteA Input");
			String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", TextBoxName);
			click(eleLoct);
			sendKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", TextBoxName),InputText);
			SendkeaboardKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", TextBoxName),Keys.ENTER);	
		}

		public void BEndInputEnter(String TextBoxName, String InputText) throws InterruptedException, IOException
		{
			waitToPageLoad();
			waitForSiebelLoader();
		//	verifyExists(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput,"SiteB Input");
			String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", TextBoxName);
			click(eleLoct);
			sendKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", TextBoxName),InputText);
			SendkeaboardKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", TextBoxName),Keys.ENTER);	
		}

		public void AEndDropdownSelection(String DropdownName, String DropdownValue) throws InterruptedException,IOException
		{
			String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value", DropdownName);
			waitForElementToAppear(eleLoct, 10);
			click(eleLoct);
//			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) 
//			{
//				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				waitForSiebelLoader();
//			}
			eleLoct= SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value", DropdownValue);
			waitForSiebelLoader();
			waitForElementToAppear(eleLoct, 10);
			click(eleLoct);
//			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) 
//			{
//				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				waitForSiebelLoader();
//			}
		}

		public void BEndDropdownSelection(String DropdownName, String DropdownValue) throws InterruptedException, IOException
		{
			String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value", DropdownName);
			waitForElementToAppear(eleLoct, 10);
			click(eleLoct);
//			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) 
//			{
//				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				waitForSiebelLoader();
//			}
			eleLoct= SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value", DropdownValue);
			waitForSiebelLoader();
			waitForElementToAppear(eleLoct, 10);
			click(eleLoct);
//			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) 
//			{
//				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
//				waitForSiebelLoader();
//			}
		}

		public void ClickHereSave() throws InterruptedException, DocumentFormatException 
		{
			try 
			{		
				verifyExists(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickheretoSaveAccess,"Click here to Save Access");
				click(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickheretoSaveAccess,"Click on SaveAccess");
				waitForSiebelLoader();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		public void SelectDropDownValue(String DropdownName, String DropdownValue) throws InterruptedException, IOException
		{
			String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.DropdownClick.replace("Value", DropdownName);
			waitForElementToAppear(eleLoct, 10);
			click(eleLoct);
			waitForSiebelSpinnerToDisappear();
			eleLoct= SiebelAddProdcutObj.ModTechModCommWaveAndLine.DropDownValue.replace("Value", DropdownValue);
			waitForElementToAppear(eleLoct, 10);
			click(eleLoct);
			waitForSiebelSpinnerToDisappear();
		}

		public void pickCalenderDateforCease(String locator, String ObjectName) throws InterruptedException, IOException 
		{
			WebElement el= findWebElement(locator);
			
			Calendar cal  = Calendar.getInstance();
		    cal.add(Calendar.DATE, -5);

		    SimpleDateFormat s = new SimpleDateFormat("d-MMM-yyyy");
		    String result = s.format(new Date(cal.getTimeInMillis()));
		    System.out.println(result);
		    
		    String[] valueDate = result.split("-"); 
		    System.out.println(valueDate[0]);
		    System.out.println(valueDate[1]);
		    System.out.println(valueDate[2]);
		    
		    el.click();
		    Thread.sleep(5000);
			Select year = new Select(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//select[@aria-label='Year']")));
			Select month = new Select(SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//select[@aria-label='Month']")));
			year.selectByVisibleText(valueDate[2]);
			month.selectByVisibleText(valueDate[1]);
			Thread.sleep(5000);
			click(("@xpath=//*[text()='"+valueDate[0]+"']"),"Check");
			Thread.sleep(5000);
		}
		
		public boolean isDisplayed(String locator)
		{
			try
			{
				SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(locator));
				//Log.info("Element Found: True");
				Report.LogInfo("isDisplay","Element Found: True", "INFO");	
				return SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath(locator)).isDisplayed();
			} catch (NoSuchElementException e)
			{
			//	Log.info("Element Found: False");
				Report.LogInfo("isDisplay","Element Found: False", "INFO");	
				return false;
			}
		}

		public void waitforAttributeloader() throws InterruptedException
		{
			Thread.sleep(2000);

			while (isDisplayed(
					"//i[contains(@class,'colt-status-spinner') and contains(@class,'colt-attr-status-icon') and contains(@class,'fa-spinner')]"))

			{
				Thread.sleep(1000);
			}
		}

		public void closePopUp() throws Exception 
		{
			verifyExists(SiebelModeObj.R5DataCoverage.PopClose,"Popup Close");
			click(SiebelModeObj.R5DataCoverage.PopClose,"Popup Close");
			waitForSiebelSpinnerToDisappear();
			waitForSiebelLoader();
		}

		public void Select1(String locator, String value) throws IOException, InterruptedException
		{
			WebElement el= findWebElement(locator);
			Select s1 = new Select(el);
			s1.selectByVisibleText(value);
		}

		public String FutureDate(int value)
		{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		    Calendar c = Calendar.getInstance();
		    c.setTime(new Date());
		    c.add(Calendar.DATE, value); 
		    return (formatter.format(c.getTime()));
		}

		public void Pagerefresh() throws InterruptedException
		{
			SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().navigate().refresh();
		}

}