package testHarness.ncFunctions;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.CarNorObj;
import pageObjects.ncObjects.CeaseOrderObj;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class CeaseOrder extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	IpAccessCeaseOrder IpCease = new IpAccessCeaseOrder();
	AccountNavigation NcAccounts = new AccountNavigation();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();
	
	public void NcCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
		
		if (ProductName.toString().contains("Line"))
		{
			CreatCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			CreatSpokeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatHubCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditHubProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("IPA"))
		{
			IpCease.CreateIpAccessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.UpdateDescription();
			IpCease.EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("IPVPN"))
		{
			
		}
		else if(ProductName.toString().contains("EVPN"))
		{
//			NcAccounts.GotoAccount1(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			NcAccounts.GotoAccount1(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatDummyCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
	}

	public void CreatCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke/Endpoint Order Number");

    	Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");			

		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,Ordernumber,"FilterInputValue");		

		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		
		String CompOrdernumber=Ordernumber;
		String[] arrOfStr = CompOrdernumber.split("#", 0);


//		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1,60,10000);
//		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*",10);		

//		waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",10);

//		waitRefreshForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);

		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*",10);		

		String ProductInstancenumber=getTextFrom(CeaseOrderObj.CeaseOrder.ProductInstNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ProductInstNumber", ProductInstancenumber);

		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 

		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		

		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,arrOfStr1[1],"FilterInputValue");	
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");			
		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String CeaseOrdernumber=getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_No", CeaseOrdernumber);

		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		//Log.info(arrOfStr2[1]);
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL", CeaseOrderscreenURL);
	}
    
	public void CreatDummyCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order Number");

    	Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");			

		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,Ordernumber,"FilterInputValue");		

		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");

		//selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Selected Name in Filter ");	
		String CompOrdernumber=Ordernumber;
		//Log.info(CompOrdernumber.get());
		String[] arrOfStr = CompOrdernumber.split("#", 0);
		//Log.info(arrOfStr[1]);
		waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",10);
		click("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","Click");		
		
		String ProductInstancenumber=getTextFrom(CeaseOrderObj.CeaseOrder.ProductInstNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ProductInstancenumber", ProductInstancenumber);

		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 

		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		

		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,arrOfStr1[1],"FilterInputValue");	
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
//		click("@xpath=//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input","Click");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");			
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		String CeaseOrdernumber=getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo,"Cease_Order_No", CeaseOrdernumber);

		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		//Log.info(arrOfStr2[1]);
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL",CeaseOrderscreenURL);
	}

	public void UpdateDescription() throws Exception
	{
		verifyExists(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"General Information Tab");
		click(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
        
		verifyExists(CeaseOrderObj.CeaseOrder.EditDesc,"Edit description");
		click(CeaseOrderObj.CeaseOrder.EditDesc,"Edit description");
		Reusable.waitForpageloadmask();
		
		verifyExists(CeaseOrderObj.CeaseOrder.OrderDescription,"Order Description");
		sendKeys(CeaseOrderObj.CeaseOrder.OrderDescription,"Cease Order created using CTAF Automation script","Order Description");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Update,"Update");
		click(CeaseOrderObj.CeaseOrder.Update,"Update");
		Reusable.waitForAjax();

	}

	public void EditProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
    	String CeaseOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
    	
 		getUrl(CeaseOrderscreenURL);
 		
 		verifyExists(CeaseOrderObj.CeaseOrder.DisconnectOrder,"DisconnectOrder");
		click(CeaseOrderObj.CeaseOrder.DisconnectOrder,"DisconnectOrder");	 		
		
		verifyExists(CeaseOrderObj.CeaseOrder.Edit,"Edit");
		click(CeaseOrderObj.CeaseOrder.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
        Reusable.waitForpageloadmask();
	}			
    
    
	public void DecomposeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
 		
		verifyExists(CeaseOrderObj.CeaseOrder.Suborder,"Suborder");
		click(CeaseOrderObj.CeaseOrder.Suborder,"Update");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	}
	

	public void ProcessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_No");		
		String CeaseOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
		
 		getUrl(CeaseOrderscreenURL);

 		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
//		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/parent::*/td//input",10);
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		click(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
	}

	public void CompleteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
		String CeaseOrdernumber =DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_No");
		String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Type of Device"); 
		String Comments = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
		String ProductType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		String Workitem = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Workitem"); 

		getUrl(CeaseOrderscreenURL);					  
		click(CeaseOrderObj.CeaseOrder.TaskTab,"Task Tab");	

		click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"Execution flow link");	
		Reusable.waitForSiebelLoader();

		click(CeaseOrderObj.CeaseOrder.Workitems,"Work items");	
		Reusable.waitForAjax();
		
		if(ProductType.equalsIgnoreCase("EVPN"))
		{
			for (int k=1; k<=Integer.parseInt(Workitem.toString());k++)
			{
				waitRefreshForElementToAppear(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,60,20000);
				click(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,"Task ready to complete");
				Completeworkitem(GetText(CeaseOrderObj.CeaseOrder.DisconnectTaskTitle),testDataFile, sheetName, scriptNo, dataSetNo);
			}
		}

		getUrl(CeaseOrderscreenURL);
        
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
        
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder4);

	    if(OrderStatus.equalsIgnoreCase("Process Started"))
	    {
		    String ErrorStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.BlockedbyError);
	    	if (ErrorStatus.equalsIgnoreCase("Blocked By Errors"))
			{
		    	if (DeviceType.contains("Stub"))
				{
//			    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2);
					getUrl(CeaseOrderscreenURL);					  
				
					verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					
					verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					
					waitForAjax();
					
					verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					
					waitForAjax();
				
					if(verifyExists("@xpath=//a[text()='Delete Service in Smarts']"))
					{
						if (!ProductType.equalsIgnoreCase("EVPN"))
						{
							ErrorWorkItems.DeleteSmartserror("Delete Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
						}
						else
						{
							for(int i=1;i<=2;i++)
							{
								ErrorWorkItems.DeleteSmartserror("Delete Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
							}
						}
	
						getUrl(CeaseOrderscreenURL);
				        
						click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
						Reusable.waitForAjax();
				        
						verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						
						selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
						
						sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
						
						verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						
						waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
						if (OrderStatus.contains("Process Completed"))
						{
							Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
							ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
						}
					}			
					else
					{
						System.out.println("Order is blocked by different errors");
					}
				}
			}
		}    
	    else if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Execution InProgress");
		}
	}
	
    public void Completeworkitem(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
    	
    	//Log.info("In Switch case with TaskName :"+taskname);
		switch(taskname[0])
		{
			case "Legacy Deactivation Completed":
			{
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				Reusable.waitForpageloadmask();
			}
			break;
			
			case "OAM Monitoring Leg Disconnect Decision":
			{
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				Reusable.waitForpageloadmask();
			}
			break;
		}
    }
    
    public void GotoErrors(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{

    	// Check the Inputdata3 variable
    			String CeaseOrdernumber =DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_No");
    			String CeaseOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
    			String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
    	 		getUrl(CeaseOrderscreenURL);

    	 		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");	
    			click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");
				String Errors= getTextFrom("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/following-sibling::*[4]");
				if(Errors.equalsIgnoreCase("Blocked by Errors"))
				{
					ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*", 10);
					click(CeaseOrderObj.CeaseOrder.TaskTab,"Task Tab");
	    			click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"Execution flow link");	
	    			Reusable.waitForAjax();
	    			Reusable.waitForpageloadmask();
	    			click(CeaseOrderObj.CeaseOrder.Errors,"Errors");	
	    			Reusable.waitForAjax();
	    			Reusable.waitForpageloadmask();
				}	
				else 
				{
					//Log.info("Not required to Navigate to Errors tab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order did not have any errors to be Captured");
				}
			}
    
    public void CreatHubCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
   	{
    	String HubOrderNumber=  DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Hub/Site Order Number"); 

		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");

		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,HubOrderNumber,"Order Description"); 
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"Apply Button");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"Apply Button");

		String[] arrOfStr = HubOrderNumber.split("#", 0);
		
//		waitRefreshForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
//
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*", 10);
		
        String ProductInstancenumber = getTextFrom(CeaseOrderObj.CeaseOrder.HubProductInstNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ProductInstNumber", ProductInstancenumber);

		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 

		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInst Tab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInst Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,""+arrOfStr1[1]+"","Filter Input Value");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String CeaseHubnumber= getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_No", CeaseHubnumber);

		String[] arrOfStr2 = CeaseHubnumber.split("#", 0); 
		
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL", CeaseOrderscreenURL);
   	}
		
    public void EditHubProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
    	
 		getUrl(HubOrderscreenURL);
 		
		verifyExists(CeaseOrderObj.CeaseOrder.DisconnectHubOrder,"Disconnect Order");
		click(CeaseOrderObj.CeaseOrder.DisconnectHubOrder,"Disconnect Order");
		
		verifyExists(CarNorObj.carNor.Edit,"Edit");
		click(CarNorObj.carNor.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
    }
    
    public void DecomposeHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
    	
 		getUrl(HubOrderscreenURL);
 		
		verifyExists(CeaseOrderObj.CeaseOrder.HubSubOrder,"HubSubOrder");
		click(CeaseOrderObj.CeaseOrder.HubSubOrder,"HubSubOrder");

		verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
 	}
    public void ProcessHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
 		String HubOrdernumber =DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_No");
    	
 		getUrl(HubOrderscreenURL);

    	verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,HubOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		click(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
    }
    
    public void CompleteHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
 		String HubOrdernumber =DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub/Site_Order_No");
		String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Type of Device");

//		getUrl(HubOrderscreenURL);
//
//		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
//		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
//
//		verifyExists(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
//		click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
//		
//		waitForAjax();
//		
//		verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
//		click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
//
//		Reusable.waitForAjax();
//		Reusable.waitForpageloadmask();
// 		
		getUrl(HubOrderscreenURL);
 		
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
        
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,HubOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,120,20000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.arrorder4);

	    if(OrderStatus.equalsIgnoreCase("Process Started"))
		{
		    String	ErrorStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.BlockedbyError);
		    if (ErrorStatus.equalsIgnoreCase("Blocked By Errors"))
		    {
		    	if (DeviceType.contains("Stub"))
				{
//			    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2);
		    		getUrl(HubOrderscreenURL);					  

					verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					
					verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					
					waitForAjax();
					
					verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					
					waitForAjax();
				
					if(verifyExists("@xpath=//a[text()='Delete Service in Smarts']"))
					{
						ErrorWorkItems.DeleteSmartserror("Delete Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
	
						getUrl(HubOrderscreenURL);
				        
						click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
						Reusable.waitForAjax();
				        
						verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						
						selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
						
						sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,HubOrdernumber);
						
						verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						
						waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
						if (OrderStatus.contains("Process Completed"))
						{
							Report.LogInfo("Validate","\""+HubOrdernumber +"\" Is Completed Successfully", "INFO");
							ExtentTestManager.getTest().log(LogStatus.INFO, HubOrdernumber +" Is Completed Successfully");
						}
					}			
					else
					{
						System.out.println("Order is blocked by different errors");
					}
				}
			}
		}    
	    else if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+HubOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, HubOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+HubOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, HubOrdernumber +" Execution InProgress");
		}
	}
    
	public void CreatSpokeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String SpokeOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke/Endpoint Order Number");

		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input Value");
		sendKeys(CarNorObj.carNor.FilterInputValue,SpokeOrderNumber);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");

		String[] arrOfStr = SpokeOrderNumber.split("#", 0);
		
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
		String ProductInstancenumber = getTextFrom(CarNorObj.carNor.ProductInstNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ProductInstNumber", ProductInstancenumber);
		
		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		click(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0);
		
		verifyExists(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		click(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		
		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		sendKeys(CarNorObj.carNor.FilterInputValue,arrOfStr1[1]);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		click(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		waitForAjax();
		
		String CeaseOrdernumber = getTextFrom(CarNorObj.carNor.LinkforOrder);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_No",CeaseOrdernumber);
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		
		String CeaseOrderscreenURL = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL",CeaseOrderscreenURL);
		
		Reusable.waitForAjax();
	}
		
	public void EditCeaseProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
		verifyExists(CarNorObj.carNor.DisconnectOrder,"Disconnect Order");
		click(CarNorObj.carNor.DisconnectOrder,"Disconnect Order");
		
		verifyExists(CarNorObj.carNor.Edit,"Edit");
		click(CarNorObj.carNor.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
	}			

	public void DecomposeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		
		getUrl(CeaseOrderscreenURL);

		verifyExists(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		click(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		
		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
		click(CarNorObj.carNor.Decompose,"Decompose");
		
		waitForAjax();
	}
	
//	public void DecomposeSpokeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
//	{
//		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
//
//		getUrl(CeaseOrderscreenURL);
//		
//		verifyExists(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
//		click(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
//		
//		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
//		click(CarNorObj.carNor.Decompose,"Decompose");
//		waitForAjax();
//		Reusable.waitForpageloadmask();
//	}
	
	public void ProcessCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_No");
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Type of Device");

		getUrl(CeaseOrderscreenURL);

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();
		
//		getUrl(CeaseOrderscreenURL);
//        
//		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
//		Reusable.waitForAjax();
        
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,180,20000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder4);

	    if(OrderStatus.equalsIgnoreCase("Process Started"))
		{
		    String	ErrorStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.BlockedbyError);
		    if (ErrorStatus.equalsIgnoreCase("Blocked By Errors"))
			{
			    if (DeviceType.contains("Stub"))
				{
//			    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2);
					getUrl(CeaseOrderscreenURL);					  

					verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					
					verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					
					waitForAjax();
					
					verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					
					waitForAjax();
				
					if(verifyExists("@xpath=//a[text()='Delete Service in Smarts']"))
					{
						ErrorWorkItems.DeleteSmartserror("Delete Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
	
						getUrl(CeaseOrderscreenURL);
						
						click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
						Reusable.waitForAjax();
				        
						verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						
						selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
						
						sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
						
						verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						
						waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,90,10000);
						if (OrderStatus.contains("Process Completed"))
						{
							Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
							ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
						}
					}			
					else
					{
						System.out.println("Order is blocked by different errors");
					}
				}
			}
		}    
	    else if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Execution InProgress");
		}
	}
		
		public void CompletSpokeworkitem(String[] taskname,String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
		{ 
		//  Log.info("In Switch case with TaskName :"+taskname);
			switch(taskname[0])
			{
				case "Legacy Deactivation Completed":
				{
					verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
					click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();				}
			}
		}	
		
	    public void NavigatebacktoCeaseOrdercreen(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
			        String CeaseOrderscreenURL= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
					getUrl(CeaseOrderscreenURL);
					verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
					click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		}

				

}

