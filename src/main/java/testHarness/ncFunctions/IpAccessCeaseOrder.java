package testHarness.ncFunctions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.CeaseOrderObj;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.IpVpnOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class IpAccessCeaseOrder extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();
	
	public void CreateIpAccessCeaseOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
 		String OrderNumber= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Spoke/Endpoint Order Number");

 		Reusable.waitForpageloadmask();
 		
 		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,OrderNumber,"FilterInputValue");		
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");

		String CompOrdernumber=OrderNumber;
		String[] arrOfStr = CompOrdernumber.split("#", 0);
//		waitRefreshForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Order to be Completed");

		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*",10);		

		String ProductInstancenumber=getTextFrom(CeaseOrderObj.CeaseOrder.IPAccessProductInstNumber);
		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo,"ProductInstNumber",ProductInstancenumber);

		//Log.info(ProductInstancenumber.get());
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0);
		//Log.info(arrOfStr1[1]);
		
		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,arrOfStr1[1],"FilterInputValue");	
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		
//		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input",10);
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String CeaseOrdernumber=getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo,"Cease_Order_No",CeaseOrdernumber);

		//Log.info(CeaseOrdernumber.get());
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		//Log.info(arrOfStr2[1]);
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo,"Cease_Order_URL",CeaseOrderscreenURL);
	}
	
	public void UpdateDescription() throws Exception
	{

		verifyExists(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"GeneralInformationTab");
		click(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"GeneralInformationTab");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(CeaseOrderObj.CeaseOrder.EditDesc,"EditDesc");
		click(CeaseOrderObj.CeaseOrder.EditDesc,"EditDesc");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		sendKeys(CeaseOrderObj.CeaseOrder.OrderDescription,"Cease Order created using Automation script","OrderDescription");

		verifyExists(CeaseOrderObj.CeaseOrder.Update,"Update");
		click(CeaseOrderObj.CeaseOrder.Update,"Update");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();	
	}



	public void EditProduct(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
	    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_URL");

	    getUrl(CeaseOrderscreenURL);
		
		verifyExists(CeaseOrderObj.CeaseOrder.DisconnectIpAccessOrder,"DisconnectIpAccessOrder");
		click(CeaseOrderObj.CeaseOrder.DisconnectIpAccessOrder,"DisconnectIpAccessOrder");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Edit,"Edit");
		click(CeaseOrderObj.CeaseOrder.Edit,"Edit");
		
		verifyExists(CeaseOrderObj.CeaseOrder.HardCeaseDelay,"HardCeaseDelay");
		click(CeaseOrderObj.CeaseOrder.HardCeaseDelay,"HardCeaseDelay");

		clearTextBox(CeaseOrderObj.CeaseOrder.HardCeaseDelay);
		
		sendKeys(CeaseOrderObj.CeaseOrder.HardCeaseDelay,"0","HardCeaseDelay");	
		Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.HardCeaseDelay, Keys.ENTER);
		
		
		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CeaseOrderObj.CeaseOrder.ColtPromiseMonth,"ColtPromiseMonth");
		click(CeaseOrderObj.CeaseOrder.ColtPromiseMonth,"ColtPromiseMonth");
		
		sendKeys(CeaseOrderObj.CeaseOrder.ColtPromiseMonth,Month.format(modifiedDate),"ColtPromiseMonth");	
	
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.ColtPromiseMonth, Keys.ENTER);
		
		
		verifyExists(CeaseOrderObj.CeaseOrder.ColtPromiseDate,"ColtPromiseDate");
		click(CeaseOrderObj.CeaseOrder.ColtPromiseDate,"ColtPromiseDate");
		sendKeys(CeaseOrderObj.CeaseOrder.ColtPromiseDate,Day.format(modifiedDate),"ColtPromiseDate");	
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.ColtPromiseDate, Keys.ENTER);
	
			
		verifyExists(CeaseOrderObj.CeaseOrder.ColtPromiseYear,"ColtPromiseYear");
		click(CeaseOrderObj.CeaseOrder.ColtPromiseYear,"ColtPromiseYear");
		sendKeys(CeaseOrderObj.CeaseOrder.ColtPromiseYear,Year.format(modifiedDate),"ColtPromiseYear");	
	
	
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.ColtPromiseYear, Keys.ENTER);
		
		
		verifyExists(CeaseOrderObj.CeaseOrder.Update,"Update");
		click(CeaseOrderObj.CeaseOrder.Update,"Update");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	}

	public void DecomposeOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
	    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_URL");
		
	    getUrl(CeaseOrderscreenURL);
		
		verifyExists(CeaseOrderObj.CeaseOrder.OrderTab,"OrderTab");
		click(CeaseOrderObj.CeaseOrder.OrderTab,"OrderTab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.SelectDisconnectIpAccessOrder,"SelectDisconnectIpAccessOrder");
		click(CeaseOrderObj.CeaseOrder.SelectDisconnectIpAccessOrder,"SelectDisconnectIpAccessOrder");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		
		verifyExists(CeaseOrderObj.CeaseOrder.Suborder,"Suborder");
		click(CeaseOrderObj.CeaseOrder.Suborder,"Suborder");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	}

	public void ProcessOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
	    String CeaseOrdernumber= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_No");
	    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_URL");
	
		getUrl(CeaseOrderscreenURL);

		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 

		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);

		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		click(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
	}
	
	public void CompleteOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
	    String CeaseOrdernumber= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_No");
	    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_URL");
	    String WorkItem= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Work Items");
		String DeviceType = DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo,"Type of Device"); 

		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
		
//		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
//		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
//		
//		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*",10);		
		
		getUrl(CeaseOrderscreenURL);

		verifyExists(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
		click(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
		click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		ErrorWorkItems.DeleteSmartserror("Delete Service in Smarts",dataFileName, SheetName, scriptNo, dataSetNo);
		
		verifyExists(CeaseOrderObj.CeaseOrder.Workitems,"Workitems");
		click(CeaseOrderObj.CeaseOrder.Workitems,"Workitems");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		for (int k=1; k<=Integer.parseInt(WorkItem);k++)
		{
			waitRefreshForElementToAppear(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,60,20000);
			verifyExists(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,"TaskReadytoComplete");
			click(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,"TaskReadytoComplete");
		    Completeworkitem(GetText(CeaseOrderObj.CeaseOrder.DisconnectTaskTitle),dataFileName, SheetName, scriptNo, dataSetNo);
		}
		getUrl(CeaseOrderscreenURL);
		
		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);

		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
//		waitRefreshForElementToAppear("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",180,20000);
		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,180,20000);
		String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder4);
		
	    if(OrderStatus.equalsIgnoreCase("Process Started"))
	    {
		    String ErrorStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.BlockedbyError);
	    	if (ErrorStatus.equalsIgnoreCase("Blocked By Errors"))
			{
		    	if (DeviceType.contains("Stub"))
				{
//			    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2);
//			    	ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*",10);

		    		getUrl(CeaseOrderscreenURL);
			    	
					verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
					
					verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
					
					waitForAjax();
					
					verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
					
					waitForAjax();
				
					if(verifyExists("@xpath=//a[text()='Delete Service in Smarts']"))
					{
						ErrorWorkItems.DeleteSmartserror("Delete Service in Smarts",dataFileName, SheetName, scriptNo, dataSetNo);
	
						getUrl(CeaseOrderscreenURL);
				        
						click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
						Reusable.waitForAjax();
				        
						verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
						
						selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
						
						sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
						
						verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
						
						waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
						if (OrderStatus.contains("Process Completed"))
						{
							Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
							ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
						}
					}			
					else
					{
						System.out.println("Order is blocked by different errors");
					}
				}
			}
		}
	    else if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Execution InProgress");
		}
		Reusable.waitForpageloadmask();
	}
	
	public void Completeworkitem(String[] taskname,String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException  
	{ 
		//Log.info("In Switch case with TaskName :"+taskname);
		switch(taskname[0])
		{
			case "Legacy Deactivation Completed":
			{
				verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				Reusable.waitForAjax();
			}
			break;
			
			case "Waiting for Hard Cease Date for L3":
			{
				verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				Reusable.waitForAjax();
			}
			break;
			
			case "Waiting for RIR Process Completion":
			{
				verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				Reusable.waitForAjax();
			}
			break;
			
			case "Release\\Reserve LAN Range":
	        {
	        	  selectByVisibleText(CeaseOrderObj.CeaseOrder.ReleaseLanRange,"Yes","ReleaseLanRange");
	              verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
	          	  click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
	          	  Reusable.waitForAjax();
	        }
			break;
			
			default:
				// Nothing Found it will try to complete the Task	
				verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
		}
		
	}
	
	public void GotoErrors(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
	    String CeaseOrdernumber= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_No");
	    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Cease_Order_URL");
	
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
		getUrl(CeaseOrderscreenURL);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Order");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Accounts Composite Orders Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	
		String Errors=getTextFrom("//a/span[contains(text(),'\"+arrOfStr2[1]+\"')]/parent::*/parent::*/following-sibling::*[4]");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Get the value of In Error column for the order");
		if(Errors.equalsIgnoreCase("Blocked by Errors"))
		{
			ClickonElementByString("//a/span[contains(text(),'\"+arrOfStr2[1]+\"')]/parent::*",10);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Composite Order");
			
			verifyExists(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
			click(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Tasks Tab");
			
			verifyExists(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
			click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");		
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Execution Flow link");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			verifyExists(CeaseOrderObj.CeaseOrder.Errors,"Errors");
			click(CeaseOrderObj.CeaseOrder.Errors,"Errors");
			ExtentTestManager.getTest().log(LogStatus.FAIL, " Step: Click on Errors Tab");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			}
		else 
		{
			//Log.info("Not required to Navigate to Errors tab");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order did not have any errors to be Captured");
		}
	}
}