package testHarness.ncFunctions;


import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.siebeltoNCObjects.OvcCircuitValidHelperObj;
import testHarness.commonFunctions.ReusableFunctions;

public class OvcCircuitValidHelper extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();

	public void InventoryProject() throws Exception {
		
		verifyExists(OvcCircuitValidHelperObj.OvcCircuitValidHelper.InventoryProject, "InventoryProject");
		click(OvcCircuitValidHelperObj.OvcCircuitValidHelper.InventoryProject, "InventoryProject");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(OvcCircuitValidHelperObj.OvcCircuitValidHelper.Circuits, "Circuits");
		click(OvcCircuitValidHelperObj.OvcCircuitValidHelper.Circuits, "Circuits");

		verifyEquals(OvcCircuitValidHelperObj.OvcCircuitValidHelper.OVCcircuit, "OVCcircuit");
		click(OvcCircuitValidHelperObj.OvcCircuitValidHelper.OVCcircuit, "OVCcircuit");

	}

	public void OvcCircuitValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)	throws Exception 
	{
	
		String CeosRefNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CeosRefNumber");
		String IpAccessSiebelOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IpAccessSiebelOrdernumber");
		String OvcCircuitnumber = CeosRefNumber + "#" + IpAccessSiebelOrdernumber;

		verifyEquals(OvcCircuitValidHelperObj.OvcCircuitValidHelper.AccountNameSorting, "AccountNameSorting");
		click(OvcCircuitValidHelperObj.OvcCircuitValidHelper.AccountNameSorting, "AccountNameSorting");

		verifyEquals(OvcCircuitValidHelperObj.OvcCircuitValidHelper.NameFiltering, "NameFiltering");
		click(OvcCircuitValidHelperObj.OvcCircuitValidHelper.NameFiltering, "NameFiltering");

		selectByVisibleText(OvcCircuitValidHelperObj.OvcCircuitValidHelper.FilterSelectName, "Name","FilterSelectName");
		sendKeys(OvcCircuitValidHelperObj.OvcCircuitValidHelper.FilterInputValue, OvcCircuitnumber, "FilterInputValue");

		verifyEquals(OvcCircuitValidHelperObj.OvcCircuitValidHelper.ApplyButton, "ApplyButton");
		click(OvcCircuitValidHelperObj.OvcCircuitValidHelper.ApplyButton, "ApplyButton");

		String ActualOvcCircuitnumber = getTextFrom(OvcCircuitValidHelperObj.OvcCircuitValidHelper.ActualOvcCircuitnumber, "ActualOvcCircuitnumber");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ActualOvcCircuitnumber",ActualOvcCircuitnumber);

		boolean isEqual = ActualOvcCircuitnumber.equals(OvcCircuitnumber);
		System.out.println(isEqual);

	}

}
