package testHarness.nhB2BFunctions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.JSONObject;
import org.json.XML;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.DataMiner;
import baseClasses.WebAPITestBase;
import io.restassured.response.Response;

public class NHB2BCommonFunctions extends WebAPITestBase
{
	public String path;
	public String testDataPath;
	public String endpoint;
	public String methodType;
	public String inputDataType;
	public String inputData;
	public String statusCode;
	public String apiURL;
	public Response response;
	public String sheetName;
	static Properties prop = new Properties();
	
	public void RunFeature(String testDataFile, String dataSheet, String scriptNo, String dataSetNo, String ScenarioName) throws Exception
	{
		try
		{
				
		String InputData = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Input_Data");
		String ExpectedStatusCode = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Expected_StatusCode");
		String ExpectedStatus = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Expected_Status");
		String TestScenario = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Scenario_Name");
		String APIEndpoint = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "API_Endpoint");
		String MethodType = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Method_Type");
		String InputDataType = fngetcolvalue(dataSheet, scriptNo, dataSetNo, "InputData_Type");
		
		InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties");
		  // load a properties file
        prop.load(input); 
        String Environment = prop.getProperty("Environment");				
		String NH_B2B_URL = fngetconfigvalue(Environment, "URL");
		String NH_B2B_Username = fngetconfigvalue( Environment, "Username");
		String NH_B2B_Password = fngetconfigvalue( Environment, "Password");
		apiURL = NH_B2B_URL + APIEndpoint;
		
	/*	Map<String, String> authhdrs = new HashMap<String, String>();
		authhdrs.put("Authorization", "Basic Y29sdDM0NTpwNTE4MlBuRA==");
	    authhdrs.put("Content-Type", "text/xml; charset=UTF-8;");	
	    */
		
	    JSONObject json = XML.toJSONObject(InputData);   
	    
	    if(dataSheet.equalsIgnoreCase("Bulk_Upload"))
	    {
	    	setColumnValues(testDataFile, dataSheet, scriptNo, dataSetNo);
	    	json = updateXMLValueBulkUpload(json);
	    	colValues.clear();
	    	
	    }else
	    {	    
	    	List<String> xmlColNames = fngetcolNames(dataSheet);    	
	    
	    	for(String colName : xmlColNames)
	    	{
	    		String NodeValue = fngetcolvalue(dataSheet, scriptNo, dataSetNo, colName);
	   	
	    		json = updateXMLValue(json, colName, NodeValue);	    	
	    		    	
	    		if(json==null)
	    		{
	    			break;
	    		}
	    	}
	    }
	    
	    InputData = convertJSONToXML(json, InputData);
	    String dataLog = json.toString();
	    
	    if(InputData!=null)
    	{   		
    	  	ExtentTestManager.getTest().log(LogStatus.INFO,TestScenario + "- Scenario Started");
	    	Report.LogInfo("Scenario Started",TestScenario + "- Scenario Started", "INFO");
	    	Report.logToDashboard(TestScenario + "- Scenario Started");

	    	switch (MethodType) {
			case "Get":
				response = get(NH_B2B_Username, NH_B2B_Password, apiURL, InputDataType, InputData, dataLog);
				break;
			case "Post":
				response = post(NH_B2B_Username, NH_B2B_Password, apiURL, InputData, dataLog);
				break;
			case "Delete":
				response = delete(NH_B2B_Username, NH_B2B_Password, apiURL,InputDataType, InputData, dataLog);
				break;
			default: 
				break;
			}	
			
	    	verifyStatusCode(response, Integer.parseInt(ExpectedStatusCode));
			
	    	compareActualWithExpected(response, ExpectedStatus);	
	    	
    	}else
    	{
    		Report.LogInfo("Exception", "Input xml/data error, execution aborted.", "FAIL");
			Report.logToDashboard("Input xml/data error, execution aborted.");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Execution aborted.");
			
    	}	    
		
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		}
		      	        
				
	}		
}
