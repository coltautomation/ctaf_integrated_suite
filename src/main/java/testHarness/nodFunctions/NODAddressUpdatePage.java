package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODAddressUpdateObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODAddressUpdatePage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();


	private static HashMap<String, String> Number= new HashMap<String, String>();

	//================================ Updated ==================================================
	public void AddressModifications(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");

		switch (VerificationType) {
		case "Address Update Notification":
			//TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			
			AddressUpdateAndNotify(testDataFile, sheetName, scriptNo, dataSetNo);			
			break;
		case "DSU_Transaction Details screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			DirectoryServiceUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Add Address_Transaction Details screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			AddAddress(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Modify Address_Transaction Details screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			ModifyAddress(testDataFile, sheetName, scriptNo, dataSetNo);
			break;

		}

	}

	//=======================================================================
	
	
	
	
	
	
	
	
	

	public void AddressUpdateAndNotify(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"NIF");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubLocality");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Number");
		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		String NotifyDetail = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Notify_Detail");
		String OrderID;
		String SuccessMessage;
		
	
		waitForAjax();
		Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");
		waitForAjax();
		switch (Country) 
		{
		case "UK":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");	
			javaScriptclick(NODAddressUpdateObj.AddressUpdate.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
				//sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			break;
		case "AT":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
				//sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			
			
			break;
			
		case "BE":
			
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LanguageBE,LanValue,"LanguageType");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			break;
			
		case "DK":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			String CityTownDrp=CityTown.trim();
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownDK,30);
		    Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownDK,CityTownDrp);  
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
				//sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			
			break;
			
		case "FR":
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
				//sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			
			
			break;
		/*	
		case "ES":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			*/
			
			
			
		case "DE":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}

			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
				
			break;

		case "IE":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}

			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			break;

		case "IT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.VatNumber, VatNumber, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetType, StreetType, "Enter Street Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.province, Province, "Enter province");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}
			waitForAjax();
		
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			break;
			/*
		case "NL":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderID2 = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID2);
			break;	
			*/
		case "PT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			break;

			
		case "SE":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter Subcriber Id");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNumber, Street_Number, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			
			break;
			

		case "CH":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Municipality,"Aarberg > BE");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Notify Colt Support Team')]"),"Notification")){
				ScrollIntoViewByString(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn);
				verifyExists(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"NotifySupportBtn");
				click(NODAddressUpdateObj.AddressUpdate.NotifySupportBtn,"Notify Colt Support Team Button");
				waitForElementToAppear(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,30);
				verifyExists(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,"AddDetailsTextArea");
			//	sendKeys(NODAddressUpdateObj.AddressUpdate.AddDetailsTextArea,NotifyDetail, "Notify Detail");
				javaScriptclick(NODAddressUpdateObj.AddressUpdate.SubmitBtn,"Submit Button");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Notified colt support team successfully");
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "colt support team not Notified");	
			}
			
			
			break;	
			
			default:
			
				ExtentTestManager.getTest().log(LogStatus.INFO, "Don't have address flow, Have Partial address flow");
				
		}	
			

		
	}



	public void DirectoryServiceUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String Customer_Name = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String House_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String Street_Name = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String City_Town = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String Postal_Code = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Order_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Order_Type");
		String Telephone_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"DSU_Telephone_Number");
		String Line_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Line_Type");
		String Tariff = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Tariff");
		String Entry_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Entry_Type");
		String Typeface = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Typeface");
		String Listing_Category = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Listing_Category");
		String Listing_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Listing_Type");
		String Priority = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Priority");
		//		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		//		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		//		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		//		String Premises_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Premises_Number");
		//		String Advertisement_Flag = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Advertisement_Flag");
		//		String Usage_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Usage_Type");
		//		String Confidentiality_Reverse_Search = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Confidentiality_Reverse_Search");

		if(isElementPresent(By.xpath("//*[@class='actionLinks ml-4 ng-star-inserted']"),"Action Link"))
		{                              
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Action Drop Down");

			//String Country1 = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");

			switch (Country) 
			{
			case "DE":
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,15);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,"Directory Service Update Option");
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		

				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
				waitForAjax();
				ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Order Success Message");

				String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
				break;

			case "IE":
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,15);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,"Directory Service Update Option");
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,15);		

				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LineTypeDrpDwn,Line_Type,"Line Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TraffDrpDwn,Tariff,"Tariff");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.EntryTypeDrpDwn,Entry_Type,"Entry Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TypeFaceDrpDwn,Typeface,"Entry Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingCategoryDrpDwn,Listing_Category,"Listing Category");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.listingTypeDrpDwn,Listing_Type,"Listing Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PriorityDrpDwn,Priority,"Listing Type");
				waitForAjax();
				ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");

				String OrderId1 = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId1);

			}
			/*
			if (Country.toString().equals("DE")) 
			{
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,15);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,"Directory Service Update Option");
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		

				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
				waitForAjax();
				ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Order Success Message");

				String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);

			}
			else if (Country.toString().equals("IE")) 
			{
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,15);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,"Directory Service Update Option");
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_DSUoption,15);		

				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LineTypeDrpDwn,Line_Type,"Line Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TraffDrpDwn,Tariff,"Tariff");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.EntryTypeDrpDwn,Entry_Type,"Entry Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TypeFaceDrpDwn,Typeface,"Entry Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingCategoryDrpDwn,Listing_Category,"Listing Category");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.listingTypeDrpDwn,Listing_Type,"Listing Type");
				selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PriorityDrpDwn,Priority,"Listing Type");
				waitForAjax();
				ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");

				String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);

			}	
			 */
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DSU is not getting updated");	

		}


	}



	public void AddAddress(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String CustomerType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Type");
		String FirstName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"First_Name");
		String LastName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Last_Name");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String DepartmentValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Department_Value");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");

		if(isElementPresent(By.xpath("//*[@class='actionLinks ml-4 ng-star-inserted']"),"Action link"))
		{                              
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Action Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_AddUpdateOpt,15);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_AddUpdateOpt,"AddressUpdate link");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			if(isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]"),"Customer Type"))
			{
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"Select Customer Type");
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"wholesaleradiobtn");

				if(CustomerType.toString().equals("Residential"))
				{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.FirstName, FirstName, "Enter First Name");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LastName, LastName, "Enter Last Name");
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,"Language");
					selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue,"LanguageType");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedInp, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
				else if(CustomerType.toString().equals("Business"))
				{
					if(Country.equals("DE") || Country.equals("DK")) 
					{
						Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
					}
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
					//Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue);
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedInp, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
			}
			else{
				WaitForAjax();
				if(isElementPresent(By.xpath("//label[contains(text(),'Service Type')]"),"Service Type"))
				{
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
				}
				if(isElementPresent(By.xpath("//input[@id='emerBusinessNameUpdated']"),"Business Name")){
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
				}else{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName, CustomerName, "Enter Customer Name");
				}
				if(isElementPresent(By.xpath("//label[contains(text(),'Subscriber ID')]"),"Subscriber ID")){
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter Subcriber Id");
				}
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
				if (isElementPresent(By.xpath("//label[contains(text(),'Department')]"),"Department"))
				{
					selectByValue(NODNumberDirectActivationObj.NumDirActivation.DepartmentType,DepartmentValue,"Department Type");
					WaitForAjax();

					ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR);
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,30);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,CityTown);
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,15);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,PostalCode);
				}
				else{
					ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,CityTown);
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}

			}
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address"))
			{                              
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateAddressBtn,"Update Address Button");
			verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Updated Successfully");
			String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
		}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not Updated Successfullys");	

		}

	}

	public void ModifyAddress(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String CustomerType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Type");
		String FirstName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"First_Name");
		String LastName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Last_Name");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String DepartmentValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Department_Value");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");

		if(isElementPresent(By.xpath("//*[@class='actionLinks ml-4 ng-star-inserted']"),"Action link"))
		{                              
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Action Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_AddUpdateOpt,15);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TxDetails_AddUpdateOpt,"AddressUpdate link");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			if(isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]")))
			{
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"Select Country");
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"wholesaleradiobtn");

				if(CustomerType.toString().equals("Residential"))
				{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.FirstName, FirstName, "Enter First Name");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LastName, LastName, "Enter Last Name");
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,"Language");
					selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue,"LanguageType");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
				else if(CustomerType.toString().equals("Business"))
				{
					if(Country.equals("DE"))
					{
						Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
					}
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
					//Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue);
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedInp, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
			}
			else{
				WaitForAjax();
				if(isElementPresent(By.xpath("//label[contains(text(),'Service Type')]"))){
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
				}
				if(isElementPresent(By.xpath("//input[@id='emerBusinessNameUpdated']"))){
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
				}else{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName, CustomerName, "Enter Customer Name");
				}
				if(isElementPresent(By.xpath("//label[contains(text(),'Subscriber ID')]"))){
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter House Number");
				}
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
				if (isElementPresent(By.xpath("//label[contains(text(),'Department')]")))
				{
					selectByValue(NODNumberDirectActivationObj.NumDirActivation.DepartmentType,DepartmentValue,"Department Type");
					WaitForAjax();

					ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR);
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,30);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,CityTown);
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,15);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,PostalCode);
				}
				else{
					ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated);
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}

			}
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]")))
			{                              
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ValidateAddressBtn,"Validate Address Button");
				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,15);
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");

			}
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateAddressBtn,"Update Address Button");
			verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Updated Successfully");
			String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
		}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not Updated Successfullys");	

		}

	}


}



