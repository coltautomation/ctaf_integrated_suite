package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODAddressUpdateObj;
import pageObjects.nodObjects.NODAddressUpdate_PreActivatedNumberObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODAddressUpdatedAndSkipDSU extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();


	private static HashMap<String, String> Number= new HashMap<String, String>();



	public void UpdateAddressAndSkipDSU(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"NIF");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubLocality");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Number");
		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		String OrderID;
		String SuccessMessage;

		TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
		TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
		waitForAjax();
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		WaitForAjax(); 
		waitForElementToAppear(NODPortInRequestObj.QuickNote.ActionOption,60);
		javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
		waitForElementToAppear(NODAddressUpdate_PreActivatedNumberObj.Address.AddressUpdateOption,15);
		javaScriptclick(NODAddressUpdate_PreActivatedNumberObj.Address.AddressUpdateOption,"Address update option");
		waitForAjax();
		waitForElementToAppear(NODAddressUpdate_PreActivatedNumberObj.Address.popup,30);
		switch (Country) 
		{
		case "IT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.VatNumber, VatNumber, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetType, StreetType, "Enter Street Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.province, Province, "Enter province");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,120);
			SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
			break;
			}
			
		}	


	












}

