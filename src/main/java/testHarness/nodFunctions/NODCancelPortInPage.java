package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODAddressUpdateObj;
import pageObjects.nodObjects.NODCancelPortInObj;
import pageObjects.nodObjects.NODCancelReservationObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODCancelPortInPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	
	//AP
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();

	public void CancelPortInNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");
		
		switch (VerificationType) {
		case "My Order screen":
			cancelPortInMyOrderPage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Transaction Details screen":
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			cancelPortInTXpage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}
		}
//---

	public void cancelPortInMyOrderPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Record"))
        {   
			WaitForAjax();
			waitForElementToBeVisible(NODCancelPortInObj.Cancel.ActionOption,30);
	        javaScriptclick(NODCancelPortInObj.Cancel.ActionOption,"Action Option");
	        waitForElementToBeVisible(NODCancelPortInObj.Cancel.CancelPortInOption,15);
	        click(NODCancelPortInObj.Cancel.CancelPortInOption,"Cancel Reservation link");
	        WaitForAjax();
	        waitForElementToBeVisible(NODCancelPortInObj.Cancel.CancelBtn,15);
	        click(NODCancelPortInObj.Cancel.CancelBtn,"Return Number button");
	        verifyExists(NODCancelPortInObj.Cancel.SuccessMsg,"Port In Order Cancel Success from My Order page");
      
	    	String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
		
        }
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	}
	
	public void cancelPortInTXpage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,60);
		Reusable.Select(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Cancel");
		WaitForAjax();
		waitForElementToBeVisible(NODCancelPortInObj.Cancel.CancelBtn,15);
        click(NODCancelPortInObj.Cancel.CancelBtn,"Cancel button");
        WaitForAjax();
        waitForElementToAppear(NODCancelPortInObj.Cancel.SuccessMsg,60);
    	ExtentTestManager.getTest().log(LogStatus.PASS, "Port In Order Cancel Success from Tx detail page");
    	String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
		/*
        if(isElementPresent(By.xpath("//*[contains(text(),'Thank')]"),"Success Message")){
        	ExtentTestManager.getTest().log(LogStatus.PASS, "Port In Order Cancel Success from Tx detail page");
        	String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
		
	}
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }*/
		
	}

}



