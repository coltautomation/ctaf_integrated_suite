package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODCancelPortInObj;
import pageObjects.nodObjects.NODCancelReservationObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODCancelReservationPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();

	private static HashMap<String, String> Number= new HashMap<String, String>();

	public void CancelReservation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Verification_Type");
		
		switch (VerificationType) {
		case "My Telephone Numbers screen":
			cancelReservationonTelPage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Transaction Details screen":
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			cancelReservationOnTXpage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}

		
	}
	
	

	public void cancelReservationonTelPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Order"))
        {    
			WaitForAjax();
			waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);
		   javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
	        waitForElementToBeVisible(NODCancelReservationObj.Cancel.CancelReservationOption,30);
	        click(NODCancelReservationObj.Cancel.CancelReservationOption,"Cancel Reservation link");
	        waitForElementToBeVisible(NODCancelReservationObj.Cancel.ReturnNumberBtn,15);
	        click(NODCancelReservationObj.Cancel.ReturnNumberBtn,"Return Number button");
	        Reusable.waitForpageloadmask();
	        Reusable.waitToPageLoad();
	        waitForElementToBeVisible(NODPortInRequestObj.QuickNote.SuccessMsg,20);
	        verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
	        String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
        }
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	}
	
	public void cancelReservationOnTXpage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		Reusable.waitForAjax();
		Reusable.Select(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Cancel Reservation");
		waitForElementToBeVisible(NODCancelReservationObj.Cancel.ReturnNumberBtn,15);
        click(NODCancelReservationObj.Cancel.ReturnNumberBtn,"Return Number button");
        Reusable.waitForpageloadmask();
        Reusable.waitToPageLoad();
        waitForElementToBeVisible(NODPortInRequestObj.QuickNote.SuccessMsg,20);
        verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
        String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
	}

}



