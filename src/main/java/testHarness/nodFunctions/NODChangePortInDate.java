package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODCancelReservationObj;
import pageObjects.nodObjects.NODChangePortInDateObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODChangePortInDate extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODMyOrderPage MyOrder = new NODMyOrderPage();

	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void ChangePortInDateAndTime(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");
		
		switch (VerificationType) {
		case "Date Change Option Not Available":
			MyOrder.SearchByOrderStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			DateChangeOption_NotPresent_MYOrderPage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Change PortIn Date and Time":
		//	MyOrder.FilterMyOrder_WithoutOrderUpdateDateFilter(testDataFile, sheetName, scriptNo, dataSetNo);
			MyOrder.SearchByOrderStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			ChangePortInDateAndTimeOnOrderPg(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Transaction Details screen":
		//	MyOrder.FilterMyOrder_WithoutOrderUpdateDateFilter(testDataFile, sheetName, scriptNo, dataSetNo);
			
			MyOrder.SearchByOrderByRangeStart(testDataFile, sheetName, scriptNo, dataSetNo);
			ChangePortinDateOnTxPage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		
			/////////////////
		case "Date Change Option Not Available for Delayed":
			MyOrder.SearchByOrderStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			DateChangeOption_NotPresent_DelayedStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Date Change Option Not Available for Delayed_TXPage":
			MyOrder.SearchByOrderStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			DateChangeOption_NotPresent_DelayedStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
			
			
			
		}

		
	}
	
	
	
	
	
	
	public void DateChangeOption_NotPresent_MYOrderPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Order"))
        {    
			WaitForAjax();
			waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);	
	        javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
	        
	        if(!isElementPresent(By.xpath("//*[@id='actionModals']//*[text()='Date Change']"),"Date Change"))
	        		{
	        		    ExtentTestManager.getTest().log(LogStatus.PASS, "Date Change option not present");
	        			
	        		}else{
			        	ExtentTestManager.getTest().log(LogStatus.FAIL, "Date Change option present");	
	        		} 
        } else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	}
	
	public void ChangePortInDateAndTimeOnOrderPg(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String PortInMonth = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Month");
		String PortInYear = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Year");
		String PortInDay = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Day");
		String PortInTime = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Time");
		String Current_Operator = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Current_Operator");
		String MainBillingNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"MainBillingNumber");
		
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Order"))
        {     
			
			WaitForAjax();
			waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);
			javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
			
//			waitForElementToBeVisible(NODChangePortInDateObj.Date.DateChangeOption,15);
//			click(NODChangePortInDateObj.Date.DateChangeOption,"Date Change");

			waitForElementToBeVisible(NODChangePortInDateObj.Date.ModifyPortAction,15);
			click(NODChangePortInDateObj.Date.ModifyPortAction,"Modify Port Action");
			
			waitForElementToBeVisible(NODChangePortInDateObj.Date.Calendarbtn,15);
			click(NODChangePortInDateObj.Date.Calendarbtn,"Port Date calendar button");
			
			waitForElementToAppear(NODChangePortInDateObj.Date.SelectDate,30);
			javaScriptclick(NODChangePortInDateObj.Date.SelectDate);
			waitForAjax();
			
//			waitForElementToBeVisible(NODChangePortInDateObj.Date.PortInTime,15);
			verifyExists(NODChangePortInDateObj.Date.PortInTime,"Porting Window");	
			click(NODChangePortInDateObj.Date.PortInTime,"Porting Window");
			verifyExists(NODChangePortInDateObj.Date.Time.replace("Time",PortInTime),"Port in time");
			click(NODChangePortInDateObj.Date.Time.replace("Time",PortInTime),"Port in time");
			waitForAjax();
			
			if (!Country.toString().equals("DE"))
			{
			verifyExists(NODChangePortInDateObj.Date.CurrentOperatorBtn,"Current Operator");	
			click(NODChangePortInDateObj.Date.CurrentOperatorBtn,"Current Operator");
			//Reusable.Select(NODChangePortInDateObj.Date.CurrentOperatorBtn,Current_Operator);
			selectByValue(NODChangePortInDateObj.Date.CurrentOperatorBtn,Current_Operator,"Current Operator");
			waitForAjax();
			}
			
			verifyExists(NODChangePortInDateObj.Date.mainBillingNumber,"Main Billing Number field");	
			sendKeys(NODChangePortInDateObj.Date.mainBillingNumber, MainBillingNumber, "Main Billing Number");
			waitForAjax();
			
			verifyExists(NODChangePortInDateObj.Date.ModifyPortBtn,"Modify Port button");
			click(NODChangePortInDateObj.Date.ModifyPortBtn,"Modify Port button");
			waitForAjax();
			
//			verifyExists(NODChangePortInDateObj.Date.DateChangeBtn,"Date Change button");
//			click(NODChangePortInDateObj.Date.DateChangeBtn,"Date Change button");
//			waitForAjax();   //MF
			
			waitForElementToBeVisible(NODPortInRequestObj.QuickNote.SuccessMsg1,20);
			verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg1,"Order Success Message");
			String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId1,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
		
			
        } else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	
	}
	
	
	public void ChangePortinDateOnTxPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String VerificationType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Verification_Type");
		String PortInMonth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"PortIn_Month");
		String PortInYear = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"PortIn_Year");
		String PortInDay = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"PortIn_Day");
		String PortInTime = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"PortIn_Time");
		String Current_Operator = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Current_Operator");
		String MainBillingNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"MainBillingNumber");
		
		switch(VerificationType) 
		{
		case "Transaction Details screen":
			DateChangeOption_TXScreen(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}
		//Date change Form

		waitForElementToBeVisible(NODChangePortInDateObj.Date.ModifyPortAction,15);
		click(NODChangePortInDateObj.Date.ModifyPortAction,"Modify Port Action");
		
		waitForElementToBeVisible(NODChangePortInDateObj.Date.Calendarbtn,15);
		click(NODChangePortInDateObj.Date.Calendarbtn,"Port Date calendar button");
			
		waitForElementToAppear(NODChangePortInDateObj.Date.SelectDate,30);
		javaScriptclick(NODChangePortInDateObj.Date.SelectDate);
		waitForAjax();
		
		/*
		waitForElementToBeVisible(NODChangePortInDateObj.Date.Calendarbtn,15);
		click(NODChangePortInDateObj.Date.Calendarbtn,"Port Date calendar button");
		
		waitForElementToBeVisible(NODChangePortInDateObj.Date.Month,15);
		verifyExists(NODChangePortInDateObj.Date.Month,"Month");
		Reusable.Select(NODChangePortInDateObj.Date.Month,PortInMonth);
		
		verifyExists(NODChangePortInDateObj.Date.Year,"Year");
		Reusable.Select(NODChangePortInDateObj.Date.Year,PortInYear);
		
		verifyExists(NODChangePortInDateObj.Date.Day.replace("day",PortInDay),"Day");
		click(NODChangePortInDateObj.Date.Day.replace("day",PortInDay),"Day");
		*/
//		waitForElementToBeVisible(NODChangePortInDateObj.Date.PortInTime,15);
		verifyExists(NODChangePortInDateObj.Date.PortInTime,"Porting Window");	
		click(NODChangePortInDateObj.Date.PortInTime,"Porting Window");
		verifyExists(NODChangePortInDateObj.Date.Time.replace("Time",PortInTime),"Port in time");
		click(NODChangePortInDateObj.Date.Time.replace("Time",PortInTime),"Port in time");
		waitForAjax();
		
		if (!Country.toString().equals("DE"))
		{
		verifyExists(NODChangePortInDateObj.Date.CurrentOperatorBtn,"Current Operator");	
		click(NODChangePortInDateObj.Date.CurrentOperatorBtn,"Current Operator");
		//Reusable.Select(NODChangePortInDateObj.Date.CurrentOperatorBtn,Current_Operator);
		selectByValue(NODChangePortInDateObj.Date.CurrentOperatorBtn,Current_Operator,"Current Operator");
		waitForAjax();
		}
		
		verifyExists(NODChangePortInDateObj.Date.mainBillingNumber,"Main Billing Number field");	
		sendKeys(NODChangePortInDateObj.Date.mainBillingNumber, MainBillingNumber, "Main Billing Number");
		waitForAjax();
		
		verifyExists(NODChangePortInDateObj.Date.ModifyPortBtn,"Modify Port button");
		click(NODChangePortInDateObj.Date.ModifyPortBtn,"Modify Port button");
		waitForAjax();
		
//		verifyExists(NODChangePortInDateObj.Date.DateChangeBtn,"Date Change button");
//		click(NODChangePortInDateObj.Date.DateChangeBtn,"Date Change button");
//		waitForAjax();   //MF
		
		waitForElementToBeVisible(NODPortInRequestObj.QuickNote.SuccessMsg1,20);
		verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg1,"Order Success Message");
		String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId1,"Order ID");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
	
	
	}

	
	public void DateChangeOption_OrderAction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		WaitForAjax();
		waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);
		javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
		waitForElementToBeVisible(NODChangePortInDateObj.Date.DateChangeOption,15);
		click(NODChangePortInDateObj.Date.DateChangeOption,"Date Change");	
	}
	
	public void DateChangeOption_TXScreen(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
	click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
	Reusable.waitForAjax();
	waitForElementToBeVisible(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,30);

	javaScriptclick(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Action Drop Down");
	Reusable.waitForAjax();
//	waitForElementToBeVisible(NODChangePortInDateObj.Date.TXPg_DateChangeOpt,15);
//	click(NODChangePortInDateObj.Date.TXPg_DateChangeOpt,"Date Change");
	}

	
	public void DateChangeOption_NotPresent_DelayedStatus(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Verification_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Verification_Type");

		//waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);

		switch (Verification_Type) 
		{
		case "Date Change Option Not Available for Delayed":						
			if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Order"))
			{    
				WaitForAjax();
				if(isElementPresent(By.xpath("//*[@class='ellipse-button fa fa-ban setPopOverPosition disabled']"),"Block Icon"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Action is BLOCKED for Delayed Status");

				}else{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Action is BLOCKED for Delayed Status");	
				} 

			} else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	

			}

			break;

		case "Date Change Option Not Available for Delayed_TXPage":	
			WaitForAjax();
			click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
			Reusable.waitForAjax();

			waitForElementToBeVisible(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,30);
			javaScriptclick(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Action Drop Down");
			Reusable.waitForAjax();

			if (!isElementPresent(By.xpath("//*[contains(text(),'Modify Port')]"),"Modify Port Action"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS,"Modify Port action is not available to change portin date for Delayed status order");
			} 

			break;
		}



	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	

}



