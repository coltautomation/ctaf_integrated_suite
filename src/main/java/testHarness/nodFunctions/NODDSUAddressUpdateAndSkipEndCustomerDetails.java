package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODAddressUpdateObj;
import pageObjects.nodObjects.NODAddressUpdate_PreActivatedNumberObj;
import pageObjects.nodObjects.NODDSUAddressUpdateObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODDSUAddressUpdateAndSkipEndCustomerDetails extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;


	public void DSUAddressUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String Street_Name = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String City_Town = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String Postal_Code = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Telephone_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"DSU_Telephone_Number");
		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String Premises_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Premises_Number");
		String Advertisement_Flag = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Advertisement_Flag");
		String OrderID;
		String SuccessMessage;
		WaitForAjax(); 
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		WaitForAjax(); 
		waitForElementToAppear(NODPortInRequestObj.QuickNote.ActionOption,60);
		javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
		waitForElementToAppear(NODAddressUpdate_PreActivatedNumberObj.Address.AddressUpdateOption,15);
		javaScriptclick(NODAddressUpdate_PreActivatedNumberObj.Address.AddressUpdateOption,"Address update option");
		waitForAjax();
		waitForElementToAppear(NODAddressUpdate_PreActivatedNumberObj.Address.popup,30);
		switch(Country) 
		{
		case "IT":
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			waitForAjax();
			click(NODDSUAddressUpdateObj.DSUAddressUpdate.SkipAddressUpdateBtn,"Skip Button");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.telphoneNo, Telephone_Number, "Enter Telephone Number");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.DSUStreetType, StreetType, "Enter Street Type");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.DSUStreetName, Street_Name, "Enter Street Name");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.PremisesNumber, Premises_Number, "Enter Premises Number");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.DSUPostalCode, Postal_Code, "Enter Street Name");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.DSUCityTown, City_Town, "Enter CityTown");
			sendKeys(NODDSUAddressUpdateObj.DSUAddressUpdate.DSUprovince, Province, "Enter province");
			selectByValue(NODDSUAddressUpdateObj.DSUAddressUpdate.AdvertisementFlag,Advertisement_Flag,"Advertisement Flag");

			//ScrollIntoViewByString(NODDSUAddressUpdateObj.DSUAddressUpdate.ValidateAddressBtn);
			click(NODDSUAddressUpdateObj.DSUAddressUpdate.DSUValidateAddressBtn,"Validate Address Button");
			waitForAjax();

		//	waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,60);
			waitForElementToAppear(NODDSUAddressUpdateObj.DSUAddressUpdate.OrderReviewBtn,60);
			click(NODDSUAddressUpdateObj.DSUAddressUpdate.OrderReviewBtn,"Order Review Button");

			waitForElementToAppear(NODDSUAddressUpdateObj.DSUAddressUpdate.OrderReviewSection,60);
			verifyExists(NODDSUAddressUpdateObj.DSUAddressUpdate.OrderReviewSection,"Order Review Section");
			click(NODDSUAddressUpdateObj.DSUAddressUpdate.SubmitButton,"Submit Button");
			waitForAjax();
			waitForElementToAppear(NODDSUAddressUpdateObj.DSUAddressUpdate.OrderConfirmationMsg,120);

			SuccessMessage=getTextFrom(NODDSUAddressUpdateObj.DSUAddressUpdate.SuccessMessage, "SuccessMessage");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
			OrderID = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order ID :"+OrderID);

			break;
		}


	}






}



