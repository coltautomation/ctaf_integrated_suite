package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODAddressUpdateObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODMyTelephoneNumberScreenPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();


	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void AddressAdditionAndUpdation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");
		
		switch (VerificationType) 
		{
		case "Modify Address_My Telephone Numbers screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			ModifyAddress(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Add Address_My Telephone Numbers screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			AddAddress(testDataFile, sheetName, scriptNo, dataSetNo);								
			break;
		case "Directory Service Update_My Telephone Numbers screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			DirectoryServiceUpdate(testDataFile, sheetName, scriptNo, dataSetNo);			
			break;

		}


	}












	public void ModifyAddress(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"NIF");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubLocality");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Number");
		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		String OrderID;
		String SuccessMessage;
		
	
		waitForAjax();
		Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");
		waitForAjax();
		switch (Country) 
		{
		case "UK":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			
			break;
		case "AT":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
			
		case "BE":	
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LanguageBE,LanValue,"LanguageType");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			waitForAjax();
			
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingOpt,60);
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingOpt,"LISTING");
			waitForAjax();

			//verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewSection,"Order Review Section");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,70);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,60);
			SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
			OrderID = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
			
		case "DK":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			String CityTownDrp=CityTown.trim();
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownDK,30);
		    Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownDK,CityTownDrp);  
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
			
		case "FR":
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
		/*	
		case "ES":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			*/
			
			
			
		case "DE":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}

			waitForAjax();
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Update Address Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);	
			break;

		case "IE":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}

			//waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderID1 = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID1);
			break;

		case "IT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.VatNumber, VatNumber, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetType, StreetType, "Enter Street Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.province, Province, "Enter province");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
			if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']"),"Address Udpate"))
			{
				SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);

			}
			break;
			/*
		case "NL":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderID2 = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID2);
			break;	
			*/
		case "PT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
			if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']"),"Address Udpate"))
			{
				SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);

			}
			break;

			
		case "SE":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter Subcriber Id");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNumber, Street_Number, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SecretListing,"No");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			
			break;
			

		case "CH":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Municipality,"Aarberg > BE");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			
			break;	
			
			default:
			
				ExtentTestManager.getTest().log(LogStatus.INFO, "Don't have modify address flow, Have Partial address flow");
				
		}	
			

	}

	public void AddAddress(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"NIF");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubLocality");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Number");
		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		String OrderID;
		String SuccessMessage;
		
	
		waitForAjax();
		Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");
		waitForAjax();
		switch (Country) 
		{
		case "UK":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			
			break;
		case "AT":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			waitForAjax();
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
			
		case "BE":
			
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LanguageBE,LanValue,"LanguageType");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingOpt,60);
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingOpt,"LISTING");
			waitForAjax();

			//verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewSection,"Order Review Section");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,60);
			SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
			OrderID = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
			
		case "DK":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			String CityTownDrp=CityTown.trim();
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownDK,30);
		    Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownDK,CityTownDrp);  
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
			
		case "FR":
			
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,60);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			break;
		/*	
		case "ES":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			*/
			
			
			
		case "DE":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}

			waitForAjax();
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Update Address Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);	
			break;

		case "IE":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}

			//waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderID1 = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID1);
			break;

		case "IT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.VatNumber, VatNumber, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetType, StreetType, "Enter Street Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.province, Province, "Enter province");
			waitForAjax();
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			}
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
			if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']"),"Address Udpate"))
			{
				SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);

			}
			break;
			/*
		case "NL":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderID2 = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID2);
			break;	
			*/
		case "PT":
			waitForAjax();
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
			if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']"),"Address Udpate"))
			{
				SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);

			}
			break;

			
		case "SE":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter Subcriber Id");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNumber, Street_Number, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SecretListing,"No");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			
			break;
			

		case "CH":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Municipality,"Aarberg > BE");
			javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,30);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
			waitForAjax();
			waitForElementToAppear(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,60);
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			
			break;	
			
			default:
			
				ExtentTestManager.getTest().log(LogStatus.INFO, "Don't have address flow, Have Partial address flow");
				
		}	
			

		
	}
	/*
	public void AddAddress(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String CustomerType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Type");
		String FirstName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"First_Name");
		String LastName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Last_Name");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String DepartmentValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Department_Value");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"NIF");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubLocality");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Number");

		if(isElementPresent(By.xpath("//*[@class='actionLinks ml-4 ng-star-inserted']"),"Action"))
		{    
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");
			waitForAjax();
			//			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionOption,"Action Option");
			//			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,15);
			//			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,"AddressUpdate link");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);

			switch (Country) 
			{
			case "PT":
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.NIF, NIF, "Enter NIF Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubLocality, SubLocality, "Enter VAT Number");
				break;

			case "SE":
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNumber, Street_Number, "Enter Street Number");
				break;

			case "CH":
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNumber, Street_Number, "Enter Street Number");
				Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Municipality,"Aarberg > BE");
				break;	




			}


			if (!Country.toString().equals("PT")) 
			{
				if(isElementPresent(By.xpath("//label[contains(text(),'Service Type')]"),"Service Type"))
				{
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
				}
			}
			if(isElementPresent(By.xpath("//input[@id='emerBusinessNameUpdated']"),"Customer Name"))
			{
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			}
			//			}else{
			//				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName, CustomerName, "Enter Customer Name");
			//			}
			if(isElementPresent(By.xpath("//input[@id='siretNumber']"),"VAT Number"))
			{
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.VatNumber, VatNumber, "Enter VAT Number");
			}		


			if(isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]")))
			{
				verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"Select Country");
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.customerTypeRadioBtn.replace("Customer",CustomerType),"wholesaleradiobtn");

				if(CustomerType.toString().equals("Residential"))
				{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.FirstName, FirstName, "Enter First Name");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LastName, LastName, "Enter Last Name");
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,"Language");
					selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue,"LanguageType");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
				else if(CustomerType.toString().equals("Business")){
					if(Country.equals("DE")){
						Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
					}
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
					//Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Language,LanValue);
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					WaitForAjax();
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
			}
			else{
				WaitForAjax();
				if(isElementPresent(By.xpath("//label[contains(text(),'Service Type')]"),"Service Type")){
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ServiceType,ServiceType);
				}
				if(isElementPresent(By.xpath("//input[@id='emerBusinessNameUpdated']"),"BusinessNameUpdated")){
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
				}else{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName, CustomerName, "Enter Customer Name");
				}
				if(isElementPresent(By.xpath("//label[contains(text(),'Subscriber ID')]"),"Subscriber ID")){
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter House Number");
				}
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNameUpdated, StreetName, "Enter Street Name");
					if (isElementPresent(By.xpath("//label[contains(text(),'Department')]"))){
					selectByValue(NODNumberDirectActivationObj.NumDirActivation.DepartmentType,DepartmentValue,"Department Type");
					WaitForAjax();
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,30);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdatedFR,CityTown);
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,15);
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdatedFR,PostalCode);
				}
				else{
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
					sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");
				}
				WaitForAjax();
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTownUpdated, CityTown, "Enter City Town");
				sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCodeUpdated, PostalCode, "Enter Postal Code");


			}
			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]")))
			{                              
				javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}
			javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateAddressBtn,"Validate Address Button");
			waitForAjax();
			verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Updated Successfully");
		}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not Updated Successfullys");	

		}


	}
	 */

	public void DirectoryServiceUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String Customer_Name = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String House_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String Street_Name = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String City_Town = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String Postal_Code = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Order_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Order_Type");
		String Telephone_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"DSU_Telephone_Number");
		String Line_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Line_Type");
		String Tariff = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Tariff");
		String Entry_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Entry_Type");
		String Typeface = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Typeface");
		String Listing_Category = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Listing_Category");
		String Listing_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Listing_Type");
		String Priority = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Priority");
		String VatNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Vat_Number");
		String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Type");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String Premises_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Premises_Number");
		String Advertisement_Flag = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Advertisement_Flag");
		String Usage_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Usage_Type");
		String Confidentiality_Reverse_Search = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Confidentiality_Reverse_Search");
		String DepartmentValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Department_Value");
		String Company_Registration_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Company_Registration_Number");
		String CompanyEmail = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Company_Email");
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String SubcriberId = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubcriberId");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Lan_Value");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"NIF");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"SubLocality");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Number");

		//	if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Order"))
		//	{                              
		//		javaScriptclick(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Action Option");
		switch(Country) 
		{
		case "UK":

			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Directory Service Update");
			//				waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,15);
			//				click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,"Directory Service Update Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LineTypeDrpDwn,Line_Type,"Line Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TraffDrpDwn,Tariff,"Tariff");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.EntryTypeDrpDwn,Entry_Type,"Entry Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TypeFaceDrpDwn,Typeface,"Entry Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingCategoryDrpDwn,Listing_Category,"Listing Category");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.listingTypeDrpDwn,Listing_Type,"Listing Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PriorityDrpDwn,Priority,"Priority");
			waitForAjax();
			ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
			break;

		case "IT":

			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");

			//	click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,"AddressUpdate link");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SkipBtn,"Skip Button");

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.DSUStreetType, StreetType, "Enter Street Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.DSUStreetName, Street_Name, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.PremisesNumber, Premises_Number, "Enter Premises Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.DSUPostalCode, Postal_Code, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.DSUCityTown, City_Town, "Enter CityTown");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.DSUprovince, Province, "Enter province");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.AdvertisementFlag,Advertisement_Flag,"Advertisement Flag");

			//ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.ValidateAddressBtn);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.DSUValidateAddressBtn,"Validate Address Button");
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"Order Review Button");

			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewSection,"Order Review Section");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
			waitForAjax();
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
			if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']")))
			{
				String SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
				//String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
				//ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "DSU is Not successful");
			}
			break;

		case "PT":

			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");

			//click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateOption,"AddressUpdate link");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			waitForAjax();

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SkipBtn,"Skip Button");
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.dsuSection,"Directory Service Update Section");

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.dsuUsageType,Usage_Type,"Usage Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.dsuConfRevSearch,Confidentiality_Reverse_Search,"Confidentiality Reverse Search");

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"Order Review Button");

			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewSection,"Order Review Section");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");

			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
			if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']")))
			{
				String SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
				//String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
				//ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
			}
			else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "DSU is Not successful");
			}
			break;

		case "AT":

			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Directory Service Update");

			//waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,15);
			//click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,"Directory Service Update Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		
			waitForAjax();

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoAT, Telephone_Number, "Enter Telephone Number");

			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderId1 = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId1);

			break;


		case "FR":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Directory Service Update");

			//waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,15);
			//click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,"Directory Service Update Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		
			waitForAjax();

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
			selectByValue(NODNumberDirectActivationObj.NumDirActivation.DepartmentType,DepartmentValue,"Department Type");

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CompRegNumber, Company_Registration_Number, "Enter CompRegNumber");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CompanyEmail, CompanyEmail, "Enter Company Email Address");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderId2 = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId2);
			break;

		case "DE": //Germany
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Directory Service Update");

			//waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,15);
			//click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdateOption,"Directory Service Update Option");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		
			waitForAjax();

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderId3 = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId3);

			break;

		case "BE":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			waitForAjax();

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, CustomerName, "Enter Customer Name");
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LanguageBE,"Language");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LanguageBE,LanValue,"LanguageType");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_StreetName, StreetName, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumberUpdated, HouseNumber, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.Update_PostalCode, PostalCode, "Enter Postal Code");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}

			if(isElementPresent(By.xpath("//*[contains(text(),'The Address you have entered is partially complete')]"),"Validation Message"))
			{                              
				if(isElementPresent(By.xpath("(//*[@class='card rounded-0'])[1]"),"Suggestions"))
				{                              
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressSuggestion,"Suggested Address");
					javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,15);
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");
					waitForAjax();
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
					waitForAjax();
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
					String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);

				}
				else
				{
					Report.LogInfo("Log Report","INFO","Address suggestions are not there");
				}			
			}
			else{
				//verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");
				if(isElementPresent(By.xpath("//button[contains(text(),'Update Address')]"),"Update Address"))
				{
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
					waitForAjax();
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
					String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);

					Report.LogInfo("Log Report","INFO","No need to select the Address from suggetion");

				}
				else
				{
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingOpt,"LISTING");
					waitForAjax();

					//verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewSection,"Order Review Section");
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
					waitForAjax();

					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
					waitForAjax();

					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
					if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']"),"Address Udpate"))
					{
						String SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
						//String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
						//ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
					}

				}

			}
			break;

		case "IR":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Directory Service Update");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.BuildingNumber, House_Number, "Enter House Number");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LineTypeDrpDwn,Line_Type,"Line Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TraffDrpDwn,Tariff,"Tariff");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.EntryTypeDrpDwn,Entry_Type,"Entry Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.TypeFaceDrpDwn,Typeface,"Entry Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingCategoryDrpDwn,Listing_Category,"Listing Category");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.listingTypeDrpDwn,Listing_Type,"Listing Type");
			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PriorityDrpDwn,Priority,"Priority");
			waitForAjax();

			ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			waitForAjax();

			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderId4 = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId4);
			break;

		case "SE":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Address Update");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatePage,15);
			waitForAjax();

			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerNameUpdated, Customer_Name, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.SubcriberId, SubcriberId, "Enter Subcriber Id");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetName, Street_Name, "Enter Street Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CityOrTown, City_Town, "Enter City Town");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.PostalCode, Postal_Code, "Enter Postal Code");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.StreetNumber, Street_Number, "Enter Street Number");

			if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Button"))
			{
				javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
			}

			if(isElementPresent(By.xpath("//*[contains(text(),'The Address you have entered is partially complete')]"),"Validation Message"))
			{                              
				if(isElementPresent(By.xpath("(//*[@class='card rounded-0'])[1]"),"Suggestions"))
				{                              
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressSuggestion,"Suggested Address");
					javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,15);
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");
					waitForAjax();
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
					waitForAjax();
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
					String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);

				}
				else
				{
					Report.LogInfo("Log Report","INFO","Address suggestions are not there");
				}			
			}
			else{
				//verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdateMessage,"Address Update Success Message");
				if(isElementPresent(By.xpath("//button[contains(text(),'Update Address')]"),"Update Address"))
				{
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdatebtn,"Address Update Button");
					waitForAjax();
					verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
					String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);

					Report.LogInfo("Log Report","INFO","No need to select the Address from suggetion");

				}
				else
				{
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
					Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ListingOpt,"LISTING");
					waitForAjax();

					//verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewSection,"Order Review Section");
					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderReviewBtn,"OrderReviewBtn Button");
					waitForAjax();

					click(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SubmitButton,"Submit Button");
					waitForAjax();

					waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.OrderConfirmationMsg,15);
					if(isElementPresent(By.xpath("//div[@id='addrUpdate-l-4']"),"Address Udpate"))
					{
						String SuccessMessage=getTextFrom(NODMyTelephoneNoScreenObj.TelephoneNoScreen_DSU.SuccessMessage, "SuccessMessage");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
						//String OrderId = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
						//ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId);
					}

				}

			}
			break;

		case "CH":
			Reusable.Select(NODMyTelephoneNoScreenObj.TelephoneNoScreen.ActionDrpDwn,"Directory Service Update");
			waitForElementToBeVisible(NODMyTelephoneNoScreenObj.TelephoneNoScreen.DirServiceUpdatePage,15);		

			selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.OrderTypeDrpDwn,Order_Type,"Order Type");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.CustomerName1, Customer_Name, "Enter Customer Name");
			sendKeys(NODMyTelephoneNoScreenObj.TelephoneNoScreen.telphoneNoDrpDwn, Telephone_Number, "Enter Telephone Number");
			//selectByValue(NODMyTelephoneNoScreenObj.TelephoneNoScreen.LanguageBMU,LanValue,"LanguageType");
			waitForAjax();
			ScrollIntoViewByString(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn);
			click(NODMyTelephoneNoScreenObj.TelephoneNoScreen.UpdateDSUBtn,"Update DSU Button");
			verifyExists(NODMyTelephoneNoScreenObj.TelephoneNoScreen.AddressUpdate_SuccessMsg,"Address Update Success Message");
			String OrderId5 = getTextFrom(NODAddressUpdateObj.AddressUpdate.OrderID,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderId5);
			break;
		}

	}








}

