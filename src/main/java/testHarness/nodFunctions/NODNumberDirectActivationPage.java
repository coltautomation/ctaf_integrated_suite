package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import testHarness.commonFunctions.ReusableFunctions;

public class NODNumberDirectActivationPage extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder = new NODMyOrderPage();
    String OrderStatus;
	String OrderType;


	public void DirectActivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException {
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Postal_Code");
		String NumberType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Number_Type");
		String BlockSize = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Block_Size");
		String QuantitySize = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Quantity_Size");
		String CustomerType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Customer_Type");
		String FirstName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "First_Name");
		String LastName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Last_Name");
		String LanValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Lan_Value");
		String DirectoryListingValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Directory_Listing_Value");
		String DepartmentValue = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Department_Type");
		String LocalAreaCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Local_AreaCode");
		String Muncipality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Muncipality");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Country");
		String SubresellerOCN = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "SubresellerOCN");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "NIF");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Street_Number");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "SubLocality");
		String Street_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Street_Type");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Province"); //Add excel also
		String SubscriberID = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Subscriber_ID");
		
		
		
		
		
		
		
		verifyExists(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwn, "Quick Link Drop down");
		click(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwnArw, "Quick Link Drop down");
		waitForAjax();
		if (isElementPresent(By.xpath("//div[contains(@class,'disabled')]"),"Activation Link")) {
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"Direct activation in Italy is not allowed � please reserve the number(s) first and then request these number(s) be activated");
		} else {
			click(NODNumberDirectActivationObj.NumDirActivation.ActivationLink,"Activation link click");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ActiationPage, 15);
			switch(Country) 
			{
			case "UK":	
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
			waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
            waitForAjax();
			break;
			
			case "AT":
				waitForAjax();
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
				waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				waitForAjax();
				
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
				ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				waitForAjax();
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
				waitForAjax();
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
				waitForAjax();
				break;
				
				case "BE":
				waitForAjax();
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.Language, "Language");
				selectByValue(NODNumberDirectActivationObj.NumDirActivation.Language, LanValue, "LanguageType");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
				waitForAjax();
				waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				waitForAjax();
					
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
				waitForAjax();
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
				ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
				waitForAjax();
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,60);
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,"DirectoryListingOption");
				selectByValue(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,DirectoryListingValue, "DirectoryListingOption");
    		
				break;
				
				case "FR":
					waitForAjax();
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
						
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
					waitForAjax();
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
					ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					waitForAjax();
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,60);
					verifyExists(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,"DirectoryListingOption");
					selectByValue(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,DirectoryListingValue, "DirectoryListingOption");
	    		
					break;
				case "DE":
					waitForAjax();
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.SubresellerOCN, SubresellerOCN);
					
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
						
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
					waitForAjax();
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
					ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
				//	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					waitForAjax();
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					break;
					
					//////////////////////
					
				case "DK":
					waitForAjax();
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");

					String CityTownDrp=CityTown.trim();
					waitForElementToBeVisible(NODPortInRequestObj.portIn.CityOrTownDK,30); //Update locator
					Reusable.Select(NODPortInRequestObj.portIn.CityOrTownDK,CityTownDrp);
					waitForAjax();
					
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
					//	verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
					//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
					//	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode, LocalAreaCode);
					//	waitForAjax();
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					waitForAjax();
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

				case "NL":
					waitForAjax();
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer",CustomerType), "wholesaleradiobtn");

					//Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerNameNL,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerNameNL, CustomerName,"Enter Customer Name");

					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetNameNL, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumberNL, HouseNumber,"Enter House Number");

					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTownNL, CityTown, "Enter City Town");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCodeNL, PostalCode, "Enter Postal Code");
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

				case "PT":
					waitForAjax();
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.SubresellerOCN, SubresellerOCN);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.NIF, NIF,"NIF");

					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.NIF, NIF,"NIF");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.SubLocality, SubLocality, "Enter SubLocality");

					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCodePT, PostalCode, "Enter Postal Code");

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

					//	verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
					//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

				case "ES":
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.NIFES, NIF,"NIF");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetTypeES, Street_Type, "Enter Street Type");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumberES, HouseNumber,"Enter House Number");

					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
					sendKeys(NODPortInRequestObj.portIn.province, Province, "Enter province");

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

					//verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
					//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

				case "SE":
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.CustomerName,15);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.SubcriberId, SubscriberID , "Enter Subcriber Id");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetNumber, Street_Number, "Enter Street Number");	
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");

					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

					//verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
					//javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

				case "IE":
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
					ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

				case "CH":
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
					sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
					Reusable.Select(NODPortInRequestObj.portIn.MunicipalityCH,"Aarberg > BE"); 
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
					waitForAjax();
					waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
					ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
					Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
					waitForAjax();

					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
					waitForAjax();
					waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
					javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
					waitForAjax();
					break;

			}
	
				WaitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.OrderReview, 60);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.OrderReview, " Order Review");
				WaitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.OrderConfirmationBtn, 60);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.OrderConfirmationBtn,"Order Confirmation Btn");
				
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ActivationConfirmationMsg, 15);
				if (isElementPresent(By.xpath("//div[@id='activation-l-6']/div[@class='row ng-star-inserted']"))) {
					String SuccessMessage = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.SuccessMessage,"SuccessMessage");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :" + SuccessMessage);
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Activation is Not successful");
				}
				String DirectActivationOrderId = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.ActivationOrderId, "Order Id");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order ID:" + DirectActivationOrderId);
			}
				
	
		}
	
	
	
	
	//////////////////////////////////
	/*
	
	verifyExists(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwn, "Quick Link Drop down");
	click(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwnArw, "Quick Link Drop down");

	if (isElementPresent(By.xpath("//div[contains(@class,'disabled')]"),"Activation Link")) {
		ExtentTestManager.getTest().log(LogStatus.PASS,
				"Direct activation in Italy is not allowed � please reserve the number(s) first and then request these number(s) be activated");
	} else {
		click(NODNumberDirectActivationObj.NumDirActivation.ActivationLink,"Activation link click");
		waitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.ActiationPage, 15);
		if (isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]"),"Customer Type")) {
			
            if (Country.equals("DE")) {
				
            	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.SubresellerOCN, SubresellerOCN);
									
			}
			
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer",CustomerType), "Select Country");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer",CustomerType), "wholesaleradiobtn");

			if (CustomerType.toString().equals("Residential")) {
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.FirstName, FirstName, "Enter First Name");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.LastName, LastName, "Enter Last Name");
			} else if (CustomerType.toString().equals("Business")) {
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
			}
			switch(Country) 
			{
			case "BE":
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.Language, "Language");
				selectByValue(NODNumberDirectActivationObj.NumDirActivation.Language, LanValue, "LanguageType");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
				break;	

			
			default:
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
			}
		}			
		
		
		else {
			if (isElementPresent(By.xpath("//label[contains(text(),'service')]"),"Service Type")) {
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType, ServiceType);
			}
			
			
			
            if (Country.equals("PT")) {
				
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.SubresellerOCN, SubresellerOCN);
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.NIF, NIF,"NIF");
									
			}
			
			
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName,"Enter Customer Name");
			
			if (Country.equals("ES")) 
			{
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.NIF, NIF,"NIF");						
			}
			
			if (Country.equals("SE")) 
			{
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetNumber, Street_Number, "Enter Street Number");							
			}
			
			if (Country.equals("CH")) 
			{
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.Municipality,"Aarberg > BE");		
			}
	
			
			
			
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");

			switch(Country) 
			{
			case "FR":
				String DepartmentValueType = DepartmentValue.trim();
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.DepartmentType, DepartmentValueType);
				ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.CityOrTownFR);
				WaitForAjax();
				waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.CityOrTownFR, 30);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.CityOrTownFR, CityTown);
				waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.PostalCodeFR, 15);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.PostalCodeFR, PostalCode);
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				break;
				
		   case "CH":
				String MuncipalityType = Muncipality.trim();
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.Muncipality, "Muncipality");
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.Muncipality, MuncipalityType);
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
				break;
				
			default:
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber,"Enter House Number");
				//sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				if(Country.equals("DK")){
					String CityTownDrp=CityTown.trim();
					waitForElementToBeVisible(NODPortInRequestObj.portIn.CityOrTownDK,30);
				    Reusable.Select(NODPortInRequestObj.portIn.CityOrTownDK,CityTownDrp);
				    
				}
				else{
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
				}
				sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
			}
			
		}

		// Validate the address--------------------------------------------------------//

		if (isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]"),"Validate Address Btn")) {
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
		}
		/
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
		    if ((Country.equals("DE"))) {
		    waitForAjax(); //----AP
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			
			if (isVisible(By.xpath("//label[contains(text(),'Your Local Area Code (LAC)')]"))) {
				
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode, LocalAreaCode);
				//verifyExists(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode, "LocalAreaCode");
			//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode, "LocalAreaCode");
			//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeOption.replace("option",LocalAreaCode), "LocalAreaCode");
			}
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
		}

		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
		waitForAjax();
		/
		if (isElementPresent(By.xpath("//label[@for='customCheckAll']"),"Number CheckBox")) {
			waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
            waitForAjax();
			
            switch(Country) 
    		{
    		case "BE":
    			verifyExists(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,"DirectoryListingOption");
				selectByValue(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,DirectoryListingValue, "DirectoryListingOption");
    			break;	

    		case "SE":
    			// secret listing code
    			break;
    		}
            
            */
            
            /////////////////////////////////////////////////////////////////////
            
            

}