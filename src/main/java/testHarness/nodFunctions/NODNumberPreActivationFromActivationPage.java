package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import testHarness.commonFunctions.ReusableFunctions;

public class NODNumberPreActivationFromActivationPage extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder = new NODMyOrderPage();
	String OrderStatus;
	String OrderType;


	public void PreActivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException {
		String NumberType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Number_Type");
		String BlockSize = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Block_Size");
		String QuantitySize = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Quantity_Size");
		String LocalAreaCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Local_AreaCode");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Country");
		
		verifyExists(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwn, "Quick Link Drop down");
		click(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwnArw, "Quick Link Drop down");
		waitForAjax();
		click(NODNumberDirectActivationObj.NumDirActivation.ActivationLink,"Activation link click");
		waitForAjax();
		waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ActiationPage, 15);
		switch(Country) 
		{
		case "UK":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;

			/*
			case "AT":
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
				ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				waitForAjax();
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
				waitForAjax();
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
				waitForAjax();
				break;
			 */
		case "BE":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
			waitForAjax();
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
			ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));

			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			waitForAjax();
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, "Number AddOns Btn");
			waitForAjax();
			break;

		case "FR":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
			waitForAjax();
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
			ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			waitForAjax();
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			//waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,60);
			//verifyExists(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,"DirectoryListingOption");
			//selectByValue(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,DirectoryListingValue, "DirectoryListingOption");
			break;
			/*
			case "DE":
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
				waitForAjax();
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
				ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
				//	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
				waitForAjax();
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
				break;
			 */

		case "DK":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);
			//	verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			//	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode, LocalAreaCode);
			//	waitForAjax();
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			waitForAjax();
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;

		case "NL":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;

		case "PT":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

			//	verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;

		case "ES":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

			//verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			//	javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;

		case "SE":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

			//verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Select Number Type radio btn");
			//javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType", NumberType),"Number Type radio btn");
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;

		case "IE":
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
			ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
			Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
			waitForAjax();

			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
			waitForAjax();
			waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
			waitForAjax();
			break;
			/*
			case "CH":
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,60);
				verifyExists(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity,"Select Type And Quantity Btn");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn,60);

				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"Local Area code");
				ScrollIntoViewByString(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeValue.replace("Value", LocalAreaCode));
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize, BlockSize);
				Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize, QuantitySize);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn, " Provide Numbers Btn");
				waitForAjax();

				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,"Number Range CheckBox");
				waitForAjax();
				waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, 15);
				javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn, " Number AddOns Btn");
				waitForAjax();
				break;
			 */
		}

		WaitForAjax();
		waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.OrderReview, 60);
		if (Country.toString().equals("DK"))
		{
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.EnrichmentMsgDE,"No enrichment available message");
		}
		else
		{
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.EnrichmentMsg,"No enrichment available message");
		}
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.OrderReview, "Order Review");
		WaitForAjax();

		waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.OrderConfirmationBtn, 60);
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.OrderConfirmationBtn,"Order Confirmation Btn");

		waitForElementToAppear(NODNumberDirectActivationObj.NumDirActivation.ActivationConfirmationMsg, 15);
		if (isElementPresent(By.xpath("//div[@id='activation-l-6']/div[@class='row ng-star-inserted']"))) {
			String SuccessMessage = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.SuccessMessage,"SuccessMessage");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :" + SuccessMessage);
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Activation is Not successful");
		}
		String DirectActivationOrderId = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.ActivationOrderId, "Order Id");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order ID:" + DirectActivationOrderId);
	}


}
