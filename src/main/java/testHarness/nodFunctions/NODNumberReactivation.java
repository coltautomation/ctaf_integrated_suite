package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberReactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODNumberReactivation extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();

	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void NumberReactivate(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");
		
		switch (VerificationType) {
		case "My Telephone Numbers screen":
			NumberReactivateOnTelNumberpage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Transaction Details screen":
			
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			NumberReactivateOnTXpage(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}

		
	}
	
	
	
	
	
	
	
	
	public void NumberReactivateOnTXpage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		Reusable.Select(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Reactivate");
		waitForElementToBeVisible(NODNumberReactivationObj.Reactivate.ReactivateNumberBtn,15);
	    click(NODNumberReactivationObj.Reactivate.ReactivateNumberBtn,"Deacivate numbers button");
	    verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
	    String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
	
	}
	
	public void NumberReactivateOnTelNumberpage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]"),"Order"))
        {        
			WaitForAjax();
			waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);
	        javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
	        waitForElementToBeVisible(NODNumberReactivationObj.Reactivate.Reactivateoption,15);
	        click(NODNumberReactivationObj.Reactivate.Reactivateoption,"Reactivate link");
	        waitForElementToBeVisible(NODNumberReactivationObj.Reactivate.ReactivateNumberBtn,15);
	        click(NODNumberReactivationObj.Reactivate.ReactivateNumberBtn,"Reactivate Number button");
	        verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
	        String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
        }
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
	}
}



