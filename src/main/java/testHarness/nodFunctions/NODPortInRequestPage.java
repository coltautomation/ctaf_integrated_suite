package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
import pageObjects.nodObjects.NODChangePortInDateObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODPortInRequestPage extends SeleniumUtils 
{
	private static Utilities testSuite = new Utilities();
	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	String OrderStatus;
	String OrderType;
	
	public void PortInNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");
		
		switch (VerificationType) {
		case "Port In Activation":
			PortInActivation(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Quick Note":
		//	myOrder.FilterMyOrder_WithoutOrderUpdateDateFilter(testDataFile, sheetName, scriptNo, dataSetNo);
			myOrder.SearchByOrderStatus(testDataFile, sheetName, scriptNo, dataSetNo);
			SendQuickNote(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "My Order Screen":
        	SearchOrderByRange(testDataFile, sheetName, scriptNo, dataSetNo);
        	ModifyPortOption_OrderAction(testDataFile, sheetName, scriptNo, dataSetNo);
        	ModifyPortTest(testDataFile, sheetName, scriptNo, dataSetNo);
	         break;
         case "Transaction Details screen":
        	 SearchOrderByRange(testDataFile, sheetName, scriptNo, dataSetNo);
        	 ModifyPortOption_TXScreen(testDataFile, sheetName, scriptNo, dataSetNo);
        	 ModifyPortTest(testDataFile, sheetName, scriptNo, dataSetNo);
	         break;
		}
		}
	
	public void PortInActivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Postal_Code");
		String NewHouseNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"New_House_Number");
		String NewStreetName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"New_Street_Name");
		String NewCityTown = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"New_City_Town");
		String NewPostalCode = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"New_Postal_Code");
		String NumbersToPort = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Numbers_To_Port");
		String SingleOrMultiLineNo = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Single_Multi_LineNo");
		String MainBillingNo = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Main_BillingNo");
		String currentOperator = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Current_Operator");
		String PortInWindow = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Window");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String CustomerType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Type");
		String SubscriberID = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Subscriber_ID");
		String PhoneNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Phone_Number");
		String CustomerLanguage = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Customer_Language");
		String FirstName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "First_Name");
		String LastName = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Last_Name");
		String CompanyRegistrationNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Company_Registration_Number");
		String VATNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "VATNumber");
		String NIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "NIFNumber");
		String providedCVP = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "providedCVP");
		String CIF = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "CIFNumber");
		String DirectoryListingOption = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Directory_Listing_Option");
		
		//String OCN = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "OCN");
		//String StreetType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"StreetType");
		//String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Province");
		String Street_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Street_Number");
		String Street_Type = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Street_Type");
		String Province = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "Province");
		String SubLocality = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "SubLocality");
		String OCN = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo, "OCN");

		
			
		
		verifyExists(NODPortInRequestObj.portIn.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODPortInRequestObj.portIn.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NODPortInRequestObj.portIn.portInLink,"Port In link click");
		waitForAjax();
		waitForElementToBeVisible(NODPortInRequestObj.portIn.portInPage,15);
		if (!Country.toString().equals("ES"))
		{
			Reusable.Select(NODPortInRequestObj.portIn.ServiceType,ServiceType);
	}
		
		
		waitForAjax();
		//Enter end-customer information
		
		switch(Country) 
		{
		case "UK":
			 sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			 sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			 sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			 sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			 break;
		case "AT":
			 sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			 sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			 sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			 sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			 break;
			 
		case "BE":
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"), "Customer type");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"),"Customer type");
			waitForAjax();
			Reusable.Select(NODPortInRequestObj.portIn.Language,CustomerLanguage);
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			 break;
		case "DK":
			 sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			 sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			 sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			 sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			 break;
			 
		case "FR":
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.CompanyRegNum, CompanyRegistrationNumber, "Company Registration Number");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			 break;
			 
		case "DE": 
			waitForAjax();
			Reusable.Select(NODPortInRequestObj.portIn.OCN,OCN);
			
		//	javaScriptclick(NODPortInRequestObj.portIn.OCN,"OCN dropn down");
			waitForAjax();
			waitForAjax();
			//verifyExists(NODPortInRequestObj.portIn.OCNvalue,"OCN Value");
			//javaScriptclick(NODPortInRequestObj.portIn.OCNvalue,"OCN value");
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"), "Customer type");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"),"Customer type");
			waitForAjax();
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			 break;
			 
		case "IE": 
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"), "Customer type");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"),"Customer type");
			waitForAjax();
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			break;
			
		case "IT": 
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.VATNumber, VATNumber, "VATNumber");
			
			sendKeys(NODPortInRequestObj.portIn.StreetType, Street_Type, "Enter Street Type");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			sendKeys(NODPortInRequestObj.portIn.Province, Province, "Enter Province");
			break;
			
		case "NL": 
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"), "Customer type");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Business"),"Customer type");
			waitForAjax();
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
			break;
			
			//KK
		case "PT": 
			Reusable.Select(NODPortInRequestObj.portIn.OCN,OCN);
			sendKeys(NODPortInRequestObj.portIn.NIF, NIF, "NIFNumber");
			sendKeys(NODPortInRequestObj.portIn.providedCVP, providedCVP, "providedCVP");
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName , "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.SubLocality, SubLocality, "Enter SubLocality");
		//	sendKeys(NODPortInRequestObj.portIn.CityOrTown, CityTown , "Enter City Town");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode , "Enter Postal Code");
			
			break;
		//KK
		case "ES":
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.CIF, CIF, "CIFNumber");
			sendKeys(NODPortInRequestObj.portIn.StreetType, Street_Type, "Enter Street Type");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName , "Enter Street Name");
			sendKeys(NODPortInRequestObj.portIn.StreetNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.province, Province, "Enter province");
		//	sendKeys(NODPortInRequestObj.portIn.CityOrTown, CityTown , "Enter City Town");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode , "Enter Postal Code");
			
			
			break;
			
			//KK 07-09-21
		case "CH":
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName , "Enter Street Name");
		//	sendKeys(NODPortInRequestObj.portIn.CityOrTown, CityTown , "Enter City Town");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode , "Enter Postal Code");
			Reusable.Select(NODPortInRequestObj.portIn.Municipality,"Aarberg > BE");		
			break;
			
		case "SE":
			sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			sendKeys(NODPortInRequestObj.portIn.SubcriberId, SubscriberID , "Enter Subcriber Id");
			sendKeys(NODPortInRequestObj.portIn.StreetNumber, Street_Number, "Enter Street Number");
			sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName , "Enter Street Name");
		//	sendKeys(NODPortInRequestObj.portIn.CityOrTown, CityTown , "Enter City Town");
			sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode , "Enter Postal Code");	
			break;
			
			
		}	
			
			/*
			
			
		if (isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]"),"Customer Type")) {
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Residential"), "Customer type");
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer","Residential"),"Customer type");
			waitForAjax();
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.FirstName, FirstName, "Enter First Name");
			sendKeys(NODNumberDirectActivationObj.NumDirActivation.LastName, LastName, "Enter Last Name");
			Reusable.Select(NODPortInRequestObj.portIn.Language,CustomerLanguage);
			
		}
		
		if (isElementPresent(By.xpath("=//input[@id='customerName']"),"Customer Name")) {
		    sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
			
		}
	    
		*/
		
		//Enter end-customers current address
		/*
		sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
		sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
		sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
		
		*/
		if(Country.equals("DK")){
			String CityTownDrp=CityTown.trim();
			waitForElementToBeVisible(NODPortInRequestObj.portIn.CityOrTownDE,30);
		    Reusable.Select(NODPortInRequestObj.portIn.CityOrTownDE,CityTownDrp);
		}
		else{
		sendKeys(NODPortInRequestObj.portIn.CityOrTown, CityTown, "Enter City Town");
		}
		
		Reusable.waitToPageLoad();
		
		ScrollIntoViewByString(NODPortInRequestObj.portIn.ValidateAddressBtn);
		waitForElementToBeVisible(NODPortInRequestObj.portIn.ValidateAddressBtn,15);
		javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
	
		waitForAjax();
		waitForElementToAppear(NODPortInRequestObj.portIn.AddressValidationMessage,60);
        if(isElementPresent(By.xpath("//*[contains(text(),'Address successfully validated')]")))
        {                              
        	javaScriptclick(NODPortInRequestObj.portIn.ProvideNumbersBtn,"Provide Numbers Btn");
        }
        else{
        	Report.LogInfo("Log Report",":","Please check that if all entered Addressed fileds are correct");
        }
        
        //Provide Number Tab
        waitForElementToBeVisible(NODPortInRequestObj.portIn.NumberForPortIn,15);
        sendKeys(NODPortInRequestObj.portIn.NumberForPortIn, NumbersToPort, "Number To Port");
        
        javaScriptclick(NODPortInRequestObj.portIn.ValidateNumbers,"Validate Numbers");
        WaitForAjax();
        javaScriptclick(NODPortInRequestObj.portIn.NumberEnrichmentBtn,"Number Enrichment Btn");
        WaitForAjax();
        //Number Add-ons Tab
       // waitForElementToBeVisible(NODPortInRequestObj.portIn.MainBillingNumberChkBox,15);
       // javaScriptclick(NODPortInRequestObj.portIn.MainBillingNumberChkBox,"Main Billing Number ChkBox");
        
        
        if(!Country.equals("DE")&&!Country.equals("NL")) {
        waitForElementToBeVisible(NODPortInRequestObj.portIn.MainBillingNo,30);
        sendKeys(NODPortInRequestObj.portIn.MainBillingNo, MainBillingNo, "Main Billing Number");
        Reusable.Select(NODPortInRequestObj.portIn.SingleAndMultiLine,SingleOrMultiLineNo);
        if(Country.equals("IT")){
        	sendKeys(NODPortInRequestObj.portIn.ScreatCode, "gtr256080222M", "Secret code");
            }
        
        WaitForAjax();
        
        if(Country.equals("SE"))
        {
   
        	  Reusable.Select(NODPortInRequestObj.portIn.DirectoryListingOption1,DirectoryListingOption);
              WaitForAjax();
              
        }
        
        if(Country.equals("BE")){
        Reusable.Select(NODPortInRequestObj.portIn.DirectoryListingOption,DirectoryListingOption);
        WaitForAjax();
        }
              
        }
        
        
        javaScriptclick(NODPortInRequestObj.portIn.ProvidePortingDocBtn,"Provide Porting Documents Btn");
		
     /* String path1 = new File(".").getCanonicalPath();
		String uploadFilePath = path1+"/TestData/Test.docx";
		sendKeys(NODPortInRequestObj.portIn.ChooseFileBtn, uploadFilePath,"Choose file for Letter Of Authorisation");
	  */
        
        //upload Document Tab
			
	        String RunBrowser = testSuite.getValue("Browsers","firefox");
	        if (RunBrowser.equals(" Firefox")){
	        String path1 = new File(".").getCanonicalPath();
			String filepath="/TestData/Test.docx";
			String path2 = filepath.replaceAll("/", "\\\\");
			String uploadFilePath= path1+path2;
			Reusable.UploadFile(NODPortInRequestObj.portIn.ChooseFileBtn, uploadFilePath); //locator also updated
	        }
	        else
	        {
	        	String path1 = new File(".").getCanonicalPath();
	    		String uploadFilePath = path1+"/TestData/Test.docx";	
	    		Reusable.UploadFile(NODPortInRequestObj.portIn.ChooseFileBtn, uploadFilePath);
	        }
	     
	      
	        if(!Country.equals("NL")) {
	    LocalDate today = LocalDate.now();
	    if(!Country.equals("IE")){
		String PortInDate=(addDaysSkippingWeekends(today, 20)).toString();
		 System.out.println(PortInDate);  
		    sendKeys(NODPortInRequestObj.portIn.PortInDate, PortInDate, "Port In Date");
	        }else{
	        	String PortInDate=(addDaysSkippingWeekends(today, 10)).toString();
	        	 System.out.println(PortInDate);  
	     	    sendKeys(NODPortInRequestObj.portIn.PortInDate, PortInDate, "Port In Date");	
	        }
	   
	  
	        /*
	   verifyExists(NODPortInRequestObj.portIn.CalendarIcon,"Calendar Icon");
	     javaScriptclick(NODPortInRequestObj.portIn.CalendarIcon,"Calendar Icon");
	   verifyExists(NODPortInRequestObj.portIn.Date,"Date");
	   javaScriptclick(NODPortInRequestObj.portIn.Date,"Date");
	   WaitForAjax();
	    */
	    sendKeys(NODPortInRequestObj.portIn.PortInWindow, PortInWindow, "Port In Window");
	    if(Country.equals("FR")){
	    javaScriptclick(NODPortInRequestObj.portIn.PortInWindowOption,"Port In window option");
	    }
	    WaitForAjax();
	    
	}
	    
	    if(Country.equals("DK"))
		{
			waitForElementToBeVisible(NODPortInRequestObj.portIn.subscriberID,30);
			sendKeys(NODPortInRequestObj.portIn.subscriberID, SubscriberID, "Enter Subscriber ID");
		}
		
	    if(!Country.equals("DE")&& !Country.equals("NL")){
	    
	    verifyExists(NODPortInRequestObj.portIn.currentOperator,"currentOperator");
	    //javaScriptclick(NODPortInRequestObj.portIn.currentOperator,"currentOperator");
	    javaScriptclick(NODPortInRequestObj.portIn.currentOperatorOption.replace("operator",currentOperator),"currentOperator");
	    }
	    WaitForAjax();
	    javaScriptclick(NODPortInRequestObj.portIn.ProvideContactDetailsBtn,"Provide Contact Details Btn");
	    WaitForAjax();
	    Reusable.waitForpageloadmask();
	    Reusable.waitToPageLoad();
	    
	    
	    
	    //Add Contact Details Tab
	    if(isElementPresent(By.xpath("//input[@id='landlinePhone']")))
	    {
	    	sendKeys(NODPortInRequestObj.portIn.PhoneNum, PhoneNumber, "Phone number");
				
	    }
	    javaScriptclick(NODPortInRequestObj.portIn.ReviewYourOrderBtn,"Review Your Order Btn");
	    WaitForAjax();
	    
	    //Order Review Tab
	    javaScriptclick(NODPortInRequestObj.portIn.ConfirmYourOrderBtn,"Confirm Your Order Btn");
	    
	    //Order confirmation
	    waitForElementToBeVisible(NODPortInRequestObj.portIn.PortInConfirmationMsg,15);
	    if(isElementPresent(By.xpath("//div[@id='portIn-l-7']//div[@class='row ng-star-inserted'][1]")))
	    {
	    	String SuccessMessage=getTextFrom(NODPortInRequestObj.portIn.SuccessMessage, "SuccessMessage");
	    //	String emailNotification=getTextFrom(NODPortInRequestObj.portIn.EmailNotification, "EmailNotification");
	   // 	ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage+"And "+emailNotification);
	     	ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :" +SuccessMessage);
	 	   
	    }
	    else{
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Port in is Not successful");
	    }
	    String portInOrderId=getTextFrom(NODPortInRequestObj.portIn.portInOrderId, "SuccessMessage");
	    WaitForAjax();
	    ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+portInOrderId);
	   
	    /*
	    javaScriptclick(NODPortInRequestObj.portIn.CheckMyOrder,"Check MyOrder Btn");
	    //WaitForAjax();
	    waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
	    click(NODPortInRequestObj.portIn.SearchTxtFld, "Send Order ID");
	    sendKeys(NODPortInRequestObj.portIn.SearchTxtFld,portInOrderId , "Send Order ID");
	    WaitForAjax();
		click(NODPortInRequestObj.portIn.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
		//waitForAjax();
		 WaitForAjax();
		//String OrderStatus;
		if(isElementPresent(By.xpath("//a[@class='transaction']"))){
		 OrderStatus = getTextFrom(NODPortInRequestObj.portIn.orderStatusVal,"Order Status");
		 OrderType = getTextFrom(NODPortInRequestObj.portIn.orderType,"Order Type");
		 
		}else
		{
			 for(int i=0;i<5;i++)
	            {          
			    	WaitForAjax();
					sleep(60000);
					refreshPage();
					waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);;
				    click(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld, "Send Order ID");
				    sendKeys(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld,portInOrderId , "Send Order ID");
				    WaitForAjax();
					click(NODNumberDirectActivationObj.NumDirActivation.SearchIcon,"Search Icon");
					//waitForAjax();
					waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
	                                                         
	                 if (isElementPresent(By.xpath("//a[@class='transaction']")))
	                 {
	                	 OrderStatus = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.orderStatusVal,"Order Status"); 
	                	 OrderType = getTextFrom(NODPortInRequestObj.portIn.orderType,"Order Type");
	                     break;         	
	                 }
	                          
		        }
		
		}
		if(OrderType.equals("New Port In")) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Type Is: "+OrderType+"Order Type Is: "+OrderStatus);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order with New Port In Type is not Present");	
		}
		*/
	
	}

	public void SendQuickNote(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Comment = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Quick_Note");

		verifyExists(NODPortInRequestObj.QuickNote.OrderRow,"Searched Record");
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,120);
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]")))
        {       
			WaitForAjax();
        waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);
	        javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
	        javaScriptclick(NODPortInRequestObj.QuickNote.QuickNoteAction,"Quick Note Action");
			verifyExists(NODPortInRequestObj.QuickNote.AddNotePopUp,"Add Note Popup");

			verifyExists(NODPortInRequestObj.QuickNote.AddNoteTxtBox,"Add Note Text Box");
	        sendKeys(NODPortInRequestObj.QuickNote.AddNoteTxtBox, Comment, "Quick Note");

	        ScrollIntoViewByString(NODPortInRequestObj.QuickNote.AddNoteBtn);
		//	verifyExists(NODPortInRequestObj.QuickNote.AddNoteBtn,"Add Note Button");
			click(NODPortInRequestObj.QuickNote.AddNoteBtn,"Add Note Button");
			Reusable.waitForpageloadmask();
			Reusable.waitToPageLoad();
			verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
			  String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);
			

        
        }
        else{
      //  	Report.LogInfo("Log Report",":","Order is not found");
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	}
	
	
	
	
	public static LocalDate addDaysSkippingWeekends(LocalDate date, int days) {
	    LocalDate result = date;
	    int addedDays = 0;
	    while (addedDays < days) {
	        result = result.plusDays(1);
	        if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY || result.getDayOfWeek() == DayOfWeek.SUNDAY)) {
	            ++addedDays;
	        }
	    }
	    return result;
	}



	public void SearchOrderByRange(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String NumbersRangeStart = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Numbers_To_Port");

		click(NODMyOrdersObj.QuickLinks.SearchTxtFld, "Search Field");
		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, NumbersRangeStart, "Range Start");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODMyOrdersObj.MyOrder.OrderIDVal,30);

	}


	public void ModifyPort(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");

		switch(VerificationType) 
		{
		case "Customer Feedback Waited_My Order Screen":
			ModifyPortOption_OrderAction(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "Customer Feedback Waited_Tx Details Page":
			ModifyPortOption_TXScreen(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}

		ModifyPortTest(testDataFile, sheetName, scriptNo, dataSetNo);

	}






	public void ModifyPortOption_OrderAction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		WaitForAjax();
		waitForElementToBeVisible(NODPortInRequestObj.QuickNote.ActionOption,30);
		javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
		javaScriptclick(NODPortInRequestObj.ModifyPort.ModifyPortAction,"Modify Port Action");

	}
	public void ModifyPortOption_TXScreen(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,30);

		javaScriptclick(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Action Drop Down");
		waitForElementToBeVisible(NODPortInRequestObj.ModifyPort.ModifyPortBtn,30);
		javaScriptclick(NODPortInRequestObj.ModifyPort.ModifyPortBtn,"Modify Port Action");
	}

	public void ModifyPortTest(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String PortInMonth = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Month");
		String PortInYear = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Year");
		String PortInDay = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Day");
		String PortInTime = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"PortIn_Time");
		String currentOperator = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Current_Operator");
		String MainBillingNo = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Main_BillingNo");

		//Modify Port Form
		waitForElementToBeVisible(NODPortInRequestObj.ModifyPort.ModifyPortScreen,30);

		waitForElementToBeVisible(NODPortInRequestObj.ModifyPort.Calendarbtn,15);
		click(NODPortInRequestObj.ModifyPort.Calendarbtn,"Port Date calendar button");
		/*
		waitForElementToBeVisible(NODPortInRequestObj.ModifyPort.Month,15);
		verifyExists(NODPortInRequestObj.ModifyPort.Month,"Month");
		Reusable.Select(NODPortInRequestObj.ModifyPort.Month,PortInMonth);
		verifyExists(NODPortInRequestObj.ModifyPort.Year,"Year");
		Reusable.Select(NODPortInRequestObj.ModifyPort.Year,PortInYear);
		verifyExists(NODPortInRequestObj.ModifyPort.Day.replace("day",PortInDay),"Day");
		click(NODPortInRequestObj.ModifyPort.Day.replace("day",PortInDay),"Day");
		 */

		waitForElementToAppear(NODChangePortInDateObj.Date.SelectDate,30);
		javaScriptclick(NODChangePortInDateObj.Date.SelectDate);
		waitForAjax();

		waitForElementToBeVisible(NODPortInRequestObj.ModifyPort.PortInTime,15);
		verifyExists(NODPortInRequestObj.ModifyPort.PortInTime,"Port in time section");

		click(NODPortInRequestObj.ModifyPort.PortInTime,"Port in time section");
		verifyExists(NODPortInRequestObj.ModifyPort.Time.replace("Time",PortInTime),"Port in time");
		click(NODPortInRequestObj.ModifyPort.Time.replace("Time",PortInTime),"Port in time");
		// sendKeys(NODPortInRequestObj.ModifyPort.PortInTime, PortInTime, "Port In Window");
		WaitForAjax();
		
		if (!Country.toString().equals("DE"))
		{
		verifyExists(NODPortInRequestObj.portIn.currentOperator,"currentOperator");
		//javaScriptclick(NODPortInRequestObj.portIn.currentOperator,"currentOperator");
		javaScriptclick(NODPortInRequestObj.portIn.currentOperatorOption.replace("operator",currentOperator),"currentOperator");
		}
		
		verifyExists(NODPortInRequestObj.portIn.MainBillingNo,"Verify MainBillingNo");  
		sendKeys(NODPortInRequestObj.portIn.MainBillingNo, MainBillingNo, "Enter MainBillingNo");

		WaitForAjax();
		verifyExists(NODPortInRequestObj.ModifyPort.ModifyPort,"Modify Port button");
		click(NODPortInRequestObj.ModifyPort.ModifyPort,"Modify Port button");
		waitForAjax();

		waitForElementToBeVisible(NODPortInRequestObj.QuickNote.SuccessMsg,20);
		verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
		String OrderID = getTextFrom(NODPortInRequestObj.QuickNote.OrderId,"Order ID");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+OrderID);

	
	}



	







}



