package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODAddressUpdateObj;
import pageObjects.nodObjects.NODChangePortInDateObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODMyTelephoneNoScreenObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberDetailsAndHistoryObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODNumberPreactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODTelephoneNumberDetailsAndHistory extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();


	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void NumberDetailsAndHistory(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String ServiceType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Service_Type");
		String Telephone_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Telephone_Number");
		String OrderID;
		String SuccessMessage;

		waitForAjax();
		verifyExists(NODNumberPreactivationObj.NumPreActivation.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODNumberPreactivationObj.NumPreActivation.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NODNumberDetailsAndHistoryObj.NumberDetails.NumDetailsAndHis_DrpDwn,"Number Details & History link");
		waitForElementToBeVisible(NODNumberDetailsAndHistoryObj.NumberDetails.TypeNumberHereTxtFld,30);

//		switch (Country) 
//		{
//		case "UK":
		
		if (!Country.toString().equals("DE"))
		{
			sendKeys(NODNumberDetailsAndHistoryObj.NumberDetails.TypeNumberHereTxtFld, Telephone_Number, "Telephone Number");
			click(NODNumberDetailsAndHistoryObj.NumberDetails.SearchIcon,"Serach Icon");

			waitForElementToAppear(NODNumberDetailsAndHistoryObj.NumberDetails.Order,120);
			waitForAjax();

			//Range Start Column & data
			verifyExists(NODNumberDetailsAndHistoryObj.NumberDetails.RangeStartColumn,"Range Start Column");

			String RangeStart = getTextFrom(NODNumberDetailsAndHistoryObj.NumberDetails.RangeStartVal,"Range Start");

			if(isElementPresent(By.xpath("//div[contains(text(),'Range Start')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Range Start Is: "+RangeStart);
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Range Start is not Present");	
			}

			//Range End Column & data
			verifyExists(NODNumberDetailsAndHistoryObj.NumberDetails.RangeEndColumn,"Range End Column");

			String RangeEnd = getTextFrom(NODNumberDetailsAndHistoryObj.NumberDetails.RangeEndVal,"Range End");

			if(isElementPresent(By.xpath("//div[contains(text(),'Range End')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Range End Is: "+RangeEnd);
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Range End is not Present");	
			}

			//Address Column & data
			verifyExists(NODNumberDetailsAndHistoryObj.NumberDetails.AddressColumn,"Address Column");

			String Address = getTextFrom(NODNumberDetailsAndHistoryObj.NumberDetails.AddressVal,"Address");

			if(isElementPresent(By.xpath("//div[contains(text(),'Address')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Address Is: "+Address);
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Address is not Present");	
			}

			//Status Column & data
			verifyExists(NODNumberDetailsAndHistoryObj.NumberDetails.StatusColumn,"Status Column");

			String Status = getTextFrom(NODNumberDetailsAndHistoryObj.NumberDetails.StatusVal,"Status");

			if(isElementPresent(By.xpath("//div[contains(text(),'Status')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Status Is: "+Status);
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Status is not Present");	
			}
		}
			waitForAjax();

			//History Section
			verifyExists(NODNumberDetailsAndHistoryObj.NumberDetails.NumberHistoryRdoBtn,"Number Histroy Radio Button");
			click(NODNumberDetailsAndHistoryObj.NumberDetails.NumberHistoryRdoBtn,"Number Histroy Radio Button");
			waitForAjax();
			sendKeys(NODNumberDetailsAndHistoryObj.NumberDetails.TypeNumberHereTxtFld, Telephone_Number, "Telephone Number");
			click(NODNumberDetailsAndHistoryObj.NumberDetails.SearchIcon,"Search Icon");

			waitForElementToBeVisible(NODNumberDetailsAndHistoryObj.NumberDetails.NumberHistory,30);
			
			verifyExists(NODNumberDetailsAndHistoryObj.NumberDetails.NumberStatus,"Status");

			String Status1 = getTextFrom(NODNumberDetailsAndHistoryObj.NumberDetails.NumberStatus,"Number Status");

			if(isElementPresent(By.xpath("//*[contains(@aria-controls,'collapseStepper')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Current Status Is: "+Status1);
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "History is not Present");	
			}
			waitForAjax();
			
		//	break;
	//	}	


	}











}

