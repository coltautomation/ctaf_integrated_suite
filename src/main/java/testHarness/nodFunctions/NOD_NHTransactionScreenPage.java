package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import pageObjects.nodObjects.NOD_NHTransactionScreenObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NOD_NHTransactionScreenPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();

	private static HashMap<String, String> Number= new HashMap<String, String>();



	public void DataValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String VerificationType = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Verification_Type");
		String Country = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Country");
		String PhoneNumber = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Telephone_Number_Range");
		
		switch(VerificationType) 
		{
		case "Telephone Number History_Telephone Number screen":
			
			
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			//TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);

			if(Country.toString().equals("DE")){
			//	TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
				TelPhNumberSearch.SearchOrderUsingNumber(PhoneNumber);
			}else{
				TelPhNumberSearch.SearchOrderByID(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			NumberHistory(testDataFile, sheetName, scriptNo, dataSetNo);
			break;

		case "Telephone Number screen":
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, sheetName, scriptNo, dataSetNo);
			//	TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			if(Country.toString().equals("DE")){
				//TelPhNumberSearch.SearchOrderByNumber(testDataFile, sheetName, scriptNo, dataSetNo);
				TelPhNumberSearch.SearchOrderUsingNumber(PhoneNumber);
			}else{
				TelPhNumberSearch.SearchOrderByID(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			TelPhNumberSearch.NavigateonTXPage(testDataFile, sheetName, scriptNo, dataSetNo);
			DetailsValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		case "My Order screen":
			OpenOrderDetailsbyOrderID(testDataFile, sheetName, scriptNo, dataSetNo);
			DetailsValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}


	}














	public void NumberHistory(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String TelephoneNumberRange = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Telephone_Number_Range");

		if(isElementPresent(By.xpath("//a[contains(text(),'Number History')]"),"Number History")) 
		{
			//			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id is Present");
			//			click(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"Order ID");

			//Number History Section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.NumberHistorysSection,"Number History Section");
			click(NODTelephoneNumberSearchObj.MyNumber.NumberHistorysSection,"Number History Section");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.SearchNumberFldLbl,"Search for a number Field");
			sendKeys(NOD_NHTransactionScreenObj.NumberDetails.SearchNumberFld, TelephoneNumberRange, "Telephone Number Range");
			click(NOD_NHTransactionScreenObj.NumberDetails.SearchIcon,"Search Icon");
			Reusable.waitForAjax();

			String NumberStatus=getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.NumberStatus, "Number Status");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Number Status is :"+NumberStatus);		

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Is not Present");	
		}		
	}


	public void DetailsValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"),"Order Details")) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderIdLbl,"Order Id label");
			String orderId = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderId,"Order ID");
			Reporter.log("Order Id displayed as : " + orderId);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Id displayed as : "+ orderId);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderTypeLbl,"Order Type label");
			String orderType = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderType,"Order Type");
			Reporter.log("Order Type displayed as : " + orderType);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Type displayed as :  "+ orderType);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderStatusLbl,"Order Status label");
			String orderstatus = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderStatus,"Order Status");
			Reporter.log("Order Status displayed as : " + orderstatus);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Status displayed as : "+ orderstatus);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.UserNameLbl,"User Name label");
			String UserName = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.UserName,"UserName");
			Reporter.log("Order User Name sdisplayed as : " + UserName);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order User Name displayed as : "+ UserName);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDateLbl,"Order Date label");
			String OrderDate = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDate,"OrderDate");
			Reporter.log("Order Date displayed as : " + OrderDate);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Date displayed as : "+ OrderDate);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDescriptionLbl,"Order Description label");
			String OrderDescription = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDescription,"OrderDescription");
			Reporter.log("Order Description displayed as : " + OrderDescription);		
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Description displayed as : "+ OrderDescription);

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
		}
	}

	public void OpenOrderDetailsbyRange(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Telephone_Number = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Telephone_Number");

		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, Telephone_Number, "Send Number range");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Order ID Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderIDColumn,"Order ID Column");

		verifyExists(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
		Reusable.waitForAjax();
	}

	public void OpenOrderDetailsbyOrderID(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Order_ID = DataMiner.fngetcolvalue( sheetName, scriptNo, dataSetNo,"Order_ID");

		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, Order_ID, "Send Order ID");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		Reusable.waitForAjax();
		waitForElementToAppear(NODMyOrdersObj.MyOrder.Order,150);
		//Order ID Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderIDColumn,"Order ID Column");

		verifyExists(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");
		Reusable.waitForAjax();
	}

}



