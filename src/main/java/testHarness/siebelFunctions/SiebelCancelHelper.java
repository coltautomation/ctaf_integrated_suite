package testHarness.siebelFunctions;

import org.testng.Assert;
import com.relevantcodes.extentreports.LogStatus;

import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelCancelHelperObj;
import testHarness.commonFunctions.ReusableFunctions;



public class SiebelCancelHelper extends SeleniumUtils{
	
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void CancelOrder(Object[] Inputdata) throws Exception {
	
	waitToPageLoad();
	Reusable.waitForSiebelLoader();
	Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.orderStatus, 10);
	click(SiebelCancelHelperObj.CancelHelper.orderStatus,"Order Satus");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Order Status dropdown");

    //these do all move and click//
	click(SiebelCancelHelperObj.CancelHelper.orderCancelled,"Order Cancelled");
	// WaitforElementtobeclickable(xml.getlocator("//locators/AbandonYes"));
	// Clickon(getwebelement(xml.getlocator("//locators/AbandonYes")));
	click(SiebelCancelHelperObj.CancelHelper.orderComplete,"Order Complete");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click Order Complete");
	Reusable.waitForSiebelLoader();

	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	
	if (verifyExists(SiebelCancelHelperObj.CancelHelper.alertAccept,"Alert Accept")) {
		Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.alertAccept, 10);
		click(SiebelCancelHelperObj.CancelHelper.alertAccept, "Alert Accept");
	}
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Ordered Cancelled");
	
	}

	
	public void verifyOrderCancelled() throws Exception
	{
	waitToPageLoad();
	Reusable.waitForSiebelLoader();
	Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceTab, 10);
	javaScriptclick(SiebelCancelHelperObj.CancelHelper.serviceTab);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on service tab");
	Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceOrderSearch, 10);
	click(SiebelCancelHelperObj.CancelHelper.serviceOrderSearch, "Service Order Search");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on service order search field");
	
	waitForAjax();
	Reusable.waitForSiebelLoader();
	sendKeys(SiebelCancelHelperObj.CancelHelper.serviceOrderSearch, "Service Order Search");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter value in servie field");
	Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceOrderArrow, 10);
	click(SiebelCancelHelperObj.CancelHelper.serviceOrderArrow, "Service Order Arrow");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on service order arrow");
	String expected = getTextFrom(SiebelCancelHelperObj.CancelHelper.verifyCancelled);
	System.out.println(expected);
	String actual="Cancelled";
	    Assert.assertEquals(expected, actual);  
	    ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order Cancel is Complete");

	}

}
