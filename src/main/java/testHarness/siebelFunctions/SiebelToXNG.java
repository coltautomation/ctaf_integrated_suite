package testHarness.siebelFunctions;

import java.io.File;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import baseClasses.DataMiner;
import baseClasses.GridHelper;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.SiebelReusableFunctions;

public class SiebelToXNG extends SeleniumUtils {

	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();

	WebDriver driver;
	static ReadExcelFile read = new ReadExcelFile();
	String dataFileName = "Siebel_testData.xls";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	Random rnd = new Random();
	public  static ThreadLocal<String> NetworkReferenceIPVPN = new ThreadLocal<>();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelMode siebelmod = new SiebelMode();
	GridHelper Grid = new GridHelper();
	
	public void enterMandatoryDetailsInXNGMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{

	
		String ProdType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");

		switch (ProdType.toString()){
		case "Ethernet Line": 
		{
			siebelmod.WaveLineMidleApplet(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.addSiteADetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.addSiteBDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.WaveLineSiteEntries(testDataFile,sheetName,scriptNo,dataSetNo);

			break;
		}
		case "Wave": 
		{
			siebelmod.WaveLineMidleApplet(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.addSiteADetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.addSiteBDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.WaveLineSiteEntries(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "Private Ethernet": 
		{
			siebelmod.PrivateEthernetMiddleAplet(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.ShowfullInfo();

			siebelmod.DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.ClickHereSave();
			//SearchSiteA();
			//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			//SearchSiteB();
			//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			System.out.println("Sites Search and Entered");
			siebelmod.AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.BEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			System.out.println("Site A & B Filled");
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBTerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			//			GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			//			ClickHereSave();
			break;
		}
		case "Private Wave Node": {
			siebelmod.middleAppletPrivateWaveNode(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.alertPopUp();
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.alertPopUp();
			//ClickHereSave();
			Reusable.savePage();
			siebelmod.SearchSiteA();
			siebelmod.SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			break;
		}
		case "Ethernet Access": {

			siebelmod.ShowfullInfo();
			siebelmod.PrivateEthernetEntry(testDataFile,sheetName,scriptNo,dataSetNo);	
			siebelmod.SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.closePopUp();
			siebelmod.SaveAndCloseMask();
			siebelmod.EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
			//		SaveAndCloseMask();
			Reusable.savePage();
			siebelmod.SearchSiteA();
			siebelmod.SearchSiteEntery();
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SaveAndCloseMask();
			siebelmod.AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SaveAndCloseMask();
			siebelmod.SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			break;

		}
		case "Private Wave Service": {
			siebelmod.EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.ShowfullInfo();
			siebelmod.privateWaveServiceEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.closePopUp();
			//siebelmod.SaveAndCloseMask();
			Reusable.savePage();

			//SearchSiteA();
			//SearchSiteEntery();
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SaveAndCloseMask();
			siebelmod.SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SaveAndCloseMask();
			//Start site B
			//SearchSiteB();
			//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//SaveAndCloseMask();
			//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);	
			break;
		}
		
		
		case "Ultra Low Latency": {
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			siebelmod.alertPopUp();
			siebelmod.OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.alertPopUp();
			siebelmod.ShowfullInfo();
			siebelmod.DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.Save();
			siebelmod.middleUltraLowLatency(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.	alertPopUp();
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			//Save();
			siebelmod.alertPopUp();
			siebelmod.ClickHereSave();
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			//SearchSiteA(); 
			//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			//SearchSiteB();
			//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.ClickHereSave();
			//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.waitForSiebelLoader();
			break;
		}
/* =====
		case "Cloud Unified Communications": {
			middleApplet();
			R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "Professional Services":  {
			middleApplet();
			R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "IP Voice Solutions": {
			middleApplet();
			R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "Managed Dedicated Firewall": {
			alertPopUp();
			middleAppletManagedDedicatedFirewall(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
*/
		case "Dark Fibre": {
			Reusable.waitForSiebelLoader();
			siebelmod.alertPopUp();
			siebelmod.OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.alertPopUp();			
			siebelmod.middleAppletDarkFibre(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.ShowfullInfo();
			siebelmod.DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.Save();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.savePage();
			//SearchSiteA();
			//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			//SearchSiteB();
			//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.waitForSiebelLoader();
			//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.waitForSiebelLoader();
			break;
		}

		case "DCA Ethernet": 
		{
			siebelmod.PrivateEthernetMiddleAplet(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBSettingClick();
			siebelmod.CSPInterconnectSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			siebelmod.ShowfullInfo();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			siebelmod.SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.ClickHereSave();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();
			siebelmod.SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			siebelmod.SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			siebelmod.Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			//SearchSiteA();
			//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			//SearchSiteB();
			//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			siebelmod.AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.BEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			//Reusable.savePage();
			//siebelmod.SiteBSettingClick();
			//siebelmod.CSPInterconnectSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			//siebelmod.Save();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			siebelmod.Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBTerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			//ClickHereSave();
			siebelmod.SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.ClickHereSave();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteAVLan(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBVLan(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.ClickHereSave();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			siebelmod.SiteBDedicatedCloudAccess(testDataFile,sheetName,scriptNo,dataSetNo);
			//      	ClickHereSave();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			//          Save();
			break;
		}

		case "CPE Solutions Service": 
		{
			siebelmod.IPCPESolutionSite(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}

		case "IP VPN Service": 
		{
			siebelmod.IPVPNServicePlusAccess(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.NetworkReferenceFillService(testDataFile,sheetName,scriptNo,dataSetNo);	
			siebelmod.IPVPNServicePlusAccess1(testDataFile,sheetName,scriptNo,dataSetNo);
			siebelmod.IPVPNServiceNetworkReference(testDataFile,sheetName,scriptNo,dataSetNo);

			break;
		}

		case "Voice Line V": {


			String DeliveryTeamCountry = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DeliveryTeamCountry");
			String DeliveryTeamCity = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DeliveryTeamCity");

			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.VoiceServiceCountryDropdownAccess,"Verify VoiceServiceCountryDropdownAccess");
			click(SiebelModeObj.VoiceLineVObj.VoiceServiceCountryDropdownAccess,"Click On :VoiceServiceCountryDropdownAccess");
			verifyExists(SiebelModeObj.VoiceLineVObj.VoiceServiceInputValue,"Verify  Voice Service Country");
			click(SiebelModeObj.VoiceLineVObj.VoiceServiceInputValue,"Click On  Voice Service Country");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
			clearTextBox(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks);
			sendKeys(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"1");
			Reusable.waitForSiebelSpinnerToDisappear();


			verifyExists(SiebelModeObj.VoiceLineVObj.EgressNumberFormatDropDownAccess,"Verify EgressNumberFormatDropDownAccess");
			click(SiebelModeObj.VoiceLineVObj.EgressNumberFormatDropDownAccess,"Click On :EgressNumberFormatDropDownAccess");
			verifyExists(SiebelModeObj.VoiceLineVObj.EgreeNUmberFormat,"Verify  EgreeNUmberFormat");
			click(SiebelModeObj.VoiceLineVObj.EgreeNUmberFormat,"Click On  EgreeNUmberFormat");

			verifyExists(SiebelModeObj.VoiceLineVObj.TopologyDropDown,"Verify Topology");
			click(SiebelModeObj.VoiceLineVObj.TopologyDropDown,"Click On :Topology");
			verifyExists(SiebelModeObj.VoiceLineVObj.Topology,"Verify  Topology");
			click(SiebelModeObj.VoiceLineVObj.Topology,"Click On Topology");

			verifyExists(SiebelModeObj.VoiceLineVObj.TotalDDi,"Verify TotalDDi");
			click(SiebelModeObj.VoiceLineVObj.TotalDDi,"Click On :TotalDDi");
			sendKeys(SiebelModeObj.VoiceLineVObj.TotalDDi, "4");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.TotalDDi,Keys.ENTER);


			verifyExists(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,"Verify CustomerdefaultNumber");
			click(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,"Click On :CustomerdefaultNumber");
			sendKeys(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber, "1234");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,Keys.ENTER);


			//		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			//		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On :ClickheretoSaveAccess");
			Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.Clicktoshowfullinfo,"Verify Clicktoshowfullinfo");
			click(SiebelModeObj.VoiceLineVObj.Clicktoshowfullinfo,"Click On Show full infor");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.SiteContactlookup,"Verify SiteContactlookup");
			click(SiebelModeObj.VoiceLineVObj.SiteContactlookup,"Click On Site Contact Search");


			verifyExists(SiebelModeObj.VoiceLineVObj.Site_Contact,"Verify Site_Contact");
			click(SiebelModeObj.VoiceLineVObj.Site_Contact,"Click On Submit Site Contac");


			verifyExists(SiebelModeObj.VoiceLineVObj.ServicePartsearch,"Verify ServicePartsearch");
			click(SiebelModeObj.VoiceLineVObj.ServicePartsearch,"Click On Site Contac Search");


			verifyExists(SiebelModeObj.VoiceLineVObj.PickAccntOk,"Verify PickAccntOk");
			click(SiebelModeObj.VoiceLineVObj.PickAccntOk,"Click On Submit Service party");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamDropDown,"Verify Delivery Team DropDown");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamDropDown,"Click On Delivery Team Drop Down");

			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,"Verify Delivery team country");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,"Verify Delivery team country");
			sendKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,DeliveryTeamCountry);

			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCityDropDown,"Verify Delivery Team DropDown");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCityDropDown,"Click On Delivery Team Drop Down");

			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,"Verify DeliveryTeamCity");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,"Click On :DeliveryTeamCity");
			sendKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,DeliveryTeamCity);
			//Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,Keys.ENTER);


			//			verifyExists(SiebelModeObj.VoiceLineVObj.PerformanceReporting,"Verify PerformanceReporting");
			//			click(SiebelModeObj.VoiceLineVObj.PerformanceReporting,"Click On :PerformanceReporting");


			verifyExists(SiebelModeObj.VoiceLineVObj.WLECInvoicing,"Verify WLECInvoicing");
			click(SiebelModeObj.VoiceLineVObj.WLECInvoicing,"Click On :WLECInvoicing");


			verifyExists(SiebelModeObj.VoiceLineVObj.Crossbutton,"Verify Crossbutton");
			click(SiebelModeObj.VoiceLineVObj.Crossbutton,"Click On :Crossbutton");


			//		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			//		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();


			break;
		}

		case "IP Access":{

			  String SLA_TierValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SLA_Tier_value");
	    	  String Offnet = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Offnet");
	    	  String capacitycheckreference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Capacity_Check_Reference");
	    	  String OSSPlatformFlag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
	    	  String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
	    	  String RouterType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Type");
	    	  String RouterTechnology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Technology");
	    	  String Layer3Resilience = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer_3_Resilience");
	    	  String StreetName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Street_Name");
	    	  String Country = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Country");
	    	  String CityORTown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"City/Town");
	    	  String PostalCode = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Postal_Code");
	    	  String Premises = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Premises");
	    	  String Service_PartyName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Party_Name");
	    	  String Site_LastName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Last Name");
	    	  String AccessType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Access_Type");
	    	  String AccessTechnology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Access_Technology");
	    	  String BuildingType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Building_Type");
	    	  String CustomerSitePopStatus = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Customer_Site_Pop_Status");
	    	  String CabinetID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cabinet_ID");
	    	  String ShelfID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Shelf_ID");
	    	  String CabinetType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cabinet_Type");
	    	  String SlotID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Slot_ID");
	    	  String PhysicalPortID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
	    	  String PresentationInterface = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Presentation_Interface");
	    	  String ConnectorType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Connector_Type");
	    	  String FibreType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Fibre_Type");
	    	  String PortRole = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Port_Role");
	    	  String Access_Time_WindowORDiversityType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Access_Time_Window/DiversityType");
	    	  String RouterCountry_Access = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Country");
	    	  String RouterModel = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Model/PortRole");
	    	  String IpAddressingFormat = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Addressing_Format");
	    	  String Ipv4AddressingType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Ipv_Addressing_Type");
	    	  String BackupBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth");				
	    	  String InputPartyName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site_LastName");
				
				if (Offnet.equals("IP Access_PartialDeliver"))
				{
				
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ServiceBandwidth));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ServiceBandwidth));
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.capacitycheckreference,"Verify capacitycheckreference");
					click(SiebelModeObj.IPAccessObj.capacitycheckreference,"Click On :capacitycheckreference");
					sendKeys(SiebelModeObj.IPAccessObj.capacitycheckreference,capacitycheckreference);
					
					verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Inputossplatformflag");
					click(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Click On :Inputossplatformflag");
					sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
				
					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Verify SelectRouterTypeDropDownAccess");
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Click On RouterTypeDropDownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Verify RouterTypeDropDownAccess");
//					click(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Click On RouterTypeDropDownAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					Reusable.waitForSiebelLoader();

				    verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitforAttributeloader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Verify Layer3ResillanceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Click On Layer3ResillanceDropdownAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitforAttributeloader();
					
//					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillancePartialSelectDropdownAccess,"Verify Layer3ResillancePartialSelectDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.Layer3ResillancePartialSelectDropdownAccess,"Click On Layer3ResillancePartialSelectDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					Reusable.waitForSiebelLoader();
								
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
					click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify StreetNameAccess");
					sendKeysByJS(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetName);
					
					verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify CountryAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);
					
					verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);
					
					verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);
					
					verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify PremisesAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);
					
					verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Verify SearchButtonAccess");
					click(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Click On SearchButtonAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On SelectPickAddressAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On ServicePartyDropdownAccess");
			
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Verify InputPartyNameAccess");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,Service_PartyName);
			
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On Search");
					
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyName Submit Access");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On Site Contact SearchAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On Site Contact DropdownAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Verify InputPartyNameAccess");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,Site_LastName);
			
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessType Dropdown Access");
					click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessType Dropdown Access");
//					verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click On AccessTypeSelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessType));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify Access Technology Dropdown Access");
					click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click On Access Technology Dropdown Access");
//					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify Access Technology SelectAccess");
//					click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click On Access Technology SelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click On BuildingTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify Building Type SelectAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click On Building Type SelectAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click On CustomerSitePopStatusSelectAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						
					}

					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click On CabinetTypeDropdownAccess");
					
//					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click On CabinetTypeDropdownAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CabinetType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CabinetType));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalPortID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Verify PresentationInterfaceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Click On PresentationInterfaceDropdownAccess");
					
					
//					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
//					click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PresentationInterface));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PresentationInterface));
					Reusable.waitForSiebelLoader();

					
					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click On ConnectorTypeDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Verify ConnectorTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Click On ConnectorTypeSelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click On FibreTypeDropdownAccess");
									
//					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
//					click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));

					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
					click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
//					verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
//					click(SiebelModeObj.IPAccessObj.PortValue,"Click On PortValue");
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click On InstallTimeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click On InstallTimeSelectAccess");
					
					Reusable.waitForSiebelLoader();
					
					
					verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
					}
			
					verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry_Access);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
				

					verifyExists(SiebelModeObj.IPAccessObj.routermodel,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.routermodel,RouterModel);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel, Keys.TAB);
								
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
					click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");
					

					verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
					click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
//					click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
					
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPV4AdressingTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
					click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.Secondarybtn,"Verify Secondarybtn");
					click(SiebelModeObj.IPAccessObj.Secondarybtn,"Click On Secondarybtn");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Input oss platform flag");
					sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag, Keys.TAB);
					
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.BackupbandwidthDropDownAccess,"Verify BackupbandwidthDropDownAccess");
					click(SiebelModeObj.IPAccessObj.BackupbandwidthDropDownAccess,"Click On BackupbandwidthDropDownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.BackupBandwidthSelectValue,"Verify BackupBandwidthSelectValue");
//					click(SiebelModeObj.IPAccessObj.BackupBandwidthSelectValue,"Click On BackupBandwidthSelectValue");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BackupBandwidth));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BackupBandwidth));
				
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}

//					repeated
					verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
					click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");
					
					String StreetNameAccess = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Street_Name");
					verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetNameAccess);
				
					verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);
				
					verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);
				
					verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccesss");
					sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);
					
					verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);
				
					verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Verify SearchButtonAccess");
					sendKeys(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Click on SearchButtonAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify PickAddressButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On PickAddressButtonAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
					click(SiebelModeObj.IPAccessObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify PartyNameAccess");
					click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On PartyNameAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, Service_PartyName);
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On PartyNameSearchAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyNameSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On PartyNameSubmitAccess");
				
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On SiteContactSearchAccess");
				
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On SiteContactDropdownAccess");
				
					String SiteLastName = read.getExcelColumnValue(testDataFile, sheetName, "Site_LastName");
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Input Party Name");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess, SiteLastName);
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On LastNameSiteSubmitAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click On AccessTypeSelectAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.ProceedButton,"Verify ProceedButton");
					click(SiebelModeObj.IPAccessObj.ProceedButton,"Click On ProceedButton");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
					click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click On AccesstechnologyDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
					click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click On AccesstechnologySelectAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click On BuildingTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify BuildingTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click On BuildingTypeSelectAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click On CustomerSitePopStatusSelectAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click On CabinetTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click On CabinetTypeSelectAccess");
					
					Reusable.waitForSiebelLoader();

					String Cabinet_ID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cabinet_ID");
					verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.CabinetID,Cabinet_ID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					String shelfid = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Shelf_ID");
					verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.shelfid,shelfid);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					String Slotid = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Slot_ID");
					verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Slotid,Slotid);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					String PhysicalportId = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
					verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalportId);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Verify PresentationInterfaceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Click On PresentationInterfaceDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
					click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click On ConnectorTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Verify ConnectorTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Click On ConnectorTypeSelectAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click On FibreTypeDropdownAccess");
					
//					String FibreType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Fibre_Type");
					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
					click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
					click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
					verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
					click(SiebelModeObj.IPAccessObj.PortValue,"Click On PortValue");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click On InstallTimeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click On InstallTimeSelectAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry_Access);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess, Keys.TAB);
					
					
					Reusable.waitForSiebelLoader();
				
					verifyExists(SiebelModeObj.IPAccessObj.routermodel,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.routermodel,RouterModel);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
					click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");
					Reusable.savePage();
					Reusable.waitForSiebelLoader();

					String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
					DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	
				}	
				else
				{
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Inputossplatformflag");
					click(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Click On :Inputossplatformflag");
					sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Type"),"Router Type");
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Type"),"Router Type");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Technology"));
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Technology"));
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterTechnology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterTechnology));
					Reusable.waitForSiebelLoader();

//				    verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
//					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
//					Reusable.waitForSiebelLoader();
//					Reusable.waitForSiebelSpinnerToDisappear();
//					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Layer3ResillanceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Layer3ResillanceDropdownAccess");
					Reusable.waitForSiebelLoader();
					
//					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceSelectDropdownAccess,"Verify Layer3ResillanceSelectDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.Layer3ResillanceSelectDropdownAccess,"Click On Layer3ResillanceSelectDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.capacitycheckreference,"Capacitycheckreference");
					click(SiebelModeObj.IPAccessObj.capacitycheckreference,"Capacitycheckreference");
					sendKeys(SiebelModeObj.IPAccessObj.capacitycheckreference,capacitycheckreference);
					Reusable.waitForSiebelLoader();
					
					//moved this at top
					/*verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					
					Reusable.waitForSiebelLoader();*/
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					Reusable.waitForSiebelLoader();
					addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
				/*	verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"SelectSiteSearchAccess");
					click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"SelectSiteSearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"StreetNameAccess");
					sendKeys(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetName);
					
					verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"CountryAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);
					
					verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"CityTownAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);
					
					verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"PostalCodeAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);
					
					verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"PremisesAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);
				
					verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"SearchButtonAccess");
					click(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"SearchButtonAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify PickAddressButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On PickAddressButtonAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
					Reusable.waitForSiebelLoader();
					*/
					Reusable.waitForSiebelSpinnerToDisappear();
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On ServicePartyDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Verify InputPartyNameAccess");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,Service_PartyName);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On Search");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyName Submit Access");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Site Contact SearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Site Contact DropdownAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.LastNameAccess,"SiteContractDropdownAccess");
					click(SiebelModeObj.IPAccessObj.LastNameAccess,"SiteContractDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, InputPartyName);
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Search");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Search");
					
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					Reusable.waitForSiebelLoader();
					
			     if(Offnet.equals("Offnet"))	
			     {		    	 
						String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"3rd_Party_Connection_Reference");
						String Third_party_access_provider = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"3rd_party_access_provider");
						String IPRange = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Range");
						
						verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
						click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						Reusable.waitForSiebelLoader();
										
						
			    	 	verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
						click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
						Reusable.waitForSiebelLoader();
						
						if(SLA_TierValue.contains("SLA"))
						{
							verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
							click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
							
						}
						else 
						{
						//	verifyExists(SiebelModeObj.IPAccessObj.Customersitepopupstatusoffnet1,"Verify Customersitepopupstatusoffnet");
						//	click(SiebelModeObj.IPAccessObj.Customersitepopupstatusoffnet1,"Click Customersitepopupstatusoffnet");
							verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
							click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
						}
						Reusable.waitForSiebelLoader();
						
						verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
						click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						Reusable.waitForSiebelLoader();//
						
											
						verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
						click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click On ThirdpartyaccessproviderDropDown");
						Reusable.waitForSiebelLoader();
						if(SLA_TierValue.contains("SLA"))
						{
							verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Third_Party_Connection_Reference));
							click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Third_Party_Connection_Reference));
							//waitforAttributeloader();
						}
						else 
						{
							verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
							click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
						}
						verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conection reference");
						click(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conection reference");
						sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,Third_Party_Connection_Reference,"Enter Third party conection reference");
						Reusable.waitForSiebelLoader();
						
						if (!AccessType.equalsIgnoreCase("ULL Fibre")) 
						{
							verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown);
							click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown);
						
							if (SLA_TierValue.contains("SLA"))  {
							
								verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",IPRange));
								click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",IPRange));
							}
						
						else {
							
							verifyExists(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
							click(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
							}
						}	
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
						click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
						Reusable.waitForSiebelLoader();
			     }
				  
			     else
			     {
			    	 verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
			    	 click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
//			    	 if(SLA_TierValue.contains("SLA"))
//			    	 {
			    		 verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
			    		 click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
			    		 //waitforAttributeloader();
//			    	 }
//			    	 else 
//			    	 {
//			    		 verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
//			    		 click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
//			    	 }	
			    	 
			    	 Reusable.waitForSiebelLoader();
			    	 
			    	 verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			    	 click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			    	 Reusable.waitForSiebelLoader();
//							verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
//							click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess," Verify AccesstechnologySelectAccess");
						
			    	verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					Reusable.waitForSiebelLoader();
					
//					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify BuildingTypeDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify BuildingTypeDropdownAccess");
//					Reusable.waitForSiebelLoader();
//					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");
					
		    		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Building Type"));
		    		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Building Type"));
		    		Reusable.waitForSiebelLoader();
		    		
		    		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BuildingType));
		    		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BuildingType));
		    		Reusable.waitForSiebelLoader();
					
		    		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Customer Site Pop Status"));
		    		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Customer Site Pop Status"));
		    		Reusable.waitForSiebelLoader();
		    		
		    		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CustomerSitePopStatus));
		    		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CustomerSitePopStatus));
		    		Reusable.waitForSiebelLoader();

//		    		verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess," Verify CustomerSitePopStatusSelectAccess");
//					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess," Verify CustomerSitePopStatusSelectAccess");

					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
				}
			}
				
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"CabinetType DropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"CabinetTypeDropdownAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"CabinetTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"CabinetTypeSelectAccess");
			
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
			
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify shelfid");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			
			Reusable.waitForSiebelLoader();
			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
				
			}
			
			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalPortID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
		
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Verify PresentationInterfaceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Click PresentationInterfaceDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface ));
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Click ConnectorTypeDropdownAccess");
			Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess," Verify ConnectorTypeSelectAccess");
//				click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess," Click ConnectorTypeSelectAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click FibreTypeDropdownAccess");
			Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreTypeDropdownAccess),"Fibre Type Select Access");
//				click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreTypeDropdownAccess ),"Fibre Type SelectAccess");
		
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
			Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.IPAccessObj.SelectValueDropdown.replace("Value","Physical Port"));
//				click(SiebelModeObj.IPAccessObj.SelectValueDropdown.replace("Value","Physical Port" ));
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Click InstallTimeDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Click InstallTimeSelectAccess");
			Reusable.waitForSiebelLoader();
			
//				verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
//				click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
//				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
//				Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Access time window");
			sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType,"access time window");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess1,"Router Country");
			click(SiebelModeObj.IPAccessObj.RouterCountryAccess1,"Router Country");
			Reusable.waitForSiebelLoader();
			//sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry,"Router Country");
			click(SiebelModeObj.IPAccessObj.RouterCountrySelect.replace("Value",RouterCountry_Access),"Select Router Country");
			//Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess1, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.routermodel1,"router model");
			click(SiebelModeObj.IPAccessObj.routermodel1,"router model");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.IPAccessObj.routermodelSelect.replace("Value",RouterModel),"Select Router model");
			//sendKeys(SiebelModeObj.IPAccessObj.routermodel,routermodel,"router model");
			//Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel1, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
			Reusable.waitForSiebelLoader();
			
			//verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
			//click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
			//Reusable.waitForSiebelLoader();
			//Reusable.waitForSiebelSpinnerToDisappear();
			
			String Alerting_NotificationEmailAddress = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String SiteName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Name");

			if(SLA_TierValue.contains("SLA"))
			{
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Alerting_NotificationEmailAddress));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Alerting_NotificationEmailAddress));
				//waitforAttributeloader();
			}
			else 
			{
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
			}	
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		
	/*		verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
			click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");
		
			verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
//				verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
//				click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
//				verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPAdressingFormatSelectAccess");
//				click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();*/
		
			verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
			click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");

			verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
			// verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
			// click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");

			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPAdressingFormatSelectAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
			click(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
			sendKeys(SiebelModeObj.IPAccessObj.NextHopIp,"10.91.112.101");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","BGP"));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","BGP"));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
			click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			Reusable.waitForSiebelSpinnerToDisappear();
			
			//GetReference(testDataFile,sheetName,scriptNo, dataSetNo);
		/*	verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
			click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");
			
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		
			String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
			DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);
			Reusable.waitForSiebelLoader();
			*/
			break;
	  
		
		}

		case "SIP Trunking":	{


			String IPGuardianVariant = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String IncludeColtTechnicalContact_in_Test_Calls = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String TestWindow = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Test_Window");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IP_Range = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Range");
			String AccessLineType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"AccessLineType");


			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.VoiceServiceCountry,"Verify Voice Service Country");
			click(SiebelModeObj.SIPTrunkingObj.VoiceServiceCountry);
			verifyExists(SiebelModeObj.SIPTrunkingObj.NewVoiceServiceCountry,"Verify New Voice Service Country");
			click(SiebelModeObj.SIPTrunkingObj.NewVoiceServiceCountry);

			//	Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.CallAdmissionControl,"Verify CallAdmissionControl");
			sendKeys(SiebelModeObj.SIPTrunkingObj.CallAdmissionControl,"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
			sendKeys(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.EgressNumberFormatDropDownAccess,"Verify EgressNumberFormatDropDownAccess");
			click(SiebelModeObj.SIPTrunkingObj.EgressNumberFormatDropDownAccess);
			verifyExists(SiebelModeObj.SIPTrunkingObj.EgreeNUmberFormat,"Verify EgreeNUmberFormat");
			click(SiebelModeObj.SIPTrunkingObj.EgreeNUmberFormat);
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.InvlidCLITreatmet,"Verify InvlidCLITreatmet");
			sendKeys(SiebelModeObj.SIPTrunkingObj.InvlidCLITreatmet,"Allow");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs,"Verify TotalNumberDDIs");
			click(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs);
			sendKeys(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.IncomingDDIs,"Verify IncomingDDIs");
			click(SiebelModeObj.SIPTrunkingObj.IncomingDDIs);
			sendKeys(SiebelModeObj.SIPTrunkingObj.IncomingDDIs,"10");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.RoutingSequence,"Verify RoutingSequence");
			click(SiebelModeObj.SIPTrunkingObj.RoutingSequence);
			sendKeys(SiebelModeObj.SIPTrunkingObj.RoutingSequence,"Sequential");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","First Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","First Codec"));
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IPGuardianVariant));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IPGuardianVariant));

			verifyExists(SiebelModeObj.SIPTrunkingObj.ClickLink.replace("Value","Show More"));
			click(SiebelModeObj.SIPTrunkingObj.ClickLink.replace("Value","Show More"));

			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Second Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Second Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IncludeColtTechnicalContact_in_Test_Calls));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IncludeColtTechnicalContact_in_Test_Calls));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Third Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Third Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fourth Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fourth Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",TestWindow));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",TestWindow));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fifth Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fifth Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",CircuitIPAddressInput));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",CircuitIPAddressInput));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Sixth Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Sixth Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IP_Range));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IP_Range));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.ClickToShowFullInfo,"Verify ClickToShowFullInfo");
			click(SiebelModeObj.SIPTrunkingObj.ClickToShowFullInfo);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSiteContact,"Verify SIPSiteContact");
			click(SiebelModeObj.SIPTrunkingObj.SIPSiteContact);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.ClickOK,"Verify OK");
			click(SiebelModeObj.SIPTrunkingObj.ClickOK);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			//	Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.ServiceParty,"Verify ServiceParty");
			click(SiebelModeObj.SIPTrunkingObj.ServiceParty);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.OKSelection,"Verify OKSelection");
			click(SiebelModeObj.SIPTrunkingObj.OKSelection);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			//		Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry,"Verify SIPDeliveryTeamNewCountry");
			click(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry,"United Kingdom");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity,"Verify SIPDeliveryTeamNewCity");
			click(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity);
			sendKeys1(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity,"London");
			//			Reusable.SendkeaboardKeys(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity, Keys.ENTER);
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.CloseSymbol,"Verify CloseSymbol");
			click(SiebelModeObj.SIPTrunkingObj.CloseSymbol,"CloseSymbol");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.Save();

			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbol,"Verify SIPVoiceConfigExpandSymbol");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbol);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence,"Verify SIPTrunkSequence");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkName,"Verify SIPTrunkName");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkName,"Trunk2");
			Reusable.waitForSiebelSpinnerToDisappear();
			//Some below codes commented as we are facing functional issue.

			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineTypeDropDown,"Verify AccessLineTypeDropDown");
			click(SiebelModeObj.SIPTrunkingObj.AccessLineTypeDropDown);

			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType),"Verify AccessLineType");
			click(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType));


			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup,"Verify AccessServiceIdlookup");
			click(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Verify SearchServiceIDbox");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Product");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchText,"Verify SearchText");
			click(SiebelModeObj.SIPTrunkingObj.SearchText);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"IP Access");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess,"Verify SearchButtonProcess");
			click(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton,"Verify SearchServiceIdOKButton");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4,"Verify SIPCustomerOriginatingIPv4");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4,"10.7.235.29/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6,"Verify SIPCustomerOriginatingIPv6");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6,"10.7.235.29/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4,"Verify SIPCustomerTerminatingIPv4");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4,"10.7.235.2/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6,"Verify SIPCustomerTerminatingIPv6");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6,"10.7.235.2/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocol,"Verify SIPSignalingTransportProtocol");
			click(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocol);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocolDropdown,"Verify SIPSignalingTransportProtocolDropdown");
			click(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocolDropdown);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocol,"Verify SIPvoIPProtocol");
			click(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocol);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocolDropDown,"Verify SIPvoIPProtocolDropDown");
			click(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocolDropDown);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType,"Verify SIPEncryptionType");
			click(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType,"TLS on, SRTP off");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent,"Verify SIPProportionPercent");
			click(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent,"12");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Verify SIPTrunkSiteNameAlias");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Alias1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNATRequired,"Verify SIPNATRequired");
			click(SiebelModeObj.SIPTrunkingObj.SIPNATRequired);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNATRequired,"N");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address,"Verify SIPNNIWANIPv4Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address,"Verify SIPNNIWANIPv6Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT,"Verify SIPIPv4AddressRangeNAT");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT,"2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange,"Verify SIPVLANStartEndRange");
			click(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange,"2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVLANID,"Verify SIPVLANID");
			click(SiebelModeObj.SIPTrunkingObj.SIPVLANID);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPVLANID,"2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPASNumber,"Verify SIPASNumber");
			click(SiebelModeObj.SIPTrunkingObj.SIPASNumber);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPASNumber,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address,"Verify SIPColtHostEquipmentIPv4Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address,"Verify SIPColtHostEquipmentIPv6Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls,"Verify SIPTrunkMaxNumberofSimoultaniousCalls");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls,"4");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit,"Verify SIPTrunkInboundOutboundSplit");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit,"No");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls,"Verify SIPTrunkMaxInboundCalls");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk,"Verify SIPTrunkOverCallLimitForTrunk");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk,"Verify SIPTrunkIncallLimitForTrunk");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk,"10");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk,"Verify SIPTrunkOutCallLimitForTrunk");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC,"Verify SIPTrunkTypeofCAC");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC,"Individual Trunk CAC");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerIPPbxIdTab,"Verify SIPCustomerIPPbxIdTab");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerIPPbxIdTab);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPAddrecordSymbolIPPBX,"Verify SIPAddrecordSymbolIPPBX");
			click(SiebelModeObj.SIPTrunkingObj.SIPAddrecordSymbolIPPBX);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasName,"Verify SIPIPPBXAliasName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasName);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField,"Verify SIPIPPBXAliasNameInputField");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField,"test1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModel,"Verify SIPIPPBXModel");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModel);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModelInput,"SWE Lite7.0");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendor,"Verify SIPIPPBXVendor");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendor);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput,"Verify SIPIPPBXVendorInput");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput,"Sonus");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSOftwareVersionField,"Verify SIPSOftwareVersionField");
			click(SiebelModeObj.SIPTrunkingObj.SIPSOftwareVersionField);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput,"Verify SIPSoftwareVersionInput");
			click(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUID,"Verify SIPIPPBXAddressUID");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUID);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXLookupoptionClick,"Verify SIPIPPBXLookupoptionClick");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXLookupoptionClick);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName,"Verify SIPIPPBXAddressUIDStreetName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName,"Parker");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName,"Verify SIPIPPBXAddressUIDStreetName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName,"United Kingdom");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName,"Verify SIPIPPBXAddressUIDStreetName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName,"London");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode,"Verify SIPIPPBXAddressUIDPostalCode");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode,"*");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"Verify SIPIPPBXAddressUIDPremiseCode");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"*");

			// Click Address search button
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton,"Verify SIPAddressUIDSearchButton");
			click(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton,"Search button");	

			// Select address record
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSelectAddressRecord,"Verify SIPSelectAddressRecord");
			click(SiebelModeObj.SIPTrunkingObj.SIPSelectAddressRecord,"Address Record");

			// Click Pick address button
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPickAddressButtonForUID,"Verify SIPPickAddressButtonForUID");
			click(SiebelModeObj.SIPTrunkingObj.SIPPickAddressButtonForUID,"Pick Address button");

			// IPBX Technical Contact
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContact,"Verify SIPPBXTechnicalContact");
			click(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContact);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTecnicalContactLookup,"Verify SIPPBXTecnicalContactLookup");
			click(SiebelModeObj.SIPTrunkingObj.SIPPBXTecnicalContactLookup,"SIPPBXTecnicalContactLookup");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton,"Verify SIPPBXTechnicalContactLookupOKButton");
			click(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton,"SIPPBXTechnicalContactLookupOKButton");
			Reusable.savePage();
			//		Reusable.Save();
			Reusable.waitForSiebelLoader();

			// click on Voice Config tab to enter the IP PBX details
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigTab,"Verify SIPVoiceConfigTab");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigTab,"SIPVoiceConfigTab");

			// Selection of IP PBX in TRUNK --
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbolIPPBXSelewction,"Verify SIPVoiceConfigExpandSymbolIPPBXSelewction");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbolIPPBXSelewction,"SIPVoiceConfigExpandSymbolIPPBXSelewction");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPReClickToIPPBXLookUp,"Verify SIPReClickToIPPBXLookUp");
			click(SiebelModeObj.SIPTrunkingObj.SIPReClickToIPPBXLookUp,"SIPReClickToIPPBXLookUp");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton,"Verify SIPOKSelectionButton");
			click(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton,"SIPOKSelectionButton");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			/* ABove code for Trunk including IPPBX code */

			// Other Tab in Voice Config

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPOtherTab,"Verify Other Tab");
			click(SiebelModeObj.SIPTrunkingObj.SIPOtherTab,"Other Tab");
			Reusable.waitForSiebelLoader();
			/*	
			if (isElementPresent(By.xpath("//div[@id='colt-s-order-connection-header']//a[text()='Save']"))) {
				Reusable.Save();
				Reusable.waitForAjax();
			}

			 */	
			waitForElementToAppear(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,30);
			ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,"Verify SIPVoiceConfigOthersTabShowFullInfo");
			//		Reusable.waitForSiebelLoader();
			//		ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);
			//		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,"SIPVoiceConfigOthersTabShowFullInfo");
			javaScriptclick(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);

			Reusable.waitForSiebelLoader();	

			/*			if (isElementPresent(By.xpath("(//*[normalize-space(text()) and normalize-space(.)='Colt DDI Ranges'])[1]/preceding::span[1]"))) {
				break;
			}else{
				javaScriptclick(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);

			}

			Reusable.waitForSiebelLoader();

			 */	
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRanges,"Verify SIPVoiceConfigOthersTabColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRanges,"SIPVoiceConfigOthersTabColtDDIRanges");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs,"Verify SIPNumberOfSingleColtDDIs");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges,"Verify SIPNumberOf10ColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges,"Verify SIPNumberOf100ColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges,"Verify SIPNumberOf1000ColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRangesCloseWindow,"Verify SIPVoiceConfigOthersTabColtDDIRangesCloseWindow");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRangesCloseWindow,"SIPVoiceConfigOthersTabColtDDIRangesCloseWindow");

			Reusable.Save();
			//	Reusable.savePage();
			Reusable.waitForSiebelLoader();
			break;  
		} 

		case "Managed Virtual Firewall": {

			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFBillingTab,"Verify MVFBilling Tab");
			click(SiebelModeObj.ManagedVirtualFirewallObj.MVFBillingTab);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFShowfullInfo,"Verify MVFShowfullInfo");
			click(SiebelModeObj.ManagedVirtualFirewallObj.MVFShowfullInfo);

			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink,"Verify MVFSecurityPolicyAttachmentlink");
			//		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink);
			sendKeys(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink, "1234");
			Reusable.waitForSiebelSpinnerToDisappear();


			//	verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFAddbutton,"Verify MVFAddbutton");
			//	click(SiebelModeObj.ManagedVirtualFirewallObj.MVFAddbutton);

			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFClosebutton,"Verify MVFClosebutton");
			click(SiebelModeObj.ManagedVirtualFirewallObj.MVFClosebutton,"MVF Close button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			//		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFSavebutton,"Verify MVFSavebutton");
			//		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFSavebutton);
			//		Reusable.waitForSiebelLoader();	

			break;
		}
		case "Ethernet VPN Access": {

			String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String StreetName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Street_Name");
			String Country = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Country");
			String CityORTown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"City/Town");
			String PostalCode = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Postal_Code");
			String Premises = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Premises");
			String InputPartyName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Party_Name");	
			String InputSiteName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_LastName");
			String Offnet = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Offnet");
			String AccessType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Access_Type");
			String Third_party_access_provider = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "3rd_party_access_provider");
			String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "3rd_Party_Connection_Reference");
			String IPRange = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IP_Range");
			String BCPReference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BCP_Reference");
			String CabinetID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Cabinet_ID");
			String ShelfID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Shelf_ID");
			String SlotID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Slot_ID");
			String Physical_PortID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
			String PresentationInterface = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Presentation_Interface");
			String FibreType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Fibre_Type");


			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
			click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Click on ServiceBandwidthDropdownAccess");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
			click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.LinkTypeDropDown,"Link Type Drop Down");
			click(SiebelModeObj.EthernetVPNAccessObj.LinkTypeDropDown,"Link Type Drop Down");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.LinkTYpeValue,"Verify LinkTYpeValue");
			click(SiebelModeObj.EthernetVPNAccessObj.LinkTYpeValue,"Click on LinkTYpeValue");
			Reusable.waitForSiebelSpinnerToDisappear();


			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ResilenceOptionDropDown,"Verify ResilenceOptionDropDown");
			click(SiebelModeObj.EthernetVPNAccessObj.ResilenceOptionDropDown,"Click on ResilenceOptionDropDown");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ResilienceValue,"Verify Resilience Value");
			click(SiebelModeObj.EthernetVPNAccessObj.ResilienceValue,"Click on Resilience Value");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.OSSPlatformDropDown,"Verify OSSPlatformDropDown");
			click(SiebelModeObj.EthernetVPNAccessObj.OSSPlatformDropDown,"Click on OSSPlatformDropDown");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.OSSplatformValue,"Verify OSSplatform Value");
			click(SiebelModeObj.EthernetVPNAccessObj.OSSplatformValue,"Click on OSSplatform Value");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
			click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess Value");
			click(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess,"Click on SelectSiteSearchAccess");

			Reusable.waitForElementToAppear(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput, 10);
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"Site ID input");


			if(Offnet.equals("Offnet"))
			{
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"LON06215/026");
			}else{
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"MAD99900/011");
			}

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.SiteIDSearchBtn,"SiteIDSearchBtn");
			click(SiebelModeObj.EthernetVPNAccessObj.SiteIDSearchBtn,"SiteIDSearchBtn");
			Reusable.waitForSiebelLoader();






			//////////////////////////// below code commets
			/*
		Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.StreetNameAccess, 10);
		verifyExists(SiebelModeObj.VoiceConfigTab.StreetNameAccess,"StreetNameAccess");
		sendKeys(SiebelModeObj.VoiceConfigTab.StreetNameAccess,StreetName);

		Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CountryAccess, 10);
		verifyExists(SiebelModeObj.VoiceConfigTab.CountryAccess,"Enter Country");
		sendKeys(SiebelModeObj.VoiceConfigTab.CountryAccess,Country);

		verifyExists(SiebelModeObj.VoiceConfigTab.CityTownAccess,"Enter CityTownAccess");
		sendKeys(SiebelModeObj.VoiceConfigTab.CityTownAccess,CityORTown);

		verifyExists(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,"Postal Code");
		sendKeys(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,PostalCode);

		verifyExists(SiebelModeObj.VoiceConfigTab.PremisesAccess,"Premises");
		sendKeys(SiebelModeObj.VoiceConfigTab.PremisesAccess,Premises);

		verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Verify SearchButtonAccess");
		click(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Click on SearchButtonAccess");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Pick Address Access");
		javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Address for Site");
		verifyExists(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
		click(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
		Reusable.waitForSiebelSpinnerToDisappear();


		verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
		javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
		verifyExists(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Buiding for Site");
		click(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Address for Site");
		Reusable.waitForSiebelSpinnerToDisappear();
			 */
			////////////above code commets


			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			click(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Service Party Search Access");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click on Service Party SearchAccess");


			///////////////////////////////////////// below code commets
			/*
		waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess, 8);
		verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify Service party drop down Acccess");
		click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click on Service party drop down Acccess");

		verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify Site Contact drop down Acccess");
		click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click on site Contact drop down Acccess");

		verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
		sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,InputPartyName);

		verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
		click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click on search");

			 */
			///////////////////////////above code commets


			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyNameSubmitAccess");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click on PartyName Submit");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click on SiteContactSearchAccess");
			Reusable.waitForSiebelLoader();


			////////////////////////////////////////////////////below code comment

			/*
		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactDropdownAccess");
		click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click on SiteContactDropdownAccess");

		verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Input Site Name");
		sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,InputSiteName);

		verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
		click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click on LastNameSiteSearch");
			 */
			/////////////////////////////////////////////////////////Above code commet


			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify Pick Contact:OK");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click on Pick Contact:OK");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();





			if(Offnet.equals("Offnet"))
			{
				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on AccessTypeDropdownAccess");
				verifyExists(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("AccessTypeValue",AccessType));
				click(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("AccessTypeValue",AccessType));
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click on ThirdpartyaccessproviderDropDown");
				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click on AccesstechnologyDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyEhternetValue,"Verify AccesstechnologySelectAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyEhternetValue,"Click on AccesstechnologySelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conectionre ference");
				sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,Third_Party_Connection_Reference);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click on BuildingTypeDropdownAccess");
				Reusable.waitForSiebelSpinnerToDisappear();
				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeOFFNetSelectAccess,"Verify BuildingTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeOFFNetSelectAccess,"Click on BuildingTypeSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click on CustomerSitePopStatusDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusOFFnetSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusOFFnetSelectAccess,"Click on CustomerSitePopStatusSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Verify BCPReference");
				click(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Click on BCPReference");
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.BCPReference,BCPReference);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForSiebelLoader();


				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Verify ThirdpartyDropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Click on ThirdpartyDropDown");

				verifyExists(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
				click(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
				click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
				Reusable.waitForSiebelLoader();

			}
			else
			{

				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on AccessTypeDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click on AccessTypeSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();


				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click on AccesstechnologyDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click on AccesstechnologySelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click on BuildingTypeDropdownAccess");
				Reusable.waitForSiebelSpinnerToDisappear();
				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify BuildingTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click on BuildingTypeSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click on CustomerSitePopStatusDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click on CustomerSitePopStatusSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Verify BCPReference");
				click(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Click on BCPReference");
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.BCPReference,BCPReference);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
				click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
				Reusable.waitForSiebelLoader();

			}
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click on CabinetTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click on CabinetTypeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify ShelfID");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			//	verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			//	click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");

			Reusable.savePage();

			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,Physical_PortID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceinput,"Verify PresentationInterfaceinput");
			sendKeys(SiebelModeObj.IPAccessObj.PresentationInterfaceinput,PresentationInterface);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.PresentationInterfaceinput, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click on ConnectorTypeDropdownAccess");

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ConnectorTypeSelect,"Verify ConnectorTypeSelect");
			click(SiebelModeObj.EthernetVPNAccessObj.ConnectorTypeSelect,"Click on ConnectorTypeSelect");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click on FibreTypeDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
			click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
			click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click on PortRoleDropDown");

			verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
			click(SiebelModeObj.IPAccessObj.PortValue,"Click on PortValue");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click on InstallTimeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click on InstallTimeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();


			siebelmod.addNetwork();
			verifyExists(SiebelModeObj.Site.SiteTab,"SiteTab");
			click(SiebelModeObj.Site.SiteTab,"SiteTab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click on RouterSiteNameDropdownAccess");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.sitenamevaluevpn,"Verify sitenamevaluevpn");
			click(SiebelModeObj.EthernetVPNAccessObj.sitenamevaluevpn,"Click on sitenamevaluevpn");
			//	Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
			click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
			click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
			click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
			click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.savePage();


			/*	

		Random rand = new Random();
		String siteA = null;
		String siteB = null;
		String CircuitReference_Value =null;

		verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
		click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");

//		String CircuitReference_Value = getTextFrom((SiebelModeObj.IPAccessObj.CircuitReferenceValue),"Value");
		CircuitReference_Value = getAttributeFrom((SiebelModeObj.IPAccessObj.CircuitReferenceValue),"Value");



		if(CircuitReference_Value.isEmpty()==true)
		{
			if(isElementPresent(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site A address"))
			{

				     String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");     
				     siteA=siteA1.substring(0, 3);
			}
			if(isElementPresent(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site B address"))
			{
			     String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");

			     siteB=siteB1.substring(0, 3);
			}


			if (siteA != null && siteB!= null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitReference_Value = siteA+"/"+siteB+"/"+"LE-"+randnumb;
			}else{
				if(siteA != null)
				{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitReference_Value = siteA+"/"+siteA+"/"+"LE-"+randnumb;
				}else{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitReference_Value = siteB+"/"+siteB+"/"+"LE-"+randnumb;
				}

			}
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitReference_Value,"Circuit Reference Number");		
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}
		Reusable.savePage();
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	

			 */	
			Reusable.waitForSiebelLoader();
			break;
		}

		////0306
		case "Interconnect": 

		{
			String BEndResilienceOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
			String Coverage = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Coverage");
			String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String IPGuardianVariant = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
			String TestWindow = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Test_Window");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IPRange = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Range");
			String SiteContact = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Contact");
			String OrderReceivedDate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_Received_Date");
			String ColtActualDate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
			String Contractterm = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Contract_term");
			String OrderSignedDate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
			String AendCountry = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Country");
			String AendCityTown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_City/Town");


			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountry,"VoiceServiceCountry");
			click(SiebelModeObj.Interconnect.VoiceServiceCountry,"Click on voice service country");

			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", BEndResilienceOption),BEndResilienceOption);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", BEndResilienceOption),BEndResilienceOption);
			System.out.println("go to if loop of call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.CallAdmissionControl,"CallAdmissionControl");
			sendKeys(SiebelModeObj.Interconnect.CallAdmissionControl,"8", "Enter call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.CallAdmissionControl, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.NumberOfSignallingTrunks,"CallAdmissionControl");
			sendKeys(SiebelModeObj.Interconnect.NumberOfSignallingTrunks,"4", "Enter call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.NumberOfSignallingTrunks, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.VoiceConfigurationReference,"CallAdmissionControl");
			sendKeys(SiebelModeObj.Interconnect.VoiceConfigurationReference,"yes", "Enter call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.VoiceConfigurationReference, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.TrafficDirection,"TrafficDirection");
			click(SiebelModeObj.Interconnect.TrafficDirection,"Click on TrafficDirection");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", Coverage),Coverage);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", Coverage));
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Incoming DDI Digits"),"Incoming DDI Digits Drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Incoming DDI Digits"),"Incoming DDI Digits Drop down");
			verifyExists(SiebelModeObj.Interconnect.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);
			click(SiebelModeObj.Interconnect.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "First Codec"),"First Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "First Codec"),"First Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value", IPGuardianVariant),IPGuardianVariant);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value", IPGuardianVariant),IPGuardianVariant);

			verifyExists(SiebelModeObj.Interconnect.ClickLink.replace("Value", "Show More"),"Show More link");
			click(SiebelModeObj.Interconnect.ClickLink.replace("Value", "Show More"),"Show More link");

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Second Codec"),"Second Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Second Codec"),"Second Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IncludeColtTechnicalContactinTestCalls ),IncludeColtTechnicalContactinTestCalls);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IncludeColtTechnicalContactinTestCalls ),IncludeColtTechnicalContactinTestCalls);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Third Codec"),"Third Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Third Codec"),"Third Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",ServiceBandwidth ),ServiceBandwidth);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",ServiceBandwidth ),ServiceBandwidth);


			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fourth Codec"),"fourth Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fourth Codec"),"fourth Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",TestWindow),TestWindow);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",TestWindow),TestWindow);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fifth Codec"),"fifth Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fifth Codec"),"fifth Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",CircuitIPAddressInput),CircuitIPAddressInput);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",CircuitIPAddressInput),CircuitIPAddressInput);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Sixth Codec"),"Sixth Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Sixth Codec"),"Sixth Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPRange),IPRange);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPRange),IPRange);

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"Save Button");
			click(SiebelModeObj.Interconnect.SaveButton,"save button");
			Reusable.waitForSiebelLoader();



			verifyExists(SiebelModeObj.Interconnect.ClickToShowFullInfo,"Click To Show Full Info");
			click(SiebelModeObj.Interconnect.ClickToShowFullInfo,"click to show full info");
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.Interconnect.SiteContact,"Site Contact");
			click(SiebelModeObj.Interconnect.SiteContact,"site contact");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Interconnect.OkButton,"Ok Button");
			click(SiebelModeObj.Interconnect.OkButton,"Click on ok button of site contact");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.ServiceParty,"Service Party");
			click(SiebelModeObj.Interconnect.ServiceParty,"Click on service party");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Interconnect.OkButtonService,"OkButtonService");
			click(SiebelModeObj.Interconnect.OkButtonService,"Click on ok button of service party");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.DelieveryTeamCountry,"Delievery Team Country drop down");
			click(SiebelModeObj.Interconnect.DelieveryTeamCountry,"Delievery Team Country drop down");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",SiteContact),SiteContact);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",SiteContact),SiteContact);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.Interconnect.DeliveryTeamCity,"Delivery Team City");
			sendKeys(SiebelModeObj.Interconnect.DeliveryTeamCity,"Shanghai", "Delivery team city");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.DeliveryTeamCity, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.CrossButtonTrunk,"CrossButtonTrunk");
			click(SiebelModeObj.Interconnect.CrossButtonTrunk,"Click on cross button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
			click(SiebelModeObj.Interconnect.SaveButton,"Click on SaveButton");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.TrunkSequence,"Trunk Sequence");
			sendKeys(SiebelModeObj.Interconnect.TrunkSequence,"1", "trunk sequence");


			verifyExists(SiebelModeObj.Interconnect.TrunkName,"Trunk Name");
			sendKeys(SiebelModeObj.Interconnect.TrunkName,"Trunk1", "Enter value in trunk name");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.AccessLineType,"AccessLineType");
			click(SiebelModeObj.Interconnect.AccessLineType,"Click on access line type");	
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderSignedDate),OrderSignedDate);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderSignedDate),OrderSignedDate);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.AccessServiceID,"AccessServiceID");
			click(SiebelModeObj.Interconnect.AccessServiceID,"Click on access service id");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Interconnect.OkButtonAccess,"OkButtonAccess");
			click(SiebelModeObj.Interconnect.OkButtonAccess,"Click on ok button");

			Reusable.waitForSiebelLoader();				

			if(isElementPresent(By.xpath("//a[text()='Show full info']")))	
			{								
				System.out.println("full trunk info is available");
				verifyExists(SiebelModeObj.Interconnect.ShowFullInfoTrunk,"OkButtonAccess");
				click(SiebelModeObj.Interconnect.ShowFullInfoTrunk,"Click on ShowFullInfoTrunk");
				Reusable.waitForSiebelLoader();
			}

			verifyExists(SiebelModeObj.Interconnect.AddButtonTrunk,"AddButtonTrunk");
			click(SiebelModeObj.Interconnect.AddButtonTrunk,"Click on plus button");


			verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP4Address,"CustomerOriginatingIP4Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP4Address,"192.168.1.1", "value in customer originating IPV4 address");

			verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"CustomerOriginatingIP6Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"192.168.1.1", "value in customer originating IPV6 address");

			verifyExists(SiebelModeObj.Interconnect.CustomerTerminatingIP4Address,"CustomerTerminatingIP4Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerTerminatingIP4Address,"38.168.1.1", "value in customer originating IPV4 address");

			verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"CustomerOriginatingIP6Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"38.168.1.1", "value in customer originating IPV6 address");

			verifyExists(SiebelModeObj.Interconnect.SignallingTransportProtocol,"SignallingTransportProtocol");
			click(SiebelModeObj.Interconnect.SignallingTransportProtocol,"Click on signalling transport protocol");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderReceivedDate),OrderReceivedDate);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderReceivedDate),OrderReceivedDate);

			verifyExists(SiebelModeObj.Interconnect.VoipProtocol,"VoipProtocol");
			click(SiebelModeObj.Interconnect.VoipProtocol,"Click on voip protocol");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",ColtActualDate),ColtActualDate);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",ColtActualDate),ColtActualDate);

			verifyExists(SiebelModeObj.Interconnect.EncryptionProtocol,"EncryptionProtocol");
			click(SiebelModeObj.Interconnect.EncryptionProtocol,"Click on encryption protocol");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",Contractterm),Contractterm);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",Contractterm),Contractterm);

			verifyExists(SiebelModeObj.Interconnect.NATRequired,"NATRequired");
			sendKeys(SiebelModeObj.Interconnect.NATRequired,"N", "Enter value in NATR");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.NATRequired, Keys.TAB);

			verifyExists(SiebelModeObj.Interconnect.InboundOutboundSplit,"InboundOutboundSplit");
			sendKeys(SiebelModeObj.Interconnect.InboundOutboundSplit,"No", "Enter value in InboundOutboundSplit field");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.InboundOutboundSplit, Keys.TAB);

			verifyExists(SiebelModeObj.Interconnect.TypeOfCAC,"TypeOfCAC");
			click(SiebelModeObj.Interconnect.TypeOfCAC,"TypeOfCAC");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCountry),AendCountry);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCountry),AendCountry);

			verifyExists(SiebelModeObj.Interconnect.CrossButtonTrunk,"CrossButtonTrunk");
			click(SiebelModeObj.Interconnect.CrossButtonTrunk,"Click on cross button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
			click(SiebelModeObj.Interconnect.SaveButton,"Click on Save Button");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();

			verifyExists(SiebelModeObj.Interconnect.VoiceTab,"VoiceTab");
			click(SiebelModeObj.Interconnect.VoiceTab,"Click on voice feature tab");
			Reusable.waitForSiebelLoader();
			// 			waitToPageLoad();

			verifyExists(SiebelModeObj.Interconnect.Language,"Language");
			sendKeys(SiebelModeObj.Interconnect.Language,"English", "Select language");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.Language, Keys.TAB);

			verifyExists(SiebelModeObj.Interconnect.Resillence,"resillence dropdown");
			click(SiebelModeObj.Interconnect.Resillence,"Click on resillence dropdown");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCityTown),AendCityTown);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCityTown),AendCityTown);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
			click(SiebelModeObj.Interconnect.SaveButton,"Click on SaveButton");
			Reusable.waitForSiebelLoader();
			//	waitToPageLoad();
			try {
				verifyExists(SiebelModeObj.Interconnect.ServiceGroupTab,"ServiceGroupTab");
				click(SiebelModeObj.Interconnect.ServiceGroupTab,"Click on service tab");
			}
			catch(Exception ex) {
				Reusable.Select1(SiebelModeObj.Interconnect.InstalltionDropdown,"Service Group");
			}

			Reusable.waitForSiebelLoader();
			//		waitToPageLoad();

			verifyExists(SiebelModeObj.Interconnect.ClickOnPlusButtonService,"ServiceGroupReference");
			click(SiebelModeObj.Interconnect.ClickOnPlusButtonService,"Click on plus button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.ServiceGroupReference,"ServiceGroupReference");
			click(SiebelModeObj.Interconnect.ServiceGroupReference,"Click on ServiceGroupReference button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.ClickOkButton,"ClickOkButton");
			click(SiebelModeObj.Interconnect.ClickOkButton,"Click on ok button");
			Reusable.waitForSiebelLoader();

			break;

		}
		case "IP Domain": {

			String DomainName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Domain_Name");


			System.out.println("Enter Middle Applet");

			System.out.println("Move Performed");

			verifyExists(SiebelModeObj.IPDomain.Clicktoshowfullinfomiddle,"Verify Clicktoshowfullinfomiddle");
			click(SiebelModeObj.IPDomain.Clicktoshowfullinfomiddle,"Click onClicktoshowfullinfomiddle");
			verifyExists(SiebelModeObj.IPDomain.Domainname," Verify SearchInput");
			sendKeys(SiebelModeObj.IPDomain.Domainname,"1","DomainName");
			verifyExists(SiebelModeObj.IPDomain.DomainordertypeData,"Verify DomainordertypeData");
			click(SiebelModeObj.IPDomain.DomainordertypeData,"Click on DomainordertypeData");
			verifyExists(SiebelModeObj.IPDomain.DNStype,"Verify DNStype");
			click(SiebelModeObj.IPDomain.DNStype,"Click on DNStype");
			verifyExists(SiebelModeObj.IPDomain.DNSTypeInput,"Verify DNSTypeInput");
			click(SiebelModeObj.IPDomain.DNSTypeInput,"Click on DNSTypeInput");

			verifyExists(SiebelModeObj.IPDomain.Crossbutton,"Verify Crossbutton");
			click(SiebelModeObj.IPDomain.Crossbutton,"Click on Crossbutton");
			verifyExists(SiebelModeObj.IPDomain.SaveButtonClick,"Verify SaveButtonClick");
			click(SiebelModeObj.IPDomain.SaveButtonClick,"Click on SaveButtonClick");
			System.out.println("Middle Complete");
			break;
		}
		case "IP Guardian": {

			String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String AutoMitigation = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
			String CustomerDNSResolvers = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");
			String IPGuardianVariant = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String TestWindow = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IPRange = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Range");

			System.out.println("Enter Gurdian Middle Applet");

			verifyExists(SiebelModeObj.IPGuardian.AlertingNotification," Verify AlertingNotification");
			sendKeys(SiebelModeObj.IPGuardian.AlertingNotification,AlertingNotificationEmailAddress,"Enter AlertingNotification");

			verifyExists(SiebelModeObj.IPGuardian.Automigration," Verify AutoMitigation");
			sendKeys(SiebelModeObj.IPGuardian.Automigration,AutoMitigation,"Enter AutoMitigation");

			verifyExists(SiebelModeObj.IPGuardian.Customerdnsresolve," Verify Customerdnsresolve");
			sendKeys(SiebelModeObj.IPGuardian.Customerdnsresolve,CustomerDNSResolvers,"Enter Customer dns resolve");

			verifyExists(SiebelModeObj.IPGuardian.IpguardianVariant," Verify IpguardianVariant");
			sendKeys(SiebelModeObj.IPGuardian.IpguardianVariant,IPGuardianVariant,"Enter IP guardiant variant");
			verifyExists(SiebelModeObj.IPGuardian.Colttechnicaltestcalls," Verify Colttechnicaltestcalls");
			sendKeys(SiebelModeObj.IPGuardian.Colttechnicaltestcalls,IncludeColtTechnicalContactinTestCalls,"Enter Enter Colt technical cells");

			verifyExists(SiebelModeObj.IPGuardian.servicebandwidth," Verify servicebandwidth");
			sendKeys(SiebelModeObj.IPGuardian.servicebandwidth,ServiceBandwidth,"Enter service bandwidth");

			verifyExists(SiebelModeObj.IPGuardian.Testwindowipaccess," Verify servicebandwidth");
			sendKeys(SiebelModeObj.IPGuardian.Testwindowipaccess,TestWindow,"Enter Test Window");

			//verifyExists(SiebelModeObj.IPGuardian.IpGurdianSave," Verify IpGurdianSave");
			//click(SiebelModeObj.IPGuardian.IpGurdianSave,"Click on IpGurdianSave");

			Reusable.waitForSiebelLoader();
			//waitToPageLoad();

			mouseMoveOn(SiebelModeObj.IPGuardian.CircuitipaddressClick);

			verifyExists(SiebelModeObj.IPGuardian.CircuitipaddressClick," Verify CircuitipaddressClick");
			click(SiebelModeObj.IPGuardian.CircuitipaddressClick,"click on circuit ip address");

			verifyExists(SiebelModeObj.IPGuardian.CircuitIPAddressInput," Verify CircuitIPAddressInput");
			click(SiebelModeObj.IPGuardian.CircuitIPAddressInput,"click on circuit ip address");

			verifyExists(SiebelModeObj.IPGuardian.CircuitIPAddressInput," Verify CircuitIPAddressInput");
			sendKeys(SiebelModeObj.IPGuardian.CircuitIPAddressInput,CircuitIPAddressInput,"Input IP address");

			verifyExists(SiebelModeObj.IPGuardian.IpRange," Verify IpRange");
			sendKeys(SiebelModeObj.IPGuardian.IpRange,IPRange,"Input IP Range");
			verifyExists(SiebelModeObj.IPGuardian.CrossButtonGurdian," Verify CrossButtonGurdian");
			click(SiebelModeObj.IPGuardian.CrossButtonGurdian,"click on CrossButtonGurdian");

			verifyExists(SiebelModeObj.IPGuardian.IpGurdianSave," Verify IpGurdianSave");
			click(SiebelModeObj.IPGuardian.IpGurdianSave,"click on IpGurdianSave");

			break;

		}
		case "Number Hosting":
		{

			String B_End_Resilience_Option = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
			String Coverage = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Coverage");
			String Alerting_Notification_Email_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String IP_Guardian_Variant = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String Include_Colt_Technical_Contact_in_Test_Calls = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String Service_Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
			String Test_Window = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Test_Window");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IP_Range = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IP_Range");
			String Site_Contact = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Contact");
			String Order_Signed_Date = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
			String Order_Received_Date = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_Received_Date");
			String Colt_Actual_Date = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
			String Contract_term = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Contract_term");
			String Aend_Country = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Country");
			String Aend_CityTown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_City/Town");

			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountry,"voice service country");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountry,"voice service country");
			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", B_End_Resilience_Option));
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", B_End_Resilience_Option));

			verifyExists(SiebelModeObj.NumberHostingObj.CallAdmissionControl,"8");
			sendKeys(SiebelModeObj.NumberHostingObj.CallAdmissionControl,"8");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.CallAdmissionControl, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks,"4");
			sendKeys(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks,"4");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference,"yes");
			sendKeys(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference,"yes");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.TrafficDirection,"Traffic Direction");
			click(SiebelModeObj.NumberHostingObj.TrafficDirection,"Traffic Direction");
			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Coverage));
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Coverage));

			//R5 Coverage Updating Code From Excel
			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Incoming DDI Digits"));
			click(SiebelModeObj.NumberHostingObj.SelectValueDropdown.replace("Value", Alerting_Notification_Email_Address));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "First Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", IP_Guardian_Variant));

			click(SiebelModeObj.NumberHostingObj.ClickLink.replace("Value", "Show More"));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Second Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Include_Colt_Technical_Contact_in_Test_Calls));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Third Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Service_Bandwidth));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fourth Codec"));
			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fourth Codec"));
			verifyExists(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Test_Window));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Test_Window));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fifth Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", CircuitIPAddressInput));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Sixth Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value",IP_Range));

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"Save Button");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"Save Button");
			waitToPageLoad();

			verifyExists(SiebelModeObj.NumberHostingObj.ShowMore,"Show More");
			click(SiebelModeObj.NumberHostingObj.ShowMore,"Show More");
			waitToPageLoad();

			verifyExists(SiebelModeObj.NumberHostingObj.ClickToShowFullInfo,"click to show full info");
			click(SiebelModeObj.NumberHostingObj.ClickToShowFullInfo,"click to show full info");

			verifyExists(SiebelModeObj.NumberHostingObj.SiteContact,"Site Contact");
			click(SiebelModeObj.NumberHostingObj.SiteContact,"Site Contact");
			waitToPageLoad();
			verifyExists(SiebelModeObj.NumberHostingObj.OkButton,"Ok Button");
			click(SiebelModeObj.NumberHostingObj.OkButton,"Ok Button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ServiceParty,"Service Party");
			click(SiebelModeObj.NumberHostingObj.ServiceParty,"Service Party");
			waitToPageLoad();
			verifyExists(SiebelModeObj.NumberHostingObj.OkButtonService,"Ok Button Service");
			click(SiebelModeObj.NumberHostingObj.OkButtonService,"Ok Button Service");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.DelieveryTeamCountry,"DelieveryTeamCountry");
			click(SiebelModeObj.NumberHostingObj.DelieveryTeamCountry,"DelieveryTeamCountry");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Site_Contact));
			waitToPageLoad();

			sendKeys(SiebelModeObj.NumberHostingObj.DeliveryTeamCity,"Shanghai");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.DeliveryTeamCity, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"cross button");
			click(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"cross button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"Save button");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"Save button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.TrunkConfiguration,"Trunk Configuration");
			sendKeys(SiebelModeObj.NumberHostingObj.TrunkSequence,"1");
			sendKeys(SiebelModeObj.NumberHostingObj.TrunkName,"Trunk1");

			verifyExists(SiebelModeObj.NumberHostingObj.AccessLineType,"AccessLine Type");
			click(SiebelModeObj.NumberHostingObj.AccessLineType,"AccessLine Type");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value",Order_Signed_Date));
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			//Popup loading takeing time

			verifyExists(SiebelModeObj.NumberHostingObj.AccessServiceID,"AccessServiceID");
			click(SiebelModeObj.NumberHostingObj.AccessServiceID,"AccessServiceID");

			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.NumberHostingObj.OkButtonAccess,"OkButtonAccess");
			click(SiebelModeObj.NumberHostingObj.OkButtonAccess,"OkButtonAccess");

			//Used in SIP Trunking//
			/*
			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup,"Verify AccessServiceIdlookup");
			click(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Verify SearchServiceIDbox");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Product");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchText,"Verify SearchText");
			click(SiebelModeObj.SIPTrunkingObj.SearchText);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"IP Access");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess,"Verify SearchButtonProcess");
			click(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton,"Verify SearchServiceIdOKButton");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
			Reusable.waitForSiebelLoader();
			 */
			////////////////////////////////////////

			if (isElementPresent(By.xpath("//a[text()='Show full info']")))
			{
				verifyExists(SiebelModeObj.NumberHostingObj.ShowFullInfoTrunk,"ShowFullInfoTrunk");
				click(SiebelModeObj.NumberHostingObj.ShowFullInfoTrunk,"ShowFullInfoTrunk");

			}
			verifyExists(SiebelModeObj.NumberHostingObj.AddButtonTrunk,"AddButtonTrunk");
			click(SiebelModeObj.NumberHostingObj.AddButtonTrunk,"AddButtonTrunk");

			verifyExists(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP4Address,"CustomerOriginatingIP4Address");
			sendKeys(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP4Address,"192.168.1.1");
			sendKeys(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP6Address,"192.18.1.2");

			sendKeys(SiebelModeObj.NumberHostingObj.CustomerTerminatingIP4Address,"38.68.1.1");
			sendKeys(SiebelModeObj.NumberHostingObj.CustomerTerminatingIP6Address,"38.168.1.1");

			verifyExists(SiebelModeObj.NumberHostingObj.SignallingTransportProtocol,"SignallingTransportProtocol");
			click(SiebelModeObj.NumberHostingObj.SignallingTransportProtocol,"SignallingTransportProtocol");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Order_Received_Date));

			verifyExists(SiebelModeObj.NumberHostingObj.VoipProtocol,"VoipProtocol");
			click(SiebelModeObj.NumberHostingObj.VoipProtocol,"VoipProtocol");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Colt_Actual_Date));

			verifyExists(SiebelModeObj.NumberHostingObj.EncryptionProtocol,"EncryptionProtocol");
			click(SiebelModeObj.NumberHostingObj.EncryptionProtocol,"EncryptionProtocol");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Contract_term));

			sendKeys(SiebelModeObj.NumberHostingObj.NATRequired,"N");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.NATRequired, Keys.TAB);

			verifyExists(SiebelModeObj.NumberHostingObj.InboundOutboundSplit,"InboundOutboundSplit");
			sendKeys(SiebelModeObj.NumberHostingObj.InboundOutboundSplit,"No");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.InboundOutboundSplit, Keys.TAB);

			verifyExists(SiebelModeObj.NumberHostingObj.TypeOfCAC,"TypeOfCAC");
			click(SiebelModeObj.NumberHostingObj.TypeOfCAC,"TypeOfCAC");
			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_Country));
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_Country));

			verifyExists(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"CrossButtonTrunk");
			click(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"CrossButtonTrunk");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			click(SiebelModeObj.NumberHostingObj.OtherTabClick,"OtherTabClick");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelModeObj.NumberHostingObj.TextInput.replace("Value", "Internal Routing Prefix"),"45");
			sendKeys(SiebelModeObj.NumberHostingObj.TextInput.replace("Value", "Reseller Profile"),"colt");

			verifyExists(SiebelModeObj.NumberHostingObj.VoiceTab,"VoiceTab");
			click(SiebelModeObj.NumberHostingObj.VoiceTab,"VoiceTab");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelModeObj.NumberHostingObj.Language,"English");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.Language, Keys.TAB);

			verifyExists(SiebelModeObj.NumberHostingObj.Resillence,"Resillence");
			click(SiebelModeObj.NumberHostingObj.Resillence,"Resillence");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_CityTown));

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			try
			{
				verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");
				click(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");

			}
			catch(Exception ex)
			{
				verifyExists(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"InstalltionDropdown");
				Reusable.Select1(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"Service Group");

			}

			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ClickOnPlusButtonService,"ClickOnPlusButtonService");
			click(SiebelModeObj.NumberHostingObj.ClickOnPlusButtonService,"ClickOnPlusButtonService");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupReference,"ServiceGroupReference");
			click(SiebelModeObj.NumberHostingObj.ServiceGroupReference,"ServiceGroupReference");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ClickOkButton,"ClickOkButton");
			click(SiebelModeObj.NumberHostingObj.ClickOkButton,"ClickOkButton");
			Reusable.waitForSiebelLoader();
			break;

		}

	default:{
			System.out.println("Above product is not exist in current list");
			break;
		}
		}
	}
	public void addSiteAXNGDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SiteAID= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Site_ID");

		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA, "Search Address SiteA");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA,"Click on Search Address SiteA");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteAXNGDetails.SiteID,"Enter Site ID");
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelModeObj.addSiteAXNGDetails.SiteID,SiteAID,"Site ID Value");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		click(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

	}

	public void addSiteBXNGDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SiteBID= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Site_ID");


		verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchAddressSiteB, "Search Address SiteA");
		click(SiebelModeObj.addSiteAXNGDetails.SearchAddressSiteB,"Click on Search Address SiteA");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteAXNGDetails.SiteID,"Enter Site ID");
		sendKeys(SiebelModeObj.addSiteAXNGDetails.SiteID,SiteBID,"Site ID Value");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		click(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
	}
}
