package testHarness.siebelGalileoFunctions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

import pageObjects.siebelGalileoObjects.CustomerTimeJourneyObj;
import pageObjects.siebelGalileoObjects.SiebelCockpitObj;
import pageObjects.siebelGalileoObjects.SiebelLeadTimeWorkflowObj;
import pageObjects.siebelGalileoObjects.WelcomeMailObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebeltoNCObjects.SiebelAddProdcutObj;
import pageObjects.siebeltoNCObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

public class CustomerTimeJourney extends SeleniumUtils 
{

	public static final String expectedCPDICDDueShortlyColumns= "Service Order Ref,Customer Service Contact,Legal Customer Name,MRR (Calc),MRC Value,Order Type/Sub Type,Order Status,Overall Connection Type (calc.),Customer Delay,COLT Delay,Followup Date,Followup Notes,Delivery Start Date,CRD,CPD,ICD,EID,Product Name,Overall CDD (calc),Overall RFS (calc),MRR Country,Resilient";
	public static final String expectedCPDICDNotSetColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, MRR Calc, MRC value, Order Type/Sub Type, Order Status, Overall Connection Type (calc.), Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Product Name, Billing Start Date, Billing End Date, MRR Country, Resilient, Project Id, Priority, Contract Country, OLO Leakage (Overall)";
	public static final String expectedDelaysColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, Colt/Customer Delay, Delay Start, Delay End, Expected Resolve Date, Next Customer Update, Task Delay Reason, Originator, MRR Calc, MRC value, Order Type/Sub Type, Order Status, Cockpit Connection Type, Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Delay Owner, Delay Owner Group, Product Name, MRR Country, Return Sched WIs On, Contract Country, Resilient, OLO Leakage (Overall)";
	public static final String expectedActivitiesColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, Activtiy Type, Activity Created Date, Description, Comments, MRR (Calc), MRC Value, Order Type/Sub Type, Order Status, Cockpit Connection Type, Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Product Name, MRR Country, Contract Country, Resilient";
	public static final String expectedPriorityOrdersColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, MRR Calc, MRC value, Long CRD, Return Date, Fast track Fee, Order Type/Subtype, Order Status, Overall Connection Type (calc.), Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Product Name, Priority, Escalation Owner, MRR Country, Resilient, OLO Leakage (Overall)";
	public static final String expectedServiceOrderColumns= "New, Service Order Ref, Fast Track flag, Customer Service Contact, Legal Customer Name, OCN, MRR Calc, MRC value, Order Type/Subtype, Order Status, Overall Connection Type (calc.), Customer Delay, COLT Delay, Delivery Start Date, CRD, CPD, Followup Date, Followup Notes, ICD, EID, Overall CDD (calc), Overall RFS (calc), Billing Start Date, Billing End Date, Product Name, Contract Country, Primary A Country, Primary A City, Primary A Address, Primary B Country, Primary B City, Primary B Address, Secondary A Country, Secondary A City, Secondary A Address, Secondary B Country, Secondary B City, Secondary B Address, Project ID, Project Manager, Priority, Escalation Owner, Site A Order Owner, Site B Order Owner, Related Order Numbers, MRR Country, Workflow Retriggered, Favourite, Favourite Flagged By, Bespoke Reference, Network Reference, Resilient, Diversity, Diverse From Service Reference, Customer Defined Route, OLO Leakage (Overall)";
	public static final String expectedCustomerDelayAuditTrialColumns= "Employee Login,Date,Field,Old Value,New Value";

	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void OpenServiceOrderNumber(String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderNumber");

	// if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
	// // System.out.println("Alert Present");
	// verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
	// click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
	// }
	// Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
	
		try
		{
		if(isVisible(SiebelModeObj.ServiceTab.ServiceOrderTab)){
		javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		}
		else{
		WebElement serviceOrderTabSelect= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderTabSelectArrow);
		Select element = new Select(serviceOrderTabSelect);
		element.selectByVisibleText("Service Order");
		}
		}
		catch (Exception e1)
		{
		e1.printStackTrace();
		}
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderNumber,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		Reusable.waitForSiebelLoader();
	
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();
	}
	
	public void navigateDelay(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String TypeofDelay = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DelayType");

		if (TypeofDelay.equalsIgnoreCase("Customer"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			Reusable.waitForSiebelLoader();
		}
		else
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.coltDelayTab,"Colt Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.coltDelayTab,"Colt Delays Tab");
			Reusable.waitForSiebelLoader();
		}
	}

	public void createDelay(String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String TypeofDelay = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DelayType");
		String DelayReason = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DelayReason");
		String DelayLocation = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DelayLocation");
		String Group = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Group");
		String ExpectedResolvDate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ExpectedResolvDate");
		String NextCustomerUpdate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"NextCustomerUpdate");
		String DairyNote = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DairyNote");
		String AddWorkitem = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"AddWorkitem");
		String WIAdded = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"WIAdded");
		String WISched = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"WISched");
		String ERDAlert = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ERDAlert");
		String SearchOwner = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SearchOwner");
		String Email = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Email");
		String DelayOwner = Configuration.SiebelUser_Username;
		String DelayStandByOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DelayStandByOption");
		
		if (TypeofDelay.equalsIgnoreCase("Customer"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.custDelayNewButton,"New Customer Delay");
			click(CustomerTimeJourneyObj.CustomerDelay.custDelayNewButton,"New Customer Delay");
			Reusable.waitForSiebelLoader();
			
			click(CustomerTimeJourneyObj.CustomerDelay.delayReason,"Delay Reason");
			sendKeys(CustomerTimeJourneyObj.CustomerDelay.delayReason,DelayReason);
			Reusable.waitForSiebelLoader();
			
			click(CustomerTimeJourneyObj.CustomerDelay.searchDelayOwner,"Delay Owner");
			click(CustomerTimeJourneyObj.CustomerDelay.addOwner,"OK button");
			Reusable.waitForSiebelLoader();
		}
		else
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.coltDelayTab,"Colt Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.coltDelayTab,"Colt Delays Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.coltDelayNewButton,"New Colt Delay");
			click(CustomerTimeJourneyObj.CustomerDelay.coltDelayNewButton,"New Colt Delay");
			Reusable.waitForSiebelLoader();
			
			boolean StandbyUnchecked = verifyExists(CustomerTimeJourneyObj.CustomerDelay.coltdelayStandbyFld);
			if(StandbyUnchecked)
			{
				Report.LogInfo("INFO", "Standby field is displayed with Unchecked", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Standby field is displayed with Unchecked");
			}
			else
			{
				Report.LogInfo("INFO", "Standby field is should be displayed with Unchecked", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Standby field is should be displayed with Unchecked");
			}
			
			click(CustomerTimeJourneyObj.CustomerDelay.coltdelayReason,"Delay Reason");
			sendKeys(CustomerTimeJourneyObj.CustomerDelay.coltdelayReason,DelayReason);
			SendkeyusingAction(Keys.ENTER);
			
			click(CustomerTimeJourneyObj.CustomerDelay.coltDelayOwner,"Delay Owner");			
			click("@xpath=//tr[@class='AppletButtons']//span[@class = 'siebui-icon-dropdown applet-form-combo applet-list-combo']","Delay Owner dropdown");
			ClickonElementByString("//li//div[text()='" + SearchOwner + "']", 20);
//			click(CustomerTimeJourneyObj.CustomerDelay.searchValue,"Search Value");
			sendKeys(CustomerTimeJourneyObj.CustomerDelay.searchValue,DelayOwner,"Search Value");
			click(CustomerTimeJourneyObj.CustomerDelay.searchButton,"Search");
			
			if(verifyExists(CustomerTimeJourneyObj.CustomerDelay.addOwner))
			{
			click(CustomerTimeJourneyObj.CustomerDelay.addOwner,"OK button");
			Reusable.waitForSiebelLoader();
			}
		}							
		
		boolean delayLocation = verifyExists(CustomerTimeJourneyObj.CustomerDelay.delayLocation);
		if(delayLocation)
		{
			click(CustomerTimeJourneyObj.CustomerDelay.delayLocation,"Delay Location");
			sendKeys(CustomerTimeJourneyObj.CustomerDelay.delayLocation,DelayLocation);						
		}		

		verifyExists(CustomerTimeJourneyObj.CustomerDelay.diaryNote,"First Diary Note");
		click(CustomerTimeJourneyObj.CustomerDelay.diaryNote,"First Diary Note");
		Reusable.waitForSiebelLoader();
		click(CustomerTimeJourneyObj.CustomerDelay.diaryNew,"New Note");
		click(CustomerTimeJourneyObj.CustomerDelay.noteArea,"Note");
		sendKeys(CustomerTimeJourneyObj.CustomerDelay.noteValue,DairyNote);
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.addNote,"Ok Diary Note");
		click(CustomerTimeJourneyObj.CustomerDelay.addNote,"Ok Diary Note");
		Reusable.waitForSiebelLoader();
		
		if(DelayStandByOption.equalsIgnoreCase("Yes"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.coltdelayStandbyFld,"StandBy Option");
			click(CustomerTimeJourneyObj.CustomerDelay.coltdelayStandbyFld,"StandBy Option");
		}
		
		if(ERDAlert.equalsIgnoreCase("Yes"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			Reusable.waitForSiebelLoader();
			
			boolean alert = verifyExists(CustomerTimeJourneyObj.CustomerDelay.ERDAlert);
			if(alert)
			{
				click(CustomerTimeJourneyObj.CustomerDelay.alertOK,"OK Button");
				
				Report.LogInfo("PASS", "ERD Date Required alert is displayed when Standby Option is not selected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "ERD Date Required alert is displayed when Standby Option is not selected");
				Report.logToDashboard("ERD Date Required alert is displayed when Standby Option is not selected");
			}
			else
			{
				Report.LogInfo("INFO", "ERD Date Required alert is not displayed when Standby Option is not selected", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "ERD Date Required alert is not displayed  when Standby Option is not selected");
				Report.logToDashboard("ERD Date Required alert is not displayed  when Standby Option is not selected");
			}			
			
			click(CustomerTimeJourneyObj.CustomerDelay.coltdelayStandbyFld,"StandBy Option");
			
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			Reusable.waitForAjax();
			
			boolean DelayOwneralert = verifyExists(CustomerTimeJourneyObj.CustomerDelay.DelayOwnerAlert);
			if(DelayOwneralert)
			{
				click(CustomerTimeJourneyObj.CustomerDelay.alertOK,"OK Button");
				click(CustomerTimeJourneyObj.CustomerDelay.coltDelayOwner,"Delay Owner");
				click(CustomerTimeJourneyObj.CustomerDelay.coltDelayOwner,"Search Owner");
				
				//clearTextBox(CustomerTimeJourneyObj.CustomerDelay.SearchOwnerusingMail);
				waitForElementToBeVisible(CustomerTimeJourneyObj.CustomerDelay.SearchOwnerusingMail,20);
				click(CustomerTimeJourneyObj.CustomerDelay.SearchOwnerusingMail,"Search by Field");
				sendKeys(CustomerTimeJourneyObj.CustomerDelay.SearchOwnerusingMail,"Email Addr","Search by Field");
				click(CustomerTimeJourneyObj.CustomerDelay.cancelOwner,"Cancel");
				
				click(CustomerTimeJourneyObj.CustomerDelay.searchValue,"Search Value");
				sendKeys(CustomerTimeJourneyObj.CustomerDelay.searchValue,Email,"Search Value");
				click(CustomerTimeJourneyObj.CustomerDelay.searchButton,"Search");
				click(CustomerTimeJourneyObj.CustomerDelay.addOwner,"OK button");
				Reusable.waitForSiebelLoader();
				
				verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
				click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			}
			
			alert = verifyExists(CustomerTimeJourneyObj.CustomerDelay.ERDAlert);
			if(alert)
			{
				click(CustomerTimeJourneyObj.CustomerDelay.alertOK,"OK Button");
				
				Report.LogInfo("PASS", "ERD Date Required alert is displayed when Standby Option is selected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "ERD Date Required alert is displayed when Standby Option is not selected");
				Report.logToDashboard("ERD Date Required alert is displayed when Standby Option is not selected");
			}
			else
			{
				Report.LogInfo("INFO", "ERD Date Required alert is not displayed when Standby Option is selected", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "ERD Date Required alert is not displayed  when Standby Option is not selected");
				Report.logToDashboard("ERD Date Required alert is not displayed  when Standby Option is not selected");
			}			
		}
		if (ExpectedResolvDate.equalsIgnoreCase("Yes"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.expResolvDate,"Expected Resolve Date Icon");
			Reusable.SelectTodaysDate(CustomerTimeJourneyObj.CustomerDelay.expResolvDate,"Expected Resolve Date");
		}
		else if (NextCustomerUpdate.equalsIgnoreCase("Yes"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.nextCustUpdate,"Next Customer Update Icon");
			Reusable.SelectTodaysDate(CustomerTimeJourneyObj.CustomerDelay.nextCustUpdate,"Next Customer Update");
		}
		
		if (AddWorkitem.equalsIgnoreCase("Yes"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.addWorkitem,"Add Workitems");
			click(CustomerTimeJourneyObj.CustomerDelay.addWorkitem,"Add Workitems");
			Reusable.waitForSiebelLoader();
			if (WIAdded.equalsIgnoreCase("Yes"))
			{
				click(CustomerTimeJourneyObj.CustomerDelay.wiAddedCheckbox,"Select WI Added");
			}
			else if (WISched.equalsIgnoreCase("Yes"))
			{
				click(CustomerTimeJourneyObj.CustomerDelay.wiSchldCheckbox,"Select WI Sched");
			}
//			verifyExists(CustomerTimeJourneyObj.CustomerDelay.okWorkitem,"Ok button");
			ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.okWorkitem);
			click(CustomerTimeJourneyObj.CustomerDelay.okWorkitem,"Ok button");
		}
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
		click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
		Reusable.waitForSiebelLoader();

//		if ((ExpectedResolvDate.equalsIgnoreCase("No"))||(NextCustomerUpdate.equalsIgnoreCase("No")))
//		{
//			Reusable.alertPopUp();
//			ExtentTestManager.getTest().log(LogStatus.PASS, "Alert Displayed when ERD or NCU not filled");
//		}
		
		if(isElementPresent(By.xpath("//button[text()='Ok']")))
		{
			Reusable.alertPopUp();
//			String PopupText = getTextFrom(CustomerTimeJourneyObj.CustomerDelay.popupText);
//			String PopupText = findWebElement(CustomerTimeJourneyObj.CustomerDelay.popupText).getText();
			
//			Report.LogInfo("INFO", "Text in Alert is " +PopupText,"PASS");
//			if (PopupText.contains("enter a valid"))
//			{
//				ExtentTestManager.getTest().log(LogStatus.PASS, "Alert Displayed when ERD or NCU not filled");
//			}
//			if (PopupText.contains("Customer Delay"))
//			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Alert Displayed when there is already delay with same reason");
//			}
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.cancelDelay,"Cancel Delay");
			click(CustomerTimeJourneyObj.CustomerDelay.cancelDelay,"Cancel Delay");
			Reusable.waitForSiebelLoader();
		}

//		click(CustomerTimeJourneyObj.CustomerDelay.cancelDelay,"Cancel Delay");
	}

	public void orderDiaryUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Note = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DairyNote");
		
		ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.diaryTable);
		Reusable.waitForSiebelLoader();
		
//		String DiaryNote=getTextFrom(CustomerTimeJourneyObj.CustomerDelay.diaryNoteValue);
		String DiaryNote=findWebElement(CustomerTimeJourneyObj.CustomerDelay.diaryNoteValue).getText();
//		WebElement Note1 = findWebElement(CustomerTimeJourneyObj.CustomerDelay.diaryNoteValue);
//		String DiaryNote = Note1.getText();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Customer Delay New Note is " +DiaryNote);
		
		try
		{
			if(isVisible(CustomerTimeJourneyObj.CustomerDelay.diaryTab))
			{
				javaScriptclick(CustomerTimeJourneyObj.CustomerDelay.diaryTab,"Click on Diary tab");
			}
			else
			{
				WebElement serviceOrderTabSelect= findWebElement(CustomerTimeJourneyObj.CustomerDelay.diaryTabSelectArrow);
				Select element = new Select(serviceOrderTabSelect);
				element.selectByVisibleText("Diary");
			}
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
		Reusable.waitForSiebelLoader();

		String OrderDiaryNote = getTextFrom(CustomerTimeJourneyObj.CustomerDelay.orderDiaryNote);
		
		if (OrderDiaryNote.contains(DiaryNote))
		{
			Report.LogInfo("INFO", "Note in Customer Delay matched Order Diary Note","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Note in Customer Delay matched Order Diary Note");

			verifyExists(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			Reusable.waitForSiebelLoader();
			
			int DelayRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Customer Delays']//tr[@tabindex='-1']")).size();
			System.out.println("Total Rows = " +DelayRows);
			
			int rowCount = getXPathCount(CustomerTimeJourneyObj.CustomerDelay.delayRowCount);
			System.out.println("Count of rows is" +rowCount);

			for (int i = 1; i <= rowCount; i++) 
			{
				String status = getTextFrom("@xpath=//table[@summary='Customer Delays']//td[contains(@id,'Status')][1]");
				System.out.println("Status is" +status);
				if (status.equalsIgnoreCase("Open"))
				{
					click("@xpath=//table[@summary='Customer Delays']//tr[@id='"+i+"']");
					break;
				}
				else 
				{
					Report.LogInfo("INFO", "Delay cannot be Edited","FAIL");
				}
			}
			
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit delay");
			click(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit delay");
			Reusable.waitForSiebelLoader();
			
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.diaryNote,"First Diary Note");
			click(CustomerTimeJourneyObj.CustomerDelay.diaryNote,"First Diary Note");
			Reusable.waitForSiebelLoader();
			click(CustomerTimeJourneyObj.CustomerDelay.diaryNew,"New Note");
			Reusable.waitForSiebelLoader();
			click(CustomerTimeJourneyObj.CustomerDelay.newNotearea,"Note");
			sendKeys(CustomerTimeJourneyObj.CustomerDelay.noteValue,Note);
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.addNote,"Ok Diary Note");
			click(CustomerTimeJourneyObj.CustomerDelay.addNote,"Ok Diary Note");
			Reusable.waitForSiebelLoader();

			Reusable.waitForAjax();

			verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
			Reusable.waitForSiebelLoader();
			
			try
			{
				if(isVisible(CustomerTimeJourneyObj.CustomerDelay.diaryTab))
				{
					javaScriptclick(CustomerTimeJourneyObj.CustomerDelay.diaryTab,"Click on Diary tab");
				}
				else
				{
					WebElement serviceOrderTabSelect= findWebElement(CustomerTimeJourneyObj.CustomerDelay.diaryTabSelectArrow);
					Select element = new Select(serviceOrderTabSelect);
					element.selectByVisibleText("Diary");
				}
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
			}
			Reusable.waitForSiebelLoader();

			String OrderDiaryNote2 = getTextFrom(CustomerTimeJourneyObj.CustomerDelay.orderDiaryNote);
			
			if (OrderDiaryNote2.contains(Note))
			{
				Report.LogInfo("INFO", "New Note in Customer Delay matched Order New Diary Note","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "New Note in Customer Delay matched Order New Diary Note");
			}
			else 
			{
				Report.LogInfo("INFO", "Customer Delay New Note didn't match Order New Diary Note","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Customer Delay New Note didn't match Order New Diary Note");
			}
		}
		else 
		{
			Report.LogInfo("INFO", "Customer Delay Note didn't match Order Diary Note","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Customer Delay Note didn't match Order Diary Note");
		}
	}	
	
	public void NavigateToServiceOrderCockpitTab() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab, 10);
		click(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab,"service Order Cockpit Tab");
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.AdvancedFilter, 90);	

	}
	
	public void cancelDelay(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Verification = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Verification");

//		List<WebElement> DelayRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Customer Delays']//tr[@tabindex='-1']"));
		int DelayRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Customer Delays']//tr[@tabindex='-1']")).size();
//		int Selected_Columns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//select[@aria-label='Selected Columns']/option")).size();
		System.out.println("Total Rows = " +DelayRows);
		
//		List<WebElement> status = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Customer Delays']//td[contains(@id,'Status')][1]"));

		int rowCount = getXPathCount(CustomerTimeJourneyObj.CustomerDelay.delayRowCount);
		System.out.println("Count of rows is" +rowCount);

		for (int i = 1; i <= rowCount; i++) 
		{
//			if (GetTextFrom(status.get(i)).equalsIgnoreCase("Open"))
			String status = getTextFrom("@xpath=//table[@summary='Customer Delays']//td[contains(@id,'Status')][1]");
			System.out.println("Status is" +status);
			if (status.equalsIgnoreCase("Open"))
			{
				click("@xpath=//table[@summary='Customer Delays']//tr[@id='"+i+"']");
				break;
			}
			else 
			{
				Report.LogInfo("INFO", "Delay cannot be Edited","FAIL");
			}
		}
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit delay");
		click(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit delay");
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.updateStatus,"Update Status");
		click(CustomerTimeJourneyObj.CustomerDelay.updateStatus,"Update Status");
		sendKeys(CustomerTimeJourneyObj.CustomerDelay.updateStatus,"Cancelled");
		SendkeyusingAction(Keys.ENTER);
//		Reusable.SendkeaboardKeys(CustomerTimeJourneyObj.CustomerDelay.updateStatus, Keys.TAB);	

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Reusable.waitForSiebelLoader();	

		Reusable.waitForAjax();
		Report.LogInfo("INFO", "Delay cancelled","INFO");


//		if(isVisible("@xpath=//span[text()='OK']"))
//		{
//			Report.LogInfo("INFO", "Pop up present", "INFO");
//			click(CustomerTimeJourneyObj.CustomerDelay.statusPopUp,"Click");
//			Report.LogInfo("INFO", "Status pop up is clicked successfully", "INFO");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Status pop up " +"is closed successfully");
//			Reusable.waitForSiebelLoader();
//		}
		
//		WEB_DRIVER_THREAD_LOCAL.get().switchTo().activeElement();
//		Reusable.alertPopUp();	
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
		click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
		Reusable.waitForSiebelLoader();
//		WEB_DRIVER_THREAD_LOCAL.get().switchTo().activeElement();
		Reusable.waitForAjax();
		Reusable.waitForAjax();

		Reusable.waitForSiebelLoader();
	}		

	
	public void cancelDelayStatus(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String MoveColumnName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "MoveColumnName");

		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Customer Delays"));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Customer Delays"), 15);
		
		List<WebElement> Columns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]").replace("CockpitSectionName", "Customer Delays")));
		HashMap<Integer, String> ColumnValue = new HashMap();
		int i, MovementcolumnIndex = 0,DestinationColIndex;
		for(i=0; i<Columns.size(); i++)
		{
			ColumnValue.put(i, Columns.get(i).getAttribute("innerText"));
			if(MoveColumnName.equalsIgnoreCase(ColumnValue.get(i)))
			{
				MovementcolumnIndex = i;
			}
		}
		
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Customer Delays"), "Customer Delays Settings");
		click(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayed.replace("CockpitSectionName", "Customer Delays"), "ColumnsDisplayed Menu");
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayedHeader, 15);				
		
		DestinationColIndex = MovementcolumnIndex-1;
		
		if(MovementcolumnIndex == 0)
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Save the Column Settings");
			Report.LogInfo("INFO", "Since it is the first column, Can't lift the column Up", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Since it is the first column, Can't lift the column Up");
		}
		else
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", MoveColumnName),"Select a column");

			click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnTop,"Click on MoveTop Button");
			click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
			Reusable.waitForAjax();
		}
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.calcStatus,"Calc Status");
//		String Status = "Cancelled";
		String CalcStatus = getAttributeFrom(CustomerTimeJourneyObj.CustomerDelay.calcStatus,"Calc Status");
//		String CalcStatus = Reusable.MultiLineWebTableCellAction("Calc Status", Status , null, "GetValue", null, 1);
		if (CalcStatus.equals("N/A"))
		{
			Report.LogInfo("INFO", "Calc Status is as expected", "PASS");
		}
		else
		{
			Report.LogInfo("INFO", "Calc Status is not as expected", "FAIL");
		}
	}
	
	public void validateAuditTrialColumns(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.customerAuditTrail);
		verifyCockpitColumns(expectedCustomerDelayAuditTrialColumns, CustomerTimeJourneyObj.CustomerDelay.auditTrailSetting,
				CustomerTimeJourneyObj.CustomerDelay.columnDisplayedTrail, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns, "Customer Delay Audit Trial");
		click(CustomerTimeJourneyObj.CustomerDelay.cancelAuditTrail, "Cancel button");	
	}
	
	public void cockpitCustDelayHyperlink(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String MoveColumnName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "MoveColumnName");

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader,"Delays Menu");

		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Delays"));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Delays"), 15);
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Delays"), "Delays Settings");
		click(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayed.replace("CockpitSectionName", "Delays"), "ColumnsDisplayed Menu");
		
		click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", MoveColumnName),"Select a column");

		click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnTop,"Click on MoveTop Button");
		click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
		Reusable.waitForAjax();

//		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayedHeader, 15);				

		click(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab,"service Order Cockpit Tab");
		Reusable.waitForSiebelLoader();
		WEB_DRIVER_THREAD_LOCAL.get().switchTo().activeElement();
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader,"Delays Menu");

		ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.ordernumberlink);
		click(CustomerTimeJourneyObj.CustomerDelay.cockpitDelayReasonLink, "Cockpit Delay Reason Link");
		Reusable.waitForAjax();
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.status,"Status Check");
		Reusable.waitForAjax();
	}	
	
	
	public void verifyReturnWIColumn(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String MoveColumnName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "MoveColumnName");

//		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Customer Delays"));
//		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Customer Delays"), 15);

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader,"Delays Menu");

		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Delays"), "Delays Settings");
		click(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayed.replace("CockpitSectionName", "Delays"), "ColumnsDisplayed Menu");
		
		click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", MoveColumnName),"Column");

		click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnTop,"MoveTop Button");
		click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save");
		Reusable.waitForAjax();

		click(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab,"Service Order Cockpit Tab");
		Reusable.waitForSiebelLoader();
		WEB_DRIVER_THREAD_LOCAL.get().switchTo().activeElement();
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader,"Delays Menu");

		verifyExists(CustomerTimeJourneyObj.CustomerDelay.returnWiSchedOn,"Return Sche WIs On");
		Report.LogInfo("INFO", "Expected column is present", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Return WI column present in Delay Applet");
	}
	
	public void verifyDelayResolvedFilter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader,"Delays Menu");
		selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayMenuDropdown,"Resolved > Review Date","Resolved filter");
		Reusable.waitForSiebelLoader();
		Report.LogInfo("INFO", "Expected filter is present", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Resolved > Review Date filter present in Delay Applet");
	}
	
	public void removeWorkitem(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String TypeofDelay = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DelayType");
		
		if (TypeofDelay.equalsIgnoreCase("Customer"))
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
			Reusable.waitForSiebelLoader();
		}
		else
		{
			verifyExists(CustomerTimeJourneyObj.CustomerDelay.coltDelayTab,"Colt Delays Tab");
			click(CustomerTimeJourneyObj.CustomerDelay.coltDelayTab,"Colt Delays Tab");
			Reusable.waitForSiebelLoader();
		}

		click("@xpath=//table[@summary='Customer Delays']//tr[@id='1']");
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit delay");
		click(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit delay");

//		verifyExists(CustomerTimeJourneyObj.CustomerDelay.removeWorkitem,"Remove Workitem");
		ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.removeWorkitem);
		click(CustomerTimeJourneyObj.CustomerDelay.removeWorkitem,"Remove Workitem");
		Reusable.waitForSiebelLoader();
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
		click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Delay");
		Reusable.waitForSiebelLoader();
		ExtentTestManager.getTest().log(LogStatus.INFO, "Workitem" +" is removed successfully");
		
	}
	
	public void createLONGCRD(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ReturnDate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ReturnDate");
		String ColumnCheck = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CheckColumn");
		
		verifyExists(CustomerTimeJourneyObj.LongCRD.workflowTab,"Workflows Tab");
		click(CustomerTimeJourneyObj.LongCRD.workflowTab,"Workflows Tab");
		Reusable.waitForSiebelLoader();
		
		switch (ColumnCheck) 
		{
			case "Return":
			{
				verifyExists(CustomerTimeJourneyObj.LongCRD.returnDate,"Return Date column");
				Report.LogInfo("INFO", "Expected column is present", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Return Date column present in Workflow tab");
			break;
			}
			
			case "Add":
			{
				click(CustomerTimeJourneyObj.LongCRD.deliveryLineitem,"Delivery Lineitem");
				
				if(isElementPresent(By.xpath("//button[@title='Workflow:LONGCRD']"))) 
				{
					click(CustomerTimeJourneyObj.LongCRD.longCRDButton,"LONGCRD button");
					Reusable.waitForSiebelLoader();
		
					Reusable.alertPopUp();
					
					String PresentCRD = getTextFrom(CustomerTimeJourneyObj.LongCRD.presentCRD);
					ExtentTestManager.getTest().log(LogStatus.INFO, "Present CRD date is " +PresentCRD);
					
					verifyExists(CustomerTimeJourneyObj.LongCRD.returnDatePick,"Return Date Calendar button");
					click(CustomerTimeJourneyObj.LongCRD.returnDatePick,"Return Date Calendar button");
		
					verifyExists(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value", ReturnDate),"Return Date");
					click(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value",ReturnDate),"Return Date");
					Reusable.waitForSiebelLoader();
					
//					Robot robot = new Robot();
//					robot.keyPress(KeyEvent.VK_ENTER);
//					robot.keyRelease(KeyEvent.VK_ENTER);
//					Reusable.waitForSiebelLoader();	

					verifyExists(CustomerTimeJourneyObj.LongCRD.dateSave,"Save button");
					click(CustomerTimeJourneyObj.LongCRD.dateSave,"Save button");
					Reusable.waitForSiebelLoader();
		
	//				String ReturnDate1 = Reusable.FutureDate(10);
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Long CRD is not possible");
				}
				break;
			}
			case "Validate":
			{
				click(CustomerTimeJourneyObj.LongCRD.deliveryLineitem,"Delivery Lineitem");

				isElementPresent(By.xpath("//button[@title='Workflow:LONGCRD']")); 
				ExtentTestManager.getTest().log(LogStatus.PASS, "LONGCRD button present");

				click(CustomerTimeJourneyObj.LongCRD.longCRDButton,"LONGCRD button");
				Reusable.waitForSiebelLoader();
		
				Reusable.alertPopUp();
				
				String PresentCRD = getTextFrom(CustomerTimeJourneyObj.LongCRD.presentCRD);
				ExtentTestManager.getTest().log(LogStatus.INFO, "Present CRD date is " +PresentCRD);
				
				verifyExists(CustomerTimeJourneyObj.LongCRD.returnDatePick,"Return Date Calendar button");
				click(CustomerTimeJourneyObj.LongCRD.returnDatePick,"Return Date Calendar button");
				
				verifyExists(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value", ReturnDate),"Return Date");
				click(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value",ReturnDate),"Return Date");
				Reusable.waitForSiebelLoader();

				verifyExists(CustomerTimeJourneyObj.LongCRD.dateSave,"Save button");
				click(CustomerTimeJourneyObj.LongCRD.dateSave,"Save button");
				Reusable.WaitForAjax();

				verifyExists(CustomerTimeJourneyObj.LongCRD.auditTrailTab,"Audit Trail Tab");
				click(CustomerTimeJourneyObj.LongCRD.auditTrailTab,"Audit Trail Tab");
				Reusable.waitForSiebelLoader();
				
				String AuditField=getTextFrom(CustomerTimeJourneyObj.LongCRD.auditTrailField);
				if (AuditField.contains("Return Date"))
				{
					Report.LogInfo("INFO", "Expected field " + AuditField +" is being Audited","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, AuditField + " displayed in Audit Trail");
				}
				else
				{
					Report.LogInfo("INFO", "Expected field " + AuditField + " is not being Audited","FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, AuditField + " not displayed in Audit Trail");
				}

			}
		}
	}
	
	public String getColtDelayStatus(String DelayType) throws IOException, InterruptedException
	{
		//int noofRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Colt Delays']//tr[@id]")).size();
		//String RowNo = Integer.toString(noofRows);
		String Status;
		
		ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.coltDelaySettings);
		Reusable.waitForElementToBeVisible(CustomerTimeJourneyObj.CustomerDelay.coltDelaySettings, 15);		
		click(CustomerTimeJourneyObj.CustomerDelay.coltDelaySettings, "Click Cockpit Settings");
		click(CustomerTimeJourneyObj.CustomerDelay.coltDelay_AdvancedSort, "Click Cockpit Settings");
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.AdvancedSortHeader, 15);
		
		click(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,"Click on SortByField1");
		sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,"Created Date","EnterValue in SortByField1");

		click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder1,"Click on AscendingOrder1");
		click(SiebelCockpitObj.ServiceOrderCockpit.OkButton,"Click on Ok Button");
		Reusable.waitForSiebelLoader();
		
		if(DelayType.equalsIgnoreCase("ColtDelay"))
		{
			Status = findWebElement(CustomerTimeJourneyObj.CustomerDelay.coltdelayStatus.replace("RowNo", "1")).getText();
		}
		else
		{
			Status = findWebElement(CustomerTimeJourneyObj.CustomerDelay.customerdelayStatus.replace("RowNo", "1")).getText();
		}
					
	
		Report.LogInfo("INFO", "Status of Colt Delay with today's ERD is :" + Status, "Colt Delay Status");
		
		return Status;
	}

	public void getWIOriginatingStatus() throws IOException, InterruptedException
	{
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.workItemsTab,"Work Items Tab");
		click(CustomerTimeJourneyObj.CustomerDelay.workItemsTab,"Work Items Tab");
		Reusable.waitForSiebelLoader();
		
		LocalDate todayDate=LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/M/yyyy");
		String DateTxt = todayDate.format(formatter);
		Report.LogInfo("INFO", "Date value : "+ DateTxt, "INFO");	
		
		List<WebElement> WIStatus = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//table[@summary='Work Items']//td[text()='Date']//parent::tr//td[contains(@id,'Originating_Work_Queue')]").replace("Date", DateTxt)));
		
		Report.LogInfo("INFO", "WI Status : "+ WIStatus, "INFO");
		for(WebElement Status:WIStatus)
		{
			String Orig_Queue = Status.getText();
			if(Orig_Queue.equalsIgnoreCase("QUOTE RECEIVED"))
			{
				Report.LogInfo("INFO", "WI returns to Original Queues on reaching ERD", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "WI returns to Original Queues on reaching ERD");
				Report.logToDashboard("WI returns to Original Queues on reaching ERD");
				break;
			}
			else
			{
				Report.LogInfo("INFO", "WI is not returned to Original Queues on reaching ERD", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "WI is not returned to Original Queues on reaching ERD");
				Report.logToDashboard("WI is not returned to Original Queues on reaching ERD");
			}			
		}				
		
	}
	
	public void RecommendedOptionVerification(String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String AEndReason = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "AEndReason");
		String BEndReason = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BEndReason");
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.workflowsTab,"Work Items Tab");
		click(CustomerTimeJourneyObj.CustomerDelay.workflowsTab,"Work Items Tab");
		Reusable.waitForSiebelLoader();
		
		WebElement WFType = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//table[@summary='Workflow']//td[contains(@id,'Workflow_Type') and text()='Delivery']"));
		WFType.click();
		
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.change_WorkflowBtn,"Change Workflow");
		click(CustomerTimeJourneyObj.CustomerDelay.change_WorkflowBtn,"Change Workflow");
		
		WEB_DRIVER_THREAD_LOCAL.get().switchTo().activeElement();
		Report.LogInfo("INFO", "Switched to alert", "PASS");
		
		if(verifyExists("@xpath=//span[@class='colt-infopanel-moredetails-msg']"))
		{
			click("@xpath=//button[text()='Yes']","Yes");			
		}
		waitForElementToBeVisible(CustomerTimeJourneyObj.CustomerDelay.category_selection_popup,10);
		
		click(CustomerTimeJourneyObj.CustomerDelay.AEnd_Reason,"AEnd Reason");
		
		List<WebElement> AEndReasons = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//span[contains(@class,'sifcat-Aend-reason')]/parent::div//following-sibling::ul/li/a"));
		
		for(WebElement Reason:AEndReasons)
		{
			if(Reason.getText().equalsIgnoreCase(AEndReason))
			{
				Reason.click();
				break;
			}
			else
			{
				Report.LogInfo("INFO", "Matching webelement not found", "INFO");
			}
		}
		
//		ScrollIntoViewByString("@xpath=//li/a[text()='" + AEndReason + "']");
//		ClickonElementByString("//li/a[text()='" + AEndReason + "']", 20);
		
		click(CustomerTimeJourneyObj.CustomerDelay.BEnd_Reason,"BEnd Reason");
		List<WebElement> BEndReasons = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//span[contains(@class,'sifcat-Bend-reason')]/parent::div//following-sibling::ul/li/a"));
	
		
		for(WebElement Reason:BEndReasons)
		{
			if(Reason.getText().equalsIgnoreCase(BEndReason))
			{
				Reason.click();
				break;
			}
			else
			{
				Report.LogInfo("INFO", "Matching webelement not found", "INFO");
			}
		}
//		ScrollIntoViewByString("@xpath=//li/a[text()='" + BEndReason + "']");
//		ClickonElementByString("//li/a[text()='" + BEndReason + "']", 20);
		
		click(CustomerTimeJourneyObj.CustomerDelay.ProceedBtn,"Proceed");
		waitForElementToBeVisible(CustomerTimeJourneyObj.CustomerDelay.changeOption_popup,10);
		
		if(AEndReason.equalsIgnoreCase("Voice - additional DDI to port")&& BEndReason.equalsIgnoreCase("Configuration"))
		{
			boolean RecommendedOption = verifyExists("@xpath=//input[@value='In-Flight WFs' and @checked]");
			if(RecommendedOption)
			{
				Report.LogInfo("INFO", "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Hold & Raise Inflight Workflow", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Recommended Option for provided " + AEndReason + "and" + BEndReason +" is Hold & Raise Inflight Workflow");
				Report.logToDashboard("Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Hold & Raise Inflight Workflow");
			}
			else
			{
				Report.LogInfo("INFO", "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is not selected properly", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is not selected properly");
				Report.logToDashboard("Recommended Option for provided " + AEndReason + " and " + BEndReason +" is not selected properly");
			}
		}
		if(AEndReason.equalsIgnoreCase("Voice - additional DDI to port")&& BEndReason.equalsIgnoreCase("Access Type"))
		{
			boolean RecommendedOption = verifyExists("@xpath=//input[@value='Kill & re-trigger' and @checked]");
			if(RecommendedOption)
			{
				Report.LogInfo("INFO", "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Kill & Retrigger Workflow", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Kill & Retrigger Workflow");
				Report.logToDashboard("Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Kill & Retrigger Workflow");
			}
			else
			{
				Report.LogInfo("INFO", "Recommended Option for provided " + AEndReason + "and" + BEndReason +" is not selected properly", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Recommended Option for provided " + AEndReason + "and" + BEndReason +" is not selected properly");
				Report.logToDashboard("Recommended Option for provided " + AEndReason + "and" + BEndReason +" is not selected properly");
			}
		}
		if(AEndReason.equalsIgnoreCase("Bandwidth")&& BEndReason.equalsIgnoreCase("Voice - additional DDI to port"))
		{
			boolean RecommendedOption = verifyExists("@xpath=//input[@value='In-Flight WFs' and @checked]");
			if(RecommendedOption)
			{
				Report.LogInfo("INFO", "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Hold & Raise Inflight Workflow", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Hold & Raise Inflight Workflow");
				Report.logToDashboard("Recommended Option for provided " + AEndReason + " and " + BEndReason +" is Hold & Raise Inflight Workflow");
			}
			else
			{
				Report.LogInfo("INFO", "Recommended Option for provided" + AEndReason + " and " + BEndReason +" is not selected properly", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Recommended Option for provided " + AEndReason + " and " + BEndReason +" is not selected properly");
				Report.logToDashboard("Recommended Option for provided  " + AEndReason + "  and" + BEndReason +" is not selected properly");
			}
		}
		
		click("@xpath=//button[text()='Cancel']","Cancel");
	}
	
	public void verify_Restriction(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
		String ReturnDate = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ReturnDate");
		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderNumber");
		String CRD_Date = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CRD_Date");
		
		MoveToElement(WelcomeMailObj.WelcomeMail.options);
		verifyExists(WelcomeMailObj.WelcomeMail.options,"Options");
		click(WelcomeMailObj.WelcomeMail.options, "Options");
		
		Reusable.waitForSiebelLoader();
		
		mouseOverAction(WelcomeMailObj.WelcomeMail.serviceOrder);
		verifyExists(WelcomeMailObj.WelcomeMail.serviceOrder,"ServiceOrder");
		selectByVisibleText(WelcomeMailObj.WelcomeMail.serviceOrder, "Service Order", "Service Order");
		
		Reusable.waitForSiebelLoader();
		try 
		{
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		} 
		catch (Exception e) 
		{
			try 
			{
			javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderNumber,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		Reusable.waitForSiebelLoader();
		
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();
				
		verifyExists(CustomerTimeJourneyObj.LongCRD.workflowTab,"Workflows Tab");
		click(CustomerTimeJourneyObj.LongCRD.workflowTab,"Workflows Tab");
		Reusable.waitForSiebelLoader();
		
		click(CustomerTimeJourneyObj.LongCRD.deliveryLineitem,"Delivery Lineitem");
		
		if(isElementPresent(By.xpath("//button[@title='Workflow:LONGCRD']"))) 
		{
			click(CustomerTimeJourneyObj.LongCRD.longCRDButton,"LONGCRD button");
			Reusable.waitForSiebelLoader();

			Reusable.alertPopUp();
			
			String PresentCRD = getTextFrom(CustomerTimeJourneyObj.LongCRD.presentCRD);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Present CRD date is " +PresentCRD);
			
			verifyExists(CustomerTimeJourneyObj.LongCRD.returnDatePick,"Return Date Calendar button");
			click(CustomerTimeJourneyObj.LongCRD.returnDatePick,"Return Date Calendar button");

			verifyExists(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value", ReturnDate),"Return Date");
			click(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value",ReturnDate),"Return Date");
			Reusable.waitForSiebelLoader();
			
			verifyExists(CustomerTimeJourneyObj.LongCRD.dateSave,"Save button");
			click(CustomerTimeJourneyObj.LongCRD.dateSave,"Save button");
			Reusable.waitForSiebelLoader();
		
			MoveToElement(CustomerTimeJourneyObj.LongCRD.workFlowStatus);
			verifyExists(CustomerTimeJourneyObj.LongCRD.workFlowStatus,"Work Flow Status");
			WebElement Status = findWebElement(CustomerTimeJourneyObj.LongCRD.workFlowStatus);
			String Status_Text = Status.getText();
			if(Status_Text.equals("Long CRD"))
			{
				Report.LogInfo("Status", "Work flow status is Long CRD", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO,"Work flow status is Long CRD");
				Report.logToDashboard("Work flow status is Long CRD");
			}
			
			Reusable.waitForSiebelLoader();
			verifyExists(CustomerTimeJourneyObj.LongCRD.orderDate,"Order Date");
			click(CustomerTimeJourneyObj.LongCRD.orderDate,"Order Date");
			Reusable.waitForSiebelLoader();
			
			verifyExists(CustomerTimeJourneyObj.LongCRD.crdDate,"CRD Date");
			click(CustomerTimeJourneyObj.LongCRD.crdDate,"CRD Date");

			verifyExists(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value", CRD_Date),"CRD Date");
			click(CustomerTimeJourneyObj.LongCRD.dateSelection.replace("Value",CRD_Date),"CRD Date");
			Reusable.waitForSiebelLoader();
			
			
			verifyExists(CustomerTimeJourneyObj.LongCRD.newCRD,"NEW CRD");
			WebElement NEWCRD = findWebElement(CustomerTimeJourneyObj.LongCRD.newCRD);
			String NEWCRD_Date = NEWCRD.getText();
			Report.LogInfo("NEWCRD", "NEW CRD date is : "+NEWCRD_Date, "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,"NEW CRD date is : "+NEWCRD_Date);
			Report.logToDashboard("NEW CRD date is : "+NEWCRD_Date);
			
						
			verifyExists(CustomerTimeJourneyObj.LongCRD.crdSave,"Save Button");
			click(CustomerTimeJourneyObj.LongCRD.crdSave,"Save Button");
			
			String AlertMessage = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().alert().getText();
			if(AlertMessage.contains("be aware this order is in LONGCRD")){
				Report.LogInfo("Result", "Alert Message was dispalyed Successfully"+AlertMessage, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,"Alert Message was dispalyed Successfully"+AlertMessage);
				Report.logToDashboard("Alert Message was dispalyed Successfully"+AlertMessage);
			}
			else{
				Report.LogInfo("Result", "Alert Message was not dispalyed Successfully"+AlertMessage, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,"Alert Message was not dispalyed Successfully"+AlertMessage);
				Report.logToDashboard("Alert Message was not dispalyed Successfully"+AlertMessage);
			}
			Reusable.alertPopUp();

		}
	
	}

	public void ValidateDetails_CPDRescheduleWindow(String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		LocalDate todayDate=LocalDate.now();
		String formattedDate = todayDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		int TimeleftToCPD = 0, DelayDuration = 0, Timeleft = 0;
		String cpd = null,icd = null;
		
		List<String> Date = new ArrayList();
		addCPD(sheetName, scriptNo, dataSetNo);
		addUpdateICD(sheetName, scriptNo, dataSetNo);
		Date = getCPDICDValues(sheetName,scriptNo,dataSetNo);		
		ResolveCustomerDelay(sheetName,scriptNo,dataSetNo);				
		waitforPagetobeenable();
		
		if(Date.get(0).length()<=9)
		{
			cpd = Date.get(0).substring(0, 3)+"0"+Date.get(0).substring(3,9);
		}
		if(Date.get(1).length()<=9)
		{
			icd = Date.get(1).substring(0, 3)+"0"+Date.get(1).substring(3,9);
		}
		Report.LogInfo("CPD", cpd, "PASS");
		Report.LogInfo("ICD", icd, "PASS");
		
		LocalDate cpdDate=LocalDate.parse(cpd, DateTimeFormatter.ofPattern("dd/MM/yyyy"));		
		LocalDate icdDate=LocalDate.parse(icd, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		while(todayDate.isBefore(cpdDate))
		{
			
			if(!(todayDate.getDayOfWeek()== DayOfWeek.SATURDAY || todayDate.getDayOfWeek()== DayOfWeek.SUNDAY)){
				TimeleftToCPD++;
			}
			todayDate= todayDate.plusDays(1);
		}
		while(todayDate.isAfter(cpdDate))
		{	
			if(!(todayDate.getDayOfWeek()== DayOfWeek.SATURDAY || todayDate.getDayOfWeek()== DayOfWeek.SUNDAY)){
				TimeleftToCPD--;
			}
			todayDate= todayDate.minusDays(1);
		}
		
		while(todayDate.isBefore(todayDate))
		{
			
			if(!(todayDate.getDayOfWeek()== DayOfWeek.SATURDAY || todayDate.getDayOfWeek()== DayOfWeek.SUNDAY)){
				DelayDuration++;
			}
			todayDate= todayDate.plusDays(1);
		}
		while(todayDate.isAfter(todayDate))
		{	
			if(!(todayDate.getDayOfWeek()== DayOfWeek.SATURDAY || todayDate.getDayOfWeek()== DayOfWeek.SUNDAY)){
				DelayDuration--;
			}
			todayDate= todayDate.minusDays(1);
		}
		if(TimeleftToCPD<0)
		{
			Timeleft = TimeleftToCPD+1;
		}
		else
		{
			Timeleft = TimeleftToCPD-1;
		}		
		
		String ExistingCPD = "Existing CPD: "+cpd+" updated on: "+formattedDate;
		String ExistingICD = "Existing ICD: "+icd+" updated on: "+formattedDate;
		String TimeLeft = "Time left to CPD when delay raised: "+Timeleft+" bd";
		String Delayduration = "Delay duration: "+DelayDuration+" bd";
		
		Report.LogInfo("CPDReschedule Info: ", ExistingCPD, "PASS");
		Report.LogInfo("CPDReschedule Info : ", ExistingICD, "PASS");
		Report.LogInfo("CPDReschedule Info : ", TimeLeft, "PASS");
		Report.LogInfo("CPDReschedule Info : ", Delayduration, "PASS");

		//Getting values from application
		OpenServiceOrderNumber(sheetName, scriptNo, dataSetNo);
		click(SiebelCockpitObj.ServiceOrderCockpit.orderDatesTab, "Order Dates tab");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		scrollDown("@xpath=//input[@aria-label='E2E Test Date']");
		if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdDate))
		{
			javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.cpdDate, "CPD Date selector");
		}
		else if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdDateReschedule))
		{
			javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.cpdDateReschedule, "CPD Date selector");
		}		
		else if(verifyExists("@xpath=//input[@aria-label='Colt Promise Date']//parent::div//span[@aria-label='Date Field']"))
		{
			javaScriptclick("@xpath=//input[@aria-label='Colt Promise Date']//parent::div//span[@aria-label='Date Field']", "CPD Date selector");
		}
		
		waitForElementToAppear(CustomerTimeJourneyObj.CustomerDelay.DateDialog,10);
		List<String> cpdRescheduleWindowInfo = new ArrayList();
		List<WebElement> DateDialoginfoWE = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[contains(@class,'header-info')]/span"));
		for(WebElement Data:DateDialoginfoWE)
		{
			cpdRescheduleWindowInfo.add(Data.getText());
			Report.LogInfo("CPD Reschedule Window Detail: ", Data.getText(), "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "CPD Reschedule Window Detail: "+ Data.getText());
		}				
		
		if(cpdRescheduleWindowInfo.get(0).equalsIgnoreCase(ExistingCPD)&&cpdRescheduleWindowInfo.get(1).equalsIgnoreCase(ExistingICD)&&cpdRescheduleWindowInfo.get(2).equalsIgnoreCase(TimeLeft)&&cpdRescheduleWindowInfo.get(3).contains(Delayduration)
			&&verifyExists(CustomerTimeJourneyObj.CustomerDelay.keepExistingDate)&&verifyExists(CustomerTimeJourneyObj.CustomerDelay.cancel)&&verifyExists(CustomerTimeJourneyObj.CustomerDelay.takethisdate))
		{
			Report.LogInfo("CPDRescheduleWindow","CPD Reschedule Window is displayed with proper Data after CustomerDelay is Resolved", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "CPD Reschedule Window is displayed with proper Data after CustomerDelay is Resolved");		
		}
		WEB_DRIVER_THREAD_LOCAL.get().switchTo().activeElement();
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.cancel,"Cancel");
		click(CustomerTimeJourneyObj.CustomerDelay.cancel,"Cancel");
		String warningMsg = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//span[@class='colt-infopanel-moredetails-msg']")).getText();
		if(warningMsg.equalsIgnoreCase("ICD/CPD must be reviewed and updated or kept immediately after Cust Delay resolved. Are you sure you want to cancel?"))
		{
			Report.LogInfo("CPDRescheduleWarningMsg: ",warningMsg, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "CPDRescheduleWarningMsg: "+warningMsg);
		}
		if(isVisible(By.xpath("//button[text()='Yes']"))) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
			click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
		}
	}
	
	public List CPDValue(String sheetName, String scriptNo, String dataSetNo) throws Exception
	{	
		List<String> values = new ArrayList();
		values = getCPDICDValues(sheetName,scriptNo,dataSetNo);
					
		if(values.get(0).equalsIgnoreCase(""))
		{
			addCPD(sheetName,scriptNo,dataSetNo);
		}
		else
		{
			Report.LogInfo("CPD: ", values.get(0), "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "CPD: "+ values.get(0));
		}
		values = getCPDICDValues(sheetName,scriptNo,dataSetNo);
		return values;
	}
	
	public void ResolveCustomerDelay(String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		int noofRows = 0;
		verifyExists(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
		click(CustomerTimeJourneyObj.CustomerDelay.customerDelayTab,"Customer Delays Tab");
		Reusable.waitForSiebelLoader();
		if(verifyExists("@xpath=//table[@summary='Customer Delays']//tr[@id]")){
			noofRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Customer Delays']//tr[@id]")).size();
		}
		else
		{
			createDelay(sheetName,scriptNo,dataSetNo);
		}
		if(noofRows>=1)
		{
			String status = getColtDelayStatus("CustomerDelay");
			
			if(status.equalsIgnoreCase("Cancelled")||status.equalsIgnoreCase("Resolved"))
			{
				createDelay(sheetName,scriptNo,dataSetNo);
			}
		}
		String Delay = "@xpath=//table[@summary='Customer Delays']//tr/td[text()='Open']";
		waitForElementToAppear(Delay,20);
		click(Delay,"Select the Open Delay");
		click(CustomerTimeJourneyObj.CustomerDelay.editDelay,"Edit Button");
		
		waitForElementToAppear(CustomerTimeJourneyObj.CustomerDelay.editDelayStatus,20);
		
		click(CustomerTimeJourneyObj.CustomerDelay.editDelayStatus,"Dropdown");
		ClickonElementByString("//div[text()='Resolved']",20);			
		
		ScrollIntoViewByString(CustomerTimeJourneyObj.CustomerDelay.saveDelay);
		click(CustomerTimeJourneyObj.CustomerDelay.saveDelay,"Save Customer Delay");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();					
	}
	
	public void addCPD(String sheetName,String scriptNo, String dataSetNo) throws Exception
	{
		OpenServiceOrderNumber(sheetName, scriptNo, dataSetNo);
		click(SiebelCockpitObj.ServiceOrderCockpit.orderDatesTab,"Order Dates");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		
		scrollDown("@xpath=//input[@aria-label='E2E Test Date']");
		if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdDate))
		{
			javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.cpdDate, "CPD Date selector");
		}
		else if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdDateReschedule))
		{
			javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.cpdDateReschedule, "CPD Date selector");
		}
		//Reusable.SelectTodaysDate(SiebelCockpitObj.ServiceOrderCockpit.cpdDate,"Customer Promised Date");
		else if(verifyExists("@xpath=//input[@aria-label='Colt Promise Date']//parent::div//span[@aria-label='Date Field']"))
		{
			javaScriptclick("@xpath=//input[@aria-label='Colt Promise Date']//parent::div//span[@aria-label='Date Field']", "CPD Date selector");
		}
		if(verifyExists("@xpath=//button[text()='Ok']"))
		{
			click("@xpath=//button[text()='Ok']","Close Alert");
		}
		
		List<WebElement> cpdDays= findWebElements(SiebelCockpitObj.ServiceOrderCockpit.cpdDaysFromCalendar);
		int cpdDaysCount= cpdDays.size();
		int cpdNewDate=0;
		for(int i=1; i<=cpdDaysCount;i++){


		if(cpdDays.get(i).getAttribute("class").contains("active")){
		String cpdActiveDayText= cpdDays.get(i).getText();
		int cpdActiveDayTextValue= Integer.parseInt(cpdActiveDayText);
		if(cpdActiveDayTextValue>1 && cpdActiveDayTextValue<31){

		cpdNewDate=cpdActiveDayTextValue-1;

		}
		else
		{
			cpdNewDate=cpdActiveDayTextValue+1;
		}

		break;
		}
		}


		click(SiebelCockpitObj.ServiceOrderCockpit.cpdDaySelectFromCalendar.replace("value",new Integer(cpdNewDate).toString()), "CPD Date");
		waitForAjax();
		
		boolean isPopupAvailable=false;
		try{
		WebElement cpdReschedulePopup= findWebElement(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedulePopup);
		isPopupAvailable= cpdReschedulePopup.isDisplayed();
		}
		catch(Exception e){
		Report.LogInfo("Verify exists", "Reschedule CPD Popup is not displayed", "INFO");
		}

		if(isPopupAvailable){
			click(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_ChangeDrivenBy,"Change Driver By");
			ClickonElementByString("//li//div[text()='COLT']",10);
			
			click(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_RescheduleReason,"Reason");
			ClickonElementByString("//li//div[text()='Human Error']",10);
			
//		selectDropdownValueByExpandingArrow_commonMethod("Change Driven By", SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_ChangeDrivenBy, "COLT");
//		selectDropdownValueByExpandingArrow_commonMethod("Reschedule Reason", SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_RescheduleReason, "Human Error");
//		click(SiebelCockpitObj.ServiceOrderCockpit.cpdRescheduleSave, "Save button");
//		Reusable.waitForSiebelLoader();
		}
		else{
		Report.LogInfo("Verify exists", "Reschedule CPD Popup is not displayed", "INFO");
		}
		Actions action=new Actions(webDriver);
		action.keyDown(Keys.CONTROL).sendKeys("S").keyUp(Keys.CONTROL).build().perform();
		Reusable.waitForSiebelLoader();
	}
	
	public void addUpdateICD(String sheetName,String scriptNo, String dataSetNo) throws Exception
	{
		OpenServiceOrderNumber(sheetName, scriptNo, dataSetNo);
		click(SiebelCockpitObj.ServiceOrderCockpit.orderDatesTab,"Order Dates");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		
		scrollDown("@xpath=//input[@aria-label='E2E Test Date']");
				
		
		if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdDateSelector))
		{
			javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.icdDateSelector, "ICD Date selector");
		}
		else if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdDateSelector1))
		{
			javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.icdDateSelector1, "ICD Date selector");
		}
		
		if(verifyExists("@xpath=//span[text()='Set ICD manually']"))
		{
			click("@xpath=//input[@id='non-strd-ld-time']/following-sibling::span","Date Selector");
			
		}
		
		if(verifyExists("@xpath=//button[text()='Ok']"))
		{
			click("@xpath=//button[text()='Ok']","Close Alert");
		}
		
		List<WebElement> cpdDays= findWebElements(SiebelCockpitObj.ServiceOrderCockpit.cpdDaysFromCalendar);
		int cpdDaysCount= cpdDays.size();
		int cpdNewDate=0;
		for(int i=1; i<=cpdDaysCount;i++){


		if(cpdDays.get(i).getAttribute("class").contains("active")){
		String cpdActiveDayText= cpdDays.get(i).getText();
		int cpdActiveDayTextValue= Integer.parseInt(cpdActiveDayText);
		if(cpdActiveDayTextValue>1 && cpdActiveDayTextValue<27){

		cpdNewDate=cpdActiveDayTextValue-1;

		}
		else
		{
			cpdNewDate=cpdActiveDayTextValue+1;
		}


		break;
		}
		}


		click(SiebelCockpitObj.ServiceOrderCockpit.cpdDaySelectFromCalendar.replace("value",new Integer(cpdNewDate).toString()), "ICD Date");
		waitForAjax();
		Reusable.waitForSiebelLoader();
		
		boolean isPopupAvailable=false;
		try{
		WebElement cpdReschedulePopup= findWebElement(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedulePopup);
		isPopupAvailable= cpdReschedulePopup.isDisplayed();
		}
		catch(Exception e){
		Report.LogInfo("Verify exists", "Reschedule CPD Popup is not displayed", "INFO");
		}

		if(isPopupAvailable){
			click(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_ChangeDrivenBy,"Change Driver By");
			ClickonElementByString("//li//div[text()='COLT']",10);
			
			click(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_RescheduleReason,"Reason");
			ClickonElementByString("//li//div[text()='Human Error']",10);
			
//		selectDropdownValueByExpandingArrow_commonMethod("Change Driven By", SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_ChangeDrivenBy, "COLT");
//		selectDropdownValueByExpandingArrow_commonMethod("Reschedule Reason", SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_RescheduleReason, "Human Error");
//		click(SiebelCockpitObj.ServiceOrderCockpit.cpdRescheduleSave, "Save button");
//		Reusable.waitForSiebelLoader();
		}
		else{
		Report.LogInfo("Verify exists", "Reschedule CPD Popup is not displayed", "INFO");
		}
		
		if(verifyExists("@xpath=//span[text()='Set ICD manually']"))
		{
			click("@xpath=//button[text()='Accept']","Accept Button");
			Reusable.waitForSiebelLoader();
		}
		
		Actions action=new Actions(webDriver);
		action.keyDown(Keys.CONTROL).sendKeys("S").keyUp(Keys.CONTROL).build().perform();
		Reusable.waitForSiebelLoader();
	}
	
	public List getCPDICDValues(String sheetName,String scriptNo, String dataSetNo) throws Exception
	{
		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");			
		
		OpenServiceOrderNumber(sheetName, scriptNo, dataSetNo);
		String cpdText=getAttributeFrom(CustomerTimeJourneyObj.CustomerDelay.cpdText,"title");
		String icdText=getAttributeFrom(CustomerTimeJourneyObj.CustomerDelay.icdText,"title");
		
		List<String> DateValues = new ArrayList();
		DateValues.add(cpdText);
		DateValues.add(icdText);
		
		return DateValues;
	}
	
	public void auditTrail_CPDICDChange(String UpdatedFieldName) throws InterruptedException, IOException

	{
		if(isVisible(CustomerTimeJourneyObj.LongCRD.auditTrailTab)){
			click(CustomerTimeJourneyObj.LongCRD.auditTrailTab,"Audit Trail Tab");
			}
			else{
				WebElement auditTrailTab= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTabSelectArrow);
				Select element = new Select(auditTrailTab);
				element.selectByVisibleText("Audit Trail");
			}
		
		Report.LogInfo("Click", "Audit Trail Tab is Selected", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Audit Trail Tab is Selected");		
		
		List<WebElement> AuditField=SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Service Order']//tr[@id]//td[contains(@id,'Field')]"));
		List<String> AuditFieldValues = new ArrayList();
		
		for(WebElement value:AuditField)
		{
			AuditFieldValues.add(value.getText());
		}
		if(UpdatedFieldName.equalsIgnoreCase("CPD"))
		{
			if (AuditFieldValues.contains("Colt Promised Date")||AuditFieldValues.contains("COLT CPD Reschedule Reason"))
			{
				Report.LogInfo("INFO", "Expected field Calc Date, Date Type and Reason for Changing fields are being Audited","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Calc Date, Date Type and Reason for Changing fields are displayed in Audit Trail");
			}
			else
			{
				Report.LogInfo("INFO", "Expected field " + AuditField + " is not being Audited","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, AuditField + " not displayed in Audit Trail");
			}
		}
		else if(UpdatedFieldName.equalsIgnoreCase("ICD"))
		{
			if (AuditFieldValues.contains("COLT Indicative Completion Date")||AuditFieldValues.contains("COLT ICD Reschedule Reason"))
			{
				Report.LogInfo("INFO", "Expected field Calc Date, Date Type and Reason for Changing fields are being Audited","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Calc Date, Date Type and Reason for Changing fields are displayed in Audit Trail");
			}
			else
			{
				Report.LogInfo("INFO", "Expected field " + AuditField + " is not being Audited","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, AuditField + " not displayed in Audit Trail");
			}
		}
		
	}

}
