package testHarness.siebelGalileoFunctions;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.ibm.icu.text.SimpleDateFormat;
import com.relevantcodes.extentreports.LogStatus;

import pageObjects.siebelGalileoObjects.SiebelCockpitObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

public class SiebelCockpit extends SeleniumUtils 
{

	public static final String expectedCPDICDDueShortlyColumns= "Service Order Ref,Customer Service Contact,Legal Customer Name,MRR (Calc),MRC Value,Order Type/Sub Type,Order Status,Overall Connection Type (calc.),Customer Delay,COLT Delay,Followup Date,Followup Notes,Delivery Start Date,CRD,CPD,ICD,EID,Product Name,Overall CDD (calc),Overall RFS (calc),MRR Country,Resilient";
	public static final String expectedCPDICDNotSetColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, MRR Calc, MRC value, Order Type/Sub Type, Order Status, Overall Connection Type (calc.), Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Product Name, Billing Start Date, Billing End Date, MRR Country, Resilient, Project Id, Priority, Contract Country, OLO Leakage (Overall)";
	public static final String expectedDelaysColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, Colt/Customer Delay, Delay Start, Delay End, Expected Resolve Date, Next Customer Update, Task Delay Reason, Originator, MRR Calc, MRC value, Order Type/Sub Type, Order Status, Cockpit Connection Type, Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Delay Owner, Delay Owner Group, Product Name, MRR Country, Return Sched WIs On, Contract Country, Resilient, OLO Leakage (Overall)";
	public static final String expectedActivitiesColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, Activtiy Type, Activity Created Date, Description, Comments, MRR (Calc), MRC Value, Order Type/Sub Type, Order Status, Cockpit Connection Type, Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Product Name, MRR Country, Contract Country, Resilient";
	public static final String expectedPriorityOrdersColumns= "Service Order Ref, Customer Service Contact, Legal Customer Name, MRR Calc, MRC value, Long CRD, Return Date, Fast track Fee, Order Type/Subtype, Order Status, Overall Connection Type (calc.), Customer Delay, COLT Delay, Followup Date, Followup Notes, Delivery Start Date, CRD, CPD, ICD, EID, Overall CDD (calc), Overall RFS (calc), Product Name, Priority, Escalation Owner, MRR Country, Resilient, OLO Leakage (Overall)";
	public static final String expectedServiceOrderColumns= "New, Service Order Ref, Fast Track flag, Customer Service Contact, Legal Customer Name, OCN, MRR Calc, MRC value, Order Type/Subtype, Order Status, Overall Connection Type (calc.), Customer Delay, COLT Delay, Delivery Start Date, CRD, CPD, Followup Date, Followup Notes, ICD, EID, Overall CDD (calc), Overall RFS (calc), Billing Start Date, Billing End Date, Product Name, Contract Country, Primary A Country, Primary A City, Primary A Address, Primary B Country, Primary B City, Primary B Address, Secondary A Country, Secondary A City, Secondary A Address, Secondary B Country, Secondary B City, Secondary B Address, Project ID, Project Manager, Priority, Escalation Owner, Site A Order Owner, Site B Order Owner, Related Order Numbers, MRR Country, Workflow Retriggered, Favourite, Favourite Flagged By, Bespoke Reference, Network Reference, Resilient, Diversity, Diverse From Service Reference, Customer Defined Route, OLO Leakage (Overall)";
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void NavigateToServiceOrderCockpitTab() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab, 10);
		click(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab,"Service Order Cockpit Tab");
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.AdvancedFilter, 500);	
	}
	
	public void Sorting(String CockpitSectionName,int SortableColumns, String SortOrder, String SortByField1, String SortByField2, String SortByField3) throws IOException, InterruptedException
	{
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);		
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
		click(SiebelCockpitObj.ServiceOrderCockpit.AdvancedSort.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.AdvancedSortHeader, 15);
		
		click(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,"Click on SortByField1");
		sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,SortByField1,"EnterValue in SortByField1");
		if(SortOrder.equalsIgnoreCase("Ascending"))
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.AscendingOrder1,"Click on AscendingOrder1");
		}
		else
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder1,"Click on DescendingOrder1");
		}
		if(SortableColumns==2)
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,"Click on SortByField1");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,SortByField1,"EnterValue in SortByField1");
			click(SiebelCockpitObj.ServiceOrderCockpit.SortByField2,"Click on SortByField2");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField2,SortByField2,"EnterValue in SortByField2");
			if(SortOrder.equalsIgnoreCase("Ascending"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.AscendingOrder1,"Click on AscendingOrder1");
				click(SiebelCockpitObj.ServiceOrderCockpit.AscendingOrder2,"Click on AscendingOrder2");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder1,"Click on DescendingOrder1");
				click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder2,"Click on DescendingOrder2");
			}
		}
		if(SortableColumns==3)
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,"Click on SortByField1");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField1,SortByField1,"EnterValue in SortByField1");
			click(SiebelCockpitObj.ServiceOrderCockpit.SortByField2,"Click on SortByField2");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField2,SortByField2,"EnterValue in SortByField2");
			click(SiebelCockpitObj.ServiceOrderCockpit.SortByField3,"Click on SortByField3");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.SortByField3,SortByField3,"EnterValue in SortByField3");
			if(SortOrder.equalsIgnoreCase("Ascending"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.AscendingOrder1,"Click on AscendingOrder1");
				click(SiebelCockpitObj.ServiceOrderCockpit.AscendingOrder2,"Click on AscendingOrder2");
				click(SiebelCockpitObj.ServiceOrderCockpit.AscendingOrder3,"Click on AscendingOrder3");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder1,"Click on DescendingOrder1");
				click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder2,"Click on DescendingOrder2");
				click(SiebelCockpitObj.ServiceOrderCockpit.DescendingOrder3,"Click on DescendingOrder3");
			}
		}
		click(SiebelCockpitObj.ServiceOrderCockpit.OkButton,"Click on Ok Button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings, 15);
		if(SortOrder.equalsIgnoreCase("Ascending"))
		{
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.AscendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField1),"Column is sorted Ascending");
			if(SortableColumns==2)
			{
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.AscendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField2),"Column is sorted Ascending");
			}
			else if(SortableColumns==3)
			{
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.AscendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField2),"Column is sorted Ascending");
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.AscendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField3),"Column is sorted Ascending");
			}
		}
		else
		{
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.DescendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField1),"Column is sorted Ascending");
			if(SortableColumns==2)
			{
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.DescendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField2),"Column is sorted Ascending");
			}
			else if(SortableColumns==3)
			{
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.DescendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField2),"Column is sorted Ascending");
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.DescendingSort.replace("CockpitAppletSection", CockpitSectionName).replace("ColumnName", SortByField3),"Column is sorted Ascending");
			}
		}		
	}
	
	public void AddRemoveColumn(String CockpitSectionName, String AddRemoveColumn, String ColumnName) throws IOException, InterruptedException
	{
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);		
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
		click(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayed.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayedHeader, 15);
		
		int Selected_Columns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//select[@aria-label='Selected Columns']/option")).size();
		int Available_Columns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//select[@aria-label='Available Columns']/option")).size();
		
		switch(AddRemoveColumn)
		{
		case "AddColumn":
			if(Available_Columns==0)
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Cancel the Column Settings");
				System.out.println("All columns are added");
				Report.LogInfo("INFO", "No Columns are left to Add", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No Columns are left to Add");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.AvailableColumns.replace("ColumnName", ColumnName),"Select a column");
				click(SiebelCockpitObj.ServiceOrderCockpit.AddColumn,"Click on Add Column");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
				boolean Addedcolumn = verifyExists("@xpath=//div[text()='CockpitAppletSection']//parent::div//following-sibling::div//div[text()='ColumnName']".replace("ColumnName", ColumnName));
				if(Addedcolumn)
				{
					Report.LogInfo("INFO", "Column " +ColumnName +" is added successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is added successfully");
				}
			}
			break;
		case "RemoveColumn":
			if(Selected_Columns == 0)
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Cancel the Column Settings");
				System.out.println("No columns are added to Remove");
				Report.LogInfo("INFO", "No columns are added to Remove", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No columns are added to Remove");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumnsA.replace("ColumnName", ColumnName),"Select a column");
				click(SiebelCockpitObj.ServiceOrderCockpit.RemoveColumn,"Remove Column");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
				boolean ColumnDisplayed = verifyExists("@xpath=//div[text()='CockpitAppletSection']//parent::div//following-sibling::div//div[text()='ColumnName']".replace("ColumnName", ColumnName));
				if(ColumnDisplayed == false)
				{
					Report.LogInfo("INFO", "Column " +ColumnName +" is Removed successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is Removed successfully");
				}
			}
			break;
		case "AddAllColumns":
			if(Available_Columns==0)
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Cancel the Column Settings");
				System.out.println("All columns are added");
				Report.LogInfo("INFO", "No Columns are left to Add", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No Columns are left to Add");
			}
			else
			{
				List<WebElement> AvailableColumns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//select[@aria-label='Available Columns']/option"));
				List<String> ColumnValue = new ArrayList();
				for(int i=0; i<AvailableColumns.size(); i++)
				{
					ColumnValue.add(AvailableColumns.get(i).getText());
				}
				click(SiebelCockpitObj.ServiceOrderCockpit.AddAllColumns,"Click on Add Column");				
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
				for(int i=0; i<ColumnValue.size(); i++)
				{
					boolean ColumnDisplayed = verifyExists("@xpath=//div[text()='CockpitAppletSection']//parent::div//following-sibling::div//div[text()='ColumnName']".replace("ColumnName", ColumnValue.get(i)));
					if(ColumnDisplayed == true)
					{
						Report.LogInfo("INFO", "Added Column is available in CockpitApplet Section", "INFO");
						ExtentTestManager.getTest().log(LogStatus.INFO, "All columns are available in CockpitApplet Section");
					}					
				}				
			}
			break;
		case "RemoveAllColumns":
			if(Selected_Columns==0)
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Cancel the Column Settings");
				System.out.println("No columns are added to Remove");
				Report.LogInfo("INFO", "No columns are added to Remove", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No columns are added to Remove");
			}
			else
			{
				List<WebElement> SelectedColumns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//select[@aria-label='Selected Columns']/option"));
				List<String> ColumnValue = new ArrayList();
				for(int i=0; i<SelectedColumns.size(); i++)
				{
					ColumnValue.add(SelectedColumns.get(i).getText());
				}
				click(SiebelCockpitObj.ServiceOrderCockpit.RemoveAllColumns,"Click on Add Column");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
				for(int i=0; i<ColumnValue.size(); i++)
				{
					boolean ColumnNotDisplayed = !verifyExists("@xpath=//div[text()='CockpitAppletSection']//parent::div//following-sibling::div//div[text()='ColumnName']".replace("ColumnName", ColumnValue.get(i)));
					if(ColumnNotDisplayed == true)
					{
						Report.LogInfo("INFO", "All Columns are Removed from CockpitApplet Section", "INFO");
						ExtentTestManager.getTest().log(LogStatus.INFO, "AAll Columns are Removed from CockpitApplet Section");
					}					
				}
			}
			break;
		}	
		
	}
	
	public void ChangeColumnOrder(String CockpitSectionName, String MoveColumn, String ColumnName) throws IOException, InterruptedException
	{
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
		
		List<WebElement> Columns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]").replace("CockpitSectionName", CockpitSectionName)));
		HashMap<Integer, String> ColumnValue = new HashMap();
		int i, MovementcolumnIndex = 0,DestinationColIndex;
		for(i=0; i<Columns.size(); i++)
		{
			ColumnValue.put(i, Columns.get(i).getAttribute("innerText"));
			if(ColumnName.equalsIgnoreCase(ColumnValue.get(i)))
			{
				MovementcolumnIndex = i;
			}
		}
		
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
		click(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayed.replace("CockpitSectionName", CockpitSectionName), "Click on ColumnsDisplayed Menu");
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ColumnsDisplayedHeader, 15);				
		
		switch(MoveColumn)
		{
		case "MoveColumnUp":
			
			DestinationColIndex = MovementcolumnIndex-1;
			
			if(MovementcolumnIndex == 0)
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Save the Column Settings");
				Report.LogInfo("INFO", "Since it is the first column, Can't lift the column Up", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Since it is the first column, Can't lift the column Up");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", ColumnName),"Select a column");
				click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnUp,"Click on MoveUp Button");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
					
				List<WebElement> Columns_Movement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]").replace("CockpitSectionName", CockpitSectionName)));
				HashMap<Integer, String> ColumnValues = new HashMap();
				int j;
				for(i=0; i<Columns_Movement.size(); i++)
				{
					ColumnValues.put(i, Columns_Movement.get(i).getAttribute("innerText"));
				}
				
				if(ColumnValues.get(DestinationColIndex).equalsIgnoreCase(ColumnName))
				{
					Report.LogInfo("INFO", "Column " +ColumnName +" is moved Up successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is moved Up successfully");
				}
				else
				{
					Report.LogInfo("FAIL", "Column " +ColumnName +" is not moved Properly", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is not moved Properly");
				}
			}
			break;
				
		case "MoveColumnDown":
			DestinationColIndex = MovementcolumnIndex+1;
			
			if(MovementcolumnIndex == Columns.size())
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Save the Column Settings");
				Report.LogInfo("INFO", "Since it is the first column, Can't lift the column Down", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Since it is the first column, Can't lift the column Down");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", ColumnName),"Select a column");
				click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnDown,"Click on MoveDown Button");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
					
				List<WebElement> Columns_Movement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]").replace("CockpitSectionName", CockpitSectionName)));
				HashMap<Integer, String> ColumnValues = new HashMap();
				int j;
				for(i=0; i<Columns_Movement.size(); i++)
				{
					ColumnValues.put(i, Columns_Movement.get(i).getAttribute("innerText"));
				}
				
				if(ColumnValues.get(DestinationColIndex).equalsIgnoreCase(ColumnName))
				{
					Report.LogInfo("INFO", "Column " +ColumnName +" is moved Down successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is moved Down successfully");
				}
				else
				{
					Report.LogInfo("FAIL", "Column " +ColumnName +" is not moved Properly", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is not moved Properly");
				}
			}
			break;
		case "MoveColumnTop":
			DestinationColIndex = 0;
			
			if(MovementcolumnIndex == 0)
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Save the Column Settings");
				Report.LogInfo("INFO", "Since it is the first column, Can't lift the column Top", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Since it is the first column, Can't lift the column Top");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", ColumnName),"Select a column");
				click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnTop,"Click on MoveTop Button");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
					
				List<WebElement> Columns_Movement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]").replace("CockpitSectionName", CockpitSectionName)));
				HashMap<Integer, String> ColumnValues = new HashMap();
				int j;
				for(i=0; i<Columns_Movement.size(); i++)
				{
					ColumnValues.put(i, Columns_Movement.get(i).getAttribute("innerText"));
				}
				
				if(ColumnValues.get(DestinationColIndex).equalsIgnoreCase(ColumnName))
				{
					Report.LogInfo("INFO", "Column " +ColumnName +" is moved Top successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is moved Top successfully");
				}
				else
				{
					Report.LogInfo("FAIL", "Column " +ColumnName +" is not moved Properly", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is not moved Properly");
				}
			}
			break;
		case "MoveColumnBottom":	
			DestinationColIndex = Columns.size();
		
			if(MovementcolumnIndex == Columns.size())
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.CancelColumnSettings,"Save the Column Settings");
				Report.LogInfo("INFO", "Since it is the first column, Can't lift the column Bottom", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Since it is the first column, Can't lift the column Bottom");
			}
			else
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.SelectedColumn.replace("ColumnName", ColumnName),"Select a column");
				click(SiebelCockpitObj.ServiceOrderCockpit.MoveColumnBottom,"Click on MoveBottom Button");
				click(SiebelCockpitObj.ServiceOrderCockpit.SaveColumnSettings,"Save the Column Settings");
				Reusable.waitForAjax();
				Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
					
				List<WebElement> Columns_Movement = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div[text()]").replace("CockpitSectionName", CockpitSectionName)));
				HashMap<Integer, String> ColumnValues = new HashMap();
				int j;
				for(i=0; i<Columns_Movement.size(); i++)
				{
					ColumnValues.put(i, Columns_Movement.get(i).getAttribute("innerText"));
				}
				
				if(ColumnValues.get(DestinationColIndex).equalsIgnoreCase(ColumnName))
				{
					Report.LogInfo("INFO", "Column " +ColumnName +" is moved Bottom successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is moved Bottom successfully");
				}
				else
				{
					Report.LogInfo("FAIL", "Column " +ColumnName +" is not moved Properly", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.INFO, "Column " +ColumnName +" is not moved Properly");
				}
			}
			break;
		}								
	}
	public void ExpandCollapseApplet(String CockpitSectionName) throws IOException, InterruptedException
	{
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.ExpandApplet.replace("CockpitSectionName", CockpitSectionName));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ExpandApplet.replace("CockpitSectionName", CockpitSectionName), 15);		
		click(SiebelCockpitObj.ServiceOrderCockpit.ExpandApplet.replace("CockpitSectionName", CockpitSectionName), "Click Expand Applet Section");
		
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ExpandAppletHeader.replace("CockpitSectionName", CockpitSectionName), 15);
		click(SiebelCockpitObj.ServiceOrderCockpit.CollapseApplet.replace("CockpitSectionName", CockpitSectionName), "Click collapse Applet Section");
		Reusable.waitForAjax();								
	}
	
	public void ExportValues(String CockpitSectionName, String RowsExport, String ColumnsExport, String OutputFormat) throws IOException, InterruptedException
	{
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName));
		Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);		
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
		
		boolean ExportOption = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.ExportOption.replace("CockpitSectionName", CockpitSectionName));
		if(!ExportOption)
		{
			Report.LogInfo("Export Option","Export option is not displayed due to no records in the applet","INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,"Export option is not displayed due to no records in the applet");
			Report.logToDashboard("Export option is not displayed due to no records in the applet");
		}
		else
		{
			click(SiebelCockpitObj.ServiceOrderCockpit.ExportOption.replace("CockpitSectionName", CockpitSectionName), "Click Cockpit Settings");
			Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.ExportHeader, 15);
					
			
			if(RowsExport.equalsIgnoreCase("All"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.ExportAllRows, "Click Export All Rows Option");
			}
			if(RowsExport.equalsIgnoreCase("CurrentRow"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.ExportCurrentRow, "Click Export Current Row Option");
			}
			if(ColumnsExport.equalsIgnoreCase("All"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.ExportAllColumns, "Click Export All Columns Option");
			}
			if(ColumnsExport.equalsIgnoreCase("VisibleColumns"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.ExportVisibleColumns, "Click Export Visible Columns Option");
			}
			if(OutputFormat.equalsIgnoreCase("TabDelimitedText"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.Output_TabDelimitedText, "Tab Delimited Output Option");
			}
			if(OutputFormat.equalsIgnoreCase("CSV"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.Output_CSV, "CSV Output Option");
			}
			if(OutputFormat.equalsIgnoreCase("HTML"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.Output_HTML, "HTML Output Option");
			}
			if(OutputFormat.equalsIgnoreCase("TextWithDelimiter"))
			{
				click(SiebelCockpitObj.ServiceOrderCockpit.Output_TextDelimiter, "Text with Delimiter Output Option");
			}
			
			click(SiebelCockpitObj.ServiceOrderCockpit.NextButton, "Click Next Button");
			Reusable.waitForAjax();
			if(verifyExists(SiebelCockpitObj.ServiceOrderCockpit.ExportSuccess))
			{
				Report.LogInfo("INFO", "Export Completed Successfully", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Export Completed Successfully");
			}
			click(SiebelCockpitObj.ServiceOrderCockpit.CloseButton, "Click Close Button");
			Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSectionName), 15);
		}
		
	}
		
	public void SiebelCockpitAppletFeatures(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
		String Subverification = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Subverification");
//		String CockpitSectionName[] = {"CPD / ICD not set","Jeopardy","Service Order,Delays","CPD/ICD Due Shortly","Priority Orders","Activities - Action Required"};
//		String CockpitSectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");
		
//			if(AppletFeature.equalsIgnoreCase("Sorting"))
//			{
				String SortableColumns = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SortableColumns");
				String SortOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SortOrder");
				String SortByField1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SortByField1");
				String SortByField2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SortByField2");
				String SortByField3 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SortByField3");
				
				Sorting(Subverification,Integer.parseInt(SortableColumns),SortOrder,SortByField1,SortByField2,SortByField3);
//			}	
			
//			if(AppletFeature.equalsIgnoreCase("AddRemoveColumn"))
//			{
				String AddRemoveColumn = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "AddRemoveColumn");
				String ColumnName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ColumnName");
				
				AddRemoveColumn(Subverification,AddRemoveColumn,ColumnName);
//			}
//			if(AppletFeature.equalsIgnoreCase("ChangeColumnOrder"))
//			{
				String MoveColumn = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "MoveColumn");
				String MoveColumnName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "MoveColumnName");
				
				ChangeColumnOrder(Subverification,MoveColumn,MoveColumnName);
//			}
//			if(AppletFeature.equalsIgnoreCase("ExpandCollapseApplet"))
//			{		
				ExpandCollapseApplet(Subverification);
//			}
//			if(AppletFeature.equalsIgnoreCase("Export"))
//			{	
				String RowsExport = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "RowsExport");
				String ColumnsExport = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ColumnsExport");
				String OutputFormat = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "OutputFormat");
				
				ExportValues(Subverification,RowsExport,ColumnsExport,OutputFormat);
//			}
				verifyFilteredText();
//		}

	}
	
	public void navigateToCockpit() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab, 10);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
		click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
		Reusable.waitForSiebelLoader();
		waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader, 10);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader,"Jeopardy Menu");
		waitForAjax();
	}
	
	public void verifyCockpitMenuOptions() throws InterruptedException, IOException 
	{
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader,"Jeopardy Menu");
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortlyAppletHeader,"CPD/ICD Due Shortly Menu");
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetAppletHeader,"CPD / ICD not set Menu");
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader,"Delays Menu");
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader,"Activities - Action Required Menu");
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.priorityOrdersAppletHeader,"Priority Orders Menu");
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderAppletHeader,"Service Order Menu");
	}
	
	public void verifyCPDICDDueShortlyColumns() throws InterruptedException, IOException 
	{
		//Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortlyColumns, 10);
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortlyAppletHeader);
		verifyCockpitColumns(expectedCPDICDDueShortlyColumns, SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortlySettingsMenu,
				SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortlyColumnsDisplayedButton, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns, "CPD/ICD Due Shortly");
		click(SiebelCockpitObj.ServiceOrderCockpit.cancelButton, "Cancel button");	
	}

	public void verifyCPDICDNotSetColumns() throws InterruptedException, IOException 
	{
		//Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetColumns, 10);
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetAppletHeader);
		verifyCockpitColumns(expectedCPDICDNotSetColumns, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetSettingsMenu,
				SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetColumnsDisplayedButton, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns, "CPD / ICD not set");
		click(SiebelCockpitObj.ServiceOrderCockpit.cancelButton, "Cancel button");
	}
	
	public void verifyDelaysColumns() throws InterruptedException, IOException 
	{
		//Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.delaysColumns, 10);
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.delaysAppletHeader);
		verifyCockpitColumns(expectedDelaysColumns, SiebelCockpitObj.ServiceOrderCockpit.delaysSettingsMenu,
				SiebelCockpitObj.ServiceOrderCockpit.delaysColumnsDisplayedButton, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns,"Delays");
		click(SiebelCockpitObj.ServiceOrderCockpit.cancelButton, "Cancel button");
	}
	
	public void verifyActivitiesColumns() throws InterruptedException, IOException 
	{
		//Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.activitiesColumns, 10);
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader);
		verifyCockpitColumns(expectedActivitiesColumns, SiebelCockpitObj.ServiceOrderCockpit.activitiesSettingsMenu,
				SiebelCockpitObj.ServiceOrderCockpit.activitiesColumnsDisplayedButton, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns, "Activities - Action Required");
		click(SiebelCockpitObj.ServiceOrderCockpit.cancelButton, "Cancel button");
	}
	
	public void verifyPriorityOrdersColumns() throws InterruptedException, IOException 
	{
		//Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.priorityOrdersColumns, 10);
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.priorityOrdersAppletHeader);
		verifyCockpitColumns(expectedPriorityOrdersColumns, SiebelCockpitObj.ServiceOrderCockpit.priorityOrdersSettingsMenu,
				SiebelCockpitObj.ServiceOrderCockpit.priorityOrdersColumnsDisplayedButton, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns, "Priority Orders");
		click(SiebelCockpitObj.ServiceOrderCockpit.cancelButton, "Cancel button");
	}
	
	public void verifyServiceOrderColumns() throws InterruptedException, IOException 
	{
		//Reusable.waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderColumns, 10);
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderAppletHeader);
		verifyCockpitColumns(expectedServiceOrderColumns, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu,
				SiebelCockpitObj.ServiceOrderCockpit.serviceOrderColumnsDisplayedButton, SiebelCockpitObj.ServiceOrderCockpit.SelectedColumns, "Service Order");
		click(SiebelCockpitObj.ServiceOrderCockpit.cancelButton, "Cancel button");
	}
	
	public void verifyOrderInDelivery(String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
		String orderInDelivery = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OrderNumber_DeliveryStatus");
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderAppletHeader);
		verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.serviceOrderStatus
				, orderInDelivery, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderNewQuery, "Delivery", "Order in Delivery status");
	}
	
	public void verifyOrderInPartialDelivery(String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException 
	{
		String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
		String orderInPartialDelivery = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OrderNumber_PartialDeliveryStatus");
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderAppletHeader);
		verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderStatus
				, orderInPartialDelivery, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderNewQuery, "Partial Delivery", "Order in Partial Delivery status");
	}
	
	public void verifyOrderOnHold(String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
		String orderOnHold = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OrderNumber_OnHoldStatus");
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderAppletHeader);
		verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderStatus
				, orderOnHold, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderNewQuery, "On-hold", "Order in On-hold status");
	}

	public void countrySearch(String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String PrimaryCountry_A = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"PrimaryCountryA");
		String PrimaryCountry_B = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"PrimaryCountryB");
		String PrimaryCity_A = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"PrimaryCityA");
		String PrimaryCity_B = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"PrimaryCityB");
		String Subverification= DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Subverification");
		
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 5);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"advanceSearch");
		click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "advanceSearch");
		//waitForAjax();
		
		Reusable.waitForSiebelLoader();
		if(Subverification.equalsIgnoreCase("Country") || Subverification.equalsIgnoreCase("City"))
		{
			MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryA);
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryA, 5);
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryA,"PrimaryCountryA");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryA,PrimaryCountry_A,"Passing Value on primaryCountryA");
			
			MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryB);
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryB, 5);
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryB,"PrimaryCountryB");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.primaryCountryB,PrimaryCountry_B,"Passing Value on primaryCountryB");
		}
		
		if(Subverification.equalsIgnoreCase("City")){
//		if(!PrimaryCity_A.equals("empty")&& !PrimaryCity_B.equals("empty"))
		MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.primaryCityA);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.primaryCityA, 5);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.primaryCityA,"PrimaryCityA");
		sendKeys(SiebelCockpitObj.ServiceOrderCockpit.primaryCityA,PrimaryCity_A,"Passing Value on primaryCityA");
		
		MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.primaryCityB);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.primaryCityB, 5);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.primaryCityB,"PrimaryCityB");
		sendKeys(SiebelCockpitObj.ServiceOrderCockpit.primaryCityB,PrimaryCity_B,"Passing Value on primaryCityB");
		}
		MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.okButton, 5);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
		click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
		Reusable.waitForSiebelLoader();
		waitForAjax();
				
		Boolean isCountryMatch = true;
		MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.countryFilter);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.countryFilter, 5);
		List<WebElement> Country_Name= GetWebElements("//table[@summary='Service Order']//td[contains(@id,'Installation_Country')]");
		
		for (WebElement country: Country_Name) {
			String country_str = country.getText().trim();
					if(!(country_str.equals(PrimaryCountry_A))){
					isCountryMatch = false;
					break;
				}
			}

		if(isCountryMatch){
			Report.LogInfo("Filtered orders","Service order has the filtered country records","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Service order has the filtered country records");
			Report.logToDashboard("Service order has the filtered country records");
		}
		else{
			Report.LogInfo("Filtered orders","Service order is not having the filtered country records","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,"Service order is not having the filtered country records");
			Report.logToDashboard("Service order is not having the filtered country records");
		}
}
	
	
	public void verifyMaximizeOption() throws Exception
	{
		List<WebElement> Maximize_Buttons= GetWebElements("//*[contains(@title,':Show more')]");
		int size=Maximize_Buttons.size();
		int Total_Count=0;
		for(int i=1;i<=size;i++){
			String maxXpath = "@xpath=(//*[contains(@title,':Show more')])["+i+"]";
			Total_Count++;
			if(verifyExists(maxXpath)){
				javaScriptclick(maxXpath);
				waitForAjax();
				verifyExists(SiebelCockpitObj.ServiceOrderCockpit.minimizeBtn);
				javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.minimizeBtn);
				waitForAjax();
	      }
	}
		if(Total_Count==size)
			Report.LogInfo("Maximize Buttons","Maximize Buttons in all Applets are Clicked Successfully","INFO");
		else
			Report.LogInfo("Maximize Buttons","Maximize Buttons in all Applets are Not-Clicked Successfully","FAIL");
		
}

	public void verifyFilteredText() throws InterruptedException, IOException 
	{
		waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMenu, 10);
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeopardyDropdown,"Jeopardy Dropdown");
		selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.jeopardyDropdown, "Blue", "Jeopardy Value");
		List<WebElement> Filtered_Text= GetWebElements("//*[@class='filter-status']");
		int List_Size=Filtered_Text.size();
		int Count_Value=0;
		for (int i = 1; i <= List_Size; i++) {	
		String Xpath = "@xpath=(//*[@class='filter-status'])["+i+"]";
		MoveToElement(Xpath);
		if (verifyExists(Xpath)) {
			Count_Value++;
			waitForAjax();
		 }		
		}
		if(Count_Value==List_Size){
			Report.LogInfo("Filtered Text","Filtered Text is present on all Applets ","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Filtered Text is present on all Applets");
			Report.logToDashboard("Filtered Text is present on all Applets");
		}
		else{
			Report.LogInfo("Filtered Text","Filtered Text is not present on all Applets","FAIL");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Filtered Text is not present on all Applets");
			Report.logToDashboard("Filtered Text is not present on all Applets");
		}
	}
	
	public void verifyMyOrders() throws IOException, NoSuchElementException, InterruptedException{
		
		WebElement selectOrderType= findWebElement(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption);
		Select element = new Select(selectOrderType);
		String firstSelectedOption= element.getFirstSelectedOption().getText();
		if(firstSelectedOption.equalsIgnoreCase("My Orders")){
			verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Orders");
			
		}
		else{
			selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
			waitForAjax();
			verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Orders");
		}
		
		
	}
	
public void verifyMyTeamOrders() throws IOException, NoSuchElementException, InterruptedException{
		
		WebElement selectOrderType= findWebElement(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption);
		Select element = new Select(selectOrderType);
		String firstSelectedOption= element.getFirstSelectedOption().getText();
		if(firstSelectedOption.equalsIgnoreCase("My Team Orders")){
			verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Team Orders");
			
		}
		else{
			ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader);
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader, 10);
			selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Team Orders", "Order Type");
			waitForAjax();
			verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Team Orders");
		}
		
		click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
		waitforPagetobeenable();
	}

public void verifyCPDDueOrders() throws IOException{
	
	verifyCPDICDDueOrderNumbers(SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_filterDropdown, 
			"CPD in 10 days", SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_cpdValue, 
			SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_OrderNumber, 
			SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_NextArrow, SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_rowCounter);
	}

public void verifyICDDueOrders() throws IOException{
	
	verifyCPDICDDueOrderNumbers(SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_filterDropdown, 
			"ICD in 10 days", SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_icdValue, 
			SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_OrderNumber, 
			SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_NextArrow, SiebelCockpitObj.ServiceOrderCockpit.cpdicdDueShortly_rowCounter);
	}

public void verifyJeopardyFlag(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
	String Subverification = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Subverification");
	String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
	String PlanFlag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Plan");
	String BuildFlag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Build");
	String ActivateFlag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Activate");
//	String JeopardyFlag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "JeopardyFlag");
//	String Xtrac_Status = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Xtrac_Status");
//	String LegendValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LegendValue");
	String FieldToBeVerified = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "FieldToBeVerified");
//	String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");	
	
	String ServiceOrders[] = ServiceOrderNumber.split("\\,");
	String Field[] = FieldToBeVerified.split("\\,");
	
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption,"Order Type");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Team Orders", "Order Type");
	waitForAjax();

	for(int i=0;i<ServiceOrders.length;i++)
	{
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), 15);
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), "Service order settings menu");
		click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Jeopardy"), "New Query");
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
		javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"),"Double Click");
		//sendKeys(orderNumberLocator, orderNumber, "Service Order Ref");
		sendKeys("@xpath=//div[@title='Jeopardy List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrderNumber);
		SendkeyusingAction(Keys.ENTER);
		
		boolean ServiceOrder = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"));
		if(!ServiceOrder)
		{
			Report.LogInfo("INFO", ServiceOrders[i] + " is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, ServiceOrders[i] + "is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed");
			Report.logToDashboard(ServiceOrders[i] + "is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed");
		
		}
		
		else
		{
			if(Subverification.equalsIgnoreCase("BDWROUT"))
			{
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
			verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", Field[0]),ServiceOrders[i],PlanFlag,"Plan Column");
			verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", Field[1]),ServiceOrders[i],BuildFlag,"Build Column");
			}
			else if (Subverification.equalsIgnoreCase("LOCSCHD"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", Field[0]),ServiceOrders[i],BuildFlag,"Build Column");
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", Field[1]),ServiceOrders[i],ActivateFlag,"Activate Column");
			}
		}
			
//			else if (Subverification.equalsIgnoreCase("Completed"))
//			{
//				 ServiceOrder = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"));
//				if(!ServiceOrder)
//				{
//				Report.LogInfo("INFO", ServiceOrderNumber + " is not displayed in Jeopardy since the order has completed in XTRACT", "PASS");
//				ExtentTestManager.getTest().log(LogStatus.PASS, ServiceOrderNumber + " is not displayed in Jeopardy since the order has completed in XTRACT");
//				Report.logToDashboard(ServiceOrderNumber + " is not displayed in Jeopardy since the order has completed in XTRACT");
//				}
//				else
//				{
//				Report.LogInfo("INFO", ServiceOrderNumber + " is displayed in Jeopardy though the order has completed in XTRACT", "PASS");
//				ExtentTestManager.getTest().log(LogStatus.FAIL, ServiceOrderNumber + " is displayed in Jeopardy though the order has completed in XTRACT");
//				Report.logToDashboard(ServiceOrderNumber + " is displayed in Jeopardy though the order has completed in XTRACT");
//				}
//			}
	}
	
	
		selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
}
public void verifyStageAndJeopardyFlag(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
	String Subverification = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Subverification");
	String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
	
	String ServiceOrders[] = ServiceOrderNumber.split("\\,");

	String FieldToBeVerified = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "FieldToBeVerified");
//	String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");	
	
	selectAllOrdersOption();
	
	for(int i=0;i<ServiceOrders.length;i++)
	{
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), 15);
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), "Service order settings menu");
		click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Jeopardy"), "New Query");
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
		javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"),"Double Click");

		sendKeys("@xpath=//div[@title='Jeopardy List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrders[i]);
		SendkeyusingAction(Keys.ENTER);
		
		boolean ServiceOrder = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"));
		if(!ServiceOrder)
		{
			Report.LogInfo("INFO", ServiceOrders[i] + " is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, ServiceOrders[i] + "is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed");
			Report.logToDashboard(ServiceOrders[i] + " is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed");
		
		}
		else
		{
			if (Subverification.equalsIgnoreCase("Stage was completed as expected"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", FieldToBeVerified),ServiceOrders[i],"GREEN_SQUARE","'"+FieldToBeVerified+" Column");
			}	
			else if (Subverification.equalsIgnoreCase("Stage was completed later than expected"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", FieldToBeVerified),ServiceOrders[i],"RED_SQUARE","'"+FieldToBeVerified+" Column");		
			}
			else if (Subverification.equalsIgnoreCase("Stage is in Progress and on Track"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", FieldToBeVerified),ServiceOrders[i],"GREEN_TRIANGLE","'"+FieldToBeVerified+" Column");		
			}
			else if (Subverification.equalsIgnoreCase("Stage is in Progress but Late"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", FieldToBeVerified),ServiceOrders[i],"RED_TRIANGLE","'"+FieldToBeVerified+" Column");		
			}
			else if (Subverification.equalsIgnoreCase("Order not in jeopardy"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "CPD_Risk"),ServiceOrders[i],"GREEN","JeopardyFlag Column");
			}
			if (Subverification.equalsIgnoreCase("CPD is missed"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "CPD_Risk"),ServiceOrders[i],"BLUE","JeopardyFlag Column");
			}
			else if (Subverification.equalsIgnoreCase("Order in Jeopardy(CPD not missed)"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "CPD_Risk"),ServiceOrders[i],"RED","JeopardyFlag Column");
			}
			else if (Subverification.equalsIgnoreCase("CPD risk not calculated"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "CPD_Risk"),ServiceOrders[i],"GREY","JeopardyFlag Column");
			}
			else if (Subverification.equalsIgnoreCase("ICD is at risk / missed and CPD not set"))
			{
				waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
				verifyJeopardyColumns(SiebelCockpitObj.ServiceOrderCockpit.jeopardyStageValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "CPD_Risk"),ServiceOrders[i],"PALE_BLUE","JeopardyFlag Column");
			}
		}
			
	}
	
				
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
}

public void verifyXtracColumnsinJeopardy(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
	
	String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");	
	String XtracColumnsJeopardy = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Xtrac_Columns_Jeopardy");
	
	String[] XtracColumn1 = XtracColumnsJeopardy.split("\\,");	
	
	List<WebElement> JeopardyColumns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath(("//div[contains(@title,'CockpitSectionName List')]//table//th//div").replace("CockpitSectionName", CockpitSection)));
	
	List<String> JeopardyColumnList = new ArrayList();
	for(int i=0; i<JeopardyColumns.size(); i++)
	{	
		JeopardyColumnList.add(JeopardyColumns.get(i).getAttribute("innerText"));
		Report.LogInfo("PASS",JeopardyColumnList.get(i) , "ColumnNames");
	}			
	
	for(int j=0; j<XtracColumn1.length; j++)
	{
		if(JeopardyColumnList.contains(XtracColumn1[j]))
		{
			Report.LogInfo("PASS", XtracColumn1[j] + " is there in Jeopardy section ", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, XtracColumn1[j] + " is there in Jeopardy section ");
			Report.logToDashboard(XtracColumn1[j] + " is there in Jeopardy section ");
		}
		else
		{
			Report.LogInfo("FAIL", XtracColumn1[j] + " is not there in Jeopardy section ", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, XtracColumn1[j] + " is not there in Jeopardy section ");
			Report.logToDashboard(XtracColumn1[j] + " is not there in Jeopardy section ");
		}
	}
		
}

public void verifyXtracColumnsinMoreInfo(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
	String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
	String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");
	String XtracColumnsMoreInfo = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Xtrac_Columns_MoreInfo");	
	
	String[] XtracColumn = XtracColumnsMoreInfo.split("\\,");
		
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), 15);
	click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), "Service order settings menu");
	click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", CockpitSection), "New Query");
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
	javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection),"Double Click");
	//sendKeys(orderNumberLocator, orderNumber, "Service Order Ref");
	sendKeys("@xpath=//div[@title='"+CockpitSection+" List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrderNumber);
	SendkeyusingAction(Keys.ENTER);		
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);	
		
	click(SiebelCockpitObj.ServiceOrderCockpit.moreInfo,"More Info link");
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.workItemsHeader,20);
	
	List<WebElement> MoreInfoColumns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[@title='Work Items List Applet']//table//th//div"));
	List<String> MoreInfoColumnList = new ArrayList();
	for(int i=0; i<MoreInfoColumns.size(); i++)
	{		
		String columnNames = MoreInfoColumns.get(i).getAttribute("innerText");
		MoreInfoColumnList.add(columnNames);
		Report.LogInfo("PASS",MoreInfoColumnList.get(i) , "ColumnNames");
	}
	
	for(int i=0; i<XtracColumn.length; i++)
	{
		if(MoreInfoColumnList.contains(XtracColumn[i]))
		{
			Report.LogInfo("PASS", XtracColumn[i] + " is there in Work Items section of the service order", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, XtracColumn[i] + " is there in Work Items section of the service order");
			Report.logToDashboard(XtracColumn[i] + " is there in Work Items section of the service order");
		}
		else
		{
			Report.LogInfo("FAIL", XtracColumn[i] + " is not there in Work Items section of the service order", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, XtracColumn[i] + " is not there in Work Items section of the service order");
			Report.logToDashboard(XtracColumn[i] + " is not there in Work Items section of the service order");
		}
	}
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.CloseButton);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CloseButton,5);
	click(SiebelCockpitObj.ServiceOrderCockpit.CloseButton,"Close Button");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
}


public void verifyExcludeOption() throws InterruptedException, IOException {

	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax, "Jeopardy Maximize Button");
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
	
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter,"Jeopardy Counter");
	WebElement Status = findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter);
	String Initial_Status = Status.getText();
	Report.LogInfo("Filtered orders", "Status Before Excluding :"+Initial_Status, "INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO,"Status Before Excluding :"+Initial_Status);
	Report.logToDashboard("Status Before Excluding :"+Initial_Status);
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.minimizeBtn);
	Reusable.waitForSiebelLoader();
	
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"AdvanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "AdvanceSearch");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.projectOrder);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.projectOrder, "Project Order");
	click(SiebelCockpitObj.ServiceOrderCockpit.projectOrder,"Project Order");
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax, "Jeopardy Maximize Button");
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
	waitForAjax();
	
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter,"Jeopardy Counter");
	WebElement Status_1 = findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter);
	String Final_Status = Status_1.getText();
	Report.LogInfo("Filtered orders", "Status after Excluding :"+Final_Status, "INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO,"Status after Excluding :"+Final_Status);
	Report.logToDashboard("Status after Excluding :"+Final_Status);
	
	if(!(Initial_Status==Final_Status)){
	Report.LogInfo("Filtered orders", "Jeopardy Applete has excluded the orders with Project ID", "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS,"Jeopardy Applete has excluded the orders with Project ID");
	Report.logToDashboard("Jeopardy Applete has excluded the orders with Project ID");
	}
	else{
	Report.LogInfo("Filtered orders", "Jeopardy Applete has not excluded the orders with Project ID", "FAIL");
	ExtentTestManager.getTest().log(LogStatus.PASS,"Jeopardy Applete has not excluded the orders with Project ID");
	Report.logToDashboard("Jeopardy Applet has not excluded the orders with Project ID");
	}
}

public void verifySpecificFilter(String sheetName, String scriptNo, String dataSetNo) throws NoSuchElementException, InterruptedException, IOException {
	
	String Order_Type = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Order_Type");
	String Product_Name = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Product_Name");
	
	
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"AdvanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "AdvanceSearch");
	Reusable.waitForSiebelLoader();
	
	if(!Order_Type.equals("")){
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.orderType);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.orderType, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.orderType,"orderType");
	sendKeys(SiebelCockpitObj.ServiceOrderCockpit.orderType,Order_Type,"Passing Value on Order Type");
	}
	
	if(!Product_Name.equals("")){
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.productName);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.productName, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.productName,"productName");
	sendKeys(SiebelCockpitObj.ServiceOrderCockpit.productName,Product_Name,"Passing Value on productName");
	}
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.maxButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.maxButton, "Service Order Maximize Button");
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.maxButton);
	waitForAjax();
	
	if(!Order_Type.equals("")){
	Boolean OrderType_Condition = true; 
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.orderType_max);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.orderType_max, 5);
	List<WebElement> OrderType= GetWebElements("//table[@summary='Service Order']//td[contains(@id,'COLT_Order_Type_Subtype')]");
	
	for(int i=1;i<OrderType.size();i++)
	{
		String OrderType_Value = OrderType.get(i).getText();
		if (!OrderType_Value.equals(Order_Type))
		{
			OrderType_Condition=false;
		break;
		}
	}
	if (OrderType_Condition){
		Report.LogInfo("Filtered orders", "Filtered orders matches the entered Order Type", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"Filtered orders matches the entered Order Type");
		Report.logToDashboard("Filtered orders matches the entered Order Type");
	}
	else{
		Report.LogInfo("Filtered orders", "Filtered orders not matches the entered Order Type", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Filtered orders not matches the entered Order Type");
		Report.logToDashboard("Filtered orders not matches the entered Order Type");
	}
	}
	
	if(!Product_Name.equals("")){
	Boolean ProductName_Condition = true; 
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.productName_max);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.productName_max, 5);
	List<WebElement> ProductName= GetWebElements("//table[@summary='Service Order']//td[contains(@id,'Colt_Product')]");
	
	for(int j=1;j<ProductName.size();j++)
	{
		String ProductName_Value = ProductName.get(j).getText();
		if (!ProductName_Value.equals(Product_Name))
		{
			ProductName_Condition=false;
		break;
		}
	}
	if (ProductName_Condition){
		Report.LogInfo("Filtered orders", "Filtered Result matches the entered ProductName", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"Filtered orders matches the entered ProductName");
		Report.logToDashboard("Filtered orders matches the entered ProductName");
	}
	else{
		Report.LogInfo("Filtered orders", "Filtered Result not matches the entered ProductName", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Filtered orders not matches the entered ProductName");
		Report.logToDashboard("Filtered orders not matches the entered ProductName");
	}
	}
}
	
public void verifyCustomerDelay() throws IOException, InterruptedException
{
	waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.delayMenu, 10);
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Delay Dropdown");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Customer Delays","Delay Value");
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"advanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "AdvanceSearch");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.exclude);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.exclude, "Exclude Option");
	click(SiebelCockpitObj.ServiceOrderCockpit.exclude);
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.statusDetails);
	WebElement Status = findWebElement(SiebelCockpitObj.ServiceOrderCockpit.statusDetails);
	String Status_Value = Status.getText();
	Report.LogInfo("Excluded orders", "Value is"+Status_Value, "INFO");
	if (Status_Value.equals("No Records"))
	{
	    Report.LogInfo("Excluded orders", "Filtered Result excludes orders with Customer Delays in Applet", "PASS");
	    ExtentTestManager.getTest().log(LogStatus.PASS,"Filtered Result excludes orders with Customer Delays in Applet");
	    Report.logToDashboard("Filtered Result excludes orders with Customer Delays in Applet");
	}
   	 else{
	    Report.LogInfo("Excluded orders", "Filtered Result not excludes orders with Customer Delays in Applet", "FAIL");
	    ExtentTestManager.getTest().log(LogStatus.FAIL,"Filtered Result not excludes orders with Customer Delays in Applet");
	    Report.logToDashboard("Filtered Result not excludes orders with Customer Delays in Applet");
	}
}	

public void verifyColtDelay() throws IOException, InterruptedException
{
	waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.delayMenu, 10);
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Delay Dropdown");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Colt Delays","Delay Value");
	
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayMenuDropdown,"Delay Menu Dropdown");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayMenuDropdown,"All unresolved","Delay Menu Value");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"AdvanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "AdvanceSearch");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.excludeColtDelay);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.excludeColtDelay, "Exclude Option");
	click(SiebelCockpitObj.ServiceOrderCockpit.excludeColtDelay);
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.statusDetails);
	WebElement Status = findWebElement(SiebelCockpitObj.ServiceOrderCockpit.statusDetails);
	String Status_Value = Status.getText();
	Report.LogInfo("Excluded orders", "Value is"+Status_Value, "INFO");
	if (Status_Value.equals("No Records"))
	{
	    Report.LogInfo("Excluded orders", "Filtered Result excludes orders with Colt Delays in Applet", "PASS");
	    ExtentTestManager.getTest().log(LogStatus.PASS,"Filtered Result excludes orders with Colt Delays in Applet");
	    Report.logToDashboard("Filtered Result excludes orders with Colt Delays in Applet");
	}
    else{
		Report.LogInfo("Excluded orders", "Filtered Result not excludes orders with Colt Delays in Applet", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Filtered Result not excludes orders with Colt Delays in Applet");
	        Report.logToDashboard("Filtered Result not excludes orders with Colt Delays in Applet");
	}
}	


public void verifyDelay_ERD(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException, ParseException 
{
	String Delay_Type = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Delay_Type");
	
	if(Delay_Type.equals("Customer Delays")){
	waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.delayMenu, 10);
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Delay Dropdown");
	//selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Customer Delays","Delay Value");
	click(SiebelCockpitObj.ServiceOrderCockpit.delay_type.replace("Customer Delays", Delay_Type),"Customer Delay");
	}
	else{
	waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.delayMenu, 10);
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Delay Dropdown");
	//selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayDropdown,"Colt Delays","Delay Value");
	click(SiebelCockpitObj.ServiceOrderCockpit.delay_type.replace("Colt Delays", Delay_Type),"Colt Delay");
	}
		
	
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayMenuDropdown,"Delay Menu Dropdown");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.delayMenuDropdown,"ERD Expires in 6 days or more","Delay Menu Value");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.delayAppletMax);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.delayAppletMax,"Delay Applet Maximize");
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.delayAppletMax);
	
	List<WebElement> DueDate = GetWebElements("//td[contains(@id,'Expected_Resolved_Date')]");
	long DifferenceinDay = 0;
	int size_list = DueDate.size();
	int count = 0;
	for(int i=0;i<size_list;i++)
	{
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dateFormat = new Date();
		String StartDate = date.format(dateFormat);
		Report.LogInfo("Start Date", "Start Date is :"+StartDate, "INFO");
	
		String LastDate = DueDate.get(i).getText().concat(" 00:00:00");
		Report.LogInfo("End Date", "End Date is :"+LastDate, "INFO");
		
		Date Start = date.parse(StartDate);
		Date End = date.parse(LastDate);
		long DifferenceinTime  = End.getTime() - Start.getTime();
		Report.LogInfo("Difference in Time", "Difference in time Between two days are :"+DifferenceinTime+" ms", "INFO");
		DifferenceinDay = (DifferenceinTime/(1000 * 60 * 60* 24))%365;
		Report.LogInfo("Difference in Days", "Difference Between two days are :"+DifferenceinDay, "INFO");
		count++;
		if(DifferenceinDay>5)
		{	
			Report.LogInfo("Output", "Displays the orders with more than 6 Expiry Days", "INFO");
	
		}
	}
	if(count==0)
	{
		Report.LogInfo("Result", "There is no orders with ERD Expires in 6 days or more", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"There is no orders with ERD Expires in 6 days or more");
	        Report.logToDashboard("There is no orders with ERD Expires in 6 days or more");
	}
	else{
		Report.LogInfo("Result", "All the orders displayed are not having ERD Expires in 6 days or more", "Fail");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"All the orders displayed are not having ERD Expires in 6 days or more");
	        Report.logToDashboard("All the orders displayed are not having ERD Expires in 6 days or more");
	}	
}

public void verifyNo_ICD_Orders() throws IOException, InterruptedException
{
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.icdMenuDropdown);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdMenuDropdown,"ICD Menu Dropdown");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.icdMenuDropdown,"CPD missed","ICD Menu Value");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"AdvanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "AdvanceSearch");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.excludeICD);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.excludeICD, "Exclude Option");
	click(SiebelCockpitObj.ServiceOrderCockpit.excludeICD);
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.icdMax);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdMax,"ICD Applet Maximize");
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.icdMax);
	
	Boolean isVerified=true;
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdValues);
	List<WebElement> ICD_List = GetWebElements("//table[@summary='CPD/ICD Due Shortly']//td[contains(@id,'ICD')]");
	for (int i=0;i<ICD_List.size();i++)
	{
		String ICD_Value = ICD_List.get(i).getText();
			if(ICD_Value.equals(""))
		{
			isVerified=false;
			break;
		}
	}
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.minimizeBtn);
	click(SiebelCockpitObj.ServiceOrderCockpit.minimizeBtn, "Minimize");
	
	if(isVerified){
		Report.LogInfo("Result", "Excluded the orders with no ICD under CPD/ICD Due shortly applet", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"Excluded the orders with no ICD under CPD/ICD Due shortly applet");
	   	Report.logToDashboard("Excluded the orders with no ICD under CPD/ICD Due shortly applet");
	}	
	else{
		Report.LogInfo("Result", "Not Excluded the orders with no ICD under CPD/ICD Due shortly applet", "Fail");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Not Excluded the orders with no ICD under CPD/ICD Due shortly applet");
	   	Report.logToDashboard("Not Excluded the orders with no ICD under CPD/ICD Due shortly applet");
	}
}

public void verifyNo_CPD_Orders() throws IOException, InterruptedException
{
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.icdMenuDropdown);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdMenuDropdown,"ICD Menu Dropdown");
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.icdMenuDropdown,"CPD missed","ICD Menu Value");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"AdvanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "AdvanceSearch");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.excludeCPD);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.excludeCPD, "Exclude Option");
	click(SiebelCockpitObj.ServiceOrderCockpit.excludeCPD);
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "OK Button");
	Reusable.waitForSiebelLoader();
	
	MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.icdMax);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.icdMax,"ICD Applet Maximize");
	javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.icdMax);
	
	Boolean isVerified=true;
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.cpdValues);
	List<WebElement> CPD_List = GetWebElements("//table[@summary='CPD/ICD Due Shortly']//td[contains(@id,'CPD')]");
	for (int i=0;i<CPD_List.size();i++)
	{
		String CPD_Value = CPD_List.get(i).getText();
			if(CPD_Value.equals(""))
		{
			isVerified=false;
			break;
		}
	}
	if(isVerified){
		Report.LogInfo("Result", "Excluded the orders with no CPD under CPD/ICD Due shortly applet", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"Excluded the orders with no CPD under CPD/ICD Due shortly applet");
	   	Report.logToDashboard("Excluded the orders with no CPD under CPD/ICD Due shortly applet");
	}
	else{
		Report.LogInfo("Result", "Not Excluded the orders with no CPD under CPD/ICD Due shortly applet", "Fail");
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Not Excluded the orders with no CPD under CPD/ICD Due shortly applet");
	   	Report.logToDashboard("Not Excluded the orders with no CPD under CPD/ICD Due shortly applet");
	}
}

	public void verifyJeopardyLegend() throws IOException, InterruptedException 
	{
		waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax, 10);
		javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelCockpitObj.ServiceOrderCockpit.legendOption,"Legend Option");
		mouseOverAction(SiebelCockpitObj.ServiceOrderCockpit.legendOption);
		Reusable.waitForSiebelLoader();
		
		boolean Image = isVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyImage);
		if(Image=true)
		{
			Report.LogInfo("Result", "Image displayed successfully", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Image displayed successfully");
	   		Report.logToDashboard("Image displayed successfully");	
		}
		else{
			Report.LogInfo("Result", "Image not displayed successfully", "Fail");
			ExtentTestManager.getTest().log(LogStatus.FAIL,"Image not displayed successfully");
	   		Report.logToDashboard("Image not displayed successfully");	
		}
			
	}
	
//	public void DaysToCPDLogic(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
//	{			
//		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");						
//				
////		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), 15);
////		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), "Service order settings menu");
////		click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", CockpitSection), "New Query");
////		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
////		javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection),"Double Click");
////		
////		sendKeys("@xpath=//div[@title='"+CockpitSection+" List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrderNumber);
////		SendkeyusingAction(Keys.ENTER);		
////		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
//		LocalDate todayDate=LocalDate.now();
//		
//		String cpdText=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.cpdValue.replace("SiebelOrderNumber", ServiceOrderNumber),"title");
//		waitForAjax();
//
//		LocalDate cpdDate=LocalDate.parse(cpdText, DateTimeFormatter.ofPattern("dd/M/yyyy"));
//		
//		int DueDays = TotalDays(todayDate,cpdDate);
//		Report.LogInfo("NoofDays",Integer.toString(DueDays), "PASS");
//
//		String DaystoCPD=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.DaysToCPD.replace("SiebelOrderNumber", ServiceOrderNumber),"title");
//		if(DaystoCPD.equals(Integer.toString(DueDays)))
//		{
//			Report.LogInfo("Days to CPD",Integer.toString(DueDays), "PASS");
//			ExtentTestManager.getTest().log(LogStatus.PASS,"DaystoCPD is displayed with valid value: " + DaystoCPD);
//		}
//		else
//		{
//			Report.LogInfo("Days to CPD",Integer.toString(DueDays), "FAIL");
//			ExtentTestManager.getTest().log(LogStatus.FAIL,"DaystoCPD is not matching with the proper value: " + DaystoCPD);
//		}
//		
//	}
	
	public int DaysToCPDLogic(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
	String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");



	// waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), 15);
	// click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), "Service order settings menu");
	// click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", CockpitSection), "New Query");
	// waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
	// javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection),"Double Click");
	//
	// sendKeys("@xpath=//div[@title='"+CockpitSection+" List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrderNumber);
	// SendkeyusingAction(Keys.ENTER);
	// waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
	LocalDate todayDate=LocalDate.now();



	String cpdText=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.cpdValue.replace("SiebelOrderNumber", ServiceOrderNumber),"title");
	waitForAjax();

	LocalDate cpdDate=LocalDate.parse(cpdText, DateTimeFormatter.ofPattern("dd/M/yyyy"));

	int DueDays = TotalDays(todayDate,cpdDate);
	Report.LogInfo("NoofDays",Integer.toString(DueDays), "PASS");

	String DaystoCPD=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.DaysToCPD.replace("SiebelOrderNumber", ServiceOrderNumber),"title");
	DaystoCPD=DaystoCPD.replaceAll(",","");
	if(DaystoCPD.equals(Integer.toString(DueDays)))
	{
	Report.LogInfo("Days to CPD",Integer.toString(DueDays), "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS,"DaystoCPD is displayed with valid value: " + DaystoCPD);
	//String DaystoCPDColour=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.DaysToCPD.replace("SiebelOrderNumber", ServiceOrderNumber),"style");
	//if(DaystoCPDColour.contains("red")){
	if(Integer.parseInt(DaystoCPD)>0 && Integer.parseInt(DaystoCPD)<=10){
	Report.LogInfo("Days to CPD colour","Days to CPD value is in Red Colour", "INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO,"Days to CPD value is in Red Colour");
	}
	else{
	Report.LogInfo("Days to CPD colour","Days to CPD value is in Black Colour", "INFO");
	ExtentTestManager.getTest().log(LogStatus.INFO,"Days to CPD value is in Black Colour");
	}
	}
	else
	{
	Report.LogInfo("Days to CPD",Integer.toString(DueDays), "FAIL");
	ExtentTestManager.getTest().log(LogStatus.FAIL,"DaystoCPD "+DueDays+" is not matching with the proper value: " + DaystoCPD);
	}
	int DaystoCPDValue= Integer.parseInt(DaystoCPD);
	return DaystoCPDValue;
	}
	
	public void JeopardyColumnValues(String sheetName, String scriptNo, String dataSetNo, String FieldToBeVerified) throws IOException, InterruptedException
	{			
		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
		//String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");
		String Verification = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "FieldToBeVerified");	
		
		String ServiceOrders[] = ServiceOrderNumber.split("\\,");
		int DaystoCPDValue = 0;
		
		for(int i=0;i<ServiceOrders.length;i++)
		{
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), 15);
			click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), "Service order settings menu");
			click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Jeopardy"), "New Query");
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
			javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"),"Double Click");
			
			sendKeys("@xpath=//div[@title='Jeopardy List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrders[i]);
			SendkeyusingAction(Keys.ENTER);	
			
			boolean ServiceOrder = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"));
			if(!ServiceOrder)
			{
				Report.LogInfo("INFO", ServiceOrders[i] + " is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, ServiceOrders[i] + "is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed");
				Report.logToDashboard(ServiceOrders[i] + "is not displayed in Jeopardy either the order is not valid for the Jeopardy Section or the order got completed");
			
			}
			
			else
			{
				try{
					if(FieldToBeVerified.equalsIgnoreCase("Plan"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Plan"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Plan"),"title");
						Report.LogInfo("Plan: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Plan : " + ColumnValue);
					}
					
					else if(FieldToBeVerified.equalsIgnoreCase("Plan Milestone"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Plan_Milestone"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Plan_Milestone"),"title");
						Report.LogInfo("Plan Milestone: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Plan Milestone: " + ColumnValue);
					}
					else if(FieldToBeVerified.equalsIgnoreCase("Build"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Build"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Build"),"title");
						Report.LogInfo("Build: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Build: " + ColumnValue);
					}
					
					else if(FieldToBeVerified.equalsIgnoreCase("Build Milestone"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Build_Milestone"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Build_Milestone"),"title");
						Report.LogInfo("Build Milestone: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Build Milestone: " + ColumnValue);
					}
					else if(FieldToBeVerified.equalsIgnoreCase("Activate"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Activate"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Activate"),"title");
						Report.LogInfo("Activate: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Activate: " + ColumnValue);
					}
					
					else if(FieldToBeVerified.equalsIgnoreCase("CPD"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Activate_Milestone"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.cpdValue.replace("SiebelOrderNumber", ServiceOrders[i]),"title");
						Report.LogInfo("CPD: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"CPD: " + ColumnValue);
					}
					else if(FieldToBeVerified.equalsIgnoreCase("Open Queues"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Open_Queues"), 30);
						String ColumnValue=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.jeopardyColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]).replace("ColumnName", "Open_Queues"),"title");
						Report.LogInfo("No of Open Queues: ",ColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"No of Open Queues: " + ColumnValue);
					}
					else if(FieldToBeVerified.equalsIgnoreCase("Days to CPD"))
					{
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.cpdValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						DaysToCPDLogic(sheetName, scriptNo, dataSetNo);				
					}
					else if(FieldToBeVerified.equalsIgnoreCase("DaystoCPDRecalculation"))
					{

						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.cpdValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						DaystoCPDValue= DaysToCPDLogic(sheetName, scriptNo, dataSetNo);
						Report.LogInfo("Days to CPD Value", "Days to CPD value before Recalculation is " +DaystoCPDValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Days to CPD value before Recalculation is " +DaystoCPDValue);

						//Plan
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						String planColumnValue=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanColumnValue.replace("SiebelOrderNumber", ServiceOrders[i])).getText();
						Report.LogInfo("Plan: ",planColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Plan: " + planColumnValue);



						//Plan Milestone
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanMilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						String planMilestoneColumnValue=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanMilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrders[i])).getText();
						Report.LogInfo("Plan Milestone: ",planMilestoneColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Plan Milestone: " + planMilestoneColumnValue);



						//Build
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						String buildColumnValue=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildColumnValue.replace("SiebelOrderNumber", ServiceOrders[i])).getText();
						Report.LogInfo("Build: ",buildColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Build: " + buildColumnValue);



						//Build Milestone
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildmilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						String buildMilestoneColumnValue=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildmilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrders[i])).getText();
						Report.LogInfo("Build Milestone: ",buildMilestoneColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Build Milestone: " + buildMilestoneColumnValue);



						//Activate
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyActivateColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						String activateColumnValue=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyActivateColumnValue.replace("SiebelOrderNumber", ServiceOrders[i])).getText();
						Report.LogInfo("Activate: ",activateColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Activate: " + activateColumnValue);

						//Jeopardy Flag
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyFlagColumnValue.replace("SiebelOrderNumber", ServiceOrders[i]), 30);
						String jeopardyFlagColumnValue=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyFlagColumnValue.replace("SiebelOrderNumber", ServiceOrders[i])).getText();
						Report.LogInfo("Jeopardy Flag: ",jeopardyFlagColumnValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Jeopardy Flag: " + jeopardyFlagColumnValue);




						DaysToCPDRecalculationLogic(sheetName, scriptNo, dataSetNo);
						NavigateToServiceOrderCockpitTab();
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), 15);
						click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), "Service order settings menu");
						click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Jeopardy"), "New Query");
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
						javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"),"Double Click");



						sendKeys("@xpath=//div[@title='Jeopardy List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrders[i]);
						SendkeyusingAction(Keys.ENTER);
						waitForAjax();
						String DaystoCPD=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.DaysToCPD.replace("SiebelOrderNumber", ServiceOrders[i]),"title");
						DaystoCPD=DaystoCPD.replaceAll(",","");
						long timeInMilliseconds=300000;
						while(DaystoCPD.equals(new Integer(DaystoCPDValue).toString())){
						Thread.sleep(timeInMilliseconds);
						long timeInSeconds= ((timeInMilliseconds/1000)%60);
						click(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderCockpitTab,"service Order Cockpit Tab");
						Reusable.waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.AdvancedFilter, 500);
						selectMyTeamOrdersOption();
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), 15);
						click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Jeopardy"), "Service order settings menu");
						click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Jeopardy"), "New Query");
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"), 15);
						javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", "Jeopardy"),"Double Click");



						sendKeys("@xpath=//div[@title='Jeopardy List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrders[i]);
						SendkeyusingAction(Keys.ENTER);
						waitForAjax();



						DaystoCPD=getAttributeFrom(SiebelCockpitObj.ServiceOrderCockpit.DaysToCPD.replace("SiebelOrderNumber", ServiceOrders[i]),"title");
						DaystoCPD=DaystoCPD.replaceAll(",","");
						if(timeInSeconds==3600)
						{
						Report.LogInfo("Days to CPD Value", "Days to CPD value is not updated even after 45 Mins " +DaystoCPDValue, "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,"Days to CPD value is not updated even after 45 Mins " +DaystoCPDValue);





						break;
						}
						}



						if(!DaystoCPD.equals(new Integer(DaystoCPDValue).toString())){
						DaystoCPDValue= DaysToCPDLogic(sheetName, scriptNo, dataSetNo);
						Report.LogInfo("Days to CPD Value", "Days to CPD value after Recalculation is " +DaystoCPDValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Days to CPD value after Recalculation is " +DaystoCPDValue);





						//Plan
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber), 30);
						String planColumnValue1=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber)).getText();
						Report.LogInfo("Plan: ",planColumnValue1, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Plan: " + planColumnValue1);



						//Plan Milestone
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanMilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber), 30);
						String planMilestoneColumnValue1=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyPlanMilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber)).getText();
						Report.LogInfo("Plan Milestone: ",planMilestoneColumnValue1, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Plan Milestone: " + planMilestoneColumnValue1);



						//Build
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber), 30);
						String buildColumnValue1=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber)).getText();
						Report.LogInfo("Build: ",buildColumnValue1, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Build: " + buildColumnValue1);



						//Build Milestone
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildmilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber), 30);
						String buildMilestoneColumnValue1=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyBuildmilestoneColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber)).getText();
						Report.LogInfo("Build Milestone: ",buildMilestoneColumnValue1, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Build Milestone: " + buildMilestoneColumnValue1);



						//Activate
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyActivateColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber), 30);
						String activateColumnValue1=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyActivateColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber)).getText();
						Report.LogInfo("Activate: ",activateColumnValue1, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Activate: " + activateColumnValue1);



						//Jeopardy Flag
						waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyFlagColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber), 30);
						String jeopardyFlagColumnValue1=findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyFlagColumnValue.replace("SiebelOrderNumber", ServiceOrderNumber)).getText();
						Report.LogInfo("Jeopardy Flag: ",jeopardyFlagColumnValue1, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,"Jeopardy Flag: " + jeopardyFlagColumnValue1);
					}
					}
					else if(FieldToBeVerified.equalsIgnoreCase("DaystoCPD_Zero"))
					{
						verifySameday_CPD();			
					}
			}
				catch (Exception e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,"Not able to fetch the value");
					
					Report.LogInfo("Verify exists", "Exception in fetching the value of the column: " + e, "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,"Exception in fetching the value of the column: " + e);
					Report.logToDashboard("Exception in fetching the value of the column: " + e);
					Assert.assertTrue(false);
				}				
			}							
		}			
		selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
	}
	
	public void verifyJeopardyViewMoreInfoColumns(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
		String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");
		String Sieble_Columns_Jeopardy = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Sieble_Columns_Jeopardy");	
		
		String[] XtracColumn = Sieble_Columns_Jeopardy.split("\\,");
			
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), 15);
		click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", CockpitSection), "Service order settings menu");
		click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", CockpitSection), "New Query");
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
		javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection),"Double Click");
		//sendKeys(orderNumberLocator, orderNumber, "Service Order Ref");
		sendKeys("@xpath=//div[@title='"+CockpitSection+" List Applet']//td[contains(@id,'Order_Number')]/input", ServiceOrderNumber);
		SendkeyusingAction(Keys.ENTER);		
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);	
			
		click(SiebelCockpitObj.ServiceOrderCockpit.moreInfo,"More Info link");
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.workItemsHeader,20);
		
		List<WebElement> MoreInfoColumns = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[@class='colt-custom-delay-section']//table//th"));
		List<String> MoreInfoColumnList = new ArrayList();
		for(int i=0; i<MoreInfoColumns.size(); i++)
		{		
			String columnNames = MoreInfoColumns.get(i).getAttribute("innerText");
			MoreInfoColumnList.add(columnNames);
			Report.LogInfo("PASS",MoreInfoColumnList.get(i) , "ColumnNames");
		}
		
		for(int i=0; i<XtracColumn.length; i++)
		{
			if(MoreInfoColumnList.contains(XtracColumn[i]))
			{
				Report.LogInfo("PASS", XtracColumn[i] + " is there in COLT/Customer Delay section of the service order", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, XtracColumn[i] + " is there in COLT/Customer Delay section of the service order");
				Report.logToDashboard(XtracColumn[i] + " is there in Work Items section of the service order");
			}
			else
			{
				Report.LogInfo("FAIL", XtracColumn[i] + " is not there in COLT/Customer Delay section of the service order", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, XtracColumn[i] + " is not there in COLT/Customer Delay section of the service order");
				Report.logToDashboard(XtracColumn[i] + " is not there in Work Items section of the service order");
			}
		}
		MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.CloseButton);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CloseButton,5);
		click(SiebelCockpitObj.ServiceOrderCockpit.CloseButton,"Close Button");
		selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
	}

	public void verifyJeopardy_CPD() throws IOException, InterruptedException 
	{
		waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax, 10);
		javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
		Reusable.waitForSiebelLoader();
		
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.jeopardyCPD, 5);
		MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.jeopardyCPD);
		List<WebElement> CPD_Values= GetWebElements("//table[@summary='Jeopardy']//td[contains(@id,'Activate_Milestone')]");
		int No_of_CPD = CPD_Values.size();
		int count=0;
		
		for(int i=1;i<CPD_Values.size();i++)
		{
			String text = CPD_Values.get(i).getText();
			if(!(text.equals(" ")||text.equals(null)))
			{
				Report.LogInfo("No CPD", "Orders with No CPD date", "INFO");
			}
			count++;
		}
		if (No_of_CPD==count)
		{
			Report.LogInfo("Result", "CPD values are mentioned correctly for all orders", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"CPD values are mentioned correctly for all orders");
	   		Report.logToDashboard("CPD values are mentioned correctly for all orders");	
		}
		else
		{
			Report.LogInfo("Result", "CPD values are not mentioned correctly for all orders", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,"CPD values are not mentioned correctly for all orders");
	   		Report.logToDashboard("CPD values are not mentioned correctly for all orders");
		}				
	}

	public void verifySameday_CPD() throws IOException, InterruptedException, ParseException
	{
		waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax, 10);
		javaScriptclick(SiebelCockpitObj.ServiceOrderCockpit.jeopardyMax);
		Reusable.waitForSiebelLoader();
		
		List<WebElement> CPD_List = GetWebElements("//table[@summary='Jeopardy']//td[contains(@id,'Activate_Milestone')]");
		List<WebElement> DaystoCPD_List = GetWebElements("//table[@summary='Jeopardy']//td[contains(@id,'Days_to_CPD')]");
		
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		Date dateFormat = new Date();
		String StartDate = date.format(dateFormat);
		Report.LogInfo("Start Date", "Start Date is :"+StartDate, "INFO");

		long DifferenceinDay = 0;
		int size_CPDList = CPD_List.size();
		for(int i=0;i<size_CPDList;i++)
		{
			String LastDate = CPD_List.get(i).getText();
			if(!(LastDate.equals(" ")||LastDate.equals(null)))
			{	
			Date Start = date.parse(StartDate);
			Report.LogInfo("Start Date", "Current Date :"+Start, "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,"Current Date: "+Start);
			Date End = date.parse(LastDate);
			Report.LogInfo("Difference in Days", "CPD Date for the order is :"+End, "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,"CPD Date for the order is : "+End);
			
			long DifferenceinTime  = End.getTime() - Start.getTime();
			DifferenceinDay = (DifferenceinTime/(1000 * 60 * 60* 24))%365+0;
			Report.LogInfo("Difference in Days", "Difference Between two days are :"+DifferenceinDay, "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,"Difference Between two days are : "+DifferenceinDay);
			
			if(DifferenceinDay==0)
			{
			String DaystoCPD = DaystoCPD_List.get(i).getText();	
			Report.LogInfo("Days to CPD", "The Days to CPD value is : "+DaystoCPD, "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,"The Days to CPD value is : "+DaystoCPD);
			long Days_Converted=Long.parseLong(DaystoCPD);
			if(Days_Converted==DifferenceinDay)
			{
				Report.LogInfo("Output", "The CPD date is same as current date", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,"The CPD date is same as current date");
	   			Report.logToDashboard("The CPD date is same as current date");		
			}
			else{
				Report.LogInfo("Output", "No CPD date is same as current date", "INFO");
				ExtentTestManager.getTest().log(LogStatus.PASS,"No CPD date is same as current date");
	   			Report.logToDashboard("No CPD date is same as current date");	
			}
			}
			}
		}		
	}
	
	public void verifyOnnetRemoteWorkOrder(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
	String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
	String onnetRemoteWork_Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"onnetRemoteWork_OrderNumber");
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetAppletHeader);
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_filterDropdown, "Set Onnet CPD", "CPD / ICD not set filter dropdown value");
	waitForAjax();
	verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_OverallConnectionType
	, onnetRemoteWork_Order, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName), "OnNet", "Overall connection type");}public void verifyOnnetSiteVisitOrder(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
	String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
	String onnetSiteVisit_Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"onnetSiteVisit_OrderNumber");
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetAppletHeader);
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_filterDropdown, "Set Onnet CPD", "CPD / ICD not set filter dropdown value");
	waitForAjax();
	verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_OverallConnectionType
	, onnetSiteVisit_Order, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName), "OnNet", "Overall connection type");}public void verifyOnnetExistingBuildingNewPopOrder(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
	String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
	String onnetExistingBuilding_Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"onnetExistingBuilding_OrderNumber");
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetAppletHeader);
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_filterDropdown, "Set Onnet CPD", "CPD / ICD not set filter dropdown value");
	waitForAjax();
	verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_OverallConnectionType
	, onnetExistingBuilding_Order, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName), "OnNet", "Overall connection type");}public void verifyOnnetNewBuildingNewPopOrder(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
	String sectionName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CockpitSectionName");
	String onnetNewBuilding_Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"onnetNewBuilding_OrderNumber");
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetAppletHeader);
	selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_filterDropdown, "Set New Building ICD", "CPD / ICD not set filter dropdown value");
	waitForAjax();
	verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSet_OverallConnectionType
	, onnetNewBuilding_Order, SiebelCockpitObj.ServiceOrderCockpit.cpdicdNotSetSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName), "OnNet - New Building", "Overall connection type");}
	public void verifyExistingFilterNotReset(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
	String productName = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Product_Name");
	String PriorityValue = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Priority");
	String SelectedFilterOrderNumber= DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"SelectedFilterOrder");
	String sectionName= "Jeopardy";
	//select Product from Advanced Search
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"Advanced Search");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "Advanced Search");
	//waitForAjax();
	Reusable.waitForSiebelLoader();
	selectDropdownValueByExpandingArrow_commonMethod("Product", SiebelCockpitObj.ServiceOrderCockpit.productDropdownClick, productName);
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.okButton, 10);
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "Ok button");
	waitForAjax();
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 15);
	//select Priority from Advanced Search
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"advanceSearch");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "Advanced Search");
	//waitForAjax();
	Reusable.waitForSiebelLoader();
	selectDropdownValueByExpandingArrow_commonMethod("Priority", SiebelCockpitObj.ServiceOrderCockpit.priorityDropdownClick, PriorityValue);
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.okButton, 10);
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "Ok button");
	waitForAjax();
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 15);
	//verifying product name column value
	verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_ProductName.replace("CockpitSection", sectionName)
	, SelectedFilterOrderNumber, SiebelCockpitObj.ServiceOrderCockpit.JeopardySettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_newQueryOption.replace("CockpitSection", sectionName), productName, "Product Name");
	//verifying priority column value
	verifyOrderStatus(sectionName, SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_serviceOrderRef, SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_serviceOrderRefInputField.replace("CockpitSection", sectionName), SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_Priority.replace("CockpitSection", sectionName)
	, SelectedFilterOrderNumber, SiebelCockpitObj.ServiceOrderCockpit.JeopardySettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_newQueryOption.replace("CockpitSection", sectionName), PriorityValue, "Priority");
	//Reset filter in advanced search
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 5);
	verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"Advanced Search");
	click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "Advanced Search");
	//waitForAjax();
	Reusable.waitForSiebelLoader();
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.resetButton);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.resetButton, 10);
	click(SiebelCockpitObj.ServiceOrderCockpit.resetButton, "Reset button");
	waitForAjax();
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 15);
	ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.okButton);
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.okButton, 10);
	click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "Ok button");
	waitForAjax();
	waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, 15);
	}


	public void verifyWelcomeCall(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{

		String welcomeCall_Order= DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"welcomeCallOrderNumber");
		String sectionName= "Activities - Action Required";

		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader, 10);
		verifyActivitiesFilterOptions(SiebelCockpitObj.ServiceOrderCockpit.activities_filterDropdown.replace("CockpitSection", sectionName),
		"Welcome Call", SiebelCockpitObj.ServiceOrderCockpit.activities_ActivityTypeValue.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.activities_OrderNumber.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.activities_serviceOrderRefInputField.replace("CockpitSection", sectionName),
		welcomeCall_Order, SiebelCockpitObj.ServiceOrderCockpit.activitiesSettingsMenu,
		SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.statusDropdownClick);
		}



		public void verifyInflightChange(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{

		String inflightChanage_Order= DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"inflightChangeOrderNumber");
		String sectionName= "Activities - Action Required";

		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader, 10);
		verifyActivitiesFilterOptions(SiebelCockpitObj.ServiceOrderCockpit.activities_filterDropdown.replace("CockpitSection", sectionName),
		"Infight Change", SiebelCockpitObj.ServiceOrderCockpit.activities_ActivityTypeValue.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.activities_OrderNumber.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.activities_serviceOrderRefInputField.replace("CockpitSection", sectionName),
		inflightChanage_Order, SiebelCockpitObj.ServiceOrderCockpit.activitiesSettingsMenu,
		SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.statusDropdownClick);
		}



		public void verifyOrderAutoClose(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{

		String orderAutoClose_Order= DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"orderAutoCloseOrderNumber");
		String sectionName= "Activities - Action Required";

		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader);
		waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.activitiesAppletHeader, 10);
		verifyActivitiesFilterOptions(SiebelCockpitObj.ServiceOrderCockpit.activities_filterDropdown.replace("CockpitSection", sectionName),
		"Order Auto Close", SiebelCockpitObj.ServiceOrderCockpit.activities_ActivityTypeValue.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.activities_OrderNumber.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.activities_serviceOrderRefInputField.replace("CockpitSection", sectionName),
		orderAutoClose_Order, SiebelCockpitObj.ServiceOrderCockpit.activitiesSettingsMenu,
		SiebelCockpitObj.ServiceOrderCockpit.newQueryOption.replace("CockpitSection", sectionName),
		SiebelCockpitObj.ServiceOrderCockpit.statusDropdownClick);
		}
		
		public void DaysToCPDRecalculationLogic(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
		{
		//String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");


		SendkeyusingAction(Keys.TAB);
		waitForAjax();
		click(SiebelCockpitObj.ServiceOrderCockpit.Jeopardy_serviceOrderRef, "Service Order Ref");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		click(SiebelCockpitObj.ServiceOrderCockpit.orderDatesTab, "Order Dates tab");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.cpdDateSelector);
		click(SiebelCockpitObj.ServiceOrderCockpit.cpdDateSelector, "CPD Date selector");
		List<WebElement> cpdDays= findWebElements(SiebelCockpitObj.ServiceOrderCockpit.cpdDaysFromCalendar);
		int cpdDaysCount= cpdDays.size();
		int cpdNewDate=0;
		for(int i=1; i<=cpdDaysCount;i++){


		if(cpdDays.get(i).getAttribute("class").contains("active")){
		String cpdActiveDayText= cpdDays.get(i).getText();
		int cpdActiveDayTextValue= Integer.parseInt(cpdActiveDayText);
		if(cpdActiveDayTextValue>=1 && cpdActiveDayTextValue<26){


		cpdNewDate=cpdActiveDayTextValue+2;
		}
		else{
		cpdNewDate=cpdActiveDayTextValue-2;
		}


		break;
		}
		}


		click(SiebelCockpitObj.ServiceOrderCockpit.cpdDaySelectFromCalendar.replace("value",new Integer(cpdNewDate).toString()), "CPD Date");
		waitForAjax();

		boolean isPopupAvailable=false;
		try{
		WebElement cpdReschedulePopup= findWebElement(SiebelCockpitObj.ServiceOrderCockpit.cpdReschedulePopup);
		isPopupAvailable= cpdReschedulePopup.isDisplayed();
		}
		catch(Exception e){
		Report.LogInfo("Verify exists", "Reschedule CPD Popup is not displayed", "INFO");
		}

		if(isPopupAvailable){
		selectDropdownValueByExpandingArrow_commonMethod("Change Driven By", SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_ChangeDrivenBy, "COLT");
		selectDropdownValueByExpandingArrow_commonMethod("Reschedule Reason", SiebelCockpitObj.ServiceOrderCockpit.cpdReschedule_RescheduleReason, "Human Error");
		click(SiebelCockpitObj.ServiceOrderCockpit.cpdRescheduleSave, "Save button");
		Reusable.waitForSiebelLoader();
		}
		else{
		Report.LogInfo("Verify exists", "Reschedule CPD Popup is not displayed", "INFO");
		}
		Actions action=new Actions(webDriver);
		action.keyDown(Keys.CONTROL).sendKeys("S").keyUp(Keys.CONTROL).build().perform();


		}
		
		public void verifyExclude_CeaseOrder(String sheetName, String scriptNo, String dataSetNo) throws NoSuchElementException, IOException, InterruptedException {
			String Order_Subtype = DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Order_Subtype");
			selectAllOrdersOption();
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeopardy_MenuBtn);
			click(SiebelCockpitObj.ServiceOrderCockpit.jeopardy_MenuBtn, "Jeopardy Menu");
			
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.newQuery);
			click(SiebelCockpitObj.ServiceOrderCockpit.newQuery, "New Query");
			
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.orderTypeField);
			click(SiebelCockpitObj.ServiceOrderCockpit.orderTypeField, "Order Type Field");
			
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.order_Type);
			click(SiebelCockpitObj.ServiceOrderCockpit.order_Type, "Order Type");
			sendKeys(SiebelCockpitObj.ServiceOrderCockpit.order_Type, Order_Subtype, "Order Sub Type");
			keyPress(SiebelCockpitObj.ServiceOrderCockpit.order_Type, Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter);
			WebElement Status = findWebElement(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter);
			String Status_Value = Status.getText();
			if (Status_Value.equals("No Records"))
			{
			    Report.LogInfo("Excluded orders", "There is no Ceased Orders", "INFO");
			}
			
			MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch);
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch,"advanceSearch");
			click(SiebelCockpitObj.ServiceOrderCockpit.advanceSearch, "Clicks on advanceSearch");
			Reusable.waitForSiebelLoader();
			
			MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.excludeCeaseOrder);
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.excludeCeaseOrder, "exclude Cease Order");
			click(SiebelCockpitObj.ServiceOrderCockpit.excludeCeaseOrder);
			
			MoveToElement(SiebelCockpitObj.ServiceOrderCockpit.okButton);
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.okButton,"OK Button");
			click(SiebelCockpitObj.ServiceOrderCockpit.okButton, "Clicks on OK Button");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelCockpitObj.ServiceOrderCockpit.jeoPardy_rowCounter);
			String Final_Value = Status.getText();
			if (Final_Value.equals("No Records"))
			{
			    Report.LogInfo("Excluded orders", "The Ceased orders were excluded", "PASS");
			    ExtentTestManager.getTest().log(LogStatus.PASS,"The Ceased orders were excluded");
	   		    Report.logToDashboard("The Ceased orders were excluded");	
			}
			else{
				Report.LogInfo("Excluded orders", "The Ceased orders were not excluded", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,"The Ceased orders were not excluded");
	   		        Report.logToDashboard("The Ceased orders were not excluded");	
			}
		}
		
		public void verifyStatus_ServiceOrderApplet(String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
		{

			ScrollIntoViewByString(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderAppletHeader);
			selectMyTeamOrdersOption();
			
			int Status_TotalCount,Delivery_TotalCount = 0,Partial_Delivery_TotalCount = 0,OnHold_TotalCount = 0;
			
			Status_TotalCount = Integer.parseInt(verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Orders"));
			Report.LogInfo("INFO","No of records available with all Statuses is "+Status_TotalCount+"", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "No of records available with all Statuses is "+Status_TotalCount+"");
			Report.logToDashboard("No of records available with all Statuses is "+Status_TotalCount+"");
			
			refreshPage();
			waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab, 20);
			click(SiebelCockpitObj.ServiceOrderCockpit.cockpitTab,"Service Orders Cockpit Tab");
			WaitForAjax();
			waitForElementToBeVisible(SiebelCockpitObj.ServiceOrderCockpit.jeopardyAppletHeader, 10);
			
			//Change the order of the column
			ChangeColumnOrder("Service Order", "MoveColumnTop", "Order Status");
			
			//Getting Count with Delivery Status
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Service Order"), 15);
			click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Service Order"), "Service order settings menu");
			click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Service Order"), "New Query");
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"), 15);
			javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"),"Double Click");
			//sendKeys(orderNumberLocator, orderNumber, "Service Order Ref");
			sendKeys("@xpath=//div[@title='Service Order List Applet']//td[contains(@id,'Order_Status')]/input", "Delivery");
			SendkeyusingAction(Keys.ENTER);
						
			boolean StatusColumn = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"));
			if(!StatusColumn)
			{
				Report.LogInfo("INFO","No records displayed for this search", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No records displayed for this search");
				Report.logToDashboard("No records displayed for this search");			
			}
			else
			{
				Delivery_TotalCount = Integer.parseInt(verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Orders"));
				Report.LogInfo("INFO","No of records displayed available with Delivery Status is "+Delivery_TotalCount+"", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No of records displayed available with Delivery Status is "+Delivery_TotalCount+"");
				Report.logToDashboard("No of records displayed available with Delivery Status is "+Delivery_TotalCount+"");	
			}								
			
			//Getting Count with Partial Delivery Status
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Service Order"), 15);
			click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Service Order"), "Service order settings menu");
			click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Service Order"), "New Query");
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"), 15);
			javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"),"Double Click");
			//sendKeys(orderNumberLocator, orderNumber, "Service Order Ref");
			sendKeys("@xpath=//div[@title='Service Order List Applet']//td[contains(@id,'Order_Status')]/input", "Partial Delivery");
			SendkeyusingAction(Keys.ENTER);
			
			StatusColumn = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"));
			if(!StatusColumn)
			{
				Report.LogInfo("INFO","No records displayed for this search", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No records displayed for this search");
				Report.logToDashboard("No records displayed for this search");			
			}
			else
			{
				Partial_Delivery_TotalCount = Integer.parseInt(verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Orders"));
				Report.LogInfo("INFO","No of records available with Partial Delivery status is "+Partial_Delivery_TotalCount+"", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No of records available with Partial Delivery status is "+Partial_Delivery_TotalCount+"");
				Report.logToDashboard("No of records available with Partial Delivery status is "+Partial_Delivery_TotalCount+"");
			}			
			
			//Getting Count with OnHold Status
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Service Order"), 15);
			click(SiebelCockpitObj.ServiceOrderCockpit.CockpitSettings.replace("CockpitSectionName", "Service Order"), "Service order settings menu");
			click(SiebelCockpitObj.ServiceOrderCockpit.newQueryMenu.replace("CockpitSectionName", "Service Order"), "New Query");
			waitForElementToAppear(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"), 15);
			javaScriptDoubleclick(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"),"Double Click");
			//sendKeys(orderNumberLocator, orderNumber, "Service Order Ref");
			sendKeys("@xpath=//div[@title='Service Order List Applet']//td[contains(@id,'Order_Status')]/input", "On-hold");
			SendkeyusingAction(Keys.ENTER);
			StatusColumn = verifyExists(SiebelCockpitObj.ServiceOrderCockpit.orderStatusColumn.replace("CockpitSectionName", "Service Order"));
			
			if(!StatusColumn)
			{
				Report.LogInfo("INFO","No records displayed for this search", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No records displayed for this search");
				Report.logToDashboard("No records displayed for this search");			
			}
			else
			{
				OnHold_TotalCount = Integer.parseInt(verifyRecordCount(SiebelCockpitObj.ServiceOrderCockpit.serviceOrderSettingsMenu, SiebelCockpitObj.ServiceOrderCockpit.serviceOrderRecordCountButton, SiebelCockpitObj.ServiceOrderCockpit.dialogbox, "Service Order", "My Orders"));
				Report.LogInfo("INFO","No of records available with On-hold status status is "+OnHold_TotalCount+"", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, "No of records available with On-hold status status is "+OnHold_TotalCount+"");
				Report.logToDashboard("No of records available with On-hold status status is "+OnHold_TotalCount+"");
			}						
			
			int StatusCount = Delivery_TotalCount + Partial_Delivery_TotalCount + OnHold_TotalCount;
			if(StatusCount == Status_TotalCount)
			{
				Report.LogInfo("PASS", "All the orders in Service Order Applet are in either Delivery/Partial Delivery/OnHold", "PASS");
			    ExtentTestManager.getTest().log(LogStatus.PASS,"All the orders in Service Order Applet are in either Delivery/Partial Delivery/OnHold");
	   		    Report.logToDashboard("All the orders in Service Order Applet are in either Delivery/Partial Delivery/OnHold");
			}
			else
			{
				Report.LogInfo("FAIL", "Some of the orders in Service Order Applet are not in either Delivery/Partial Delivery/OnHold", "FAIL");
			    ExtentTestManager.getTest().log(LogStatus.PASS,"Some of the orders in Service Order Applet are not in either Delivery/Partial Delivery/OnHold");
	   		    Report.logToDashboard("Some of the orders in Service Order Applet are not in either Delivery/Partial Delivery/OnHold");
			}
			selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
		}
}
