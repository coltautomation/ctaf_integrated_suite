package testHarness.siebelGalileoFunctions;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.ibm.icu.text.SimpleDateFormat;
import com.relevantcodes.extentreports.LogStatus;

import pageObjects.siebelGalileoObjects.SiebelCockpitObj;
import pageObjects.siebelGalileoObjects.SiebelLeadTimeWorkflowObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import pageObjects.siebeltoNCObjects.EthernetOrderObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

public class SiebelLeadTimeWorkflow extends SeleniumUtils 
{

	public static final String expectedTaskValues="BDWROUT, CSPPLAN, FIBRE & CIVILS DELIVERY, L2 ACTIVATION, L3 ACTIVATION, LOCINST, LOCSCHD, OLOORDR, ORDER ENTRY, ORDER MANAGEMENT, VALIDATION, VOICE - APT, VOICE - COCOM, VOICE - NUMBER PORTING, VOICE - ORDERING SYSTEM, VOICE - OTHER, VOICE - ROUTER, VOICE - SONUS/RIBBON, VOICE - TDM, VOICE - XNG";
	public static final String expectedRCATeamValues="APT Support Team, ASTAC Voice, BSS ROSS Team, COO Projects, COO SD SSC IN IP Configuration, COT/CST, CSP, Capacity Mgmt Team, Coop, Engineering, External Resource Management, Fibre team, Field Installations, IN Citrix Team, INT Scheduling team, IP Team, IT/system issues, NC Support Team, NOC Team, Node Team, External Resource Management, Offnet Team, Order Entry, Order Manager, Porting Desk, Quoting Team, Routing, SIP Delivery Team, Sales, Sales Engineer, OLO Team, Offnet Team, TAC, TRC, TSP, Tx Planning Team, VS IN Order Management, Voice Line Delivery Team, WhiteLabel, Workshop, Scheduling, Supplier Chain Management, TAC, TRC, TSP, Tx Planning Team, VS IN Order Management, Voice Line Delivery Team, WhiteLabel, Workshop";

	ReusableFunctions Reusable = new ReusableFunctions();

	public void navigateToQualityTab() throws InterruptedException, IOException 
	{
		if(isVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTab)){
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTab, "Quality tab");
			}
			else{
				WebElement qualityTabSelect= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTabSelectArrow);
				Select element = new Select(qualityTabSelect);
				element.selectByVisibleText("Quality");
			}
			Reusable.waitForSiebelLoader();
			waitforPagetobeenable();
			waitForElementToBeVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton,20);
			ScrollIntoViewByString(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton);
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton, "Create New Quality Issue button");
			Reusable.waitForSiebelLoader();
			waitForElementToBeVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton,50);
	}
	
	public void verifyVoiceSpecificValues(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {

		String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
		//		String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");
		//		//String qualityIssue_TaskValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_TaskValue");
		//		
		//		ScrollIntoViewByString(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.jeopardyAppletHeader);
		//		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.CockpitSettings.replace("CockpitSectionName", CockpitSection), 15);
		//		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.CockpitSettings.replace("CockpitSectionName", CockpitSection), "Service order settings menu");
		//		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newQueryMenu.replace("CockpitSectionName", CockpitSection), "New Query");
		//		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
		//		javaScriptDoubleclick(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderfield.replace("CockpitSectionName", CockpitSection),"Double Click");
		//		
		//		sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderText.replace("CockpitSectionName", CockpitSection), ServiceOrderNumber);
		//		SendkeyusingAction(Keys.ENTER);	
		//		SendkeyusingAction(Keys.TAB);
		//		waitForAjax();
		//		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderfield.replace("CockpitSectionName", CockpitSection), "Service Order Ref");
		//		Reusable.waitForSiebelLoader();

		waitforPagetobeenable();
		if(isVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTab)){
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTab, "Quality tab");
		}
		else{
			WebElement qualityTabSelect= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTabSelectArrow);
			Select element = new Select(qualityTabSelect);
			element.selectByVisibleText("Quality");
		}
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton, "Create New Quality Issue button");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		verifyDropdownValues(expectedTaskValues, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TaskDropdown, "Which task were you working on when you identified the issue?");
		waitForAjax();
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_CancelButton, "Cancel button");
		Reusable.waitForSiebelLoader();
	}

	public void verifyListOfRCATeamValues(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
		qualityIssueCreationProcess(sheetName, scriptNo, dataSetNo);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_RCATeamSearch, "RCA Team Search button");
		waitforPagetobeenable();
		verifyRCATeamValues(expectedRCATeamValues, "Which team should carry out the root cause analysis on this issue?");
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_OKButton, "OK button");
		waitForAjax();
	}

	public void createQualityIssue(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
		String RCATeamValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_RCATeamValue");
		qualityIssueCreationProcess(sheetName, scriptNo, dataSetNo);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_RCATeamSearch, "RCA Team Search button");
		waitforPagetobeenable();
		selectRCATeamValue(RCATeamValue,"Which team should carry out the root cause analysis on this issue?");
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_FinishButton));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_FinishButton, "Finish button");
		Reusable.waitForSiebelLoader();
		Report.LogInfo("PASS", "Quality issue is created successfully", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Quality issue is created successfully");
		Report.logToDashboard("Quality issue is created successfully");
		verifyIssueDetails(sheetName, scriptNo, dataSetNo);
		waitForAjax();
	}

	public void verifyValidationTask(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
		String RCATeamValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_RCATeamValue");
		qualityIssueCreationProcess(sheetName, scriptNo, dataSetNo);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_RCATeamSearch, "RCA Team Search button");
		waitforPagetobeenable();
		selectRCATeamValue(RCATeamValue,"Which team should carry out the root cause analysis on this issue?");
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_FinishButton));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_FinishButton, "Finish button");
		Reusable.waitForSiebelLoader();
		Report.LogInfo("PASS", "Quality issue is created successfully", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Quality issue is created successfully");
		Report.logToDashboard("Quality issue is created successfully");
		verifyIssueDetails(sheetName, scriptNo, dataSetNo);
		String PIAValue= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createdIssue.replace("value", "PIA")).getText();
		Assert.assertEquals(PIAValue, "Sales");
		Report.LogInfo("Verify exists", "PIA mapping value is displayed as "+PIAValue, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,
				"PIA mapping value is displayed as "+PIAValue);
		waitForAjax();
	}

	public void qualityIssueCreationProcess(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {

		//String ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderNumber");
		//String CockpitSection = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CockpitSectionName");
		String qualityIssue_TaskValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_TaskValue");
		String qualityIssue_CountryValue = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_CountryValue");
		String BDWROUT= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_BDWROUT");
		String CSPPLAN= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_CSPPLAN");
		String FiberCivilsDelivery= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_FiberCivilsDelivery");
		String L2Activation= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_L2Activation");
		String L3Activation= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_L3Activation");
		String LOCINST= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_LOCINST");
		String LOCSCHD= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_LOCSCHD");
		String OLOORDR= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_OLOORDR");
		String OrderEntry= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_OrderEntry");
		String OrderManagement= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_OrderManagement");
		String Validation= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_Validation");
		String VoiceAPT= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceAPT");
		String VoiceCOCOM= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceCOCOM");
		String VoiceNumberPorting= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceNumberPorting");
		String VoiceOrderingSystem= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceOrderingSystem");
		String VoiceOther= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceOther");
		String VoiceRouter= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceRouter");
		String VoiceSonusRibbon= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceSonusRibbon");
		String VoiceTDM= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceTDM");
		String VoiceXNG= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "issueType_VoiceXNG");
		String IssueCausedBy= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_IssueCausedBy");
		String ActionTaken= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_ActionTaken");
		String ActionTaken_OtherValue= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "actionTaken_OtherValue");
		String describeIssueValue= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_DescribeIssueValue");
		String issueFixedOption= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "qualityIssue_IssueFixedOption");

		//		ScrollIntoViewByString(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.jeopardyAppletHeader);
		//		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.CockpitSettings.replace("CockpitSectionName", CockpitSection), 15);
		//		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.CockpitSettings.replace("CockpitSectionName", CockpitSection), "Service order settings menu");
		//		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newQueryMenu.replace("CockpitSectionName", CockpitSection), "New Query");
		//		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderfield.replace("CockpitSectionName", CockpitSection), 15);
		//		javaScriptDoubleclick(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderfield.replace("CockpitSectionName", CockpitSection),"Double Click");
		//		
		//		sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderText.replace("CockpitSectionName", CockpitSection), ServiceOrderNumber);
		//		SendkeyusingAction(Keys.ENTER);	
		//		SendkeyusingAction(Keys.TAB);
		//		waitForAjax();
		//		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.serviceOrderfield.replace("CockpitSectionName", CockpitSection), "Service Order Ref");
		//		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		if(isVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTab)){
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTab, "Quality tab");
		}
		else{
			WebElement qualityTabSelect= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTabSelectArrow);
			Select element = new Select(qualityTabSelect);
			element.selectByVisibleText("Quality");
		}
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton, "Create New Quality Issue button");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		selectDropdownValueByExpandingArrow_commonMethod("Which task were you working on when you identified the issue?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TaskDropdown, qualityIssue_TaskValue);
		selectDropdownValueByExpandingArrow_commonMethod("Where are you based?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_CountryDropdown, qualityIssue_CountryValue);
		waitForAjax();
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_proceedButton, "Proceed button");
		Reusable.waitForSiebelLoader();
		waitforPagetobeenable();
		if(qualityIssue_TaskValue.equals("BDWROUT")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, BDWROUT);
		}
		else if(qualityIssue_TaskValue.equals("CSPPLAN")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, CSPPLAN);
		}
		else if(qualityIssue_TaskValue.equals("FIBRE & CIVILS DELIVERY")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, FiberCivilsDelivery);
		}
		else if(qualityIssue_TaskValue.equals("L2 ACTIVATION")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, L2Activation);
		}
		else if(qualityIssue_TaskValue.equals("L3 ACTIVATION")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, L3Activation);
		}
		else if(qualityIssue_TaskValue.equals("LOCINST")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, LOCINST);
		}
		else if(qualityIssue_TaskValue.equals("LOCSCHD")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, LOCSCHD);
		}
		else if(qualityIssue_TaskValue.equals("OLOORDR")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, OLOORDR);
		}
		else if(qualityIssue_TaskValue.equals("ORDER ENTRY")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, OrderEntry);
		}
		else if(qualityIssue_TaskValue.equals("ORDER MANAGEMENT")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, OrderManagement);
		}
		else if(qualityIssue_TaskValue.equals("VALIDATION")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, Validation);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - APT")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceAPT);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - COCOM")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceCOCOM);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - NUMBER PORTING")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceNumberPorting);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - ORDERING SYSTEM")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceOrderingSystem);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - OTHER")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceOther);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - ROUTER")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceRouter);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - SONUS/RIBBON")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceSonusRibbon);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - TDM")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceTDM);
		}
		else if(qualityIssue_TaskValue.equals("VOICE - XNG")){
			selectTypeOfIssue("What type of issue have you identified?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_TypeOfIssueList, VoiceXNG);
		}		
		else{
			Report.LogInfo("Type of issue", "'What type of issue have you identified?' field value is not selected in input sheet", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'What type of issue have you identified?' field value is not selected in input sheet");
			Report.logToDashboard("'What type of issue have you identified?' field value is not selected in input sheet");
		}

		WebElement nextButton= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_NextButton);
		scrolltoview(nextButton);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_NextButton, "Next button");
		waitforPagetobeenable();
		selectTypeOfIssue("Was the issue caused by:", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_IssueCausedByList, IssueCausedBy);
		selectTypeOfIssue("What Action You have taken?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_ActionTakenList, ActionTaken);
		if(ActionTaken.equals("Other (please specify)")){
				sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_ActionTakenOtherField, ActionTaken_OtherValue,"Action taken other value");
				waitForAjax();		
		}
		sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_DescribeIssue, describeIssueValue,"'Please describe the issue in more detail' field");
		selectTypeOfIssue("Has issue been fixed?", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssue_IssueFixedOption, issueFixedOption);

	}

	public void verifyDropdownValues(String expectedValues, String dropdownArrow, String labelname) throws InterruptedException, IOException {

		List<String> expectedTempValues = Arrays.asList(expectedValues.split(",", -1));
		List<String> expectedValuesList = new ArrayList<>();
		for (String s : expectedTempValues) {
			expectedValuesList.add(s.trim());
		}

		try {
			if (isVisible(dropdownArrow)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				findWebElement(dropdownArrow).click();
				waitForAjax();
				// verify list of values inside dropdown
				List<WebElement> listofvalues = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get()
						.findElements(By.xpath("//ul[@role='combobox']/li/div"));

				List<String> listOfTaskValues = new ArrayList<>();
				/*
				 * selectedColumnsList.stream() .map((WebElement w)
				 * ->w.getText().trim()).collect(Collectors.toList());
				 */
				for (WebElement we : listofvalues) {
					listOfTaskValues.add(we.getText().trim());

				}
				ExtentTestManager.getTest().log(LogStatus.INFO, "List of Values for '"+labelname+"' field: " + listOfTaskValues);

				if (expectedValuesList.containsAll(listOfTaskValues)) {
					Report.LogInfo("Verify exists", "'"+labelname+"' field dropdown values are matching with expected values",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'"+labelname+"' field dropdown values are matching with expected values");
					Report.logToDashboard("'"+labelname+"' field dropdown values are matching with expected values");
				} else {
					Report.LogInfo("Verify exists", "'"+labelname+"' field dropdown values are not matching with expected values",
							"FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"'"+labelname+"' field dropdown values are not matching with expected values");
					Report.logToDashboard("'"+labelname+"' field dropdown values are not matching with expected values");
				}
			}
			else{
				Report.LogInfo("INFO", "'"+labelname + "' field is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+labelname + "' field is not displaying");
				Report.logToDashboard("'"+labelname + "' field is not displaying");
			}
		} catch (Exception e) {
			Report.LogInfo("Verify exists", "Exception in "+labelname+" field dropdown values verification: " + e, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Exception in "+labelname+" field dropdown values verification: " + e);
			Report.logToDashboard("Exception in "+labelname+" field dropdown values verification: " + e);
			Assert.assertTrue(false);
		}

	}

	public void selectTypeOfIssue(String labelName, String locator, String expectedValue) throws IOException{

		waitForAjax();
		List<WebElement> typeOfIssueList= findWebElements(locator);
		for(WebElement typeOfIssue:typeOfIssueList){
			String typeOfIssueText= typeOfIssue.getAttribute("value");
			if(typeOfIssueText.equalsIgnoreCase(expectedValue)){
				typeOfIssue.click();
				Report.LogInfo("PASS", "Selected '"+expectedValue+ "' option for '"+labelName+ "' field", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Selected '"+expectedValue+ "' option for '"+labelName+ "' field");
				Report.logToDashboard("Selected '"+expectedValue+ "' option for '"+labelName+ "' field");
				break;
			}
		}

		waitForAjax();
	}

	public void verifyRCATeamValues(String expectedValues, String labelname) throws InterruptedException, IOException{

		boolean isPopupAvailable=false;
		try{
			WebElement RCATeamPopup= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup);
			isPopupAvailable= RCATeamPopup.isDisplayed();
		}
		catch(Exception e){
			Report.LogInfo("Verify exists", "Pick suggested team Popup is not displayed", "INFO");
		}

		if(isPopupAvailable){

			List<String> expectedTempValues = Arrays.asList(expectedValues.split(",", -1));
			List<String> expectedValuesList = new ArrayList<>();
			for (String s : expectedTempValues) {
				expectedValuesList.add(s.trim());
			}

			try {
				waitForAjax();
				// verify list of values inside popup
				List<WebElement> listofvalues = findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamList);

				List<String> listOfTeamValues = new ArrayList<>();

				// while()
				boolean isNextAvailable = false;
				do {
					for (WebElement we : listofvalues) {
						listOfTeamValues.add(we.getText().trim());
					}
					try{
						WebElement RCATeamPopup_NextArrowDisabled= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_NextArrowDisabled);
						String nextArrowState=RCATeamPopup_NextArrowDisabled.getAttribute("class");
						if(!nextArrowState.contains("disabled")){
							WebElement RCATeamNextArrow= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_NextArrow);
							isNextAvailable= RCATeamNextArrow.isDisplayed();
							if(isNextAvailable){
								click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_NextArrow, "Next arrow");
							}
						}
						else{
							break;
						}
					}
					catch(Exception e){
						Report.LogInfo("Verify exists", "Next Arrow is not displayed", "INFO");
						break;
					}
				}while (isNextAvailable);

				ExtentTestManager.getTest().log(LogStatus.INFO, "List of Values for '"+labelname+"' field: " + listOfTeamValues);

				if (expectedValuesList.containsAll(listOfTeamValues)) {
					Report.LogInfo("Verify exists", "'"+labelname+"' field values are matching with expected values",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'"+labelname+"' field values are matching with expected values");
					Report.logToDashboard("'"+labelname+"' field values are matching with expected values");
				} else {
					Report.LogInfo("Verify exists", "'"+labelname+"' field values are not matching with expected values",
							"FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"'"+labelname+"' field values are not matching with expected values");
					Report.logToDashboard("'"+labelname+"' field values are not matching with expected values");
				}

			} catch (Exception e) {
				Report.LogInfo("Verify exists", "Exception in "+labelname+" field values verification: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in "+labelname+" field values verification: " + e);
				Report.logToDashboard("Exception in "+labelname+" field values verification: " + e);
				Assert.assertTrue(false);
			}
		}
		else{
			Report.LogInfo("Verify exists", "Pick suggested team popup is not displayed for "+labelname+" field", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Pick suggested team popup is not displayed for "+labelname+" field");
			Report.logToDashboard("Pick suggested team popup is not displayed for "+labelname+" field");
		}
	}

	public void selectRCATeamValue(String expectedValue, String labelname) throws InterruptedException, IOException{

		boolean isPopupAvailable=false;
		try{
			WebElement RCATeamPopup= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup);
			isPopupAvailable= RCATeamPopup.isDisplayed();
		}
		catch(Exception e){
			Report.LogInfo("Verify exists", "Pick suggested team Popup is not displayed", "INFO");
		}

		if(isPopupAvailable){

			try {
				waitForAjax();

				sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_TeamNameInputField, expectedValue, "Suggested team name field");
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_GoButton, "Go button");
				waitForAjax();
				try{
					if(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_GoButton).isDisplayed()){
						SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("(//td[contains(@id,'Value')])[1]")).click();
						click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.RCATeamPopup_OKButton, "OK button");
						waitForAjax();
					}
				}catch(Exception e){
					Report.LogInfo("Verify exists", "Go button is not displayed", "INFO");
				}
			} catch (Exception e) {
				Report.LogInfo("Verify exists", "Exception in "+labelname+" field values verification: " + e, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Exception in "+labelname+" field values verification: " + e);
				Report.logToDashboard("Exception in "+labelname+" field values verification: " + e);
				Assert.assertTrue(false);
			}
		}
		else{
			Report.LogInfo("Verify exists", "Pick suggested team popup is not displayed for "+labelname+" field", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Pick suggested team popup is not displayed for "+labelname+" field");
			Report.logToDashboard("Pick suggested team popup is not displayed for "+labelname+" field");
		}
	}

	public void verifyIssueDetails(String sheetName, String scriptNo, String dataSetNo) throws IOException, NoSuchElementException, InterruptedException{
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createdIssue.replace("value", "SR_Number"), 15);
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createdIssue.replace("value", "SR_Number")));
		getTextFrom(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createdIssue.replace("value", "SR_Number"), "Issue#");
		getTextFrom(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createdIssue.replace("value", "PIA"), "PIA");

	}

	public void verifyIFCWFPopupMessage(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab, "Workflows tab");
		waitforPagetobeenable();
		waitForAjax();
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton, "'+' button");
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeDropdownClick, 15);
		selectValueFromDropdownList("Workflow Type", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeDropdownClick, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValuesList, "In-Flight Change");
		waitForAjax();



		boolean isPopupAvailable=false;
		try{
		WebElement workflowPopup= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopup);
		isPopupAvailable= workflowPopup.isDisplayed();
		}
		catch(Exception e){
		Report.LogInfo("Verify exists", "Workflow Popup is not displayed", "INFO");
		}



		if(isPopupAvailable){
		String workflowPopupMessage= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopupMessage).getText();
		Report.LogInfo("Verify exists", "Popup message is displaying as '"+workflowPopupMessage+"'", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,
		"Popup message is displaying as '"+workflowPopupMessage+"'");
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopupOkButton, "Ok button");
		}
		else{
		Report.LogInfo("Verify exists", "Workflow popup is not displayed", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,
		"Workflow popup is not displayed");
		Report.logToDashboard("Workflow popup is not displayed");
		}
		waitForAjax();
	}	
	public void verifyIFCWFWarningMessage(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
	scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab));
	click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab, "Workflows tab");
	waitforPagetobeenable();
	scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));

	boolean InFlightChange=false;
	try{
	List<WebElement> workflowTypeValue1= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
	List<WebElement> workflowStatusValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowStatusValue);
	for(int i=0;i<workflowTypeValue1.size();i++){
	WebElement value= workflowTypeValue1.get(i);
	if(value.getText().equals("In-Flight Change") && !workflowStatusValue.get(i).getText().equals("Completed")){
	Report.LogInfo("Verify exists", "In-Flight Change workflow is already available", "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS,"In-Flight Change workflow is already available");
	InFlightChange=true;
	break;
	}
	}
	}catch(Exception e){
	Report.LogInfo("Verify exists", "No existing workflow with In-Flight Change is available", "FAIL");
	ExtentTestManager.getTest().log(LogStatus.FAIL,
	"No existing workflow with In-Flight Change is available");
	}

	if(!InFlightChange){
	inflightChangeWorkflowCreation(sheetName, scriptNo, dataSetNo);

	waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowSaveButton, 15);
	click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowSaveButton, "Save button");
	Reusable.waitForSiebelLoader();

	waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton, 15);
	scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));
	}
	boolean InFlightChangeStatus1=false;
	try{
	List<WebElement> workflowTypeValue2= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
	for(WebElement value1:workflowTypeValue2){
	if(value1.getText().equals("In-Flight Change")){
	waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton, 15);
	scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));
	InFlightChangeStatus1=true;
	break;
	}
	}
	if(InFlightChangeStatus1){
	//if(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowStatusValue).getText().equals("In Progress"))
	//{
	scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab));
	click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab, "Workflows tab");
	waitforPagetobeenable();
	scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));

	inflightChangeWorkflowCreation(sheetName, scriptNo, dataSetNo);
	boolean isPopupAvailable=false;
	try{
	waitForAjax();
	waitforPagetobeenable();
	waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopupMessage, 15);
	WebElement workflowPopup= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopupMessage);
	isPopupAvailable= workflowPopup.isDisplayed();
	}
	catch(Exception e){
	Report.LogInfo("Verify exists", "Workflow Popup is not displayed", "INFO");
	}



	if(isPopupAvailable){
	String workflowWarningMessage= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopupMessage).getText();
	Report.LogInfo("Verify exists", "Warning message is displaying as '"+workflowWarningMessage+"'", "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS,
	"Warning message is displaying as '"+workflowWarningMessage+"'");
	click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowPopupIgnore, "No button");
	}
	else{
	Report.LogInfo("Verify exists", "Workflow popup is not displayed", "FAIL");
	ExtentTestManager.getTest().log(LogStatus.FAIL,
	"Workflow popup is not displayed");
	Report.logToDashboard("Workflow popup is not displayed");
	}



	}
	// }
	// else{
	// Report.LogInfo("Verify exists", "Workflow status is not 'In Progress'", "FAIL");
	// ExtentTestManager.getTest().log(LogStatus.FAIL,
	// "Workflow status is not 'In Progress'");
	// }

	}catch(Exception e){
	Report.LogInfo("Verify exists", "No existing workflow with In-Flight Change is available", "FAIL");
	ExtentTestManager.getTest().log(LogStatus.FAIL,
	"No existing workflow with In-Flight Change is available");
	}
	}
	
	public void selectWorkflowType(String labelname, String dropdownArrow, String expectedValueToAdd)
			throws InterruptedException {
			waitForAjax();

			try {

			if (isVisible(dropdownArrow)) {
			Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
			if (expectedValueToAdd.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
			} else {
			findWebElement(dropdownArrow).click();
			waitForAjax();
			// verify list of values inside dropdown
			List<WebElement> listofvalues = findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValuesList);

			for (WebElement valuetypes : listofvalues) {
			if(valuetypes.getText().equals(expectedValueToAdd)){
				valuetypes.click();
				break;
			}
			}
			waitForAjax();

			Report.LogInfo("Verify exists", labelname + " dropdown value is selected as: " +expectedValueToAdd, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value is selected as: " +expectedValueToAdd);
			Report.logToDashboard(labelname + " dropdown value is selected as: " +expectedValueToAdd);

			}
			} else {
			Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			Report.logToDashboard(labelname + " is not displaying");

			}
			} catch (NoSuchElementException e) {

			System.out.println(labelname + " is not displaying");
			} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
			" NOt able to perform selection under " + labelname + " dropdown");
			System.out.println(" NO value selected under " + labelname + " dropdown");
			}
		}
	
	public void verifyOtherOptionNotDisplayed(String dataSheet, String scriptNo, String dataSetNo, String TaskName) throws Exception
	{
		String Country = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "qualityIssue_CountryValue");
		
		try{
			waitForElementToBeVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityIssuesLabel,40);
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.issueTaskNameTxb,"Task Name");
			sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.issueTaskNameTxb,TaskName,"Task Name");
			SendkeyusingAction(Keys.ENTER);	
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.regionTxb,"Country");
			sendKeys(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.regionTxb,Country,"Country");
			SendkeyusingAction(Keys.ENTER);	
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.proceedBtn,"Proceed Button");
			waitForElementToBeVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.typeofIssueLabel,40);
			
			List<WebElement> TypeOfIssueList = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//span[@id='SSQuestionList']/table//span[@class='scField']//span"));
			List<String> IssueList = new ArrayList();
			for(int i=0; i<TypeOfIssueList.size(); i++)
			{
				IssueList.add(TypeOfIssueList.get(i).getText());
			}
			if(!IssueList.contains("Other"))
			{
				Report.LogInfo("INFO", "'"+TaskName + "' does not contains the option Other in the list", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+TaskName + "' does not contains the option Other in the list");
				Report.logToDashboard("'"+TaskName + "'"+TaskName + "' does not contains the option Other in the list");
			}
			else
			{
				Report.LogInfo("FAIL", "'"+TaskName + "' contains the option Other in the list", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'"+TaskName + "' contains the option Other in the list");
				Report.logToDashboard("'"+TaskName + "'"+TaskName + "' contains the option Other in the list");
			}
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.cancelBtn,"Cancel Button");
			Reusable.waitForSiebelLoader();
			waitForElementToBeVisible(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton,40);
					
			click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.createQualityIssueButton,"Create Quality Issue Button");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Report.LogInfo("FAIL", " Exception "+e+"", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, " Exception occured "+e+"");
		}
	}
	
	public void verifyPrimarySecondaryInflightWorkflow(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{

		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab));
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowsTab, "Workflows tab");
		waitforPagetobeenable();
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));

		boolean InFlightChange=false;
		try{
		List<WebElement> workflowTypeValue1= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
		List<WebElement> workflowStatusValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowStatusValue);
		List<WebElement> primarySecondaryValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.primarySecondaryValue);
		for(int i=0;i<workflowTypeValue1.size();i++){
		WebElement value= workflowTypeValue1.get(i);
		if(value.getText().equals("In-Flight Change") && !workflowStatusValue.get(i).getText().equals("Completed") && primarySecondaryValue.get(i).getText().equals("Primary")){
		Report.LogInfo("Verify exists", "In-Flight Change workflow is already available", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"In-Flight Change workflow is already available");
		InFlightChange=true;
		break;
		}
		}
		}catch(Exception e){
		Report.LogInfo("Verify exists", "No existing workflow with In-Flight Change is available", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,
		"No existing workflow with In-Flight Change is available");
		}

		if(!InFlightChange){
		PrimarySecondaryIFCWFCreation(sheetName, scriptNo, dataSetNo, "Primary");
		waitForAjax();
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowSaveButton, 15);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowSaveButton, "Save button");
		Reusable.waitForSiebelLoader();

		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton, 15);
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));

		}
		verifyPrimarySecondaryValue("Primary");

		boolean InFlightChange1=false;
		try{
		List<WebElement> workflowTypeValue1= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
		List<WebElement> workflowStatusValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowStatusValue);
		List<WebElement> primarySecondaryValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.primarySecondaryValue);
		for(int i=0;i<workflowTypeValue1.size();i++){
		WebElement value= workflowTypeValue1.get(i);
		if(value.getText().equals("In-Flight Change") && !workflowStatusValue.get(i).getText().equals("Completed") && primarySecondaryValue.get(i).getText().equals("Secondary")){
		Report.LogInfo("Verify exists", "In-Flight Change workflow is already available", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"In-Flight Change workflow is already available");
		InFlightChange1=true;
		break;
		}
		}
		}catch(Exception e){
		Report.LogInfo("Verify exists", "No existing workflow with In-Flight Change is available", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,
		"No existing workflow with In-Flight Change is available");
		}

		if(!InFlightChange1){
		PrimarySecondaryIFCWFCreation(sheetName, scriptNo, dataSetNo, "Secondary");
		waitForAjax();
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowPopupAccept, 15);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowPopupAccept, "Yes button");
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowSaveButton, 15);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowSaveButton, "Save button");
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton, 15);
		scrolltoview(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.newButton));

		}
		verifyPrimarySecondaryValue("Secondary");
		}

		public void PrimarySecondaryIFCWFCreation(String sheetName, String scriptNo, String dataSetNo, String primarySecondaryFieldValue) throws IOException, InterruptedException{

		String reasonCategoryValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "workflowChange_ReasonCategoryValue");
		String AendReasonValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "workflowChange_AendReasonValue");
		String BendReasonValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "workflowChange_BendReasonValue");

		boolean deliveryStatus=false;
		try{
		List<WebElement> workflowTypeValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
		List<WebElement> primarySecondaryValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.primarySecondaryValue);
		for(int i=0; i<workflowTypeValue.size(); i++)
		{
		WebElement value= workflowTypeValue.get(i);
		if(value.getText().equals("Delivery") && primarySecondaryValue.get(i).getText().equals(primarySecondaryFieldValue)){
		value.click();
		deliveryStatus=true;
		break;
		}
		}
		if(deliveryStatus){
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowButton, "Change Workflow");
		waitForAjax();
		try{
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowPopupAccept, 20);
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowPopupAccept, "Yes button");
		}catch(Exception e){
		Report.LogInfo("Verify exists", "No Popup available", "INFO");
		}
		waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowReasonPopup, 15);
		selectValueFromDropdownList("Reason Category ", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.reasonCategoryDropdownArrow, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.reasonCategoryDropdownValues, reasonCategoryValue);
		selectValueFromDropdownList("A End", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.AendReasonDropdownArrow, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.AendReasonDropdownValues, AendReasonValue);
		try{
		if(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.BendReasonDropdownArrow).isDisplayed())
		{
		if(!BendReasonValue.equals("No Change")){
		selectValueFromDropdownList("B End", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.BendReasonDropdownArrow, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.BendReasonDropdownValues, BendReasonValue);
		}
		}
		}catch(Exception e){
		Report.LogInfo("Verify exists", "B End Reason field is not available", "INFO");
		}
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.proceedButton, "Proceed button");




		WebElement inflightWorkflowOption= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.inflightWorkflowRadioButton);
		if(inflightWorkflowOption.isSelected()){
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowOKButton, "Ok button");
		}
		else{
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.inflightWorkflowRadioButton, "Hold & Raise Inflight Workflow otpion");
		click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowOKButton, "Ok button");
		}
		Reusable.waitForSiebelLoader();
		}
		}catch(Exception e){
		Report.LogInfo("Verify exists", "No existing workflows available", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,
		"No existing workflows available");
		}



		}



		public void verifyPrimarySecondaryValue(String primarySecondaryFieldValue){

		try{
		List<WebElement> workflowTypeValue1= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
		List<WebElement> primarySecondaryValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.primarySecondaryValue);
		for(int j=0;j<workflowTypeValue1.size();j++){
		WebElement value=workflowTypeValue1.get(j);
		if(value.getText().equals("In-Flight Change") && primarySecondaryValue.get(j).getText().equals(primarySecondaryFieldValue)){

		Report.LogInfo("Verify exists", "Primary/Secondary field value for Inflight change workflow is displayed as '"+primarySecondaryFieldValue+"'", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,"Primary/Secondary field value for Inflight change workflow is displayed as '"+primarySecondaryFieldValue+"'");
		break;
		}
		}
		}catch(Exception e){
		Report.LogInfo("Verify exists", "No existing workflow with In-Flight Change is available", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL,
		"No existing workflow with In-Flight Change is available");
		}
		}
		
		public void selectValueFromDropdownList(String labelname, String dropdownArrow, String dropdownValues, String expectedValueToAdd)
				throws InterruptedException {
				waitForAjax();



				try {



				if (isVisible(dropdownArrow)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				if (expectedValueToAdd.equalsIgnoreCase("null")) {
				Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {
				findWebElement(dropdownArrow).click();
				waitForAjax();
				// verify list of values inside dropdown
				List<WebElement> listofvalues = findWebElements(dropdownValues);



				for (WebElement valuetypes : listofvalues) {
				if(valuetypes.getText().equals(expectedValueToAdd)){
				valuetypes.click();
				break;
				}
				}
				waitForAjax();



				Report.LogInfo("Verify exists", labelname + " dropdown value is selected as: " +expectedValueToAdd, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value is selected as: " +expectedValueToAdd);
				Report.logToDashboard(labelname + " dropdown value is selected as: " +expectedValueToAdd);



				}
				} else {
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
				Report.logToDashboard(labelname + " is not displaying");



				}
				} catch (NoSuchElementException e) {



				System.out.println(labelname + " is not displaying");
				} catch (Exception ee) {
				ee.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
				" NOt able to perform selection under " + labelname + " dropdown");
				System.out.println(" NO value selected under " + labelname + " dropdown");
				}
				}



				public void inflightChangeWorkflowCreation(String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{

				String reasonCategoryValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "workflowChange_ReasonCategoryValue");
				String AendReasonValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "workflowChange_AendReasonValue");
				String BendReasonValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "workflowChange_BendReasonValue");

				boolean deliveryStatus=false;
				try{
				List<WebElement> workflowTypeValue= findWebElements(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowTypeValue);
				for(WebElement value:workflowTypeValue){
				if(value.getText().equals("Delivery")){
				value.click();
				deliveryStatus=true;
				break;
				}
				}
				if(deliveryStatus){
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowButton, "Change Workflow");
				waitForAjax();
				try{
				waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowPopupAccept, 20);
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowPopupAccept, "Yes button");
				}catch(Exception e){
				Report.LogInfo("Verify exists", "No Popup available", "INFO");
				}
				waitForElementToAppear(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.workflowReasonPopup, 15);
				selectValueFromDropdownList("Reason Category ", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.reasonCategoryDropdownArrow, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.reasonCategoryDropdownValues, reasonCategoryValue);
				//Please select a change type in either or both "Reason" fields
				// if(!AendReasonValue.equals("No Change")){
				selectValueFromDropdownList("A End", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.AendReasonDropdownArrow, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.AendReasonDropdownValues, AendReasonValue);
				// }
				try{
				if(findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.BendReasonDropdownArrow).isDisplayed())
				{
				if(!BendReasonValue.equals("No Change")){
				selectValueFromDropdownList("B End", SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.BendReasonDropdownArrow, SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.BendReasonDropdownValues, BendReasonValue);
				}
				}
				}catch(Exception e){
				Report.LogInfo("Verify exists", "B End Reason field is not available", "INFO");
				}
				// if(AendReasonValue.equals("No Change") && BendReasonValue.equals("No Change")){
				// Report.LogInfo("Verify exists", "Please select a change type in either or both Reason fields from Data sheet", "FAIL");
				// ExtentTestManager.getTest().log(LogStatus.FAIL,
				// "Please select a change type in either or both Reason fields from Data sheet");
				// }
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.proceedButton, "Proceed button");

				WebElement inflightWorkflowOption= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.inflightWorkflowRadioButton);
				if(inflightWorkflowOption.isSelected()){
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowOKButton, "Ok button");
				}
				else{
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.inflightWorkflowRadioButton, "Hold & Raise Inflight Workflow otpion");
				click(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.changeWorkflowOKButton, "Ok button");
				}
				Reusable.waitForSiebelLoader();
				}
				}catch(Exception e){
				Report.LogInfo("Verify exists", "No existing workflows available", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
				"No existing workflows available");
				}
				}



}

