package testHarness.siebelGalileoFunctions;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.ibm.icu.text.SimpleDateFormat;
import com.relevantcodes.extentreports.LogStatus;

import pageObjects.siebelGalileoObjects.SiebelCockpitObj;
import pageObjects.siebelGalileoObjects.SiebelLeadTimeWorkflowObj;
import pageObjects.siebelGalileoObjects.WelcomeCallActivityObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import pageObjects.siebelObjects.SiebelManualValidationObj;
import pageObjects.siebeltoNCObjects.EthernetOrderObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

public class WelcomeCallActivity extends SeleniumUtils 
{

	public static ThreadLocal<String> Circuitreferencenumber = new ThreadLocal<>();
	
	public static final String expectedTaskValues="BDWROUT, CSPPLAN, FIBRE & CIVILS DELIVERY, L2 ACTIVATION, L3 ACTIVATION, LOCINST, LOCSCHD, OLOORDR, ORDER ENTRY, ORDER MANAGEMENT, VALIDATION, VOICE - APT, VOICE - COCOM, VOICE - NUMBER PORTING, VOICE - ORDERING SYSTEM, VOICE - OTHER, VOICE - ROUTER, VOICE - SONUS/RIBBON, VOICE - TDM, VOICE - XNG";
	public static final String expectedRCATeamValues="APT Support Team, ASTAC Voice, BSS ROSS Team, COO Projects, COO SD SSC IN IP Configuration, COT/CST, CSP, Capacity Mgmt Team, Coop, Engineering, External Resource Management, Fibre team, Field Installations, IN Citrix Team, INT Scheduling team, IP Team, IT/system issues, NC Support Team, NOC Team, Node Team, External Resource Management, Offnet Team, Order Entry, Order Manager, Porting Desk, Quoting Team, Routing, SIP Delivery Team, Sales, Sales Engineer, OLO Team, Offnet Team, TAC, TRC, TSP, Tx Planning Team, VS IN Order Management, Voice Line Delivery Team, WhiteLabel, Workshop, Scheduling, Supplier Chain Management, TAC, TRC, TSP, Tx Planning Team, VS IN Order Management, Voice Line Delivery Team, WhiteLabel, Workshop";

	ReusableFunctions Reusable = new ReusableFunctions();


	public int navigateToActivitiesTab() throws IOException, InterruptedException 
	{
		waitforPagetobeenable();
		if(isVisible(WelcomeCallActivityObj.WelcomeCallActivity.activitiesTab)){
			click(WelcomeCallActivityObj.WelcomeCallActivity.activitiesTab, "Activities tab");
		}
		else{
			WebElement activitiesTabSelect= findWebElement(SiebelLeadTimeWorkflowObj.LeadTimeWorkflow.qualityTabSelectArrow);
			Select element = new Select(activitiesTabSelect);
			element.selectByVisibleText("Activities");
		}
		
		waitForElementToBeVisible(WelcomeCallActivityObj.WelcomeCallActivity.ActivitiesPage,30);
		
		int NoofRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Activities']//tr")).size();
//		Report.LogInfo("WelcomeCallActivity", "No of Entries :"+ NoofRows , "INFO");
//		ExtentTestManager.getTest().log(LogStatus.INFO,  "No of Entries :"+ NoofRows);
		Reusable.waitForAjax();
		return NoofRows;		
	}
	
	public void VerifyActivitiesTable(int Rows, String OrderStatus) throws IOException, InterruptedException 
	{				
		String ActivityType = "Call - Outbound";
		String Description = "Welcome Call";
		String status = "Not Started";
		List<String> ActivityCallValues = new ArrayList();
		List<String> ColumnHeader = new ArrayList();	
		
		navigateToActivitiesTab();
		
		int NoofRows = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Activities']//tr")).size();
		
		List<WebElement> ColumnHeaderWE = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[contains(@title,'Activities List')]//table[@datatable='0']//th//div[text()]"));
		for(WebElement Data:ColumnHeaderWE)
		{
			ColumnHeader.add(Data.getText());
			 if(ColumnHeaderWE.contains(null))
			 {
				 ColumnHeader.add(Data.getAttribute("innertext"));
			 }			 
		}
		
		
		List<WebElement> ActivityValues = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Activities']//tr["+NoofRows+"]//td[@id]"));
		for(WebElement Data:ActivityValues)
		{
			 ActivityCallValues.add(Data.getText());
			 if(ActivityCallValues.contains(null))
			 {
				 ActivityCallValues.add(Data.getAttribute("innertext"));
			 }
//			 Report.LogInfo("INFO", "ActivityCallValue: " + Data.getText(), "INFO");
//			 ExtentTestManager.getTest().log(LogStatus.PASS, Data.getText() +" The value is populated properly");
		}
		for(int i=0; i<10; i++)
		{
			Report.LogInfo("INFO", ColumnHeader.get(i)+":" + ActivityCallValues.get(i), "INFO");
			ExtentTestManager.getTest().log(LogStatus.PASS, ColumnHeader.get(i)+":" + ActivityCallValues.get(i));
		}
		
		if((NoofRows>Rows)|| (OrderStatus.equalsIgnoreCase("Delivery"))||(OrderStatus.equalsIgnoreCase("Completed")))
		{	
			if(OrderStatus.equalsIgnoreCase("Delivery"))
			{
				Reusable.waitForSiebelLoader();
				if(ActivityCallValues.contains(ActivityType)&&ActivityCallValues.contains(Description)&&ActivityCallValues.contains(status))
				{
					Report.LogInfo("WelcomeCallActivity", "Welcome Call Activity is generated", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,  "Welcome Call Activity is generated");
				}
				else
				{
					Report.LogInfo("WelcomeCallActivity", "Welcome Call Activity is not generated", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,  "Welcome Call Activity is generated");
				}
			}
			else if(OrderStatus.equalsIgnoreCase("Completed"))
			{
				Reusable.waitForAjax();
				List<WebElement> ActivityValues1 = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Activities']//tr[2]//td[@id]"));
				List<String> ActivityCallValues1 = new ArrayList();
				for(WebElement Data:ActivityValues1)
				{
					ActivityCallValues1.add(Data.getText());
					 if(ActivityValues1.contains(null))
					 {
						 ActivityCallValues1.add(Data.getAttribute("innertext"));
					 }
//					 Report.LogInfo("INFO", "ActivityCallValue: " + Data.getText(), "INFO");
//					 ExtentTestManager.getTest().log(LogStatus.PASS, Data.getText() +" The value is populated properly");
				}
				
				for(int i=0; i<10; i++)
				{
					Report.LogInfo("INFO", ColumnHeader.get(i)+":" + ActivityCallValues1.get(i), "INFO");
					ExtentTestManager.getTest().log(LogStatus.PASS, ColumnHeader.get(i)+":" + ActivityCallValues1.get(i));
				}
				
				if(ActivityCallValues1.contains("Order Auto Close")&&ActivityCallValues1.contains("Done")&&ActivityCallValues.contains("Cancelled"))
				{
					Report.LogInfo("WelcomeCallActivity", "New WCA is created with status Done and the Previous WCA is Cancelled", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,  "New WCA is created with status Done and the Previous WCA is Cancelled");
				}
				else
				{
					Report.LogInfo("WelcomeCallActivity", "Welcome Call Activity is not generated properly", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,  "Welcome Call Activity is not generated properly");
					
				}
			}
				
		}
		else
		{	

				Report.LogInfo("WelcomeCallActivity", "Welcome Call Activity is not generated newly", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.INFO,  "Welcome Call Activity is not generated newly");			
		}				
		
	}
	
	public void ChangeStatus() throws Exception {
				
		Reusable.waitForSiebelLoader();
		verifyExists(WelcomeCallActivityObj.WelcomeCallActivity.OrderStatusDropdown,"Order Status Dropdown");
		ScrollIntoViewByString(WelcomeCallActivityObj.WelcomeCallActivity.OrderStatusDropdown);
		click(WelcomeCallActivityObj.WelcomeCallActivity.OrderStatusDropdown,"Order Status Dropdown");
			
		verifyExists(WelcomeCallActivityObj.WelcomeCallActivity.SelectOnholdValidation,"Select Delivery Validation");
		click(WelcomeCallActivityObj.WelcomeCallActivity.SelectOnholdValidation,"Entered Order Status Delivery");
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();
		
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();

		Reusable.waitForAjax();		

	}
	public void OrderComplete() throws Exception 
	{
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
		click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
		
		verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
		click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.PrimaryTestingMethod,"Primary Testing Method");
		sendKeys(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.PrimaryTestingMethod,"Not Required");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		
        waitForElementToAppear(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.ManualValidation, 10);
		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.ManualValidation,"Manual Validation");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.ManualValidation,"Manual Validation");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatus,"Order Status");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatus,"Order Status");
		clearTextBox(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatus);

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatusDropdown,"Order status drop down");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatusDropdown,"Order status drop down");

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.SelectCompleted,"Completed Status");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.SelectCompleted,"Completed Status");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderComplete,"Order Complete");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderComplete,"Order Complete");
		Reusable.waitForSiebelLoader();
		
		if(isVisible(By.xpath("//span[text()='Ok']"))) {
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
		}
		if(isVisible(By.xpath("//button[text()='Ok']"))) {
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert Accept");
		}
		if(isVisible(By.xpath("//button[@class='colt-primary-btn']"))) {
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
			click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
		}
 		Reusable.waitForSiebelLoader();
		String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus.contains("Progress")) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			waitForElementToAppear(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,10);
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
						
			if(isVisible(By.xpath("//button[text()='Yes']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}	
		System.out.println("Order complete");
		String CompValidation= null;
		CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		
		if(CompValidation.equals("Completed")) 
		{
			Report.LogInfo("verify","<i></b> order status completed", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> order status not competed", "Fail");
		}
		
		Reusable.savePage();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		
		if(isVisible(By.xpath("//button[text()='Ok']"))) {
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert Accept");
		}
		Reusable.waitForSiebelLoader();
	}
	
	public void CircuitReferenceGeneration() throws Exception 
	{
		Random rand = new Random();
		String siteA = null;
		String siteB = null;
		String CircuitRefNumber =null;
		
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	     
		//verifyExists(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickLink.replace("Value", "Sites"));
		//click(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickLink.replace("Value", "Sites"));
		verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites tab");
		click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceAccess,"Circuit Reference Access");
		click(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceAccess,"Circuit Reference Access");
		//refreshPage();
		Reusable.waitForAjax();
		Reusable.waitForAjax();
		Circuitreferencenumber.set(getAttributeFrom(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue,"value"));
		
		if(Circuitreferencenumber.get().isEmpty()==true)
		{
			if(isVisible(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']")))
			{
				     String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");     
				     siteA=siteA1.substring(0, 3);
			}
			if(isVisible(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']")))
			{
			     String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");
				 
			     siteB=siteB1.substring(0, 3);
			}
			if (siteA != null && siteB!= null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitRefNumber = siteA+"/"+siteB+"/"+"LE-"+randnumb;
			}else{
				if(siteA != null)
				{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitRefNumber = siteA+"/"+siteA+"/"+"LE-"+randnumb;
				}else{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitRefNumber = siteB+"/"+siteB+"/"+"LE-"+randnumb;
				}
			
			}
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitRefNumber,"Circuit Reference Number");		
			Reusable.waitForSiebelLoader();
			Robot robot = new Robot();
	        robot.keyPress(KeyEvent.VK_TAB);
	        Reusable.waitForAjax();
	        Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForAjax();
	        Reusable.waitForSiebelLoader();
			Circuitreferencenumber.set(CircuitRefNumber);
		//	Reusable.savePage();
		}
		
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Generated circuit reference No: " + Circuitreferencenumber.get());
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}
	
	public void clickOnManualValidationA() throws Exception {
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();
		//if (isElementPresent(By.xpath("//button[@aria-label='Service Orders:Manual Validation' and not(@disabled='disabled')]"))) {
		if (waitForElementToBeVisible(SiebelManualValidationObj.Validation.manualvalidation1,60)) {
			Reusable.alertPopUp();
			verifyExists(SiebelManualValidationObj.Validation.manualvalidation1,"Manual Validation button");
			ScrollIntoViewByString(SiebelManualValidationObj.Validation.manualvalidation1);
			click(SiebelManualValidationObj.Validation.manualvalidation1,"Manual Validation button");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
//			Reusable.waitToPageLoad();
			Reusable.waitForAjax();
		}
	}
public void EnterInstallationChargeInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		

//		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
//		
//		Reusable.waitForSiebelLoader();
//		if (!ProductName.toString().equals("Cloud Unified Communications") 
//				&& !ProductName.toString().equals("Professional Services")){
			
	//		verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstalltionDropdown, "Installation and Test");
	//		click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstalltionDropdown, "Installation and Test");
			
	//		select(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstalltionDropdown, "Click on Installation Dropdown button and Select Installation and Test");
			
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
			click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
			Reusable.waitForSiebelLoader();
			if (isVisible(By.xpath("//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Installation and Test')]"))) {
			verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
			click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
			}else{
				verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstallationAndTestTab,"Installation and Test");
				click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstallationAndTestTab,"Installation and Test");
			}
					
			Reusable.waitForSiebelLoader();
			
			if (isVisible(By.xpath("//input[@aria-label='Primary Testing Method'][@aria-readonly='false']"))) {
//				if (ProductName.toString().equals("IP Access")||ProductName.toString().equals("Ethernet VPN Access")||ProductName.toString().equals("On Demand Port")||ProductName.toString().equals("IP VPN Service")||ProductName.toString().equals("Ethernet Line")||ProductName.toString().equals("Ethernet Spoke")||ProductName.toString().equals("IP VPN Service")||ProductName.toString().equals("Private Wave Service")||ProductName.toString().equals("Private Ethernet")||ProductName.toString().equals("DCA Ethernets")||ProductName.toString().equals("Ethernet Hub")||ProductName.toString().equalsIgnoreCase("Wave")||ProductName.toString().equals("Ultra Low Latency")||ProductName.toString().equals("DCA Ethernet")){ 
//				verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod,"Not Required");
//				sendKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod, "Not Required");			
//				}else{
					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod,"Not Required");
					sendKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod, "Not Required");
				//}
			
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod, Keys.TAB);
			}
			Reusable.savePage();
			
			Reusable.waitForSiebelLoader();
//			if (!ProductName.toString().equalsIgnoreCase("Wave")
//					&& !ProductName.toString().equalsIgnoreCase("Interconnect")
//					&& !ProductName.toString().equalsIgnoreCase("Ethernet Spoke")
//					&& !ProductName.toString().equalsIgnoreCase("SWIFTNet")
//					&& !ProductName.toString().equalsIgnoreCase("SIP Trunking")
//					&& !ProductName.toString().equalsIgnoreCase("IP VPN Wholesale")
//					&& !ProductName.toString().equalsIgnoreCase("Ethernet Line")) {
			
				if (isVisible(By.xpath("//a[@class='colt-noedit-config-save']"))) {
					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SaveOrderContinue,"Save Order Continue button");
					click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SaveOrderContinue,"Save Order Continue");
					Reusable.waitForSiebelLoader();
				}
			//}
			
			Reusable.alertPopUp();
		//}
		Reusable.alertPopUp();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}

	public void WCAVerification_GCNAccount() throws NoSuchElementException, IOException, InterruptedException
	{
		if(isVisible("@xpath=//div[text()='GCN']"))
		{
			navigateToActivitiesTab();
			Reusable.waitForSiebelLoader();
			int size = SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@summary='Activities']//tr")).size();
			if(size==1)
			{
				Report.LogInfo("WelcomeCallActivity", "WCA is not created for GCN Account", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,  "WCA is not created for GCN Account");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}
			else
			{
				Report.LogInfo("WelcomeCallActivity", "WCA is created for GCN Account", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,  "WCA is not created for GCN Account");
			}
		}
	}

}

