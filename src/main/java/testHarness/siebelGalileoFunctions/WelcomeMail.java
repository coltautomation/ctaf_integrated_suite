package testHarness.siebelGalileoFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.server.handler.FindElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
import pageObjects.siebelGalileoObjects.WelcomeMailObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;

public class WelcomeMail extends SeleniumUtils
{
ReusableFunctions Reusable = new ReusableFunctions();
private static Utilities testSuite = new Utilities();

String Product_Name,ServiceOrderNumber,Footer1_Text,Footer2_Text,PrimaryAddressA_Text, PrimaryAddressB_Text;

	public List verify_Welcomemail_English(String sheetName, String scriptNo,String dataSetNo) throws Exception {
		
	
		ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderNumber");
		String To_Mail = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"To_Mail");
		String Body_Input = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Body_Input");
		String Closing = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Closing");
		String Subverification= DataMiner.fngetcolvalue(sheetName,scriptNo,dataSetNo,"Subverification");
		
//		if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
//			//	System.out.println("Alert Present");
//			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
//			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
//		}
//		Reusable.waitForSiebelLoader();
		
		Reusable.waitForSiebelLoader();
		try 
		{
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		} 
		catch (Exception e) 
		{
			try 
			{
			javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderNumber,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		Reusable.waitForSiebelLoader();
		
		verifyExists(WelcomeMailObj.WelcomeMail.product,"Product Name");
		WebElement Product = findWebElement(WelcomeMailObj.WelcomeMail.product);
		Product_Name = Product.getText();

		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();

		
		MoveToElement(WelcomeMailObj.WelcomeMail.status_Reason);
		verifyExists(WelcomeMailObj.WelcomeMail.status_Reason,"status_Reason");
		click(WelcomeMailObj.WelcomeMail.status_Reason, "status_Reason");
		Reusable.waitForSiebelLoader();	
		
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_F9);
		robot.keyRelease(KeyEvent.VK_F9);
		Reusable.waitForSiebelLoader();	
		
		MoveToElement(WelcomeMailObj.WelcomeMail.orderingContact);
		verifyExists(WelcomeMailObj.WelcomeMail.orderingContact,"Ordering Contact");
		click(WelcomeMailObj.WelcomeMail.orderingContact, "Ordering Contact");
		
		MoveToElement(WelcomeMailObj.WelcomeMail.okBtn);
		verifyExists(WelcomeMailObj.WelcomeMail.okBtn,"OK Button");
		click(WelcomeMailObj.WelcomeMail.okBtn, "OK Button");
		
		verifyExists(WelcomeMailObj.WelcomeMail.toMail);
		click(WelcomeMailObj.WelcomeMail.toMail, "To Mail");
		sendKeys(WelcomeMailObj.WelcomeMail.toMail, To_Mail, "TO Mail");
//		keyPress(WelcomeMailObj.WelcomeMail.toMail, Keys.ENTER);
		
		if(Subverification.contains("English")||Subverification.contains("French")||Subverification.contains("German")||Subverification.contains("Netherland")||Subverification.contains("Japan")||Subverification.contains("IPAccess")||Subverification.contains("Spain"))
		{
		verifyExists(WelcomeMailObj.WelcomeMail.bodyInput,"Body Input");
		click(WelcomeMailObj.WelcomeMail.bodyInput, "Body Input");
		sendKeys(WelcomeMailObj.WelcomeMail.bodyInput, Body_Input, "Body");
		Report.LogInfo("Verification", "Required value is : "+Body_Input, "INFO");
		keyPress(WelcomeMailObj.WelcomeMail.bodyInput, Keys.ENTER);
		
		
		verifyExists(WelcomeMailObj.WelcomeMail.closing,"Closing");
		click(WelcomeMailObj.WelcomeMail.closing, "Closing");
		sendKeys(WelcomeMailObj.WelcomeMail.closing, Closing, "Closing");
		Report.LogInfo("Verification", "Required value is : "+Closing, "INFO");
		keyPress(WelcomeMailObj.WelcomeMail.closing, Keys.ENTER);
		Reusable.waitForSiebelLoader();
		}
		
		
		
		SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().switchTo().frame(0);
//		verifyExists(WelcomeMailObj.WelcomeMail.contents);
//		click(WelcomeMailObj.WelcomeMail.contents);
		Report.LogInfo("Verification", "Switched to Frame", "INFO");
		
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_END);
		robot.keyRelease(KeyEvent.VK_END);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Reusable.waitForSiebelLoader();	
		
		robot.keyPress(KeyEvent.VK_PAGE_UP);
		robot.keyRelease(KeyEvent.VK_PAGE_UP);
		robot.keyPress(KeyEvent.VK_PAGE_UP);
		robot.keyRelease(KeyEvent.VK_PAGE_UP);
		
		
		MoveToElement(WelcomeMailObj.WelcomeMail.footer1);
		verifyExists(WelcomeMailObj.WelcomeMail.footer1);
		WebElement Footer1 = findWebElement(WelcomeMailObj.WelcomeMail.footer1);
		Footer1_Text = Footer1.getText();
		Report.LogInfo("Verification", "Required value is : "+Footer1_Text, "INFO");
		
		MoveToElement(WelcomeMailObj.WelcomeMail.footer2);
		verifyExists(WelcomeMailObj.WelcomeMail.footer2);
		WebElement Footer2 = findWebElement(WelcomeMailObj.WelcomeMail.footer2);
		Footer2_Text = Footer2.getText();
		Report.LogInfo("Verification", "Required value is : "+Footer2_Text, "INFO");
			
//	    if(!MailTemplate.equals("empty")){
//	    	Report.LogInfo("Verification", MailTemplate, "PASS");
//	    	ExtentTestManager.getTest().log(LogStatus.PASS, MailTemplate+" The text area is filled properly");
//			Report.logToDashboard(MailTemplate+" The text area is filled properly");
//	    }
//	    else{
//	    	Report.LogInfo("Verification", "The text area is not filled properly", "PASS");
//	    	ExtentTestManager.getTest().log(LogStatus.FAIL, MailTemplate+" The text area is not filled properly");
//	    	Report.logToDashboard(MailTemplate+" The text area is not filled properly");
//	    }
	    switchToDefaultFrame();
	    Reusable.waitForSiebelLoader();
	    MoveToElement(WelcomeMailObj.WelcomeMail.sendButton);
	    verifyExists(WelcomeMailObj.WelcomeMail.sendButton);
	    Report.LogInfo("Verification", "Send Button is clickable", "INFO");
	    javaScriptclick(WelcomeMailObj.WelcomeMail.sendButton,"Send Button");
	     
	    List<String> EmailContent = new ArrayList();
	    EmailContent.add(ServiceOrderNumber);
	    EmailContent.add(Product_Name);
	    EmailContent.add(Footer1_Text);
	    EmailContent.add(Footer2_Text);
		return EmailContent;

	}
	
	public void MailBox(List mailContent) throws IOException, InterruptedException 
	{				
		String ServiceOrderNumber = (String) mailContent.get(0);
		String Product_Name = (String) mailContent.get(1);
		String Footer1_Text = (String) mailContent.get(2);
		String Footer2_Text = (String) mailContent.get(3);
				
		String driverPath = g.getRelativePath()+"\\Resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",driverPath);

		Map<String,Object> preferences= new HashMap<>();
		preferences.put("profile.default_content_settings.popups", 0);
		preferences.put("download.prompt_for_download", "false");
		preferences.put("download.default_directory", g.getRelativePath()+"\\TestData\\Downloads");

		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", preferences);

		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		//capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		capabilities.setBrowserName("chrome");
		capabilities.setPlatform(Platform.WINDOWS);

		g.setHubAddress("http://localhost:5555/wd/hub");
		
		Thread.sleep(50000);
		
		RemoteWebDriver remoDriver= new RemoteWebDriver(new URL(g.getHubAddress()), capabilities);
		remoDriver.setFileDetector(new LocalFileDetector());
		webDriver = remoDriver;	

		webDriver.get("https://endtest.io/mailbox?email=coltautomation@endtest-mail.io");
		waitForElementToBeVisiblewebDriver("@xpath=////div[text()='Endtest Mailbox']",20,webDriver);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Launched application: " + "https://endtest.io/mailbox?email=coltautomation@endtest-mail.io");
		Report.logToDashboard("Launched application: " + "https://endtest.io/mailbox?email=coltautomation@endtest-mail.io");
		Report.LogInfowebDriver("Application Launch", "Launched application: " + "https://endtest.io/mailbox?email=coltautomation@endtest-mail.io", "PASS",webDriver);
		
		
		
		verifyExistswebDriver(WelcomeMailObj.WelcomeMail.subject,"Subject",webDriver);
		WebElement Received_Mail = findWebElementwebDriver(WelcomeMailObj.WelcomeMail.subject,webDriver);
		String Subject = Received_Mail.getText();
		clickwebDriver(WelcomeMailObj.WelcomeMail.subject, "Subject",webDriver);
		MoveToElementwebDriver(WelcomeMailObj.WelcomeMail.Footer1,webDriver);
		verifyExistswebDriver(WelcomeMailObj.WelcomeMail.Footer1,"Footer",webDriver);
		WebElement Footer = findWebElementwebDriver(WelcomeMailObj.WelcomeMail.Footer1,webDriver);
		String Footer_Text = Footer.getText();
		WebElement Footer2 = findWebElementwebDriver(WelcomeMailObj.WelcomeMail.signature,webDriver);
		String Footer_Sign = Footer2.getText();
		if (Subject.contains(ServiceOrderNumber)&&Subject.contains(Product_Name)&&Footer_Text.contains(Footer1_Text)&&Footer_Sign.contains(Footer2_Text)) {
			 Report.LogInfowebDriver("Verification", "Mail received for the required order", "PASS",webDriver);
			 ExtentTestManager.getTest().log(LogStatus.PASS, "Mail received for the required order");
			 Report.logToDashboard("Mail received for the required order");	
		}
		else{
			Report.LogInfowebDriver("Verification", "Mail not received for the required order", "FAIL",webDriver);
			 ExtentTestManager.getTest().log(LogStatus.FAIL, "Mail not received for the required order");
			 Report.logToDashboard("Mail not received for the required order");
		}
	}
	
	public void getSeperateServer()
	{
		String RunBrowser=null;
		Utilities testSuite = new Utilities();
		String Port = null;
		
		try
		{
			RunScript(g.getRelativePath()+"//Grid","Hub.bat");
			
			RunBrowser = testSuite.getValue("Browsers", "chrome");
			Port = testSuite.getValue("ports", "5555");
			String brwArray[] =RunBrowser.split(",");
			
				String portArray[] =Port.split(",");

				//g.setPortNumber(portArray);

				for(int i=0;i<portArray.length;i++)
				{
					if(automationSuite.equalsIgnoreCase("NMTS Number Management")){
						brwArray[i] = "IE";
					}
//					m.prepareNode(portArray[i],brwArray[i]);
//					RunScript(g.getRelativePath()+"//Grid","node.bat");
					Thread.sleep(3000);
				}								
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public List verifyCarrierHotelCrossConnect(String sheetName, String scriptNo,String dataSetNo) throws IOException, InterruptedException, AWTException {
		ServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderNumber");
		String To_Mail = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"To_Mail");
		String Body_Input = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Body_Input");
		String Closing = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Closing");

		MoveToElement(WelcomeMailObj.WelcomeMail.options);
		verifyExists(WelcomeMailObj.WelcomeMail.options,"Options");
		click(WelcomeMailObj.WelcomeMail.options, "Options");

		Reusable.waitForSiebelLoader();


		mouseOverAction(WelcomeMailObj.WelcomeMail.serviceOrder);
		verifyExists(WelcomeMailObj.WelcomeMail.serviceOrder,"ServiceOrder");
		selectByVisibleText(WelcomeMailObj.WelcomeMail.serviceOrder, "Service Order", "Service Order");

		Reusable.waitForSiebelLoader();

		// try
		// {
		// verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		// click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		// }
		// catch (Exception e)
		// {
		// try
		// {
		// javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		// }
		// catch (Exception e1)
		// {
		// e1.printStackTrace();
		// }
		// }
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderNumber,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();

		MoveToElement(WelcomeMailObj.WelcomeMail.primayAddressA);
		verifyExists(WelcomeMailObj.WelcomeMail.primayAddressA,"Primary Address A");
		WebElement PrimaryAddressA = findWebElement(WelcomeMailObj.WelcomeMail.primayAddressA);
		PrimaryAddressA_Text = PrimaryAddressA.getText();
		Report.LogInfo("Verification", "Text A is :"+PrimaryAddressA_Text, "INFO");
		Reusable.waitForSiebelLoader();

		// MoveToElement(WelcomeMailObj.WelcomeMail.primaryAddressB);
		// verifyExists(WelcomeMailObj.WelcomeMail.primaryAddressB,"Primary Address B");
		// WebElement PrimaryAddressB = findWebElement(WelcomeMailObj.WelcomeMail.primayAddressA);
		// PrimaryAddressB_Text = PrimaryAddressB.getText();
		// Report.LogInfo("Verification", "Text B is: "+PrimaryAddressB_Text, "INFO");
		// Reusable.waitForSiebelLoader();

		MoveToElement(WelcomeMailObj.WelcomeMail.status_Reason);
		verifyExists(WelcomeMailObj.WelcomeMail.status_Reason,"status_Reason");
		click(WelcomeMailObj.WelcomeMail.status_Reason, "status_Reason");
		Reusable.waitForSiebelLoader();

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_F9);
		robot.keyRelease(KeyEvent.VK_F9);
		Reusable.waitForSiebelLoader();

		MoveToElement(WelcomeMailObj.WelcomeMail.orderingContact);
		verifyExists(WelcomeMailObj.WelcomeMail.orderingContact,"Ordering Contact");
		click(WelcomeMailObj.WelcomeMail.orderingContact, "Ordering Contact");

		MoveToElement(WelcomeMailObj.WelcomeMail.okBtn);
		verifyExists(WelcomeMailObj.WelcomeMail.okBtn,"OK Button");
		click(WelcomeMailObj.WelcomeMail.okBtn, "OK Button");

		verifyExists(WelcomeMailObj.WelcomeMail.toMail);
		click(WelcomeMailObj.WelcomeMail.toMail, "To Mail");
		sendKeys(WelcomeMailObj.WelcomeMail.toMail, To_Mail, "TO Mail");

		click(WelcomeMailObj.WelcomeMail.bodyInput, "Body Input");
		sendKeys(WelcomeMailObj.WelcomeMail.bodyInput, Body_Input, "Body");
		Report.LogInfo("Verification", "Required value is : "+Body_Input, "INFO");
		keyPress(WelcomeMailObj.WelcomeMail.bodyInput, Keys.ENTER);


		verifyExists(WelcomeMailObj.WelcomeMail.closing,"Closing");
		click(WelcomeMailObj.WelcomeMail.closing, "Closing");
		sendKeys(WelcomeMailObj.WelcomeMail.closing, Closing, "Closing");
		Report.LogInfo("Verification", "Required value is : "+Closing, "INFO");
		keyPress(WelcomeMailObj.WelcomeMail.closing, Keys.ENTER);
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
		MoveToElement(WelcomeMailObj.WelcomeMail.sendButton);
		verifyExists(WelcomeMailObj.WelcomeMail.sendButton);
		Report.LogInfo("Verification", "Send Button is clickable", "INFO");
		javaScriptclick(WelcomeMailObj.WelcomeMail.sendButton,"Send Button");

		List<String> EmailContent_2 = new ArrayList();
		EmailContent_2.add(PrimaryAddressA_Text);
		// EmailContent_2.add(PrimaryAddressB_Text);
		return EmailContent_2;
		}



		public void MailBox_1(List EmailContent_2, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String Subverification = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Subverification");
		if(Subverification.equals("P2P")||Subverification.equals("IP")){
		String A_End = (String) EmailContent_2.get(0);
		Report.LogInfowebDriver("Verification", "A End Address is :"+A_End, "INFO",webDriver);
		ExtentTestManager.getTest().log(LogStatus.INFO, "A End Address is :"+A_End);
		Report.logToDashboard("A End Address is :"+A_End);
		String StreetA=A_End.split(",")[0];

		// String B_End = (String) EmailContent_2.get(1);
		// Report.LogInfowebDriver("Verification", "A End Address is :"+B_End, "INFO",webDriver);
		// ExtentTestManager.getTest().log(LogStatus.INFO, "A End Address is :"+B_End);
		// Report.logToDashboard("A End Address is :"+B_End);
		// String StreetB=B_End.split(",")[0];

		String driverPath = g.getRelativePath()+"\\Resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",driverPath);



		Map<String,Object> preferences= new HashMap<>();
		preferences.put("profile.default_content_settings.popups", 0);
		preferences.put("download.prompt_for_download", "false");
		preferences.put("download.default_directory", g.getRelativePath()+"\\TestData\\Downloads");



		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", preferences);



		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		//capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		capabilities.setBrowserName("chrome");
		capabilities.setPlatform(Platform.WINDOWS);



		g.setHubAddress("http://localhost:5555/wd/hub");

		Thread.sleep(50000);

		RemoteWebDriver remoDriver= new RemoteWebDriver(new URL(g.getHubAddress()), capabilities);
		remoDriver.setFileDetector(new LocalFileDetector());
		webDriver = remoDriver;



		webDriver.get("https://endtest.io/mailbox?email=coltautomation@endtest-mail.io");
		waitForElementToBeVisiblewebDriver("@xpath=////div[text()='Endtest Mailbox']",20,webDriver);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Launched application: " + "https://endtest.io/mailbox?email=coltautomation@endtest-mail.io");
		Report.logToDashboard("Launched application: " + "https://endtest.io/mailbox?email=coltautomation@endtest-mail.io");
		Report.LogInfowebDriver("Application Launch", "Launched application: " + "https://endtest.io/mailbox?email=coltautomation@endtest-mail.io", "PASS",webDriver);

		verifyExistswebDriver(WelcomeMailObj.WelcomeMail.subject,"Subject",webDriver);
		clickwebDriver(WelcomeMailObj.WelcomeMail.subject, "Subject",webDriver);

		MoveToElementwebDriver(WelcomeMailObj.WelcomeMail.MailAddressA,webDriver);
		verifyExistswebDriver(WelcomeMailObj.WelcomeMail.MailAddressA,"MailAddressA",webDriver);
		WebElement MailAddress_A = findWebElementwebDriver(WelcomeMailObj.WelcomeMail.MailAddressA,webDriver);
		String Address_A = MailAddress_A.getText();
		Report.LogInfowebDriver("Verification", "A End Address is :"+Address_A, "INFO",webDriver);
		ExtentTestManager.getTest().log(LogStatus.INFO, "A End Address is :"+Address_A);
		Report.logToDashboard("A End Address is :"+Address_A);
		String Street1=Address_A.split(",")[0];

		// WebElement MailAddress_B = findWebElementwebDriver(WelcomeMailObj.WelcomeMail.MailAddressB,webDriver);
		// String Address_B = MailAddress_B.getText();
		// Report.LogInfowebDriver("Verification", "B End Address is :"+Address_B, "INFO",webDriver);
		// ExtentTestManager.getTest().log(LogStatus.INFO,"B End Address is :"+Address_B);
		// Report.logToDashboard("B End Address is :"+Address_B);
		// String Street2=Address_B.split(",")[0];

		// if (Street1.contains(StreetA)&&Street1.contains(StreetB)) {
		if(Street1.contains(StreetA)){
		Report.LogInfowebDriver("Verification", "Mail received with the required CHCC information", "PASS",webDriver);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Mail received with the required CHCC information");
		Report.logToDashboard("Mail received with the required CHCC information");
		}
		else{
		Report.LogInfowebDriver("Verification"," Mail not received with the required CHCC information", "FAIL",webDriver);
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Mail not received with the required CHCC information");
		Report.logToDashboard("Mail not received with the required CHCC information");
		}
		}
		}

}

