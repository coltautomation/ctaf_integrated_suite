package testHarness.siebeltoNcFunctions;


import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.siebeltoNCObjects.EvpnOrderObj;
import pageObjects.siebeltoNCObjects.ModifyOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import pageObjects.ncObjects.NewHubOrderObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;

public class CreateEvpnOrder extends SeleniumUtils
{
	public static ThreadLocal<String> SpokeOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> HubOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();

	NcCreateOrder order=new NcCreateOrder();
	ReusableFunctions Reusable=new ReusableFunctions();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();
	
	public void CreatEVPNetworkProductOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{	
		verifyExists(EvpnOrderObj.CompositOrders.NewCompositeOrder,"Click on New Composite Order");
		click(EvpnOrderObj.CompositOrders.NewCompositeOrder,"Click on New Composite Order");
		
		String EVPNetworkProductOrdernumber = getTextFrom(EvpnOrderObj.CompositOrders.NcOrderNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Order_No", EVPNetworkProductOrdernumber);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.OrderDescription,"Order Description Updated");
		sendKeys(EvpnOrderObj.CompositOrders.OrderDescription,"EVPN Network Product Order Created using CTAF Automation script");
			
		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		
		String OrderscreenURL=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Order_URL", OrderscreenURL);
		System.out.println(Orderscreenurl);
		Reusable.waitForAjax();
		
	}
	
	public void AddEVPNNetworkProduct(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		verifyExists(EvpnOrderObj.CompositOrders.OrderTab,"Click on Order Tab");
		click(EvpnOrderObj.CompositOrders.OrderTab,"Click on Order Tab");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.AddonOrderTab,"Click on Add Product Link");
		click(EvpnOrderObj.CompositOrders.AddonOrderTab,"Click on Add Product Link");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.EVPNetworkCheckBox,"Select Ethernt EVPN network product order Option");
		click(EvpnOrderObj.CompositOrders.EVPNetworkCheckBox,"Select Ethernt EVPN network product order Option");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Addbutton,"Click On Add button");
		click(EvpnOrderObj.CompositOrders.Addbutton,"Click On Add button");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.UnderlyningEvpnnetworkOrder,"Open the Under Lying Order");
		click(EvpnOrderObj.CompositOrders.UnderlyningEvpnnetworkOrder,"Open the Under Lying Order");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Edit,"Click On Edit button");
		click(EvpnOrderObj.CompositOrders.Edit,"Click On Edit button");
		Reusable.waitForSiebelLoader();

	}
	
	public void AddEVPNetworkProductdetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String COS=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Class of Service");

		String EVPNNetworkSiebelOrdernumber=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site/SpokeOrderReference_Number");
		String EVPNetworkReference=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NetworkReference_Number");
		String[] orderNo=EVPNNetworkSiebelOrdernumber.split("/");	
		
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.OrderSystemName,"SPARK","OrderSystemName");
		
		Reusable.waitForSiebelLoader();
	
		sendKeys(EvpnOrderObj.CompositOrders.OrderSysSerId,orderNo[0]);
		
		Reusable.waitForSiebelLoader();
		sendKeys(EvpnOrderObj.CompositOrders.SiteOrderNumber,EVPNNetworkSiebelOrdernumber);
		Reusable.waitForSiebelLoader();
		sendKeys(EvpnOrderObj.CompositOrders.Ordersystemetworkid,EVPNetworkReference);
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EvpnOrderObj.CompositOrders.NetworkType,"Data","Network Type");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EvpnOrderObj.CompositOrders.ClassOfService,COS,"Class of Service");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EvpnOrderObj.CompositOrders.LANtype,"EP-LAN","LAN Type");
		Reusable.waitForSiebelLoader();
	
		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click On update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click On update button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
	}
	
	public void ProcessEVPNetworkOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String Ordernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Order_No");
		String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
	}
	
	public void CompleteEVPNetworkOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String Ordernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Order_No");
		String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber,"Ordernumber");
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1);

	    if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+Ordernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Ordernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+Ordernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Ordernumber +" Execution InProgress");
		}
		getUrl(Orderscreenurl);
	
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        Reusable.waitForAjax();
   	}
	
	public void CreatEVPNSubNetworkProductOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{			
		verifyExists(EvpnOrderObj.CompositOrders.NewCompositeOrder,"Click on New Composite Order");
		click(EvpnOrderObj.CompositOrders.NewCompositeOrder,"Click on New Composite Order");
		
		String EVPSubNetworkProductOrdernumber = getTextFrom(EvpnOrderObj.CompositOrders.NcOrderNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_No", EVPSubNetworkProductOrdernumber);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.OrderDescription,"Order Description Updated");
		sendKeys(EvpnOrderObj.CompositOrders.OrderDescription,"EVPN SubNetwork Product Order Created using CTAF Automation script");
			
		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		
		String SubNwOrderscreenURL=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_URL", SubNwOrderscreenURL);
		Reusable.waitForAjax();
	}
	
	public void AddEVPNSubNetworkProduct(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		verifyExists(EvpnOrderObj.CompositOrders.OrderTab,"Click on Order Tab");
		click(EvpnOrderObj.CompositOrders.OrderTab,"Click on Order Tab");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.AddonOrderTab,"Click on Add Product Link");
		click(EvpnOrderObj.CompositOrders.AddonOrderTab,"Click on Add Product Link");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.EVPsubnetworkCheckBox,"Select EVPN Subnetwork product order");
		click(EvpnOrderObj.CompositOrders.EVPsubnetworkCheckBox,"Select EVPN Subnetwork product order");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Addbutton,"Click On Add button");
		click(EvpnOrderObj.CompositOrders.Addbutton,"Click On Add button");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.UnderlyningEvpnsubnetworkOrder,"Open the Under Lying Order");
		click(EvpnOrderObj.CompositOrders.UnderlyningEvpnsubnetworkOrder,"Open the Under Lying Order");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Edit,"Click On Edit button");
		click(EvpnOrderObj.CompositOrders.Edit,"Click On Edit button");
		Reusable.waitForSiebelLoader();
	}
	
	public void AddEVPNSubnetworkProductdetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String EVPNetworkReference=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NetworkReference_Number");
		
		/*if (Inputdata[0].toString().contains("Existing"))
		{
			EVPNNetworkSiebelOrdernumber.set(Inputdata[1].toString());
			EVPNetworkReference.set(Inputdata[2].toString());
		}*/
		
		sendKeys(EvpnOrderObj.CompositOrders.Ordersystemetworkid,EVPNetworkReference);
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.Ordersystemsubnetworkid,EVPNetworkReference+"-S1");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click On update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click On update button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
	}

	public void ProcessEVPNSubnetworkOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String SubNwOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_No");
		String SubNwOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
		
		getUrl(SubNwOrderscreenurl);
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SubNwOrdernumber,"SubNwOrdernumber");
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
	}
	
	
	public void CompleteEVPNSubnetworkOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String SubNwOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_No");
		String SubNwOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
		
		getUrl(SubNwOrderscreenurl);
		
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SubNwOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ SubNwOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ SubNwOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);

	    if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+SubNwOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, SubNwOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+SubNwOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, SubNwOrdernumber +" Execution InProgress");
		}
		getUrl(SubNwOrderscreenurl);
	
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        Reusable.waitForAjax();
   	}
	
	public void CreateEVPNOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{			
		verifyExists(EvpnOrderObj.CompositOrders.NewCompositeOrder,"Click on New Composite Order");
		click(EvpnOrderObj.CompositOrders.NewCompositeOrder,"Click on New Composite Order");
		
		String EVPDummyOrdernumber = getTextFrom(EvpnOrderObj.CompositOrders.NcOrderNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Site_Order_No", EVPDummyOrdernumber);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.OrderDescription,"Order Description Updated");
		sendKeys(EvpnOrderObj.CompositOrders.OrderDescription,"EVPN 1-1 Dummy Order created using CTAF Automation script");
			
		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		
		String DummyOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Site_Order_URL", DummyOrderscreenurl);
		Reusable.waitForAjax();
	}
	
	public void AddProduct(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String DummyOrderScreenURL=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site_Order_URL");

		getUrl(DummyOrderScreenURL);
		verifyExists(EvpnOrderObj.CompositOrders.OrderTab,"Click on Order Tab");
		click(EvpnOrderObj.CompositOrders.OrderTab,"Click on Order Tab");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.AddonOrderTab,"Click on Add Product Link");
		click(EvpnOrderObj.CompositOrders.AddonOrderTab,"Click on Add Product Link");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.EthernetProductCheckBox,"EthernetProductCheckBox");
		click(EvpnOrderObj.CompositOrders.EthernetProductCheckBox,"EthernetProductCheckBox");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Addbutton,"Click On Add button");
		click(EvpnOrderObj.CompositOrders.Addbutton,"Click On Add button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();

	}
	
	public void AddProductdetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
	String EVPNetworkReference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NetworkReference_Number");
	String LinkType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Link_Type");	
	String COSApplicable = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Class of Service");
	String COSClassification = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "COS Classification");
	String Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service_Bandwidth1");
	String EVPNNetworkSiebelOrdernumber=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site/SpokeOrderReference_Number");

	String[] orderNo=EVPNNetworkSiebelOrdernumber.split("/");	
	
		/*if (Inputdata[0].toString().contains("Existing"))
		{
			EVPNNetworkSiebelOrdernumber.set(Inputdata[1].toString());
			EVPNetworkReference.set(Inputdata[2].toString());
			EVPNAccessSiebelsubnetworkid.set(Inputdata[3].toString());
		}*/
		

		
		verifyExists(EvpnOrderObj.CompositOrders.UnderlyningOrder,"Open the Under Lying Ethernet Order");
		click(EvpnOrderObj.CompositOrders.UnderlyningOrder,"Open the Under Lying Ethernet Order");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Edit,"Click on Edit button");
		click(EvpnOrderObj.CompositOrders.Edit,"Click on Edit button");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.NetworkReference,EVPNetworkReference,"NetworkReference");
		Reusable.waitForSiebelLoader();
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.EvpnLinkType,LinkType,"EvpnLinkType");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EvpnOrderObj.CompositOrders.OrderSystemName,"SPARK","OrderSystemName");
		Reusable.waitForSiebelLoader();
	
		verifyExists(EvpnOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
		click(EvpnOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
		sendKeys(EvpnOrderObj.CompositOrders.ServiceBandwidth,Bandwidth);
		Reusable.SendkeaboardKeys((EvpnOrderObj.CompositOrders.ServiceBandwidth),Keys.ENTER);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.CircuitCategory,"CircuitCategory");
		click(EvpnOrderObj.CompositOrders.CircuitCategory,"CircuitCategory");
		selectByVisibleText(EvpnOrderObj.CompositOrders.CircuitCategory,"LE","Circuit Category");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.OrderSysSerId,orderNo[0],"OrderSysSerId");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.SiteOrderNumber,EVPNNetworkSiebelOrdernumber);
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.CommercialProductName,"ELAN");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.Ordersystemetworkid,EVPNetworkReference);
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.Ordersystemsubnetworkid,EVPNetworkReference+"-S1");
		Reusable.waitForSiebelLoader();	
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.ClassOfService,COSApplicable,"Class of Service");
		Reusable.waitForSiebelLoader();	
		System.out.println(COSApplicable);
		if (COSApplicable.toString().equalsIgnoreCase("Yes"))
		{
			selectByVisibleText(EvpnOrderObj.CompositOrders.cosClassification, COSClassification, "Cos Classification");
		}
		Reusable.waitForSiebelLoader();
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.Topology,"Any to Any","Topology");
		Reusable.waitForSiebelLoader();
		
		

		verifyExists(EvpnOrderObj.CompositOrders.Update,"Update");
		click(EvpnOrderObj.CompositOrders.Update,"Update");
		Reusable.waitForSiebelLoader();
	}
	
	public void DecomposeEVPNOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
        String DummyOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Order_URL");
        
		getUrl(DummyOrderscreenurl);
		
        verifyExists(NcOrderObj.generalInformation.Suborder,"Ethernet Connection Product Order");
        click(NcOrderObj.generalInformation.Suborder,"Ethernet Connection Product Order");
       
        verifyExists(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        click(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        waitForAjax();
        Reusable.waitForpageloadmask();
	}
	
	public void EndSiteDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{ 
		String AccessTechnology=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Access_Technology/ Primary");
		String AccessType=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Access_Type/ Primary");
		String MaxMACLimitExceed=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Max MAC Limit Exceed");
		String SiteID=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Site_ID");
		String PortRole=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Port_Role");
		String VlanTaggingMode = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tagging_Mode");
		String VlanTagId = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tag_ID");
        String DummyOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Order_URL");
		String ResilienceOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String Broadcast_Limit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Broadcast Limit");
		String Unknown_Unicast_Limit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Multicast Limit");
		String Multicast_Limit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Unknown Unicast Limit");
		String Max_MAC_Limit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Max MAC Limit");
		getUrl(DummyOrderscreenurl);
		
		verifyExists(EvpnOrderObj.CompositOrders.NewEndSiteProductAend,"NewEndSiteProductAend");
		click(EvpnOrderObj.CompositOrders.NewEndSiteProductAend,"NewEndSiteProductAend");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Edit,"Edit");
		click(EvpnOrderObj.CompositOrders.Edit,"Edit");
		Reusable.waitForSiebelLoader();
		
		verifyExists(NcOrderObj.addEndSiteDetails.ResilienceOption,"Resilience Option");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.ResilienceOption,ResilienceOption,"ResilienceOption");
		
		verifyExists(NcOrderObj.addEndSiteDetails.AccessTechnology,"Access Technology");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.AccessTechnology,AccessTechnology,"AccessTechnology");

		verifyExists(NcOrderObj.addEndSiteDetails.AccessType,"Access Type");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.AccessType,AccessType,"AccessType");

		verifyExists(NcOrderObj.addEndSiteDetails.SiteEnd,"Site End");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.SiteEnd,"A END","A End");

		verifyExists(NcOrderObj.addEndSiteDetails.SiteID,"Site ID");
		sendKeys(NcOrderObj.addEndSiteDetails.SiteID,SiteID);
		
		verifyExists(EvpnOrderObj.CompositOrders.EvpnLinkUsage,"Site End");
		selectByVisibleText(EvpnOrderObj.CompositOrders.EvpnLinkUsage,"EOAM-UNI","EvpnLinkUsage");

		sendKeys(EvpnOrderObj.CompositOrders.BroadcastLimit,Broadcast_Limit,"Broadcast Limit");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.MultiCastLimit,Multicast_Limit,"Multicast Limit");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.UnknownUnicastLimit,Unknown_Unicast_Limit,"Unknown Unicast Limit");
		Reusable.waitForSiebelLoader();
		
		sendKeys(EvpnOrderObj.CompositOrders.Maxmaclimit,Max_MAC_Limit,"Max MAC Limit");
		Reusable.waitForSiebelLoader();
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.MaxMACLimitExceed,MaxMACLimitExceed,"MaxMACLimitExceed");
		Reusable.waitForSiebelLoader();

		verifyExists(EvpnOrderObj.CompositOrders.Update,"Required details are Updated");
		click(EvpnOrderObj.CompositOrders.Update,"Required details are Updated");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.GeneralInformationTab,"Navigate to General Information Tab");
		click(EvpnOrderObj.CompositOrders.GeneralInformationTab,"Navigate to General Information Tab");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.AccessPortLink,"Click on Access Port Link");
		click(EvpnOrderObj.CompositOrders.AccessPortLink,"Click on Access Port Link");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Edit,"Click on Edit button");
		click(EvpnOrderObj.CompositOrders.Edit,"Click on Edit button");
		Reusable.waitForSiebelLoader();
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.PresentConnectType,"LC/PC","Presentation Connetory Type");
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.AccessportRole,PortRole,"Port Role");

		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		click(EvpnOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.GeneralInformationTab,"Click to General Information Tab");
		click(EvpnOrderObj.CompositOrders.GeneralInformationTab,"Click to General Information Tab");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.CPElink,"Click on CPE Link");
		click(EvpnOrderObj.CompositOrders.CPElink,"Click on CPE Link");
		Reusable.waitForSiebelLoader();

		verifyExists(EvpnOrderObj.CompositOrders.Edit,"Click on Edit button");
		click(EvpnOrderObj.CompositOrders.Edit,"Click on Edit button");
		Reusable.waitForSiebelLoader();

		sendKeys(EvpnOrderObj.CompositOrders.CabinetID,"43");
		Reusable.waitForSiebelLoader();
		
		selectByVisibleText(EvpnOrderObj.CompositOrders.CabinetType,"Existing Colt Cabinet","Cabinet Type");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		click(EvpnOrderObj.CompositOrders.Update,"Click on Update button");
		Reusable.waitForSiebelLoader();

		Reusable.waitForAjax();
	}	
	
	public void ProcessEVPNDummyOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String EVPDummyOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site_Order_No");
		String DummyOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Order_URL");
		
		getUrl(DummyOrderscreenurl);
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,EVPDummyOrdernumber,"EVPDummyOrdernumber");
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
	}

	public void CompleteDummyOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo, String type) throws IOException, InterruptedException, Exception
	{
		String EVPDummyOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site_Order_No");
		String DummyOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Order_URL");
        String WorkItems = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Work Items");
        
		getUrl(DummyOrderscreenurl);
	    
        verifyExists(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
        click(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
       
        verifyExists(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        click(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        waitForAjax();
   
        verifyExists(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        click(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        waitForAjax();
       
        for (int k=1; k<=Integer.parseInt(WorkItems);k++)
        {
        	waitRefreshForElementToAppear(NcOrderObj.workItems.TaskReadytoComplete,90,20000);
            click(NcOrderObj.workItems.TaskReadytoComplete,"Workitem in Ready status");
            //need to verify
            CompletworkitemEVPN(GetText(NcOrderObj.taskDetails.TaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);         
        }
       
        if(!type.equalsIgnoreCase("Cancel"))
        {	
        verifyExists(NcOrderObj.taskDetails.TaskTab,"TaskTab");
		click(NcOrderObj.taskDetails.TaskTab,"TaskTab");
		Reusable.waitForSiebelLoader();	
		
		click("@xpath=//a/span[text()='Set OAM Management Leg']/parent::*/parent::*/preceding-sibling::*//span[contains(text(),'New E-LAN Leg CFS Order')]","Set OAM Management Leg");
		verifyExists(NcOrderObj.taskDetails.OrderParameters,"OrderParameters");
		click(NcOrderObj.taskDetails.OrderParameters,"OrderParameters");
		Reusable.waitForSiebelLoader();	
		
		verifyExists(NcOrderObj.taskDetails.CFSInstance,"OrderParameters");
		click(NcOrderObj.taskDetails.CFSInstance,"OrderParameters");
		Reusable.waitForSiebelLoader();
		
		String CFSInstanceURL= getURLFromPage();
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CFSInstanceUrl", CFSInstanceURL);
        }
        
		getUrl(DummyOrderscreenurl);
       
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,EVPDummyOrdernumber,"EVPDummyOrdernumber");
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

        if(type.equalsIgnoreCase("Cancel"))
        {
        	verifyExists(EthernetOrderObj.CompositOrders.Checkbox.replace("Value", EVPDummyOrdernumber),"Checkbox");
    		click(EthernetOrderObj.CompositOrders.Checkbox.replace("Value", EVPDummyOrdernumber),"Checkbox");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Cancel,"Cancel Button");
    		click(EthernetOrderObj.CompositOrders.Cancel,"Cancel Button");
    		
    		Reusable.waitForSiebelLoader();
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Execute,"Execute Button");
    		click(EthernetOrderObj.CompositOrders.Execute,"Execute Button");
    		
    		Reusable.waitForSiebelLoader();
    		Reusable.waitForSiebelLoader();

    		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
            click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
           
    		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    		
    		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
    		
    		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,EVPDummyOrdernumber,"EVPDummyOrdernumber");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
    		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
    		
        	waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ EVPDummyOrdernumber + EthernetOrderObj.CompositOrders.arrorder5,180,20000);
    	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ EVPDummyOrdernumber + EthernetOrderObj.CompositOrders.arrorder5);

    	    if (OrderStatus.contains("Processing Cancelled"))
    		{
    			Report.LogInfo("Validate","\""+EVPDummyOrdernumber +"\" Is Completed Successfully", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, EVPDummyOrdernumber +" Is Completed Successfully");
    		}
    		else if (OrderStatus.contains("Process Started"))
    		{
    			Report.LogInfo("Validate","\""+EVPDummyOrdernumber +"\" Execution InProgress", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, EVPDummyOrdernumber +" Execution InProgress");
    		}
        }

        else
        {
        	waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ EVPDummyOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,180,20000);
    	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ EVPDummyOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);

    	    if (OrderStatus.contains("Process Completed"))
    		{
    			Report.LogInfo("Validate","\""+EVPDummyOrdernumber +"\" Is Completed Successfully", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, EVPDummyOrdernumber +" Is Completed Successfully");
    		}
    		else if (OrderStatus.contains("Process Started"))
    		{
    			Report.LogInfo("Validate","\""+EVPDummyOrdernumber +"\" Execution InProgress", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, EVPDummyOrdernumber +" Execution InProgress");
    		}
        }
		
	}
	
	public void productSearchOrderEVPN(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String TechnicalOrderServiceid=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "TechnicalServiceid_Spoke");
		
		Reusable.waitForSiebelLoader();
		verifyExists(EvpnOrderObj.CompositOrders.Fastsearch,"Click on Document Link");
		click(EvpnOrderObj.CompositOrders.Fastsearch,"Click on Document Link");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.ServiceInventorySearch,"COLT Service Inventory Search Folder link");
		mouseMoveOn(EvpnOrderObj.CompositOrders.ServiceInventorySearch);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.ProductOrderSearch,"Click on COLT Product Order Search Profile Link");
		click(EvpnOrderObj.CompositOrders.ProductOrderSearch,"Click on COLT Product Order Search Profile Link");
		Reusable.waitForSiebelLoader();
	
		verifyExists(EvpnOrderObj.CompositOrders.ServiceIDsearchcriterion,"Click on COLT Product Order Search Profile Link");
		sendKeys(EvpnOrderObj.CompositOrders.ServiceIDsearchcriterion,TechnicalOrderServiceid);
		keyPress((EvpnOrderObj.CompositOrders.ServiceIDsearchcriterion),Keys.TAB);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.searchButton,"Click on search button");
		click(EvpnOrderObj.CompositOrders.searchButton,"Click on search button");
		Reusable.waitForSiebelLoader();
		
		String EvpnOrderNo = getTextFrom(EvpnOrderObj.CompositOrders.OrderNolink);
		DataMiner.fnsetcolvalue( SheetName, scriptNo, dataSetNo, "Endpoint_Order_No", EvpnOrderNo);
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.OrderNolink,"Click on search button");
		click(EvpnOrderObj.CompositOrders.OrderNolink,"Click on search button");
		Reusable.waitForSiebelLoader();
		
		String EvpnOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "Endpoint_Order_URL", EvpnOrderscreenurl);
		System.out.println(Orderscreenurl);
		Reusable.waitForAjax();
	}

	public void CompleteOrderEVPN(String testDataFile,String sheetName,String scriptNo,String dataSetNo, String OrderType) throws Exception
	{
		switch (OrderType)
		{
		case "New":
		String EvpnOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		String EvpnOrderNo = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Endpoint_Order_No");
		String Workitems = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke Work item");
		String Flag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Scenario_Name");

	 	String[] arrOfStr = EvpnOrderNo.split("#", 0); 
	    getUrl(EvpnOrderscreenurl);
	    
	 	
	 	verifyExists(EvpnOrderObj.CompositOrders.TaskTab,"TaskTab");
		click(EvpnOrderObj.CompositOrders.TaskTab,"TaskTab");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.ExecutionFlowlink,"ExecutionFlowlink");
		click(EvpnOrderObj.CompositOrders.ExecutionFlowlink,"ExecutionFlowlink");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.Workitems,"Workitems");
		click(EvpnOrderObj.CompositOrders.Workitems,"Workitems");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Thread.sleep(5000);
		for (int k=1; k<=Integer.parseInt(Workitems);k++)
		{
			waitRefreshForElementToAppear(EvpnOrderObj.CompositOrders.TaskReadytoComplete,90,20000);
	
			verifyExists(EvpnOrderObj.CompositOrders.TaskReadytoComplete,"TaskReadytoComplete");
			click(EvpnOrderObj.CompositOrders.TaskReadytoComplete,"TaskReadytoComplete");
			CompletworkitemEVPN(GetText(NcOrderObj.taskDetails.TaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);   
		}
		getUrl(EvpnOrderscreenurl);
	
		verifyExists(EvpnOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		click(EvpnOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		Reusable.waitForSiebelLoader();
		
		verifyExists(EvpnOrderObj.CompositOrders.AccountNameSorting,"AccountNameSorting");
		click(EvpnOrderObj.CompositOrders.AccountNameSorting,"AccountNameSorting");
		Reusable.waitForSiebelLoader();
		if(!Flag.contains("Cancel"))
		{
		waitRefreshForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",180,20000);
		}
		break;
        
		case "Modify":
			 EvpnOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Modify_Order_No");
			 EvpnOrderNo = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Modify_Order_URL");
			 Workitems = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Modify Work Items");
		        
		        
				
			 	 arrOfStr = EvpnOrderNo.split("#", 0); 
			    getUrl(EvpnOrderscreenurl);
			    
			 	
			 	verifyExists(EvpnOrderObj.CompositOrders.TaskTab,"TaskTab");
				click(EvpnOrderObj.CompositOrders.TaskTab,"TaskTab");
				Reusable.waitForSiebelLoader();
				
				verifyExists(EvpnOrderObj.CompositOrders.ExecutionFlowlink,"ExecutionFlowlink");
				click(EvpnOrderObj.CompositOrders.ExecutionFlowlink,"ExecutionFlowlink");
				Reusable.waitForSiebelLoader();
				
				verifyExists(EvpnOrderObj.CompositOrders.Workitems,"Workitems");
				click(EvpnOrderObj.CompositOrders.Workitems,"Workitems");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				Thread.sleep(5000);
				for (int k=1; k<=Integer.parseInt(Workitems);k++)
				{
					waitRefreshForElementToAppear(EvpnOrderObj.CompositOrders.TaskReadytoComplete,60,20000);

					verifyExists(EvpnOrderObj.CompositOrders.TaskReadytoComplete,"TaskReadytoComplete");
					click(EvpnOrderObj.CompositOrders.TaskReadytoComplete,"TaskReadytoComplete");
					CompletworkitemEVPN(GetText(NcOrderObj.taskDetails.TaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);   
				}
				
				getUrl(EvpnOrderscreenurl);
				
				
					
				verifyExists(EvpnOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
				click(EvpnOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
				Reusable.waitForSiebelLoader();
				
				verifyExists(EvpnOrderObj.CompositOrders.AccountNameSorting,"AccountNameSorting");
				click(EvpnOrderObj.CompositOrders.AccountNameSorting,"AccountNameSorting");
				Reusable.waitForSiebelLoader();
					
				waitRefreshForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",90,20000);
				
		break;
	
		}
}
	
	public void CompletworkitemEVPN(String [] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String OAM_profile = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "OAM profile");
		String Service_profile = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service profile");
        String Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service_Bandwidth1");
        String BMNormal = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BM Normal %");
        String BMPriority = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BM Priority %");
        String MaxMACLimit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Max MAC Limit");
        String MaxMACLimitExceed = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Max MAC Limit Exceed");
        String BroadcastLimit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Broadcast Limit");
        String MulticastLimi = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Multicast Limi");
        String UnknownUnicastLimit = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Unknown Unicast Limit");
        String SiteID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SiteID");       
        String PeNwElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
        String PEPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
        String PEPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
        String VcxControl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend VCX Control");
        String Beacon = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Beacon");
        String PrimarySR = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Primary SR");
        String PrimarySRGIL = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Primary SR GIL");
        String OLONNIProfile = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "OLO NNI Profile");
        String SerialNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SerialNumber");
        String AEndResilienceOption= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
        String AendAccessNWElement= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end Access NW Element");
        String AendAccessport= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end Access port");
        String AendCPENNITrunkPort1= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
        String AendCPENNITrunkPort2= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
        String BEndResilienceOption=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
        String BendAccessNWElement= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B end Access NW Element");
        String BendAccessport= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B end Access port");
        String BendCPENNITrunkPort1=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 1");
        String BendCPENNITrunkPort2= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 2");
        String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Type of Device");
        String OamMonitoringLeg = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CFSInstanceUrl");	
			
		System.out.println("In Switch case with TaskName :"+taskname[0]);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Start completion of Task: "+taskname[0]);
		if(isVisible(NcOrderObj.workItems.DescriptionPopup))
		click(NcOrderObj.workItems.DescriptionPopup,"DescriptionPopup");
		
		switch(taskname[0])
		{
		case "Set OAM Management Leg":
		{
			verifyExists(EvpnOrderObj.CompositOrders.OAMProfile,"OAMProfile");
			sendKeys(EvpnOrderObj.CompositOrders.OAMProfile,OAM_profile);
			Reusable.waitForSiebelLoader();
			verifyExists(EvpnOrderObj.CompositOrders.oammonitoringleg,"oammonitoringleg");
			sendKeys(EvpnOrderObj.CompositOrders.oammonitoringleg,OamMonitoringLeg);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			
			Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.oammonitoringleg, Keys.ENTER);
			Reusable.waitForSiebelLoader();

			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;

		case "Set Bandwidth Distribution Parameters":
		{
			verifyExists(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			click(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			Reusable.waitForSiebelLoader();

			/*	getwebelement(xml.getlocator("//locators/PremiumCir")).clear();
			SendKeys(getwebelement(xml.getlocator("//locators/PremiumCir")),Inputdata[68].toString());

			getwebelement(xml.getlocator("//locators/InternetCir")).clear();
			SendKeys(getwebelement(xml.getlocator("//locators/InternetCir")),Inputdata[69].toString());

			Clickon(getwebelement(xml.getlocator("//locators/Update")));

			Clickon(getwebelement(xml.getlocator("//locators/Edit")));
			Thread.sleep(10000);
			 */
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;
		
		case "Service Profile Confirmation":
		{
			clearTextBox(EvpnOrderObj.CompositOrders.Serviceprofile);
			Reusable.waitForSiebelLoader();
			
			sendKeys(EvpnOrderObj.CompositOrders.Serviceprofile,Service_profile );
			Reusable.waitForSiebelLoader();
	
			Reusable.waitForSiebelLoader();
			Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.Serviceprofile, Keys.ENTER);
			
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}	
		break;

		case "Check/Update Storm Control Parameters":
		{
			
			String band_width[]=Bandwidth.toString().split(" ");
			System.out.println("Iam in switch case"+band_width[0]);
			System.out.println("bw1"+band_width[1]);

			int value=Integer.parseInt(band_width[0]);
			if(value>1 && band_width[1].contentEquals("Gbps"))
			{
				verifyExists(EvpnOrderObj.CompositOrders.peportdedicatedforStormControl,"peportdedicatedforStormControl");
				select(EvpnOrderObj.CompositOrders.peportdedicatedforStormControl,"True");
				
			}
			else
			{
				verifyExists(EvpnOrderObj.CompositOrders.peportdedicatedforStormControl,"peportdedicatedforStormControl");
				select(EvpnOrderObj.CompositOrders.peportdedicatedforStormControl,"False");
				
			}
		
		
			if (BMNormal.contains("Default"))
			{
				clearTextBox(EvpnOrderObj.CompositOrders.BMNormal);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.BMNormal,BMNormal );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.BMNormal, Keys.ENTER);
			}

			if(BMPriority.contains("Default"))
			{
				
				clearTextBox(EvpnOrderObj.CompositOrders.BMPriority);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.BMPriority,BMPriority );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.BMPriority, Keys.ENTER);
				
			}

			if(MaxMACLimit.contains("Default"))
			{
				clearTextBox(EvpnOrderObj.CompositOrders.Maxmaclimit);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.Maxmaclimit,MaxMACLimit );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.Maxmaclimit, Keys.ENTER);
				
			}

			if(MaxMACLimitExceed.contains("Default"))
			{
				clearTextBox(EvpnOrderObj.CompositOrders.MaxMACLimitExceed);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.MaxMACLimitExceed,MaxMACLimitExceed );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.MaxMACLimitExceed, Keys.ENTER);
			}
			
			if(BroadcastLimit.contains("Default"))
			{
				clearTextBox(EvpnOrderObj.CompositOrders.BroadcastLimit);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.BroadcastLimit,BroadcastLimit );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.BroadcastLimit, Keys.ENTER);
				
			}
			
			if(MulticastLimi.contains("Default"))
			{
				clearTextBox(EvpnOrderObj.CompositOrders.MultiCastLimit);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.MultiCastLimit,MulticastLimi );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.MultiCastLimit, Keys.ENTER);
				
			}
			
			if(UnknownUnicastLimit.contains("Default"))
			{
				clearTextBox(EvpnOrderObj.CompositOrders.UnknownUnicastLimit);
				Reusable.waitForSiebelLoader();
			
				sendKeys(EvpnOrderObj.CompositOrders.UnknownUnicastLimit,UnknownUnicastLimit );
				Reusable.waitForSiebelLoader();
		
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.UnknownUnicastLimit, Keys.ENTER);
				
		
			}
			
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;

		case "Review E-LAN Leg Parameters":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break; 

		case "Inprogress Order Verify Task":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;       

	    case "New Mngm Network Connection Activation":
	    {
             verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
             click(NcOrderObj.workItems.Complete,"Complete Workitem");
             Reusable.waitForAjax();
             if(DeviceType.equalsIgnoreCase("Stub"))
             {
             	if (AendAccessNWElement.contains("SC") || BendAccessNWElement.contains("SC"))
             	{
             		System.out.println("Not an Stub GX/LTS Device");
             	}
             	else
             	{
             		ErrorWorkItems.errorWorkitems("EIP: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
             		ErrorWorkItems.checkipaddress("Check IP address in DNS",testDataFile, sheetName, scriptNo, dataSetNo);
             	}
             }
	    }
        break;      

		case "Test Resilience":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;

		 case "Reserve Access Resources":
		 {		
//				verifyExists(EthernetOrderObj.CompositOrders.OrderDifferenceTab,"Order Difference Tab");
//				click(EthernetOrderObj.CompositOrders.OrderDifferenceTab,"Order Difference Tab");
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				
				
				verifyExists(EthernetOrderObj.CompositOrders.ServiceInfoTab,"Service Information Tab");
				click(EthernetOrderObj.CompositOrders.ServiceInfoTab,"Service Information Tab");
				
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				
				String EVPN_LinkUsage = getTextFrom(EthernetOrderObj.CompositOrders.EVPNLinkUsage,"EVPNLinkUsage");
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				
				Reusable.waitForAjax();
				System.out.println(EVPN_LinkUsage);
				
				if(EVPN_LinkUsage.equalsIgnoreCase("EOAM-UNI"))
				{
					if(AEndResilienceOption.equalsIgnoreCase("Protected"))
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AendAccessNWElement);
						
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						if(!AendAccessNWElement.contains("SC"))
						{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//							clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//							clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,AendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//							clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,AendCPENNITrunkPort2);
						waitForAjax();
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						}
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//							workitemcounter.set(workitemcounter.get()+1);
					}
					else
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						waitForAjax();
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AendAccessNWElement);
						waitForAjax();							
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();

						if(!AendAccessNWElement.contains("SC"))
						{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						waitForAjax();
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AendAccessport);
						waitForAjax();
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//							clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,AendCPENNITrunkPort1);
						waitForAjax();
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						}
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//							workitemcounter.set(workitemcounter.get()+1);
					}
				}
				else 
				{
					if(BEndResilienceOption.equalsIgnoreCase("Protected"))
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,BendAccessNWElement);
						
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						if(!BendAccessNWElement.contains("SC"))
						{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//							clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,BendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//							clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,BendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//							clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,BendCPENNITrunkPort2);
						waitForAjax();
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						}
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//							workitemcounter.set(workitemcounter.get()+1);
					}
					else
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,BendAccessNWElement);
						
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						if(!BendAccessNWElement.contains("SC"))
						{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//							clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,BendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//							clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,BendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						}
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//							workitemcounter.set(workitemcounter.get()+1);
					}
				}
		 }
		 break;


      case "Transport Circuit Design":
      {
      if (AEndResilienceOption.contains("Protected"))
      {
             //Code for Protected
             verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
//           clearTextBox(NcOrderObj.workItems.PENetworkElement);
             sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
             Reusable.waitForAjax();
             Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
             Reusable.waitForAjax();
             
             if (DeviceType.contains("Stub"))
             {
                    if (AendAccessNWElement.contains("SC"))
                    {
                           verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
                           clearTextBox(NcOrderObj.workItems.VCXController);
                           sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
                          Reusable.waitForAjax();
                           Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
                           Reusable.waitForSiebelSpinnerToDisappear();
                    }                   
//                                      if (Beacon.contains("Beacon"))
//                                      {
//                                             verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//                                             clearTextBox(NcOrderObj.workItems.Beacon);
//                                             sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//                                            Reusable.waitForAjax();
//                                             Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//                                             Reusable.waitForSiebelSpinnerToDisappear();
//                                      }
             }                                       
             verifyExists(NcOrderObj.createOrder.Update,"Update button");
             click(NcOrderObj.createOrder.Update,"Update button");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

         verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
             click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
             Reusable.waitForAjax();

            verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
             click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
             
             verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
             click(NcOrderObj.addProductDetails.Edit,"Edit button");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//           clearTextBox(NcOrderObj.workItems.PePort);
             sendKeys(NcOrderObj.workItems.PePort,PEPort1);
             Reusable.waitForAjax();
             Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
             Reusable.waitForSiebelSpinnerToDisappear();

             verifyExists(NcOrderObj.createOrder.Update,"Update button");
             click(NcOrderObj.createOrder.Update,"Update button");
             Reusable.waitForAjax();
             
             verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
             click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

         verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
             click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
             Reusable.waitForAjax();

            verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
             click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
             
             verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
             click(NcOrderObj.addProductDetails.Edit,"Edit button");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//           clearTextBox(NcOrderObj.workItems.PePort);
             sendKeys(NcOrderObj.workItems.PePort,PEPort2);
             Reusable.waitForAjax();
             Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
             Reusable.waitForSiebelSpinnerToDisappear();

             verifyExists(NcOrderObj.createOrder.Update,"Update button");
             click(NcOrderObj.createOrder.Update,"Update button");
             Reusable.waitForAjax();
             
             verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
             click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

             verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
             click(NcOrderObj.addProductDetails.Edit,"Edit button");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
             click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
             Reusable.waitForAjax();
      }
      else
      {
      //Code for Unprotected
             verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             Reusable.waitForAjax();

             verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
//           clearTextBox(NcOrderObj.workItems.PENetworkElement);
             sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
             Reusable.waitForAjax();
             Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
             Reusable.waitForSiebelSpinnerToDisappear();
             
             verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//           clearTextBox(NcOrderObj.workItems.PePort);
             sendKeys(NcOrderObj.workItems.PePort,PEPort1);
             Reusable.waitForAjax();
             Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
             Reusable.waitForAjax();
             Reusable.waitForpageloadmask();

             if (DeviceType.contains("Stub"))
             {
                    if (AendAccessNWElement.contains("SC"))
                    {
                           verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
                           clearTextBox(NcOrderObj.workItems.VCXController);
                           sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
                          Reusable.waitForAjax();
                           Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
                           Reusable.waitForSiebelSpinnerToDisappear();
                    }
//                                      if (Beacon.contains("Beacon"))
//                                      {
//                                             verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//                                             clearTextBox(NcOrderObj.workItems.Beacon);
//                                             sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//                                            Reusable.waitForAjax();
//                                             Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//                                             Reusable.waitForSiebelSpinnerToDisappear();
//                                      }
             }                                       
             verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
             click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

             verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
             click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
             Reusable.waitForAjax();
      }
      }
      break;

		case "Verify SR Router and GIL":
		{
			clearTextBox(EvpnOrderObj.CompositOrders.Asr);
			Reusable.waitForSiebelLoader();
		
			sendKeys(EvpnOrderObj.CompositOrders.Asr,PrimarySR);
			Reusable.waitForSiebelLoader();
	
			Reusable.waitForSiebelLoader();
			Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.Asr, Keys.ENTER);
			
			clearTextBox(EvpnOrderObj.CompositOrders.AsrGil);
			Reusable.waitForSiebelLoader();
		
			sendKeys(EvpnOrderObj.CompositOrders.AsrGil,PrimarySRGIL);
			Reusable.waitForSiebelLoader();
	
			Reusable.waitForSiebelLoader();
			Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.AsrGil, Keys.ENTER);
			
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;

		case "Select OLO NNI Profile":
		{
			verifyExists(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			click(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			Reusable.waitForSiebelLoader();
			
			clearTextBox(EvpnOrderObj.CompositOrders.OLONNIProfile);
			Reusable.waitForSiebelLoader();
		
			sendKeys(EvpnOrderObj.CompositOrders.OLONNIProfile,OLONNIProfile );
			Reusable.waitForSiebelLoader();
	
			Reusable.waitForSiebelLoader();
			Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.OLONNIProfile, Keys.ENTER);
			
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}	
		break;

		case "Select OVC Circuit":
		{		
			verifyExists(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			click(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			Reusable.waitForSiebelLoader();
			
			clearTextBox(EvpnOrderObj.CompositOrders.OVCCircuit);
			Reusable.waitForSiebelLoader();
		
			sendKeys(EvpnOrderObj.CompositOrders.OVCCircuit,OLONNIProfile );
			Reusable.waitForSiebelLoader();
	
			Reusable.waitForSiebelLoader();
			Reusable.SendkeaboardKeys(EvpnOrderObj.CompositOrders.OVCCircuit, Keys.ENTER);
			
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}	
		break;

		case "Set/Validate Serial Number":
		{
            verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
            click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
            if (DeviceType.contains("Stub"))
            {
                if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
                {
                    verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
                    click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
                    sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
                    Reusable.waitForAjax();
                    
                    verifyExists(NcOrderObj.createOrder.Update,"Update button");
                    click(NcOrderObj.createOrder.Update,"Update button");
                    Reusable.waitForAjax();
                    
//                    verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//                    click(NcOrderObj.taskDetails.TaskTitle,"Task title link");



                    verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
                    click(NcOrderObj.addProductDetails.Edit,"Edit button");
                    Reusable.waitForAjax();
                }
                else
                {
                    verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
                    click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
                    sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
                    Reusable.waitForAjax();
                    
                    verifyExists(NcOrderObj.createOrder.Update,"Update button");
                    click(NcOrderObj.createOrder.Update,"Update button");
                    Reusable.waitForAjax();
                    
                    verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
                    click(NcOrderObj.addProductDetails.Edit,"Edit button");
                    Reusable.waitForAjax();
                }
                Reusable.waitForAjax();
                verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
                click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
                Reusable.waitForAjax();
                if(DeviceType.equalsIgnoreCase("Stub"))
                {
                    if (AendAccessNWElement.contains("SC") || BendAccessNWElement.contains("SC"))
                    {
                        System.out.println("Not an Stub GX/LTS Device");
                    }
                    else
                    {
                        for(int i=1;i<=3;i++)
                        {
                        	ErrorWorkItems.errorWorkitems("Beacon: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
                        }
                    }
                }
            }
            else
            {
                if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
                {
                    verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
                    click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
                }
                else
                {
                    verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
                    click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
                }
            }
            Reusable.waitForAjax();
//            verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
//            click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
            Reusable.waitForAjax();
        }
        break;

		case "OAM Parameters Manual Confirmation":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;

		case "Set Ingress Classification Parameters":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break; 

		case "Activation Start Confirmation":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
			
			if(DeviceType.equalsIgnoreCase("Stub"))
			{
				verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
				click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
				waitForAjax();

				if (verifyExists("@xpath=//a[text()='Check GX/LTS Smarts Status in NC OSS']"))
				{
					ErrorWorkItems.errorWorkitems("Check GX/LTS Smarts Status in NC OSS",testDataFile, sheetName, scriptNo, dataSetNo);
				}
			}
			if(DeviceType.contains("Stub"))
			{
				ErrorWorkItems.errorWorkitems("Create Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
			}
		}
		break;

		case "Check Order parameters":
		{
			verifyExists(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			click(EvpnOrderObj.CompositOrders.TaskTitle,"TaskTitle");
			Reusable.waitForSiebelLoader();
			
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;

		case "Bandwidth Profile Confirmation":
		{
			verifyExists(EvpnOrderObj.CompositOrders.Complete,"Complete");
			click(EvpnOrderObj.CompositOrders.Complete,"Complete");
			Reusable.waitForSiebelLoader();
		}
		break;
		}
	}
	public void UpdateAccessPortDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SelectValueDropdown17 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Select Value Dropdown");
		String SelectValueDropdown19 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Select Value Dropdown");
		String SelectValueDropdown18 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Select Value Dropdown");

		verifyExists(ModifyOrderObj.ModifyOrder.ClickDrpDwn,"ClickDrpDwn");
		click(ModifyOrderObj.ModifyOrder.ClickDrpDwn.replace("Value", "Presentation Interface"));

		verifyExists(ModifyOrderObj.ModifyOrder.SelectValueDropdown);
		click(ModifyOrderObj.ModifyOrder.SelectValueDropdown.replace("Value",SelectValueDropdown17));

		Reusable.waitForSiebelLoader();
		verifyExists(ModifyOrderObj.ModifyOrder.ClickDrpDwn);
		click(ModifyOrderObj.ModifyOrder.ClickDrpDwn.replace("Value", "Connector Type"));

		verifyExists(ModifyOrderObj.ModifyOrder.SelectValueDropdown);
		click(ModifyOrderObj.ModifyOrder.SelectValueDropdown.replace("Value",SelectValueDropdown18));

		Reusable.waitForSiebelLoader();
		verifyExists(ModifyOrderObj.ModifyOrder.ClickDrpDwn);
		click(ModifyOrderObj.ModifyOrder.ClickDrpDwn.replace("Value", "Fibre Type"));

		verifyExists(ModifyOrderObj.ModifyOrder.SelectValueDropdown);
		click(ModifyOrderObj.ModifyOrder.SelectValueDropdown.replace("Value",SelectValueDropdown19));

		Reusable.waitForSiebelLoader();
		verifyExists(ModifyOrderObj.ModifyOrder.SavePage);
		click(ModifyOrderObj.ModifyOrder.SavePage,"Click on Click here to save and continue");

	
	}
	public void UpdateBandwidth(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SelectValueDropdown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service_Bandwidth");
		
		Reusable.waitForSiebelLoader();
		verifyExists(ModifyOrderObj.ModifyOrder.Service_BW,"Service bandwidth");
		click(ModifyOrderObj.ModifyOrder.Service_BW,"Service bandwidth");

		verifyExists(ModifyOrderObj.ModifyOrder.selectService_BW.replace("Value", SelectValueDropdown),"selectService_BW");
		click(ModifyOrderObj.ModifyOrder.selectService_BW.replace("Value", SelectValueDropdown),"selectService_BW");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Updated Service Bandwidth");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();


	}	
	public void UpdateLinkType(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SelectValueDropdown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Select Value Dropdown");
		
		verifyExists(ModifyOrderObj.ModifyOrder.ClickDrpDwn);
		click(ModifyOrderObj.ModifyOrder.ClickDrpDwn.replace("Value", "Link Type"));

		verifyExists(ModifyOrderObj.ModifyOrder.SelectValueDropdown);
		click(ModifyOrderObj.ModifyOrder.SelectValueDropdown.replace("Value", SelectValueDropdown));

		Reusable.waitForSiebelLoader();
		verifyExists(ModifyOrderObj.ModifyOrder.SavePage);
		click(ModifyOrderObj.ModifyOrder.SavePage,"Click on Click here to save and continue");


	}
	public void UpdateVlanDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String Port_Role = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Port_Role");
		String VLAN_Tag_ID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_VLAN_Tag_ID");
		String VLAN_Tagging_Mode = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_VLAN_Tagging_Mode");

		verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
		click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Port_Role));
		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Port_Role));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPAccessObj.Proceed,"Proceed");
		click(SiebelModeObj.IPAccessObj.Proceed,"Proceed");
		
		

		Reusable.waitForSiebelLoader();
		if(!Port_Role.equalsIgnoreCase("Physical Port"))
		{
			verifyExists(SiebelModeObj.IPAccessObj.VLANTagMode,"Verify VLANTagMode");
			click(SiebelModeObj.IPAccessObj.VLANTagMode,"Click On VLANTagMode");
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",VLAN_Tagging_Mode));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",VLAN_Tagging_Mode));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.VLANTagID,"Verify VLANTagID");
			click(SiebelModeObj.IPAccessObj.VLANTagID,"Click On VLANTagID");
			sendKeys(SiebelModeObj.IPAccessObj.VLANTagID,VLAN_Tag_ID,"VLANTagID");
			Reusable.waitForSiebelLoader();
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Port Role Not VLAN");
		}
		Reusable.waitForSiebelLoader();
		verifyExists(ModifyOrderObj.ModifyOrder.SavePage);
		click(ModifyOrderObj.ModifyOrder.SavePage);

	
	}
//	public void ReadyForNcCheckbox() throws Exception 
//	{
//		int count = getwebelementscount("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']");
//		for (int i=0;i<count;i++){
//			click("(//span[text()='Ready For NC']/parent::div/input[@aria-checked='N'])["+(i+1)+"]");	
//			Thread.sleep(6000);
//		}
//		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select 'Ready For NC' check box");
//
//		if(count>0)
//		{
//			verifyExists(ModifyOrderObj.ReadyForNcCheckbox.SaveButtondisp,"Verify on save button");
//			click(ModifyOrderObj.ReadyForNcCheckbox.SaveButtondisp,"click on save button");
//
//		}
//	}
	



}
