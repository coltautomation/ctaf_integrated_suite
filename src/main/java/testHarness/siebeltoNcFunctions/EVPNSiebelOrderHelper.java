package testHarness.siebeltoNcFunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.siebeltoNCObjects.EVPNSiebelOrderObj;
import pageObjects.siebeltoNCObjects.SiebelOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class EVPNSiebelOrderHelper extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();

	public void addEvpnAccesskFetures(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String Bespoke = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bespoke");
		String COS = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Class of Service");
		String Classification_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"COS Classification");

		
		if (Bespoke.toString().contains("Yes")) // bespoke
		{
			click(EVPNSiebelOrderObj.EVPNSiebelOrder.Bespokecheckbox,"Bespokecheckbox");
			Reusable.waitForAjax();
			Reusable.savePage();
			Reusable.waitForAjax();
			sendKeys(EVPNSiebelOrderObj.EVPNSiebelOrder.Bespoketext,"BSEVPN123","Bespoketext");
			Reusable.waitForAjax();
			Reusable.savePage();
			Reusable.waitForAjax();
					
		
			 if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
	            {
	                if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
	                {
	                    verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
	                    click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
	                    Reusable.waitForAjax();
	                    Reusable.waitForSiebelLoader();
	                    Reusable.waitForpageloadmask();
	                }
	            }

		}


		if (COS.toString().contains("Yes")) // adding class of service feature
		{
			//as of now we have only one feature
			javaScriptclick(EVPNSiebelOrderObj.EVPNSiebelOrder.ClickShowFullinfo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			
			click(EVPNSiebelOrderObj.EVPNSiebelOrder.Classofserviceaccess,"Classofserviceaccess");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			String cosVallue = null;
			if(Classification_Type.toString().contains("PCP"))
			{
			cosVallue="802.1p";
			}
			else if(Classification_Type.toString().contains("IPv4"))
			{
				cosVallue="IP DSCP";

			}
			else if(Classification_Type.toString().contains("IPv6"))
			{
				cosVallue="IPv6 DSCP";
			}
			
			verifyExists(EVPNSiebelOrderObj.EVPNSiebelOrder.Classificationtypedrpdown,"Classificationtypedrpdown");
			click(EVPNSiebelOrderObj.EVPNSiebelOrder.Classificationtypedrpdown,"Classificationtypedrpdown");
			click("@xpath=//*[text()='"+cosVallue+"']");
			Reusable.SendkeaboardKeys(EVPNSiebelOrderObj.EVPNSiebelOrder.Classificationtypeinput, Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			javaScriptclick(EVPNSiebelOrderObj.EVPNSiebelOrder.Closeevpnaccess,"Closeevpnaccess");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			verifyExists(EVPNSiebelOrderObj.EVPNSiebelOrder.SaveButtondisp,"SaveButtondisp");
			click(EVPNSiebelOrderObj.EVPNSiebelOrder.SaveButtondisp,"SaveButtondisp");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		}
	}

}