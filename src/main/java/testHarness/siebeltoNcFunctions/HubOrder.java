package testHarness.siebeltoNcFunctions;


import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.siebeltoNCObjects.NcOrderObj;
import pageObjects.siebeltoNCObjects.NewHubOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class HubOrder extends SeleniumUtils
{
	public static ThreadLocal<String> SpokeOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> HubOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();

	ReusableFunctions Reusable=new ReusableFunctions();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();
	
    public void CreatHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
    	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);
		
		verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"NewCompositeOrder");
		click(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"NewCompositeOrder");
		String OrderDesc="Hub Order Created using Automation script";
		String[] arrOfStr = Ordernumber.split("#", 0); 
		Reporter.log(arrOfStr[1]);
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderDescription,"Order Description");
		sendKeys(NewHubOrderObj.NewHubOrder.OrderDescription,OrderDesc,"Order Description");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
		click(NewHubOrderObj.NewHubOrder.Update,"Update");
		
		Reporter.log(Orderscreenurl.get());
	}

    public void AddHubProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String productValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Product");
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		
		verifyExists(NewHubOrderObj.NewHubOrder.HubProductCheckBox,"Hub Option");
		click(NewHubOrderObj.NewHubOrder.HubProductCheckBox,"Hub Option");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Addbutton,"Add Button");
		click(NewHubOrderObj.NewHubOrder.Addbutton,"Add Button");
		
		Reusable.waitForAjax();
	}
	
    public void AddHubProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
    	String HubResiliencOption=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A_End_Resilience_Option");
    	String OrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
    	String NetworkRef=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NetworkReference_Number");
    	String HubBandwidth=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service_Bandwidth1");
//    	String Oderreferencenumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order Sys Ref ID");
//    	String Topology=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Topology");
//    	String CommercialProductName=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Commercial Product Name");
    	
		String[] OrderRefNumber = OrderNumber.split("/");
    	
		verifyExists(NewHubOrderObj.NewHubOrder.UnderlyningHubOrder,"Under Lying Hub Order");
		click(NewHubOrderObj.NewHubOrder.UnderlyningHubOrder,"Under Lying Hub Order");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");

		verifyExists(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
    	click(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
    	Reusable.waitForSiebelLoader();
    	selectByVisibleText(EthernetOrderObj.CompositOrders.OrderSystemName,"SPARK","Order System Name");
    	
    	verifyExists(EthernetOrderObj.CompositOrders.Orderreferencenumber,"Orderreferencenumber");
    	sendKeys(EthernetOrderObj.CompositOrders.Orderreferencenumber,OrderRefNumber[0],"Orderreferencenumber");	
    	
    	verifyExists(NewHubOrderObj.NewHubOrder.NetworkReference,"Network Reference Number");
    	sendKeys(NewHubOrderObj.NewHubOrder.NetworkReference,NetworkRef,"Network Reference Number");
    	
		verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"Hub Resilience Option");
		click(EthernetOrderObj.CompositOrders.ResilienceOption,"Hub Resilience Option");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,HubResiliencOption,"Hub Resilience Option");
    	
    	verifyExists(NewHubOrderObj.NewHubOrder.Topology,"Topology");
    	click(NewHubOrderObj.NewHubOrder.Topology,"Topology");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NewHubOrderObj.NewHubOrder.Topology,"Hub & Spoke","Topology");
    	
		verifyExists(EthernetOrderObj.CompositOrders.Ordernumber,"Ordernumber");
		sendKeys(EthernetOrderObj.CompositOrders.Ordernumber,OrderNumber,"Ordernumber");	
    	
		verifyExists(EthernetOrderObj.CompositOrders.CommercialProductName,"CommercialProductName");
		sendKeys(EthernetOrderObj.CompositOrders.CommercialProductName,"ETHERNET HUB","CommercialProductName");	
			
		verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"Service Bandwidth");
		sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,HubBandwidth,"Service Bandwidth");
		Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
		
		verifyExists(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		click(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.CircuitCategory,"LE","Circuit Category");
		
		Reusable.waitForSiebelLoader();
		
		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
		click(NewHubOrderObj.NewHubOrder.Update,"Update");
		waitForAjax();
		//Required Details are Updated
	 }

    public void DecomposeHubOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
    {
    	String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");

 		getUrl(Orderscreenurl);
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");

		verifyExists(NewHubOrderObj.NewHubOrder.HubSuborder,"Hub Sub order");
		click(NewHubOrderObj.NewHubOrder.HubSuborder,"Hub Sub order");
	
		verifyExists(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
		click(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
		waitForAjax();
    }
     	 
     	public void HubProductDeviceDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException 
    	{ 
    		String HubAccessTechnolgy=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Access_Technology/ Primary");
    		String HubAccessType=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Access_Type/ Primary");
    		String HubSiteID=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Site_ID");
    		String HubCabinetID=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Cabinet_ID");
    		String HubCabinetType=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Cabinet_Type");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.HubSiteProductend,"New Hub Site Product End");
    		click(NewHubOrderObj.NewHubOrder.HubSiteProductend,"New Hub Site Product End");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
    		click(EthernetOrderObj.CompositOrders.Edit,"Edit");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
    		click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Hub Access Technology");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,HubAccessTechnolgy,"Hub Access Technology");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
    		click(EthernetOrderObj.CompositOrders.AccessType,"Hub Access Type");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessType,HubAccessType,"Hub Access Type");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.SiteID,"Site ID");
    		sendKeys(NewHubOrderObj.NewHubOrder.SiteID,HubSiteID,"Site ID");		
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Update,"details are Updated");
    		click(EthernetOrderObj.CompositOrders.Update,"details are Updated");
    		
    	    waitForAjax();

    		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
    		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
    		click(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
    		click(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
    		click(EthernetOrderObj.CompositOrders.PresentConnectType,"Hub Presentation Connector Type");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,"LC/PC","Hub Presentation Connector Type");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
    		click(EthernetOrderObj.CompositOrders.AccessportRole,"Hub Port Role");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,"Physical Port","Hub Port Role");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
    		click(EthernetOrderObj.CompositOrders.Update,"Update");

    		Reusable.waitForAjax();
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
    		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
    		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
    		click(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
    		click(EthernetOrderObj.CompositOrders.Edit,"Edit");
    	
    		verifyExists(NewHubOrderObj.NewHubOrder.CabinateID,"Cabinate ID");
    		sendKeys(NewHubOrderObj.NewHubOrder.CabinateID,HubCabinetID,"Cabinate ID");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
    		click(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.CabinetType,HubCabinetType,"Cabinet Type");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
    		click(EthernetOrderObj.CompositOrders.Update,"Update");
    	    waitForAjax();
    	}
    	
     	public void ProcessHubOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
    	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Order Number", Ordernumber);
     		String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");

     		getUrl(Orderscreenurl);
    		//Navigate to Hub Order screen
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
			String[] arrOfStr = Ordernumber.split("#", 0);
    		click(NewHubOrderObj.NewHubOrder.hubo1+arrOfStr+NewHubOrderObj.NewHubOrder.hubo2);
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Proccessing");
		 	click(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Proccessing");	
    	}
     	
     	public void CompleteHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
    		String Order_Number = null;
    		Order_Number = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_No");
    		String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");
    		String WorkItems = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Work Items");
    		String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Type of Device");
    		String Product = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");

    		getUrl(Orderscreenurl);
    		
    		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
    		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
    		click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
    		
    		waitForAjax();
    	
    		verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
    		click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
    		
    		waitForAjax();
    		
    		
    		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
    		{
    			waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.TaskReadytoComplete,90,20000);
    			click(EthernetOrderObj.CompositOrders.TaskReadytoComplete,"Workitem in Ready status");
    			//need to verify
    			CompletHubworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
    		}
    		
    		waitForAjax();
    		verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
		 	click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.Workitems,"Workitems");
		 	click(NewHubOrderObj.NewHubOrder.Workitems,"Workitems");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.WorkItemSelect,"Work Item Select");
		 	click(NewHubOrderObj.NewHubOrder.WorkItemSelect,"Work Item Select");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.View,"View");
		 	click(NewHubOrderObj.NewHubOrder.View,"View");
    		
		 	String NCServiceId=getTextFrom(NewHubOrderObj.NewHubOrder.NCSID);
		 	DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Hub_NCS_ID", NCServiceId);
		 	
    		getUrl(Orderscreenurl);
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
    		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
    		
//    		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
//    		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    		
    		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
    		
    		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Order_Number);
    		
    		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
    		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

    		waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder1,180,20000);
    	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder4);
    	    if(OrderStatus.equalsIgnoreCase("Process Started"))
    		{
    	    	String	ErrorStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.BlockedbyError);
    	    	if (ErrorStatus.equalsIgnoreCase("Blocked By Errors"))
    	    	{
    		    	if (DeviceType.contains("Stub"))
    				{
    			    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrOfStr2);
    				
    					verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
    					click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
    					
    					verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
    					click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
    					
    					waitForAjax();

    					verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
    					click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
    					
    					waitForAjax();
    				
    					if(verifyExists("@xpath=//a[text()='Create Service in Smarts']"))
    					{
    						ErrorWorkItems.Smartserror("Create Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
    		
    						getUrl(Orderscreenurl);
    				        
    						click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
    						Reusable.waitForAjax();
    				        
    						verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    						click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    						
    						selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
    						
    						sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Order_Number);
    						
    						verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
    						click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
    						
    						waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder1,90,20000);
    						if (OrderStatus.contains("Process Completed"))
    						{
    							Report.LogInfo("Validate","\""+Order_Number +"\" Is Completed Successfully", "INFO");
    							ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Is Completed Successfully");
    						}
    					}			
    					else
    					{
    						System.out.println("Order is blocked by different errors");
    					}
    				}
    	    	}
    		}
    	    else if (OrderStatus.contains("Process Completed"))
    		{
    			Report.LogInfo("Validate","\""+Order_Number +"\" Is Completed Successfully", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Is Completed Successfully");
    		}
    		else if (OrderStatus.contains("Process Started"))
    		{
    			Report.LogInfo("Validate","\""+Order_Number +"\" Execution InProgress", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Execution InProgress");
    		}
    		Reusable.waitForpageloadmask();
    	}
     	
     	public void CompletHubworkitem(String[] taskTitle, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
     		String HubResiliencOption=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A_End_Resilience_Option");
    		String HubAccessNWElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end Access NW Element");
    		String HubAccessport = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end Access port");
    		String HubCPENNITrunkPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
    		String HubCPENNITrunkPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
    		String HubPeNwElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
    		String HubPEPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
    		String HubPEPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
    		String HubVcxControl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend VCX Control");
    		String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Type of Device");
    	
     		System.out.println("In Switch case with TaskName :"+taskTitle);
    		switch(taskTitle[0])
    		{
			case "Reserve Access Resources":
			if (HubResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,HubAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,HubAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,HubCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,HubCPENNITrunkPort2);
				waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
			}
			else
			{
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,HubAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,HubAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,HubCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
//				workitemcounter.set(workitemcounter.get()+1);
			}
			break;
			
			case "Transport Circuit Design":
			if (HubResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,HubPeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForAjax();
				
				if (DeviceType.contains("Stub"))
				{
					if (HubAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,HubVcxControl);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
					}			
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,HubPEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,HubPEPort2);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,HubPeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,HubPEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForAjax();

				if (DeviceType.contains("Stub"))
				{
					if (HubAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,HubVcxControl);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
					}
	
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
					
			case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.equalsIgnoreCase("Stub"))
				{
                	if (HubAccessNWElement.contains("SC"))
                	{
                		System.out.println("Not an Stub GX/LTS Device");
                	}
                	else
					{
                		ErrorWorkItems.errorWorkitems("EIP: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
                		ErrorWorkItems.checkipaddress("Check IP address in DNS",testDataFile, sheetName, scriptNo, dataSetNo);
					}
				}
	    	}
	    	break;
	    	
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					Reusable.waitForAjax();
					verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					Reusable.waitForAjax();
                    if(DeviceType.equalsIgnoreCase("Stub"))
                    {
                    	if (HubAccessNWElement.contains("SC"))
                    	{
                    		System.out.println("Not a Stub GX/LTS Device");
                    	}
                    	else
                    	{
        					for(int i=1;i<=3;i++)
        					{
        						ErrorWorkItems.errorWorkitems("Beacon: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
        					}
        					ErrorWorkItems.errorWorkitems("Check GX/LTS Smarts Status in NC OSS",testDataFile, sheetName, scriptNo, dataSetNo);
                    	}
                    	ErrorWorkItems.Smartserror("Create Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
                    }
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
				}
			}
			break;
    		}
    	}
    	 
    	public void GotoHubErrors(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
    		String Ordernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Order Number");
    		String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");
    		
    		String[] arrOfStr = Ordernumber.split("#", 0);
    		String Errors = getTextFrom(NewHubOrderObj.NewHubOrder.Errors1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.Errors2);

    		
	   		getUrl(Orderscreenurl);
	   		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
   		//Navigate to Accounts Composite Orders Tab
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
	   		if(Errors.equalsIgnoreCase("Blocked by Errors"))
	   		{
	   			
	   			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
	   			click(NewHubOrderObj.NewHubOrder.CompositeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.CompositeOrder2,"Composite order");
	   			
	   			verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
	   			click(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
	   		
	   			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
	   			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
	   			waitForAjax();
	   			
	   			verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
	   			click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
	   			waitForAjax();
	   		}
	   		else 
	   		{
	   			Reporter.log("Not required to Navigate to Errors tab");
	   			//Hub Order did not have any errors to be Captured
	   		}
	   	}

    	public void CreateSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
    	{
    		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
    				
    		waitForElementToAppear(NcOrderObj.createOrder.NewCompositeOrder, 10);
    		verifyExists(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
    		click(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
    		Reusable.waitForAjax();
    		
    		String SpokeOrdernumber = getTextFrom(NcOrderObj.createOrder.OrderNumber);
    		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_No", SpokeOrdernumber);
    		Reusable.waitForAjax();
    		String[] arrOfStr = SpokeOrdernumber.split("#", 0);
//    		Log.info(arrOfStr[1]);
    		
    		sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Spoke order created using CTAF Automation");
    		
    		verifyExists(NcOrderObj.createOrder.Update,"Update");
    		click(NcOrderObj.createOrder.Update,"Update");
    		
    		String SpokeOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
    		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_URL", SpokeOrderscreenurl);
    		System.out.println(SpokeOrderscreenurl);
    		Reusable.waitForAjax();
    	}
     	 
    	public void AddSpokeProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	    {		
		 	verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AddonOrderTab,"Addon Order Tab");
			click(NewHubOrderObj.NewHubOrder.AddonOrderTab,"Addon Order Tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.EthernetProductCheckBox,"Ethernet Product CheckBox");
			click(NewHubOrderObj.NewHubOrder.EthernetProductCheckBox,"Ethernet Product CheckBox");

			verifyExists(NewHubOrderObj.NewHubOrder.Addbutton,"Add button");
			click(NewHubOrderObj.NewHubOrder.Addbutton,"Add button");
			
			Reusable.waitForAjax();
	    }

    	public void AddSpokeProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
//	   	 	String SpokeOrderRef=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Order Sys Ref ID");
//	   		String SpokeTopology=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Topology");
//	   	 	String SpokeCommercialProduct=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke Commercial Product");
	   	 	String SpokeOrderNumber=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SiteOrderReference_Number");
	   	 	String SpokeBandwidth=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Site/SpokeService_Bandwidth");
	   	 	String CircuitCategory="LE";
//	   	 	String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
//    		getUrl(SpokeOrderscreenurl);
	   	 	
	   	 	String[] SpokeOrderRef = SpokeOrderNumber.split("/");
	   	 	
	   	 	verifyExists(NewHubOrderObj.NewHubOrder.UnderlyningOrder,"Under Lying Order");
			click(NewHubOrderObj.NewHubOrder.UnderlyningOrder,"Under Lying Order");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
	
			verifyExists(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
			click(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.OrderSystemName,"SPARK","Order System Name");
	
	   		verifyExists(NewHubOrderObj.NewHubOrder.Orderreferencenumber,"Order reference number");
	   		sendKeys(NewHubOrderObj.NewHubOrder.Orderreferencenumber,SpokeOrderRef[0],"Order reference number");
	
			verifyExists(EthernetOrderObj.CompositOrders.Topology,"Topology");
			click(EthernetOrderObj.CompositOrders.Topology,"Topology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.Topology,"Hub & Spoke","Topology");
	   		
			verifyExists(EthernetOrderObj.CompositOrders.Ordernumber,"Order number");
			sendKeys(EthernetOrderObj.CompositOrders.Ordernumber,SpokeOrderNumber,"Order number");	
	   		
			verifyExists(EthernetOrderObj.CompositOrders.CommercialProductName,"Commercial Product Name");
			sendKeys(EthernetOrderObj.CompositOrders.CommercialProductName,"ETHERNET SPOKE","Commercial Product Name");	
			
			verifyExists(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
			click(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.CircuitCategory,"LE","Circuit Category");
			
			verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
			sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,SpokeBandwidth,"ServiceBandwidth");
			Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
	   		
			Reusable.waitForAjax();
			
	   		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
	   		click(NewHubOrderObj.NewHubOrder.Update,"Update");
    	}

    	public void AddSpokeFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
		  	String feature="End Site Product";
	
//	   	 	String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
//    		getUrl(SpokeOrderscreenurl);

    		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab," General Information Tab");
			click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"General Information Tab");
			
			waitForAjax();
			
			verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			
		    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,feature,"Product Display");
		    Thread.sleep(30000);
		    Reusable.waitForAjax();
			click(EthernetOrderObj.CompositOrders.EndSite,"End Site Product Select");
		    
			verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			
			waitForAjax();
    	}
	  
    	public void DecomposeSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
     	{
    		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
    		
    		getUrl(SpokeOrderscreenurl);
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Orders Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Orders Tab");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Suborder,"Suborder");
			click(NewHubOrderObj.NewHubOrder.Suborder,"Suborder");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
			click(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
     		
     		waitForAjax();
     	}
     	 
    	public void AddHub(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    	{
    		String NCServiceId = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Hub_NCS_ID");
    		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");

    		getUrl(SpokeOrderscreenurl);

    		verifyExists(NewHubOrderObj.NewHubOrder.NewEthernetConnProduct,"Ethernet Connection Product Order");
    		click(NewHubOrderObj.NewHubOrder.NewEthernetConnProduct,"Ethernet Connection Product Order");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AddHub,"Add Hub button");
    		click(NewHubOrderObj.NewHubOrder.AddHub,"Add Hub button");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.TypeofHub,"Service Id");
    	    sendKeys(NewHubOrderObj.NewHubOrder.TypeofHub,NCServiceId); 
    		waitForAjax();
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AddHubProdOrder,"Hub Order");
    		click(NewHubOrderObj.NewHubOrder.AddHubProdOrder,"Hub Order");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
    		click(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
    		waitForAjax();
//    		getUrl(SpokeOrderscreenurl);
    		
    	}
    	
   	 public void SpokeProductDeviceDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
   		{ 
   			String HubPortRole=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_Port_Role");
   			String HubVLanTaggingMode=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_VLAN_Tagging_Mode");
   			String HubVLanTagId=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Aend_VLAN_Tag_ID");
   			String SpokeAccessTechnolgy=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Access_Technology/Secondary");
   			String SpokeAccessType=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Access_Type/Secondary");
   			String SpokeResilienceOption=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B_End_Resilience_Option");
   			String SpokeSiteID=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Site_ID");
   			String SpokePortRole=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Port_Role");
   			String SpokeVLanTaggingMode=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_VLAN_Tagging_Mode");
   			String SpokeVLanTagId=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_VLAN_Tag_ID");
    		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
   			//String PresentConnectTypeValue=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "");
    		
    		getUrl(SpokeOrderscreenurl);
   		 
   		 	verifyExists(NewHubOrderObj.NewHubOrder.NewEndSiteProductAend,"A end Site Product Order");
   			click(NewHubOrderObj.NewHubOrder.NewEndSiteProductAend,"A end Site Product Order");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information tab");
   			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information tab");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port link");
   			click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port link");
   		
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   		
   			verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
   			click(EthernetOrderObj.CompositOrders.PresentConnectType,"A End Presentation Connector Type");
   			Reusable.waitForSiebelLoader();
   			selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,"LC/PC","A End Presentation Connector Type");
   			
   			verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
   			click(EthernetOrderObj.CompositOrders.AccessportRole,"A End port Role");
   			Reusable.waitForSiebelLoader();
   			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,HubPortRole,"A End Port Role");
   			
   			verifyExists(EthernetOrderObj.CompositOrders.VlanTaggingMode,"VLAN Tagging Mode");
   			click(EthernetOrderObj.CompositOrders.VlanTaggingMode,"A End VLAN Tagging Mode");
   			Reusable.waitForSiebelLoader();
   			selectByVisibleText(EthernetOrderObj.CompositOrders.VlanTaggingMode,HubVLanTaggingMode,"A End VLAN Tagging Mode");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   			click(NewHubOrderObj.NewHubOrder.Update,"Update");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   			click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			waitForAjax();
   			
			String typeoffeature="VLAN";
			verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			
		    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,typeoffeature,"Product Display");
		    Reusable.waitForAjax();
			click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
		    
			verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			
			Reusable.waitForAjax();
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN link");
   			click(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN link");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			
			String etherType= "VLAN (0x8100)";
			verifyExists(EthernetOrderObj.CompositOrders.Ethertype,"Ether type");
			String etherTypeclr= EthernetOrderObj.CompositOrders.Ethertype;
			clearTextBox(etherTypeclr);
		    sendKeys(EthernetOrderObj.CompositOrders.Ethertype,etherType,"Ether Type");
		    
		    waitForAjax();
		    
			verifyExists(EthernetOrderObj.CompositOrders.VlanTagId,"Vlan Tag Id");  
		    sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,HubVLanTagId,"Vlan Tag Id");
   			waitForAjax();
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   			click(NewHubOrderObj.NewHubOrder.Update,"Update");
   			waitForAjax();
   			
    		getUrl(SpokeOrderscreenurl);
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
   			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.NewEndSiteProductBend,"B end Site Product Order");
   			click(NewHubOrderObj.NewHubOrder.NewEndSiteProductBend,"B end Site Product Order");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			
			verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
			click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"B End Access Technology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,SpokeAccessTechnolgy,"B End Access Technology");
   			
			verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
			click(EthernetOrderObj.CompositOrders.AccessType,"B End Access Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessType,SpokeAccessType,"B End Access Type");
   			
   			if(SpokeResilienceOption.equalsIgnoreCase("Protected"))
   			{
				String ResilienceOptions= "Protected";
				verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
				click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"Order System Name");
   			}
   			else
   			{
				String ResilienceOptions= "Unprotected";
				verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
				click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"A End Resilience Option");
   			}
   			
   			waitForAjax();
   			
			verifyExists(EthernetOrderObj.CompositOrders.SiteEnd,"Site End");
			click(EthernetOrderObj.CompositOrders.SiteEnd,"B End Site");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.SiteEnd,"B END","B End Site");
   		
			verifyExists(EthernetOrderObj.CompositOrders.SiteID," SiteID");  
		    sendKeys(EthernetOrderObj.CompositOrders.SiteID,SpokeSiteID," SiteID");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   			click(NewHubOrderObj.NewHubOrder.Update,"Update");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			waitForAjax();
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
   			click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			waitForAjax();
   			
			verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
			click(EthernetOrderObj.CompositOrders.PresentConnectType,"B End Presentation Connector Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,"LC/PC","B End Presentation Connector Type");
			
			verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
			click(EthernetOrderObj.CompositOrders.AccessportRole,"B End port Role");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,SpokePortRole,"B End Port Role");
			
			verifyExists(EthernetOrderObj.CompositOrders.VlanTaggingMode,"VLAN Tagging Mode");
			click(EthernetOrderObj.CompositOrders.VlanTaggingMode,"B End VLAN Tagging Mode");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.VlanTaggingMode,SpokeVLanTaggingMode,"B End VLAN Tagging Mode");

			verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
			click(EthernetOrderObj.CompositOrders.Update,"Update");
			
			waitForAjax();
   			//Required Details are Entered
   			
   			if(SpokePortRole.contains("VLAN"))
   			{
   				verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   				click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   				
   				verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   				click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   				waitForAjax();
   				
   				verifyExists(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
   				click(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
   				
				verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
				click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
				
//				verifyExists(EthernetOrderObj.CompositOrders.TypeofFeature,"Type of feature area");  
			    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,typeoffeature,"Product Display");
			    Thread.sleep(30000);
			    Reusable.waitForAjax();
				click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
			    
//				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.TypeofFeature, Keys.ENTER);
			    
				verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
				click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
				click(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
				
				verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
				click(EthernetOrderObj.CompositOrders.Edit,"Edit button");
				
				verifyExists(EthernetOrderObj.CompositOrders.Ethertype,"Ether type");
				String betherTypeclr= EthernetOrderObj.CompositOrders.Ethertype;
				clearTextBox(betherTypeclr);
			    sendKeys(EthernetOrderObj.CompositOrders.Ethertype,etherType,"Ether Type");
			    
			    waitForAjax();
			    
				verifyExists(EthernetOrderObj.CompositOrders.VlanTagId,"VlanTagId Details are Entered");  
			    sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,SpokeVLanTagId,"VlanTagId Details are Entered");
			    
			    waitForAjax();
   				
   				verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   				 click(NewHubOrderObj.NewHubOrder.Update,"Update");
   				 waitForAjax();
   				 
   				//Required Details are Entered
   			}
   			else 
			{
				Reporter.log("B End Not VLAN");
			}
			Reporter.log("Required details are Updated");
   		}
   	 
   	 public void ProcessSpokeOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
   	 {
 		String SpokeOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_No");
 		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
 		
 		getUrl(SpokeOrderscreenurl);
 		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
 		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
 		Reusable.waitForAjax();
 	
 		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
 		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
 		Reusable.waitForAjax();
 		
 		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
 		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
 		
 		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
 		
 		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SpokeOrdernumber);
 		
 		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
 		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

 		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
 		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
 		
 		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
 		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
 		Reusable.waitForAjax();
   	 }
   	 
     	public void CompleteSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
     	{
	 		String SpokeOrdernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Spoke_Order_No");
	 		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
			String WorkItems = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Spoke Work item");
	
//			verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//			click(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
			
			verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		
	 		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
	 		
	 		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SpokeOrdernumber);
	 		
	 		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
	 		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

	 		String arrOfStr = EthernetOrderObj.CompositOrders.arrOfStr1+ SpokeOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2;
			verifyExists(arrOfStr,"Composite Order");
			click(arrOfStr,"Composite Order");
		
//			getUrl(SpokeOrderscreenurl);
			
			verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			
			verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			
			waitForAjax();
		
			verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
			click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
			
			waitForAjax();
			
			
			for (int k=1; k<=Integer.parseInt(WorkItems);k++)
			{
				waitRefreshForElementToAppear(EthernetOrderObj.CompositOrders.TaskReadytoComplete,120,10000);
				click(EthernetOrderObj.CompositOrders.TaskReadytoComplete,"Workitem in Ready status");
				//need to verify
				CompletSpokeworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
			}
			
			waitForAjax();
			
			getUrl(SpokeOrderscreenurl);
			
			verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
			click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
			
//			verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
//			click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
			
			verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		
	 		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
	 		
	 		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SpokeOrdernumber);
	 		
	 		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
	 		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
			
			waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ SpokeOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,180,10000);
		    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ SpokeOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
			if (OrderStatus.contains("Process Completed"))
			{
				Report.LogInfo("Validate","\""+SpokeOrdernumber +"\" Is Completed Successfully", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, SpokeOrdernumber +" Is Completed Successfully");
			}
			else if (OrderStatus.contains("Process Started"))
			{
				Report.LogInfo("Validate","\""+SpokeOrdernumber +"\" Execution InProgress", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, SpokeOrdernumber +" Execution InProgress");
			}
			
		}
     	
     	public void CompletSpokeworkitem( String[] taskTitle, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
     		String SpokeResiliencOption=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B_End_Resilience_Option");
    		String DeviceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Type of Device");
    		String SpokeAccessNWElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B end Access NW Element");
    		String SpokeAccessport = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B end Access port");
    		String SpokeCPENNITrunkPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 1");
    		String SpokeCPENNITrunkPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 2");
    		String SpokePeNwElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend PE NW Element");
    		String SpokePEPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend PE Port 1");
    		String SpokePEPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend PE Port 2");
    		String SpokeVcxControl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend VCX Control");
    		String LegacyCpeProfile= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LegacyCpeProfile");
    		String LegacyCpeOamLevel= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LegacyCpeOamLevel");
    		String OAMProfile= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "OAM Profile");
    	    String LegacyCpeSupportsCFM="yes";
    	
     		System.out.println("In Switch case with TaskName :"+taskTitle);
    		switch(taskTitle[0])
    		{
			case "Reserve Access Resources":
			if (SpokeResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,SpokeAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,SpokeAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,SpokeCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,SpokeCPENNITrunkPort2);
				waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
			}
			else
			{
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,SpokeAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,SpokeAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,SpokeCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
//				workitemcounter.set(workitemcounter.get()+1);
			}
			break;
			
			case "Transport Circuit Design":
			if (SpokeResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,SpokePeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForAjax();
				
				if (DeviceType.contains("Stub"))
				{
					if (SpokeAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,SpokeVcxControl);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
					}			
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,SpokePEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,SpokePEPort2);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,SpokePeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,SpokePEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForAjax();

				if (DeviceType.contains("Stub"))
				{
					if (SpokeAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,SpokeVcxControl);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
					}
	
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
					
			case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.contains("Stub"))
				{
                	if (SpokeAccessNWElement.contains("SC"))
                	{
                		System.out.println("Not an Stub GX/LTS Device");
                	}
                	else
                	{
						ErrorWorkItems.errorWorkitems("EIP: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
						ErrorWorkItems.checkipaddress("Check IP address in DNS",testDataFile, sheetName, scriptNo, dataSetNo);
                	}
				}
	    	}
	    	break;
	    	
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					Reusable.waitForAjax();
					verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					Reusable.waitForAjax();
                    if(DeviceType.equalsIgnoreCase("Stub"))
					{
	                	if (SpokeAccessNWElement.contains("SC"))
	                	{
	                		System.out.println("Not an Stub GX/LTS Device");
	                	}
	                	else
	                	{
							for(int i=1;i<=3;i++)
							{
								ErrorWorkItems.errorWorkitems("Beacon: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
							}
	                	}
					}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Set Legacy CPE Capability Profile":
			{
				clearTextBox(NcOrderObj.workItems.LegacyCpeProfile);
				waitForAjax();
				
				sendKeys(NcOrderObj.workItems.LegacyCpeProfile,LegacyCpeProfile,"Legacy Cpe Profile");
				waitForAjax();
				SendkeyusingAction(Keys.ENTER);
				clearTextBox(NcOrderObj.workItems.LegacyCpeOamLevel);
				waitForAjax();
				sendKeys(NcOrderObj.workItems.LegacyCpeOamLevel,LegacyCpeOamLevel,"Legacy Cpe Oam Level");
				Thread.sleep(5000);
				SendkeyusingAction(Keys.ENTER);
				clearTextBox(NcOrderObj.workItems.LegacyCpeSupportsCFM);
				waitForAjax();
				sendKeys(NcOrderObj.workItems.LegacyCpeSupportsCFM,"yes","Legacy CPE Supports CFM");
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				//Completed Set Legacy CPE Capability Profile
			}
			break;
			
			case "Legacy Activation Completed":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Select OAM Profile":
			{
				verifyExists(NcOrderObj.workItems.OAMProfile,"OAM Profile");
				click(NcOrderObj.workItems.OAMProfile,"OAM Profile");
				waitForAjax();
				sendKeys(NcOrderObj.workItems.OAMProfile,OAMProfile,"OAM Profile");
				SendkeyusingAction(Keys.ENTER);
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				//Completed Select OAM Profile
			}	
			break;
			
			case "Activation Start Confirmation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.contains("Stub"))
				{
                	if (SpokeAccessNWElement.contains("SC"))
                	{
                		System.out.println("Not an Stub GX/LTS Device");
                	}
                	else
                	{
                		ErrorWorkItems.errorWorkitems("Check GX/LTS Smarts Status in NC OSS",testDataFile, sheetName, scriptNo, dataSetNo);
                	}
					ErrorWorkItems.Smartserror("Create Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
				}
	    	}
	    	break;
	    	
			case "Bandwidth Profile Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
    	}
    }
     	
     	public void GotoSpokeErrors(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
    	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);

     		String[] arrOfStr = Ordernumber.split("#", 0);
     		String Errors=getTextFrom(NewHubOrderObj.NewHubOrder.Errors1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.Errors2);
     		
    		openurl(SpokeOrderscreenurl.get());
    		
    		//Navigate to Composite Order
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
          	//Get the value of In Error column for the order");
    		if(Errors.equalsIgnoreCase("Blocked by Errors"))
    		{
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
    			click(NewHubOrderObj.NewHubOrder.CompositeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.CompositeOrder2,"Composite order");
    			//verifyExists(Errors);
    			//click(Errors);
    			//Click on Composite Order
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    			click(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    		
    			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			waitForAjax();
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			waitForAjax();
    		}
    		else 
    		{
    			Reporter.log("Not required to Navigate to Errors tab");
    			//Spoke Order did not have any errors to be Captured
    		}
    	}
		
		public void AddDummyIpaHubProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    	{
    		String Product = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Protected");
    		String Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "DummyHub_Bandwidth");
    		String ServiceOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderReference_Number");
    		
    		select(NewHubOrderObj.NewHubOrder.OrderSystemName,"SPARK");
    		String[] OrderSysSerid = ServiceOrder.split("/");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Orderreferencenumber,"Order Reference Number");
    		sendKeys(NewHubOrderObj.NewHubOrder.Orderreferencenumber,OrderSysSerid[0]);
			Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.NewHubOrder.NetworkReference,"Network Reference"); 
    		sendKeys(NewHubOrderObj.NewHubOrder.NetworkReference,"9876545"); 

    		if(Product.toString().equalsIgnoreCase("Protected"))
    		{
    			verifyExists(NewHubOrderObj.NewHubOrder.ResilienceOption,"Protected");
    			select(NewHubOrderObj.NewHubOrder.ResilienceOption,"Protected");
    		}
    		else
    		{
    			verifyExists(NewHubOrderObj.NewHubOrder.ResilienceOption,"Unprotected");
    			select(NewHubOrderObj.NewHubOrder.ResilienceOption,"Unprotected");
    		}
    		verifyExists(NewHubOrderObj.NewHubOrder.Topology,"Hub & Spoke");  
    		select(NewHubOrderObj.NewHubOrder.Topology,"Hub & Spoke");  
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Ordernumber);
    		sendKeys(NewHubOrderObj.NewHubOrder.Ordernumber,ServiceOrder); //excel add column -- order number //871538443/190709-0010
			Reusable.waitForAjax();
    		
			verifyExists(NewHubOrderObj.NewHubOrder.CommercialProductName,"Ethernet Hub"); 
    		sendKeys(NewHubOrderObj.NewHubOrder.CommercialProductName,"Ethernet Hub"); 
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.CircuitCategory,"LE");
    		select(NewHubOrderObj.NewHubOrder.CircuitCategory,"LE");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.ServiceBandwidth,"ServiceBandwidth"); 
    		sendKeys(NewHubOrderObj.NewHubOrder.ServiceBandwidth,Bandwidth); 
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Updated");
    		click(NewHubOrderObj.NewHubOrder.Update,"Updated");
			Reusable.waitForSiebelLoader();
    	}

		public void CompleteIpaDummyHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    	{
    		String HubOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PrimaryAccessType");
    		String Orderscreenurl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Order_URL");

    		String Ordernumber = getTextFrom(NcOrderObj.createOrder.OrderNumber);
    		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Order_No", Ordernumber);
    		Reusable.waitForAjax();
    		String[] arrOfStr = Ordernumber.split("#", 0); 
    				
    		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Account Name Sorting");
    		click(NcOrderObj.generalInformation.AccountNameSorting);
    		Reusable.waitForAjax();

    		verifyExists("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*");
    		click("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*");

    		verifyExists(NcOrderObj.taskDetails.TaskTab,"Task Tab");
    		click(NcOrderObj.taskDetails.TaskTab);
    		Reusable.waitForAjax();

    		
    		verifyExists(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow link");
    		click(NcOrderObj.taskDetails.ExecutionFlowlink);
    		Reusable.waitForAjax();

    		
    		verifyExists(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
    		click(NcOrderObj.taskDetails.Workitems,"Click on Workitems Tab");
    		Reusable.waitForAjax();

    		for (int k=1; k<=Integer.parseInt(HubOrder.toString());k++)
    		{

    			ClickonElementByString(NcOrderObj.workItems.TaskReadytoComplete,60);
    			click(NcOrderObj.workItems.TaskReadytoComplete);
    			waitForAjax();
//    			CompletDummyIpaHubworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
    		}
    		verifyExists(NcOrderObj.taskDetails.Errors,"Errors Tab");
    		click(NcOrderObj.taskDetails.Errors);
    		Reusable.waitForAjax();

    		verifyExists(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
    		click(NcOrderObj.taskDetails.Workitems);
    		Reusable.waitForAjax();

    		verifyExists(NcOrderObj.workItems.WorkItemSelect," Completed Workitem");
    		click(NcOrderObj.workItems.WorkItemSelect);
    		Reusable.waitForAjax();

    		verifyExists(NcOrderObj.workItems.View,"View button");
    		click(NcOrderObj.workItems.View);
    		Reusable.waitForAjax();

    		
    		String NCServiceId=getTextFrom(NcOrderObj.workItems.NCSID);
    		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Circuit Referenceinput", NCServiceId);
    		System.out.println(NCServiceId);
    		Reusable.waitForAjax();
    		
    		getUrl(Orderscreenurl);
    		
    		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Composite Orders Tab");
    		click(NcOrderObj.generalInformation.Accountbredcrumb);
    		Reusable.waitForAjax();

    		
    		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Account Name Sorting");
    		click(NcOrderObj.generalInformation.AccountNameSorting);
    		Reusable.waitForAjax();

    		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",90);
    	
    		getUrl(Orderscreenurl);
    		
    		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Composite Orders Tab");
    		click(NcOrderObj.generalInformation.Accountbredcrumb);
    		Reusable.waitForAjax();

    		
    		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Account Name Sorting");
    		click(NcOrderObj.generalInformation.AccountNameSorting);
    		Reusable.waitForAjax();

    	}
    	
    	public void AddDummyHSHubProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    	{
    		String HubResiliencOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub Resilienc Option");
    		String Orderreferencenumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Order reference  number");
    		String NetworkReference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Network Reference");
    		String Ordernumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Order number");
    		String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service Band width");

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.OrderSystemName,"SPARK");
    		select(NewHubOrderObj.AddDummyHSHubProductdetails.OrderSystemName,"SPARK");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.Orderreferencenumber); // 871538443 add new column excel p2p - order system service id
    		sendKeys(NewHubOrderObj.AddDummyHSHubProductdetails.Orderreferencenumber,Orderreferencenumber);
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.NetworkReference);  //HAK-9876545
    		sendKeys(NewHubOrderObj.AddDummyHSHubProductdetails.NetworkReference,NetworkReference);
    		Reusable.waitForAjax();

    		if(HubResiliencOption.equalsIgnoreCase("Protected"))
    		{
    			verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.ResilienceOption,"Protected");
    			select(NewHubOrderObj.AddDummyHSHubProductdetails.ResilienceOption,"Protected");

    		}
    		else
    		{
    			verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.ResilienceOption,"Unprotected");
    			select(NewHubOrderObj.AddDummyHSHubProductdetails.ResilienceOption,"Unprotected");
    		}
    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.Topology,"Hub & Spoke");  
    		select(NewHubOrderObj.AddDummyHSHubProductdetails.Topology,"Hub & Spoke");  
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.Ordernumber,"Order number"); //excel add column -- order number //871538443/190709-0010
    		sendKeys(NewHubOrderObj.AddDummyHSHubProductdetails.Ordernumber,Ordernumber); //excel add column -- order number //871538443/190709-0010
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.CommercialProductName,"Ethernet Hub"); 
    		sendKeys(NewHubOrderObj.AddDummyHSHubProductdetails.CommercialProductName,"Ethernet Hub"); 
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.CircuitCategory,"LE");
    		select(NewHubOrderObj.AddDummyHSHubProductdetails.CircuitCategory,"LE");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.ServiceBandwidth,"Service Band width"); 
    		sendKeys(NewHubOrderObj.AddDummyHSHubProductdetails.ServiceBandwidth,ServiceBandwidth); 
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyHSHubProductdetails.Update);
    		click(NewHubOrderObj.AddDummyHSHubProductdetails.Update);
    	}
    	public void AddDummyP2PHubProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    	{
    		String ServiceBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Service Band width");
    		String HubResiliencOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub Resilienc Option");
    		String HubNetworkReference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Hub Network Reference");

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.OrderSystemName,"SPARK");
    		select(NewHubOrderObj.AddDummyP2PHubProductdetails.OrderSystemName,"SPARK");

    		String ServiceOrder="871751138/191121-0048";
    		String[] OrderSysSerid = ServiceOrder.split("/");
    		
    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.Orderreferencenumber);
    		sendKeys(NewHubOrderObj.AddDummyP2PHubProductdetails.Orderreferencenumber,OrderSysSerid[0]);
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.NetworkReference);  //HAK-9876545
    		sendKeys(NewHubOrderObj.AddDummyP2PHubProductdetails.NetworkReference,HubNetworkReference);
    		Reusable.waitForAjax();

    		if(HubResiliencOption.toString().equalsIgnoreCase("Protected"))
    		{
    			verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.ResilienceOption,"Protected");
    			select(NewHubOrderObj.AddDummyP2PHubProductdetails.ResilienceOption,"Protected");
    		}
    		else
    		{
    			verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.ResilienceOption,"Unprotected");
    			select(NewHubOrderObj.AddDummyP2PHubProductdetails.ResilienceOption,"Unprotected");
    		}
    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.Topology,"Hub & Spoke");  
    		select(NewHubOrderObj.AddDummyP2PHubProductdetails.Topology,"Hub & Spoke");  
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.Ordernumber);
    		sendKeys(NewHubOrderObj.AddDummyP2PHubProductdetails.Ordernumber,ServiceOrder); //excel add column -- order number //871538443/190709-0010
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.CommercialProductName,"Ethernet Hub"); 
    		sendKeys(NewHubOrderObj.AddDummyP2PHubProductdetails.CommercialProductName,"Ethernet Hub"); 
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.CircuitCategory,"LE");
    		select(NewHubOrderObj.AddDummyP2PHubProductdetails.CircuitCategory,"LE");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.ServiceBandwidth); 
    		sendKeys(NewHubOrderObj.AddDummyP2PHubProductdetails.ServiceBandwidth,ServiceBandwidth); 
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.AddDummyP2PHubProductdetails.Update);
    		click(NewHubOrderObj.AddDummyP2PHubProductdetails.Update);
    		
    	}
    	public void DummyHubHSDeviceDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    	{ 
    		String SiteID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SiteID");
    		String OrderscreenURL = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "OrderscreenURL");

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.NewHubSiteProductend,"verify on Hub Site End");
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.NewHubSiteProductend,"Click on Hub Site End");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.Edit,"Verify on Edit button");
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.Edit,"Click on Edit button");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.AccessTechnolgy,"Ethernet over NGN");
    		select(NewHubOrderObj.DummyHubHSDeviceDetails.AccessTechnolgy,"Ethernet over NGN");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.AccessType,"Colt Fibre");
    		select(NewHubOrderObj.DummyHubHSDeviceDetails.AccessType,"Colt Fibre");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.SiteID,"Site ID");  //a-end site id
    		sendKeys(NewHubOrderObj.DummyHubHSDeviceDetails.SiteID,SiteID);  //a-end site id
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.Update);
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.Update);
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.GeneralInformationTab,"Navigate to General Information");
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.GeneralInformationTab,"Click on General Information");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.AccessPortLink,"Verify on Access Port Link");
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.AccessPortLink,"Click on Access Port Link");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.Edit,"Edit button");
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.Edit,"Click on Edit Button");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.PresentConnectType,"LC/PC");
    		select(NewHubOrderObj.DummyHubHSDeviceDetails.PresentConnectType,"LC/PC");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.AccessportRole,"Verify Physical Port");
    		select(NewHubOrderObj.DummyHubHSDeviceDetails.AccessportRole,"Click on Physical Port");
    		Reusable.waitForAjax();

    		verifyExists(NewHubOrderObj.DummyHubHSDeviceDetails.Update,"Required Details are Entered");
    		click(NewHubOrderObj.DummyHubHSDeviceDetails.Update);
    		Reusable.waitForAjax();

    		getUrl(OrderscreenURL);
    	}

}
 