package testHarness.siebeltoNcFunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.siebeltoNCObjects.IpAccessModifyOrderObj;
import pageObjects.siebeltoNCObjects.OloIpAccessOrderObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelModeObj;
import pageObjects.siebelObjects.SiebelSpokeSiteObj;
import testHarness.commonFunctions.ReusableFunctions;

public class IpAccessModifyOrderHelper extends SeleniumUtils 
{
	ReusableFunctions Reusable = new ReusableFunctions();

	public void modTech(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		
		String Comments = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
		String Order_type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary/Secondary");
		String Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String Related_VoIP_Service = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Related_Voip_Service");
		String IPv4_6_Network_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IPv4/6 Network Address");
		String IPv4_Prefix = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IPv4 Prefix Inp");
		String LAN_VRRP_IPv4_6_Address_1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN VRRP IPv4/6 Address 1");
		String Second_CPE_LAN_Interface_IPv4_6_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Second CPE LAN Interface IPv4/6 Address");
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product");
		String Port_Role = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Port_Role");
		String VLAN_Tagging_Mode = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tagging_Mode");
		String VLAN_Tag_ID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tag_ID");

		if(Order_type.equalsIgnoreCase("empty"))
		{
            Order_type="";
        }
		switch (Comments.toString()) 
		{

		case "BW Upgrade-Siebel": 
		{   
			if(ProductName.equalsIgnoreCase("EVPN")||ProductName.equalsIgnoreCase("Ethernet Spoke"))
			{
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
				
				click(("@xpath=//li/*[text()='"+Bandwidth.toString()+"']"),"Bandwidth");
				Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidth, Keys.ENTER);
				
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}
			else
			{
				String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");
				DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Layer3_Resilience", resillenceValue);	

			/*verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			
			click(("@xpath=//li[text()='"+"Business Hours"+"']"),"Business Hours");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();*/

				if (Order_type.toString().contains("Primary") ||Order_type.toString().contentEquals("")) 
				{
					verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
					click(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
					
					click(("@xpath=//li/*[text()='"+Bandwidth.toString()+"']"),"Bandwidth");
					Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidth, Keys.ENTER);
					
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
	
					if (resillenceValue.contains("Dual Access")) 
					{
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
						waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
						javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
	
						verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
						click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
						
						verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
						click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
						click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
						
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
					}
				}
				else if (Order_type.toString().contains("Secondary"))
				{
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
					waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
					javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
					verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.BackupBandwidthDrpDwn,"BackupBandwidthDrpDwn");
					click(IpAccessModifyOrderObj.IpAccessModifyOrder.BackupBandwidthDrpDwn,"BackupBandwidthDrpDwn");
					
					click(("@xpath=//li/*[text()='"+Bandwidth.toString()+"']"),"Bandwidth");
					Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.BackupBandwidth, Keys.ENTER);
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
					verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
					click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
					click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
					
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
				}
			}
		
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			break;
		}
		
		case "Spoke BW Upgrade And VLANPortRole": 
		{   

			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn, "SerBandwidthDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn, "SerBandwidthDrpDwn");

			click(("@xpath=//li/*[text()='" + Bandwidth.toString() + "']"), "Bandwidth");
			Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidth, Keys.ENTER);
			Reusable.waitForAjax();
			if (isElementPresent(By.xpath("//button[text()='Proceed']"))) {
				click(SiebelModeObj.IPAccessObj.ProceedButton, "Proceed");
			}
			Reusable.waitForAjax();
			Reusable.waitForAjax();

			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
						
		
			verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Port Role"),"Port Role");
			click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Port Role"),"Port Role");
			click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value",Port_Role),"Port Value");
		
			if(Port_Role.equalsIgnoreCase("VLAN Port (existing)"))
			{
			verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "VLAN Tagging Mode"),"Port Role");
			click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "VLAN Tagging Mode"),"VLAN Tagging Mode");
			click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value",VLAN_Tagging_Mode),"VLAN Tag");
		

			clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "VLAN Tag ID"));
			sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "VLAN Tag ID"), VLAN_Tag_ID, "VLAN Tag ID");
			Reusable.SendkeaboardKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "VLAN Tag ID"),Keys.ENTER);
			}
			
			verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Install Time"),"Install Time");
			click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Install Time"),"Install Time");
			click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", "Business Hours"),"Install Time Value");
			Reusable.waitForSiebelLoader();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			Reusable.waitForAjax();
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			break;
		}

		case "NO-QoS to QoS (VoIP)-Siebel": 
		{
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "resillenceValue", resillenceValue);	

			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			
			click(("@xpath=//li[text()='"+"Business_Hours"+"']"),"Business Hours");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			if (resillenceValue.contains("Dual Access")) 
			{
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
				javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
				
				click(("@xpath=//li[text()='"+"Business Hours"+"']"),"Business Hours");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
				
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}


			if (Related_VoIP_Service.toString().contains("Dedicated") || Related_VoIP_Service.toString().contains("Converged"))
			{
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebRelatedVoipServiceDrpDwn,"SiebRelatedVoipServiceDrpDwn");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebRelatedVoipServiceDrpDwn,"SiebRelatedVoipServiceDrpDwn");
				
				click(("@xpath=//li/*[text()='"+Related_VoIP_Service+"']"),"Related_VoIP_Service");
				Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebRelatedVoipService, Keys.ENTER);
			
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.ServiceGrpTab,"ServiceGrpTab");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.ServiceGrpTab,"ServiceGrpTab");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.ServiceGrpNew,"ServiceGrpNew");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.ServiceGrpNew,"ServiceGrpNew");

				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.ServGrpRef,"ServGrpRef");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.ServGrpRef,"ServGrpRef");
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.AddServGrp,"AddServGrp");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.AddServGrp,"AddServGrp");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
			}
			break;
		}

		case "PA to PI IPV4-Siebel": 
		{

			String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "resillenceValue", resillenceValue);	


			//For IPv4
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.IpShowFullInfo,"IpShowFullInfo");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.IpShowFullInfo,"IpShowFullInfo");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4AddresTypeDrpDwn,"SiebIpv4AddresTypeDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4AddresTypeDrpDwn,"SiebIpv4AddresTypeDrpDwn");
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Provider Independent IP (PI)"),"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Provider Independent IP (PI)"),"Click On InstallTimeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();
			//click(("@xpath=//li/*[text()='Provider Independent IP (PI)']"),"Provider Independent");
			//Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();

			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Address,"SiebIpv4Address");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Address,"SiebIpv4Address");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Address);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Address,IPv4_6_Network_Address,"IPv4_6_Network_Address");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();

			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Prefix,"SiebIpv4Prefix");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Prefix,"SiebIpv4Prefix");
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv4Prefix,IPv4_Prefix,"IPv4_Prefix");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv4Address1,"LANVRRPIPv4Address1");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv4Address1,"LANVRRPIPv4Address1");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv4Address1);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv4Address1,LAN_VRRP_IPv4_6_Address_1,"LANVRRPIPv4Address1");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();

			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.NextHopIP,"Next Hop IP");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.NextHopIP,"Next Hop IP");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.NextHopIP);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.NextHopIP,Second_CPE_LAN_Interface_IPv4_6_Address,"Next Hop IP");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIPAccessClose,"SiebIPAccessClose");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIPAccessClose,"SiebIPAccessClose");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();

			if (resillenceValue.contains("Dual Access")) 
			{
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
				javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
				
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}

			break;
		}

		case "Add additional PI IPV6-Siebel": 
		{

			String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "resillenceValue", resillenceValue);	

			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			click(("@xpath=//li[text()='"+"Business Hours"+"']"),"Business Hours");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.IpShowFullInfo,"IpShowFullInfo");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.IpShowFullInfo,"IpShowFullInfo");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.ProviderIndependentIPv6,"ProviderIndependentIPv6");
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Address,"SiebIpv6Address");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Address,"SiebIpv6Address");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Address);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Address,IPv4_6_Network_Address,"SiebIpv6Address");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Prefix,"SiebIpv6Prefix");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Prefix,"SiebIpv6Prefix");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Prefix);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIpv6Prefix,IPv4_Prefix,"SiebIpv6Prefix");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv6Address1,"LANVRRPIPv6Address1");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv6Address1,"LANVRRPIPv6Address1");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv6Address1);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.LANVRRPIPv6Address1,LAN_VRRP_IPv4_6_Address_1,"LANVRRPIPv6Address1");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondCPELANInterfaceIPv6Address,"SecondCPELANInterfaceIPv6Address");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondCPELANInterfaceIPv6Address,"SecondCPELANInterfaceIPv6Address");

			clearTextBox(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondCPELANInterfaceIPv6Address);
			sendKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondCPELANInterfaceIPv6Address,Second_CPE_LAN_Interface_IPv4_6_Address,"SecondCPELANInterfaceIPv6Address");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIPAccessClose,"SiebIPAccessClose");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIPAccessClose,"SiebIPAccessClose");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			

			if (resillenceValue.contains("Dual Access")) 
			{
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
				javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
				
				click(("@xpath=//li[text()='"+"Business Hours"+"']"),"Business Hours");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
				
				verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
				click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
				
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
			}
			break;
		}
		
		case "BW-Upgrade-Siebel&LANIpRange-Removal": 
		{   
            String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");
            String LANIPRange_Removal= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LANIPRange_Removal");
            DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "resillenceValue", resillenceValue);    

            /*verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
            click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
            
            click(("@xpath=//li[text()='"+"Business Hours"+"']"),"Business Hours");
            Reusable.waitF    orAjax();
            Reusable.waitForpageloadmask();*/

            if (Order_type.toString().contains("Primary") ||Order_type.toString().contentEquals("")) 
            {
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
                
                click(("@xpath=//li/*[text()='"+Bandwidth.toString()+"']"),"Bandwidth");
                Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidth, Keys.ENTER);
                
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();

                if (resillenceValue.contains("Dual Access")) 
                {
                    Reusable.waitForAjax();
                    Reusable.waitForpageloadmask();
                    waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
                    javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
                    Reusable.waitForAjax();
                    Reusable.waitForpageloadmask();

                    verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
                    click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
                    
                    verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
                    click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
                    Reusable.waitForSiebelSpinnerToDisappear();
                    
                    verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
                    click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
                    
                    Reusable.waitForAjax();
                    Reusable.waitForpageloadmask();
                }
            }
            else if (Order_type.toString().contains("Secondary"))
            {
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
                
                waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
                javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
                
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.BackupBandwidthDrpDwn,"BackupBandwidthDrpDwn");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.BackupBandwidthDrpDwn,"BackupBandwidthDrpDwn");
                
                click(("@xpath=//li/*[text()='"+Bandwidth.toString()+"']"),"Bandwidth");
                Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.BackupBandwidth, Keys.ENTER);
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
                
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
                
                verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
                click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
                Reusable.waitForSiebelSpinnerToDisappear();
                
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
                
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
                
            }
        
            verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
            click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
            Reusable.waitForAjax();
            Reusable.waitForpageloadmask();
            
            //Lan IpRange Removal
            
            verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
            click(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
            
            if(LANIPRange_Removal.equalsIgnoreCase("IPv4"))
            {
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.LANIPv4Remove,"Ipv4 Component");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.LANIPv4Remove,"Ipv4 Component");
                Reusable.waitForSiebelLoader();
            }    
            else if(LANIPRange_Removal.equalsIgnoreCase("IPv6"))
            {
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.LANIPv6Remove,"Ipv6 Component");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.LANIPv6Remove,"Ipv6 Component");
                Reusable.waitForSiebelLoader();
            }    

            verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
            click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
            Reusable.waitForSiebelLoader();
            Reusable.ClickHereSave();

            break;
        }
		
		case "Modify BGP":
		{
			String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Layer3_Resilience", resillenceValue);
			String BGPValue= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BGP Value");
			String TTLSecurity= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "TTL_Security");
	
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
			Reusable.waitForpageloadmask();
			           
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.IpShowFullInfo,"IpShowFullInfo");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.IpShowFullInfo,"IpShowFullInfo");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			if(!BGPValue.equalsIgnoreCase("empty"))
			{  
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebBgpFeedTypeDrpDwn,"SiebBgpFeedTypeDrpDwn");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebBgpFeedTypeDrpDwn,"SiebBgpFeedTypeDrpDwn");
				scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BGPValue));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BGPValue));
				//ClickonElementByString("//*[text()='"+BGP4_Feed_Type+"']",10);
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebBgpFeedType,Keys.ENTER);
				Reusable.waitForAjax();
			}
			if(!TTLSecurity.equalsIgnoreCase("empty"))
			{
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.TTL,"TTL");
				click(OloIpAccessOrderObj.OloIpAccessOrder.TTL,"TTL");
				Reusable.waitForAjax();
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", TTLSecurity));
				Reusable.waitForAjax();
				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					click(SiebelModeObj.IPAccessObj.ProceedButton,"Proceed");
				}
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				Reusable.waitForAjax();
			}
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIPAccessClose,"SiebIPAccessClose");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SiebIPAccessClose,"SiebIPAccessClose");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			if (resillenceValue.contains("Dual Access"))
			{
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
				javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
               
                verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
                click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
                Reusable.waitForSiebelSpinnerToDisappear();
               
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
               
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
               
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
               
            }
		}
		break;
		
		case "Add BFD": 
		{   
			String Bespoke_Reference = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bespoke_Reference");
			String resillenceValue= getAttributeFrom(IpAccessModifyOrderObj.IpAccessModifyOrder.Inputlayer3resillence,"value");

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceCheckbox,"Bespoke Reference checkbox");
			click(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceCheckbox,"Select Bespoke Reference checkbox");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceValue,"Bespoke Reference value");
			click(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceValue,"Bespoke Reference value");
			sendKeys(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceValue,Bespoke_Reference, "Bespoke Reference value");
			Reusable.waitForSiebelLoader();

			Reusable.waitForAjax();
			Reusable.waitForAjax();

			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		
			verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Install Time"),"Install Time");
			click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Install Time"),"Install Time");
			click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", "Business Hours"),"Install Time Value");
			Reusable.waitForSiebelLoader();
			
			verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
			Reusable.waitForAjax();
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			if (resillenceValue.contains("Dual Access"))
			{
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,10);
				javaScriptclick(IpAccessModifyOrderObj.IpAccessModifyOrder.SecondarySite,"SecondarySite");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.InstallTimeDrpDwn,"InstallTimeDrpDwn");
               
                verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Verify InstallTimeSelectAccess");
                click(SiebelModeObj.IPAccessObj.searchInput.replace("Value", "Business Hours"),"Click On InstallTimeSelectAccess");
                Reusable.waitForSiebelSpinnerToDisappear();
               
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
               
                verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
                click(IpAccessModifyOrderObj.IpAccessModifyOrder.PrimarySite,"PrimarySite");
               
                Reusable.waitForAjax();
                Reusable.waitForpageloadmask();
               
            }
		}
		break;

		default: 
		{
			break;
		}
		}
	}

}
