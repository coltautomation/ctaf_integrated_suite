package testHarness.siebeltoNcFunctions;


import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.NcOrderObj;
import pageObjects.ncObjects.NewHubOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class NewHubOrder extends SeleniumUtils
{
	public static ThreadLocal<String> SpokeOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> HubOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();

	NcCreateOrder order=new NcCreateOrder();
	ReusableFunctions Reusable=new ReusableFunctions();
	
	public void AddHub(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String NCServiceId = getTextFrom(NewHubOrderObj.NewHubOrder.NCSID);
 		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Service_Id", NCServiceId);

		verifyExists(NewHubOrderObj.NewHubOrder.NewEthernetConnProduct,"Ethernet Connection Product Order");
		click(NewHubOrderObj.NewHubOrder.NewEthernetConnProduct,"Ethernet Connection Product Order");
		
		verifyExists(NewHubOrderObj.NewHubOrder.AddHub,"Add Hub button");
		click(NewHubOrderObj.NewHubOrder.AddHub,"Add Hub button");
		
		verifyExists(NewHubOrderObj.NewHubOrder.TypeofHub,"Service Id");
	    sendKeys(NewHubOrderObj.NewHubOrder.TypeofHub,NCServiceId); 
		waitForAjax();
		
		verifyExists(NewHubOrderObj.NewHubOrder.AddHubProdOrder,"Hub Order");
		click(NewHubOrderObj.NewHubOrder.AddHubProdOrder,"Hub Order");
		
		verifyExists(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
		click(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
		waitForAjax();
		openurl(SpokeOrderscreenurl.get());
		
	}
	
	public void AddHubProduct(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException
	{
		String productValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Product");
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		
		if(productValue.equalsIgnoreCase("Hub Product"))
		{
			verifyExists(NewHubOrderObj.NewHubOrder.HubProductCheckBox,"Hub Option");
			click(NewHubOrderObj.NewHubOrder.HubProductCheckBox,"Hub Option");
		}
		else
		{
			Reporter.log("Not a valid option");
		}
		
		verifyExists(NewHubOrderObj.NewHubOrder.Addbutton,"Add Button");
		click(NewHubOrderObj.NewHubOrder.Addbutton,"Add Button");
		
		verifyExists(NewHubOrderObj.NewHubOrder.UnderlyningHubOrder,"Under Lying Hub Order");
		click(NewHubOrderObj.NewHubOrder.UnderlyningHubOrder,"Under Lying Hub Order");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
	}
	
	public void HubProductDeviceDetails(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException 
	{ 
		String AccessTechnolgyValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Access Technology");
		String AccessTypeValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Access type");
		String SiteIDValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Hub Site ID");
		String PresentConnectTypeValue = "LC/LP";
		//String PresentConnectTypeValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "");
		String AccessportRoleValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Port Role");
		String CabinateIDValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Hub Cabinet ID");
		String CabinateTypeValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Hub Cabinet Type");
		
		verifyExists(NewHubOrderObj.NewHubOrder.HubSiteProductend,"New Hub Site Product End");
		click(NewHubOrderObj.NewHubOrder.HubSiteProductend,"New Hub Site Product End");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		
		verifyExists(NewHubOrderObj.NewHubOrder.AccessTechnolgy,"Access Technolgy");
		click(AccessTechnolgyValue,"Access Technolgy");
		
		verifyExists(NewHubOrderObj.NewHubOrder.AccessType,"Access Type");
		click(AccessTypeValue,"Access Type");
		
		verifyExists(NewHubOrderObj.NewHubOrder.SiteID,"Site ID");
		sendKeys(NewHubOrderObj.NewHubOrder.SiteID,SiteIDValue,"Site ID");		
		
		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
		click(NewHubOrderObj.NewHubOrder.Update,"Update");
	
		verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
		click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
		click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		
		verifyExists(NewHubOrderObj.NewHubOrder.PresentConnectType,"Present Connect Type");
		click(PresentConnectTypeValue,"Present Connect Type");
		
		verifyExists(NewHubOrderObj.NewHubOrder.AccessportRole,"Access port Role");
		click(AccessportRoleValue,"Access port Role");
		
		verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
		click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
		click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
		
		//navgate to composite order
		
		verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
		click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(NewHubOrderObj.NewHubOrder.CPElink,"CPE link");
		click(NewHubOrderObj.NewHubOrder.CPElink,"CPElink");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		
		verifyExists(NewHubOrderObj.NewHubOrder.CabinateID,"Cabinate ID");
		sendKeys(NewHubOrderObj.NewHubOrder.CabinateID,CabinateIDValue,"Cabinate ID");
		
		verifyExists(NewHubOrderObj.NewHubOrder.CabinateType,"Cabinate Type");
		click(CabinateTypeValue,"Cabinate Type");
		
		verifyExists(NewHubOrderObj.NewHubOrder.CPElink,"CPE link");
		click(NewHubOrderObj.NewHubOrder.CPElink,"CPE link");
	}
	
	 public void AddSpokeProduct(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	    {		
		 	String product=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Product");
		 
		 	verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AddonOrderTab,"Addon Order Tab");
			click(NewHubOrderObj.NewHubOrder.AddonOrderTab,"Addon Order Tab");
			
			if(product.equalsIgnoreCase("Ethernet Connection Product"))
			{
				verifyExists(NewHubOrderObj.NewHubOrder.EthernetProductCheckBox,"Ethernet Product CheckBox");
				click(NewHubOrderObj.NewHubOrder.EthernetProductCheckBox,"Ethernet Product CheckBox");
			}
		    else
		    {
				Reporter.log("Not a valid option");
		    }
			
			verifyExists(NewHubOrderObj.NewHubOrder.Addbutton,"Add button");
			click(NewHubOrderObj.NewHubOrder.Addbutton,"Add button");
		
			verifyExists(NewHubOrderObj.NewHubOrder.UnderlyningOrder,"Under Lying Order");
			click(NewHubOrderObj.NewHubOrder.UnderlyningOrder,"Under Lying Order");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
	    }
	 
	 
	 public void SpokeProductDeviceDetails(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
		{ 
			String PresentConnectTypeValue = "LC/LP";
			String AccessportRoleValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Port Role");
			String VLanTaggingModeValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Vlan Tagging Mode");
			String TypeofFeatureValue = "VLAN";
			String EthertypeValue = "VLAN (0x8100)";
			String VLanTagIdValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Vlan Tag Id");
			String AccessTechnolgyValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Access Technology");
			String AccessTypeValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Access type");
			String ResilienceOptionValue1=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Resilience Option");
			String ResilienceOptionValue2 = "Unprotected";
			String SiteEndValue = "B END";
			String SiteIDValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Spoke Site ID");

			//String PresentConnectTypeValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "");
		 
		 	verifyExists(NewHubOrderObj.NewHubOrder.NewEndSiteProductAend,"A end Site Product Order");
			click(NewHubOrderObj.NewHubOrder.NewEndSiteProductAend,"A end Site Product Order");
			
			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information tab");
			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port link");
			click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port link");
		
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		
			verifyExists(NewHubOrderObj.NewHubOrder.PresentConnectType,"Present Connect Type");
			click(PresentConnectTypeValue,"Present Connect Type");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccessportRole,"Access port Role");
			click(AccessportRoleValue,"Access port Role");
			
			verifyExists(NewHubOrderObj.NewHubOrder.VlanTaggingMode,"VLan Tagging Mode");
			click(VLanTaggingModeValue,"VLan Tagging Mode");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
			click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
			
			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
			click(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
			
			verifyExists(NewHubOrderObj.NewHubOrder.TypeofFeature,"Type of Feature");
			sendKeys(NewHubOrderObj.NewHubOrder.TypeofFeature,TypeofFeatureValue,"Type of Feature");
			waitForAjax();
			SendkeyusingAction(Keys.ENTER);
			
			verifyExists(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
			click(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN link");
			click(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN link");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Ethertype,"Ether type");
			clearTextBox(NewHubOrderObj.NewHubOrder.Ethertype);
			//getwebelement(xml.getlocator("//locators/SiteProductDetail/VLAN/Ethertype")).clear();
			sendKeys(NewHubOrderObj.NewHubOrder.Ethertype,EthertypeValue,"Ether type");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.VlanTagId,"VLan Tag Id");
			sendKeys(NewHubOrderObj.NewHubOrder.VlanTagId,VLanTagIdValue,"VLan Tag Id");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
			waitForAjax();
			
			//Required Details are Entered
			openurl(SpokeOrderscreenurl.get());
			//Navigate to Spoke Composite Order
			
			verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.NewEndSiteProductBend,"B end Site Product Order");
			click(NewHubOrderObj.NewHubOrder.NewEndSiteProductBend,"B end Site Product Order");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccessTechnolgy,"Access Technolgy");
			click(AccessTechnolgyValue,"Access Technolgy");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccessType,"Access Type");
			click(AccessTypeValue,"Access Type");
			
			if(ResilienceOptionValue1.equalsIgnoreCase("Protected"))
			{
				verifyExists(NewHubOrderObj.NewHubOrder.ResilienceOption,"Resilience Option Protected");
				click(NewHubOrderObj.NewHubOrder.ResilienceOption,"Resilience Option Protected");
			}
			else
			{
				verifyExists(NewHubOrderObj.NewHubOrder.ResilienceOption,"Resilience Option Unprotected");
				click(ResilienceOptionValue2,"Resilience Option Unprotected");
			}
			
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.SiteEnd,"B END");
			click(SiteEndValue,"B END");
		
			verifyExists(NewHubOrderObj.NewHubOrder.SiteID,"Site ID");
			sendKeys(NewHubOrderObj.NewHubOrder.SiteID,SiteIDValue,"Site ID");
			//Required Details are Entered
			
			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
			
			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
			click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccessportRole,"Access port Role");
			click(AccessportRoleValue,"Access port Role");
			waitForAjax();
			
			verifyExists(NewHubOrderObj.NewHubOrder.PresentConnectType,"Present Connect Type");
			click(PresentConnectTypeValue,"Present Connect Type");
			waitForAjax();
			//Required Details are Entered
			
			if(TypeofFeatureValue.contains("VLAN"))
			{
				verifyExists(NewHubOrderObj.NewHubOrder.VlanTaggingMode,"VLan Tagging Mode");
				click(VLanTaggingModeValue,"VLan Tagging Mode");
				//Required Details are Entered
				verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
				click(NewHubOrderObj.NewHubOrder.Update,"Update");
				
				verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
				click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
				
				verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
				click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
				waitForAjax();
				
				verifyExists(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
				click(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(NewHubOrderObj.NewHubOrder.TypeofFeature,"Type of Feature");
				sendKeys(NewHubOrderObj.NewHubOrder.TypeofFeature,TypeofFeatureValue,"Type of Feature");
				SendkeyusingAction(Keys.ENTER);
				waitForAjax();
				
				//VLAN Feature added
				
				verifyExists(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN Link");
				click(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN Link");
				
				verifyExists(NewHubOrderObj.NewHubOrder.VlanTagId,"Vlan Tag Id");
				sendKeys(NewHubOrderObj.NewHubOrder.VlanTagId,VLanTagIdValue,"Vlan Tag Id");
				waitForAjax();
				
		//		Select(getwebelement(xml.getlocator("//locators/SiteProductDetail/VLAN/Ethertype")),"VLAN (0x8100)");
				
				verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
				 click(NewHubOrderObj.NewHubOrder.Update,"Update");
				 waitForAjax();
				 
				//Required Details are Entered
			}
			else 
				{
				 verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
				 click(NewHubOrderObj.NewHubOrder.Update,"Update");
				 waitForAjax();
				}
			openurl(SpokeOrderscreenurl.get());
		}
	 
	 public void ProcessSpokeOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
		{
		 	String SpokeOrdernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
  			DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Spoke_Order_No", SpokeOrdernumber);
		 	String[] arrOfStr = SpokeOrdernumber.split("#", 0); 
		 	verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
			//Navigated to Composite Orders Tab
			
			verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
			verifyExists(NewHubOrderObj.NewHubOrder.arrStr1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.arrStr2,"Spoke order");
			click(NewHubOrderObj.NewHubOrder.arrStr1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.arrStr2,"Spoke order");
		    //Select the Spoke Order
			
			verifyExists(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Processing button");
			click(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Processing button");
		}
	 
	 public void AddHubProductdetails(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
		{
		 	String ResilienceOptionValue1=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Resilience Option");
		 	String OrderSysRefIDValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Order Sys Ref ID");
		 	String NetworkRefValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Network Ref");
		 	String OrderSystemNameValue="SPARK";
		 	String TopologyValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Topology");
		 	String OrderNumberValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Order Number");
		 	String CommercialProductNameValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Commercial Product Name");
		 	String ServiceBandwidthValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "SerBand");
		 	String CircuitCategoryValue="LE";
		    String ResilienceOptionValue2="Unprotected";
		    
		 	verifyExists(NewHubOrderObj.NewHubOrder.OrderSystemName,"OrderSystemName");
			click(OrderSystemNameValue,"OrderSystemName");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Orderreferencenumber,"Orderreferencenumber");
			sendKeys(NewHubOrderObj.NewHubOrder.Orderreferencenumber,OrderSysRefIDValue,"Orderreferencenumber");
			
			verifyExists(NewHubOrderObj.NewHubOrder.NetworkReference,"Network Reference Number");
			sendKeys(NewHubOrderObj.NewHubOrder.NetworkReference,NetworkRefValue,"Network Reference Number");
			
			if(ResilienceOptionValue1.equalsIgnoreCase("Protected"))
			{
				verifyExists(NewHubOrderObj.NewHubOrder.ResilienceOption,"Resilience Option");
				click(ResilienceOptionValue1,"Resilience Option");
			}
			else
			{
				verifyExists(NewHubOrderObj.NewHubOrder.ResilienceOption,"Account Name Sorting");
				click(ResilienceOptionValue2,"Resilience Option");
			}
			Thread.sleep(20000);
			
			verifyExists(NewHubOrderObj.NewHubOrder.Topology,"Topology");
			click(TopologyValue,"Topology");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Ordernumber1+OrderNumberValue+NewHubOrderObj.NewHubOrder.Ordernumber2,"Order number");
			sendKeys(NewHubOrderObj.NewHubOrder.Ordernumber1+OrderNumberValue+NewHubOrderObj.NewHubOrder.Ordernumber2,"Order number");
			
			verifyExists(NewHubOrderObj.NewHubOrder.CommercialProductName,"Commercial Product Name");
			sendKeys(NewHubOrderObj.NewHubOrder.CommercialProductName,CommercialProductNameValue,"Commercial Product Name");
			
			verifyExists(NewHubOrderObj.NewHubOrder.ServiceBandwidth,"Service Bandwidth");
			sendKeys(NewHubOrderObj.NewHubOrder.ServiceBandwidth,ServiceBandwidthValue,"Service Bandwidth");
			
			verifyExists(NewHubOrderObj.NewHubOrder.CircuitCategory,"Circuit Category");
			click(CircuitCategoryValue,"Circuit Category");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
			waitForAjax();
			//Required Details are Updated
		}

         public void DecomposeHubOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	    {
			
		 	String OrderNumberValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Order Number");
		 	openurl(Orderscreenurl.get());
			
			verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			//Navgiate to Orders Tab
			verifyExists(NewHubOrderObj.NewHubOrder.HubSuborder,"Hub Sub order");
			click(NewHubOrderObj.NewHubOrder.HubSuborder,"Hub Sub order");
		
			//verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	//click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
			verifyExists(NewHubOrderObj.NewHubOrder.Ordernumber1+OrderNumberValue+NewHubOrderObj.NewHubOrder.Ordernumber2,"Order number");
			click(NewHubOrderObj.NewHubOrder.Ordernumber1+OrderNumberValue+NewHubOrderObj.NewHubOrder.Ordernumber2,"Order number");

			verifyExists(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
			click(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
			waitForAjax();
	    }
         
         
         
         public void CreatHubOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
     	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);
     		
        	verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"NewCompositeOrder");
 			click(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"NewCompositeOrder");
 			String OrderDesc="Hub Order Created using Automation script";
     		String[] arrOfStr = Ordernumber.split("#", 0); 
     		Reporter.log(arrOfStr[1]);
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.OrderDescription,"Order Description");
     		sendKeys(NewHubOrderObj.NewHubOrder.OrderDescription,OrderDesc,"Order Description");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
			
     		Reporter.log(Orderscreenurl.get());
     	}

     	
     	  public void AddSpokeFeatureDetails(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
     	    {
     		  	String feature="End Site Product";
     		  	String TypeofFeatureValue = "VLAN";
     		  	verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
     		  	click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
     			waitForAjax();

     			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
    			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
    			
     			verifyExists(NewHubOrderObj.NewHubOrder.TypeofFeature,"Type of Feature");
     			sendKeys(NewHubOrderObj.NewHubOrder.TypeofFeature,feature,"Type of Feature");
     			waitForAjax();
     			
     			verifyExists(NewHubOrderObj.NewHubOrder.TypeofFeature,"Type of Feature");
				sendKeys(NewHubOrderObj.NewHubOrder.TypeofFeature,TypeofFeatureValue,"Type of Feature");
				SendkeyusingAction(Keys.ENTER);
     			
     			verifyExists(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
     			sendKeys(NewHubOrderObj.NewHubOrder.SelectFeature,feature,"Select Feature");
     			waitForAjax();
     		}
     	  
     	 public void AddSpokeProductdetails(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
         {
 		 	String OrderreferencenumberValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Product");
 			String TopologyValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Topology");
		 	String OrderNumberValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Order Number");
		 	String CommercialProductNameValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "Commercial Product Name");
		 	String ServiceBandwidthValue=DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo, "SerBand");
		 	String CircuitCategoryValue="LE";
		 	String OrderSystemNameValue="SPARK";
		 	

 			verifyExists(NewHubOrderObj.NewHubOrder.OrderSystemName,"Order System Name");
			click(OrderSystemNameValue,"Order System Name");

			verifyExists(NewHubOrderObj.NewHubOrder.Orderreferencenumber,"Order reference number");
			sendKeys(NewHubOrderObj.NewHubOrder.Orderreferencenumber,OrderreferencenumberValue,"Order reference number");

			verifyExists(NewHubOrderObj.NewHubOrder.Topology,"Topology");
			click(TopologyValue,"Topology");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Ordernumber1+OrderNumberValue+NewHubOrderObj.NewHubOrder.Ordernumber2,"Order number");
			sendKeys(NewHubOrderObj.NewHubOrder.Ordernumber1+OrderNumberValue+NewHubOrderObj.NewHubOrder.Ordernumber2,"Order number");
			
			verifyExists(NewHubOrderObj.NewHubOrder.CommercialProductName,"Commercial Product Name");
			sendKeys(NewHubOrderObj.NewHubOrder.CommercialProductName,CommercialProductNameValue,"Commercial Product Name");
				
			verifyExists(NewHubOrderObj.NewHubOrder.CircuitCategory,"Circuit Category");
			click(CircuitCategoryValue,"Circuit Category");
			
			verifyExists(NewHubOrderObj.NewHubOrder.ServiceBandwidth,"Service Bandwidth");
			sendKeys(NewHubOrderObj.NewHubOrder.ServiceBandwidth,ServiceBandwidthValue,"Service Bandwidth");
			
     		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
        	} 
     	 
     	
     	 
     	public void GotoSpokeErrors(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
    	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);

     		String[] arrOfStr = Ordernumber.split("#", 0);
     		String Errors=getTextFrom(NewHubOrderObj.NewHubOrder.Errors1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.Errors2);
     		
    		openurl(SpokeOrderscreenurl.get());
    		
    		//Navigate to Composite Order
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
          	//Get the value of In Error column for the order");
    		if(Errors.equalsIgnoreCase("Blocked by Errors"))
    		{
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
    			click(NewHubOrderObj.NewHubOrder.CompositeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.CompositeOrder2,"Composite order");
    			//verifyExists(Errors);
    			//click(Errors);
    			//Click on Composite Order
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    			click(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    		
    			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			waitForAjax();
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			waitForAjax();
    		}
    		else 
    		{
    			Reporter.log("Not required to Navigate to Errors tab");
    			//Spoke Order did not have any errors to be Captured
    		}
    	}
     	
     	public void CompleteHubOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
    	{
    		waitForAjax();
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);

        	String NCServiceId = getTextFrom(NewHubOrderObj.NewHubOrder.NCSID);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Service_Id", NCServiceId);

        	String TaskTitle = getTextFrom(NewHubOrderObj.NewHubOrder.TaskTitle);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Task_Title", TaskTitle);
        	
  			String WorkItems = DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo,"Work items");

     		
    		String[] arrOfStr = Ordernumber.split("#", 0); 

    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
			click(Ordernumber);
    		//click(NewHubOrderObj.NewHubOrder.HubOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.HubOrder2));
    		//Click on the Hub Order
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"Task Tab");
			click(NewHubOrderObj.NewHubOrder.TaskTab,"Task Tab");
    		//Navigate to Tasks Tab
			
			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flow link");
			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flow link");
    		
    		waitForAjax();
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Workitems,"Workitems Tab");
			click(NewHubOrderObj.NewHubOrder.Workitems,"Workitems Tab");
    		
			waitForAjax();
    		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
    		{
    			verifyExists(NewHubOrderObj.NewHubOrder.TaskReadytoComplete,"Task Ready to Complete");
    			click(NewHubOrderObj.NewHubOrder.TaskReadytoComplete,"Task Ready to Complete");
    			waitForAjax();
    			CompletHubworkitem(dataFileName, SheetName, scriptNo, dataSetNo,TaskTitle);

    		}
    		waitForAjax();
    		verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
		 	click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.Workitems,"Workitems");
		 	click(NewHubOrderObj.NewHubOrder.Workitems,"Workitems");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.WorkItemSelect,"Work Item Select");
		 	click(NewHubOrderObj.NewHubOrder.WorkItemSelect,"Work Item Select");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.View,"View");
		 	click(NewHubOrderObj.NewHubOrder.View,"View");
    		
    		Reporter.log(NCServiceId);
    		//Pick the NC Service ID
    		
    		openurl(Orderscreenurl.get());
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
    		waitForAjax();
        //	Clickon(getwebelement("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"));
    		
    	//	waitandclickForOrderCompleted("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",90);
    		clickAndWait(NewHubOrderObj.NewHubOrder.HubOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.HubOrder2);
    		//Waiting for Hub Order to be Completed");
    		openurl(Orderscreenurl.get());
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		//Navigate to Composite Orders Tab");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			waitForAjax();
			
    	}
     	
     	public void ProcessHubOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
    	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "Order Number", Ordernumber);

    		openurl(Orderscreenurl.get());
    		//Navigate to Hub Order screen
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
			String[] arrOfStr = Ordernumber.split("#", 0);
    		click(NewHubOrderObj.NewHubOrder.hubo1+arrOfStr+NewHubOrderObj.NewHubOrder.hubo2);
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Proccessing");
		 	click(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Proccessing");	
    	}
     	
     	 public void CreatCompositSpokeOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
         {
//         	String[] arrOfStr = Ordernumber.split("#", 0);
//         	if((//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::*/following-sibling::*/following-sibling::*).equals("Process Completed"))
//         			{
     		 
     		String SpokeOrdernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
  			DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Spoke_Order_No", SpokeOrdernumber); 
  			String OrderDescription="Spoke Order Created using Automation script";
  			  			
  			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
			click(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
     	
     		Reporter.log(SpokeOrdernumber);

     		String[] arrOfStr = SpokeOrdernumber.split("#", 0); 
     		Reporter.log(arrOfStr[1]);
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.OrderDescription,"Order Description");
     		sendKeys(NewHubOrderObj.NewHubOrder.OrderDescription,OrderDescription,"Order Description");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
			click(NewHubOrderObj.NewHubOrder.Update,"Update");
			
     		Reporter.log(SpokeOrderscreenurl.get());
         }
     	 
     	 
     	public void GotoHubErrors(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
    	{
     		String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "Order Number", Ordernumber);
     		
    		String[] arrOfStr = Ordernumber.split("#", 0);
     		String Errors = getTextFrom(NewHubOrderObj.NewHubOrder.Errors1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.Errors2);

     		
    		openurl(Orderscreenurl.get());
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		//Navigate to Accounts Composite Orders Tab
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
    		if(Errors.equalsIgnoreCase("Blocked by Errors"))
    		{
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
    			click(NewHubOrderObj.NewHubOrder.CompositeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.CompositeOrder2,"Composite order");
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    			click(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    		
    			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			waitForAjax();
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			waitForAjax();
    		}
    		else 
    		{
    			Reporter.log("Not required to Navigate to Errors tab");
    			//Hub Order did not have any errors to be Captured
    		}
    	}
     	
     	 public void DecomposeSpokeOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
     	{
     		openurl(SpokeOrderscreenurl.get());
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Orders Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Orders Tab");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Suborder,"Suborder");
			click(NewHubOrderObj.NewHubOrder.Suborder,"Suborder");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
			click(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
     		
     		waitForAjax();
     	}
     	 
     	public void CompleteSpokeOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
    	{
     		String TaskTitle = getTextFrom(NewHubOrderObj.NewHubOrder.TaskTitle);
     		DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Task_Title", TaskTitle);
     		
     		String SpokeOrdernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
  			DataMiner.fnsetcolvalue(SheetName, scriptNo, dataSetNo, "NC_Spoke_Order_No", SpokeOrdernumber);
  			
  			String WorkItems = DataMiner.fngetcolvalue(SheetName, scriptNo, dataSetNo,"Work items");
  			
    		String[] arrOfStr = SpokeOrdernumber.split("#", 0); 
    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
    		
    		click(NewHubOrderObj.NewHubOrder.SpokeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.SpokeOrder2,"Spoke Order");

    		verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"Task Tab");
			click(NewHubOrderObj.NewHubOrder.TaskTab,"Task Tab");
    		//Navigate to Tasks Tab
			
			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flow link");
			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flow link");
    		waitForAjax();
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Workitems,"Workitems Tab");
			click(NewHubOrderObj.NewHubOrder.Workitems,"Workitems Tab");
			waitForAjax();
			
    		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
    		{
    			verifyExists(NewHubOrderObj.NewHubOrder.TaskReadytoComplete,"Task Ready to Complete");
    			click(NewHubOrderObj.NewHubOrder.TaskReadytoComplete,"Task Ready to Complete");
    			waitForAjax();
    			CompletSpokeworkitem(dataFileName, SheetName, scriptNo, dataSetNo,TaskTitle);
    		}
    		
    		openurl(HubOrderscreenurl.get());
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		//Navigate to Composite Orders Tab");
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
    		waitForAjax();
    		
    		waitForElementToAppear(NewHubOrderObj.NewHubOrder.processCompleted1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.processCompleted2, 60);
    		//Waiting for Spoke Order to be Completed
    		openurl(HubOrderscreenurl.get());
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		//Navigate to Composite Orders Tab");
    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			waitForAjax();
    	}
     	
     	
     	public void CompletHubworkitem(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String taskTitle) throws InterruptedException, AWTException, IOException
    	{
     		String ResilienceOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "End Resilience Option");
    		String AccessNwElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Access nwElement");
    		String AccessPort = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Access port");
    		String CPENNiPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CPE NNI Port 1");
    		String CPENNiPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CPE NNI Port 2");
    	
     		System.out.println("In Switch case with TaskName :"+taskTitle);
    		switch(taskTitle)
    		{
			case "Reserve Access Resources":
			if (ResilienceOption.contains("Proctected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.workItems.AccessNetworkElement,"Access Network Element");
				clearTextBox(NcOrderObj.workItems.AccessNetworkElement);
				sendKeys(NcOrderObj.workItems.AccessNetworkElement,AccessNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AccessPort,"Access Port");
				clearTextBox(NcOrderObj.workItems.AccessPort);
				sendKeys(NcOrderObj.workItems.AccessPort,AccessPort);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort1,"CPE NNI Port1");
				clearTextBox(NcOrderObj.workItems.CPENNIPort1);
				sendKeys(NcOrderObj.workItems.CPENNIPort1,CPENNiPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort1, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort2,"CPE NNI Port2");
				clearTextBox(NcOrderObj.workItems.CPENNIPort2);
				sendKeys(NcOrderObj.workItems.CPENNIPort2,CPENNiPort2);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort2, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
			/*	verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();*/
			}
			else
			{
				//Code for Unprotected
				verifyExists(NcOrderObj.workItems.AccessNetworkElement,"Access Network Element");
				clearTextBox(NcOrderObj.workItems.AccessNetworkElement);
				sendKeys(NcOrderObj.workItems.AccessNetworkElement,AccessNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AccessPort,"Access Port");
				clearTextBox(NcOrderObj.workItems.AccessPort);
				sendKeys(NcOrderObj.workItems.AccessPort,AccessPort);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort1,"CPE NNI Port");
				clearTextBox(NcOrderObj.workItems.CPENNIPort1);
				sendKeys(NcOrderObj.workItems.CPENNIPort1,CPENNiPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort1, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

			/*	verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();*/
			}
			break;
			
			case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
				{
					verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
				}
				else
				{
					verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
				}
				Reusable.waitForSiebelLoader();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
    		
    		}
    	}
     	
     	
     	public void CompletSpokeworkitem(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String taskTitle) throws InterruptedException, AWTException, IOException
    	{
     		String ResilienceOption = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "End Resilience Option");
    		String AccessNwElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Access nwElement");
    		String AccessPort = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Access port");
    		String CPENNiPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CPE NNI Port 1");
    		String CPENNiPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CPE NNI Port 2");
    		String PeNwElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PE NetworkElement");
    		String PEPort1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PEPort_1");
    		String PEPort2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PEPort_2");
    		String VcxControl = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "VCX Control");
    		String Beacon = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Beacon");
    		String LegacyCpeProfile= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LegacyCpeProfile");
    		String LegacyCpeOamLevel= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LegacyCpeOamLevel");
    		String OAMProfile= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "OAM Profile");

    	    String LegacyCpeSupportsCFM="yes";
    				
    	
     		System.out.println("In Switch case with TaskName :"+taskTitle);
    		switch(taskTitle)
    		{
			case "Reserve Access Resources":
			if (ResilienceOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.workItems.AccessNetworkElement,"Access Network Element");
				clearTextBox(NcOrderObj.workItems.AccessNetworkElement);
				sendKeys(NcOrderObj.workItems.AccessNetworkElement,AccessNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AccessPort,"Access Port");
				clearTextBox(NcOrderObj.workItems.AccessPort);
				sendKeys(NcOrderObj.workItems.AccessPort,AccessPort);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort1,"CPE NNI Port1");
				clearTextBox(NcOrderObj.workItems.CPENNIPort1);
				sendKeys(NcOrderObj.workItems.CPENNIPort1,CPENNiPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort1, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort2,"CPE NNI Port2");
				clearTextBox(NcOrderObj.workItems.CPENNIPort2);
				sendKeys(NcOrderObj.workItems.CPENNIPort2,CPENNiPort2);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort2, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
			/*	verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();*/
			}
			else
			{
				//Code for Unprotected
				verifyExists(NcOrderObj.workItems.AccessNetworkElement,"Access Network Element");
				clearTextBox(NcOrderObj.workItems.AccessNetworkElement);
				sendKeys(NcOrderObj.workItems.AccessNetworkElement,AccessNwElement);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AccessPort,"Access Port");
				clearTextBox(NcOrderObj.workItems.AccessPort);
				sendKeys(NcOrderObj.workItems.AccessPort,AccessPort);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.CPENNIPort1,"CPE NNI Port");
				clearTextBox(NcOrderObj.workItems.CPENNIPort1);
				sendKeys(NcOrderObj.workItems.CPENNIPort1,CPENNiPort1);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.CPENNIPort1, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

			/*	verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();*/
			}
			break;
			
			case "Transport Circuit Design":
				if (ResilienceOption.contains("Protected"))
				{
					//Code for Protected
					verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
					clearTextBox(NcOrderObj.workItems.PENetworkElement);
					sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
					clearTextBox(NcOrderObj.workItems.VCXController);
					sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
					clearTextBox(NcOrderObj.workItems.Beacon);
					sendKeys(NcOrderObj.workItems.Beacon,Beacon);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(NcOrderObj.createOrder.Update,"Update button");
					click(NcOrderObj.createOrder.Update,"Update button");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

					verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
					click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
					click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
					
					verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
					click(NcOrderObj.addProductDetails.Edit,"Edit button");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.PePort,"PE Port");
					clearTextBox(NcOrderObj.workItems.PePort);
					sendKeys(NcOrderObj.workItems.PePort,PEPort1);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(NcOrderObj.createOrder.Update,"Update button");
					click(NcOrderObj.createOrder.Update,"Update button");
					Reusable.waitForSiebelLoader();
					
					verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
					click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

					verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
					click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
					click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
					
					verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
					click(NcOrderObj.addProductDetails.Edit,"Edit button");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.PePort,"PE Port");
					clearTextBox(NcOrderObj.workItems.PePort);
					sendKeys(NcOrderObj.workItems.PePort,PEPort2);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(NcOrderObj.createOrder.Update,"Update button");
					click(NcOrderObj.createOrder.Update,"Update button");
					Reusable.waitForSiebelLoader();
					
					verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
					click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

					verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
					click(NcOrderObj.addProductDetails.Edit,"Edit button");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					Reusable.waitForAjax();
				}
				else
				{
	            	//Code for Unprotected
					verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					Reusable.waitForSiebelLoader();

					verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
					clearTextBox(NcOrderObj.workItems.PENetworkElement);
					sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(NcOrderObj.workItems.PePort,"PE Port");
					clearTextBox(NcOrderObj.workItems.PePort);
					sendKeys(NcOrderObj.workItems.PePort,PEPort1);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
					clearTextBox(NcOrderObj.workItems.VCXController);
					sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
					clearTextBox(NcOrderObj.workItems.Beacon);
					sendKeys(NcOrderObj.workItems.Beacon,Beacon);
					Reusable.waitForSiebelLoader();
					Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
					click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

					verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					Reusable.waitForAjax();
				}
				break;
				
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
				{
					verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
				}
				else
				{
					verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
				}
				Reusable.waitForSiebelLoader();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Set Legacy CPE Capability Profile":
			{
				clearTextBox(NcOrderObj.workItems.LegacyCpeProfile);
				waitForAjax();
				
				sendKeys(NcOrderObj.workItems.LegacyCpeProfile,LegacyCpeProfile,"Legacy Cpe Profile");
				waitForAjax();
				SendkeyusingAction(Keys.ENTER);
				clearTextBox(NcOrderObj.workItems.LegacyCpeOamLevel);
				waitForAjax();
				sendKeys(NcOrderObj.workItems.LegacyCpeOamLevel,LegacyCpeOamLevel,"Legacy Cpe Oam Level");
				Thread.sleep(5000);
				SendkeyusingAction(Keys.ENTER);
				clearTextBox(NcOrderObj.workItems.LegacyCpeSupportsCFM);
				waitForAjax();
				sendKeys(NcOrderObj.workItems.LegacyCpeSupportsCFM,"yes","Legacy CPE Supports CFM");
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				//Completed Set Legacy CPE Capability Profile
			}
			break;
			
			case "Legacy Activation Completed":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Select OAM Profile":
			{
				verifyExists(NcOrderObj.workItems.OAMProfile,"OAM Profile");
				click(NcOrderObj.workItems.OAMProfile,"OAM Profile");
				waitForAjax();
				sendKeys(NcOrderObj.workItems.OAMProfile,OAMProfile,"OAM Profile");
				SendkeyusingAction(Keys.ENTER);
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				//Completed Select OAM Profile
			}	
			break;
			
			case "Activation Start Confirmation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Bandwidth Profile Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
    	}
    }  	
}
