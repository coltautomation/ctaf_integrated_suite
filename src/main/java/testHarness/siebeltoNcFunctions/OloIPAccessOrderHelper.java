package testHarness.siebeltoNcFunctions;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.siebeltoNCObjects.OloIpAccessOrderObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;

public class OloIPAccessOrderHelper extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();

	public void SiebelAddSecondarySite(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Layer_3_Resilience = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Layer3_Resilience");
		String Second_Bandwidth= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Backup Bandwidth");
		String Secondary_Site_ID= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Site_ID");
		String SecondaryAccessType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Access_Type/Secondary");
		String SecondaryAccessTechnology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Access_Technology/Secondary");
		String BuildingType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Building_Type");
		String CustomerSitePopStatus= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Customer_Site_Pop_Status");
		String Thirdpartyaccessprovider= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Third_party_access_provider");
		String Thirdpartyconnectref = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "3rd_Party_Connection_Reference");
		String Third_PartySLATier= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "SLA_Tier_values");
		String SecondaryResilienceOption= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "B_End_Resilience_Option");
		String Cabinet_Type= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cabinet_Type");
		String Cabinet_ID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Cabinet_ID");
		String PresentationInterface= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Presentation_Interface");
		String ConnectorType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Connector_Type");
		String FibreType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Fibre_Type");
		String SecondaryPortRole= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Port_Role");
		String Router_Model= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Router_Model");
		String Access_Type= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Access_Type/Secondary");
		String SiteName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Name");
		
		if (Layer_3_Resilience.contains("Dual Access")) 
		{
			//Log.info("Inside Secondary Site Details");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ClickDropdown.replace("Value", "Backup Bandwidth"),"ClickDropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.ClickDropdown.replace("Value", "Backup Bandwidth"),"ClickDropdown");
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SelectValueDropdown.replace("Value",Second_Bandwidth),"ClickDropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SelectValueDropdown.replace("Value",Second_Bandwidth),"ClickDropdown");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();			
						
			if (SecondaryAccessType.contains("3rd Party Leased Line"))
			{
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.TextInput.replace("Value","OSS Platform Flag"),"Legacy");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.TextInput.replace("Value","OSS Platform Flag"), Keys.ENTER);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();	
				}
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();	
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();	
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");			
		
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SelectSite,"SelectSite");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SelectSite,"SelectSite");
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiteIdSearch,"Site ID");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiteIdSearch,Secondary_Site_ID);		

			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiteSearch,"SiteSearch");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiteSearch,"SiteSearch");
			
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
			click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ServicePartySearchAccess,"ServicePartySearchAccess");
			click(OloIpAccessOrderObj.OloIpAccessOrder.ServicePartySearchAccess,"ServicePartySearchAccess");

			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PartyNameSubmitAccess,"PartyNameSubmitAccess");
			click(OloIpAccessOrderObj.OloIpAccessOrder.PartyNameSubmitAccess,"PartyNameSubmitAccess");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiteContactSearchAccess,"SiteContactSearchAccess");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiteContactSearchAccess,"SiteContactSearchAccess");

			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.LastNameSiteSubmitAccess,"LastNameSiteSubmitAccess");
			click(OloIpAccessOrderObj.OloIpAccessOrder.LastNameSiteSubmitAccess,"LastNameSiteSubmitAccess");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();;

			if (Access_Type.contains("Colt Fibre")) 
			{
		    	 verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
		    	 click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
		    	 
		    	 verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SecondaryAccessType));
		    	 click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SecondaryAccessType));
		    	 
		    	 Reusable.waitForSiebelLoader();
		    	 
		    	 verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
		    	 click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
		    	 Reusable.waitForSiebelLoader();
					
		    	verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",SecondaryAccessTechnology));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",SecondaryAccessTechnology));
				Reusable.waitForSiebelLoader();
				
	    		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Building Type"));
	    		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Building Type"));
	    		Reusable.waitForSiebelLoader();
	    		
	    		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BuildingType));
	    		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BuildingType));
	    		Reusable.waitForSiebelLoader();
				
	    		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Customer Site Pop Status"));
	    		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Customer Site Pop Status"));
	    		Reusable.waitForSiebelLoader();
	    		
	    		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CustomerSitePopStatus));
	    		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CustomerSitePopStatus));
	    		Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
			}
			else
			{
				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SecondaryAccessType));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SecondaryAccessType));
				Reusable.waitForSiebelLoader();
								
				
	    	 	verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));

				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SecondaryAccessType));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SecondaryAccessType));
				Reusable.waitForSiebelLoader();//
				
									
				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click On ThirdpartyaccessproviderDropDown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Thirdpartyconnectref));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Thirdpartyconnectref));
				
				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Thirdpartyaccessprovider));
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Thirdpartyaccessprovider));
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.TextInput.replace("Value", "3rd Party Connection Reference"));
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.TextInput.replace("Value","3rd Party Connection Reference"),Thirdpartyconnectref);
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.TextInput.replace("Value", "3rd Party Connection Reference"),Keys.ENTER);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ClickDropdown.replace("Value", "Third Party SLA Tier"),"ClickDropdown");
				click(OloIpAccessOrderObj.OloIpAccessOrder.ClickDropdown.replace("Value", "Third Party SLA Tier"),"ClickDropdown");	
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SelectValueDropdown.replace("Value",Third_PartySLATier),"SelectValueDropdown");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SelectValueDropdown.replace("Value",Third_PartySLATier),"SelectValueDropdown");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}
			
			//Reusable.AEndDropdownSelection("Layer 2 Resilience",SecondaryResilienceOption);
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ClickDropdown.replace("Value", "Layer 2 Resilience"),"ClickDropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.ClickDropdown.replace("Value", "Layer 2 Resilience"),"ClickDropdown");	
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SelectValueDropdown.replace("Value",SecondaryResilienceOption),"SelectValueDropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SelectValueDropdown.replace("Value",SecondaryResilienceOption),"SelectValueDropdown");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
					
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"CabinetType DropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"CabinetTypeDropdownAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"CabinetTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"CabinetTypeSelectAccess");
			
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,Cabinet_ID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
			
			Reusable.waitForSiebelLoader();
			
			Random rand = new Random();
			int rand_int1 = rand.nextInt(1000);
						
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify shelfid");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,Integer.toString(rand_int1));
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
			
			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,Integer.toString(rand_int1));
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
			Thread.sleep(4000);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,Integer.toString(rand_int1));
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
			Thread.sleep(4000);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Verify PresentationInterfaceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Click PresentationInterfaceDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface ));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Click ConnectorTypeDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click FibreTypeDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",SecondaryPortRole));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",SecondaryPortRole));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Click InstallTimeDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Click InstallTimeSelectAccess");
			Reusable.waitForSiebelLoader();
						
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.routermodel1,"router model");
			click(SiebelModeObj.IPAccessObj.routermodel1,"router model");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.IPAccessObj.routermodelSelect.replace("Value",Router_Model),"Select Router model");
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"ClickDropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");	
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		
		}
		else
		{
			//Log.info("Not a Resilient Order");
		}
	}
	
	public void SiebelIpAddressingFeatureUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String IP_Addressing_Format= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IP Addressing Format");
		String IPv4IPv6_Addressing_Type= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IPv4/IPv6 Addressing Type");
		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
		click(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpAdrFormatDrpDwn,"SiebIpAdrFormatDrpDwn");
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpAdrFormatDrpDwn,"SiebIpAdrFormatDrpDwn");
		
		scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IP_Addressing_Format));
		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IP_Addressing_Format));
//		ClickonElementByString("//*[text()='"+IP_Addressing_Format+"']",10);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpAddrFormat, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		//repeated same code
		if(IP_Addressing_Format.equalsIgnoreCase("IPv4 format only"))
		{
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4AddresTypeDrpDwn,"SiebIpv4AddresTypeDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4AddresTypeDrpDwn,"SiebIpv4AddresTypeDrpDwn");
			
			ScrollIntoViewByString("@xpath=//*[text()='"+IPv4IPv6_Addressing_Type+"']");
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
//			ClickonElementByString("//*[text()='"+IPv4IPv6_Addressing_Type+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4AddresType, Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		}
		else if(IP_Addressing_Format.equalsIgnoreCase("IPv6 format only"))
		{
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6AddresTypeDrpDwn,"SiebIpv6AddresTypeDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6AddresTypeDrpDwn,"SiebIpv6AddresTypeDrpDwn");
			
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
//			ClickonElementByString("//*[text()='"+IPv4IPv6_Addressing_Type+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6AddresType, Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			}
		else
		{
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4AddresTypeDrpDwn,"SiebIpv4AddresTypeDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4AddresTypeDrpDwn,"SiebIpv4AddresTypeDrpDwn");
			
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
//			ClickonElementByString("//*[text()='"+IPv4IPv6_Addressing_Type+"']",10);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();

			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6AddresTypeDrpDwn,"SiebIpv6AddresTypeDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6AddresTypeDrpDwn,"SiebIpv6AddresTypeDrpDwn");
			
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IPv4IPv6_Addressing_Type));
//			ClickonElementByString("//*[text()='"+IPv4IPv6_Addressing_Type+"']",10);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		}
	}

	public void SiebelProviderAggregatedUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Format=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Format");
		String Number_of_IPv4_Addresses=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Number of IPv4 Addresses");
		String Number_of_IPv6_Addresses=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Number of IPv6 Addresses");
		String PA_Remote_Static_Route=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PA_Remote_Static_Route");
		String Routing_Option=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Routing_Option");
		String Next_Hop_IP_IPv4=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Next Hop IPv4");				
		String NextHopIp4PA=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NextHopIpv4");				
		String NextHopIp6PA=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NextHopIpv6");	
		String Primary_Range=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Primary_Range");
		String Static_Route=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Static_Route");
		
		//	For IPv4
		if(Format.equalsIgnoreCase("IPv4"))
		{
			scrollDown(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv4 Addresses"));
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv4 Addresses"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv4 Addresses"));
			
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv4_Addresses));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv4_Addresses));
			//ClickonElementByString("//*[text()='"+Number_of_IPv4_Addresses+"']",10);
			Reusable.waitForAjax();
			
			if(Static_Route.equals("Yes"))
			{
				verifyExists(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
				click(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
				sendKeys(SiebelModeObj.IPAccessObj.NextHopIp,NextHopIp4PA,"Next HopIp");
				Reusable.waitForSiebelLoader();
	
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput,"PAstatticrouteinput");
				click(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput,"PAstatticrouteinput");
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput,Static_Route,"PAstatticrouteinput");
				Reusable.waitForAjax();

				verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
				click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
				Reusable.waitForSiebelLoader();
	
				verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				Reusable.waitForSiebelLoader();
			}
		}
		//For IPv6
		else if(Format.equalsIgnoreCase("IPv6"))
		{
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv6 Addresses"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv6 Addresses"));
			
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv6_Addresses));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv6_Addresses));
			//ClickonElementByString("//*[text()='"+Number_of_IPv6_Addresses+"']",10);
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.NoIpv6Add,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput,"PAprimaryrangeinput");
			click(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput,"PAprimaryrangeinput");
			
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput,"Yes","PAprimaryrangeinput");
			Reusable.waitForAjax();

			if(Static_Route.equals("Yes"))
			{
				verifyExists(SiebelModeObj.IPAccessObj.NextHopIpv6,"Next Hop IP");
				click(SiebelModeObj.IPAccessObj.NextHopIpv6,"Next Hop IP");
				sendKeys(SiebelModeObj.IPAccessObj.NextHopIpv6,NextHopIp6PA,"Next HopIp");
				Reusable.waitForSiebelLoader();
	
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv6staticrouteinput,"PAstatticrouteinput");
				click(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv6staticrouteinput,"PAstatticrouteinput");
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv6staticrouteinput);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv6staticrouteinput,Static_Route,"PAstatticrouteinput");
				Reusable.waitForAjax();

				verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
				click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
				Reusable.waitForSiebelLoader();
	
				verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				Reusable.waitForSiebelLoader();
			}
		}
		//For IPv4 & IPv6	
		else 
		{
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv6 Addresses"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv6 Addresses"));
			
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv6_Addresses));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv6_Addresses));
			Reusable.waitForAjax();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput,"PAprimaryrangeinput");
			click(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput,"PAprimaryrangeinput");
			
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAprimaryrangeinput,"No","PAprimaryrangeinput");
			Reusable.waitForAjax();
			
			if(Static_Route.equals("Yes"))
			{
				verifyExists(SiebelModeObj.IPAccessObj.NextHopIpv6,"Next Hop IP");
				click(SiebelModeObj.IPAccessObj.NextHopIpv6,"Next Hop IP");
				sendKeys(SiebelModeObj.IPAccessObj.NextHopIpv6,NextHopIp6PA,"Next HopIp");
				Reusable.waitForSiebelLoader();
							
				verifyExists(SiebelModeObj.IPAccessObj.ClickDropdownIpv6.replace("Value","Routing Option"));
				click(SiebelModeObj.IPAccessObj.ClickDropdownIpv6.replace("Value","Routing Option"));
				Reusable.waitForSiebelLoader();
	
				verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				Reusable.waitForSiebelLoader();
			}
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput,"PAstatticrouteinput");
			click(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput,"PAstatticrouteinput");
			
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput,"Yes","PAstatticrouteinput");
			Reusable.waitForAjax();
			
			if(PA_Remote_Static_Route.equalsIgnoreCase("Yes"))
			{
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput,"PAstatticrouteinput");
				click(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput,"PAstatticrouteinput");
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAstatticrouteinput,"Yes","PAstatticrouteinput");
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
								
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAroutingoptioninputipvp4,"PAroutingoptioninputipvp4");
				click(OloIpAccessOrderObj.OloIpAccessOrder.PAroutingoptioninputipvp4,"PAroutingoptioninputipvp4");
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAroutingoptioninputipvp4);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAroutingoptioninputipvp4,Routing_Option,"PAroutingoptioninputipvp4");
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAnexthopipinputipvp4);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAnexthopipinputipvp4,Next_Hop_IP_IPv4,"PAroutingoptioninputipvp4");
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
			}
			
			scrollDown(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv4 Addresses"));
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv4 Addresses"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Number of IPv4 Addresses"));

			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Number_of_IPv4_Addresses));
			Reusable.waitForAjax();
			
			//Select the Primary Range
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Primary Range"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Primary Range"));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Primary_Range));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Primary_Range));
			Reusable.waitForSiebelLoader();

			if(Static_Route.equals("Yes"))
			{
				verifyExists(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
				click(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
				sendKeys(SiebelModeObj.IPAccessObj.NextHopIp,NextHopIp4PA,"Next HopIp");
				Reusable.waitForSiebelLoader();
				
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput,"PAstatticrouteinput");
				click(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput,"PAstatticrouteinput");
				
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PAIpv4statticrouteinput,Static_Route,"PAstatticrouteinput");
				Reusable.waitForAjax();
	
				verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
				click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","Static"));
				Reusable.waitForSiebelLoader();
			}
//			else
//			{
//				verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","BGP"));
//				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","BGP"));
//				Reusable.waitForSiebelLoader();
//			}							
						
		}
	}

	public void SiebelProviderIndependentUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Format=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Format");
		String LAN_Inter_IPv4_Address=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Address");
		String IPv4_Prefix_Inp=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IPv4 Prefix Inp");
		String IPv4IPv6_Addressing_Type= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IPv4/IPv6 Addressing Type");
		String PIRemoteStaticRoute= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PI/Remote Static Route");
		String PINextHopIPv4=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PI_NextHopIpv4");
		String PINextHopIPv6=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PI_NextHopIpv6");
		String LAN_Inter_IPv6_Address=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LAN Inter IPv6 Address");
		String IPv6_Prefix_Inp=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IPv6 Prefix Inp");
		String IPv6_VRRP_2nd_CPE_Add=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IPv6 VRRP 2nd CPE Add");
		String IP_Addressing_Format=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IP Addressing Format");
//		String PI_Remote_Static_Route_ipv6= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "PI/Remote Static Route");
		String LANVRRPIPv4Add1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LAN VRRP IPv4 Add 1");
		String LANVRRPIPv4Add2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "LAN VRRP IPv4 Add 2");
		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
				//For IPv4
		if(Format.equalsIgnoreCase("IPv4"))
		{
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address,LAN_Inter_IPv4_Address,"SiebIpv4Address");
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,"SiebIpv4Prefix");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,"SiebIpv4Prefix");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,IPv4_Prefix_Inp,"SiebIpv4Prefix");
			Reusable.waitForpageloadmask();
			
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4LanVrrpAdd1);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4LanVrrpAdd1,LANVRRPIPv4Add1,"SiebIpv4LanVrrpAdd1");
			Reusable.waitForAjax();

			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4LanVrrpAdd2);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4LanVrrpAdd2,LANVRRPIPv4Add2,"SiebIpv4LanVrrpAdd2");
			Reusable.waitForAjax();
			
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SecondCpeLanInterfIpv4Add);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SecondCpeLanInterfIpv4Add,LAN_Inter_IPv4_Address,"SecondCpeLanInterfIpv4Add");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();

			if(IPv4IPv6_Addressing_Type.equalsIgnoreCase("PA and PI"))
			{
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv4PrimaryRanDrpDwn,"SiebPiIpv4PrimaryRanDrpDwn");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv4PrimaryRanDrpDwn,"SiebPiIpv4PrimaryRanDrpDwn");
				
				ClickonElementByString("//li[text()='No']",10);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

				if(PIRemoteStaticRoute.equalsIgnoreCase("Yes"))
				{
					verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4,"StaticRouteinputipv4");
					click(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4,"StaticRouteinputipv4");
					
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4,"Yes","StaticRouteinputipv4");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv4);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv4,PINextHopIPv4,"nexthopippiipv4");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
				}
			}
//			else
//			{
//				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PIIpv4primaryrangeinput);
//				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PIIpv4primaryrangeinput,"Yes","PAprimaryrangeinput");
//				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.PIIpv4primaryrangeinput,Keys.ENTER);
//				
//				Reusable.waitForAjax();
//				Reusable.waitForpageloadmask();
//			}
		}
		//		For IPv6
		else if(Format.equalsIgnoreCase("IPv6"))
		{
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,"SiebIpv6Address");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,"SiebIpv6Address");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,LAN_Inter_IPv6_Address,"SiebIpv6Address");
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,"SiebIpv6Prefix");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,"SiebIpv6Prefix");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,IPv6_Prefix_Inp,"SiebIpv6Prefix");
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6LanVrrpAdd);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6LanVrrpAdd,IPv6_VRRP_2nd_CPE_Add,"SiebIpv6LanVrrpAdd");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			if(IPv4IPv6_Addressing_Type.equalsIgnoreCase("PA and PI"))
			{
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRanDrpDwn,"SiebPiIpv6PrimaryRanDrpDwn");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRanDrpDwn,"SiebPiIpv6PrimaryRanDrpDwn");
				ClickonElementByString("//*[text()='No']",10);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

				if(PIRemoteStaticRoute.equalsIgnoreCase("Yes") )
				{
					verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6,"StaticRouteinputipv6");
					click(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6,"StaticRouteinputipv6");
					
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6,"Yes","StaticRouteinputipv6");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
					click(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv6,"nexthopippiipv6");					
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
				}
			}
			else
			{
//				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRanDrpDwn,"SiebPiIpv6PrimaryRanDrpDwn");
//				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRanDrpDwn,"SiebPiIpv6PrimaryRanDrpDwn");
//				ClickonElementByString("//*[text()='Yes']",10);

				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"Yes","PAprimaryrangeinput");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}
		
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebSecondCpeIpv6LanAdd);
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSecondCpeIpv6LanAdd,LAN_Inter_IPv6_Address,"SiebSecondCpeIpv6LanAdd");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();		
		
		}
		//		For IPv4 & IPv6
		else 
		{
			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address);
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address,"SiebIpv4Address");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address,"SiebIpv4Address");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Address,LAN_Inter_IPv4_Address,"SiebIpv4Address");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			
			if(IPv4IPv6_Addressing_Type.equalsIgnoreCase("PA and PI"))
			{
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv4PrimaryRanDrpDwn,"SiebPiIpv4PrimaryRanDrpDwn");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv4PrimaryRanDrpDwn,"SiebPiIpv4PrimaryRanDrpDwn");
				ClickonElementByString("//*[text()='No']",10);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				if(PIRemoteStaticRoute.equalsIgnoreCase("Yes") )
				{
					verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4,"StaticRouteinputipv4");
					click(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4,"StaticRouteinputipv4");
					
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv4,"Yes","StaticRouteinputipv4");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();	
					
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv4);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv4,PINextHopIPv4,"nexthopippiipv4");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();	
				}
			}
			else
			{
//				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv4PrimaryRanDrpDwn,"SiebPiIpv4PrimaryRanDrpDwn");
//				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv4PrimaryRanDrpDwn,"SiebPiIpv4PrimaryRanDrpDwn");
//				ClickonElementByString("//*[text()='Yes']",10);
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.PIIpv4primaryrangeinput);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.PIIpv4primaryrangeinput,"Yes","PAprimaryrangeinput");

				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,"SiebIpv4Prefix");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,"SiebIpv4Prefix");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,IPv4_Prefix_Inp,"SiebIpv4Prefix");
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4Prefix,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
//			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4LanVrrpAdd1);
//			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4LanVrrpAdd1,LAN_Inter_IPv4_Address,"SiebIpv4LanVrrpAdd");
//			Reusable.waitForAjax();
//			Reusable.waitForpageloadmask();	
//			
//			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebSecondCpeIpv4LanAdd);
//			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSecondCpeIpv4LanAdd,LAN_Inter_IPv4_Address,"SiebSecondCpeIpv4LanAdd");
//			Reusable.waitForAjax();
//			Reusable.waitForpageloadmask();	
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,"Ipv6 Address");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,"Ipv6 Address");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,LAN_Inter_IPv6_Address,"Ipv6 Address");
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			

			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,"Ipv6 Prefix");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,"Ipv6 Prefix");
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Prefix,IPv6_Prefix_Inp,"Ipv6 Prefix");
			//Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6Address,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
//			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6LanVrrpAdd);
//			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6LanVrrpAdd,IPv6_VRRP_2nd_CPE_Add ,"SiebIpv6LanVrrpAdd");
//			Reusable.waitForAjax();
//			Reusable.waitForpageloadmask();	
			
			if((IPv4IPv6_Addressing_Type.equalsIgnoreCase("PA and PI")) || (IP_Addressing_Format.contains("IPv4 and IPv6"))) 
			{
//				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"SiebPiIpv6PrimaryRange");
//				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"SiebPiIpv6PrimaryRange");
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"No","PI Ipv6 PrimaryRange");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			
				
				if(PIRemoteStaticRoute.equalsIgnoreCase("Yes"))
				{
					verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6,"Static Route input IPv6");
					click(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6,"Static Route input IPv6");
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.StaticRouteinputipv6,"Yes","Static Route input IPv6");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					
//					clickCheckBox(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv6,"CHECK",true);					
					clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv6);
					sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.nexthopippiipv6,PINextHopIPv6,"nexthopippiipv6");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
				}
			}
			else
			{
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"PI Ipv6 PrimaryRange");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"PI Ipv6 PrimaryRange");
				clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange);
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPiIpv6PrimaryRange,"Yes","PI Ipv6 PrimaryRange");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
			}
//			clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebSecondCpeIpv6LanAdd);
//			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSecondCpeIpv6LanAdd,IPv6_VRRP_2nd_CPE_Add,"SiebSecondCpeIpv6LanAdd");
//			Reusable.waitForAjax();
//			Reusable.waitForpageloadmask();
		}
	}
	
	public void SiebelBgpFeedUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String BGP4_Feed_Type=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BGP4 Feed Type");
		String Type_of_AS=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Type of AS");
		String BGP4_Feed_Required=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "BGP4 Feed Required");
		String AS_Number=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "AS Number");
		String TTLSecurity=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "TTL_Security");

		click(OloIpAccessOrderObj.OloIpAccessOrder.BgpFeedCheck,"Click BGP Checkbox");
		Reusable.waitForAjax();
		//Reusable.waitForpageloadmask();
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebBgpFeedTypeDrpDwn,"SiebBgpFeedTypeDrpDwn");
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebBgpFeedTypeDrpDwn,"SiebBgpFeedTypeDrpDwn");
		scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BGP4_Feed_Type));
		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BGP4_Feed_Type));
		//ClickonElementByString("//*[text()='"+BGP4_Feed_Type+"']",10);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebBgpFeedType,Keys.ENTER);
		Reusable.waitForAjax();
		//Reusable.waitForpageloadmask();
		
		//verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebASTypeDrpDwn,"SiebASTypeDrpDwn");
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebASType,"SiebASTypeDrpDwn");
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebASType,Type_of_AS,"Type of AS");
		//ClickonElementByString("//*[text()='"+Type_of_AS+"']",10);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebASType,Keys.ENTER);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebASType,Keys.TAB);
		Reusable.waitForAjax();
		
//		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebTTlSecurity,"SiebTTlSecurity");
//		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebTTlSecurity,"SiebTTlSecurity");
//		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",TTLSecurity));
//		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebTTlSecurity,Keys.ENTER);
//		Reusable.waitForAjax();
		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.SiebASNum);
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebASNum,AS_Number,"SiebASNum");
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebASNum, Keys.ENTER);
		Reusable.waitForAjax();
		//Reusable.waitForpageloadmask();
	}

	
	public void SiebelDhcpUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Feature_Format=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Feature Format");
		String DHCP_Address=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "DHCP Address");
		String DHCP_Prefix=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "DHCP Prefix");
		String Primary_WINS_Server_Add=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Primary WINS Server Add");
		String Gateway_IP_Add=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Gateway IP Add");
		String Domain_Name=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Domain Name");
		
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpCheck,"Siebel Dhcp Check");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpFormatDrpDwn,"SiebDhcpFormatDrpDwn");
		ScrollIntoViewByString(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpFormatDrpDwn);
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpFormatDrpDwn,"SiebDhcpFormatDrpDwn");
		//scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Feature_Format));
		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Feature_Format),"DHCP Format");
		//ClickonElementByString("//*[text()='"+Feature_Format+"']",10);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpFormat,Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		if(Feature_Format.equalsIgnoreCase("IPv4"))
		{
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv4Add,DHCP_Address,"SiebDhcpIpv4Add");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv4Add,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv4Prefix,DHCP_Prefix,"SiebDhcpIpv4Prefix");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv4Prefix,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrimIpv4NameServerAdd,Primary_WINS_Server_Add,"SiebPrimIpv4NameServerAdd");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrimIpv4NameServerAdd,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebGateIpv4Add,Gateway_IP_Add,"SiebGateIpv4Add");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebGateIpv4Add,Keys.ENTER);
			Reusable.waitForAjax();

			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.DhcpDomainName,Domain_Name,"Dhcp Domain Name");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.DhcpDomainName,Keys.ENTER);
			Reusable.waitForAjax();
			
		}
		else
		{
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv6Add,DHCP_Address,"SiebDhcpIpv6Add");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv6Add,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			

			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv6Prefix,DHCP_Prefix,"SiebDhcpIpv6Prefix");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebDhcpIpv6Prefix,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
		
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrimIpv6NameServerAdd,Primary_WINS_Server_Add,"SiebPrimIpv6NameServerAdd");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrimIpv6NameServerAdd,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebGateIpv6Add,Gateway_IP_Add,"SiebGateIpv6Add");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebGateIpv6Add,Keys.ENTER);
			Reusable.waitForAjax();

			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.DhcpDomainName,Domain_Name,"Dhcp Domain Name");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.DhcpDomainName,Keys.ENTER);
			Reusable.waitForAjax();
			
		}
	}


	public void SiebelSnmpUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Feature_Format=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Feature Format");
		String Community_String=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Community String");
		String Traps_Required=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Traps Required");
		String Device_Address_IPv4=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Device Address IPv4");
		String Device_Address_IPv6=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Device Address IPv6");
		
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpCheck,"Sieb Snmp Check");
		Reusable.waitForAjax();
		//Reusable.waitForpageloadmask();
		scrollDown(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpFormatDrpDwn);
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpFormatDrpDwn,"SiebSnmpFormatDrpDwn");		
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpFormatDrpDwn,"SiebSnmpFormatDrpDwn");
		//sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpFormat,Feature_Format,"SiebSnmp Format Input");
		scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Feature_Format));
		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Feature_Format));
		//ClickonElementByString("//*[text()='"+Feature_Format+"']",10);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpFormat,Keys.ENTER);
		Reusable.waitForAjax();
								 
		
		if(Feature_Format.equalsIgnoreCase("IPv4"))
		{
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4CommunityString,Community_String,"SiebSnmpIpv4CommunityString");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4CommunityString,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4TrapReqDrpDwn,"SiebSnmpIpv4TrapReqDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4TrapReqDrpDwn,"SiebSnmpIpv4TrapReqDrpDwn");
			ClickonElementByString("//*[text()='"+Traps_Required+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4TrapReq,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4DeviceAdd,Device_Address_IPv4,"SiebSnmpIpv4DeviceAdd");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv4DeviceAdd,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
		}
		else
		{
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6CommunityString,Community_String,"SiebSnmpIpv6CommunityString");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6CommunityString,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6TrapReqDrpDwn,"SiebSnmpIpv6TrapReqDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6TrapReqDrpDwn,"SiebSnmpIpv6TrapReqDrpDwn");
			ClickonElementByString("//*[text()='"+Traps_Required+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6TrapReq,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6DeviceAdd,Device_Address_IPv6,"SiebSnmpIpv6DeviceAdd");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebSnmpIpv6DeviceAdd,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
		}
	}

	public void SiebelNatUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String IP_Addressing_Format= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IP Addressing Format");
		String NAT_Feature_Name=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NAT Feature Name");		
		String Private_Port=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Private Port");
		String Private_IP_Address=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Private IP Address");		
		String Public_Port=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Public Port");
		String Public_IP_Address=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Public IP Address");
		String IP_Protocol=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "IP Protocol");
		String Public_IP_Add_Identifier=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Public IP Add Identifier");			
		String NAT_from_Mask=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NAT from Mask");
		String NAT_IPv4_Range_From=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NAT IPv4 Range From");
		String NAT_IPv4_Range_To=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "NAT IPv4 Range To");

		
		if(!IP_Addressing_Format.equalsIgnoreCase("IPv6 format only"))  //ipvp6 only NAT wont support
		{
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatCheck,"Siebel NAT Check");
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatTypeDrpDwn,"SiebNatTypeDrpDwn");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatTypeDrpDwn,"SiebNatTypeDrpDwn");
			scrollDown(SiebelModeObj.IPAccessObj.searchInput.replace("Value",NAT_Feature_Name));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",NAT_Feature_Name));
			//ClickonElementByString("//*[text()='"+NAT_Feature_Name+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatType,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			if(NAT_Feature_Name.contains("Static"))
			{
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrivIpPort,Private_Port,"SiebPrivIpPort");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrivIpPort,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrivIpAdd,Private_IP_Address,"SiebPrivIpAdd");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPrivIpAdd,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpPort,Public_Port,"SiebPublIpPort");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpPort,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpAdd,Public_IP_Address,"SiebPublIpAdd");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpAdd,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpAddIdentifierDrpDwn,"SiebPublIpAddIdentifierDrpDwn");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpAddIdentifierDrpDwn,"SiebPublIpAddIdentifierDrpDwn");
				ClickonElementByString("//*[text()='"+Public_IP_Add_Identifier+"']",10);
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebPublIpAddIdentifier,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpProtocolDrpDwn,"SiebIpProtocolDrpDwn");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpProtocolDrpDwn,"SiebIpProtocolDrpDwn");
				ClickonElementByString("//*[text()='"+IP_Protocol+"']",10);
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpProtocol,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
			
			}
			else if(NAT_Feature_Name.contains("Dynamic"))  
			{
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatFromMask,NAT_from_Mask,"SiebNatFromMask");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatFromMask,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatIpv4AddRangeFrm,NAT_IPv4_Range_From,"SiebNatIpv4AddRangeFrm");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatIpv4AddRangeFrm,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
				sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatIpv4AddRangeTo,NAT_IPv4_Range_To,"SiebNatIpv4AddRangeTo");
				Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebNatIpv4AddRangeTo,Keys.ENTER);
				Reusable.waitForAjax();
				//Reusable.waitForpageloadmask();
				
			}
			else
			{
				//static and dynamic
			}
		}
	}
	
	public void SiebelStaticIpv4RouteUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Route_Feature_Name= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Route Feature Name");
		String Next_Hop_IPv4=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Next Hop IPv4");		
		String Target_Network_IPv4=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Target Network IPv4");
		String Subnet_Mask_IPv4=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Subnet Mask IPv4");
		String Next_Hop_IPv6=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Next Hop IPv6");		
		String Target_Network_IPv6=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Target Network IPv6");
		String Subnet_Mask_IPv6=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Subnet Mask IPv6");

		
		if(Route_Feature_Name.equalsIgnoreCase("Static IPv4 Route"))
		{
			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebStaticIpv4RouteCheck,"Siebel Static IPv4 RouteCheck");
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4NextHopIp,Next_Hop_IPv4,"SiebIpv4NextHopIp");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4NextHopIp,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4TargetNetwork,Target_Network_IPv4,"SiebIpv4TargetNetwork");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4TargetNetwork,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4SubNetMask,Subnet_Mask_IPv4,"SiebIpv4SubNetMask");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv4SubNetMask,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			/*
			 * clickCheckBox(OloIpAccessOrderObj.OloIpAccessOrder.ReadyforNC,"CHECK",* true); 
			 * Reusable.waitForAjax();
			 * Reusable.waitForpageloadmask();
			 */
			
		}
		else
		{

			click(OloIpAccessOrderObj.OloIpAccessOrder.SiebStaticIpv6RouteCheck,"SiebStaticIpv6RouteCheck");
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6NextHopIp,Next_Hop_IPv6,"SiebIpv6NextHopIp");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6NextHopIp,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6TargetNetwork,Target_Network_IPv6,"SiebIpv6TargetNetwork");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6TargetNetwork,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6SubNetMask,Subnet_Mask_IPv6,"SiebIpv6SubNetMask");
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebIpv6SubNetMask,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			/*
			 * clickCheckBox(OloIpAccessOrderObj.OloIpAccessOrder.ReadyforNC,"CHECK",* true); 
			 * Reusable.waitForAjax();
			 * Reusable.waitForpageloadmask();
			 */
		
		}
	}
	
	public void siebelCloudPrioritizationUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Router_Technology=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Router_Technology");
		String Bandwidthpercentage=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bandwidth_percent");
		String CSP_Name=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CSP_Name");

		if(Router_Technology.equalsIgnoreCase("Managed Physical Router"))  //only MPR supports
		{

			click(OloIpAccessOrderObj.OloIpAccessOrder.sibelcloudprioritzation,"sibelcloudprioritzation");
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.bandwidthclouddropdown,"bandwidthclouddropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.bandwidthclouddropdown,"bandwidthclouddropdown");
			ClickonElementByString("//*[text()='"+Bandwidthpercentage+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.bandwidthcloudinput,Keys.ENTER);
			Reusable.waitForAjax();
			//Reusable.waitForpageloadmask();
			
			
			//currently CSP name only value is present below are not required - auto populate
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.cspnameclouddropdown,"cspnameclouddropdown");
			click(OloIpAccessOrderObj.OloIpAccessOrder.cspnameclouddropdown,"cspnameclouddropdown");
			ClickonElementByString("//*[text()='"+CSP_Name+"']",10);
			Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.cspnamecloudinput,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		

		}
	}
	
	public void SiebelDiversityAndRoutingUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
//		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
//		click(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
		
		click(OloIpAccessOrderObj.OloIpAccessOrder.DiverRoutCheck,"DiverRoutCheck");
		Reusable.waitForAjax();
		
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,"Diversityinput");
		//click(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,"Diversityinput");
		/*sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,Keys.chord(Keys.CONTROL,"a"),"Diversityinput");
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,Keys.chord(Keys.BACK_SPACE),"Diversityinput");
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,Keys.chord(Keys.BACK_SPACE),"Diversityinput"); //---> Duplicate. In old framework, it was written twice.Need to check while running the script.
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();*/
		
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,"Yes","Diversityinput");
		keyPress(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput,Keys.ENTER);

		Reusable.waitForAjax();
		
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.Diversityinput, Keys.TAB);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		clearTextBox(OloIpAccessOrderObj.OloIpAccessOrder.DivServRef);
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.DivServRef,"2255662","DivServRef");
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.DivServRef, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ResOptDivCir,"ResOptDivCir");
		click(OloIpAccessOrderObj.OloIpAccessOrder.ResOptDivCir,"ResOptDivCir");
		ClickonElementByString("//*[text()='Gold']",10);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	}
	
	public void SubmitTechnicalOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Access_Type=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Access_Type");
		String Bend_Access_Type=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Bend_Access_Type");

		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		String resilienceValue=getAttributeFrom(OloIpAccessOrderObj.OloIpAccessOrder.SiebLayer3Resilience,"value");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "resilienceValue",resilienceValue );
		String PrimaryAccessType=Access_Type;
		String SecondaryAccessType=Bend_Access_Type;
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.PrimarySite,"PrimarySite");
		click(OloIpAccessOrderObj.OloIpAccessOrder.PrimarySite,"PrimarySite");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	
		if (Access_Type.contains("Colt Fibre"))
		{
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SubmitTechnicalOrderButton,"SubmitTechnicalOrderButton");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SubmitTechnicalOrderButton,"SubmitTechnicalOrderButton");
			
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			String Circuitreferencenumberprimary=getAttributeFrom(OloIpAccessOrderObj.OloIpAccessOrder.CircuitReferenceinput,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Circuitreferencenumberprimary",Circuitreferencenumberprimary );
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.TechnicalOrderTab,"TechnicalOrderTab");
			click(OloIpAccessOrderObj.OloIpAccessOrder.TechnicalOrderTab,"TechnicalOrderTab");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			String IpaccessNCserviceidPrimary=getTextFrom(OloIpAccessOrderObj.OloIpAccessOrder.IpAccessPrimary);
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "IpaccessNCserviceidPrimary",IpaccessNCserviceidPrimary );
			
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SitesTab,"SitesTab");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SitesTab,"SitesTab");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
		}

		if (resilienceValue.contains("Dual Access")) 
		{
			verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
			click(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
			if (Bend_Access_Type.contains("Colt Fibre"))
			{
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SubmitTechnicalOrderButton,"SubmitTechnicalOrderButton");
				click(OloIpAccessOrderObj.OloIpAccessOrder.SubmitTechnicalOrderButton,"SubmitTechnicalOrderButton");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				String Circuitreferencenumbersecondary=getAttributeFrom(OloIpAccessOrderObj.OloIpAccessOrder.CircuitReferenceinput,"value");
				DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Circuitreferencenumbersecondary",Circuitreferencenumbersecondary );
				verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.TechnicalOrderTab,"TechnicalOrderTab");
				click(OloIpAccessOrderObj.OloIpAccessOrder.TechnicalOrderTab,"TechnicalOrderTab");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				String IpaccessNCserviceidSecondary=getTextFrom(OloIpAccessOrderObj.OloIpAccessOrder.IpAccessSecondary);
				DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "IpaccessNCserviceidSecondary",IpaccessNCserviceidSecondary );
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
			}
		}
		//	Pagerefresh();
	}
	
	/*
	 * public void SiebelAddSiteDetails(String testDataFile, String sheetName,String scriptNo, String dataSetNo) throws Exception { 
	 * String Aend_Site_ID=DataMiner.fngetcolvalue(sheetName, scriptNo,dataSetNo, "Aend_Site_ID");
	 * 
	 * Reusable.waitForAjax(); Reusable.waitForpageloadmask();
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SelectSite,"SelectSite");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.SelectSite,"SelectSite");
	 * 
	 * sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiteIdSearch,Aend_Site_ID,
	 * "SiteIdSearch");
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiteSearch,"SiteSearch");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.SiteSearch,"SiteSearch");
	 * ClickonElementByString(
	 * "//table[@id='colt-siteselection-result-table']//tr//td[contains(text(),'"+
	 * Aend_Site_ID+"')]",10);
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpAPickSiteBtn,
	 * "IpAPickSiteBtn");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.IpAPickSiteBtn,"IpAPickSiteBtn");
	 * 
	 * Reusable.waitForAjax(); Reusable.waitForpageloadmask();
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpAServicePartySearch,
	 * "IpAServicePartySearch");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.IpAServicePartySearch,
	 * "IpAServicePartySearch");
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpAOkButton,"IpAOkButton");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.IpAOkButton,"IpAOkButton");
	 * Reusable.waitForAjax(); Reusable.waitForpageloadmask();
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpASiteContactSearch,
	 * "IpASiteContactSearch");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.IpASiteContactSearch,
	 * "IpASiteContactSearch");
	 * 
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.LastNameSiteSubmit,
	 * "LastNameSiteSubmit");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.LastNameSiteSubmit,
	 * "LastNameSiteSubmit"); Reusable.waitForAjax();
	 * Reusable.waitForpageloadmask();
	 * 
	 * verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
	 * click(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
	 * Reusable.waitForAjax(); Reusable.waitForpageloadmask(); }
	 * 
	 */
	

	public void SiebelNatPrivateIpv4Update(String testDataFile, String sheetName,String scriptNo, String dataSetNo) throws Exception
	{
		String Public_IP_Address=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Public IP Address");
		String Public_Port=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Public Port");
		
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanInterfIpv4Add,Public_IP_Address);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanInterfIpv4Add, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanInterfIpv4Add,Public_Port);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanInterfIpv4Add, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanVrrpIpv4Add1,Public_IP_Address);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanVrrpIpv4Add1, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanVrrpIpv4Add2,Public_IP_Address);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SiebLanVrrpIpv4Add2, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		sendKeys(OloIpAccessOrderObj.OloIpAccessOrder.SecondCpeLanInterfIpv4Add,Public_IP_Address);
		Reusable.SendkeaboardKeys(OloIpAccessOrderObj.OloIpAccessOrder.SecondCpeLanInterfIpv4Add, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	
	}
	
	 public void SiebelServiceGroupUpdate(String testDataFile, String sheetName,String scriptNo, String dataSetNo) throws Exception
	    {
	        String Related_Voip_Service=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "Related_Voip_Service");
	       
	        if(Related_Voip_Service.equals("Custom CoS") || Related_Voip_Service.equals("Converged") || Related_Voip_Service.equals("Dedicated"))
	        {
	            Reusable.waitForAjax();
	            Reusable.waitForpageloadmask();
	           
	            verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ServiceGrpTab,"ServiceGrpTab");
	            click(OloIpAccessOrderObj.OloIpAccessOrder.ServiceGrpTab,"ServiceGrpTab");
	            Reusable.waitForAjax();
	            Reusable.waitForpageloadmask();
	           
	            verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ServiceGrpNew,"ServiceGrpNew");
	            click(OloIpAccessOrderObj.OloIpAccessOrder.ServiceGrpNew,"ServiceGrpNew");
	           
	            verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.ServiceGrpSearch,"Service Grp Search");
	            click(OloIpAccessOrderObj.OloIpAccessOrder.ServiceGrpSearch,"Service Grp Search");
	           
	            verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.AddServGrp,"AddServGrp");
	            click(OloIpAccessOrderObj.OloIpAccessOrder.AddServGrp,"AddServGrp");
	            Reusable.waitForAjax();
	            Reusable.waitForpageloadmask();
	        }
	       
	    }

	public void ReadyforNCCheckbox() throws Exception 
	{	
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
		click(OloIpAccessOrderObj.OloIpAccessOrder.IpShowFullInfo,"IpShowFullInfo");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		clickCheckBox(OloIpAccessOrderObj.OloIpAccessOrder.ReadyforNC,"CHECK",true);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SiebIPAccessClose,"SiebIPAccessClose");
		click(OloIpAccessOrderObj.OloIpAccessOrder.SiebIPAccessClose,"SiebIPAccessClose");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
		click(OloIpAccessOrderObj.OloIpAccessOrder.SavePage,"SavePage");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

	}

}
