package testHarness.siebeltoNcFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebeltoNCObjects.SiebelAddProdcutObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelAccounts extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void NavigateToAccounts() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelAccountsObj.Accounts.accountsTab, 10);
		verifyExists(SiebelAccountsObj.Accounts.accountsTab,"Account Tab");
		click(SiebelAccountsObj.Accounts.accountsTab,"Account Tab");
		waitForAjax();
		verifyExists(SiebelAccountsObj.Accounts.OCNTxb,"Account Number");	
		if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
		}
		if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) {
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
			click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
		}
		
	}
	
	public void SearchAccount(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String OCN = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Account_Number");
		    
		verifyExists(SiebelAccountsObj.Accounts.OCNTxb,"Account Number");
		//click(SiebelAccountsObj.Accounts.OCNTxb,"OCNTxb");
		sendKeys(SiebelAccountsObj.Accounts.OCNTxb, OCN, "Account Number Textbox");
		verifyExists(SiebelAccountsObj.Accounts.GoBtn,"Go Button");
		click(SiebelAccountsObj.Accounts.GoBtn,"Go Button");
		Reusable.waitForSiebelLoader();
		ClickonElementByString("//td[text()='"+OCN+"']//following-sibling::*//a", 30);
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAccountsObj.Accounts.InstalledAssets_NewBtn, 10);
		verifyExists(SiebelAccountsObj.Accounts.InstalledAssets_NewBtn,"Installed Assets New Button");
		click(SiebelAccountsObj.Accounts.InstalledAssets_NewBtn,"Installed Assets New Button");
		Reusable.waitForSiebelLoader();
	}	

	

}
