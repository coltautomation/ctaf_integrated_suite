package testHarness.siebeltoNcFunctions;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.siebeltoNCObjects.IpAccessModifyOrderObj;
import pageObjects.siebeltoNCObjects.OloIpAccessOrderObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebeltoNCObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import pageObjects.siebeltoNCObjects.SiebelCEOSObj;
import pageObjects.siebelObjects.SiebelManualValidationObj;
import pageObjects.siebeltoNCObjects.SiebelModeObj;
import pageObjects.siebeltoNCObjects.SiebelNewOrderOnnetHelperObj;
import pageObjects.siebeltoNCObjects.SiebelOrderObj;
import pageObjects.siebelObjects.SiebelServiceOrderObj.ServiceOrder;
import testHarness.commonFunctions.ReusableFunctions;
//import testHarness.commonFunctions.SiebelReusableFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.DocumentFormatException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import com.mysql.cj.jdbc.DatabaseMetaDataUsingInfoSchema;
import com.relevantcodes.extentreports.LogStatus;

//import baseClasses.PropertyReader;
import baseClasses.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelAddProduct extends SeleniumUtils {
	
	public static ThreadLocal<String> CeosOrder = new ThreadLocal<>();
	public static ThreadLocal<String> ServiceOrder2 = new ThreadLocal<>();
	public static ThreadLocal<String> NetworkReferenceIPVPN = new ThreadLocal<>();
	public static ThreadLocal<String> Circuitreferencenumber = new ThreadLocal<>();
	public static ThreadLocal<String> CompValidation = new ThreadLocal<>();
	
//	GlobalVariables g = new GlobalVariables();
	ReusableFunctions Reusable = new ReusableFunctions();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();
//	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelManualValidation ManualValidation = new SiebelManualValidation();
	SiebelMode siebelmod = new SiebelMode();
	SiebelLoginPage Login = new SiebelLoginPage();
	
	
	public void productSelection(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		int StatusHeader=-1;
		int i=0;
		boolean found=false;
	    do
	    {
	   	if(i>0)
	    	{
	    		click(SiebelAddProdcutObj.product.ProductSelectionArrow,"Product arrow icon");
	    		Reusable.waitForSiebelLoader();
	    		waitForAjax();
	 
	    		i=0;
	    	}
    	List<WebElement> ProductList=SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[@class='product-name']"));
    	
	    	for(WebElement ele :ProductList)
			  {
	    		scrollIntoView(ele);
				 String Text=ele.getText();
				 if(Text.equalsIgnoreCase(ProductName))
				 {
					 StatusHeader=i;
					 found=true;
					 break;
				 }
				 i++;
			  }	
	    	if(found)
	    		break;
	    }while(findWebElement(SiebelAddProdcutObj.product.ProductSelectionArrow).isDisplayed());
		
	    String plusButton=SiebelAddProdcutObj.product.addProductPlusIcon;
		String plusbutton=plusButton.replace("-10",String.valueOf(i+1));
	
		click(plusbutton,"Product plus Icon");
		Reusable.waitForSiebelLoader();
	}	
	
	
	public void productSelection_Hub(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
//		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type_Hub");
		int StatusHeader=-1;
		int i=0;
		boolean found=false;
	    do
	    {
	   	if(i>0)
	    	{
	    		click(SiebelAddProdcutObj.product.ProductSelectionArrow,"Product arrow icon");
	    		Reusable.waitForSiebelLoader();
	    		waitForAjax();
	 
	    		i=0;
	    	}
    	List<WebElement> ProductList=SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[@class='product-name']"));
    	
	    	for(WebElement ele :ProductList)
			  {
	    		scrollIntoView(ele);
				 String Text=ele.getText();
				 if(Text.equalsIgnoreCase("Ethernet Hub"))
				 {
					 StatusHeader=i;
					 found=true;
					 break;
				 }
				 i++;
			  }	
	    	if(found)
	    		break;
	    }while(findWebElement(SiebelAddProdcutObj.product.ProductSelectionArrow).isDisplayed());
		
	    String plusButton=SiebelAddProdcutObj.product.addProductPlusIcon;
		String plusbutton=plusButton.replace("-10",String.valueOf(i+1));
	
		click(plusbutton,"Product plus Icon");
		Reusable.waitForSiebelLoader();
	}
	
	public void productSelection_EvpnNetwork(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
//		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type_Hub");
		int StatusHeader=-1;
		int i=0;
		boolean found=false;
	    do
	    {
	   	if(i>0)
	    	{
	    		click(SiebelAddProdcutObj.product.ProductSelectionArrow,"Product arrow icon");
	    		Reusable.waitForSiebelLoader();
	    		waitForAjax();
	 
	    		i=0;
	    	}
    	List<WebElement> ProductList=SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//div[@class='product-name']"));
    	
	    	for(WebElement ele :ProductList)
			  {
	    		scrollIntoView(ele);
				 String Text=ele.getText();
				 if(Text.equalsIgnoreCase("Ethernet VPN Network"))
				 {
					 StatusHeader=i;
					 found=true;
					 break;
				 }
				 i++;
			  }	
	    	if(found)
	    		break;
	    }while(findWebElement(SiebelAddProdcutObj.product.ProductSelectionArrow).isDisplayed());
		
	    String plusButton=SiebelAddProdcutObj.product.addProductPlusIcon;
		String plusbutton=plusButton.replace("-10",String.valueOf(i+1));
	
		click(plusbutton,"Product plus Icon");
		Reusable.waitForSiebelLoader();
	}
	
	public void openServiceOrderNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		String serviceOrderRefNum = getTextFrom(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderReference_Number", serviceOrderRefNum);	
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();
	}
	
	public void openSpokeServiceOrderNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		String spokeServiceOrderRefNum = getTextFrom(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Site/SpokeOrderReference_Number", spokeServiceOrderRefNum);	
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();
	}

	public void openIPVPNSite(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws Exception {
		{
//			ServiceOrder.set(getAttributeFrom(SiebelAddProdcutObj.ServiceOrderReferenceNo2.ServiceOrderReferenceNo2,"Value"));
			
//			String SiteServiceOrder = ServiceOrder2.get();
			
			String SiteServiceOrder = getTextFrom(SiebelAddProdcutObj.ServiceOrderReferenceNo2.ServiceOrderReferenceNo2,"Value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Site/SpokeOrderReference_Number", SiteServiceOrder);
			
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Generated Service Order Reference No: " + ServiceOrder2.get());
			//Log.info(ServiceOrder2.get());

			click(SiebelAddProdcutObj.ServiceOrderReferenceNo2.ServiceOrderReferenceNo2,"Service Order ReferenceNo2");
			Reusable.waitForSiebelLoader();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Service Order Reference No");
		}
	}


	public void SelectAttachmentTab(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception {

		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");

		if (ProductName.toString().equals("IP Voice Solutions") 
				|| ProductName.toString().equals("SIP Trunking") 
				|| ProductName.toString().equals("Voice Line V")
				|| ProductName.toString().equals("Managed Dedicated Firewall")
				|| ProductName.toString().equals("Managed Virtual Firewall")) {

			verifyExists(SiebelAddProdcutObj.SelectAttachmentTab.tabDropdown,"Tab drop down");
			click(SiebelAddProdcutObj.SelectAttachmentTab.tabDropdown,"Tab drop down");
			verifyExists(SiebelAddProdcutObj.SelectAttachmentTab.Attachmentsoption,"Attachments");
			click(SiebelAddProdcutObj.SelectAttachmentTab.Attachmentsoption,"Attachments");
			
		//	select(SiebelAddProdcutObj.SelectAttachmentTab.tabDropdown,"Attachments");
			verifyExists(SiebelAddProdcutObj.SelectAttachmentTab.AttachmentTabSelection,"AttachmentTabSelection");
			click(SiebelAddProdcutObj.SelectAttachmentTab.AttachmentTabSelection,"AttachmentTabSelection");
			Reusable.waitForSiebelLoader();
			
		}
		}
	
	public void UploadDocument(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception {

		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		
		if (ProductName.toString().equals("IP Voice Solutions")) {
			UploadSOWTypeDocument(ProductName, "SOW");
		} else if (ProductName.toString().equals("Voice Line V") || ProductName.toString().equals("SIP Trunking")) {
			UploadSOWTypeDocument(ProductName,"Call Barring Form");
			
		} else if (ProductName.toString().equals("Managed Virtual Firewall")|| ProductName.toString().equals("Managed Dedicated Firewall")) {
			UploadSOWTypeDocument(ProductName, "Security Policy");
			
		}
		}
	
	
	

	public void UploadSOWTypeDocument(String Inputdata, String Filetype) throws Exception {

		if (Inputdata.toString().equals("Voice Line V") || Inputdata.toString().equals("SIP Trunking")) // Added by Rekha
		{
		//	Reusable.uploadafile(SiebelAddProdcutObj.UploadSOWTypeDocument.FileUpload, "test.txt");
			
			String uploadFile = g.getRelativePath()+"\\TestData\\test.txt";
			String uploadFile1 = g.getRelativePath()+"\\TestData\test1.txt";
		
			
			Reusable.UploadFile(SiebelAddProdcutObj.UploadSOWTypeDocument.FileUpload, uploadFile);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumnetTypeOther,"Other File Type");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumnetTypeOther,"Other File Type Clicked");
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DownArrow,"Down Arrow");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DownArrow,"Down Arrow");
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumentTypeCallBaringForm,"Document Type Call Baring Form");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumentTypeCallBaringForm,"CallBarring Form File Type Selected");
			Reusable.waitForSiebelLoader();
			
		//	Reusable.uploadafile(SiebelAddProdcutObj.UploadSOWTypeDocument.FileUpload, "test1.txt");
			Reusable.UploadFile(SiebelAddProdcutObj.UploadSOWTypeDocument.FileUpload, uploadFile1);
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumnetTypeOther,"Other File Type");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumnetTypeOther,"Other File Type Clicked");
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DownArrow,"Down Arrow");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DownArrow,"Down Arrow");
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumentTypeDisaster,"Disaster File Type");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumentTypeDisaster,"Disaster File Type Selected");
			
		} else {

            String uploadFile = g.getRelativePath()+"\\TestData\\test.txt";
			
			Reusable.UploadFile(SiebelAddProdcutObj.UploadSOWTypeDocument.FileUpload, uploadFile);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumnetTypeOther,"Other File Type");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DocumnetTypeOther,"Other File Type Clicked");
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DownArrow,"Down Arrow");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DownArrow,"Down Arrow");
			
			verifyExists(SiebelAddProdcutObj.UploadSOWTypeDocument.DoucmentTypeSelection.replace("Filetype", Filetype),"Disaster File Type");
			click(SiebelAddProdcutObj.UploadSOWTypeDocument.DoucmentTypeSelection.replace("Filetype", Filetype));
		}
	}
	
	public void SelectServiceGroupTab(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		
String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		
		if (ProductName.toString().equals("Voice Line V") || ProductName.toString().equals("SIP Trunking"))  {

			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//a[text()='Service Group']"))) {
			
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGroupTab,"Service Group");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGroupTab,"Service Group");
						
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				Reusable.waitForSiebelLoader();
			}
			else{
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
				
		//		select(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown, "Service Group");
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupoption,"Service Group");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupoption,"Service Group");
		
				
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				Reusable.waitForSiebelLoader();
			}
				
		}
	}
	
	public void EnterInstallationChargeInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		String Layer3Resilience= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
        String PrimarySecondary= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary/Secondary");
		
		Reusable.waitForSiebelLoader();
		if (!ProductName.toString().equals("Cloud Unified Communications") 
				&& !ProductName.toString().equals("Professional Services")){
			
	//		verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstalltionDropdown, "Installation and Test");
	//		click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstalltionDropdown, "Installation and Test");
			
	//		select(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstalltionDropdown, "Click on Installation Dropdown button and Select Installation and Test");
			
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
			click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Installation and Test')]"))) 
			{
				verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
				click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
			}
			else
			{
				verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstallationAndTestTab,"Installation and Test");
				click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstallationAndTestTab,"Installation and Test");
			}
					
			Reusable.waitForSiebelLoader();
			
			if (isElementPresent(By.xpath("//input[@aria-label='Primary Testing Method'][@aria-readonly='false']"))) 
			{
				if (ProductName.toString().equals("IP Access")||ProductName.toString().equals("Ethernet VPN Access")||
						ProductName.toString().equals("Ethernet Access")||ProductName.toString().equals("IP VPN Service")||
						ProductName.toString().equals("Ethernet Line")||ProductName.toString().equals("Ethernet Spoke")||
						ProductName.toString().equals("IP VPN Service")||ProductName.toString().equals("Private Wave Service")||
						ProductName.toString().equals("Private Ethernet")||ProductName.toString().equals("DCA Ethernets")||
						ProductName.toString().equals("Ethernet Hub")||ProductName.toString().equalsIgnoreCase("Wave")||
						ProductName.toString().equals("Ultra Low Latency")||ProductName.toString().equals("DCA Ethernet"))
				{ 
					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod,"Not Required");
					sendKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod, "Not Required");			
				}
				else
				{
					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod,"Primary Testing Method");
					sendKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod, "Primary Testing Method");
				}
			
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.PrimaryTestingMethod, Keys.TAB);
			}
//			if ((Layer3Resilience.toString().contains("Dual")) && (PrimarySecondary.toString().contains("Secondary")))
//			{
//				Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.SubmitOrder.SecondarySite,"Secondary Site");
//				click(SiebelModeObj.SubmitOrder.SecondarySite,"Secondary Site");
//				  
//				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
//				click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
//				Reusable.waitForSiebelLoader();
//				if (isElementPresent(By.xpath("//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Installation and Test')]"))) 
//				{
//					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
//					click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
//				}
//				else
//				{
//					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstallationAndTestTab,"Installation and Test");
//					click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.InstallationAndTestTab,"Installation and Test");
//				}
						
				Reusable.waitForSiebelLoader();
				
				if (isElementPresent(By.xpath("//input[@aria-label='Secondary Testing Method'][@aria-readonly='false']"))) 
				{
					if (ProductName.toString().equals("IP Access")||ProductName.toString().equals("Ethernet VPN Access")||ProductName.toString().equals("Ethernet Access")
							||ProductName.toString().equals("IP VPN Service")||ProductName.toString().equals("Ethernet Line")||ProductName.toString().equals("Ethernet Spoke")
							||ProductName.toString().equals("IP VPN Service")||ProductName.toString().equals("Private Wave Service")||ProductName.toString().equals("Private Ethernet")
							||ProductName.toString().equals("DCA Ethernets")||ProductName.toString().equals("Ethernet Hub")||ProductName.toString().equalsIgnoreCase("Wave")
							||ProductName.toString().equals("Ultra Low Latency")||ProductName.toString().equals("DCA Ethernet"))
					{ 
						verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SecondaryTestingMethod,"Not Required");
						sendKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SecondaryTestingMethod, "Not Required");			
					}
					else
					{
						verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SecondaryTestingMethod,"Primary Testing Method");
						sendKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SecondaryTestingMethod, "Primary Testing Method");
					}
					
					Reusable.SendkeaboardKeys(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SecondaryTestingMethod, Keys.TAB);
				}
//	          }
			Reusable.savePage();
			
			Reusable.waitForSiebelLoader();
			if (!ProductName.toString().equalsIgnoreCase("Wave")
					&& !ProductName.toString().equalsIgnoreCase("Interconnect")
					&& !ProductName.toString().equalsIgnoreCase("Ethernet Spoke")
					&& !ProductName.toString().equalsIgnoreCase("SWIFTNet")
					&& !ProductName.toString().equalsIgnoreCase("SIP Trunking")
					&& !ProductName.toString().equalsIgnoreCase("IP VPN Wholesale")
					&& !ProductName.toString().equalsIgnoreCase("Ethernet Line")) 
			{
//				if (isElementPresent(By.xpath("//a[@class='colt-noedit-config-save']"))) 
//				{
//					verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SaveOrderContinue,"Save Order Continue button");
//					click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.SaveOrderContinue,"Save Order Continue");
//					Reusable.waitForSiebelLoader();
//				}
			}
			Reusable.alertPopUp();
		}
		Reusable.alertPopUp();
//		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}
	
	public void CommercialValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

     //  String ServiceOrderReference_Number = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		
          Reusable.waitForSiebelLoader();
          Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelAddProdcutObj.CommercialValidation.ServiceOrderSourceRefNumber,"Service Order Source Ref Number");
	//	sendKeys(SiebelAddProdcutObj.CommercialValidation.ServiceOrderSourceRefNumber,ServiceOrderReference_Number,"Service Order Source Ref Number");
		sendKeys(SiebelAddProdcutObj.CommercialValidation.ServiceOrderSourceRefNumber,"12345","Service Order Source Ref Number");
		
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.CommercialValidation.OrderStatus,"Order Status");
		click(SiebelAddProdcutObj.CommercialValidation.OrderStatus,"Order Status");
		
		sendKeys(SiebelAddProdcutObj.CommercialValidation.OrderStatus,"Commercial Validation","Entered Order Status Commercial Validation");
			
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CommercialValidation.OrderStatus, Keys.ENTER);
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CommercialValidation.OrderStatus, Keys.TAB);
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();
		Reusable.waitToPageLoad();
		Reusable.waitForAjax();
		
	} 
	
	public void TechnicalValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		Thread.sleep(50000);
		String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
	//	Reusable.waitForSiebelLoader();
	//	Reusable.waitToPageLoad();
	//	Reusable.waitForAjax();
			verifyExists(SiebelAddProdcutObj.TechnicalValidation.OrderStatusDropdown,"Order Status Dropdown");
			click(SiebelAddProdcutObj.TechnicalValidation.OrderStatusDropdown, "Entered Order Status Technical Validation");
			 
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.TechnicalValidation.SelectTechnicalValidation,"Select Technical Validation");
			click(SiebelAddProdcutObj.TechnicalValidation.SelectTechnicalValidation,"Select Technical Validation");
			Reusable.waitForSiebelLoader();
			Reusable.alertPopUp();
			Reusable.waitForSiebelLoader();
//			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			
			if (ServiceType.toString().equalsIgnoreCase("IP VPN Wholesale")) // Added by Abhay
			{
				Reusable.ClickContinue();
				Reusable.waitForSiebelLoader();
			}
			Reusable.alertPopUp();
			if (ProductName.toString().equalsIgnoreCase("Wave")
					|| ProductName.toString().equalsIgnoreCase("Private Ethernet")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Line")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Hub")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Spoke")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Access")
					|| ProductName.toString().equalsIgnoreCase("Ultra Low Latency")
					|| ProductName.toString().equalsIgnoreCase("Dark Fibre")
					|| ProductName.toString().equalsIgnoreCase("DCA Ethernet")
					|| ProductName.toString().equalsIgnoreCase("SWIFTNet")
					|| ProductName.toString().equalsIgnoreCase("Ethernet VPN Access")
					|| ProductName.toString().equalsIgnoreCase("IP Access")
					|| ProductName.toString().equalsIgnoreCase("IP VPN Service")
				|| ProductName.toString().equals("Voice Line V")
				|| ProductName.toString().equalsIgnoreCase("IP Domain"))
			{
				Reusable.waitForSiebelLoader();
				// as per Aman added try catch
				if (isSelected(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox")) 
				{
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					Reusable.alertPopUp();
					Reusable.waitToPageLoad();
					Reusable.waitForSiebelLoader();
				} 
				else 
				{
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					click(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					
					Reusable.waitForSiebelLoader();
					Reusable.alertPopUp();
					Reusable.waitToPageLoad();
					Reusable.waitForSiebelLoader();
				}
				/*try {
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					Reusable.alertPopUp();
					Reusable.waitForSiebelLoader();
				} catch (Exception ex) {
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					click(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
										
					Reusable.waitForSiebelLoader();
				}*/
			}
	}
	
	public void TechnicalValidation_Hub(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		Thread.sleep(50000);
		String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type_Hub");
	//	Reusable.waitForSiebelLoader();
	//	Reusable.waitToPageLoad();
	//	Reusable.waitForAjax();
			verifyExists(SiebelAddProdcutObj.TechnicalValidation.OrderStatusDropdown,"Order Status Dropdown");
			click(SiebelAddProdcutObj.TechnicalValidation.OrderStatusDropdown, "Entered Order Status Technical Validation");
			 
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.TechnicalValidation.SelectTechnicalValidation,"Select Technical Validation");
			click(SiebelAddProdcutObj.TechnicalValidation.SelectTechnicalValidation,"Select Technical Validation");
			Reusable.waitForSiebelLoader();
			Reusable.alertPopUp();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			
			if (ServiceType.toString().equalsIgnoreCase("IP VPN Wholesale")) // Added by Abhay
			{
				Reusable.ClickContinue();
				Reusable.waitForSiebelLoader();
			}
			Reusable.alertPopUp();
			if (ProductName.toString().equalsIgnoreCase("Wave")
					|| ProductName.toString().equalsIgnoreCase("Private Ethernet")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Line")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Hub")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Spoke")
					|| ProductName.toString().equalsIgnoreCase("Ethernet Access")
					|| ProductName.toString().equalsIgnoreCase("Ultra Low Latency")
					|| ProductName.toString().equalsIgnoreCase("Dark Fibre")
					|| ProductName.toString().equalsIgnoreCase("DCA Ethernet")
					|| ProductName.toString().equalsIgnoreCase("SWIFTNet")
					|| ProductName.toString().equalsIgnoreCase("Ethernet VPN Access")
					|| ProductName.toString().equalsIgnoreCase("IP Access")
					|| ProductName.toString().equalsIgnoreCase("IP VPN Service")
				|| ProductName.toString().equals("Voice Line V")
				|| ProductName.toString().equalsIgnoreCase("IP Domain"))
			{
				Reusable.waitForSiebelLoader();
				// as per Aman added try catch
				if (isSelected(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox")) {
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					Reusable.alertPopUp();
					Reusable.waitForSiebelLoader();
				} else {
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					click(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
										
					Reusable.waitForSiebelLoader();
				}
				/*try {
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					Reusable.alertPopUp();
					Reusable.waitForSiebelLoader();
				} catch (Exception ex) {
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					click(SiebelAddProdcutObj.TechnicalValidation.BDWREVW_CheckBox,"BDWREVW CheckBox");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
					click(SiebelAddProdcutObj.TechnicalValidation.TriggerTRButton,"TriggerTR Button");
										
					Reusable.waitForSiebelLoader();
				}*/
			}
	}
	
	public void CircuitReferenceGeneration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		Random rand = new Random();
		String siteA = null;
		String siteB = null;
		String CircuitRefNumber =null;
		
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	     
		//verifyExists(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickLink.replace("Value", "Sites"));
		//click(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickLink.replace("Value", "Sites"));
		verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites tab");
		click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceAccess,"Circuit Reference Access");
		click(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceAccess,"Circuit Reference Access");
//		Reusable.waitForSiebelSpinnerToDisappear();
//		Reusable.waitForSiebelSpinnerToDisappear();
		Circuitreferencenumber.set(getAttributeFrom(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue,"value"));
		
		if(Circuitreferencenumber.get().isEmpty()==true)
		{
			if(isElementPresent(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site A address"))
			{
				     String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");     
				     siteA=siteA1.substring(0, 3);
			}
			if(isElementPresent(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site B address"))
			{
			     String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");
				 
			     siteB=siteB1.substring(0, 3);
			}
			if (siteA != null && siteB!= null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitRefNumber = siteA+"/"+siteB+"/"+"LE-"+randnumb;
			}else{
				if(siteA != null)
				{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitRefNumber = siteA+"/"+siteA+"/"+"LE-"+randnumb;
				}else{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitRefNumber = siteB+"/"+siteB+"/"+"LE-"+randnumb;
				}
			
			}
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitRefNumber,"Circuit Reference Number");		
			Reusable.waitForSiebelLoader();
			Robot robot = new Robot();
	        robot.keyPress(KeyEvent.VK_TAB);
//	        Reusable.waitForSiebelSpinnerToDisappear();
	        Reusable.waitForSiebelLoader();
			Reusable.savePage();
//			Reusable.waitForSiebelSpinnerToDisappear();
	        Reusable.waitForSiebelLoader();
			Circuitreferencenumber.set(CircuitRefNumber);
		//	Reusable.savePage();
		}
		
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Generated circuit reference No: " + Circuitreferencenumber.get());
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}
	
	public void DeliveryValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.DeliveryValidation.OrderStatusDropdown,"Order Status Dropdown");
		click(SiebelAddProdcutObj.DeliveryValidation.OrderStatusDropdown,"Order Status Dropdown");
			
//		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.DeliveryValidation.SelectDeliveryValidation,"Select Delivery Validation");
		click(SiebelAddProdcutObj.DeliveryValidation.SelectDeliveryValidation,"Entered Order Status Delivery");
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();
		
		if(isElementPresent(By.xpath("//span[text()='OK']"))) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.DeliveryAlertAccept,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.DeliveryAlertAccept,"Click on Alert Accept");
		}
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();

		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		if (ServiceType.toString().equalsIgnoreCase("IP VPN Wholesale")) 
		{
			Reusable.ClickContinue();
			Reusable.waitForSiebelLoader();
		}
	}
	
	public void LaunchingCEOSApplication(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String username, String password) throws Exception 
	{
		String CEOSNum = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOS_OrderReferenceNo");
		String reference_no = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"reference_no");
		String Supplier_CircuitID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Supplier_circuit");
		String Supplier_Product = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SupplierCeos");
		String Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String Interface = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOS_Interface");
		String Connector = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Connector_Type");
		String Offnet_Access_Technology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOS_OffnetAccessTech");
		String Tariff = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Tariff");
		String CEOSCity = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOSCity");
		String CEOSSite = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOSSite");
		String Handover_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOSSite");
		String Channel_VCI = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Channel_VCI");
		String BearerID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"BearerID");
		String Port = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEOSPort");
		String DDF_ODF_SHELF = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DDF/ODF");
		
		////Open CEOS URL and perform login action
		
		openBrowser("firefox");
		openurl(Configuration.CEOS_URL);
		Reusable.waitForSiebelLoader();
				
		try
		{

			waitForElementToAppear(SiebelAddProdcutObj.LaunchingCEOSApplication.userNameTxb, 5);
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.userNameTxb,"User Name textbox");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.userNameTxb, username,"Enter User Name");
				
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.passWordTxb,"Password textbox");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.passWordTxb, password,"Password textbox field");
				
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.authenticationTxb,"Authentication");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.authenticationTxb,"Authentication");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.loginBtn,"Login button");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.loginBtn,"Login button");
			Reusable.waitForSiebelLoader();
			
//				verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"TB search save changes");
//				click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Clicked on Searchbtn");
				
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.CeosOrder,"CeosOrder");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.CeosOrder,CEOSNum);
				
			if(isElementPresent(By.xpath(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept)))
			{			
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				click(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				Reusable.waitForSiebelLoader();
			}			
				
			if(isElementPresent(By.xpath(SiebelAddProdcutObj.LaunchingCEOSApplication.AcceptUser)))
			{			
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.AcceptUser,"Accept");
				click(SiebelAddProdcutObj.LaunchingCEOSApplication.AcceptUser,"Accept");
				Reusable.waitForSiebelLoader();
			}			
			
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Search btn");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Clicked on Searchbtn");
			Reusable.waitForSiebelLoader();

			/*dr.findElement(By.id("TBsearchsavechanges")).click();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clicked on Searchbtn ");
			Reusable.waitForSiebelLoader();*/
			
			
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierCircuitID,"CeosOrder");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierCircuitID,Supplier_CircuitID, "Supplier Circuit ID");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierOrderingReference,"Supplier Ordering Reference");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierOrderingReference,reference_no,"Supplier Ordering Reference");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierProduct,"Supplier Product");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierProduct,"Supplier Product");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierProduct,"Supplier Product");
			PickValue(Supplier_Product);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.Bandwidth,"Bandwidth");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Bandwidth,"Bandwidth");
			Reusable.waitForSiebelLoader();

			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Bandwidth,"Bandwidth");
			PickValue("1 Gbps");
			Reusable.waitForSiebelLoader();
								
//				click(SiebelAddProdcutObj.LaunchingCEOSApplication.Bandwidth1.replace("Value", reference_no));
//				Reusable.waitForSiebelLoader();
				
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.Interface,"Interface");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Interface,"Interface");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Interface,"Interface");
			PickValue(Interface);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.Connector,"Connector");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Connector,"Connector");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Connector,"Connector");
			PickValue(Connector);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.OffnetAccessTechnology,"Offnet Access Technology");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.OffnetAccessTechnology,"OffnetTech");
			Reusable.waitForSiebelLoader();

			//click(SiebelAddProdcutObj.LaunchingCEOSApplication.OffnetAccessTechnology,"Offnet Access Technology");
			PickValue(Offnet_Access_Technology);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.OLOAdmin,"OLO Admin");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.OLOAdmin,"ctaf");
			Reusable.waitForSiebelLoader();
			
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.OrderingDate,"Ordering Date");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.OrderingDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.RequestedDate,"Requested Date");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.RequestedDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierConfirmationDate,"Supplier Confirmation Date");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierConfirmationDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.ConfirmedDeliveryDate,"Confirmed Delivery Date");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.ConfirmedDeliveryDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.Expecteddeliverydate,"Expected delivery Date");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.Expecteddeliverydate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierHandoverDate,"Supplier HandOver Date");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierHandoverDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.InstallCost,"Install Cost");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.InstallCost, "Install Cost");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.InstallCost, "1.00");
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.RecurringCost,"Recurring Cost");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.RecurringCost, "Recurring Cost");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.RecurringCost, "1.00");
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.ContractPeriod,"Contract Period");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.ContractPeriod, "3");
			Reusable.waitForSiebelLoader();
			
//			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.ContractEnds,"Contract Ends");
//			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.ContractEnds,Reusable.CurrentDate());
//			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.Tariff,"Tariff");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Tariff, "Tariff");
			Reusable.waitForSiebelLoader();

//			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Tariff,"Tariff");
			PickValue(Tariff);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.OLOUse,"OLO Use");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.OLOUse, "OLO Use");
			Reusable.waitForSiebelLoader();

			PickValue("Customer access ");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierInvoiceStartDate,"Supplier Invoice StartDate");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.SupplierInvoiceStartDate,Reusable.CurrentDate() );
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Save btn");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Clicked on Savebtn");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.InterconnectDetails,"Interconnect Details");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.InterconnectDetails,"Interconnect Details Tab" );
			Reusable.waitForSiebelLoader();
											
//			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.ICNotRequiredcheckbox1,"Interconnect Not Required Chekbox");
//			click(SiebelAddProdcutObj.LaunchingCEOSApplication.ICNotRequiredcheckbox1,"Interconnect Not Required Chekbox" );
//			Reusable.waitForSiebelLoader();
//			
//			if(isElementPresent(By.xpath(SiebelAddProdcutObj.LaunchingCEOSApplication.ICNotReqConf)))
//			{			
//				Reusable.waitForSiebelLoader();
//				verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.ICNotReqConf,"Alert Accept");
//				click(SiebelAddProdcutObj.LaunchingCEOSApplication.ICNotReqConf,"Alert Accept");
//				Reusable.waitForSiebelLoader();
//			}			

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.City,"City");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.City, "City");
			Reusable.waitForSiebelLoader();

			PickValue(CEOSCity);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.Site,"Site");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.Site, "Site");
			Reusable.waitForSiebelLoader();

			PickValue(CEOSSite);
			Reusable.waitForSiebelLoader();
			
			if(Handover_Type.contains("Bearer"))
			{
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.BearerHandover,"BearerHandover");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.BearerHandover,"BearerHandover");
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.BearerID,BearerID);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.Channel,Channel_VCI);
			Reusable.waitForSiebelLoader();
			}
			else
			{
				verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.PortHandover,"PortHandover");
				click(SiebelAddProdcutObj.LaunchingCEOSApplication.PortHandover,"PortHandover");
				Reusable.waitForSiebelLoader();
				
				sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.DDF_ODF_Shelf,DDF_ODF_SHELF);
				Reusable.waitForSiebelLoader();
				
				sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.Port,Port);
				Reusable.waitForSiebelLoader();
			}
						
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.XngtransferstatusButton,"XngtransferstatusButton");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.XngtransferstatusButton,"XngtransferstatusButton");
			Reusable.waitForSiebelLoader();
			
			String xngstatus=getAttributeFrom(SiebelAddProdcutObj.LaunchingCEOSApplication.Xngtransferstatus,"Xngtransferstatus");
			String NCstatus=getAttributeFrom(SiebelAddProdcutObj.LaunchingCEOSApplication.NCtransferstatus,"Xngtransferstatus");
			Reusable.waitForSiebelLoader();

	//		Assert.assertEquals(xngstatus,"SUCCESS");
			Assert.assertEquals(NCstatus,"Success");

			r.keyPress(KeyEvent.VK_ENTER);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"TB search save changes");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Clicked on Savebtn");
			Reusable.waitForSiebelLoader();
				
			//newly added given by Gokul
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBnewsearch,"TB New Search");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBnewsearch,"Clicked on New Search btn");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.CeosOrder,"CeosOrder");
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.CeosOrder,CEOSNum);
			
			if(isElementPresent(By.xpath(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept)))
			{
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				click(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				Reusable.waitForSiebelLoader();
			}
			
			if(isElementPresent(By.xpath(SiebelAddProdcutObj.LaunchingCEOSApplication.AcceptUser)))
			{
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.AcceptUser,"Accept");
				click(SiebelAddProdcutObj.LaunchingCEOSApplication.AcceptUser,"Accept");
				Reusable.waitForSiebelLoader();
			}
			
			r.keyPress(KeyEvent.VK_ENTER);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Search btn");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Clicked on Searchbtn");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.ActualDeliveryRfsDate,"Actual Delivery (RFS) Date");
			waitForElementToAppear(SiebelAddProdcutObj.LaunchingCEOSApplication.ActualDeliveryRfsDate, 10);
			sendKeys(SiebelAddProdcutObj.LaunchingCEOSApplication.ActualDeliveryRfsDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.OrderManagement,"Order Management");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.OrderManagement,"Order Management Tab" );
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.FailureParty,"Failure Party");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.FailureParty,"Failure Party");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.FailureParty,"Failure Party");
			PickValue("COLT");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.FailureReason,"Failure Reason");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.FailureReason,"Failure Reason");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.FailureReason,"Failure Reason");
			PickValue("COLT capacity issue");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Save btn");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.TBsearchsavechanges,"Clicked on Savebtn");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.logoutBtn,"Logout button");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.logoutBtn,"Logout button");
			Reusable.waitForSiebelLoader();	
		}
		catch(Exception e)
		{
			verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.logoutBtn,"Logout button");
			click(SiebelAddProdcutObj.LaunchingCEOSApplication.logoutBtn,"Logout button");
			Reusable.waitForSiebelLoader();
			Report.LogInfo("Exception", "Exception in Launching CEOS Application "+e.getMessage(), "FAIL");
		}

		closeWebdriver();					

	}
	
	public void getReferenceNo(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception, Exception {
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		
		if (ProductName.toString().equalsIgnoreCase("Wave")
				|| ProductName.toString().equalsIgnoreCase("Ethernet Line")|| ProductName.toString().equalsIgnoreCase("Ethernet VPN Access")
				|| ProductName.toString().equalsIgnoreCase("Private Ethernet") 
				|| ProductName.toString().equalsIgnoreCase("DCA Ethernet")|| ProductName.toString().equalsIgnoreCase("Ultra Low Latency"))
		{
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Expected Delivery Date");
			click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
			Reusable.waitForSiebelLoader();
						
			GetReference(testDataFile,sheetName,scriptNo, dataSetNo);
		//	Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();	
		}
	}
	
	public void GetReference(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException, IOException {
		String CircuitReference_Value = null;
		String siteA = null;
		String siteB = null;
		Random rand = new Random();
		
		verifyExists(SiebelAddProdcutObj.GetReference.GetReference,"Get Reference");
		click(SiebelAddProdcutObj.GetReference.GetReference,"Get Reference");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		//Circuitreferencenumber.set(getAttributeFrom(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue),"value");
		Circuitreferencenumber.set(getAttributeFrom(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue,"value"));
		//System.out.println(Circuitreferencenumber.get());
	//	ExtentTestManager.getTest().log(LogStatus.PASS," Step: Generated circuit reference No: " + Circuitreferencenumber.get());
		
		CircuitReference_Value=Circuitreferencenumber.get();
		
		if(CircuitReference_Value.isEmpty()==true)
		{
			if(isElementPresent(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site A address"))
			{
			
				     String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");     
				     siteA=siteA1.substring(0, 3);
			}
			if(isElementPresent(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site B address"))
			{
			     String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");
				 
			     siteB=siteB1.substring(0, 3);
			}
			
			
			if (siteA != null && siteB!= null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitReference_Value = siteA+"/"+siteB+"/"+"LE-"+randnumb;
			}else{
				if(siteA != null)
				{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitReference_Value = siteA+"/"+siteA+"/"+"IA-"+randnumb;
				}else{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitReference_Value = siteB+"/"+siteB+"/"+"IA-"+randnumb;
				}
			
			}
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitReference_Value,"Circuit Reference Number");		
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}
		
		if(isElementPresent(By.xpath("//span[text()='Other Circuit Reference']/following-sibling::input"),"Other Circuite Referance"))
		{
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.OtherCircuitReferenceValue, CircuitReference_Value,"Circuit Reference Number");		
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, Keys.TAB);
		}
		
		
		Reusable.savePage();
		
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	
		Reusable.waitForSiebelLoader();
		
			
	}
	
		
	public void Selectproduct(String productname) throws InterruptedException, AWTException, IOException 
	{
		//String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		int StatusHeader=-1;
		int i=0;
		boolean found=false;
	    do
	    {
	   	if(i>0)
	    	{
	    		click(SiebelAddProdcutObj.product.ProductSelectionArrow,"Product arrow icon");
	    		Reusable.waitForSiebelLoader();
	    		waitForAjax();
	 
	    		i=0;
	    	}
    	List<WebElement> ProductList=webDriver.findElements(By.xpath("//div[@class='product-name']"));
    	
	    	for(WebElement ele :ProductList)
			  {
	    		scrollIntoView(ele);
				 String Text=ele.getText();
				 if(Text.equalsIgnoreCase(productname))
				 {
					 StatusHeader=i;
					 found=true;
					 break;
				 }
				 i++;
			  }	
	    	if(found)
	    		break;
	    }while(findWebElement(SiebelAddProdcutObj.product.ProductSelectionArrow).isDisplayed());
		
	    String plusButton=SiebelAddProdcutObj.product.addProductPlusIcon;
		String plusbutton=plusButton.replace("-10",String.valueOf(i+1));
	
		click(plusbutton,"Product plus Icon");
		Reusable.waitForSiebelLoader();
	}	
	
		
	public void createFile(String Path) throws IOException {
		FileOutputStream fos = new FileOutputStream(Path);
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("NewOrder");
		Row row = sheet.createRow(0);
		Cell cell0 = row.createCell(0);
		cell0.setCellValue("Product Name");

		Cell cell1 = row.createCell(1);
		cell1.setCellValue("Service OrderNumber");

		Cell cell2 = row.createCell(2);
		cell2.setCellValue("Date");
		workbook.write(fos);
		fos.flush();
		fos.close();
	}
	
	public void enterMandatoryFieldsInHeader(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		
		String MaintenanceAddress = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Input Party Addres Street Name");

		waitToPageLoad(); 
	
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch,"Order Sub Type Search");
		click(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch,"Order Sub Type Search");
		
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType,"Add Order Sub Type");
		click(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType,"Add Order Sub Type");
		
		String OrderSubtype = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OrderSubtype");
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType, OrderSubtype, "Input Order Sub Type");
		
		Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);
	    
	    waitForAjax();
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType,"Submit Sub Order Type");
		click(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType,"Submit Sub Order Type");
		
		Reusable.waitForSiebelLoader();
		
		 robot.keyPress(KeyEvent.VK_TAB);
	
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ContractSearch, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.ContractSearch,"Contract Search");
		click(SiebelModeObj.MandatoryFieldsInHeader.ContractSearch,"Contract Search");
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputContractId, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.InputContractId,"InputContractId");
		
		String ContractId = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ContractId");
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputContractId, ContractId, "Input Contract Id");

	
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ContractIdSearch, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.ContractIdSearch,"Contract Id Search");
		click(SiebelModeObj.MandatoryFieldsInHeader.ContractIdSearch,"Contract Id Search");
		
		Reusable.waitForSiebelLoader();
	
		String Product_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");

		
		if (Product_Type.equals("Ethernet Hub")) {
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
			click(SiebelModeObj.MandatoryFieldsInHeader.AddNetworkReferenceSearch,"Add Network Reference Search");
	
			String HubNetworkReferenceNo = getAttributeFrom(SiebelModeObj.MandatoryFieldsInHeader.TextNetworkReference, "value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "HubNetworkReferenceNo", HubNetworkReferenceNo);				
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			
			Reusable.waitForSiebelLoader();
			
		} else if (Product_Type.equals("Ethernet Spoke") || Product_Type.equals("Dark Fibre")
				|| Product_Type.equals("Private Wave Node")||Product_Type.equals("Ethernet VPN Access")) {
			
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
		
			
			String Call_Admission_Control = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Network reference");
			sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputNetworkReference, Call_Admission_Control, "Network reference");

			
			
		
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SearchNetworkReference,"Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SearchNetworkReference,"Network Reference");
			

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
		//	waitforPagetobeenable();
			

		} else if (Product_Type.equals("HNS")) {
			
			
			String Hub_Network_Reference_No = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Network reference from New Hub");
			sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputNetworkReference, Hub_Network_Reference_No, "Network reference from New Hub");
		
			

		}
		
		if (!Product_Type.equals("Cloud Unified Communications")
				&& !Product_Type.equals("IP Voice Solutions")
				&& !Product_Type.equals("Professional Services")
				&& !Product_Type.equalsIgnoreCase("Wave")
				&& !Product_Type.equalsIgnoreCase("Ethernet Line")
				&& !Product_Type.equals("Ethernet Spoke") && !Product_Type.equals("Ethernet Hub")
				&& !Product_Type.equals("Private Ethernet")
				&& !Product_Type.equals("Private Wave Service")
				&& !Product_Type.equals("DCA Ethernet")
				&& !Product_Type.equals("Ultra Low Latency")
				&& !Product_Type.equals("Ethernet Spoke")
				&& !Product_Type.equals("Ethernet Access")
				&& !Product_Type.equals("Private Wave Node")
				&& !Product_Type.equals("IP VPN Service")
				&& !Product_Type.equals("Ethernet VPN Access")) {
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch, 10);
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
		
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.NetworkPlusSign, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkPlusSign,"Network Plus Sign");
			
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SelectNetworkReference,"Select Network Reference");

			click(SiebelModeObj.MandatoryFieldsInHeader.SelectNetworkReference,"Select Network Reference");
			
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			 Reusable.waitForSiebelLoader();
			
		}
	
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary, 10);
		
		String ExistingCapacityLeadTimePrimary = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ExistingCapacityLeadTimePrimary");
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary,"Existing Capacity Lead Time Primary");

		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType, ExistingCapacityLeadTimePrimary, "Existing Capacity Lead Time Primary");
		
		Reusable.SendkeaboardKeys(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary, Keys.ENTER);

		Reusable.SendkeaboardKeys(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary, Keys.TAB);
		
		   
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartySearch,"Maintenance Party Search");
		
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyPopupDropdown,"Maintenance Party Popup Dropdown");
		javaScriptclick(SiebelModeObj.MandatoryFieldsInHeader.AccountStatus,"Account Status");
			
			
		String MaintenanceParty = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"MaintenanceParty");
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputAccountStatus, MaintenanceParty, "Input Account Status");
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSearch, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSearch,"Account Status Search");
		Reusable.waitForSiebelLoader();
		
			
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSubmit, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSubmit,"Account Status Submit");
	

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContact, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContact,"Maintenance Party Contact");
		Reusable.waitForSiebelLoader();
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactPopupDropdown, 10);
																									
		javaScriptclick(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactPopupDropdown,"Maintenance Party Contact Popup Dropdown");
		
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenanceLastName,"Maintenance Last Name");
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastName, 10);
		String MaintenanceContact = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"InputMaintenanceLastName");
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastName, MaintenanceContact, "Input Maintenance LastName");
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastNameSearch, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastNameSearch,"Input Maintenance LastName Search");

		Reusable.waitForSiebelLoader();
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactSubmit, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactSubmit,"Maintenance Party Contact Submit");
		

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddress, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddress,"Maintenance Party Address");	
		Reusable.waitForSiebelLoader();
		
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddresPopupDropdown, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddresPopupDropdown,"Maintenance Party Address popup Dropdown");	
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.PartyAddresStreetName, 10);
		
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetName, MaintenanceAddress, "Input Party Addres Street Name");
		
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetNameSearch, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetNameSearch,"Input Party Addres Street Name Search");	
		
		Reusable.waitForSiebelLoader();
		
			
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddressSubmit, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddressSubmit,"Maintenance Party Address Submit");	
		Reusable.waitForSiebelLoader();
	
		click("//input[@aria-labelledby='COLT_ProContact_FullName_Label']/following-sibling::span");
			
			
			Reusable.waitForElementToAppear("//button[@aria-label='Pick Contact:OK']",10);
			click("//button[@aria-label='Pick Contact:OK']");
			
			Reusable.savePage();
			
			Reusable.waitToPageLoad();
			Reusable.waitForSiebelLoader();
			
		   if ((Product_Type.toString().equals("IP VPN Service"))|| (Product_Type.toString().equals("Ultra Low Latency"))
				   || (Product_Type.toString().equals("DCA Ethernet"))|| (Product_Type.toString().equals("Private Ethernet")))
			   
		   {
			   Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSearch, 10);
               click(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSearch,"Proactive Contact Search");
               Reusable.waitForSiebelLoader();
               Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContact, 10);
               click(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContact,"Proactive Contact");

               Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSubmit, 10);
               click(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSubmit,"Proactive Contact Submit");
		   }
//		   waitToPageLoad();
		   Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) {
				
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges,"Save Order Changes");
				
			waitToPageLoad();
				
			Reusable.waitForSiebelLoader();
			
			}
	}
	
	public void NetworkReferenceFill(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
	{
		String NetworkReferenceNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"NetworkReference_Number");
		
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		waitForElementToAppear(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceSearch, 20);
		verifyExists(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceSearch,"Network Reference Search");
		Reusable.waitForSiebelLoader();
		click(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceSearch,"Network Reference Search");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		sendKeys(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceInput,NetworkReferenceNumber,"NetworkReferenceNumber");

		
	//	sendKeys(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceInput,NetworkReferenceIPVPN.get().toString());
	//	sendKeys(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceInput,siebelmod.NetworkReferenceIPVPN.get().toString());
	
		Reusable.waitForSiebelLoader();
		click(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceSearchin,"Network Reference Searching");
		Reusable.waitForSiebelLoader();
		click(SiebelAddProdcutObj.NetworkReferenceFill.NetworkReferenceGo,"Network Reference Go");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}

	
	public void ServiceChargeforIPVPNSite(String SamleName,String testDataFile, String sheetName, String scriptNo, String dataSetNo, String Amount) throws Exception {
		

		String BCN = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"BCN");
		
		verifyExists(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.ExpandAllButton,"Expand All Button");
		click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.ExpandAllButton, "Expand All Button");
		Reusable.waitForSiebelLoader();
		
		int RowCount = getXPathCount(SiebelModeObj.EnterServiceChargeInFooter.BillingRow) - 1;
		
		if (RowCount!=0)
		{
		while (!getAttributeFrom(SiebelModeObj.EnterServiceChargeInFooter.BillingLastRow, "class")
				.contains("highlight")) {

	//		RowCount = getXPathCount(SiebelModeObj.EnterServiceChargeInFooter.BillingRow) - 1;
			System.out.println(RowCount);

			
			for (int i = 1; i <= RowCount; i++) {
				Reusable.waitForSiebelLoader();
				javaScriptclick(SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmount.replace("Value",String.valueOf(i)));
				Reusable.waitForSiebelLoader();
				
				if (!getAttributeFrom((SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmount.replace("Value",String.valueOf(i))), "class").contains("disabled"))
					 {	
					sendKeys(SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmountInput.replace("Value", String.valueOf(i)), Amount);
					Reusable.waitForSiebelLoader();
				} else {
					System.out.println("Not Required to fill");
				}

				javaScriptclick(SiebelModeObj.EnterServiceChargeInFooter.BillingRowBCN.replace("Value",String.valueOf(i)));
				Reusable.waitForSiebelLoader();
				
				if (!getAttributeFrom((SiebelModeObj.EnterServiceChargeInFooter.BillingRowBCN.replace("Value",String.valueOf(i))), "class").contains("disabled"))
				{
					click(SiebelModeObj.EnterServiceChargeInFooter.BCNSearchClick,"BCN Search");
					Reusable.waitForSiebelLoader();
					sendKeys(SiebelModeObj.EnterServiceChargeInFooter.BCNInstallationChargeNRCInput,BCN, "Enter BCNInstallationChargeNRCInput");
					click(SiebelModeObj.EnterServiceChargeInFooter.BCNNRCSearch,"Click BCN Search ");
					Reusable.waitForSiebelLoader();

				} else {
					System.out.println("Not Required to fill");
				}
		
			}
			//click(SiebelModeObj.EnterServiceChargeInFooter.FirstLineitem,"Click FirstLineitem");
			//click(SiebelModeObj.EnterServiceChargeInFooter.ClickNextPage,"Click on next page option");
		
			Reusable.waitForSiebelLoader();	
			Reusable.savePage();
		}
		
	}
		
}
		

	
//////////////////////////KK////
	public void OperationAttribute(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		String ColName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ProductType");

		if (ColName.toString().equals("Voice Line V")) 
		{
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.OperationAttribute.Voiceconfigtab,"Voice Config Tab");
			click(SiebelAddProdcutObj.OperationAttribute.Voiceconfigtab,"Voice Config Tab");

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OperationAttribute,"Operation Attribute");
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttribute,"Operation Attribute");

			verifyExists(SiebelAddProdcutObj.OperationAttribute.AttributeNew,"Attribute New");
			click(SiebelAddProdcutObj.OperationAttribute.AttributeNew,"Attribute New");

			verifyExists(SiebelAddProdcutObj.OperationAttribute.AttributeNameDropDown,"Attribute Name DropDown");
			click(SiebelAddProdcutObj.OperationAttribute.AttributeNameDropDown,"Attribute Name DropDown");
			click(SiebelAddProdcutObj.OperationAttribute.AttributeName,"Attribute Name");
			Reusable.waitForSiebelLoader();
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttributeSubmit,"Click on Submit");

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OtherTab,"Click on OtherTab");
			click(SiebelAddProdcutObj.OperationAttribute.OtherTab,"Click on OtherTab");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OperationAttribute1,"Click on Operation Attribute");
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttribute1,"Click on Operation Attribute");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.AttributeValue1,"Attribute Value1");
			click(SiebelAddProdcutObj.OperationAttribute.AttributeValue1,"Attribute Value1");
			sendKeys(SiebelAddProdcutObj.OperationAttribute.AttributeValue1,"test");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OperationAttributeSubmit,"Submit button");
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttributeSubmit,"Submit button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OperationAttribute2,"Operation Attribute");
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttribute2,"Operation Attribute");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.AttributeValue1,"Attribute Value1");
			click(SiebelAddProdcutObj.OperationAttribute.AttributeValue1,"Attribute Value1");
			sendKeys(SiebelAddProdcutObj.OperationAttribute.AttributeValue1,"test");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OperationAttributeSubmit,"Submit button");
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttributeSubmit,"Submit button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.OperationAttribute,"Operation Attribute");
			click(SiebelAddProdcutObj.OperationAttribute.OperationAttribute,"Operation Attribute");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.OperationAttribute.AttributeValueDropDown,"Attribute Value DropDown");
			click(SiebelAddProdcutObj.OperationAttribute.AttributeValueDropDown,"Attribute Value DropDown");
			Reusable.waitForSiebelLoader();
			click(SiebelAddProdcutObj.OperationAttribute.AttributeValue,"Attribute Value");
		}
	}

	public void OrderCompleteEthernetHubSpoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		String HubServiceOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		String SpokeServiceOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site/SpokeOrderReference_Number");
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		
		verifyExists(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.InputOrderNo, HubServiceOrder);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.SearchOrderNo);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.SearchOrderNo);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.ClickOrderNo);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.ClickOrderNo);
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
		click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
		
		verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
		click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.Installationoption,"Installation and Test");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.PrimaryTestingMethod,"Primary Testing Method");
		sendKeys(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.PrimaryTestingMethod,"Not Required");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
//		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		
        waitForElementToAppear(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.ManualValidation, 10);
		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.ManualValidation,"Manual Validation");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.ManualValidation,"Manual Validation");
		Reusable.waitForSiebelLoader();
		
		/*if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
		{		
			Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
			Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}	*/

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatus,"Order Status");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatus,"Order Status");
		clearTextBox(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatus);

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatusDropdown,"Order status drop down");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderStatusDropdown,"Order status drop down");

		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.SelectCompleted,"Completed Status");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.SelectCompleted,"Completed Status");
		Reusable.waitForSiebelLoader();		
		
		verifyExists(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderComplete,"Order Complete");
		click(SiebelAddProdcutObj.OrderCompleteEthernetHubSpoke.OrderComplete,"Order Complete");
		Reusable.waitForSiebelLoader();
		
		if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
		}
		if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) {
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
			click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
		}
		Reusable.alertPopUp();
 		Reusable.waitForSiebelLoader();
		String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus.contains("Progress")) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			waitForElementToAppear(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,10);
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}	
		System.out.println("Order complete");
		String CompValidation= null;
		CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		
		if(CompValidation.equals("Completed")) 
		{
			Report.LogInfo("verify","<i></b> order status completed", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> order status not competed", "Fail");
		}
		
		Reusable.waitForSiebelLoader();
		Reusable.WaitForAjax();
		Reusable.waitForAjax();
		Reusable.alertPopUp();
	}

	/*
	public void openAsset(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Asset Button");
		click(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Asset Button");

		if(isElementPresent(By.xpath(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept)))
		{			
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
			click(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Alert Button");
			click(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Alert Button");
		}
		Reusable.waitForSiebelLoader();

		String Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		String[] Order1=Order.split("/");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,"ServiceOrderOM");
		click(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,"ServiceOrderOM");
		sendKeys(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,Order1[0]);

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.validateSlaMatrix.GoButton,"GoButton");
		click(SiebelAddProdcutObj.validateSlaMatrix.GoButton,"GoButton");

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetNumber,"AssetNumber");
		click(SiebelAddProdcutObj.validateSlaMatrix.AssetNumber,"AssetNumber");
	}*/

	public void validateSlaMatrix(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		String ColName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SLA_Tier_values");
		String Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Completed_Service_order");
		String SLA = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SLA_Tier_values");
		
		if (ColName.toString().contains("SLA")) 
		{
		
			openAsset(testDataFile, sheetName, scriptNo, dataSetNo);
			Reusable.waitForSiebelLoader();
		
			/*
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Asset Button");
			click(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Asset Button");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept)))
			{			
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				click(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Alert Button");
				click(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Alert Button");
			}
			Reusable.waitForSiebelLoader();

			
			String[] Order1=Order.split("/");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,"ServiceOrderOM");
			click(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,"ServiceOrderOM");
			sendKeys(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,Order1[0]);

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.GoButton,"GoButton");
			click(SiebelAddProdcutObj.validateSlaMatrix.GoButton,"GoButton");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetNumber,"AssetNumber");
			click(SiebelAddProdcutObj.validateSlaMatrix.AssetNumber,"AssetNumber");
			
			*/
/*
			try
			{
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.InstalltionDropdown,"Entitlements Tab");
				select(SiebelAddProdcutObj.validateSlaMatrix.InstalltionDropdown,"Entitlements");
			}
			catch(Exception e)
			{
				waitForElementToAppear(SiebelAddProdcutObj.validateSlaMatrix.EntitlementsTab,10);
				click(SiebelAddProdcutObj.validateSlaMatrix.EntitlementsTab,"Entitlements Tab");
			}
			*/
			
			if (isElementPresent(By.xpath("//a[text()='Entitlements']"))) {
				waitForElementToAppear(SiebelAddProdcutObj.validateSlaMatrix.EntitlementsTab,10);
				click(SiebelAddProdcutObj.validateSlaMatrix.EntitlementsTab,"Entitlements Tab");
			}
			else{
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.InstalltionDropdown,"InstalltionDropdown");
			//	select(SiebelAddProdcutObj.validateSlaMatrix.InstalltionDropdown,"Entitlements");
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.Entitlementsoption,"Entitlementsoption");
				click(SiebelAddProdcutObj.validateSlaMatrix.Entitlementsoption,"Entitlementsoption");
				Reusable.waitForSiebelLoader();
				waitForElementToAppear(SiebelAddProdcutObj.validateSlaMatrix.EntitlementsTab,10);
				click(SiebelAddProdcutObj.validateSlaMatrix.EntitlementsTab,"Entitlements Tab");
			}
			
			Reusable.waitForSiebelLoader();
			String ActualSLA=getTextFrom(SiebelAddProdcutObj.validateSlaMatrix.SlaTag);
			
			String ExpectedSLA=SLA;
			if (ActualSLA.contains(ExpectedSLA)) 
			{
				Report.LogInfo("VerifyExixts","Validate SLA matrix is displayed as " +ActualSLA, "PASS");
			}
			else 
			{
				Report.LogInfo("VerifyExixts","Validate SLA matrix is displayed as " +ActualSLA, "FAIL");
			}
		}

	}

	public void Carnor(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		String Carnorflag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo, "CarNor_RunFlag");
		String Layer3Resilience = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		
		waitForElementToAppear(SiebelAddProdcutObj.Carnor.CarReason,10);
		click(SiebelAddProdcutObj.Carnor.CarReason,"Carnor reason field");
		sendKeys(SiebelAddProdcutObj.Carnor.CarReason,"Access Line Change");
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.Carnor.CarReason, Keys.TAB);	

		Reusable.waitForSiebelLoader();
		click(SiebelAddProdcutObj.Carnor.CarNorRunFlag,"CarNorRunFlag");
		sendKeys(SiebelAddProdcutObj.Carnor.CarNorRunFlag,Carnorflag);
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.Carnor.CarNorRunFlag, Keys.TAB);	

		click(SiebelAddProdcutObj.Carnor.ExistingCapacityLeadTimePrimary,"ExistingCapacityLeadTimePrimary");
		sendKeys(SiebelAddProdcutObj.Carnor.ExistingCapacityLeadTimePrimary,"No");
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.Carnor.ExistingCapacityLeadTimePrimary, Keys.ENTER);	
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.Carnor.ExistingCapacityLeadTimePrimary, Keys.TAB);	
		Reusable.savePage();

		verifyExists(SiebelAddProdcutObj.Carnor.CarNorButton,"CarNorButton");
		click(SiebelAddProdcutObj.Carnor.CarNorButton,"CarNorButton");
		Reusable.SendkeaboardKeys(SiebelAddProdcutObj.Carnor.ExistingCapacityLeadTimePrimary, Keys.F5);	
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
//		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		Thread.sleep(60000);

		String ProductType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");

		waitForElementToAppear(SiebelAddProdcutObj.Carnor.carnorOrderReferencevalue,50);
		String Carnor = getAttributeFrom(SiebelAddProdcutObj.Carnor.carnorOrderReferencevalue, "value");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "NOR_ServiceOrderNumber", Carnor);	
		System.out.println(Carnor);
		Report.LogInfo("VerifyExixts","Carnor order No:" + Carnor,"");
		
//		Reusable.waitForSiebelSpinnerToDisappear();
		
		verifyExists(SiebelModeObj.IPAccessObj.ReasonNotIQNet,"ReasonNotIQNet");
		click(SiebelModeObj.IPAccessObj.ReasonNotIQNet,"ReasonNotIQNet");
		clearTextBox(SiebelModeObj.IPAccessObj.ReasonNotIQNet);
		sendKeys(SiebelModeObj.IPAccessObj.ReasonNotIQNet,"IQNET Order","ReasonNotIQNet");
		keyPress(SiebelModeObj.IPAccessObj.ReasonNotIQNet,Keys.ENTER);
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		
		if (Layer3Resilience.contains("Dual")) 
		{
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelOrderObj.SiebelOrder.SecondarySite,"SecondarySite");
			click(SiebelOrderObj.SiebelOrder.SecondarySite,"SecondarySite");	
			
			Reusable.waitForSiebelLoader();
			
			waitForElementToAppear(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondary, 10);
			verifyExists(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondarydropdown,"OSSPlatformflagsecondarydropdown");
			click(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondarydropdown,"OSSPlatformflagsecondarydropdown");
			
			ClickonElementByString("//*[text()='IQNet']",10);
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondary, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}

		
		if (isElementPresent(By.xpath("//li[not(contains(@style,'display'))]//a[text()='Sites']")))
		{
			verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites Tab");
			click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
			Reusable.waitForSiebelLoader();
		}
	}

	public void ServiceChargeforIPVPNSite(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String Amount ) throws Exception 
	{
		waitForElementToAppear(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.ExpandAllButton,10);
		click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.ExpandAllButton,"Expand All Button");

		while (!getAttributeFrom(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingLastRow, "class").contains("highlight")) 
		{
			int RowCount = getXPathCount(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRow) - 1;
			System.out.println(RowCount);

			for (int i = 1; i <= RowCount; i++) 
			{
				click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRowAmount.replace("Value", String.valueOf(i)));
				Reusable.waitForSiebelLoader();
				if (!getAttributeFrom(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRowAmount.replace("Value", String.valueOf(i)),"class").contains("disabled"))
				{
					clearTextBox(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRowAmountInput);
					sendKeys(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRowAmountInput.replace("Value", String.valueOf(i)),Amount);
					Reusable.waitForSiebelLoader();
				}
				else 
				{
					Report.LogInfo("VerifyExixts","Not Required to fill", "");
				}

				click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRowBCN.replace("Value", String.valueOf(i)));
				Reusable.waitForSiebelLoader();
				if (!getAttributeFrom(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BillingRowBCN.replace("Value", String.valueOf(i)),"class").contains("disabled"))
				{
					click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BCNSearchClick);
					Reusable.waitForSiebelLoader();
					String BCNInstallationChargeNRC = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"BCN");
					sendKeys(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BCNSearchClick,BCNInstallationChargeNRC,"BCNInstallationChargeNRC");
					Reusable.waitForSiebelLoader();
					click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.BCNNRCSearch);
					Reusable.waitForSiebelLoader();	
				}
				else 
				{
					Report.LogInfo("VerifyExixts","Not Required to fill", "");
				}

				click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.FirstLineitem,"First Line item");
				click(SiebelAddProdcutObj.ServiceChargeforIPVPNSite.ClickNextPage,"Next Page");
				Reusable.waitForSiebelLoader();		
			}	

		}

	}

	public void ColtPromissDate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
	/*	waitForElementToAppear(SiebelAddProdcutObj.ColtPromissDate.ColtPromissday,10);
		verifyExists(SiebelAddProdcutObj.ColtPromissDate.ColtPromissday,"Promiss Date");
		sendKeys(SiebelAddProdcutObj.ColtPromissDate.ColtPromissday, Reusable.CurrentDate(),"Enter Promiss Date");
*/
		Reusable.waitForSiebelLoader();	
		verifyExists(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day");
		Reusable.waitForSiebelLoader();	
		
		
	}

	public void ModTechModCommWaveAndLine(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		waitToPageLoad();		
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		if (ProductName.toString().equalsIgnoreCase("Ethernet Line")&&ProductName.toString().equalsIgnoreCase("Wave"))
		{
			
			Reusable.ClickHereSave();
			// A End And B End Installation Entries Start
			String InstallTime95 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
			Reusable.AEndDropdownSelection("Aend_Install_Time",InstallTime95);
			String ColName123 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"InvlidCLITreatmet");
			Reusable.AEndInputEnter("Access Notification Period",ColName123);
			String ColName125 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"RoutingSequence");
			Reusable.AEndInputEnter("Access Time Window",ColName125);

			String BInstallTime112 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
			Reusable.BEndDropdownSelection("Bend_Install_Time",BInstallTime112);
			String ColName124 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"TotalNumberDDIs");
			Reusable.AEndInputEnter("Access Notification Period",ColName124);
			String ColName126 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"FirstCodec");
			Reusable.AEndInputEnter("Access Time Window",ColName126);

			String Comments = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
			if (ProductName.equalsIgnoreCase("Ethernet Line")&&Comments.equalsIgnoreCase("Tech"))
			{
				//Update bandwidth with new Values
				//Service BandWidth
				verifyExists(SiebelAddProdcutObj.ModTechModCommWaveAndLine.MiddleDropDown,"MiddleDropDown");
				click(SiebelAddProdcutObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "Service Bandwidth"));
				waitToPageLoad();		

				String ColName40 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
				click(SiebelAddProdcutObj.ModTechModCommWaveAndLine.MiddleLi.replace("Value", ColName40));
			}
			
			if (ProductName.equalsIgnoreCase("Wave")&&Comments.equalsIgnoreCase("Tech"))
			{
				//Update bandwidth with new Values
				//Service BandWidth
				verifyExists(SiebelAddProdcutObj.ModTechModCommWaveAndLine.MiddleDropDown,"MiddleDropDown");
				click(SiebelAddProdcutObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "A End Resilience Option"));
				waitToPageLoad();		

				String ColName76 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
				click(SiebelAddProdcutObj.ModTechModCommWaveAndLine.MiddleLi.replace("Value", ColName76));
			}		
		}
	}
	
	
//////////////////////
	public void CreateIpvpnSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception, Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");

//        String username = null;
//        String password = null;
    	
	
		Reusable.waitForSiebelLoader();
//		Reusable.savePage();
		
		
		if (ProductName.toString().equals("IP VPN Service"))// **Start** Added By Abhay dated 28-Sep-2019
		{
			String ServOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
			
			Reusable.savePage();
		
			verifyExists(SiebelAddProdcutObj.CompletedValidation.ClickLink.replace("Value", "Customer Orders"));
			javaScriptclick(SiebelAddProdcutObj.CompletedValidation.ClickLink.replace("Value", "Customer Orders"));
			Reusable.waitForSiebelLoader();
						
			
			// String x= ServiceOrder.get();
			// System.out.println(x);
			// String string = "004-034556";
			String[] parts = ServOrder.split("/");
			String part1 = parts[0];
			String part2 = parts[1];
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelAddProdcutObj.CompletedValidation.CustOrder, part2);
			click(SiebelAddProdcutObj.CompletedValidation.CustOrderGo,"CustOrderGo");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.CompletedValidation.ClickSeibelOrder,"Click Seibel Order");
			Reusable.waitForSiebelLoader();
			
			click(SiebelAddProdcutObj.CompletedValidation.NewServiceOrder,"New Service Order");
			Reusable.waitForSiebelLoader();
			
			Selectproduct("IP VPN Site");
		}
	}

	public void CompletedValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
		String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
		String ColName74 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Offnet");
		String username = Configuration.CEOS_Username;
		String password = Configuration.CEOS_Password;

		Reusable.alertPopUp();
		Validation.clickOnManualValidationA();
		
		if (ProductName.toString().equals("IP VPN Service"))// **Start** Added By Abhay dated 28-Sep-2019
		{
			//Comment below line for onnet
//		if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
//		{		
//			Reusable.SelectDropDownValue("Reason Not IQNET","IQNet Order");
//			Reusable.waitForSiebelSpinnerToDisappear();	
//			Reusable.waitForSiebelLoader();
//			Reusable.savePage();
//		}	
		Reusable.waitForSiebelLoader();
		if(ProductName.contains("IP VPN Service")&& ColName74.contains("Offnet"))
		{			
			CEOS.CEOS_Offnet(testDataFile, sheetName, scriptNo, dataSetNo);
			Login.SiebelLogout();
			LaunchingCEOSApplication(testDataFile, sheetName, scriptNo, dataSetNo,username,password);
			openBrowser("chrome");
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"New");
		}
		Reusable.waitForSiebelLoader();
		click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
		click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
		Reusable.waitForSiebelLoader();
		
		if (ServiceType.equalsIgnoreCase("IP VPN Wholesale")) // Added by Abhay
		{
				Reusable.ClickContinue();
		}
		
		click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Click Order Complete");
		Reusable.waitForSiebelLoader();
		
		Reusable.alertPopUp();
		Reusable.waitForSiebelLoader();
			

		String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus.contains("Progress")) 
		{
			Reusable.refreshPage();
			Reusable.savePage();
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}
			////////////////////////////////////////////Added new ///////////////////////
		Reusable.waitForSiebelLoader();
		String Orderstatus1 = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus1.contains("Delivery")) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}	
	
		Reusable.waitForSiebelLoader();
		String Orderstatus2 = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus2.contains("Progress")) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}

//////////////////////////////
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.alertPopUp();
			
			if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) 
			{
				System.out.println("");
				System.out.println("Alert Present");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
				click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
			}
			
			if(isElementPresent(By.xpath("//button[text()='Ok']"))) 
			{
				System.out.println("");
				System.out.println("Alert Present");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert2 Accept"); // 
			}
			Reusable.ScrollIntoViewByString(SiebelAddProdcutObj.getReferenceNo.Sites);
			verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites Tab");
			click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
			
			Reusable.alertPopUp();

			Reusable.waitForSiebelLoader();
			Reusable.savePage();

			MovetoIPService(testDataFile, sheetName,  scriptNo,  dataSetNo);

		
	} // **End of IPVPN server condition**

		
//		Reusable.savePage();
//		Reusable.waitForSiebelLoader();

		if (ProductName.toString().equals("Ethernet VPN Access") || (ProductName.toString().equals("Ethernet Line")|| (ProductName.toString().equals("IP Access"))))
		{
			if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
			{		
				Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
//				Reusable.waitForSiebelSpinnerToDisappear();	
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
			}	
		}
		
	
		verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
		click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");

		verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
		click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
		
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();

		verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"OrderComplete");
		click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Click Order Complete");
		Reusable.waitForSiebelLoader();
		Reusable.waitToPageLoad();
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
		}
		if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) {
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
			click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
		}
 		Reusable.waitForSiebelLoader();
		String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus.contains("Progress")) {
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) {
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}
		////////////////////////////////////////////Added new ///////////////////////
Reusable.waitForSiebelLoader();
String Orderstatus1 = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
if(Orderstatus1.contains("Delivery")) {
verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
Reusable.waitForSiebelLoader();
Reusable.waitForAjax();
if(isElementPresent(By.xpath("//button[text()='Yes']"))) {
verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
Reusable.waitForSiebelLoader();
Reusable.waitForAjax();
}

}	

Reusable.waitForSiebelLoader();
String Orderstatus2 = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
if(Orderstatus2.contains("Progress")) {
verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
Reusable.waitForSiebelLoader();
Reusable.waitForAjax();
if(isElementPresent(By.xpath("//button[text()='Yes']"))) {
verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
Reusable.waitForSiebelLoader();
Reusable.waitForAjax();
}
}
	
//////////////////////////////////////
		System.out.println("Order complete");
		String CompValidation= null;
		CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		
		if(CompValidation.equals("Completed")) 
		{
			Report.LogInfo("verify","<i></b> order status completed", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> order status not competed", "Fail");
		}
		Reusable.waitForSiebelLoader();
	}
	
		public void IPVPNSITEMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
		{

			String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
			String SiteType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site Type");
			String RouterType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Type");
			String Layer3Resilience = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
			String SiteBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site/SpokeService_Bandwidth");
			String OssPlatformFlag = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
			String Netflow_Enabled = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Netflow Enabled");
			String SNMP_Enabled = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SNMP Feature");
			String StreetName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Street_Name");
			String Country = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Country");
			String CityTown = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_City/Town");
			String PostalCode = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
			String Premises = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Premises");
			String ServicePartyName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Party_Name");
			String AccessType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Access_Type/ Primary");
			String AccessTechnology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Access_Technology/ Primary");
			String ThirdpartyAccessProvider = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Third_party_access_provider");
			String BuildingType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Building_Type");
			String CustomerSitePopStatus = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Customer_Site_Pop_Status");
			String CabinetType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Cabinet_Type");
			String Cabinet_ID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Cabinet_ID");
			String PresentationInterface = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Presentation_Interface");
			String ConnectorType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Connector_Type");
			String BuildingId = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Building_ID");
			String FibreType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Fibre_Type");
			String InstallTime = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Install_Time");
			String SiteName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Name");
			String RouterModel = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Model");
			String VpnBandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DummyHub_Bandwidth");
			String Offnet = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Offnet");

			Reusable.waitForSiebelLoader();
			if (ServiceType.equals("IP VPN Plus") || ServiceType.equals("IP VPN Access")) 
			{
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Site Type"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Site Type"));
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SiteType),SiteType);
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SiteType),SiteType);
			
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Layer 3 Resilience"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Layer 3 Resilience"));
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Layer3Resilience),Layer3Resilience);
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Layer3Resilience),Layer3Resilience);

				Reusable.waitForSiebelLoader();
				Reusable.ClickHereSave();
//				Reusable.waitForSiebelSpinnerToDisappear();

			}

			if (ServiceType.equals("IP VPN Access")) {
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Type"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Type"));
//				Reusable.waitForSiebelSpinnerToDisappear();
				//verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", ColName31),"Select Value Dropdown");
				javaScriptclick(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", RouterType));
				Reusable.waitForSiebelLoader();

			}

			if (ServiceType.equals("SWIFTNet")) {
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Layer 3 Resilience"),"Layer 3 Resilience");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Layer 3 Resilience"),"Layer 3 Resilience");
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SiteType),SiteType);
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SiteType),SiteType);
//				Reusable.waitForSiebelSpinnerToDisappear();
			

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Type"),"Router Type DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Type"),"Router Type DropDown");
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", RouterType),RouterType);
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", RouterType),RouterType);
//				Reusable.waitForSiebelSpinnerToDisappear();
			}

			if (ServiceType.equals("PrizmNet")) {
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Site Type"),"Site Type DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Site Type"),"Site Type DropDown");
				Reusable.waitForSiebelLoader();
				//verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",ColName30),ColName30);
				javaScriptclick(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",SiteType),SiteType);
			
//				Reusable.waitForSiebelSpinnerToDisappear();
			
				Reusable.savePage();
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value","Router Type"),"Router Type DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Type"),"Router Type DropDown");
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", RouterType),RouterType);
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", RouterType),RouterType);
//				Reusable.waitForSiebelSpinnerToDisappear();
			}
//			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Service Bandwidth (Primary)"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Service Bandwidth (Primary)"),"Service Bandwidth (Primary)");
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SiteBandwidth));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Capacity Check Reference"),"TextInput");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Capacity Check Reference"),"12");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Capacity Check Reference"),Keys.ENTER);
            Reusable.waitForAjax();
            Reusable.waitForAjax();

//			Reusable.waitForSiebelSpinnerToDisappear();
            Reusable.savePage();
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Hard Modify Flag"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Hard Modify Flag"),"Hard Modify Flag");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", "N"),"SelectValueDropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", "N"),"Select 'N' value");
			
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "OSS Platform Flag (Primary)"), OssPlatformFlag,"OSS Platform Flag (Primary)");
			Reusable.waitForSiebelLoader();

			if (Netflow_Enabled.contains("Yes"))
			{
				if(isElementPresent(By.xpath("//span[text()='Netflow enabled']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.netflowEnabledCheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.netflowEnabledCheckbox);
						Reusable.waitForSiebelLoader();
					}
				}
			}
			
			if (SNMP_Enabled.contains("Yes"))
			{
				if(isElementPresent(By.xpath("//span[text()='SNMP enabled']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.snmpEnabledcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.snmpEnabledcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}
			}
			Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();
		
//			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
			click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
			click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
			click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
			click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			//Reusable.waitForSiebelSpinnerToDisappear();
/*
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectSiteSearchAccess,"SelectSiteSearchAccess");
			mouseMoveOn(SiebelModeObj.IPVPNSITEMiddleApplet.SelectSiteSearchAccess);
			javaScriptclick(SiebelModeObj.IPVPNSITEMiddleApplet.SelectSiteSearchAccess,"Click on Search Site");

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.StreetNameAccess,"Street Name Access");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.StreetNameAccess,ColName61, "Enter Street Name");

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.CountryAccess,"Country");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.CountryAccess,ColName62, "Enter Country");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.CityTownAccess,"CityTownAccess");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.CityTownAccess,ColName63, "Enter City");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PostalCodeAccess,"Country");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.PostalCodeAccess,ColName64, "Enter  Postal Code");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PremisesAccess,"PremisesAccess");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.PremisesAccess,ColName65, "Enter Premises");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SearchButtonAccess,"SearchButtonAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SearchButtonAccess,"Click on Search");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectPickAddressAccess,"Select Address for Sites");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectPickAddressAccess,"Select Address for Site");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PickAddressButtonAccess,"SearchButtonAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.PickAddressButtonAccess,"Click on Pick Addrss");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectPickBuildingAccess,"SelectPickBuildingAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectPickBuildingAccess,"Select Buiding for Site");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PickBuildingButtonAccess,"Pick Building Button Access");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.PickBuildingButtonAccess,"Submit Buiding for Site");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectPickSiteAccess,"Select Site");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectPickSiteAccess,"Select Site");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PickSiteButtonAccess,"PickSiteButtonAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.PickSiteButtonAccess,"PickSiteButtonAccess");
			*/
			
			siebelmod.addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ServicePartySearchAccess,"Search Service Party");
			mouseMoveOn(SiebelModeObj.IPVPNSITEMiddleApplet.ServicePartySearchAccess);
			javaScriptclick(SiebelModeObj.IPVPNSITEMiddleApplet.ServicePartySearchAccess,"Search Service Party");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ServicePartyDropdownAccess,"Service Party Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ServicePartyDropdownAccess,"Click on Service Party Dropdown");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PartyNameAccess,"Select Party Name");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.PartyNameAccess,"Click on Select Party Name");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.InputPartyNameAccess,"InputPartyNameAccess");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.InputPartyNameAccess,ServicePartyName, "Enter Party Name");
		
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PartyNameSearchAccess,"PartyNameSearchAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.PartyNameSearchAccess,"Click On Search");
			
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.PartyNameSubmitAccess,"PartyNameSubmitAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.PartyNameSubmitAccess,"Click On Submit");
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SiteContactSearchAccess,"SiteContactSearchAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SiteContactSearchAccess,"Search Site Contact");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SiteContactDropdownAccess,"SiteContactDropdownAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SiteContactDropdownAccess,"Click on Site Name Dropdown");
			Reusable.waitForSiebelLoader();

//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.InputSiteNameAccess,"InputSiteNameAccess");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.InputSiteNameAccess,BuildingId, "Enter Site Name");
//			Reusable.waitForSiebelLoader();
//
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.LastNameSiteSearchAccess,"LastNameSiteSearchAccess");
//			click(SiebelModeObj.IPVPNSITEMiddleApplet.LastNameSiteSearchAccess,"Click On Search");
//			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.LastNameSiteSubmitAccess,"LastNameSiteSubmitAccess");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.LastNameSiteSubmitAccess,"Click On Submit");

			Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();


			if(Offnet.equalsIgnoreCase("Offnet"))
			{
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.AccessTypeDropdownAccess,"AccessTypeDropdownAccess");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.AccessTypeDropdownAccess,"Access Type Dropdown Access");

				click(SiebelModeObj.IPVPNSITEMiddleApplet.AccesstypeOffnet.replace("AccessTypeValue", "3rd Party Leased Line"));
				Reusable.waitForSiebelLoader();
				
				//Click on save button to populate extra fields//
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.IpGurdianSave,"Save Button");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.IpGurdianSave,"Click on Save button");
				Reusable.waitForSiebelLoader();

				 //3rd party access provider//
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ThirdPartyAccessProvDropDown,"Third party access provider DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ThirdPartyAccessProvDropDown.replace("Value", "Third Party Access Provider"));
				
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ThirdpartyAccessOffnet.replace("AccessProviderValue", ThirdpartyAccessProvider));
				Reusable.waitForSiebelLoader();

				//Third party connection reference//

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.Thirdpartyconectionreference,"Thirdpartyconectionreference");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.Thirdpartyconectionreference,"Click Thirdpartyconectionreference");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.Thirdpartyconectionreference,"no colt reference","3rd Party connection reference");
				Reusable.waitForSiebelLoader();

				//BCP reference//
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.Bcpreference,"BCP Reference");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.Bcpreference,"Click BCP Reference");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.Thirdpartyconectionreference,"Colt Automation","BCP Reference");
				Reusable.waitForSiebelLoader();
				
				//Site name Alias//

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SiteNameAlias,"Site Name Alias");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.Bcpreference,"Click BCP Reference");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.SiteNameAlias,"HPGT1823","Site Name Alias");
				Reusable.waitForSiebelLoader();
				
				//Click on save button//
				
				Reusable.ClickHereSave();
				Reusable.waitForSiebelLoader();
				Reusable.waitForSiebelLoader();

//				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.IpGurdianSave,"Save Button");
//				click(SiebelModeObj.IPVPNSITEMiddleApplet.IpGurdianSave,"Click on Save button");
//				Reusable.waitForSiebelLoader();
			}

			else
			{
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Access Type"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Access Type"));
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", AccessType),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", AccessType),"Access Type");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Access Technology"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Access Technology"));
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", AccessTechnology),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", AccessTechnology));
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Building Type"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Building Type"));
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", BuildingType),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", BuildingType));
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Customer Site Pop Status"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Customer Site Pop Status"));
//				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", CustomerSitePopStatus),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", CustomerSitePopStatus));
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "3rd Party Connection Reference"),"TextInput");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "3rd Party Connection Reference"),"As12");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "3rd Party Connection Reference"),"TextInput");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "BCP Reference"),"As12");

				Reusable.waitForSiebelLoader();
			}
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Cabinet Type"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Cabinet Type"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", CabinetType),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", CabinetType));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Cabinet ID"),"TextInput");
			clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Cabinet ID"));
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Cabinet ID"),"43");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Cabinet ID"),Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Shelf ID"),"TextInput");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Shelf ID"),"12");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Shelf ID"),Keys.ENTER);

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Slot ID"),"TextInput");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Slot ID"),"12");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Slot ID"),Keys.ENTER);
		
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Presentation Interface"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Presentation Interface"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", PresentationInterface),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", PresentationInterface));
			Reusable.waitForSiebelLoader();
			

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Connector Type"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Connector Type"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", ConnectorType),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", ConnectorType));
			Reusable.waitForSiebelLoader();
			

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Fibre Type"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Fibre Type"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", FibreType),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", FibreType));
			Reusable.waitForSiebelLoader();
			
			// Added new 		
			if (ServiceType.equals("PrizmNet"))
			{
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Encryption Capable"),"Encryption Capable");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Encryption Capable"));

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value","N"),"Select Encryption Capable Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value","N"));
				Reusable.waitForSiebelLoader();

			}
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Install Time"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Install Time"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", InstallTime),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", InstallTime));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Model"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Router Model"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",RouterModel),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",RouterModel));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Site Name"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Site Name"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",SiteName),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",SiteName));
			Reusable.waitForSiebelLoader();

			Reusable.ClickHereSave();
//			Reusable.waitForSiebelSpinnerToDisappear();
//			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			//Operational Attribute
			
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickLink.replace("Value", "IP Details"),"IP Details");
			Reusable.waitForSiebelLoader();
//			
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "CPE IPv4 Address"),"CPE IPv4 Address");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "CPE IPv4 Address"),"123.65.19.1");
//			
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "IPv4 Prefix"),"IPv4 Prefix");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "IPv4 Prefix"),"123.65.19.1");
//			
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "PE IPv4 Address"),"PE IPv4 Address");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "PE IPv4 Address"),"123.65.19.1");
//			Reusable.waitForSiebelLoader();
//
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "PE Name"),"PE Nanme");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "PE Name"),"xyz");
//			Reusable.waitForSiebelLoader();
//
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "CPE IPv6 Address"),"CPE Ipv6 Address");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "CPE IPv6 Address"),"2001:0920:3022:0000:0000:0000:0000:0000");
//			Reusable.waitForSiebelLoader();
//
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "IPv6 Prefix"),"IPv6 Prefix");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "IPv6 Prefix"),"56");
//			Reusable.waitForSiebelLoader();
//
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "PE IPv6 Address"),"PE IPv6 Address");
//			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "PE IPv6 Address"),"2001:0920:3022:0000:0000:0000:0000:0000");
//			Reusable.waitForSiebelLoader();
//
//			Reusable.ClickHereSave();
//			Reusable.waitForSiebelSpinnerToDisappear();
			
          /*
            //---
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Address"),"TextInput");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Address"),"123.65.19.1");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Prefix"),"TextInput");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Prefix"),"123.65.19.1");
			Reusable.waitForSiebelLoader();
			//--
			 */
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickLink.replace("Value", "End Point VPN"),"ClickLink");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickLink.replace("Value", "End Point VPN"),"ClickLink");
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.IPDetailsPlus,"IPDetailsPlus");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.IPDetailsPlus,"IPDetailsPlus");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "VPN Bandwidth"),"DropDown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "VPN Bandwidth"));
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",VpnBandwidth),"Select Value Dropdown");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value",VpnBandwidth));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Sub VPN ID"),"Sub VPN ID");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Sub VPN ID"),"VPN1");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Sub VPN ID"), Keys.TAB);
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			/*
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Sub VPN ID"),"Sub VPN ID");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Sub VPN ID"),"LAN1");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SearchInput.replace("Value", "Physical Port ID Primary"),"SearchInput");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SearchInput.replace("Value", "Physical Port ID Primary"));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.AccessPortList,"AccessPortList");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.AccessPortList, "AccessPortList");
			
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SubmitSubVPNList,"SubmitSubVPNList");
			click(SiebelModeObj.IPVPNSITEMiddleApplet.SubmitSubVPNList, "SubmitSubVPNList");
			Reusable.waitForSiebelLoader();
          */
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SubVPNID,"SubVPNID");
//			click(SiebelModeObj.IPVPNSITEMiddleApplet.SubVPNID,"SubVPNID");
//			
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectSubVPNList,"SelectSubVPNList");
//			click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectSubVPNList,"SelectSubVPNList");
//			
//			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SubmitSubVPNList,"SubmitSubVPNList");
//			click(SiebelModeObj.IPVPNSITEMiddleApplet.SubmitSubVPNList,"SubmitSubVPNList");
	
			Reusable.waitForSiebelLoader();

			if (ServiceType.equals("IP VPN Plus") || ServiceType.equals("PrizmNet")||ServiceType.equals("IP VPN Access")) {
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SearchInput.replace("Value", "Physical Port ID Primary"),"Physical Port ID Input");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SearchInput.replace("Value", "Physical Port ID Primary"),"Physical Port ID Input");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.AccessPortList,"AccessPortList");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.AccessPortList, "AccessPortList");

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SubmitSubVPNList, "SubmitSubVPNList");
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickLink.replace("Value", "IP Details"),"ClickLink");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickLink.replace("Value", "IP Details"),"IP Details");
				Reusable.waitForSiebelLoader();
				
				/*
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Address"),"TextInput");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Address"),"123.65.19.1");
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Prefix"),"TextInput");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "LAN Interface IPv4 Prefix"),"123.65.19.1");
				Reusable.waitForSiebelSpinnerToDisappear();
				*/
			}

			Reusable.savePage();
			//Reusable.ClickHereSave();
			Reusable.waitForpageloadmask();
		}
	
	
		public void MovetoIPService(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
		{
			String ServiceOrderReference_Number = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
			Reusable.ScrollIntoViewByString(SiebelAddProdcutObj.MovetoIPService.ClickLink.replace("Value", "Network"));
			verifyExists(SiebelAddProdcutObj.MovetoIPService.ClickLink.replace("Value", "Network"));
			javaScriptclick(SiebelAddProdcutObj.MovetoIPService.ClickLink.replace("Value", "Network"));

			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelAddProdcutObj.MovetoIPService.NetworkService,"NetworkService");
			click(SiebelAddProdcutObj.MovetoIPService.NetworkService,"Click on Network Service");

		
			click(SiebelAddProdcutObj.MovetoIPService.GOIPService.replace("Value", ServiceOrderReference_Number));
			Reusable.waitForSiebelLoader();
			
		}
		
	
		public void OperationalAttributesforIPVPN(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
		{
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Sites"),"ClickLink");
			click(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Sites"));
			Reusable.waitForSiebelLoader();		
			Reusable.waitForAjax();
			
			ScrollIntoViewByString(SiebelAddProdcutObj.CompletedValidation_offnet.RouterSpecificationSetting);
			verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.RouterSpecificationSetting,"RouterSpecificationSetting");
			click(SiebelAddProdcutObj.CompletedValidation_offnet.RouterSpecificationSetting,"RouterSpecificationSetting");
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();

			int RowCount = getXPathCount(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueCount);
			for (int i = 0; i < RowCount; i++) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"OperationalAttribueClick");
				click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"OperationalAttribueClick");

				verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText,"OperationalAttributeText");
				click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText,"OperationalAttributeText");
				sendKeys(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText, "Test1");
				if(i<RowCount-1)
				scrollDown(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 2)));
			}
			ScrollIntoViewByString(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK);
			click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK,"OperationalAttributeOK");
			Reusable.savePage();	
		}
		
		public void SecondOperationalAttributesIPVPN(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
		{
//			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Sites"),"Click Link");
			click(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Sites"));
			Reusable.waitForSiebelLoader();		
			Reusable.waitForAjax();
			
			ScrollIntoViewByString(SiebelAddProdcutObj.CompletedValidation_offnet.SecondRouterSpecificationSetting);
			verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.SecondRouterSpecificationSetting,"Router Specification Setting");
			click(SiebelAddProdcutObj.CompletedValidation_offnet.SecondRouterSpecificationSetting,"Router Specification Setting");
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();

			int RowCount = getXPathCount(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueCount);
			for (int i = 0; i < RowCount; i++) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"Operational Attribue Click");
				click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"Operational Attribue Click");

				verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText,"Operational Attribute Text");
				click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText,"Operational Attribute Text");
				sendKeys(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText, "Test1");
				if(i<RowCount-1)
				scrollDown(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 2)));

			}
			ScrollIntoViewByString(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK);
			click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK,"Operational Attribute OK");
			Reusable.savePage();	
		}
		
		public void WriteServiceOrderNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {

			waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
	//		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
			String seriveOrderNum = getTextFrom(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo);
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "seriveOrderNum", seriveOrderNum);	
		
			
		/*	String path = "src//test//resources//ServiceOrder.xlsx";
			File file = new File(path);
			if (!file.exists()) {
				try {
					file.createNewFile();
					createFile(path);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			AppendData(path, Inputdata);
		}
		
		*/
     }
		public void CompletedValidation_offnet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
			
			String ProductType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			String username = Configuration.CEOS_Username;
			String password = Configuration.CEOS_Password;
			String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
			
		waitToPageLoad();
		Reusable.savePage();
		waitToPageLoad();
		if (ProductType.equals("IP VPN Service"))
		{
		Reusable.savePage();
		waitToPageLoad();
//		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink,"MiddleDropDown");
//		click(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Customer Orders"));

		String ServiceOrder = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		String[] parts = ServiceOrder.split("/");
		String part1 = parts[0];
		String part2 = parts[1];
		waitToPageLoad();

		sendKeys(SiebelAddProdcutObj.CompletedValidation_offnet.CustOrder, part2,"Cust Order");
		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.CustOrderGo,"CustOrderGo");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.CustOrderGo,"CustOrderGo");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.ClickSeibelOrder,"ClickSeibelOrder");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.ClickSeibelOrder,"ClickSeibelOrder");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.NewServiceOrder,"New Service Order");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.NewServiceOrder,"New Service Order");
		Reusable.waitForSiebelLoader();

		Selectproduct("IP VPN Site");
	//	openIPVPNSite();
		openIPVPNSite(testDataFile, sheetName, scriptNo,dataSetNo);
		enterMandatoryFieldsInHeader(testDataFile, sheetName, scriptNo, dataSetNo);
		NetworkReferenceFill(testDataFile, sheetName, scriptNo, dataSetNo);
		IPVPNSITEMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
		siebelmod.EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		siebelmod.EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);

		if (!ServiceType.equals("IP VPN Wholesale"))
		{
			ServiceChargeforIPVPNSite(testDataFile, sheetName, scriptNo, dataSetNo, "2");
		}
		if (!ServiceType.equals("IP VPN Wholesale"))
		{
			OperationalAttributesforIPVPN(testDataFile, sheetName, scriptNo, dataSetNo);
		}

		EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Reusable.alertPopUp();
		ManualValidation.clickOnManualValidationA();

		if (ProductType.contains("IP VPN Service"))
		{
			CEOS.CEOS_Offnet(testDataFile, sheetName, scriptNo, dataSetNo);
			Login.SiebelLogout();
			LaunchingCEOSApplication(testDataFile, sheetName, scriptNo, dataSetNo,username,password);
			openBrowser("chrome");
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		waitToPageLoad();

		if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
		{		
			Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
//			Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}	
		waitToPageLoad();
		
		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OrderStatusDropdown,"Order Status Dropdown");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.OrderStatusDropdown,"Order Status Dropdown");
		Reusable.waitForSiebelLoader();
		click(SiebelAddProdcutObj.CompletedValidation_offnet.SelectCompleted,"Select Completed");
		waitToPageLoad();

		if (ServiceType.equalsIgnoreCase("IP VPN Wholesale"))
		{
		Reusable.ClickContinue();

		}

		click(SiebelAddProdcutObj.CompletedValidation_offnet.OrderComplete,"Order Complete");
		Reusable.alertPopUp();
		waitToPageLoad();

		Reusable.savePage();
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		Reusable.savePage();

		if(isElementPresent(By.xpath("//span[text()='Ok']")))
		{
		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.AlertAccept,"Alert Accept");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.AlertAccept,"Alert Accept");

		}
		Reusable.savePage();
		waitToPageLoad();
		MovetoIPService(testDataFile, sheetName,  scriptNo,  dataSetNo);
		}

		Reusable.savePage();
		waitToPageLoad();

		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OrderStatusDropdown,"Order Status Dropdown");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.OrderStatusDropdown,"Order Status Dropdown");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.SelectCompleted,"Select Completed");
		waitToPageLoad();

		click(SiebelAddProdcutObj.CompletedValidation_offnet.OrderComplete,"Order Complete");
		waitToPageLoad();
		Reusable.savePage();
		waitToPageLoad();
		Reusable.savePage();
		waitToPageLoad();

		if(isElementPresent(By.xpath("//span[text()='Ok']")))
		{
		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.AlertAccept,"Alert Accept");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.AlertAccept,"Alert Accept");

		}

		if (isElementPresent(By.xpath("//button[@class='colt-primary-btn']")))
		{
		verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.SubnetworkPopUP,"Subnetwork PopUP");
		click(SiebelAddProdcutObj.CompletedValidation_offnet.SubnetworkPopUP,"Subnetwork PopUP");

		}

		CompValidation.set(getAttributeFrom(SiebelAddProdcutObj.CompletedValidation_offnet.OrderStatusInput,"value"));

		}
		
		public void openAsset(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
			Reusable.waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.GoToAssetBtn,"GoToAsset Button");
			click(SiebelAddProdcutObj.validateSlaMatrix.GoToAssetBtn,"GoToAsset Button");
			

			if(isElementPresent(By.xpath(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept)))
			{			
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				click(SiebelAddProdcutObj.validateSlaMatrix.AlertAccept,"Alert Accept");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Asset Button");
				click(SiebelAddProdcutObj.validateSlaMatrix.AssetButton,"Asset Button");
			}
			Reusable.waitForSiebelLoader();

		/*	String Order = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
			String[] Order1=Order.split("/");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,"ServiceOrderOM");
			click(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,"ServiceOrderOM");
			sendKeys(SiebelAddProdcutObj.validateSlaMatrix.ServiceOrderOM,Order1[0]);
			
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.GoButton,"GoButton");
			click(SiebelAddProdcutObj.validateSlaMatrix.GoButton,"GoButton");
		 
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelAddProdcutObj.validateSlaMatrix.AssetNumber,"AssetNumber");
			click(SiebelAddProdcutObj.validateSlaMatrix.AssetNumber,"AssetNumber");
			*/
		}

		public void InputAndOpenServiceOrderNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String TypeofOrder ) throws Exception
		{
		// String SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"seriveOrderNum");
		
			String Product_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			
			String SiteServiceOrderNumber = null;
			if (TypeofOrder.toString().contains("Carnor"))
			{
				SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"NOR_ServiceOrderNumber");
			}
			else if (TypeofOrder.toString().contains("Cease"))
			{
				SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CEASE_ServiceOrderNumber");
			}
			else if(TypeofOrder.toString().equalsIgnoreCase("Modify"))
			{
				SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Modify_ServiceOrderNumber");
			}
			else
			{
				if (Product_Type.equals("IP VPN Service")) 
				{
					SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site/SpokeOrderReference_Number");
				}
				else
				{
					SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
				}
			}
			
			if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
				//	System.out.println("Alert Present");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
			}
			Reusable.waitForSiebelLoader();

			Reusable.waitForSiebelLoader();
			try 
			{
				verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
				click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} 
			catch (Exception e) 
			{
				try 
				{
				javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
			}
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
			click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
			sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,SiteServiceOrderNumber,"Service OrderReference Number");
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
			click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
			Reusable.waitForSiebelLoader();
	
			 Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
			verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
			// String seriveOrderRefNum = getTextFrom(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo);
			// DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderReference_Number", seriveOrderRefNum);
			click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
			Reusable.waitForSiebelLoader();
		}
		
		public void InputAndOpenCarnorOrderNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception
		{
		// String SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"seriveOrderNum");
		
			String Product_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			
			String SiteServiceOrderNumber = null;
//			if (Product_Type.equals("IP VPN Service")) 
//			{
				SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"NOR_ServiceOrderNumber");
//			}
//			else
//			{
//				SiteServiceOrderNumber = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
//			}
			
			if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
				//	System.out.println("Alert Present");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
			}
			Reusable.waitForSiebelLoader();

			Reusable.waitForSiebelLoader();
			try 
			{
				verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
				click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} 
			catch (Exception e) 
			{
				try 
				{
				javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
			}
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
			click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
			sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,SiteServiceOrderNumber,"Service OrderReference Number");
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
			click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
			Reusable.waitForSiebelLoader();
	
			 Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
			verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
			// String seriveOrderRefNum = getTextFrom(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo);
			// DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "ServiceOrderReference_Number", seriveOrderRefNum);
			click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
			Reusable.waitForSiebelLoader();
		}

		public void getReferenceNoXNG(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception, Exception {
			String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");

			if (ProductName.toString().equalsIgnoreCase("Wave")
			|| ProductName.toString().equalsIgnoreCase("Ethernet Line")|| ProductName.toString().equalsIgnoreCase("Ethernet VPN Access")
			|| ProductName.toString().equalsIgnoreCase("Private Ethernet")
			|| ProductName.toString().equalsIgnoreCase("DCA Ethernet")|| ProductName.toString().equalsIgnoreCase("IP Access"))
			{

			Reusable.waitForSiebelLoader();


			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Click on sites");
			click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
			Reusable.waitForSiebelLoader();

			GetReferenceXNG(testDataFile,sheetName,scriptNo, dataSetNo);
			// Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();

			}
			}
		
		public void GetReferenceXNG(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException, IOException {
			String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			
			if (!ProductName.toString().equals("IP VPN Service"))// 
			{
			verifyExists(SiebelAddProdcutObj.GetReference.GetReference,"Get Reference");
			click(SiebelAddProdcutObj.GetReference.GetReference,"Get Reference");
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			String Circuitreferencenumber = getAttributeFrom(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CircuitReference_NumberXNG", Circuitreferencenumber);
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, Keys.TAB);

//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			}
			}
		public void GetReferenceXNG1(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException, IOException {
			String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			
			if (ProductName.toString().equals("IP VPN Service"))// 
			{
			verifyExists(SiebelAddProdcutObj.GetReference.GetReference,"Get Reference");
			click(SiebelAddProdcutObj.GetReference.GetReference,"Get Reference");
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			String Circuitreferencenumber = getAttributeFrom(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CircuitReference_NumberXNG", Circuitreferencenumber);
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, Keys.TAB);

//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			}
			}
		
		public void getReferenceNoforIPVPNSite(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception, Exception 
		{
			String ServiceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
		
			if(ServiceType.toString().contains("IP VPN Wholesale") || ServiceType.toString().contains("IP VPN") || ServiceType.toString().contains("IP VPN Access") ||
					ServiceType.toString().contains("IP VPN Plus")|| ServiceType.toString().contains("SWIFTNet") || ServiceType.toString().contains("PrizmNet"))
			{
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Expected Delivery Date");
				click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
				Reusable.waitForSiebelLoader();
							
				GetReference(testDataFile,sheetName,scriptNo, dataSetNo);
			//	Reusable.waitForSiebelLoader();
				Reusable.savePage();
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
			}
		}
		
		public void addEvpnNetworkFeatures(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception, Exception 
		{
			String Cos = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Class of Service");
			
			if (Cos.contains("Yes"))
			{
				javaScriptclick(SiebelOrderObj.SiebelOrder.ShowFullInfo); 
				Reusable.waitForAjax();

				Reusable.waitForSiebelLoader();

				click(SiebelAddProdcutObj.EvpnNetworkFeature.Cos,"Class of Service Network");
				Reusable.waitForAjax();

				javaScriptclick(SiebelAddProdcutObj.EvpnNetworkFeature.CloseEvpnNetwork);
																				
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

				if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
						click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					}
					Reusable.waitForSiebelLoader();
				}
			}
		}

		public void updateCarnorOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
		{
			String Scenario = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Scenario");
			String Install_Time = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
			String Router_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Type");
			String Router_Technology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Technology");
			
			switch (Scenario) 
			{

			case "IP Access resilience - no change": 
			{     
				Reusable.waitForAjax();
				String resillenceValue = getAttributeFrom(SiebelOrderObj.SiebelOrder.Inputlayer3resillence,"value");
				String relatedService = getAttributeFrom(SiebelOrderObj.SiebelOrder.SiebRelatedVoipService,"value");
				
				click(SiebelModeObj.installationTimeUpdate.ClickDropdown.replace("Value","Install Time"));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Installation Time");
				click(SiebelModeObj.installationTimeUpdate.SelectValueDropdown.replace("Value",Install_Time));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Fill Install Time");
				Reusable.waitForpageloadmask();

				if (relatedService.contains("Dedicated") || relatedService.contains("Converged"))
				{
					click(SiebelOrderObj.SiebelOrder.ServiceGrpTab,"Service Group Tab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Service Group");
					Reusable.waitForAjax();
					click(SiebelOrderObj.SiebelOrder.ServiceGrpNew,"Service Group New");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New Service Group");
					click(SiebelOrderObj.SiebelOrder.ServGrpRef,"Service Group Reference");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Service Group Search");
					click(SiebelOrderObj.SiebelOrder.AddServGrp,"Add Service group");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on OK in Add Service Group");
					Reusable.waitForAjax();
									
					click(SiebelOrderObj.SiebelOrder.SitesTab,"Sites tab");
					Reusable.waitForAjax();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Sites tab.");
				}

				if (resillenceValue.contains("Dual")) 
				{
					Reusable.waitForpageloadmask();
					verifyExists(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
					click(OloIpAccessOrderObj.OloIpAccessOrder.SecondarySite,"SecondarySite");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();

					click(SiebelModeObj.installationTimeUpdate.ClickDropdown.replace("Value","Install Time"));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Installation Time");
					click(SiebelModeObj.installationTimeUpdate.SelectValueDropdown.replace("Value",Install_Time));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Fill Install Time");
					Reusable.waitForpageloadmask();

					click(OloIpAccessOrderObj.OloIpAccessOrder.PrimarySite,"Click Primary Site");
					Reusable.waitForAjax();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on primary site button.");
				}

				Reusable.Save();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on save button.");
				System.out.println("no change");
				break;
			}
			case "IP Access MPR to MVR": 
			{     
				verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Type"),"Router Type");
				click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Type"),"Router Type");
				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Router_Type));
				click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Router_Type));
				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
				
				if(Router_Type.equalsIgnoreCase("Managed Router"))
				{
					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Technology"));
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Technology"));
					Reusable.waitForSiebelLoader();
//					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Router_Technology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Router_Technology));
					Reusable.waitForSiebelLoader();
				}
				click(SiebelModeObj.installationTimeUpdate.ClickDropdown.replace("Value","Install Time"));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Installation Time");
				click(SiebelModeObj.installationTimeUpdate.SelectValueDropdown.replace("Value",Install_Time));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Fill Install Time");
				Reusable.waitForpageloadmask();
				
				Reusable.Save();
				Reusable.waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on save button.");			
				break;
			}

			default: {
				break;
			}
			}
		}
		public void ErrorHandling(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
			if(isVisible("@xpath=//button[text()='Ok']")) 
			{
				System.out.println("Alert Present");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert2 Accept"); // 
				Reusable.waitForSiebelLoader();
				if(isVisible("@xpath=//span[text()='Workflows']/following-sibling::button[@title='Close']"))
				{
					System.out.println("Alert Present");
					click("@xpath=//span[text()='Workflows']/following-sibling::button[@title='Close']","Click on Alert2 Accept");
					Reusable.waitForSiebelLoader();
				}
				TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			}
		}
		
		public void InflightUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
		{
		String Order_type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary/Secondary");
		String Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Bandwidth_3");

		 if (Order_type.toString().contains("Primary") ||Order_type.toString().contentEquals("empty"))
		{
		verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
		click(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidthDrpDwn,"SerBandwidthDrpDwn");
		click(("@xpath=//li/*[text()='"+Bandwidth.toString()+"']"),"Bandwidth");
		Reusable.SendkeaboardKeys(IpAccessModifyOrderObj.IpAccessModifyOrder.SerBandwidth, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		verifyExists(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
		click(IpAccessModifyOrderObj.IpAccessModifyOrder.SaveButton,"SaveButton");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		}
		}

}
