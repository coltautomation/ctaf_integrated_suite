package testHarness.siebeltoNcFunctions;


import java.io.IOException;
import java.util.Random;
import java.util.List;
import pageObjects.c4cObjects.C4CAccountsObj;

import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelCeaseObj;
import pageObjects.siebelObjects.SiebelLiabrary_Obj;
import pageObjects.siebelObjects.SiebelModTechObj;
import pageObjects.siebeltoNCObjects.SiebelModeObj;
import pageObjects.siebeltoNCObjects.SiebelOrderObj;
import pageObjects.siebelObjects.SiebelSpokeSiteObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
//import java.awt.List;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
public class SiebelCEASE extends SeleniumUtils
{
		public static ThreadLocal<String> ServiceOrder2 = new ThreadLocal<>();
		public static ThreadLocal<String> ServiceOrder = new ThreadLocal<>();
		public static ThreadLocal<String> SiebelCeaseOrdernumber = new ThreadLocal<>();
		
		ReusableFunctions Reusable = new ReusableFunctions();

		public void CeaseMainMethod(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
		{
			String ProductType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			String ServiceOrderReference_Number = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
			String SiteServiceOrderReference_Number = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SiteOrderReference_Number");
			
			Reusable.waitForSiebelLoader();
			
			try 
			{
				verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceTab, "Verify ServiceTab");
				click(SiebelCeaseObj.CeaseMainMethodObj.ServiceTab, "Click on ServiceTab");
	
				if (isElementPresent(By.xpath("//locators/AlertAccept")))
				{
					verifyExists(SiebelCeaseObj.CeaseMainMethodObj.AlertAccept, "Verify AlertAccept");
					click(SiebelCeaseObj.CeaseMainMethodObj.AlertAccept, "Click on AlertAccept");
				}
				System.out.println("Service tab clickon");
			} 
			catch (Exception e) 
			{
				try 
				{
					verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceTab, "Verify ServiceTab");
					click(SiebelCeaseObj.CeaseMainMethodObj.ServiceTab, "Click on ServiceTab");
					System.out.println("Service tab javascript");
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
			}
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		//	click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		//	Reusable.waitForSiebelLoader();
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderSearchForAll, "Verify ServiceOrderSearchForAll");
			click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderSearchForAll, "Click on ServiceOrderSearchForAll");
			if (ProductType.equalsIgnoreCase("IPVPN"))
			{
				sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,SiteServiceOrderReference_Number,"Site OrderReference Number");
				
			}
			else 
			{
				sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderReference_Number,"Service OrderReference Number");
			}
			
		//	sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderReference_Number,"Service OrderReference Number");
		
			
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
			click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
			Reusable.waitForSiebelLoader();
		
			////need to comment below code
			
			Reusable.waitForSiebelLoader();
			
		
		//	verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderArrow, "Verify ServiceOrderArrow");
		//	click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderArrow, "Click on ServiceOrderArrow");
			
			//// Need to comment above code
			 
			boolean billing = false;
			String BillingStatus = null;
			for (int i = 0; i < 5; i++)
			{
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderArrow, "Verify ServiceOrderArrow");
			click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderArrow, "Click on ServiceOrderArrow");

			Reusable.waitForSiebelLoader();
			BillingStatus = GetBillingStatus();

			if (BillingStatus.equalsIgnoreCase("COMPLETE"))
			{
			billing = true;
			break;
			} else if (BillingStatus.equalsIgnoreCase("BILLING ERROR"))
			{
			billing = false;
			break;
			}
			}
			//String TempOrder = ProductType.equalsIgnoreCase("IP VPN Service") ? ServiceOrder2.get().toString(): ServiceOrder.get().toString();
			String TempOrder = getTextFrom(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo);

			Assert.assertTrue(BillingStatus.equalsIgnoreCase("COMPLETE") || BillingStatus.equalsIgnoreCase("BILLING ERROR") || BillingStatus.equalsIgnoreCase("SENT TO BILLING"),"Not Able to Proceed The Cease for Order Number : " + TempOrder + "as Billing Status is : "+ BillingStatus);

			if (!billing || BillingStatus.equalsIgnoreCase("SENT TO BILLING"))
			{
				verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo, "Verify ServiceOrderReferenceNo");
				click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo, "Click on ServiceOrderReferenceNo");
				Reusable.waitForSiebelLoader();
	
				verifyExists(SiebelCeaseObj.CeaseMainMethodObj.CeaseInternal, "Verify CeaseInternal");
				click(SiebelCeaseObj.CeaseMainMethodObj.CeaseInternal, "Click on CeaseInternal");
				Reusable.waitForSiebelLoader();
			}
			else
			{
				System.out.println(" Step: Doing Cease by External Cease Option");
				verifyExists(SiebelCeaseObj.CeaseMainMethodObj.CeaseOrder, "Verify CeaseOrder");
				click(SiebelCeaseObj.CeaseMainMethodObj.CeaseOrder, "Click on CeaseOrder");
	//			javaScriptclick(SiebelCeaseObj.CeaseMainMethodObj.CeaseOrder,"Cease button");
				Reusable.waitForSiebelLoader();
			}
			
			
			Reusable.waitForpageloadmask();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		//	Thread.sleep(15000);
		//	SiebelCeaseOrdernumber.set(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderModifyNumber);
			
					
			sendKeys(SiebelCeaseObj.CeaseMainMethodObj.RequestReceivedDate, Reusable.CurrentDate());
			Reusable.waitForSiebelLoader();
			
			String CEASEOrderNumber = getTextFrom(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo);
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CEASE_ServiceOrderNumber", CEASEOrderNumber);	
			
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo, "Verify ServiceOrderReferenceNo");
			click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo, "Click on ServiceOrderReferenceNo");

			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.OrderSubTypeSearch, "Verify OrderSubTypeSearch");
			click(SiebelCeaseObj.CeaseMainMethodObj.OrderSubTypeSearch, "Click on OrderSubTypeSearch");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.AddOrderSubType, "Verify AddOrderSubType");
			click(SiebelCeaseObj.CeaseMainMethodObj.AddOrderSubType, "Click on AddOrderSubType");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.InputOrderSubType, "Verify InputOrderSubType");
			sendKeys(SiebelCeaseObj.CeaseMainMethodObj.InputOrderSubType, "All");
			Reusable.SendkeaboardKeys(SiebelCeaseObj.CeaseMainMethodObj.InputOrderSubType, Keys.ENTER);
			Reusable.SendkeaboardKeys(SiebelCeaseObj.CeaseMainMethodObj.InputOrderSubType, Keys.TAB);

			Reusable.alertPopUp();

			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.SubmitSubOrderType, "Verify SubmitSubOrderType");
			click(SiebelCeaseObj.CeaseMainMethodObj.SubmitSubOrderType, "Click on SubmitSubOrderType");

			Reusable.waitForSiebelLoader();
			
			Reusable.waitForSiebelSpinnerToDisappear();
			
			
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.CeaseReason, "Verify CeaseReason");
			click(SiebelCeaseObj.CeaseMainMethodObj.CeaseReason, "Click on CeaseReason Field");

			sendKeys(SiebelCeaseObj.CeaseMainMethodObj.CeaseReason, "Cease Initiated by Colt");
			Reusable.SendkeaboardKeys(SiebelCeaseObj.CeaseMainMethodObj.CeaseReason, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			System.out.println("Enter Domain Footer");
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.OrderDates, "Verify OrderDates");
			click(SiebelCeaseObj.CeaseMainMethodObj.OrderDates, "Click on OrderDates");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date Icon");
//			Reusable.threeDaysBefore();
			Reusable.pickCalenderDateforCease(SiebelModeObj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date Icon");
			Reusable.pickCalenderDateforCease(SiebelModeObj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date Icon");
			Reusable.pickCalenderDateforCease(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date");
		
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.Billing, "Verify Billing");
			click(SiebelCeaseObj.CeaseMainMethodObj.Billing, "Click on Billing");

			System.out.println("BILLING TAB");
			Reusable.waitForSiebelLoader();
		//	String BillingStartDate = Reusable.FutureDate(15);
			
			verifyExists(SiebelModeObj.EnterBillingDateInFooter.BillingStartDateAccessIcon,"Billing Start Date Access Icon");
			Reusable.pickCalenderDateforCease(SiebelModeObj.EnterBillingDateInFooter.BillingStartDateAccessIcon,"Billing End Date Access");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelCeaseObj.CeaseMainMethodObj.BillingEndDateAccessIcon, "Verify BillingEndDate");
			Reusable.pickCalenderDateforCease(SiebelCeaseObj.CeaseMainMethodObj.BillingEndDateAccessIcon,"Billing Start Date Access");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.EnterBillingDateInFooter.POStartDateAccessIcon,"PO Start Date Access Icon");
			Reusable.pickCalenderDateforCease(SiebelModeObj.EnterBillingDateInFooter.POStartDateAccessIcon,"PO Start Date Access");

			//verifyExists(SiebelCeaseObj.CeaseMainMethodObj.POStartDateAccess, "Verify POStartDateAccess");
			//click(SiebelCeaseObj.CeaseMainMethodObj.POStartDateAccess, "Click on POStartDateAccess");
					
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			System.out.println("page load succesfully now come to middle applet");
			
		}
		
		public void CeaseCompletedValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
		{
			Reusable.waitForSiebelLoader();
//			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelCeaseObj.CeaseCompletedValidation.OrderStatusDropdown,"Verify OrderStatusDropdown");
			click(SiebelCeaseObj.CeaseCompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelCeaseObj.CeaseCompletedValidation.SelectCompleted,"Verify SelectCompleted");
			click(SiebelCeaseObj.CeaseCompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelCeaseObj.CeaseCompletedValidation.OrderComplete,"Verify OrderComplete");
			click(SiebelCeaseObj.CeaseCompletedValidation.OrderComplete,"Click Completed StatusOrder Complete");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				System.out.println("");
				System.out.println("Alert Present");
				verifyExists(SiebelCeaseObj.CeaseCompletedValidation.AlertAccept,"Verify AlertAccept");
				click(SiebelCeaseObj.CeaseCompletedValidation.AlertAccept,"Click on AlertAccept");
				Reusable.waitForSiebelLoader();
			}
			
			try {
				if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']")))
				{
					System.out.println("");
					System.out.println("Alert Present");
					verifyExists(SiebelCeaseObj.CeaseCompletedValidation.SubnetworkPopUP,"Verify SubnetworkPopUP");
					click(SiebelCeaseObj.CeaseCompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
					Reusable.waitForSiebelLoader();
			
				}
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			Reusable.waitForSiebelLoader();
			String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
			if(Orderstatus.contains("Progress")) {
				Reusable.savePage();
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
				click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
				click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				if(isElementPresent(By.xpath("//button[text()='Yes']"))) {
					verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
					click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
					Reusable.waitForSiebelLoader();
					Reusable.waitForAjax();
				}
			}
			
			String CompValidation= null;
			CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
			
			if(CompValidation.equals("Completed")) {
				Report.LogInfo("verify","<i></b> order status completed", "PASS");
			}else{
				Report.LogInfo("verify","<i></b> order status not competed", "Fail");

			}
			
			Reusable.waitForSiebelLoader();
			
	    }
		
		public void DeliveryValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
		{
			String ProductType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			String Comments = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
			String ResilienceType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
			String PrimarySecondary= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary/Secondary");

			Reusable.waitForSiebelLoader();	
			verifyExists(SiebelCeaseObj.DeliveryValidation.OrderStatusDropdown,"Verify DeliveryValidation");
			click(SiebelCeaseObj.DeliveryValidation.OrderStatusDropdown,"Click on DeliveryValidation");
			Reusable.waitForSiebelLoader();	
			verifyExists(SiebelCeaseObj.DeliveryValidation.SelectDeliveryValidation,"Verify SelectDeliveryValidation");
			click(SiebelCeaseObj.DeliveryValidation.SelectDeliveryValidation,"Entered Order Status Delivery");
			
			if (!ProductType.equalsIgnoreCase("Ethernet Hub")) 
			{
				Reusable.waitForSiebelLoader();	
			}
			if (ProductType.equalsIgnoreCase("IP VPN Wholesale")) // Added by Abhay
			{
				Reusable.ClickContinue();
				Reusable.waitForSiebelLoader();	
			}
			if (ProductType.equalsIgnoreCase("Ethernet Hub")) 
			{
				try 
				{
					if(isElementPresent(By.xpath("//button[text()='Yes']")))
					{
						verifyExists(SiebelCeaseObj.DeliveryValidation.PopupYes,"Verify PopupYes");
						click(SiebelCeaseObj.DeliveryValidation.PopupYes,"Pop Up Yes Clicked");
						waitToPageLoad();
						System.out.println("Pop Up Yes Clicked");
						Reusable.waitForSiebelLoader();	
					}
				} 
				catch (Exception e) 
				{
					System.out.println("Pop Up Yes Clicked");
				}
			}
//			if (ProductType.equalsIgnoreCase("IPA")) 
//			{
//				Reusable.alertPopUp();
//			}
			
			Reusable.waitForSiebelLoader();
			if (isVisible((SiebelLiabrary_Obj.serviceOrder.AlertAccept3))) 
			{
				verifyExists(SiebelLiabrary_Obj.serviceOrder.AlertAccept3,"AlertAccept");
				click(SiebelLiabrary_Obj.serviceOrder.AlertAccept3,"AlertAccept");
				Reusable.waitForSiebelLoader();
			}
			else
			{
//				if (isElementPresent(By.xpath("//button[text()='Yes']")))
//				{
//					verifyExists(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
//					click(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
//					waitToPageLoad();
//					Reusable.waitForSiebelLoader();
//				}
//				try 
//				{
//					if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']")))
//					{
//						System.out.println("");
//						System.out.println("Alert Present");
//						verifyExists(SiebelCeaseObj.DeliveryValidation.SubnetworkPopUP,"Verify SubnetworkPopUP");
//						click(SiebelCeaseObj.DeliveryValidation.SubnetworkPopUP,"SubnetworkPopUP Clicked");
//						Reusable.waitForSiebelLoader();	
//					}
//				} 
//				catch (Exception e) 
//				{
//					System.out.println(e.toString());
//				}
//				Reusable.waitForSiebelLoader();	
//				
//				if(isElementPresent(By.xpath("//button[text()='Ok']"))) 
//				{
//					System.out.println("");
//					System.out.println("Alert Present");
//					click(SiebelAddProdcutObj.CompletedValidation.AlertAccept2,"Click on Alert2 Accept"); // 
//				}
				Reusable.waitForSiebelLoader();
				
				String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
				if(Orderstatus.contains("Commercial Validation")) 
				{
//					DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
					if(!Comments.contains("OLO"))
					{
						if (ProductType.equalsIgnoreCase("IPVPN"))
						{
							verifyExists(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");

							Reusable.alertPopUp();

							click(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");

							verifyExists(SiebelOrderObj.SiebelOrder.Primarycomp2,"Site TechnicalOrder Serviceid");
							String PrimComp2TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Primarycomp2);
							DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalOrderServiceid", PrimComp2TechnicalServiceid);
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();

							verifyExists(SiebelOrderObj.SiebelOrder.Primarycomp3,"Ednpoint TechnicalOrder Serviceid");
							String PrimComp3TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Primarycomp3);
							DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Comp3TechnicalServiceid_Prim", PrimComp3TechnicalServiceid);
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();
							
							if (ResilienceType.contains("Dual") && (PrimarySecondary.equalsIgnoreCase("Secondary")))
							{
								verifyExists(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");
								click(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");
								
								verifyExists(SiebelOrderObj.SiebelOrder.Secondarycomp2,"TechnicalOrder Serviceid");
								String SecondComp2TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Secondarycomp2);
								DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalServiceid_Spoke", SecondComp2TechnicalServiceid);
								Reusable.waitForSiebelLoader();
								Reusable.waitForSiebelSpinnerToDisappear();

								verifyExists(SiebelOrderObj.SiebelOrder.Secondarycomp3,"TechnicalOrder Serviceid");
								String SecondComp3TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Secondarycomp3);
								DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Comp3TechnicalServiceid_Second", SecondComp3TechnicalServiceid);
								Reusable.waitForSiebelLoader();
								Reusable.waitForSiebelSpinnerToDisappear();
							}

						}
						else if (ProductType.equalsIgnoreCase("IPA"))
						{
							verifyExists(SiebelModeObj.SubmitOrder.CircuitReference,"CircuitReference");
							String CircuitReferenceNumber = getTextFrom(SiebelModeObj.SubmitOrder.CircuitReference);
							DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CircuitReference_No", CircuitReferenceNumber);
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();
							
							verifyExists(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");
							click(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");
							
							verifyExists(SiebelModeObj.SubmitOrder.TechnicalOrderServiceid,"TechnicalOrderServiceid");
							String TechnicalOrderServiceid = getTextFrom(SiebelModeObj.SubmitOrder.TechnicalOrderServiceid);
							DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalOrderServiceid", TechnicalOrderServiceid);
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();
							
							if (ResilienceType.contains("Dual") && (PrimarySecondary.equalsIgnoreCase("Secondary")))
							{
								verifyExists(SiebelModeObj.SubmitOrder.SecondaryTechnicalOrderServiceid,"TechnicalOrderServiceid");
								String SecondComp2TechnicalServiceid = getTextFrom(SiebelModeObj.SubmitOrder.SecondaryTechnicalOrderServiceid);
								DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalServiceid_Spoke", SecondComp2TechnicalServiceid);
								Reusable.waitForSiebelLoader();
								Reusable.waitForSiebelSpinnerToDisappear();
							}
						}
						else
						{
							verifyExists(SiebelModeObj.SubmitOrder.CircuitReference,"CircuitReference");
							String CircuitReferenceNumber = getTextFrom(SiebelModeObj.SubmitOrder.CircuitReference);
							DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CircuitReference_No", CircuitReferenceNumber);
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();
							
							verifyExists(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");
							click(SiebelModeObj.SubmitOrder.TechnicalOrderTab,"TechnicalOrderTab");
							
							verifyExists(SiebelModeObj.SubmitOrder.TechnicalOrderServiceid,"TechnicalOrderServiceid");
							String TechnicalOrderServiceid = getTextFrom(SiebelModeObj.SubmitOrder.TechnicalOrderServiceid);
							DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalOrderServiceid", TechnicalOrderServiceid);
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();
						}
					}
					else
					{
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Not required to move to Delivery status");
						Thread.sleep(5000);
					}
			}
			
			}
		}
		
		
		
		
		//************* K 15-12
		public void CeaseCommercialValidation(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
		{
			String ProductName = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			
			verifyExists(SiebelAddProdcutObj.CommercialValidation.ServiceOrderSourceRefNumber,"Service Order Source Ref Number");
			//	sendKeys(SiebelAddProdcutObj.CommercialValidation.ServiceOrderSourceRefNumber,ServiceOrderReference_Number,"Service Order Source Ref Number");
			sendKeys(SiebelAddProdcutObj.CommercialValidation.ServiceOrderSourceRefNumber,"12345","Service Order Source Ref Number");
			Reusable.alertPopUp();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.alertPopUp();
			verifyExists(SiebelCeaseObj.CeaseCommercialValidationObj.OrderStatus, "OrderStatus");
			sendKeys(SiebelCeaseObj.CeaseCommercialValidationObj.OrderStatus,"Commercial Validation");
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CommercialValidation.OrderStatus, Keys.ENTER);
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CommercialValidation.OrderStatus, Keys.TAB);
			Reusable.waitForSiebelLoader();
		
		//	if (!ProductName.equalsIgnoreCase("Ethernet Hub"))
		//	Reusable.SendkeaboardKeys(SiebelCeaseObj.CeaseCommercialValidationObj.OrderStatus, Keys.TAB);

			if (ProductName.equalsIgnoreCase("Ethernet Hub"))
			{
				Reusable.waitForSiebelLoader();
			try
			{
			if (isElementPresent(By.xpath("//button[text()='Yes']")))
			{
			verifyExists(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
			click(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			}
			}
			catch (Exception e)
			{
			System.out.println("Pop Up Yes Clicked");
			}
			}
			
		
			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//button[text()='Yes']")))
			{
			verifyExists(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
			click(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
			Reusable.waitForSiebelLoader();
			}
			
			if (isElementPresent(By.xpath("//button[text()='Yes']")))
			{
			verifyExists(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
			click(SiebelCeaseObj.CeaseCommercialValidationObj.PopupYes, "PopupYes");
			Reusable.waitForSiebelLoader();
			}
		
			try {
			if (isElementPresent(By.xpath("//button[@class='colt-primary-btn']")))
			{
			verifyExists(SiebelCeaseObj.CeaseCommercialValidationObj.SubnetworkPopUP, "SubnetworkPopUP");
			click(SiebelCeaseObj.CeaseCommercialValidationObj.SubnetworkPopUP, "SubnetworkPopUP");
			Reusable.waitForSiebelLoader();
			}
			}
			catch (Exception e) {
			System.out.println(e.toString());
			}
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

		}
		
		public String GetBillingStatus() throws Exception,InterruptedException, AWTException, IOException 
		{
		
	   //List<WebElement> HeaderList= GetWebElements(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderGridHeader);
    	List<WebElement> HeaderList =SeleniumUtils.WEB_DRIVER_THREAD_LOCAL.get().findElements(By.xpath("//table[@class='ui-jqgrid-htable']//th//div"));

	    
		int StatusHeader = -1;
		int i = 0;
		/*for (WebElement ele : HeaderList)
		{
		//clickByJS(ele);
			click(ele);
		//click(locator);
			
		String Text = ele.getText();

		*/
		
		
		for(WebElement ele :HeaderList)
		  {
  	  //   click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderGridHeader);
  	
			 String Text=ele.getText();	
		
		
		
		System.out.println("Column : " + Text);
		if (Text.equalsIgnoreCase("Billing Status")) {
		StatusHeader = i;
		break;
		}
		i++;
		}
		Assert.assertTrue(StatusHeader > -1, "Column Name 'Billing Status' not found");

		String temp = SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderGridItem.replace("-10", String.valueOf(i - 2));
		String ServiceOrderStatus = getTextFrom(temp);
		System.out.println("Service Order Current Status : " + ServiceOrderStatus);
		return ServiceOrderStatus;
		}
		
		
		
		public void updateProductConfigurationDetailsAndCompleteOrderStatusCEASE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
		{
			SiebelAddProduct Product = new SiebelAddProduct();
			String ProdType = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Product_Type");
			String Samle = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Samle");
			SiebelManualValidation Validation = new SiebelManualValidation();
			SiebelModTech ModTechObj = new SiebelModTech();
			SiebelCEASE CEASE = new SiebelCEASE();
			
			Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
			CEASE.CeaseCommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			CEASE.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			CEASE.CeaseCompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			
		}	
		
}





