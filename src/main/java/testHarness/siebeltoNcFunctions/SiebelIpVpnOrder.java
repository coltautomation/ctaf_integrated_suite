package testHarness.siebeltoNcFunctions;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.siebeltoNCObjects.SiebelAddProdcutObj;
import pageObjects.siebeltoNCObjects.SiebelLiabrary_Obj;
import pageObjects.siebeltoNCObjects.SiebelModeObj;
import pageObjects.siebeltoNCObjects.SiebelOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class SiebelIpVpnOrder extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	SiebelAddProduct AddProduct = new SiebelAddProduct();
	
	public void addIpvpnSecondarySite(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Layer3_Resilience = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String Service_Bandwidth_2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
		String Site_ID_Secondary_Site = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Site_ID");
		String Secondary_Access_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Access_Type/Secondary");
		String Secondary_Access_Technology = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Access_Technology/Secondary");
		String Building_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Building_Type");
		String Customer_Site_Pop_Status = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Customer_Site_Pop_Status");
		String Third_party_access_provider = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Third_party_access_provider");
		String Cabinet_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Cabinet_Type");
		String Cabinet_ID = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Cabinet_ID");
		String Port_Role = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Port_Role");
		String Backup_Bandwidth = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
		String Install_Time= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
		String Site_Name = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Site_Name");
		String Fibre_Type=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String presentation_interface=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Presentation_Interface");
		String Connector_Type=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Connector_Type");

		if (Layer3_Resilience.contains("Dual")) 
		{
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelOrderObj.SiebelOrder.SecondarySite,"SecondarySite");
			click(SiebelOrderObj.SiebelOrder.SecondarySite,"SecondarySite");	
			
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "Service Bandwidth (Secondary)"),"Click Dropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "Service Bandwidth (Secondary)"),"Click Dropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Service_Bandwidth_2),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Service_Bandwidth_2),"SelectValueDropdown");
			Reusable.waitForSiebelLoader();
			

			waitForElementToAppear(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondary, 10);
			verifyExists(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondarydropdown,"OSSPlatformflagsecondarydropdown");
			click(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondarydropdown,"OSSPlatformflagsecondarydropdown");
			
			ClickonElementByString("//*[text()='IQNet']",10);
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.OSSPlatformflagsecondary, Keys.TAB);
			Reusable.waitForSiebelLoader();
			clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Capacity Check Reference"));
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Capacity Check Reference"),"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Capacity Check Reference"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					Reusable.waitForSiebelLoader();
				}
			}
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","Sites"),"ClickLink");
			click(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","Sites"),"ClickLink");

			Reusable.waitForpageloadmask();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA, "Search Address SiteA");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA,"Click on Search Address SiteA");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

//			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.addSiteAXNGDetails.SiteID,"Enter Site ID");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.addSiteAXNGDetails.SiteID,Site_ID_Secondary_Site,"Site ID Value");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
			click(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
			click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
//			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					Reusable.waitForSiebelLoader();
				}
			}
            refreshPage();
        	Reusable.waitForSiebelLoader();
			verifyExists(SiebelOrderObj.SiebelOrder.SecondarySite,"SecondarySite");
			click(SiebelOrderObj.SiebelOrder.SecondarySite,"SecondarySite");
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDrpDwn.replace("Value", "Service Party"),"ClickDrpDwn");
			click(SiebelOrderObj.SiebelOrder.ClickDrpDwn.replace("Value", "Service Party"),"ClickDrpDwn");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.PartyNameSubmit,"PartyNameSubmit");
			click(SiebelOrderObj.SiebelOrder.PartyNameSubmit,"PartyNameSubmit");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelOrderObj.SiebelOrder.ClickDrpDwn.replace("Value", "Site Contact"),"ClickDrpDwn");
			click(SiebelOrderObj.SiebelOrder.ClickDrpDwn.replace("Value", "Site Contact"),"ClickDrpDwn");				
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelOrderObj.SiebelOrder.LastNameSiteSubmit,"LastNameSiteSubmit");
			click(SiebelOrderObj.SiebelOrder.LastNameSiteSubmit,"LastNameSiteSubmit");
			Reusable.waitForSiebelLoader();
			
			if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					Reusable.waitForSiebelLoader();
				}
			}

			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "Access Type"),"Access Type");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "Access Type"),"Access Type");	

			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", Secondary_Access_Type),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", Secondary_Access_Type),"SelectValueDropdown");	

			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//button[text()='Proceed']")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
					click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
					Reusable.waitForAjax();
					Reusable.waitForSiebelLoader();
				}
			}

			if (Secondary_Access_Type.contains("Colt Fibre")) 
			{
				
				verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Access Technology"),"Access Technology");
				click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "Access Technology"),"Access Technology");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Secondary_Access_Technology),"SelectValueDropdown");
				click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Secondary_Access_Technology),"SelectValueDropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Building Type"),"ClickDropdown");
				click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Building Type"),"ClickDropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Building_Type),"SelectValueDropdown");
				click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Building_Type),"SelectValueDropdown");	
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Customer Site Pop Status"),"ClickDropdown");
				click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Customer Site Pop Status"),"ClickDropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Customer_Site_Pop_Status),"SelectValueDropdown");
				click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Customer_Site_Pop_Status),"SelectValueDropdown");	
				Reusable.waitForSiebelLoader();				
		
			}
			if (Secondary_Access_Type.contains("3rd Party Leased"))
			{

				verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Third Party Access Provider"),"ClickDropdown");
				click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Third Party Access Provider"),"ClickDropdown");	
				Reusable.waitForSiebelLoader();
			
				verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Third_party_access_provider),"SelectValueDropdown");
				click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Third_party_access_provider),"SelectValueDropdown");	
				Reusable.waitForSiebelLoader();	
				
				clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "3rd Party Connection Reference"));
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "3rd Party Connection Reference"),"Test123");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "3rd Party Connection Reference"), Keys.ENTER);

				Reusable.waitForSiebelLoader();	
			}

			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Cabinet Type"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Cabinet Type"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Cabinet_Type),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Cabinet_Type),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();	
			
			clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Cabinet ID"));
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Cabinet ID"),Cabinet_ID);

			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Cabinet ID"), Keys.ENTER);
			Reusable.waitForSiebelLoader();	
			
			Random rand = new Random();
			int rand_int1 = rand.nextInt(1000);

			clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Shelf ID"));
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Shelf ID"),Integer.toString(rand_int1));
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Shelf ID"), Keys.ENTER);
			Reusable.waitForSiebelLoader();	
			
			clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Slot ID"));
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Slot ID"),Integer.toString(rand_int1));
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Slot ID"), Keys.ENTER);
			Reusable.waitForSiebelLoader();	

			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Connector Type"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Connector Type"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Connector_Type),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Connector_Type),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();	
			
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Fibre Type"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Fibre Type"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Fibre_Type),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Fibre_Type),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();	
			
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Port Role"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Port Role"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Port_Role),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Port_Role),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();			
			
	
			String bandwidth[]=Backup_Bandwidth.split(" ");
			int value=Integer.parseInt(bandwidth[0]);

			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Presentation Interface"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Presentation Interface"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",presentation_interface),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",presentation_interface),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();	
			
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Install Time"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Install Time"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Install_Time),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Install_Time),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Router Model"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Router Model"),"ClickDropdown");	
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","NA"),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","NA"),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();
			
	
			verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Site Name"),"ClickDropdown");
			click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value","Site Name"),"ClickDropdown");	

			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Site_Name),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Site_Name),"SelectValueDropdown");	
			Reusable.waitForSiebelLoader();
			
			if(isElementPresent(By.xpath("//button[text()='Proceed']")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
					click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
					Reusable.waitForSiebelLoader();
				}
			}
			if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					Reusable.waitForSiebelLoader();
				}
			}
			Reusable.waitForAjax();
			AddProduct.SecondOperationalAttributesIPVPN(testDataFile, sheetName, scriptNo, dataSetNo);
		}
	}
	
	public void addipvpnServiceFetures(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String Netflow_Enabled = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Netflow Enabled");
		String IPv4address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IPv4address");
		String Service_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Service_Type");
		String Netflow_version = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Netflow_version");
		String Port_Number = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Port Number");
		String SNMP_Enabled = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SNMP Feature");
		String SNMP_Format = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SNMP Format");
		String Group_Name  = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Group Name");
		String Username = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Username");
		String Authentication_Password = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Authentication Password");
		String Privacy_Password = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Privacy Password");
		String SNMP_Device_IPv4_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SNMP Device IPv4 Address");
		String Customer_Sub_VPN_Alias = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Customer Sub-VPN Alias");
		String Device_Type = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"SNMP Device Type");
		
		javaScriptclick(SiebelOrderObj.SiebelOrder.ShowFullInfo);
		Reusable.waitForSiebelLoader();

		if (Netflow_Enabled.contains("Yes"))
		{
			if(Service_Type.contains("IP VPN Plus"))
			{
				click(SiebelOrderObj.SiebelOrder.Netflow,"Netflow");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "IPv4 Address"),"IPv4 Address");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","IPv4 Address"),IPv4address);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","IPv4 Address"),Keys.ENTER);
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Netflow version"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Netflow version"));
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Netflow_version),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Netflow_version));
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Port Number"),"TextInput");
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Port Number"),Port_Number);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "Port Number"),Keys.ENTER);

				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}
			}
		}
		if (SNMP_Enabled.contains("Yes"))
		{
			if(Service_Type.contains("IP VPN Plus"))
			{
				click(SiebelOrderObj.SiebelOrder.snmpfeature,"SNMpFEATURE");
				Reusable.waitForSiebelLoader();
				scrollDown(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Authentication Password"));

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "SNMP Format"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "SNMP Format"),"Dropdown");
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SNMP_Format),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", SNMP_Format));
				Reusable.waitForSiebelLoader();

				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Authentication Password"),Authentication_Password);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Authentication Password"),Keys.ENTER);
				Reusable.waitForSiebelLoader();
				
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Group Name"),Group_Name);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Group Name"),Keys.ENTER);
				Reusable.waitForSiebelLoader();
				
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Privacy Password"),Privacy_Password);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Privacy Password"),Keys.ENTER);
				Reusable.waitForSiebelLoader();

				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Username"),Username);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","Username"),Keys.ENTER);
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Device Type"),"DropDown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Device Type"),"Dropdown");
				
				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Device_Type),"Select Value Dropdown");
				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Device_Type));
				Reusable.waitForSiebelLoader();
				
				sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","SNMP Device IPv4 Address"),SNMP_Device_IPv4_Address);
				Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value","SNMP Device IPv4 Address"),Keys.ENTER);
				Reusable.waitForSiebelLoader();

//				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Sub VPN ID"),"DropDown");
//				click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "Sub VPN ID"));
//
//				verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Customer_Sub_VPN_Alias),"Select Value Dropdown");
//				click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Customer_Sub_VPN_Alias));
//				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelOrderObj.SiebelOrder.Subvpnidrpdown,"Subvpnidrpdown");
				click(SiebelOrderObj.SiebelOrder.Subvpnidrpdown,"Subvpnidrpdown");

				verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown2.replace("Value",Customer_Sub_VPN_Alias),"SelectValueDropdown2");
				click(SiebelOrderObj.SiebelOrder.SelectValueDropdown2.replace("Value", Customer_Sub_VPN_Alias),"SelectValueDropdown2");	
				Reusable.waitForAjax();
				
				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}
			}
		}
		//close window
		javaScriptclick(SiebelOrderObj.SiebelOrder.SiebIPVpcnClose);
		
//		verifyExists(SiebelOrderObj.SiebelOrder.SiebIPVpcnClose,"IPVPN Service Close");
//		click(SiebelOrderObj.SiebelOrder.SiebIPVpcnClose,"IPVPN Service Close");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
		{
			if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
			{
				verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
				click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
			}
			Reusable.waitForSiebelLoader();
		}
	}
	
	public void addFeatureIpvnsiteRight(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String Router_Type  = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Type");
		String Class_of_Service = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Class of Service");
		String Business_1_Percent = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Business 1 %");
		String Business_2_Percent = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Business 2 %");
		String Business_3_Percent  = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Business 3 %");
		String Management_Percent = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Management %");
		String Premium_Percent = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Premium %");
		String Standard_Percent = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Standard %");
		String CoS_Marking  = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CoS Marking");
		String Dest_IPv4_Netmask = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Dest IPv4 Netmask");
		String Sour_IPv4_Netmask = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Sour IPv4 Netmask");
		String Dest_IPv4_Add = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Dest IPv4 Add");
		String Sour_IPv4_Add  = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Sour IPv4 Add");
		String Dest_IPv4_Netmask_Business1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Dest IPv4 Netmask_Business1");
		String Sour_IPv4_Netmask_Business1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Sour IPv4 Netmask_Business1");
		String Dest_IPv4_Add_Business1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Dest IPv4 Add_Business1");
		String Sour_IPv4_Add_Business1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Sour IPv4 Add Business1");
		String DHCP_relay_enabled = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DHCP Feature");
		String DHCP_relay_IPv4_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DHCP Address");
		String NAT_Feature = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"NAT Feature");
		String Inside_IPv4_Addr = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Inside IPv4 Addr");
		String Outside_IPv4_Addr = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Outside IPv4 Addr");
		String Subnet_Mask = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Subnet Mask");
		
		if (Router_Type.contains("Managed Router"))
		{
			verifyExists(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","IP Details"),"ClickLink");
			click(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","IP Details"),"ClickLink");

			Reusable.waitForpageloadmask();

			javaScriptclick(SiebelOrderObj.SiebelOrder.ChildIPDetails);
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();

			javaScriptclick(SiebelOrderObj.SiebelOrder.ShowInfoRight);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();

			//to add features from right side show full info
			
			
			if (DHCP_relay_enabled.equals("Yes")) //if DHCP is yes
			{
				javaScriptclick(SiebelOrderObj.SiebelOrder.DHCP);

				Reusable.waitForSiebelLoader();	

//				ScrollIntoViewByString(SiebelOrderObj.SiebelOrder.TextInput);
				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","DHCP Relay IPv4 Address"),"DHCP IPv4 Add");
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","DHCP Relay IPv4 Address"),DHCP_relay_IPv4_Address,"DHCP IPv4 Add");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","DHCP Relay IPv4 Address"), Keys.ENTER);
				Reusable.waitForAjax();

				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}
			}

			if (Class_of_Service.equals("Yes")) //if cos is yes
			{
				javaScriptclick(SiebelOrderObj.SiebelOrder.Classofservice);
				Reusable.waitForSiebelLoader();
				//added new line ready for nc- only for rfs

				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}	
				
				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Business 1 %"),"TextInput");
				click(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Business 1 %"),"TextInput");	
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 1 %"),Keys.chord(Keys.CONTROL,"a",Keys.BACK_SPACE),"TextInput");
				//sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 1 %"),Keys.chord(Keys.BACK_SPACE),"TextInput");
				Reusable.waitForAjax();
				Reusable.waitForAjax();

				clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Business 1 %"));
				//Clear(getwebelement(xml.getlocator("//locators/IPVPNSite/TextInput").replace("Value", "Business 1 %")));
				Reusable.waitForAjax();
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 1 %"),Business_1_Percent,"TextInput");
				
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 1 %"), Keys.ENTER);
				Reusable.waitForAjax();
				
				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						
						Reusable.waitForSiebelLoader();
					}
				} 
				
				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Business 2 %"),"TextInput");
				click(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Business 2 %"),"TextInput");	
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 2 %"),Keys.chord(Keys.CONTROL,"a",Keys.BACK_SPACE),"TextInput");
				Reusable.waitForSiebelLoader();

				//clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Business 2 %"));
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 2 %"),Business_2_Percent ,"TextInput");
				
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 2 %"), Keys.ENTER);
				Reusable.waitForAjax();
				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						
						Reusable.waitForSiebelLoader();
					}
				}
				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Business 3 %"),"TextInput");
				click(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Business 3 %"),"TextInput");	
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 3 %"),Keys.chord(Keys.CONTROL,"a",Keys.BACK_SPACE),"TextInput");
				Reusable.waitForSiebelLoader();
//				//clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Business 3 %"));
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 3 %"),Business_3_Percent,"TextInput");
				
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Business 3 %"), Keys.ENTER);
				Reusable.waitForSiebelLoader();

				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						
						Reusable.waitForSiebelLoader();
					}
				}

				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Management %"),"TextInput");
				click(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Management %"),"TextInput");	
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Management %"),Keys.chord(Keys.CONTROL,"a",Keys.BACK_SPACE),"TextInput");
				Reusable.waitForSiebelLoader();
				//clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Management %"));

				Reusable.waitForSiebelLoader();
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Management %"),Management_Percent,"TextInput");

				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Management %"), Keys.ENTER);
				Reusable.waitForSiebelLoader();
				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						
						Reusable.waitForSiebelLoader();
					}
				}

				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Premium %"),"TextInput");
				click(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Premium %"),"TextInput");	
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value",  "Premium %"),Keys.chord(Keys.CONTROL,"a",Keys.BACK_SPACE),"TextInput");
				Reusable.waitForSiebelLoader();
//				//clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Premium %"));

				Reusable.waitForSiebelLoader();
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Premium %"),Premium_Percent,"TextInput");

				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Premium %"), Keys.ENTER);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Premium %"), Keys.TAB);
				Reusable.waitForSiebelLoader();
				
				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						
						Reusable.waitForSiebelLoader();
					}
				}
						     
//				verifyExists(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Standard %"),"TextInput");
//				click(SiebelOrderObj.SiebelOrder.TextInput.replace("Value", "Standard %"),"TextInput");
//				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Standard %"),Keys.chord(Keys.CONTROL,"a"));
//				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Standard %"),Keys.chord(Keys.BACK_SPACE));
//				Reusable.waitForAjax();
//				//clearTextBox(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Standard %"));
//
//				Reusable.waitForAjax();
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Standard %"),Standard_Percent,"TextInput");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Standard %"), Keys.ENTER);
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Standard %"), Keys.TAB);

				if(isElementPresent(By.xpath("//button[text()='Proceed']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.Proceed).isDisplayed())
					{
						verifyExists(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						click(SiebelOrderObj.SiebelOrder.Proceed,"Proceed");
						
						Reusable.waitForSiebelLoader();
					}
				}

				verifyExists(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "CoS Marking"),"ClickDropdown");
				click(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "CoS Marking"),"ClickDropdown");
				
				Reusable.waitForAjax();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", CoS_Marking),"ClickDropdown");
				click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", CoS_Marking),"ClickDropdown");
				
				Reusable.waitForAjax();
				Reusable.waitForSiebelLoader();



				if (CoS_Marking.equals("Custom ACL")) //For Cos marking selection
				{
					boolean p=true;
					boolean b1=true;
					boolean b2=true;
					boolean b3=true;

					if(Integer.parseInt(Premium_Percent)>0)
					{
						p = true;
					}
					if(Integer.parseInt(Business_1_Percent)>0)
					{
						b1 = true;
					}
					if(Integer.parseInt(Business_2_Percent)>0)
					{
						b2 = true;
					}
					if(Integer.parseInt(Business_3_Percent)>0)
					{
						b3 = true;
					}

					if(p)
					{
						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
						Reusable.waitForAjax();
						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Premium","Premium");
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.classinputa, Keys.TAB);
						//verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Premium"),"SelectValueDropdown");
						//click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Premium"),"SelectValueDropdown");
						
						Reusable.waitForAjax();
						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"UDP","protocolinput");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
						Reusable.waitForAjax();


						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Dest_IPv4_Netmask,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Sour_IPv4_Netmask,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Dest_IPv4_Add,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Sour_IPv4_Add,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();		

					

					//	Log.info("updtaed premium address marking");
						p=true;
					}
					else if(b1)
					{
						clearTextBox(host);
						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
						Reusable.waitForAjax();	
						
						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Business 1","Business 1");
						Reusable.waitForAjax();		

						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Business 1"),"SelectValueDropdown");
						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Business 1"),"SelectValueDropdown");
						Reusable.waitForAjax();		
						
						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
						Reusable.waitForAjax();		

						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"TCP","protocolinput");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
						Reusable.waitForAjax();		
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Dest_IPv4_Netmask_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Sour_IPv4_Netmask_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Dest_IPv4_Add_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Sour_IPv4_Add_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();						
				

						//Log.info("updtaed 'Business 1' address marking");
						b1=true;
					}
//					else if(b2)
//					{
//						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Business 2","Business 2");
//						Reusable.waitForAjax();		
//
//						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Business 2"),"SelectValueDropdown");
//						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Business 2"),"SelectValueDropdown");
//						Reusable.waitForAjax();		
//						
//						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"TCP","protocolinput");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Inputdata174,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Inputdata175,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Inputdata176,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Inputdata177,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();		
//						
//						//Log.info("updtaed 'Business 2' address marking");
//
//						b2=true;
//					}
//					else if(b3)
//					{
//						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Business 3","Business 3");
//						Reusable.waitForAjax();		
//
//						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Business 3"),"SelectValueDropdown");
//						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Business 3"),"SelectValueDropdown");
//						Reusable.waitForAjax();		
//						
//						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"TCP","protocolinput");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Inputdata178,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Inputdata179,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Inputdata180,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Inputdata181,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();	
//											
//						//Log.info("updtaed 'Business 3' address marking");
//
//						b3=true;
//					}

					else
					{
						System.out.println("nothing updated , should be added atleast one IPv4 Address Based Marking component");
					}

					if(p)
					{
						//if by default address marking added then this line wont execute - directly update the value 
						clickCheckBox(SiebelOrderObj.SiebelOrder.addressmarkingadd, "CHECK", true);
						Reusable.waitForSiebelLoader();	
						
						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
						Reusable.waitForAjax();	
						
						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"UDP","protocolinput");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
						Reusable.waitForAjax();	
					
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Dest_IPv4_Netmask,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Sour_IPv4_Netmask,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Dest_IPv4_Add ,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Sour_IPv4_Add,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();	
						
						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
						Reusable.waitForAjax();	
						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Premium","Premium");
						Reusable.waitForAjax();	
						
						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Premium"),"SelectValueDropdown");
						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Premium"),"SelectValueDropdown");
						Reusable.waitForAjax();	


						//Log.info("added premium address marking");

					}
					if(b1)
					{
						clickCheckBox(SiebelOrderObj.SiebelOrder.addressmarkingadd,"CHECK",true);
						Reusable.waitForSiebelLoader();
						
						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
						Reusable.waitForAjax();	
						
						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"TCP","protocolinput");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
						Reusable.waitForAjax();	
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Dest_IPv4_Netmask_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Sour_IPv4_Netmask_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
						Reusable.waitForAjax();
						
						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
						Reusable.waitForAjax();	
						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Business 1","Business 1");
						Reusable.waitForAjax();	
						
						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Business 1"),"SelectValueDropdown");
						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Business 1"),"SelectValueDropdown");
						Reusable.waitForSiebelLoader();					
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Dest_IPv4_Add_Business1,"TextInputblank");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Destination IPv4 Address value enetered");
						Reusable.waitForAjax();
						
						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Sour_IPv4_Add_Business1,"TextInputblank");
						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
						Reusable.waitForAjax();	
						

						//Log.info("added 'Business 1' address marking");
					}
//					if(b2)
//					{
//						clickCheckBox(SiebelOrderObj.SiebelOrder.addressmarkingadd,"CHECK",true);
//						Reusable.waitForSiebelLoader();
//						
//						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"TCP","protocolinput");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
//						Reusable.waitForAjax();	
//					
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Inputdata174,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Inputdata175,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//
//						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
//						Reusable.waitForAjax();	
//						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Business 2","Business 2");
//						Reusable.waitForAjax();	
//						
//						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Business 2"),"SelectValueDropdown");
//						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Business 2"),"SelectValueDropdown");
//						Reusable.waitForSiebelLoader();					
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Inputdata176,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Inputdata177,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();	
//						
//					//	Log.info("added 'Business 2' address marking");
//					}
//					if(b3)
//					{
//						clickCheckBox(SiebelOrderObj.SiebelOrder.addressmarkingadd,"CHECK",true);
//						Reusable.waitForSiebelLoader();
//						
//						clearTextBox(SiebelOrderObj.SiebelOrder.protocolinput);
//						Reusable.waitForAjax();	
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.protocolinput,"TCP","protocolinput");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.protocolinput, Keys.ENTER);
//						Reusable.waitForAjax();	
//					
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"),Inputdata178,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"),Inputdata179,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Netmask"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						clearTextBox(SiebelOrderObj.SiebelOrder.classinput);
//						Reusable.waitForAjax();	
//						sendKeys(SiebelOrderObj.SiebelOrder.classinputa,"Business 3","Business 3");
//						Reusable.waitForAjax();	
//						
//						verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value", "Business 3"),"SelectValueDropdown");
//						click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value","Business 3"),"SelectValueDropdown");
//						Reusable.waitForSiebelLoader();					
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"),Inputdata180,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Destination IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();
//						
//						sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"),Inputdata181,"TextInputblank");
//						Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Source IPv4 Address"), Keys.ENTER);
//						Reusable.waitForAjax();	
//						
//											//	Log.info("added 'Business 3' address marking");
//					}

					else
					{
						System.out.println("no additon IPv4 Address Based Marking added");
					}
				}
			}

			if (NAT_Feature.equals("Yes")) //if Static NAT is yes
			{
				javaScriptclick(SiebelOrderObj.SiebelOrder.StaticNAT);

				Reusable.waitForAjax();	
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Inside IPv4 Address"),Inside_IPv4_Addr,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Inside IPv4 Address"), Keys.ENTER);
				Reusable.waitForAjax();


				sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Outside IPv4 Address"),Outside_IPv4_Addr,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Outside IPv4 Address"), Keys.ENTER);
				Reusable.waitForAjax();
			
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Sub Net Mask"),Subnet_Mask,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Sub Net Mask"), Keys.ENTER);
				Reusable.waitForAjax();

				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}

				sendKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Rule Number"),Subnet_Mask,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","Rule Number"), Keys.ENTER);
				Reusable.waitForAjax();
			}

			Reusable.waitForSiebelLoader();
			if(findWebElement(SiebelOrderObj.SiebelOrder.Close).isDisplayed())
			{
			javaScriptclick(SiebelOrderObj.SiebelOrder.Close);
			}
			else if(findWebElement(SiebelOrderObj.SiebelOrder.CloseIPVpnNRComponent).isDisplayed())
			{
			javaScriptclick(SiebelOrderObj.SiebelOrder.CloseIPVpnNRComponent);
			}
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					Reusable.waitForAjax();
					Reusable.waitForSiebelLoader();
					Reusable.waitForpageloadmask();
				}
			}
		}
	}
	
	public void addFeatureIpvnsiteLeft(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String Layer3_Resilience = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String CPE_IPv4_Add = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"CPE IPv4 Add");
		String IPv4_Prefix = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"IPv4 Prefix");
		String PEIPv4Add = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"PE_IPv4_Add");
		String PE_NetworkElement = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"A end Access NW Element");
		String LAN_VLAN = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN VLAN");
		String LAN_Inter_IPv4_Add = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN Inter IPv4 Add");
		String LAN_Inter_IPv4_Prefix = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN Inter IPv4 Prefix");
		String LAN_VRRP_IPv4_Add_1 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN VRRP IPv4 Add 1");
		String LAN_VRRP_IPv4_Add_2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN_VRRP_IPv4_Add 2");//Missing - Ignored
		String Second_CPE_LAN_IPv4_Add = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Second CPE LAN IPv4 Add");
		String LAN_Inter_IPv4_Add_2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN Inter IPv4 Add-2");
		String LAN_VRRP_IPv4_Add_1_2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN VRRP IPv4 Add 1-2");
		String Second_CPE_LAN_IPv4_Add_2 = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Second CPE LAN IPv4 Add-2");
		String Count_of_LAN_Interface_ipv4_address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Count of LAN Interface ipv4 address");
		String Static_BGP_Blank_NA = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN Routing Protocol");
		String Target_Network = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Target_Network");
		String Next_Hop_IP = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Next_Hop_IP");
		String Subnet_Mask = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Subnet_Mask");
		String LNP_Ipv4_Peer_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Ipv4 Peer Address");
		String LNP_Ipv6_Peer_Address = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Ipv6 Peer Address");
		String WAN_bgp_AS_no = DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"WAN bgp-AS no");
		String MD5_Pwd= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"MD5 Pwd");
		String HoldTimer= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Hold Timer(s)");
		String KeepAlive= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Keep Alive(s)");
		String WAN_Routing_protocol= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"WAN Routing protocol");
		String Additional_LAN_Routing_static_ipv4_count= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Additional LAN Routing static ipv4 -count");
		
		
		Reusable.waitForSiebelLoader();
		javaScriptclick(SiebelOrderObj.SiebelOrder.IPDetails);

		javaScriptclick(SiebelOrderObj.SiebelOrder.ChildIPDetails);

		Reusable.waitForSiebelLoader();

		if (Layer3_Resilience.equals("No Resilience")) 
		{
			//to fill WAN Addressing
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","CPE IPv4 Address"),CPE_IPv4_Add,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInputblank.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv4 Prefix"),IPv4_Prefix,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv4 Prefix"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","PE IPv4 Address"),PEIPv4Add,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","PE IPv4 Address"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","PE Name"),PE_NetworkElement,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","PE Name"), Keys.ENTER);
			Reusable.waitForAjax();
			
			if (LAN_VLAN.contains("Yes"))
			{
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Address"),LAN_Inter_IPv4_Add,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Address"), Keys.ENTER);
				Reusable.waitForAjax();
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Address"),LAN_Inter_IPv4_Prefix,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Address"), Keys.ENTER);
				Reusable.waitForAjax();
			}
		}
		if (Layer3_Resilience.equals("Dual Access Primary & Backup")  || (Layer3_Resilience.contains("Dual Access Load Shared"))) 
		{
			//to fill WAN Addressing
			scrollDown(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","CPE IPv4 Address"));

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","CPE IPv4 Address"),CPE_IPv4_Add,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","IPv4 Prefix"),IPv4_Prefix,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","IPv4 Prefix"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","PE IPv4 Address"),PEIPv4Add,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","PE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","PE Name"),PE_NetworkElement,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","PE Name"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","PE IPv6 Address"),"2001:0920:3024:0000:0000:0000:0000:0001","TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput1.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","CPE IPv4 Address"),CPE_IPv4_Add,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","IPv4 Prefix"),IPv4_Prefix,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","IPv4 Prefix"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","PE IPv4 Address"),PEIPv4Add,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","PE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","PE Name"),PE_NetworkElement,"TextInputblank");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","PE Name"), Keys.ENTER);
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","CPE IPv6 Address"),"2001:0922:3023:0000:0000:0000:0000:0000","TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","IPv6 Prefix"),"56","TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","PE IPv6 Address"),"2001:0922:3025:0000:0000:0000:0000:0001","TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput2.replace("Value","CPE IPv4 Address"), Keys.ENTER);
			Reusable.waitForSiebelLoader();

//			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Address"),LAN_Inter_IPv4_Add,"TextInputblank");
//			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Address"), Keys.ENTER);
//			Reusable.waitForSiebelLoader();
//			
//			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Prefix"),LAN_Inter_IPv4_Prefix,"TextInputblank");
//			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN Interface IPv4 Prefix"), Keys.ENTER);
//			Reusable.waitForSiebelLoader();
//			
//			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN VRRP IPv4 Address 1"),LAN_VRRP_IPv4_Add_1,"TextInputblank");
//			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN VRRP IPv4 Address 1"), Keys.ENTER);
//			Reusable.waitForSiebelLoader();
//			
//			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN VRRP IPv4 Address 2"),LAN_VRRP_IPv4_Add_2,"TextInputblank");
//			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","LAN VRRP IPv4 Address 2"), Keys.ENTER);
//			Reusable.waitForSiebelLoader();
//			
//			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Second CPE LAN Interface IPv4 Address"),Second_CPE_LAN_IPv4_Add,"TextInputblank");
//			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Second CPE LAN Interface IPv4 Address"), Keys.ENTER);
//			Reusable.waitForSiebelLoader();
//			
//			click(SiebelOrderObj.SiebelOrder.PrimaryRange);

			Reusable.waitForAjax();

			if(isElementPresent(By.xpath("//*[contains(text(),'Click here to')]")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
				{
					verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
					Reusable.waitForAjax();
					Reusable.waitForSiebelLoader();
					Reusable.waitForpageloadmask();
				}
			}
		}	

		//select features from Left side show full info
		javaScriptclick(SiebelOrderObj.SiebelOrder.ShowInfoLeft);

		Reusable.waitForAjax();
		if (Count_of_LAN_Interface_ipv4_address.toString().equals("2"))//if additional LAN interface ipv3 required Required (max 2 is implemented)
		{
			javaScriptclick(SiebelOrderObj.SiebelOrder.LanInterfaceIPv42);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","LAN Interface IPv4 Address"),LAN_Inter_IPv4_Add_2,"TextInput3");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","LAN Interface IPv4 Address"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","LAN Interface IPv4 Prefix"),LAN_Inter_IPv4_Prefix,"TextInput3");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","LAN Interface IPv4 Prefix"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","LAN VRRP IPv4 Address 1"),LAN_VRRP_IPv4_Add_1_2,"TextInput3");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","LAN VRRP IPv4 Address 1"), Keys.ENTER);
			Reusable.waitForAjax();

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","Second CPE LAN Interface IPv4 Address"),Second_CPE_LAN_IPv4_Add_2,"TextInput3");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput3.replace("Value","Second CPE LAN Interface IPv4 Address"), Keys.ENTER);
			Reusable.waitForAjax();			
		}
		// To fill LAN Routing
		if (Static_BGP_Blank_NA.contains("Static") || Static_BGP_Blank_NA.toString().contains("BGP")) 
		{
			Reusable.waitForSiebelLoader();
			javaScriptclick(SiebelOrderObj.SiebelOrder.LANRouting);
			Reusable.waitForAjax();		
			javaScriptclick(SiebelOrderObj.SiebelOrder.ClickDropdown.replace("Value", "Routing Protocol"));
			Reusable.waitForAjax();			
			verifyExists(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Static_BGP_Blank_NA),"SelectValueDropdown");
			click(SiebelOrderObj.SiebelOrder.SelectValueDropdown.replace("Value",Static_BGP_Blank_NA),"SelectValueDropdown");
			Reusable.waitForAjax();			

			if (Static_BGP_Blank_NA.contains("Static"))
			{
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Next Hop IP"),Next_Hop_IP,"TextInput");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Next Hop IP"), Keys.ENTER);
				Reusable.waitForAjax();
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Sub Net Mask"),Subnet_Mask,"TextInput");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Sub Net Mask"), Keys.ENTER);
				Reusable.waitForAjax();
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Target Network"),Target_Network ,"TextInput");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Target Network"), Keys.ENTER);
				Reusable.waitForAjax();
				
				if (Additional_LAN_Routing_static_ipv4_count.contains("2"))
				{
					javaScriptclick(SiebelOrderObj.SiebelOrder.StaticIPV4Route);

					Reusable.waitForAjax();
					Reusable.waitForSiebelLoader();
					
					sendKeys(SiebelOrderObj.SiebelOrder.NextHopIp,Next_Hop_IP,"NextHopIp");
					Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.NextHopIp, Keys.ENTER);
					Reusable.waitForAjax();
					
					sendKeys(SiebelOrderObj.SiebelOrder.SubNetMask,Subnet_Mask,"SubNetMask");
					Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.SubNetMask, Keys.ENTER);
					Reusable.waitForAjax();
					
					sendKeys(SiebelOrderObj.SiebelOrder.TargetNet,Target_Network,"TargetNet");
					Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TargetNet, Keys.ENTER);
					Reusable.waitForAjax();
					
				}
			}
			else if(Static_BGP_Blank_NA.equals("BGP")) //LAN BGP	
			{
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv4 Peer Address"),LNP_Ipv4_Peer_Address,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv4 Peer Address"), Keys.ENTER);
				Reusable.waitForAjax();
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","MD5 Password"),MD5_Pwd,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","MD5 Password"), Keys.ENTER);
				Reusable.waitForAjax();
				
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Peer AS Number"),WAN_bgp_AS_no,"TextInputblank");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Peer AS Number"), Keys.ENTER);
				Reusable.waitForAjax();
				
				/*
				 * sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Hold Timer"), Inputdata84,"TextInputblank");
				 * Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Hold Timer"), Keys.ENTER);
				 * Reusable.waitForAjax();
				 * sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Keep Alive"),"10","TextInputblank");
				 * Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Keep Alive"), Keys.ENTER);
				 * Reusable.waitForAjax();
				 */
			}
		}
		// To fill WAN Routing
		if (WAN_Routing_protocol.equals("BGP")) //For wan BGP Fature
		{
			javaScriptclick(SiebelOrderObj.SiebelOrder.WANBGP);

			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv4 Peer Address"),LNP_Ipv4_Peer_Address,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv4 Peer Address"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv6 Peer Address"),LNP_Ipv6_Peer_Address,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","IPv6 Peer Address"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","MD5 Password"),MD5_Pwd,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","MD5 Password"), Keys.ENTER);
			Reusable.waitForAjax();
			
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Peer AS Number"),WAN_bgp_AS_no,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Peer AS Number"), Keys.ENTER);
			Reusable.waitForAjax();
		
			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Hold Timer"),HoldTimer,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Hold Timer"), Keys.ENTER);
			Reusable.waitForAjax();

			sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Keep Alive"),KeepAlive,"TextInput");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","Keep Alive"), Keys.ENTER);
			Reusable.waitForAjax();

			if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
			{
				if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
				{
					click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
					Reusable.waitForSiebelLoader();
				}
			}
		}
		if(findWebElement(SiebelOrderObj.SiebelOrder.Close).isDisplayed())
		{
		javaScriptclick(SiebelOrderObj.SiebelOrder.Close);
		}
		else if(findWebElement(SiebelOrderObj.SiebelOrder.CloseIPVpnNRComponent).isDisplayed())
		{
		javaScriptclick(SiebelOrderObj.SiebelOrder.CloseIPVpnNRComponent);
		}
		Reusable.waitForAjax();

//		waitForElementToAppear(SiebelOrderObj.SiebelOrder.SaveButton,10);
		if(verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton))
		{
			if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
			{
				verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
				click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
				Reusable.waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
			}
		}
	}

	public void addIpvpnIpdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Primary_Bandwidth= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"DummyHub_Bandwidth");
		String Router_Type= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Router_Type");
		String WAN_VLAN_ID= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"WAN VLAN ID");
		String Layer_3_Resilience= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String Backup_Bandwidth=DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
		String LAN_VLAN= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"LAN VLAN");
		String VLAN_Tag_ID= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tag_ID");//Add
		
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","IP Details"),"ClickLink");
		click(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","IP Details"),"ClickLink");

		Reusable.waitForpageloadmask();
		
		verifyExists(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","End Point VPN"),"Click Endpoint Link");
		click(SiebelOrderObj.SiebelOrder.ClickLink.replace("Value","End Point VPN"),"Click Endpoint Link");
		
		Reusable.waitForpageloadmask();
		
		verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.IPDetailsPlus,"IPDetailsPlus");
		click(SiebelModeObj.IPVPNSITEMiddleApplet.IPDetailsPlus,"IPDetailsPlus");

//		verifyExists(SiebelOrderObj.SiebelOrder.SubVPNID,"SubVPNID");
//		click(SiebelOrderObj.SiebelOrder.SubVPNID,"SubVPNID");
//		
//		Reusable.waitForAjax();
//		Reusable.waitForpageloadmask();
//		
//		verifyExists(SiebelOrderObj.SiebelOrder.SelectSubVPNList,"SelectSubVPNList");
//		click(SiebelOrderObj.SiebelOrder.SelectSubVPNList,"SelectSubVPNList");
//		
//		
//		verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
//		click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
//		
//		Reusable.waitForAjax();
//		Reusable.waitForpageloadmask();

		verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "VPN Bandwidth"),"DropDown");
		click(SiebelModeObj.IPVPNSITEMiddleApplet.ClickDropdown.replace("Value", "VPN Bandwidth"));
		
		verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Primary_Bandwidth),"Select Value Dropdown");
		click(SiebelModeObj.IPVPNSITEMiddleApplet.SelectValueDropdown.replace("Value", Primary_Bandwidth));
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();

		if (Router_Type.contains("Unmanaged"))
		{
			verifyExists(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "WAN VLAN ID"),"TextInput");
			sendKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "WAN VLAN ID"),WAN_VLAN_ID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNSITEMiddleApplet.TextInput.replace("Value", "WAN VLAN ID"),Keys.ENTER);

//			sendKeys(SiebelOrderObj.SiebelOrder.Wanvlanid1,WAN_VLAN_ID,"Wanvlanid1");//it should be more than 2
//			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.Wanvlanid1,Keys.TAB);

			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			
		}

		if (Layer_3_Resilience.contains("Dual"))
		{
			verifyExists(SiebelOrderObj.SiebelOrder.VpnBandwidthinput2,"DropDown");
			click(SiebelOrderObj.SiebelOrder.VpnBandwidthinput2, "VPN Bandwidth");
			
			waitForElementToAppear(SiebelOrderObj.SiebelOrder.VpnBandwidthinput2,10);
			clearTextBox(SiebelOrderObj.SiebelOrder.VpnBandwidthinput2);
			Reusable.waitForAjax();
			sendKeys(SiebelOrderObj.SiebelOrder.VpnBandwidthinput2,Primary_Bandwidth,"VpnBandwidthinput2");
			Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.VpnBandwidthinput2,Keys.ENTER);
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			//updated

			if (Router_Type.contains("Unmanaged"))
			{
				sendKeys(SiebelOrderObj.SiebelOrder.Wanvlanid2,WAN_VLAN_ID,"Wanvlanid2");//it should be more than 2
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.Wanvlanid2,Keys.TAB);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
			}
		}

		if(!LAN_VLAN.contains("No"))
		{
			if (LAN_VLAN.equals("1"))//if Multi Lan VLan Required
			{
				verifyExists(SiebelOrderObj.SiebelOrder.Physicalportidprimary,"Physicalportidprimary");
				click(SiebelOrderObj.SiebelOrder.Physicalportidprimary,"Physicalportidprimary");
				Reusable.waitForAjax();
				
				verifyExists(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				click(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				Reusable.waitForAjax();

				verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				if (Layer_3_Resilience.contains("Dual"))
				{
				verifyExists(SiebelOrderObj.SiebelOrder.PhysicalportidSecondary,"PhysicalportidSecondary");
				click(SiebelOrderObj.SiebelOrder.PhysicalportidSecondary,"PhysicalportidSecondary");
				Reusable.waitForAjax();
				
				verifyExists(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				click(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				Reusable.waitForAjax();

				verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				}

				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","VLAN Tag ID"),VLAN_Tag_ID,"TextInput");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.vlantagidinput,Keys.TAB);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}	
			}
			else if (LAN_VLAN.equals("2")) //if additional Lan VLan Required
			{
				verifyExists(SiebelOrderObj.SiebelOrder.Physicalportidprimary,"Physicalportidprimary");
				click(SiebelOrderObj.SiebelOrder.Physicalportidprimary,"Physicalportidprimary");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				click(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				if (Layer_3_Resilience.contains("Dual"))
				{
				verifyExists(SiebelOrderObj.SiebelOrder.PhysicalportidSecondary,"PhysicalportidSecondary");
				click(SiebelOrderObj.SiebelOrder.PhysicalportidSecondary,"PhysicalportidSecondary");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				click(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();			
				}
				sendKeys(SiebelOrderObj.SiebelOrder.TextInput.replace("Value","VLAN Tag ID"),VLAN_Tag_ID,"SubmitSubVPNList");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.vlantagidinput,Keys.TAB);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();	
				
				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}	
				
				javaScriptclick(SiebelOrderObj.SiebelOrder.MultiLanVlan);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();	
				
				verifyExists(SiebelOrderObj.SiebelOrder.SecPhysicalPortIdPrimary,"SecPhysicalPortIdPrimary");
				click(SiebelOrderObj.SiebelOrder.SecPhysicalPortIdPrimary,"SecPhysicalPortIdPrimary");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				click(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();	

				verifyExists(SiebelOrderObj.SiebelOrder.SecPhysicalPortIdSecondary,"SecPhysicalPortIdSecondary");
				click(SiebelOrderObj.SiebelOrder.SecPhysicalPortIdSecondary,"SecPhysicalPortIdSecondary");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				click(SiebelOrderObj.SiebelOrder.AccessPhysicalPort,"AccessPhysicalPort");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				click(SiebelOrderObj.SiebelOrder.SubmitSubVPNList,"SubmitSubVPNList");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();	
				
				
				sendKeys(SiebelOrderObj.SiebelOrder.SecTextInput.replace("Value","VLAN Tag ID"),VLAN_Tag_ID,"SecTextInput");
				Reusable.SendkeaboardKeys(SiebelOrderObj.SiebelOrder.vlantagidinput2,Keys.TAB);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();	

				if(isElementPresent(By.xpath("//span[text()='Ready For NC']/parent::div/input[@aria-checked='N']")))
				{
					if(findWebElement(SiebelOrderObj.SiebelOrder.readyforNCcheckbox).isDisplayed())
					{
						click(SiebelOrderObj.SiebelOrder.readyforNCcheckbox);
						Reusable.waitForSiebelLoader();
					}
				}	
			}
			else
			{
				System.out.println("More than  2 LAN VLAN doesn't support! code not implemented or please provide proper value - minimum LAn VLAN mandatory in Siebel!");
			}
		}

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		if(verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton))
		{
			if(findWebElement(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton).isDisplayed())
			{
				verifyExists(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
				click(SiebelOrderObj.SiebelOrder.SaveProductConfiguraton,"SaveProductConfiguraton");
				
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				Reusable.waitForAjax();

			}
		}
	}
	
	public void SubmitIpvpnOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String PrimaryAccessType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Access_Type/ Primary");
		String SecondaryAccessType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Access_Type/Secondary");
		String Layer3Resilience= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String PrimarySecondary= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary/Secondary");
		String TypeofOrder= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
		
		if (!PrimaryAccessType.toString().contains("Leased Line"))
		{
			Reusable.waitForAjax();
			verifyExists(SiebelOrderObj.SiebelOrder.SubmitTechnicalOrder,"Submit Technical Order");
			click(SiebelOrderObj.SiebelOrder.SubmitTechnicalOrder,"Submit Technical Order");

			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			
			Thread.sleep(360000);

			click(SiebelOrderObj.SiebelOrder.SitesTab,"TechnicalOrderTab");
			Reusable.waitForpageloadmask();
			
			if(isElementPresent(By.xpath("//span[text()='Ok']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
			}
			Reusable.waitForSiebelLoader();
			
			if (TypeofOrder.contains("Carnor"))
			{
				AddProduct.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"Carnor");
			}
			else
			{
				AddProduct.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"New");
			}
			
			waitForElementToAppear(SiebelOrderObj.SiebelOrder.CircuitReference,30);
			String PrimCircuitReferenceNumber = getAttributeFrom(SiebelOrderObj.SiebelOrder.CircuitReferenceValue,"value");
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Circuitreference_Number", PrimCircuitReferenceNumber);
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelOrderObj.SiebelOrder.TechnicalOrderTab,"Technical Order Tab");
			click(SiebelOrderObj.SiebelOrder.TechnicalOrderTab,"TechnicalOrderTab");

			verifyExists(SiebelOrderObj.SiebelOrder.Primarycomp2,"Site TechnicalOrder Serviceid");
			String PrimComp2TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Primarycomp2);
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalOrderServiceid", PrimComp2TechnicalServiceid);
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelOrderObj.SiebelOrder.Primarycomp3,"Ednpoint TechnicalOrder Serviceid");
			String PrimComp3TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Primarycomp3);
			DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Comp3TechnicalServiceid_Prim", PrimComp3TechnicalServiceid);
			Reusable.waitForSiebelLoader();
//			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelOrderObj.SiebelOrder.SitesTab,"Technical Order Tab");
			click(SiebelOrderObj.SiebelOrder.SitesTab,"TechnicalOrderTab");
			Reusable.waitForpageloadmask();
		}
		if ((Layer3Resilience.toString().contains("Dual")) && (PrimarySecondary.toString().contains("Secondary")))
		{
			if (!SecondaryAccessType.toString().contains("Leased Line"))
			{
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelOrderObj.SiebelOrder.SecondarySite,"Secondary Site");
				click(SiebelOrderObj.SiebelOrder.SecondarySite,"Secondary Site");	
//				Reusable.waitForSiebelSpinnerToDisappear();

				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(SiebelOrderObj.SiebelOrder.SubmitTechnicalOrder,10);
				verifyExists(SiebelOrderObj.SiebelOrder.SubmitTechnicalOrder,"Submit Technical Order");
				click(SiebelOrderObj.SiebelOrder.SubmitTechnicalOrder,"Submit Technical Order");

				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();
				Reusable.waitForpageloadmask();

				Thread.sleep(360000);

				click(SiebelOrderObj.SiebelOrder.SitesTab,"TechnicalOrderTab");
				Reusable.waitForpageloadmask();

				if(isElementPresent(By.xpath("//span[text()='Ok']"))) 
				{
					verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
					click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
				}
				Reusable.waitForSiebelLoader();

				if (TypeofOrder.contains("Carnor"))
				{
					AddProduct.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"Carnor");
				}
				else
				{
					AddProduct.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"New");
				}

				verifyExists(SiebelOrderObj.SiebelOrder.SecondarySite,"Secondary Site");
				click(SiebelOrderObj.SiebelOrder.SecondarySite,"Secondary Site");	

				waitForElementToAppear(SiebelOrderObj.SiebelOrder.CircuitReference,30);
				String SecondCircuitRefNumber = getAttributeFrom(SiebelOrderObj.SiebelOrder.CircuitReferenceValue,"value");
				DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "CircuitReferencenumber_Spoke", SecondCircuitRefNumber);
				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebelOrderObj.SiebelOrder.TechnicalOrderTab,"Technical Order Tab");
				click(SiebelOrderObj.SiebelOrder.TechnicalOrderTab,"TechnicalOrderTab");

				verifyExists(SiebelOrderObj.SiebelOrder.Secondarycomp2,"TechnicalOrder Serviceid");
				String SecondComp2TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Secondarycomp2);
				DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalServiceid_Spoke", SecondComp2TechnicalServiceid);
				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebelOrderObj.SiebelOrder.Secondarycomp3,"TechnicalOrder Serviceid");
				String SecondComp3TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Secondarycomp3);
				DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Comp3TechnicalServiceid_Second", SecondComp3TechnicalServiceid);
				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebelOrderObj.SiebelOrder.SitesTab,"Technical Order Tab");
				click(SiebelOrderObj.SiebelOrder.SitesTab,"TechnicalOrderTab");
				Reusable.waitForpageloadmask();

				Reusable.waitForSiebelLoader();
				verifyExists(SiebelOrderObj.SiebelOrder.PrimarySite,"Primary Site");
				click(SiebelOrderObj.SiebelOrder.PrimarySite,"Primary Site");	
			}
		}
	}
	
		public void SyncIpvpnOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
		String PrimaryAccessType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Aend_Access_Type/ Primary");
		String SecondaryAccessType= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Bend_Access_Type/Secondary");
		String Layer3Resilience= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String PrimarySecondary= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Primary/Secondary");
		String TypeofOrder= DataMiner.fngetcolvalue(sheetName, scriptNo, dataSetNo,"Comments");
		if (!PrimaryAccessType.toString().contains("Leased Line"))
		{
		Reusable.waitForAjax();
		verifyExists(SiebelOrderObj.SiebelOrder.SyncTechnicalorder,"Sync Technical Order");
		click(SiebelOrderObj.SiebelOrder.SyncTechnicalorder,"Sync Technical Order");
	
		 Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Thread.sleep(360000);
		if (verifyExists(SiebelLiabrary_Obj.serviceOrder.Error))
		{
		Reusable.waitForSiebelLoader();
		}
		else
		{
		click(SiebelOrderObj.SiebelOrder.SitesTab,"TechnicalOrderTab");
		Reusable.waitForpageloadmask();
		if(isElementPresent(By.xpath("//span[text()='Ok']")))
		{
		verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
		click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
		}
		Reusable.waitForSiebelLoader();
		if (TypeofOrder.contains("Carnor"))
		{
		AddProduct.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"Carnor");
		}
		else
		{
		AddProduct.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo,"New");
		}
		waitForElementToAppear(SiebelOrderObj.SiebelOrder.CircuitReference,30);
		String PrimCircuitReferenceNumber = getAttributeFrom(SiebelOrderObj.SiebelOrder.CircuitReferenceValue,"value");
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Circuitreference_Number", PrimCircuitReferenceNumber);
		Reusable.waitForSiebelLoader();
		// Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelOrderObj.SiebelOrder.TechnicalOrderTab,"Technical Order Tab");
		click(SiebelOrderObj.SiebelOrder.TechnicalOrderTab,"TechnicalOrderTab");
	
		 verifyExists(SiebelOrderObj.SiebelOrder.Primarycomp2,"Site TechnicalOrder Serviceid");
		String PrimComp2TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Primarycomp2);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "TechnicalOrderServiceid", PrimComp2TechnicalServiceid);
		Reusable.waitForSiebelLoader();
		// Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelOrderObj.SiebelOrder.Primarycomp3,"Ednpoint TechnicalOrder Serviceid");
		String PrimComp3TechnicalServiceid = getTextFrom(SiebelOrderObj.SiebelOrder.Primarycomp3);
		DataMiner.fnsetcolvalue(sheetName, scriptNo, dataSetNo, "Comp3TechnicalServiceid_Prim", PrimComp3TechnicalServiceid);
		Reusable.waitForSiebelLoader();
		// Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelOrderObj.SiebelOrder.SitesTab,"Technical Order Tab");
		click(SiebelOrderObj.SiebelOrder.SitesTab,"TechnicalOrderTab");
		Reusable.waitForpageloadmask();
		}
		}
		}
}

	