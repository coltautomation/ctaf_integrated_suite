package testHarness.siebeltoNcFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelManualValidationObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelManualValidation extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void clickOnManualValidationB() throws Exception {
		Reusable.waitForSiebelLoader();
		try {
			//if (isElementPresent(By.xpath("//button[@aria-label='Service Orders:Manual Validation' and not(@disabled='disabled')]"))) {
			if (waitForElementToBeVisible(SiebelManualValidationObj.Validation.manualvalidation2,60)) {
				verifyExists(SiebelManualValidationObj.Validation.manualvalidation2,"Manual Validation button");
				ScrollIntoViewByString(SiebelManualValidationObj.Validation.manualvalidation2);
				click(SiebelManualValidationObj.Validation.manualvalidation2,"Manual Validation button");
				Reusable.waitForSiebelLoader();
			}
			else 
			{
			//	System.out.println("Manual validation button is not present");
			}
		} catch (Exception e) 
		{
			System.out.println("Manual validation button is not present in catch block");
		}
//		Reusable.waitToPageLoad();
		Reusable.waitForAjax();
	}
	
	public void clickOnManualValidationA() throws Exception {
		Reusable.waitForSiebelLoader();
		Reusable.alertPopUp();
		//if (isElementPresent(By.xpath("//button[@aria-label='Service Orders:Manual Validation' and not(@disabled='disabled')]"))) {
		if (waitForElementToBeVisible(SiebelManualValidationObj.Validation.manualvalidation1,60)) {
			Reusable.alertPopUp();
			verifyExists(SiebelManualValidationObj.Validation.manualvalidation1,"Manual Validation button");
			ScrollIntoViewByString(SiebelManualValidationObj.Validation.manualvalidation1);
			click(SiebelManualValidationObj.Validation.manualvalidation1,"Manual Validation button");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
//			Reusable.waitToPageLoad();
			Reusable.waitForAjax();
		}
	}
}
