package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.siebelFunctions.SiebelLibrary;

public class TS_03_Onnet_Order_Completion extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	SiebelLibrary Siebel_Library = new SiebelLibrary();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testOnnetOrderCompletion(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			//C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))   
            {
				Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
            }
			else
			{
            	Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
            }
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo); 
			
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID");
			
									
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
				
				AddProduct.AddProductToQuote(ProductName);
//				calling the below method to enter the site address
				 AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
								
//				calling the below method to configure the product
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					
			} else {
//				Calling the below method to add the product in CPQ
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
			
			}	
			
			String Flow_Type=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
			if (Flow_Type.equalsIgnoreCase("OnnetDualEntry")) {
				
						AddProduct.verifyQuoteStage("Waiting for BCP");
						CPQLogin.CPQLogout();
						
//						Login On Explore
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						
//				calling the below method to configure the product
						sResult = ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
						
//						Calling the Below method to perform C4C Login	
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);
						
				//		calling the below method reconfigure the product
						sResult = AddProduct.clickProductConfigurationBtn(ProductName);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to update and save the product in cpq
						sResult = AddProduct.updateSaveProductCPQ("Save");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					}
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage1 = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage1);
			
			if (quoteStage1.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		           
				//Login with CPQ SE user
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				//Approvals.SwitchCPQUser(testDataFile, "Configuration.User_Type", Quote_ID);
				
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					CPQLogin.CPQLogout();
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
				}
				
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
				
			}
			AddProduct.verifyQuoteStage("Priced");
			String HubId = AddProduct.addHubrefernce(ProductName);
			if (HubId.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			if(HubId.equals("True"))DataMiner.fnsetcolvalue(dataSheet, scriptNo, dataSetNo, "Hub_Reference", HubId);
			
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
			String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
			
			if(ProductName.equalsIgnoreCase("CpeSolutionsSite"))
			{
				
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
				}
				else
				{ Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
			} 
			else {	
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
				}
				else
				{ Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }
			}
			

			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				if(ProductName.equalsIgnoreCase("ColtIpAccess")&&Flow_Type.equalsIgnoreCase("DC")) 
				{
					AddProduct.uploadFilesAttachmentTab(ProductName, "EOF(xls)", "", 1, "Quote_Level");
					AddProduct.uploadFilesAttachmentTab(ProductName, "Confidential", "", 1, "Quote_Level");
				}
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
							
			}
			else 
			{
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.SubmitOrder();
			

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	

}
