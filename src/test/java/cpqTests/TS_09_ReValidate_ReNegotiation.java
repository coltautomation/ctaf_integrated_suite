package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_09_ReValidate_ReNegotiation extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testReValidateReNegotiation(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			String sResult;
//			if(ProductName.equalsIgnoreCase("Wave")) {
//				
//				sResult = AddProduct.verifyQuoteStage("Priced");
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//					
//				String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
//				sResult = Approvals.SwitchCPQUser(testDataFile, "SE_User", Quote_ID);
//
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//					
//				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
//
//				sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//					
//				sResult = AddProduct.updateSaveProductCPQ("Save");
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
//					
//			}
			
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			AddProduct.verifyQuoteStage("Waiting for 3rd Party");
			CPQLogin.CPQLogout();
			
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.EXPLORE_URL);
			
			sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
		
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			Login.C4CLogin(testDataFile, Configuration.User_Type);
			AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
				
			sResult = AddProduct.clickProductConfigurationBtn(ProductName);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			sResult = AddProduct.updateSaveProductCPQ("Save");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

			sResult = AddProduct.verifyQuoteStage("Priced");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
//			if(ProductName.equalsIgnoreCase("Wave")) 
//			{
//				sResult = Approvals.SwitchCPQUser(testDataFile, "SE_User", Quote_ID);
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//				
//			}
			sResult = AddProduct.clickProductConfigurationBtn(ProductName);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			sResult = AddProduct.reValidatereNegotiateQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }

			sResult = AddProduct.updateSaveProductCPQ("Save");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			AddProduct.verifyQuoteStage("Waiting for 3rd Party");
			CPQLogin.CPQLogout();
			
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.EXPLORE_URL);
			sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
		
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			Login.C4CLogin(testDataFile, Configuration.User_Type);
			AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);	

			sResult = AddProduct.clickProductConfigurationBtn(ProductName);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

			sResult = AddProduct.updateSaveProductCPQ("Save");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
		
			String quoteStage1 = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage1);
			
			if (quoteStage1.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		           
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					CPQLogin.CPQLogout();
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
				}
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
			}

			sResult = AddProduct.verifyQuoteStage("Priced");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
		
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
            String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
           
            if(ProductName.equalsIgnoreCase("CpeSolutionsSite"))
            {
               
                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
                }
                else
                { Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
            }
            else {   
                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
                }
                else
                { Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }
            }

			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
							
			}
			else 
			{
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	
}
