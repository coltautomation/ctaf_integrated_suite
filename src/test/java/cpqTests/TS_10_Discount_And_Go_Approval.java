package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_10_Discount_And_Go_Approval extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage LoginC4C = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
    GlobalVariables g = new GlobalVariables();
    RecordVideo record = new RecordVideo();
    
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testDiscountAndGoApproval(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName)
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");		
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
		    openurl(Configuration.C4C_URL);
			LoginC4C.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
	
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");			
			AddProduct.AddProductToQuote(ProductName);
			String Product_Type = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Product_Type");
			String Network_Reference = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Network_Reference");
			if (Product_Type.equalsIgnoreCase("Addon")) {
				DataMiner.fnsetcolvalue( dataSheet, scriptNo, dataSetNo, "Related_Network_Reference", Network_Reference);	
				AddProduct.ipAddonProductConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			} 
			else {
				AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			String Quote_ID=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID");
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			
			String quoteStage1 = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage1);
			
			if (quoteStage1.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);

				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					CPQLogin.CPQLogout();
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
				}
				
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				LoginC4C.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
				
			}
			
			String NRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "MRC");
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("To be Priced")) 
			{
				AddProduct.editEthernetProductConfiguration(); 
				Approvals.SwitchCPQUser(testDataFile, "AD_User", Quote_ID);
				AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.verifyQuoteStage("Portfolio Pricing Review");
				AddProduct.clickProductConfigurationBtn(ProductName);
				AddProduct.UpdateBasePriceException(NRC, MRC);
				AddProduct.ClickSendToSales();

				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				LoginC4C.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
			}
						
			AddProduct.verifyQuoteStage("Priced");
			AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
			String CAM_Approve=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "CAM_Approve");
				
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User")&&(CAM_Approve.equalsIgnoreCase("Yes")))	
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Discount");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username,Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				AddProduct.SubmitForApproval();
				
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				LoginC4C.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);	
			}
			else 
			{
				 AddProduct.SubmitForApproval();
			}
		
			String Discount_Percentage = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Percentage");
			String Revise_Quote=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Revise_Quote");
			if(Revise_Quote.equalsIgnoreCase("Yes"))
			{			
				if (Integer.parseInt(Discount_Percentage) > 5) {
					AddProduct.verifyQuoteStage("Pending Governance Approval");
				if (Integer.parseInt(Discount_Percentage) > 10) {
						
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
					Approvals.SwitchCPQUser(testDataFile, "VPSales1_User", Quote_ID);
				}
				else {
					
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
					Approvals.SwitchCPQUser(testDataFile, "VPSales2_User", Quote_ID);	
				}
				AddProduct.quoteDiscountGovernanceReject("Reject");
				AddProduct.verifyQuoteStage("Approval Denied");

				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				LoginC4C.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
                AddProduct.verifyQuoteStage("Approval Denied");
                AddProduct.reviseQuote();	
    			AddProduct.verifyQuoteStage("Priced");
    			AddProduct.SubmitForApproval();
    			
    			if (Integer.parseInt(Discount_Percentage) > 5) {
					AddProduct.verifyQuoteStage("Pending Governance Approval");
				if (Integer.parseInt(Discount_Percentage) > 10) {
						
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
					Approvals.SwitchCPQUser(testDataFile, "VPSales1_User", Quote_ID);
				}
				else {
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
					Approvals.SwitchCPQUser(testDataFile, "VPSales2_User", Quote_ID);
					}		
				AddProduct.quoteDiscountGovernanceApprove("Approve");
				AddProduct.verifyQuoteStage("Approved");
		
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				LoginC4C.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
    			}	
				}
				
				else {
					AddProduct.verifyQuoteStage("Approved");				
					}
			}
			else
			{
					if (Integer.parseInt(Discount_Percentage) > 5) {
					AddProduct.verifyQuoteStage("Pending Governance Approval");
					if (Integer.parseInt(Discount_Percentage) > 10) {		
						Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
						Approvals.SwitchCPQUser(testDataFile, "VPSales1_User", Quote_ID);
					}
					else {
						
						Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
						Approvals.SwitchCPQUser(testDataFile, "VPSales2_User", Quote_ID);
					}
					
					AddProduct.quoteDiscountGovernanceApprove("Approve");
					AddProduct.verifyQuoteStage("Approved");

					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.C4C_URL);
					LoginC4C.C4CLogin(testDataFile, Configuration.User_Type);
					AddProduct.navigateQuotesFromHomepage();
					AddProduct.searchQuoteC4C(Quote_ID);             
	                	
				}   else {
					AddProduct.verifyQuoteStage("Approved");
					
				}
			}
			
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);			
			
			
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
            String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
           
            if(ProductName.equalsIgnoreCase("CpeSolutionsSite"))
            {
               
                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
                }
                else
                { Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
            }
            else {   
                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
                }
                else
                { Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }
            }
            Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

			}
			else 
			{
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			if (!Product_Type.equalsIgnoreCase("Addon")) {
		}
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.SubmitOrder();
	
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	
}
