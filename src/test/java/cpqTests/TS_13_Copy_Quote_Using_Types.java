package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_13_Copy_Quote_Using_Types extends SeleniumUtils
{

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testCopyQuoteUsingTypes(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName)
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");		
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
			AddProduct.AddProductToQuote(ProductName);
			}
			String Product_Type=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
	
			
			if (Product_Type.equalsIgnoreCase("Addon")) {
				AddProduct.ipAddonProductConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);				
				}
			else if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
				
			}else {
				AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);				
				}			
			
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectParentQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					
				}
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectParentQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					CPQLogin.CPQLogout();
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectParentQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
				}
				
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
			}
		
			String NRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "MRC");
            verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
            quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
            System.out.println("quoteStage: "+quoteStage);
            if (quoteStage.equalsIgnoreCase("To be Priced"))
            {
                AddProduct.editEthernetProductConfiguration();
                Approvals.SwitchCPQUser(testDataFile, "AD_User", Quote_ID);
                AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
                AddProduct.verifyQuoteStage("Portfolio Pricing Review");
                AddProduct.clickProductConfigurationBtn(ProductName);
                
                if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
                AddProduct.UpdateBasePriceException(NRC, MRC);
                AddProduct.ClickSendToSales();
                } else {
              
                	AddProduct.BasePriceException(ProductName,NRC, MRC);
                }
            	quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
            }
			
			AddProduct.verifyQuoteStage("Priced");
			
			String Copy_Type=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Copy_Type");
			AddProduct.copyQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				 if (ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
					 
					 AddProduct.CPEModify("CPESolutionsSite"); 
					 AddProduct.cpeSiteConfigurationMulti(testDataFile, dataSheet, scriptNo, dataSetNo);
					 AddProduct.saveCPQ("Main");
				 }
		
				String sProduct = ProductName.replaceAll("(?!^)([A-Z])", " $1");
				String bProduct_RowNumber = null; String aProduct_RowNumber = null;
				if (Copy_Type.equalsIgnoreCase("Lineitem")) {
					bProduct_RowNumber = Reusable.MultiLineWebTableCellAction("Product", sProduct, "","GetRow", null,1);
					aProduct_RowNumber = Reusable.MultiLineWebTableCellAction("Product", sProduct, "","GetRow", null, Integer.parseInt(bProduct_RowNumber)+2);
				}
				AddProduct.clickUpdateConfigBtn();
				String Quote_ID1;
				if (Copy_Type.equalsIgnoreCase("Lineitem")) {			
					Quote_ID1 = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");		
				}else{
					Quote_ID1 = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
				}				
				quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
				System.out.println("quoteStage: "+quoteStage);
					if (quoteStage.equalsIgnoreCase("SE Required")) 
					{	
						AddProduct.verifyQuoteStage("SE Required");	
						  if(Copy_Type.equalsIgnoreCase("LineItem")) {
								AddProduct.SE_ReEngagementForIPAddon(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						Approvals.SwitchCPQUser(testDataFile, "SE_User", Quote_ID1);
						AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);	
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID1);	
					}
						
				AddProduct.verifyQuoteStage("Priced");
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);

				String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
			    String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
			
		            if(ProductName.equalsIgnoreCase("CpeSolutionsSite"))
		            {
		                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
		                {
		                    Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
		                }
		                else
		                { Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
		            }
		            else {   
		                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
		                {
		                    Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
		                }
		                else
		                { Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }
		            }
								
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
					quit();
					openBrowser(g.getBrowser());
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				else 
				{
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.SubmitOrder();
	
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}		
}
