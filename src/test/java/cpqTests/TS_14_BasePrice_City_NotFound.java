package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_14_BasePrice_City_NotFound extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testBasePriceCityNotFound(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			String Quote_ID=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID");
                if (!ProductName.equalsIgnoreCase("CPESolutionsSite")) {
				
				AddProduct.AddProductToQuote(ProductName);
				AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					
			} else {
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
			
			}
			   verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
    			String quoteStage1 = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
    			System.out.println("quoteStage: "+quoteStage1);
    			
    			if (quoteStage1.equalsIgnoreCase("SE Required")) 
    			{
    				AddProduct.verifyQuoteStage("SE Required");
    				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
    				{
    					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
    					quit();
    					openBrowser(g.getBrowser());		
    					openurl(Configuration.CPQ_URL);
    					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
    					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
    				}
    				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
    		        
    				quit();
    				openBrowser(g.getBrowser());		
    				openurl(Configuration.CPQ_URL);
    				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
    				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
    				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
    				
    				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
    				{
    					CPQLogin.CPQLogout();
    					quit();
    					openBrowser(g.getBrowser());		
    					openurl(Configuration.CPQ_URL);
    					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
    					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
    					AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
    				}
    				
    				quit();
    				openBrowser(g.getBrowser());		
    				openurl(Configuration.C4C_URL);
    				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
    				AddProduct.navigateQuotesFromHomepage();
    				AddProduct.searchQuoteC4C(Quote_ID);	
    			}
    			AddProduct.verifyQuoteStage("To be Priced");
			
			String Pricing_City=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Pricing_City");
			String Pricing_City_Exp=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Scenario_Name");
			String NRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "MRC");
			String BaseCost=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Base_Cost");
			
			AddProduct.editEthernetProductConfiguration();
			AddProduct.verifyQuoteStage("Portfolio Pricing Review");
			Approvals.SwitchCPQUser(testDataFile, "AD_User", Quote_ID);
			AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.clickProductConfigurationBtn(ProductName);
			if(Pricing_City_Exp.contains("Pricing_City_NotFound"))
			{
				AddProduct.UpdatepricingCityException(ProductName,Pricing_City,NRC,MRC);
			}
			else if(Pricing_City_Exp.contains("BaseCost_NotFound"))
			{
				AddProduct.UpdateBaseCostException(BaseCost);
				
			}
			else {
				AddProduct.UpdateBasePriceException(NRC, MRC);
				AddProduct.ClickSendToSales();
				
			}
			
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
			AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
			AddProduct.verifyQuoteStage("Priced");
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	
}
