package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.exploreFunctions.ExploreLoginPage;

public class TS_20_SubFunctions_Coverage extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	RecordVideo record = new RecordVideo();
	
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testSubFunctionsCoverage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			
			openurl(Configuration.C4C_URL);
			Login.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		
			
			String[] sub_function = "Reject|Accept".split("\\|");
			String Product_Name=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			String Flow_Type=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			
			String sResult;
			
			for (int i=0; i < sub_function.length; i++) {
			
				sResult = AddProduct.AddProductToQuote(Product_Name);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				sResult = AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
		
				
				if (Flow_Type.equalsIgnoreCase("ManualOffnet")) {
				
					sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
					if (sub_function[i].equalsIgnoreCase("Reject")) {
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						sResult = ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					} else {
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					}
					
				}
				
				else if (Flow_Type.equalsIgnoreCase("ManualNearnet")) {
							sResult = AddProduct.verifyQuoteStage("Waiting for BCP");
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							
							
							if (sub_function[i].equalsIgnoreCase("Reject")) {
								quit();
								openBrowser(g.getBrowser());		
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.nearnetExploreRejectOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							} else {
								quit();
								openBrowser(g.getBrowser());	
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							}
							
						}
				
				else if (Flow_Type.equalsIgnoreCase("ManualDSL")) {
					
							sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }														
							
							if (sub_function[i].equalsIgnoreCase("Reject")) {
								quit();
								openBrowser(g.getBrowser());		
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							} else {
								quit();
								openBrowser(g.getBrowser());		
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							}	
							
						}
				
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				
				Login.C4CLogin(testDataFile, Configuration.User_Type);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				AddProduct.navigateQuotesFromHomepage();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				sResult = AddProduct.searchQuoteC4C(Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
				
				sResult = AddProduct.clickProductConfigurationBtn(Product_Name);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				if (sub_function[i].equalsIgnoreCase("Reject")) {
					AddProduct.markPartialSave(Product_Name);
			
				}else{
					AddProduct.updateSaveProductCPQ("Save");
				}
				 		
				if (sub_function[i].equalsIgnoreCase("Reject")) {
		
					sResult = Approvals.deleteProductCPQ(Product_Name);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
				} else {
							
					waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem, 60);
					Reusable.WaitforCPQloader();
					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
//					verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
//					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
				//	System.out.println("quoteStage: "+quoteStage);
					if (quoteStage.equalsIgnoreCase("SE Required")) 
					{
						AddProduct.verifyQuoteStage("SE Required");
						if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
						{
							AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
							quit();
							openBrowser(g.getBrowser());		
							openurl(Configuration.CPQ_URL);
							CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
							CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				           
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
						CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
						AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
						
						if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
						{
							CPQLogin.CPQLogout();
							quit();
							openBrowser(g.getBrowser());		
							openurl(Configuration.CPQ_URL);
							CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
							CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
							AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
						}
						
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						Login.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);
						
					}
					sResult = AddProduct.verifyQuoteStage("Priced");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
				//continue;
			}
			
			AddProduct.discountingProcess(testDataFile, dataSheet ,scriptNo, dataSetNo);
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
			{
				CPQLogin.CPQLogout();
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				//AddProduct.CamHandoverToAgent(testDataFile, dataSheet, scriptNo, dataSetNo);					
			}
			AddProduct.dealPricePLTabEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Deal Pricing Review");					
			
			Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			Approvals.SwitchCPQUser(testDataFile, "DP_User", Quote_ID);
			
			AddProduct.assignQuoteToDPUser(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.dealPricingApproval("Reject");	
			
			CPQLogin.CPQLogout();
				
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			Login.C4CLogin(testDataFile, Configuration.User_Type);
			AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
			AddProduct.verifyQuoteStage("Approval Denied");
			
			AddProduct.reviseQuote();	
			AddProduct.verifyQuoteStage("Priced");
			AddProduct.clearQuoteDiscount();
			
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
            String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
           
            if(Product_Name.equalsIgnoreCase("CpeSolutionsSite"))
            {
               
                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
                }
                else
                { Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
            }
            else {   
                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformation(Product_Name, BCN_Agent, "BCN_NRC");
                }
                else
                { Submit.addBillingInformation(Product_Name, BCN, "BCN_NRC"); }
            }
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				
			}
			else 
			{
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
		
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}		

}
