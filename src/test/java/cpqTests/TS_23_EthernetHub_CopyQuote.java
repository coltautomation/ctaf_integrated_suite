package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_23_EthernetHub_CopyQuote extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQQuoteCreation CreateQuote = new CPQQuoteCreation();
	RecordVideo record = new RecordVideo();

	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testEthernetHubCopyQuote(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			
			String Copy_Quote_At=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Copy_Quote_At");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String sResult;
			
			if(Copy_Quote_At.equalsIgnoreCase("Created") || Copy_Quote_At.equalsIgnoreCase("Base Price Not Found"))
			{
				
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				sResult = AddProduct.siteDetailEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				if(ProductName.equalsIgnoreCase("ColtIpAccess")) {
					sResult = AddProduct.routerTypeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}

				else {
					sResult = AddProduct.onnetConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				
				AddProduct.markPartialSave(ProductName);
				AddProduct.verifyQuoteStage("Created");
				
				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
				System.out.println("Quote ID:  "+Quote_ID);
					
			}
			
			else if(Copy_Quote_At.equalsIgnoreCase("To be Priced")) 
			{
				
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				sResult = AddProduct.siteDetailEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				if(ProductName.equalsIgnoreCase("ColtIpAccess")) {
					sResult = AddProduct.routerTypeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}else {
					sResult = AddProduct.onnetConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.addtionalProductdata(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				AddProduct.updateSaveProductCPQ("SaveToQuote");
				
			}else {
				
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
		}
			
			String Discount_Percentage = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Percentage");
			String Discount_Applicable = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Applicable");
			String CAM_Approve=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "CAM_Approve");

			if(Discount_Applicable.equalsIgnoreCase("Yes")) {
				
					sResult = AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
					System.out.println("quoteStage: "+quoteStage);
					if(Configuration.User_Type.equalsIgnoreCase("Agent_User")&&(CAM_Approve.equalsIgnoreCase("Yes")))	
					{
						AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Discount");
						quit();
						openBrowser(g.getBrowser());
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
						CPQLogin.SelectQuote(Quote_ID);
						
						AddProduct.SubmitForApproval();
						
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);	
					}
					else 
					{
						 AddProduct.SubmitForApproval();
					}
					
					if (Integer.parseInt(Discount_Percentage) > 5) {

					sResult = AddProduct.verifyQuoteStage("Pending Governance Approval");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
				if (Integer.parseInt(Discount_Percentage) > 10) {

					sResult = Approvals.SwitchCPQUser(testDataFile, "VPSales1_User", Quote_ID);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				} else {

					sResult = Approvals.SwitchCPQUser(testDataFile, "VPSales2_User", Quote_ID);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					}
					sResult = AddProduct.quoteDiscountGovernanceApproval("Reject");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

					sResult = AddProduct.verifyQuoteStage("Approval Denied");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }					
					
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.C4C_URL);
					C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
					AddProduct.navigateQuotesFromHomepage();
					AddProduct.searchQuoteC4C(Quote_ID);
					} 
				}
			
			String Copy_Type = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Type");
			Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");	
		
		
			if(Copy_Type.equalsIgnoreCase("LineItem")) {
				Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");	}
			else
			{
				Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
			}
	
			Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo, Quote_ID);
	
			if(!Copy_Quote_At.equalsIgnoreCase("To be Priced")) 
			{
							
			if(!Discount_Applicable.equalsIgnoreCase("Yes")) 
			{
				if(Copy_Type.equalsIgnoreCase("LineItem")) {
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");	}
				else
				{
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
				}
				
				    AddProduct.SubmitForApproval();
					sResult = AddProduct.verifyQuoteStage("Approved");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					}
			
		      	Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
			String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");

				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
                {
                    Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
                }
                else
                { Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }				
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);			
	        }
			
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else 
			{
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);		
			Submit.SubmitOrder();
			}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	

	}
