package cpqTests;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_26_ColtIPAccess_CopyQuote extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQQuoteCreation CreateQuote = new CPQQuoteCreation();
	ExploreConfig ExpConfig = new ExploreConfig();
    RecordVideo record = new RecordVideo();
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testColtIPAccessCopyQuote(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
		
			openurl(Configuration.C4C_URL);
			//C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			String Product_Type = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Product_Type");
			String Network_Reference = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Network_Reference");
			
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
		    Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo); 
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");	
			AddProduct.AddProductToQuote(ProductName);
			String Copy_Quote_At=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Copy_Quote_At");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String sResult;											
			
		    if(Copy_Quote_At.equalsIgnoreCase("Created")) 
			{
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below entry to enter the site details
				sResult = AddProduct.siteDetailEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				sResult = AddProduct.onnetConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
				AddProduct.markPartialSave(ProductName);
				//AddProduct.updateSaveProductCPQ("SaveToQuote");
			AddProduct.verifyQuoteStage("Created");
				
//			    calling the below method to perform copyquote					
				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
				System.out.println("Quote ID:  "+Quote_ID);	
				
			}
			/*else if(Copy_Quote_At.equalsIgnoreCase("To be Priced")) 
			{
				
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				calling the below entry to enter the site details
				sResult = AddProduct.siteDetailEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				sResult = AddProduct.onnetConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
		
				AddProduct.markPartialSave(ProductName);
	
				//AddProduct.markPartialSave(ProductName);
				//AddProduct.updateSaveProductCPQ("SaveToQuote");
				
		    calling the below method to perform copyquote					
				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
				System.out.println("Quote ID:  "+Quote_ID);	

				
			}	*/		
			else {
				
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
			}
			
		//String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
            if(Copy_Quote_At.equalsIgnoreCase("Estimated"))
            {
                quit();
              openBrowser(g.getBrowser());       
               openurl(Configuration.EXPLORE_URL);
               
                ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
                                                  
                quit();
               openBrowser(g.getBrowser());       
              openurl(Configuration.C4C_URL);
               C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
               AddProduct.navigateQuotesFromHomepage();
              AddProduct.searchQuoteC4C(Quote_ID);   
                  
//               calling the below method reconfigure the product
                AddProduct.clickProductConfigurationBtn(ProductName);
                AddProduct.updateSaveProductCPQ("Save");
            }
			
			String Discount_Percentage = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Percentage");
			String Discount_Applicable = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Applicable");
			String CAM_Approve=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "CAM_Approve");
//			Below Condtion is for discount
			if(Discount_Applicable.equalsIgnoreCase("Yes")) {
			
//					Calling the below method to enter the discounting process
				sResult = AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//					calling the below method to verify quote stage and status
					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
					System.out.println("quoteStage: "+quoteStage);
					if(Configuration.User_Type.equalsIgnoreCase("Agent_User")&&(CAM_Approve.equalsIgnoreCase("Yes")))	
					{
						AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Discount");
						quit();
						openBrowser(g.getBrowser());
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.SEUser_Password);
						CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
						
						AddProduct.SubmitForApproval();
					
						quit();
						openBrowser(g.getBrowser());		
					openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);

						
					}
					else 
					{
						 AddProduct.SubmitForApproval();
					}
				
				if (Integer.parseInt(Discount_Percentage) > 5) {
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Pending Governance Approval");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
			if (Integer.parseInt(Discount_Percentage) > 10) {
//				calling the below method to switch to VP sales user
				sResult = Approvals.SwitchCPQUser(testDataFile, "VPSales1_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} else {
//				calling the below method to switch to VP sales user
				sResult = Approvals.SwitchCPQUser(testDataFile, "VPSales2_User", Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
			
//			calling the below method to switch to VP sales user
			sResult = AddProduct.quoteDiscountGovernanceApproval("Reject");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
		
//			calling the below method to verify quote stage and status
		sResult = AddProduct.verifyQuoteStage("Approval Denied");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to verify quote stage and status
			sResult = AddProduct.logoutCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
		
//			Calling the Below method to perform C4C Login
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
		AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
		} 				
		}									
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
		AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
			
			Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
			
			String Copy_Type = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Type");
		
			if(Copy_Type.equalsIgnoreCase("LineItem")) {
				Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");	}
			else
			{
				Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
			}				

//			Calling below method to tear down if stage is to be priced
			//if(!Copy_Quote_At.equalsIgnoreCase("To be Priced")) {
			
//	      Calling the below method to Capture Hub Id
				if (ProductName.equalsIgnoreCase("EthernetHub")) 
			{
				ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
				String HubId = AddProduct.addHubrefernce(ProductName);
				if (HubId.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				if(HubId.equals("True"))DataMiner.fnsetcolvalue(dataSheet, scriptNo, dataSetNo, "Hub_Reference", HubId);
			}
			
			
			if(!Discount_Applicable.equalsIgnoreCase("Yes")) 
			{
//			        calling the below method to move the quote status to Commercial Approval
				    AddProduct.SubmitForApproval();
					
//					calling the below method to verify quote stage and status
					sResult = AddProduct.verifyQuoteStage("Approved");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
//	      		calling the below method to perform copyquote when commerically approved
		      	Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);

//			calling the below method to add Legal and Technical Contact Info
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
	
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
			String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
			/*if (Copy_Type.equalsIgnoreCase("Lineitem")) 
			{
				sResult =AddProduct.copyQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} else {*/
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
				}
				else
				{ Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }		
		//	}
			
//			Calling the below method to generate and send proposal
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			
//			calling the below method to make contact info entry
			/*if(!Product_Type.equalsIgnoreCase("Addon")) {
			if (Copy_Type.equalsIgnoreCase("Lineitem")) {
				sResult = AddProduct.navigateContactInfoTab();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				sResult =AddProduct.copyQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo,"Contact");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
			} 
			else {
				Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);		
				}
			}else {
//				calling the below method to login to cpq as cst user
				sResult = AddProduct.contactInfoEntryAddons(testDataFile, scriptNo, dataSetNo, ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}*/
			
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				String Password = Configuration.SEUser_Password;
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

				
			}
			else 
			{
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);		
			Submit.SubmitOrder();
		/*	AddProduct.verifyQuoteStage("Ordered");
			
//	      calling the below method to perform copyquote
			Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			
//			calling the below method to capture the service order
			if (Copy_Type.equalsIgnoreCase("Lineitem")) {
				sResult =AddProduct.copyQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo,"ServiceOrder");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} else {
				Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
			*/
			
//			calling the below method to verify quote stage and status
//			CPQLogin.CPQLogout();	
			//}

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	
}
