package cpqTests;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_27_CPE_CopyQuote extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQQuoteCreation CreateQuote = new CPQQuoteCreation();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testCPECopyQuote(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			String Product_Type = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Product_Type");
			String Network_Reference = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Network_Reference");
			if (Product_Type.equalsIgnoreCase("Addon")) { 
				DataMiner.fnsetcolvalue(dataSheet, scriptNo, dataSetNo, "Related_Network_Reference", Network_Reference); 
			}
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
		
			String Copy_Quote_At=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Copy_Quote_At");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String sResult;														
			
			if(Copy_Quote_At.equalsIgnoreCase("Created")) 
			{
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
								
					Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
					System.out.println("Quote ID:  "+Quote_ID);	
				
			}
			else if(Copy_Quote_At.equalsIgnoreCase("To be Priced")) 
			{
				
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
				
					
					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
					System.out.println("quoteStage: "+quoteStage);
					if (quoteStage.equalsIgnoreCase("SE Required")) 
					{
						AddProduct.verifyQuoteStage("SE Required");
						if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
						{
							AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
							quit();
							openBrowser(g.getBrowser());		
							openurl(Configuration.CPQ_URL);
							CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
							CPQLogin.SelectQuote(Quote_ID);
						}
						AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				     
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
						CPQLogin.SelectQuote(Quote_ID);
						AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);
					}
			}else {
				
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");					
				
			}
			
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");

			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(Quote_ID);
				}
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		            
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(Quote_ID);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
			}
		
			String Discount_Percentage = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Percentage");
			String Discount_Applicable = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Discount_Applicable");
			String CAM_Approve=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "CAM_Approve");
			
			if(Discount_Applicable.equalsIgnoreCase("Yes")) {
				
				sResult = AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
				quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
				System.out.println("quoteStage: "+quoteStage);
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User")&&(CAM_Approve.equalsIgnoreCase("Yes")))	
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Discount");
					quit();
					openBrowser(g.getBrowser());
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(Quote_ID);
					
					AddProduct.SubmitForApproval();
					
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.C4C_URL);
					C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
					AddProduct.navigateQuotesFromHomepage();
					AddProduct.searchQuoteC4C(Quote_ID);

					
				}
				else 
				{
					 AddProduct.SubmitForApproval();
				}
				
				sResult = AddProduct.verifyQuoteStage("Deal Pricing Review");
				if (sResult.equalsIgnoreCase("True")){ throw new SkipException("Skipping this test"); }
				
				if (Integer.parseInt(Discount_Percentage) > 5) {
				sResult = AddProduct.verifyQuoteStage("Pending Governance Approval");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
			if (Integer.parseInt(Discount_Percentage) > 10) {
				sResult = Approvals.SwitchCPQUser(testDataFile, "VPSales1_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} else {
				sResult = Approvals.SwitchCPQUser(testDataFile, "VPSales2_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
			
			sResult = AddProduct.quoteDiscountGovernanceApproval("Reject");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			sResult = AddProduct.verifyQuoteStage("Approval Denied");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
			AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
			} 				
			}

			Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID");			
			
			Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
		String Copy_Type = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Type");

		if(Copy_Type.equalsIgnoreCase("LineItem")) {
			Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");	}
		else
		{
			Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
		}
		
		quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
		System.out.println("quoteStage: "+quoteStage);
		if (quoteStage.equalsIgnoreCase("SE Required")) 
		{
			AddProduct.verifyQuoteStage("SE Required");
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
			{
				AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(Quote_ID);
			}
			AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
	            
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.CPQ_URL);
			CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
			CPQLogin.SelectQuote(Quote_ID);
			AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
			quit();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
			AddProduct.navigateQuotesFromHomepage();
			AddProduct.searchQuoteC4C(Quote_ID);
		}
		
				if(!Discount_Applicable.equalsIgnoreCase("Yes")) 
				{
					 Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
					
						String BaseCost=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Base_Cost");
						ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
						String MRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "MRC");
			            String NRC=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "NRC");
			           
			            String ExceptionType=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Exception_Type");
			            String Pricing_City=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Pricing_City");						
						
						quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
						System.out.println("quoteStage: "+quoteStage);
						if (quoteStage.equalsIgnoreCase("SE Required")) 
						{
							AddProduct.verifyQuoteStage("SE Required");
							if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
							{
								AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo,"SE Engage");
								quit();
								openBrowser(g.getBrowser());		
								openurl(Configuration.CPQ_URL);
								CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
								CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
							}
							AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					            
							quit();
							openBrowser(g.getBrowser());		
							openurl(Configuration.CPQ_URL);
							CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
							CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
							AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
							quit();
							openBrowser(g.getBrowser());		
							openurl(Configuration.C4C_URL);
							C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
							AddProduct.navigateQuotesFromHomepage();
							AddProduct.searchQuoteC4C(Quote_ID);
						}
						
						verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
						String quoteStage1 = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
						System.out.println("quoteStage: "+quoteStage1);
						
						if (quoteStage1.equalsIgnoreCase("To be Priced")) 
						{
							AddProduct.editEthernetProductConfiguration(); 
							Approvals.SwitchCPQUser(testDataFile, "AD_User", Quote_ID);
							AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
							AddProduct.verifyQuoteStage("Portfolio Pricing Review");
							AddProduct.clickProductConfigurationBtn(ProductName);
							if(ExceptionType.equalsIgnoreCase("PricingCity"))
				            {
								AddProduct.UpdatepricingCityException(ProductName,Pricing_City,NRC,MRC);
				            }
							else if(ExceptionType.equalsIgnoreCase("BaseCost"))
				            {
				            	AddProduct.UpdateBaseCostException(BaseCost);
				            }
				            else
				            { 
				            	AddProduct.UpdateBasePriceException(NRC, MRC); 
				            	AddProduct.ClickSendToSales();
				            }
						}
						
						
					AddProduct.SubmitForApproval();
		
					sResult = AddProduct.verifyQuoteStage("Approved");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}

				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
		
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				String CPE_Service_BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
				String CPE_Service_BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
				String CPEProduct = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "CPEProduct");
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
				{
					Submit.addBillingInformationCPE(CPEProduct,CPE_Service_BCN_Agent,"BCN");
				}
				else
				{
					Submit.addBillingInformationCPE(CPEProduct, CPE_Service_BCN, "BCN");
				}

				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
				{
					
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
					quit();
					openBrowser(g.getBrowser());
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);	
				}
				else 
				{
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);

				Submit.SubmitOrder();

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	
}
