package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_37_Multiple_ManualRequsts_Order_Completion extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	GlobalVariables g = new GlobalVariables();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testOffnetOrderCompletion(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			//Login.C4CLogin(testDataFile, Configuration.User_Type);
		    Login.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			System.out.println("scriptNo: "+scriptNo);
			System.out.println("dataSetNo: "+dataSetNo);System.out.println("dataSheet: "+dataSheet);
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
		    {
		    Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);	
			
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String Flow_Type=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
//			if(ProductName.equalsIgnoreCase("Wave")) {
//				
////				calling the below method to verify quote stage and status
//				sResult = AddProduct.verifyQuoteStage("Created");
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//					
////				calling the below method to add Legal and Technical Contact Info
//				//String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
//				sResult = Approvals.SwitchCPQUser(testDataFile, "SE_User", Quote_ID);
//
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//					
////				calling the below method reconfigure the product
//				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
//					
////				calling the below method to configure the product
//				sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
//					
////				calling the below method to update and save the product in cpq
//				sResult = AddProduct.updateSaveProductCPQ("Save");
//				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
//					
//			}
//          Verify the Quote Stage
			if (Flow_Type.equalsIgnoreCase("ManualNearnet"))
			{
				AddProduct.verifyQuoteStage("Waiting for BCP");
			}
			else
			{
				AddProduct.verifyQuoteStage("Waiting for 3rd Party");
			}
			//String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String CopyQuote = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"CopyQuote");			
			
			if (Flow_Type.equalsIgnoreCase("ManualDiverse")||Flow_Type.equalsIgnoreCase("ManualDSL")||Flow_Type.equalsIgnoreCase("ManualNearnet")||Flow_Type.equalsIgnoreCase("ManualOffnet"))
			{																							
				if (CopyQuote.equalsIgnoreCase("Yes"))
				{
					AddProduct.copyQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
					DataMiner.fnsetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID",Quote_ID);
					
					AddProduct.clickProductConfigurationBtn(ProductName);												 
					
						if(Flow_Type.equalsIgnoreCase("ManualOffnet"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								break;
								
							case "EthernetLine":
								AddProduct.offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								AddProduct.offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}														
						}
						else if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								break;
								
							case "EthernetLine":
								AddProduct.nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								AddProduct.nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}																	
						}
						else if(Flow_Type.equalsIgnoreCase("ManualDSL"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");	
								break;
							case "EthernetLine":
								AddProduct.dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");	
								AddProduct.dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}														
						}
						else if(Flow_Type.equalsIgnoreCase("ManualDiverse"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								break;
								
							case "EthernetLine":
								AddProduct.ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");	
								AddProduct.ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}															
						}
					
					if(ProductName.equalsIgnoreCase("Wave"))
					{	
						Approvals.SwitchCPQUser(testDataFile, "SE_User", Quote_ID);
						AddProduct.clickProductConfigurationBtn(ProductName);
						AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);							
					}							
					AddProduct.updateSaveProductCPQ("Save");							
				}
					CPQLogin.CPQLogout();
							
				//	calling the below method to Reject the Request in Explorer
						quit();
						openBrowser(g.getBrowser());
						openurl(Configuration.EXPLORE_URL);
						if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
						{
							ExpConfig.nearnetExploreRejectOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						else 
						{ 
							ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo); 
							
						}	
														
				//		Calling the Below method to perform C4C Login and navigage to the Quote in CPQ	
						quit();
						openBrowser(g.getBrowser());			
						openurl(Configuration.C4C_URL);
						Login.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);									
																				
				// 		calling the below method to verify the Status of Manual Requests in CPQ and Recreate the Manual Request
						switch(ProductName) {
						
						case "EthernetSpoke": case "ColtIpAccess":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							AddProduct.updateSaveProductCPQ("Save");
							break;
							
						case "EthernetLine": case "Wave":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							AddProduct.markPartialSave(ProductName);
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"B_End");
							AddProduct.updateSaveProductCPQ("Save");
							AddProduct.verifyQuoteStage("Waiting for 3rd Party");	
							break;
						}							
										
				//		calling the below method to configure the product
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
						{
							ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						else { ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo); }					
																				
				//		Calling the Below method to perform C4C Login	
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						Login.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);
											
														
				// 		calling the below method to verify the Status of Manual Requests in CPQ and Recreate the Manual Request
						switch(ProductName) {
						
						case "EthernetSpoke": case "ColtIpAccess":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							break;
							
						case "EthernetLine": case "Wave":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"B_End");
							break;
						}												
					}			
				
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
				String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
				System.out.println("quoteStage: "+quoteStage);
				if (quoteStage.equalsIgnoreCase("SE Required")) 
				{
					AddProduct.verifyQuoteStage("SE Required");
					AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo); 
					//Login with CPQ SE user
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
					//Approvals.SwitchCPQUser(testDataFile, Configuration.User_Type, Quote_ID);
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.C4C_URL);
					Login.C4CLogin(testDataFile, Configuration.User_Type);
					AddProduct.navigateQuotesFromHomepage();
					AddProduct.searchQuoteC4C(Quote_ID);
				}
		
				AddProduct.verifyQuoteStage("Priced");
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);				
				
				String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
	            String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
	           
	            if(ProductName.equalsIgnoreCase("CpeSolutionsSite"))
	            {
	               
	                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
	                {
	                    Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
	                }
	                else
	                { Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
	            }
	            else {   
	                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
	                {
	                    Submit.addBillingInformation(ProductName, BCN_Agent, "BCN_NRC");
	                }
	                else
	                { Submit.addBillingInformation(ProductName, BCN, "BCN_NRC"); }
	            }		
				
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
				{
					
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
					quit();
					openBrowser(g.getBrowser());
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

					
				}
				else 
				{
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
				Submit.SubmitOrder();
		

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	
	
}
