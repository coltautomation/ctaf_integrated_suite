package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.siebelFunctions.SiebelLibrary;

public class TS_40_ProjectQuote extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	SiebelLibrary Siebel_Library = new SiebelLibrary();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testProjectQuoteOrderCompletion(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
		
			openurl(Configuration.C4C_URL);
			//C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
			C4CLogin.C4CLogin(testDataFile,Configuration.User_Type);
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote("");
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo); 
			
			String ProductName=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID");
			
									
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
				
				AddProduct.AddProductToQuote(ProductName);
//				calling the below method to enter the site address
				 AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
								
//				calling the below method to configure the product
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					
			} else {
//				Calling the below method to add the product in CPQ
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
			
			}
		
			String Flow_Type=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
			if (Flow_Type.equalsIgnoreCase("OnnetDualEntry")) {
				
						AddProduct.verifyQuoteStage("Waiting for BCP");
						CPQLogin.CPQLogout();
						
//						Login On Explore
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						
//				calling the below method to configure the product
						sResult = ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
						
//						Calling the Below method to perform C4C Login	
						quit();
						openBrowser(g.getBrowser());		
						openurl(Configuration.C4C_URL);
						C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
						AddProduct.navigateQuotesFromHomepage();
						AddProduct.searchQuoteC4C(Quote_ID);
						
				//		calling the below method reconfigure the product
						sResult = AddProduct.clickProductConfigurationBtn(ProductName);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
				//		calling the below method to update and save the product in cpq
						sResult = AddProduct.updateSaveProductCPQ("Save");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					}
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		            
				//Login with CPQ SE user
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				//Approvals.SwitchCPQUser(testDataFile, Configuration.User_Type, Quote_ID);
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);
			}
			AddProduct.verifyQuoteStage("Priced");
						
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			
			if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
			{
			String CPE_Service_BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
			Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
			} else {
			String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation_ProjectQuote(ProductName, BCN, "BCN NRC");
			}
			

			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);	
			Submit.SelectProjectQuoteoption(testDataFile, dataSheet, scriptNo, dataSetNo);
			String RejectService = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"RejectService_Parent");
			if(RejectService.equalsIgnoreCase("Yes"))
			{
				Submit.RejectionQuote(testDataFile, dataSheet, scriptNo, dataSetNo);	
			}
			else{
				Submit.SubmitOrder_ProjectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.verifyQuoteStage("Approved");
				String OCNupdate = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"OCN_Update");
				if(OCNupdate.equalsIgnoreCase("Yes")) 
				{
				AddProduct.clickonRevise_ProjectQuote();
				AddProduct.updateOCN(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				} 
			
			
				String Product_Name = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Product_Name");
				String ChildQuote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "ChildQuote_ID");
				String SEReEngagement = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"SEReEngagement");
			
				
				if(SEReEngagement.equalsIgnoreCase("No"))
				{
					AddProduct.clickonRevise_ProjectQuote();
					AddProduct.clickProductConfigurationBtn(Product_Name);
					AddProduct.UpdateWaveProductConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.SE_ReEngagementForWave(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.SubmitForApproval();
					AddProduct.verifyQuoteStage("Approved");
					AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);	
				}
				
				
				
				if(SEReEngagement.equalsIgnoreCase("Yes")) 
				{
					AddProduct.clickonRevise_ProjectQuote();
					AddProduct.clickProductConfigurationBtn(Product_Name);
					AddProduct.FeatureLinkOpen();
					AddProduct.selectProductFeaturesBespoke(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.updateSaveProductCPQ("SAVE"); 
				    String SE_Opp_Tickets = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"SE_Opp_Tickets");
					if(SE_Opp_Tickets.equalsIgnoreCase("Yes")) 
					{
						AddProduct.C4CPSConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					}
					else
					{
					AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					}
					//Login with CPQ SE user
					quit();
					openBrowser(g.getBrowser());       
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
					CPQLogin.SelectChildQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
					CPQLogin.CPQLogout();
				    String SEReEngagementTrue = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"SEReEngagementTrue");
					if(SEReEngagementTrue.equalsIgnoreCase("Yes")) 
					{	
	            //Login with CPQ Sales user
					quit();
					openBrowser(g.getBrowser());		
					openurl(Configuration.C4C_URL);
					C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
					AddProduct.navigateQuotesFromHomepage();
					AddProduct.searchQuoteC4C(ChildQuote_ID);
					AddProduct.SE_ReEngagementForChildQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					CPQLogin.CPQLogout();
					//Login with CPQ SE user
					quit();
					openBrowser(g.getBrowser());       
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
					CPQLogin.SelectChildQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.verifyQuoteStage("SE Required");
					AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
					}
	            quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				C4CLogin.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(ChildQuote_ID);
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				
						

				if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
				{
					if(OCNupdate.equalsIgnoreCase("Yes")) 
					{
						String BCNupdate = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Update");
						Submit.addBillingInformationCPE("CPESolutionsSite", BCNupdate, "BCN");
					
					}else{
						String CPE_Service_BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
						Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
					}	
			} else {
				if(OCNupdate.equalsIgnoreCase("Yes")) 
				{
					String BCNupdate = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Update");
					Submit.addBillingInformation(ProductName, BCNupdate, "BCN NRC");
				}else{
					String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
					Submit.addBillingInformation(ProductName, BCN, "BCN NRC");	
					}
			}
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			String RejectServiceChild = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"RejectService_Child");
			if(RejectServiceChild.equalsIgnoreCase("Yes"))
			{
				Submit.RejectionQuote_Child(testDataFile, dataSheet, scriptNo, dataSetNo);	
				AddProduct.verifyQuoteStage("Rejected by customer");
			}else{
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.SubmitOrder();
			}	
		}
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	

}
