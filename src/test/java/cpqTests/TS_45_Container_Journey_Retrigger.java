package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_45_Container_Journey_Retrigger extends SeleniumUtils
{

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	GlobalVariables g = new GlobalVariables();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testContainerJourney(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin(testDataFile,Configuration.User_Type);
			
			Accounts.NavigateToAccounts();
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
			Accounts.SearchAccount_Agent(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else{
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.addProductandQuoteInC4C(testDataFile, dataSheet, scriptNo, dataSetNo);	
			
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);						
			
			String Product_Name = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Product_Name");
			String No_Of_Copies = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"No_Of_Copies");
			String Option= DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Select_Type");
			String Option1= DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"Select_Type1");
			String Quote_ID = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Quote_ID");
			String Retrigger_DPReview = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Retrigger_DealPricing");


			System.out.println("Before"+No_Of_Copies);
		
			AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)",1 ,Option,"Quote_Level");
			Report.LogInfo("INFO", "1", "INFO");
			
			AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)", 1,Option,"Quote_Level");
			Report.LogInfo("INFO", "2", "INFO");
		
		    AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)", 1,Option,"Quote_Level");
		    Report.LogInfo("INFO", "3", "INFO");
			
			for(int i=1; i<Integer.parseInt(No_Of_Copies); i++) 
			{
				
				AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", i,Option1,"Line_Item");
				Report.LogInfo("INFO", "Customer Facing_1", "INFO");
			
				AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", i,Option1,"Line_Item");
				Report.LogInfo("INFO", "Customer Facing_2", "INFO");
			
			    AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", i,Option1,"Line_Item");
			    Report.LogInfo("INFO", "Customer Facing_3", "INFO");
			}	
			Reusable.WaitforCPQloader();
			
			//Saving the changes
			waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, 10);
			verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
			click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
			
			Reusable.WaitforCPQloader();
			waitForElementToDisappear(CPQQuoteSubmissionObj.QuoteSubmit.ProcessingIcon,300);
			
			String Add_Product = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Add_Product");
			String Container_Product=DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Container_Product");
			
			if(Add_Product.equalsIgnoreCase("Yes"))
			{
				AddProduct.ModComAddProduct(Container_Product);
				AddProduct.ContainerProductConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo,Container_Product);
				AddProduct.saveCPQ("Main");							
				
				refreshPage();
				Reusable.waitForAjax();
				AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", 2,Option1,"Line_Item");
				Report.LogInfo("INFO", "Customer Facing_1", "INFO");
			
				AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", 2,Option1,"Line_Item");
				Report.LogInfo("INFO", "Customer Facing_2", "INFO");
			
			    AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", 2,Option1,"Line_Item");
			    Report.LogInfo("INFO", "Customer Facing_3", "INFO");
			}	
			
			if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
			{
				quit();
				openBrowser(g.getBrowser());
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
				CPQLogin.SelectQuote(Quote_ID);							
			}
			
			AddProduct.ModComSubmitForApproval(dataSheet, scriptNo, dataSetNo);
			
			AddProduct.verifyQuoteStage("Commercially Approved");
			AddProduct.retrigger();
		
			AddProduct.retriggerApproval(testDataFile, dataSheet, scriptNo, dataSetNo,"Container");
			
			if(Retrigger_DPReview.contains("No"))
			{	
				AddProduct.verifyQuoteStage("Commercially Approved");
				
				AddProduct.Submit4TechnicalApproval();
	            CPQLogin.CPQLogout();
				
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(Quote_ID);
				Approvals.QuoteSEApproval(testDataFile, dataSheet, scriptNo, dataSetNo);
	            CPQLogin.CPQLogout();
					
				quit();
				openBrowser(g.getBrowser());			
				openurl(Configuration.CPQ_URL);
				Reusable.WaitforCPQloader();
				CPQLogin.CPQLogin(Configuration.CSTUser_Username, Configuration.CSTUser_Password);
				CPQLogin.SelectQuote(Quote_ID);
				Approvals.QuoteCSTApproval();
				quit();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				Login.C4CLogin(testDataFile, Configuration.User_Type);
				AddProduct.navigateQuotesFromHomepage();
				AddProduct.searchQuoteC4C(Quote_ID);	
				
				String BCN = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN");
	            String BCN_Agent = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo,"BCN_Agent");
	           
	            if(Product_Name.equalsIgnoreCase("CpeSolutionsSite"))
	            {
	               
	                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
	                {
	                    Submit.addBillingInformationCPE("CPESolutionsSite", BCN_Agent, "BCN");
	                }
	                else
	                { Submit.addBillingInformationCPE("CPESolutionsSite", BCN, "BCN"); }
	            }
	            else {   
	                if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))
	                {
	                    Submit.addBillingInformation(Product_Name, BCN_Agent, "BCN_NRC");
	                }
	                else
	                { Submit.addBillingInformation(Product_Name, BCN, "BCN_NRC"); }
	            }
				
	            
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.verifyQuoteStage("Sent To Customer");
				
				if(Configuration.User_Type.equalsIgnoreCase("Agent_User"))	
				{
					AddProduct.EngageCAM_SEEngage(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer Acceptance");
					quit();
					openBrowser(g.getBrowser());
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.CAM_Username, Configuration.CAM_Password);
					CPQLogin.SelectQuote(Quote_ID);
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				else 
				{
					Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				Submit.SubmitOrder();
			}
			else{
				AddProduct.verifyQuoteStage("Deal Pricing Review");

			}
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}	

}
