package ncTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.ncFunctions.NcLogin;
import testHarness.ncFunctions.AccountNavigation;
import testHarness.ncFunctions.ModifyOrder;
import testHarness.ncFunctions.NcCreateOrder;

public class TS_02_Modify_Order extends SeleniumUtils
{
//	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	Properties prop = new Properties();

	NcLogin Login = new NcLogin();
	AccountNavigation NcAccounts = new AccountNavigation();
	NcCreateOrder NcCreateOrder = new NcCreateOrder();
	ModifyOrder NcModifyOrder = new ModifyOrder();
	
	GlobalVariables g = new GlobalVariables();
	RecordVideo record = new RecordVideo();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testModifyOrder(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) 
		{

            // load a properties file
            prop.load(input);
		} 
		catch (IOException ex) 
		{
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);

		try
		{
			openurl(Configuration.NC_URL);
			Login.Login(Configuration.NC_Username, Configuration.NC_Password);	
			Login.VerifySuccessLogin();
//			NcAccounts.GotoDocument();
//			NcAccounts.GotoAccount();
			NcAccounts.GotoAccount1(testDataFile, dataSheet, scriptNo, dataSetNo);
 			NcModifyOrder.NcModifyOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
		}
		catch(Exception e)
		{
            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();
		}
	}	
}
