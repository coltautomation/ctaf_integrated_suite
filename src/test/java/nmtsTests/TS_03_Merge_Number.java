package nmtsTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.nmtsFunctions.NMTSAddNumberPage;
import testHarness.nmtsFunctions.NMTSLoginPage;
import testHarness.nmtsFunctions.NMTSMergeNumberPage;
import testHarness.nmtsFunctions.NMTSSplitFreeNumberPage;


public class TS_03_Merge_Number extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	NMTSLoginPage Login = new NMTSLoginPage();
	NMTSAddNumberPage AddNumber = new NMTSAddNumberPage();
	GlobalVariables g = new GlobalVariables();
	RecordVideo record = new RecordVideo();
	NMTSSplitFreeNumberPage SplitFreeNumber = new NMTSSplitFreeNumberPage();
	NMTSMergeNumberPage MergeNumber = new NMTSMergeNumberPage();
	
	

	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testMergeNumber(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = g.getBotId();			
		record.startVideoRecording(botId, ScenarioName);
		try
		{
			openNMTSUrl(Configuration.NMTS_URL);
			MergeNumber.MergerdNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
				
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
		
	}	

}
