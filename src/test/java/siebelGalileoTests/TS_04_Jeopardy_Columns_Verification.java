package siebelGalileoTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.siebelGalileoObjects.SiebelCockpitObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.siebelGalileoFunctions.SiebelCockpit;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCEOS;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebelFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNumberManagement;

public class TS_04_Jeopardy_Columns_Verification extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	//String dataFileName = "Siebel_testData.xlsx";
	//File path = new File("./src/test/resources/"+dataFileName);
	//String testDataFile = path.toString();
	//String tsSheetName = "New_Order";
	//String scriptNo = "2";
	//String dataSetNo = "12";
	//String Amount = "2";
	
	//private static GlobalVariables g;
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddProduct = new SiebelCreateCustomerOrder();
	SiebelCockpit Cockpit = new SiebelCockpit();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelMode siebelmod = new SiebelMode();
	SiebelNumberManagement NumMana = new SiebelNumberManagement();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();
	   
	Properties prop = new Properties();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void cockpitAppletFacilities(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
        record.startVideoRecording(botId, ScenarioName);
		try
		{
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			Cockpit.NavigateToServiceOrderCockpitTab();
						
			if(ScenarioName.contains("Jeopardy_Milestone"))
			{
				Cockpit.verifyJeopardyFlag(dataSheet, scriptNo, dataSetNo);
			}
			if(ScenarioName.contains("Legend_Verification"))
			{
				Cockpit.verifyStageAndJeopardyFlag(dataSheet, scriptNo, dataSetNo);
			}
			if(ScenarioName.contains("Jeopardy_WorkItems"))
			{
				String Subverification = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Subverification");
				Cockpit.selectMyTeamOrdersOption();
				if(Subverification.equalsIgnoreCase("JeopardySection")){
					Cockpit.verifyXtracColumnsinJeopardy(dataSheet, scriptNo, dataSetNo);
				}
				if(Subverification.equalsIgnoreCase("MoreInfoSection")){
					Cockpit.verifyXtracColumnsinMoreInfo(dataSheet, scriptNo, dataSetNo);
				}
				if(Subverification.equalsIgnoreCase("SiebelColumns")){					
					Cockpit.verifyJeopardyViewMoreInfoColumns(dataSheet, scriptNo, dataSetNo);
				}
			}
			if(ScenarioName.contains("Jeopardy_ColumnValues_Verification"))
			{
				Cockpit.selectMyTeamOrdersOption();
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Plan");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Plan Milestone");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Build");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Build Milestone");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Activate");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"CPD");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Open Queues");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Days to CPD");
				Cockpit.JeopardyColumnValues(dataSheet, scriptNo, dataSetNo,"Days to CPD Recalculation");
				selectByVisibleText(SiebelCockpitObj.ServiceOrderCockpit.allOrdersOption, "My Orders", "Order Type");
			}
			if(ScenarioName.contains("LegendImage")){
				Cockpit.verifyJeopardyLegend();
			}
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
				Assert.assertTrue(false);
	    }
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}
			
}
