package siebelGalileoTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelGalileoFunctions.CustomerTimeJourney;
import testHarness.siebelGalileoFunctions.SiebelCockpit;
import testHarness.siebelGalileoFunctions.SiebelLeadTimeWorkflow;
import testHarness.siebelFunctions.SiebelLoginPage;

public class TS_10_PIAMapping_LTW extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelLeadTimeWorkflow leadTimeWorkflow = new SiebelLeadTimeWorkflow();
	CustomerTimeJourney CTJ = new CustomerTimeJourney();
	
	Properties prop = new Properties();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testServiceOrder_leadTimeWorkflow(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        String botId = g.getBotId();			
        record.startVideoRecording(botId, ScenarioName);
		try
		{
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			CTJ.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
			//String Subverification= DataMiner.fngetcolvalue(dataSheet,scriptNo,dataSetNo,"Subverification");
											
			if(ScenarioName.contains("Quality Issue Validation")){
				leadTimeWorkflow.verifyVoiceSpecificValues(dataSheet,scriptNo,dataSetNo);
				CTJ.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				leadTimeWorkflow.verifyListOfRCATeamValues(dataSheet,scriptNo,dataSetNo);
				CTJ.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				leadTimeWorkflow.createQualityIssue(dataSheet,scriptNo,dataSetNo);
				CTJ.OpenServiceOrderNumber( dataSheet, scriptNo, dataSetNo);
				leadTimeWorkflow.verifyValidationTask(dataSheet,scriptNo,dataSetNo);
				}
				if(ScenarioName.contains("InflightChange_Workflow")){
				leadTimeWorkflow.verifyIFCWFPopupMessage(dataSheet,scriptNo,dataSetNo);
				CTJ.OpenServiceOrderNumber( dataSheet, scriptNo, dataSetNo);
				leadTimeWorkflow.verifyIFCWFWarningMessage(dataSheet, scriptNo, dataSetNo);
				}
				if(ScenarioName.contains("LTW_PrimarySecondaryValue_InflightChange"))
				{
					leadTimeWorkflow.verifyPrimarySecondaryInflightWorkflow(dataSheet, scriptNo, dataSetNo);
				}
				
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
				Assert.assertTrue(false);
	    }
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}
			
}
