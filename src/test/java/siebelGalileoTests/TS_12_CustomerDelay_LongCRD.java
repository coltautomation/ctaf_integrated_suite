package siebelGalileoTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelGalileoFunctions.CustomerTimeJourney;
import testHarness.siebelGalileoFunctions.SiebelCockpit;
import testHarness.siebeltoNcFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelLoginPage;

public class TS_12_CustomerDelay_LongCRD extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCockpit Cockpit = new SiebelCockpit();
	CustomerTimeJourney Ctj = new CustomerTimeJourney();

	Properties prop = new Properties();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testServiceOrder_Cockpit(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        String botId = g.getBotId();			
        record.startVideoRecording(botId, ScenarioName);
		try
		{
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);

			if (ScenarioName.contains("Create"))
			{
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.createDelay(dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("CancelDelay"))
			{
//				Ctj.createDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.navigateDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
				Ctj.cancelDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("CancelStatus"))
			{
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.navigateDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
				Ctj.cancelDelayStatus(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("Remove"))
			{
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.navigateDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
				Ctj.removeWorkitem(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("AuditTrailColumns"))
			{
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.navigateDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
				Ctj.validateAuditTrialColumns(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			if (ScenarioName.contains("Diary"))
			{
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.navigateDelay(testDataFile, dataSheet, scriptNo, dataSetNo);
				Ctj.orderDiaryUpdate(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("ReturnWI"))
			{
				Cockpit.NavigateToServiceOrderCockpitTab();
				Ctj.verifyReturnWIColumn(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("Resolved"))
			{
				Cockpit.NavigateToServiceOrderCockpitTab();
				Ctj.verifyDelayResolvedFilter(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("Add_LongCRD")||(ScenarioName.contains("ReturnDate"))||(ScenarioName.contains("Validate")))
			{
				Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				Ctj.createLONGCRD(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("DelayHyperLink"))
			{
				Cockpit.NavigateToServiceOrderCockpitTab();
				Ctj.cockpitCustDelayHyperlink(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			else if (ScenarioName.contains("verify_Restriction"))
			{
				Ctj.verify_Restriction(dataSheet, scriptNo, dataSetNo);
			}
			waitForAjax();
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
				Assert.assertTrue(false);
	    }
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}
			
}
