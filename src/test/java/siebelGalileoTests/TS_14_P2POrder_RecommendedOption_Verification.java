package siebelGalileoTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelGalileoFunctions.CustomerTimeJourney;
import testHarness.siebelGalileoFunctions.SiebelCockpit;
import testHarness.siebeltoNcFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelLoginPage;

public class TS_14_P2POrder_RecommendedOption_Verification extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCockpit Cockpit = new SiebelCockpit();
	CustomerTimeJourney Ctj = new CustomerTimeJourney();

	Properties prop = new Properties();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testServiceOrder_Cockpit(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        String botId = g.getBotId();			
        record.startVideoRecording(botId, ScenarioName);
		try
		{
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
			Ctj.RecommendedOptionVerification(dataSheet, scriptNo, dataSetNo);
			waitForAjax();
			
			String Subverification = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Subverification");
			if(Subverification.equalsIgnoreCase("StatusVerification"))
			{
				Ctj.getColtDelayStatus("ColtDelay");
				Ctj.getWIOriginatingStatus();
			}
			
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
				Assert.assertTrue(false);
	    }
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}
			
}
