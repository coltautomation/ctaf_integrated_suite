package siebelGalileoTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelGalileoFunctions.CustomerTimeJourney;
import testHarness.siebelGalileoFunctions.SiebelCockpit;
import testHarness.siebelGalileoFunctions.WelcomeCallActivity;
import testHarness.siebelGalileoFunctions.WelcomeMail;


public class TS_16_WelcomeActivityCall extends SeleniumUtils {
	

	ReusableFunctions Reusable = new ReusableFunctions();
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	WelcomeMail Mail = new WelcomeMail();
	CustomerTimeJourney CTJ = new CustomerTimeJourney();
	WelcomeCallActivity WCA = new WelcomeCallActivity();
	
	Properties prop = new Properties();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testServiceOrder_Cockpit(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        String botId = g.getBotId();			
        record.startVideoRecording(botId, ScenarioName);
        try
		{
			String subVerification = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Subverification");
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);			
			CTJ.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
			int NoofRows = WCA.navigateToActivitiesTab();
			if(subVerification.equalsIgnoreCase("Delivery"))
			{
				WCA.VerifyActivitiesTable(NoofRows,subVerification);
			}
			if(subVerification.equalsIgnoreCase("On-hold"))
			{
				WCA.ChangeStatus();
				Reusable.savePage();
				CTJ.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				WCA.VerifyActivitiesTable(NoofRows,subVerification);
			}
			if(subVerification.equalsIgnoreCase("Completed"))
			{				
				WCA.CircuitReferenceGeneration();
				WCA.OrderComplete();
				CTJ.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				WCA.VerifyActivitiesTable(NoofRows,subVerification);
			}						
			
			if(subVerification.equalsIgnoreCase("GCN"))
			{
				WCA.WCAVerification_GCNAccount();
			}
				
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
				Assert.assertTrue(false);
	    }
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}


}
