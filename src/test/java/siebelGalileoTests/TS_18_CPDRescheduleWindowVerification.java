package siebelGalileoTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelGalileoFunctions.CustomerTimeJourney;
import testHarness.siebelGalileoFunctions.SiebelCockpit;
import testHarness.siebeltoNcFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelLoginPage;

public class TS_18_CPDRescheduleWindowVerification extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCockpit Cockpit = new SiebelCockpit();
	CustomerTimeJourney Ctj = new CustomerTimeJourney();

	Properties prop = new Properties();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testServiceOrder_Cockpit(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        
        String testDataFile = path.toString();
        String botId = g.getBotId();			
        record.startVideoRecording(botId, ScenarioName);
		try
		{
			String Scenario_Name = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "Scenario_Name");
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			
			if(Scenario_Name.contains("CPDRescheduleWindow_Verification")){
			Ctj.ValidateDetails_CPDRescheduleWindow(dataSheet, scriptNo, dataSetNo);
			}
			
			if(Scenario_Name.contains("AuditTrail"))
			{				
				//Ctj.addCPD(dataSheet, scriptNo, dataSetNo);
				//Ctj.OpenServiceOrderNumber(dataSheet, scriptNo, dataSetNo);
				//Ctj.auditTrail_CPDICDChange("CPD");
				Ctj.addUpdateICD(dataSheet, scriptNo, dataSetNo);
				Ctj.auditTrail_CPDICDChange("ICD");
			}
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
				Assert.assertTrue(false);
	    }
		finally{
			//		Terminating the Execution once it gets ended		
			Report.createTestCaseReportFooter();
			Report.SummaryReportlog(ScenarioName);
			ExtentTestManager.endTest();
			ExtentManager.getReporter().flush();
			record.stopVideoRecording();       
		}
	}
			
}
