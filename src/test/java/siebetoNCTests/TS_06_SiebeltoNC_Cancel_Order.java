package siebetoNCTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelLoginObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.ncFunctions.NcLogin;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebeltoNcFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebeltoNcFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNewOrderOnnetHelper;
import testHarness.siebelFunctions.SiebelNumberManagement;
import testHarness.siebeltoNcFunctions.AccountNavigation;
import testHarness.siebeltoNcFunctions.EthernetOrder;
import testHarness.siebeltoNcFunctions.NcCreateOrder;

public class TS_06_SiebeltoNC_Cancel_Order extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	Properties prop = new Properties();

	GlobalVariables g = new GlobalVariables();

	SiebelAccounts Accounts = new SiebelAccounts();
	SiebelLoginPage SiebelLogin = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddOrder = new SiebelCreateCustomerOrder();
	SiebelMode Mode = new SiebelMode();
	SiebelAddProduct AddProduct = new SiebelAddProduct();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testNewOrder(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) 
		{

            // load a properties file
            prop.load(input);
		} 
		catch (IOException ex) 
		{
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        String botId = g.getBotId();			
        record.startVideoRecording(botId, ScenarioName);

		try
		{
			String username = Configuration.NC_Username;
			String password = Configuration.NC_Password;
			String Nc_Flag = DataMiner.fngetcolvalue(dataSheet, scriptNo, dataSetNo, "NCFlag");
            if(!Nc_Flag.equalsIgnoreCase("Yes"))
            {
    			openurl(Configuration.Siebel_URL);
			    SiebelLogin.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
				Accounts.NavigateToAccounts();
				Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddOrder.createCustomerOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
            }
			Mode.CancelOrder(testDataFile, dataSheet, scriptNo, dataSetNo, username, password);
		    
		}
		catch(Exception e)
		{
            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
            Report.logToDashboard("Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Exception in "+ ScenarioName + ": "+e.getMessage());
			Assert.assertTrue(false);
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
		record.stopVideoRecording();       
//        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	
}
